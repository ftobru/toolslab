<?php namespace Core\Http\Responses;

use Illuminate\Routing\ResponseFactory as HttpResponseFactory;
use Input;

/**
 * Class Response
 * @package Core\Http\Responses
 */
class ResponseFactory extends HttpResponseFactory
{
    public function api($data = [], $status = 200, array $header = [], $options = 0)
    {
        if ($protocol = Input::get('p', false)) {
            return $this->jsonp($protocol, $data, $status, $header, $options);
        } else {
            return $this->json($data, $status, $header, $options);
        }
    }
}
