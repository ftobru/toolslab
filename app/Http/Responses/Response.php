<?php namespace Core\Http\Responses;

use Illuminate\Support\Facades\Facade;

/**
 * Class Response
 * @package Core\Http\Responses
 */
class Response extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'response';
    }

}
