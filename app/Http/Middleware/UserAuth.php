<?php namespace Core\Http\Middleware;

use Saas\References\UserReference;
use Saas\Models\User;
use Saas\Services\UserService;
use Auth;
use Closure;
use Illuminate\Http\Request as Request;
use Response;

/**
 * Class UserAuth
 * @package Core\Http\Middleware
 */
class UserAuth
{
    /** @var UserService  */
    protected $service;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param Request $request
     * @param callable $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|
     * \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::getUser();
            if ($user->status === UserReference::STATUS_ACTIVE) {
                return $next($request);
            } else {
                abort(403);
            }

        }
        return redirect()->guest('auth/login');
    }
}
