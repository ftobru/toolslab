<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Route::get('/', 'WelcomeController@index');

// Api

//Route::group(['prefix' => 'api/v1/auth', 'namespace' => 'Saas\Http\Controllers\Auth', 'before' => 'oauth'], function () {
//    Route::resource('password', 'PasswordController', ['only' => ['store']]);
//    Route::resource('registration', 'RegisterController', ['only' => ['store', 'update']]);
//    Route::resource('rememberToken', 'RememberTokenController', ['only' => ['index', 'store']]);
//    Route::resource('login', 'LoginController', ['only' => ['store']]);
//    Route::resource('logout', 'LogoutController', ['only' => ['index']]);
//});


//Route::group(['prefix' => 'service', 'namespace' => 'Saas\Http\Controllers\Service'], function () {
//    Route::resource('oauth/token', 'OauthTokenController', ['only' => ['store']]);
//});
//
//Route::group([
//    'prefix' => 'api/v1',
//    'middleware' => ['jwt.auth'],
//    'before' => 'oauth'
//], function () {
//    Route::group(['prefix' => 'crm', 'namespace' => 'Crm\Http\Controllers\Api'], function () {
//        Route::resource('product', 'ProductController');
//        Route::resource('client', 'ClientController');
//        Route::resource('task', 'TaskController');
//        Route::resource('order', 'OrderController');
//        Route::resource('cart', 'CartController');
//    });
//
//    Route::group(['prefix' => 'saas', 'namespace' => 'Saas\Http\Controllers\Api'], function () {
//        Route::resource('user', 'UserController');
//        Route::resource('form', 'FormController');
//        Route::resource('attachUserToCompanyDivision', 'AttachDivisionController', ['only' => 'store']);
//        Route::resource('formEntity', 'FormEntityController');
//        Route::resource('company', 'CompanyController');
//        Route::resource('attachUserToCompany', 'AttachCompanyController', ['only' => 'store']);
//        Route::resource('profile', 'ProfileController', ['only' => ['index', 'store']]);
//        Route::resource('otherCompany', 'OtherCompanyController', ['only' => 'index']);
//        Route::resource('ownCompany', 'OwnCompanyController', ['only' => 'index']);
//        Route::resource('checkActivationCode', 'CheckActivationCodeController', ['only' => 'index']);
//
//    });
//
//    Route::group(['prefix' => 'landing', 'namespace' => 'LandingControl\Http\Controllers\Api'], function () {
//        Route::controller('filemanager', 'FilemanagerLaravelController');
//        Route::resource('insertLink', 'InsertLinkController');
//        Route::resource('replacementGeo', 'ReplacementGeoController');
//        Route::resource('replacementLink', 'ReplacementLinkController');
//        Route::resource('replacementParam', 'ReplacementParamController');
//        Route::resource('replacementTime', 'ReplacementTimeController');
//        Route::resource('domain', 'DomainController');
//    });
//
//    Route::group(['prefix' => 'billing', 'namespace' => 'Billing\Http\Controllers\Api'], function () {
//        Route::resource('account', 'BillingAccountController', ['only' => 'index']);
//        Route::resource('invoice', 'BillingInvoiceController', ['only' => 'index']);
//        Route::resource('payment', 'BillingPaymentController', ['only' => 'index']);
//    });
//});

Route::group([
    'prefix' => 'api/v1',
    'middleware' => ['jwt.auth'],
    'before' => 'oauth'
], function () {
    Route::group(['prefix' => 'crm', 'namespace' => 'Crm\Http\Controllers\Api'], function () {
        Route::resource('product', 'ProductController');
        Route::resource('client', 'ClientController');
        Route::resource('task', 'TaskController');
        Route::resource('order', 'OrderController');
        Route::resource('cart', 'CartController');
    });

    Route::group(['prefix' => 'saas', 'namespace' => 'Saas\Http\Controllers\Api'], function () {
        Route::resource('user', 'UserController');
        Route::resource('form', 'FormController');
        Route::resource('attachUserToCompanyDivision', 'AttachDivisionController', ['only' => 'store']);
        Route::resource('formEntity', 'FormEntityController');
        Route::resource('company', 'CompanyController');
        Route::resource('attachUserToCompany', 'AttachCompanyController', ['only' => 'store']);
        Route::resource('profile', 'ProfileController', ['only' => ['index', 'store']]);
        Route::resource('otherCompany', 'OtherCompanyController', ['only' => 'index']);
        Route::resource('ownCompany', 'OwnCompanyController', ['only' => 'index']);
        Route::resource('checkActivationCode', 'CheckActivationCodeController', ['only' => 'index']);

    });

    Route::group(['prefix' => 'landing', 'namespace' => 'LandingControl\Http\Controllers\Api'], function () {
        Route::controller('filemanager', 'FilemanagerLaravelController');
        Route::resource('insertLink', 'InsertLinkController');
        Route::resource('replacementGeo', 'ReplacementGeoController');
        Route::resource('replacementLink', 'ReplacementLinkController');
        Route::resource('replacementParam', 'ReplacementParamController');
        Route::resource('replacementTime', 'ReplacementTimeController');
        Route::resource('domain', 'DomainController');
    });

    Route::group(['prefix' => 'billing', 'namespace' => 'Billing\Http\Controllers\Api'], function () {
        Route::resource('account', 'BillingAccountController', ['only' => 'index']);
        Route::resource('invoice', 'BillingInvoiceController', ['only' => 'index']);
        Route::resource('payment', 'BillingPaymentController', ['only' => 'index']);
    });
});

// Application

Route::get('/', 'Saas\Http\Controllers\App\IndexController@index');
Route::get('/about', 'Saas\Http\Controllers\App\IndexController@about');
Route::get('/analitycs', 'Saas\Http\Controllers\App\IndexController@analitycs');
Route::get('/multilanding', 'Saas\Http\Controllers\App\IndexController@multilanding');
Route::get('/cms', 'Saas\Http\Controllers\App\IndexController@cms');
Route::get('/traffic', 'Saas\Http\Controllers\App\IndexController@traffic');
/*Route::get('/beta-test', 'Saas\Http\Controllers\App\IndexController@regCallback');*/



Route::get('/recovery_password', 'Saas\Http\Controllers\App\UserController@recoveryUserPassword');
Route::get('/link_request', 'Saas\Http\Controllers\App\IndexController@linkRequest');
Route::get('/demo', ['as' => 'demo', 'uses' => 'Saas\Http\Controllers\App\DemoController@index']);

Route::group([
    'prefix' => 'companies',
    'namespace' => 'Saas\Http\Controllers\App',
    'middleware' => ['auth']
], function () {
    Route::get('my', ['as' => 'my', 'uses' => 'CompanyController@my']);

    Route::group(['prefix' => 'ajax'], function () {
        Route::post('createCompany', ['as' => 'ajax.createCompany', 'uses' => 'CompanyController@createCompany']);
    });
});

Route::group(['prefix' => 'auth', 'namespace' => 'Saas\Http\Controllers\App'], function () {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::get('registration', ['as' => 'registration', 'uses' => 'AuthController@getRegistration']);
    Route::get('forgetPassword', ['as' => 'forgetPassword', 'uses' => 'AuthController@getForgetPassword']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
    Route::get('invite-confirmation', ['as' => 'invite.confirmation', 'uses' => 'AuthController@getInviteConfirmation']);





    Route::group(['prefix' => 'ajax'], function () {
        Route::post('login', ['as' => 'ajax.login', 'uses' => 'AuthController@postLogin']);
        Route::post('completeInviteConfirmation', ['as' => 'ajax.invite.complete', 'uses' => 'AuthController@postInviteComplete']);
        Route::post('registration', ['as' => 'ajax.registration', 'uses' => 'AuthController@postRegistration']);
        Route::post('forgetPassword', ['as' => 'ajax.forgetPassword', 'uses' => 'AuthController@postForgetPassword']);
        Route::post(
            'updateRegistration',
            [
                'as' => 'ajax.updateRegistration',
                'uses' => 'AuthController@updateRegistration'
            ]
        );
        Route::post(
            'activateAccount',
            [
                'as' => 'ajax.activateAccount',
                'uses' => 'AuthController@postActivationCode'
            ]
        );
    });

    Route::group(['prefix' => 'ajax', 'middleware' => ['notActiveUser']], function () {
        Route::post(
            'resendActivationCode',
            [
                'as' => 'ajax.resendActivationCode',
                'uses' => 'AuthController@resendActivationCode'
            ]
        );
    });
});

//Route::group(['prefix' => 'auth', 'namespace' => 'Saas\Http\Controllers\App'], function () {
//    Route::group(['prefix' => 'ajax', 'middleware' => ['auth']], function () {
//        Route::post(
//            'resendActivationCode',
//            [
//                'as' => 'ajax.resendActivationCode',
//                'uses' => 'AuthController@resendActivationCode'
//            ]
//        );
//    });
//});


Route::group(['prefix' => 'cabinet', 'namespace' => 'Saas\Http\Controllers\App'], function () {
    //Route::get('/', ['as' => 'cabinet.index', 'uses' => 'CabinetController@index']);
    //Route::get('profile', ['as' => 'cabinet.profile', 'uses' => 'CabinetController@profile']);
});

Route::group(['prefix' => 'user', 'namespace' => 'Saas\Http\Controllers\App'], function () {
    Route::get('/', ['as' => 'user.profile', 'uses' => 'UserController@index']);
    Route::get('/restorePassword', ['as' => 'user.restorePasssword', 'uses' => 'UserController@restorePassword']);
    Route::group(['prefix' => 'ajax'], function () {
        Route::post('changeName', ['as' => 'user.changeName', 'uses' => 'UserController@postAjaxChangeName']);
        Route::post('changeEmailPhone', ['as' => 'user.changeEmailPhone', 'uses' => 'UserController@postAjaxChangeEmailPhone']);
        Route::post('changePassword', ['as' => 'user.changePassword', 'uses' => 'UserController@postAjaxChangePassword']);
        Route::post('checkUserEmail', ['as' => 'ajax.user.checkUserEmail', 'uses' => 'UserController@postAjaxCheckUserEmail']);
        Route::post('/newPassword', ['as' => 'ajax.user.restorePasssword', 'uses' => 'UserController@postAjaxSetNewPassword']);
        Route::post('demoRegistration', ['as' => 'user.demoRegistration', 'uses' => 'UserController@postAjaxDemoRegistration']);
    });
});

Route::group(['prefix' => '{company_alias}/crm',
    'where' => ['company_alias' => '[0-9a-z]+'],
    'middleware' => ['auth', 'company'],
    'namespace' => 'Crm\Http\Controllers\App'],
    function () {
        Route::get('dashboard', ['as' => 'crm.dashboard', 'uses' => 'DashboardController@index', 'middleware' => 'crm:dashboard,menu']);
        Route::get('products', ['as' => 'crm.products', 'uses' => 'ProductController@index', 'middleware' => 'crm:settings,fields']);
        Route::get('products/fields', ['as' => 'crm.products.fields', 'uses' => 'ProductController@fields', 'middleware' => 'crm:products,view']);
        Route::get('product/edit/{product_id}', ['as' => 'crm.product.edit', 'uses' => 'ProductController@edit', 'middleware' => 'crm:products,edit']);
        Route::get('firms', ['as' => 'crm.firms', 'uses' => 'FirmController@index', 'middleware' => 'crm:firms,view']);
        Route::get('firms/fields', ['as' => 'crm.firms.fields', 'uses' => 'FirmController@fields', 'middleware' => 'crm:settings,fields']);
        Route::get('contacts/fields', ['as' => 'crm.clients.fields', 'uses' => 'ClientController@fields', 'middleware' => 'crm:settings,fields']);
        Route::get('contacts', ['as' => 'crm.clients', 'uses' => 'ClientController@index', 'middleware' => 'crm:contacts,view']);
        Route::get('deals', ['as' => 'crm.deals', 'uses' => 'OrderController@index', 'middleware' => 'crm:deals,view']);
        Route::get('tasks', ['as' => 'crm.tasks', 'uses' => 'TaskController@index', 'middleware' => 'crm:tasks,view']);
        Route::get('tasks-tiles', ['as' => 'crm.tasks.tiles', 'uses' => 'TaskController@indexTiles', 'middleware' => 'crm:tasks,view']);
        // Analytics
        Route::get('analytics/users/orders', ['as' => 'analytics.users.orders', 'uses' => 'AnalyticsUsersController@orders']);
        Route::get('analytics/users/contacts', ['as' => 'analytics.users.contacts', 'uses' => 'AnalyticsUsersController@contacts']);
        Route::get('analytics/users/tasks', ['as' => 'analytics.users.tasks', 'uses' => 'AnalyticsUsersController@tasks']);

        Route::get('analytics/sales', ['as' => 'analytics.sales', 'uses' => 'AnalyticsSalesController@index', 'middleware' => 'crm:analytics,deals']);

        Route::get('scripts', ['as' => 'crm.scripts']);
        Route::get('statuses', ['as' => 'crm.statuses']);

        Route::get('orders', ['as' => 'crm.statistics']);

        Route::post('product/edit/{product_id}', ['as' => 'crm.product.edit', 'uses' => 'ProductController@postEdit', 'middleware' => 'crm:products,edit']);
        // Card
        Route::group(['prefix' => 'card'], function () {
            Route::pattern('type', '[0-9]+');
            Route::get('{type}/{id}', ['as' => 'crm.card', 'uses' => 'CardController@getShow']);

            Route::get('ajaxAttachFirm', ['as' => 'crm.card.ajax.ajaxAttachFirm', 'uses' => 'CardController@ajaxAttachFirm']);
            Route::get('ajaxAttachClient', ['as' => 'crm.card.ajax.ajaxAttachClient', 'uses' => 'CardController@ajaxAttachClient']);
            Route::get('ajaxCreateClient', ['as' => 'crm.card.ajax.statistics', 'uses' => 'CardController@ajaxCreateClient']);
            Route::get('ajaxGetOrdersByFirm', ['as' => 'crm.card.ajax.ajaxGetOrdersByFirm', 'uses' => 'CardController@ajaxGetOrdersByFirm']);
            Route::get('ajaxGetOrdersByClientId', ['as' => 'crm.card.ajax.ajaxGetOrdersByClientId', 'uses' => 'CardController@ajaxGetOrdersByClientId']);
            Route::get('ajaxGetTasksByClientId', ['as' => 'crm.card.ajax.ajaxGetTasksByClientId', 'uses' => 'CardController@ajaxGetTasksByClientId']);
            Route::get('ajaxGetTasksByFirm', ['as' => 'crm.card.ajax.ajaxGetTasksByFirm', 'uses' => 'CardController@ajaxGetTasksByFirm']);
            Route::get('ajaxGetStatisticsByFirm', ['as' => 'crm.card.ajax.ajaxGetStatisticsByFirm', 'uses' => 'CardController@ajaxGetStatisticsByFirm']);
            Route::get('ajaxUpdateFirm', ['as' => 'crm.card.ajax.ajaxUpdateFirm', 'uses' => 'CardController@ajaxUpdateFirm']);
            Route::get('ajaxUpdateClient', ['as' => 'crm.card.ajax.ajaxUpdateClient', 'uses' => 'CardController@ajaxUpdateClient']);
            Route::get('ajaxGetClient/{id}', ['as' => 'crm.card.ajax.ajaxGetClient', 'uses' => 'CardController@ajaxGetClient']);

        });

        Route::group(['prefix' => 'ajax'], function () {

            // Заказы
            Route::group(['prefix' => 'deals'], function () {
                Route::post('get', ['as' => 'ajax.deals.get', 'uses' => 'OrderController@getOrders', 'middleware' => 'crm:deals,view']);
                Route::post('create', ['as' => 'ajax.deals.create', 'uses' => 'OrderController@createOrder', 'middleware' => 'crm:deals,create']);
                Route::put('update', ['as' => 'ajax.deals.update', 'uses' => 'OrderController@updateOrder', 'middleware' => 'crm:deals,edit']);
                Route::post('delete', ['as' => 'ajax.deals.delete', 'uses' => 'OrderController@deleteOrder', 'middleware' => 'crm:deals,delete']);
            });

            // Задачи
            Route::group(['prefix' => 'tasks'], function () {
                Route::post('get', ['as' => 'ajax.tasks.get', 'uses' => 'TaskController@getTasks', 'middleware' => 'crm:tasks,view']);
                Route::post('getByID', ['as' => 'ajax.tasks.getByID', 'uses' => 'TaskController@getTaskByID', 'middleware' => 'crm:tasks,view']);
                Route::post('getTaskTiles', ['as' => 'ajax.tasks.getTiles', 'uses' => 'TaskController@getTaskTiles', 'middleware' => 'crm:tasks,view']);
                Route::post('create', ['as' => 'ajax.tasks.create', 'uses' => 'TaskController@createTask', 'middleware' => 'crm:tasks,create']);
                Route::post('complete', ['as' => 'ajax.tasks.complete', 'uses' => 'TaskController@completeTask', 'middleware' => 'crm:tasks,edit']);
                Route::put('update', ['as' => 'ajax.tasks.update', 'uses' => 'TaskController@updateTask', 'middleware' => 'crm:tasks,edit']);
                Route::post('delete', ['as' => 'ajax.tasks.delete', 'uses' => 'TaskController@deleteTask', 'middleware' => 'crm:tasks,delete']);
            });

            // Сотрудники
            Route::group(['prefix' => 'employees'], function () {
                Route::post('get', ['as' => 'ajax.employees.get', 'uses' => 'CompanyController@getEmployees', 'middleware' => 'crm:employees,view']);
                Route::post('getByID', ['as' => 'ajax.employees.getByID', 'uses' => 'CompanyController@getEmployeeByID', 'middleware' => 'crm:employees,view']);
                Route::post('invite', ['as' => 'ajax.employees.invite', 'uses' => 'CompanyController@inviteEmployee', 'middleware' => 'crm:employees,invite']);
                Route::post('detach', ['as' => 'ajax.employees.detach', 'uses' => 'CompanyController@detachEmployee', 'middleware' => 'crm:employees,delete']);
            });

            // Продукты
            Route::group(['prefix' => 'products'], function () {
                Route::post('get', ['as' => 'ajax.products.get', 'uses' => 'ProductController@getProducts', 'middleware' => 'crm:products,view']);
                Route::post('create', ['as' => 'ajax.products.create', 'uses' => 'ProductController@createProduct', 'middleware' => 'crm:products,create']);
                Route::put('update', ['as' => 'ajax.products.update', 'uses' => 'ProductController@updateProduct', 'middleware' => 'crm:products,edit']);
                Route::post('delete', ['as' => 'ajax.products.delete', 'uses' => 'ProductController@deleteProduct', 'middleware' => 'crm:products,delete']);
            });

            // Фирмы
            Route::group(['prefix' => 'firms'], function () {
                Route::post('get', ['as' => 'ajax.firms.get', 'uses' => 'FirmController@getFirms', 'middleware' => 'crm:firms,view']);
                Route::post('create', ['as' => 'ajax.firms.create', 'uses' => 'FirmController@createFirm', 'middleware' => 'crm:firms,create']);
                Route::put('update', ['as' => 'ajax.firms.update', 'uses' => 'FirmController@updateFirm', 'middleware' => 'crm:firms,edit']);
                Route::post('delete', ['as' => 'ajax.firms.delete', 'uses' => 'FirmController@deleteFirm', 'middleware' => 'crm:firms,delete']);
            });

            // Контакты
            Route::group(['prefix' => 'contacts'], function () {
                Route::post('get', ['as' => 'ajax.contacts.get', 'uses' => 'ClientController@getClients', 'middleware' => 'crm:contacts,view']);
                Route::post('create', ['as' => 'ajax.contacts.create', 'uses' => 'ClientController@createClient', 'middleware' => 'crm:contacts,create']);
                Route::put('update', ['as' => 'ajax.contacts.update', 'uses' => 'ClientController@updateClient', 'middleware' => 'crm:contacts,edit']);
                Route::post('delete', ['as' => 'ajax.contacts.delete', 'uses' => 'ClientController@deleteClient', 'middleware' => 'crm:contacts,delete']);
            });
        });
    });

Route::group([
    'prefix' => '{company_alias}/landingControl',
    'middleware' => ['auth', 'company'],
    'namespace' => 'LandingControl\Http\Controllers\App'
], function () {
//    Route::get('/', ['as' => 'landingControl']);
//    Route::get('/templates', ['as' => 'landingControl.templates']);
//    Route::get('/replace', ['as' => 'landingControl.replace']);

    Route::get('/replacement-params', ['as' => 'landingControl.replacementParams', 'uses' => 'ReplacementParamController@index', 'middleware'=>'landingControl:multylanding,variables']);
    Route::get('/replacement-param/edit/{replacement_param_id}', ['as' => 'landingControl.replacementParams.edit', 'uses' => 'ReplacementParamController@edit', 'middleware'=>'landingControl:multylanding,variables']);
    Route::post('/replacement-param/edit/{replacement_param_id}', ['as' => 'landingControl.replacementParams.edit.post', 'uses' => 'ReplacementParamController@postEdit', 'middleware'=>'landingControl:multylanding,variables']);

    Route::get('/replacement-times', ['as' => 'landingControl.replacementTimes', 'uses' => 'ReplacementTimeController@index', 'middleware'=>'landingControl:multylanding,time-replacement']);
    Route::get('/replacement-time/edit/{replacement_time_id}', ['as' => 'landingControl.replacementTime.edit', 'uses' => 'ReplacementTimeController@edit', 'middleware'=>'landingControl:multylanding,time-replacement']);
    Route::post('/replacement-time/edit/{replacement_time_id}', ['as' => 'landingControl.replacementTime.edit.post', 'uses' => 'ReplacementTimeController@postEdit', 'middleware'=>'landingControl:multylanding,time-replacement']);

    Route::get('/replacement-links', ['as' => 'landingControl.replacementLinks', 'uses' => 'ReplacementLinkController@index', 'middleware'=>'landingControl:multylanding,link-replacement']);
    Route::get('replacement-link/edit/{replacement_link_id}', ['as' => 'landingControl.replacementLinks.edit', 'uses' => 'ReplacementLinkController@edit', 'middleware'=>'landingControl:multylanding,link-replacement']);
    Route::post('replacement-link/edit/{replacement_link_id}', ['as' => 'landingControl.replacementLinks.edit.post', 'uses' => 'ReplacementLinkController@postEdit', 'middleware'=>'landingControl:multylanding,link-replacement']);
    Route::get('replacement-link/create', ['as' => 'landingControl.replacementLinks.create', 'uses' => 'ReplacementLinkController@create', 'middleware'=>'landingControl:multylanding,link-replacement']);
    Route::post('replacement-link/create', ['as' => 'landingControl.replacementLinks.create.post', 'uses' => 'ReplacementLinkController@postCreate', 'middleware'=>'landingControl:multylanding,link-replacement']);

    Route::get('/replacement-products', ['as' => 'landingControl.replacementProducts', 'uses' => 'ReplacementProductController@index', 'middleware'=>'landingControl:multylanding,product-replacement']);
    Route::get('/replacement-product/edit/{replacement_product_id}', ['as' => 'landingControl.replacementProduct.edit', 'uses' => 'ReplacementProductController@edit', 'middleware'=>'landingControl:multylanding,product-replacement']);
    Route::get('/replacement-product/create', ['as' => 'landingControl.replacementProduct.create', 'uses' => 'ReplacementProductController@create', 'middleware'=>'landingControl:multylanding,product-replacement']);

    Route::get('/replacement-geo', ['as' => 'landingControl.replacementGeo', 'uses' => 'ReplacementGeoController@index', 'middleware'=>'landingControl:multylanding,geo-replacement']);
    Route::get('/replacement-geo/edit/{replacement_geo_id}', ['as' => 'landingControl.replacementGeo.edit', 'uses' => 'ReplacementGeoController@edit', 'middleware'=>'landingControl:multylanding,geo-replacement']);
    Route::post('/replacement-geo/edit/{replacement_geo_id}', ['as' => 'landingControl.replacementGeo.edit.post', 'uses' => 'ReplacementGeoController@postEdit', 'middleware'=>'landingControl:multylanding,geo-replacement']);
    Route::get('/replacement-geo/create', ['as' => 'landingControl.replacementGeo.create', 'uses' => 'ReplacementGeoController@create', 'middleware'=>'landingControl:multylanding,geo-replacement']);
    Route::post('/replacement-geo/create', ['as' => 'landingControl.replacementGeo.create.post', 'uses' => 'ReplacementGeoController@postCreate', 'middleware'=>'landingControl:multylanding,geo-replacement']);

    Route::get('/insert-links', ['as' => 'landingControl.insertLinks', 'uses' => 'InsertLinkController@index', 'middleware'=>'landingControl:multylanding,link-insert']);
    Route::get('insert-link/edit/{ins_link_id}', ['as' => 'landingControl.insertLinks.edit', 'uses' => 'InsertLinkController@edit', 'middleware'=>'landingControl:multylanding,link-insert']);
    Route::get('insert-link/create', ['as' => 'landingControl.insertLinks.create', 'uses' => 'InsertLinkController@create', 'middleware'=>'landingControl:multylanding,link-insert']);
    Route::post('insert-link/create', ['as' => 'landingControl.insertLinks.create.post', 'uses' => 'InsertLinkController@postCreate', 'middleware'=>'landingControl:multylanding,link-insert']);
    Route::post('insert-link/edit/{ins_link_id}', ['as' => 'landingControl.insertLinks.edit.post', 'uses' => 'InsertLinkController@postEdit', 'middleware'=>'landingControl:multylanding,link-insert']);

    Route::get('/geo-splits', ['as' => 'landingControl.geoSplits', 'uses' => 'GeoSplitController@index', 'middleware'=>'landingControl:create,geo-split']);
    Route::get('/geo-split/edit/{geo_split_id}', ['as' => 'landingControl.geoSplits.edit', 'uses' => 'GeoSplitController@edit', 'middleware'=>'landingControl:create,geo-split']);
    Route::get('/geo-split/create', ['as' => 'landingControl.geoSplits.create', 'uses' => 'GeoSplitController@create', 'middleware'=>'landingControl:create,geo-split']);

    Route::get('/ab-splits', ['as' => 'landingControl.abSplits', 'uses' => 'ABSplitController@index', 'middleware'=>'landingControl:create,ab-test']);
    Route::get('/ab-split/edit/{ab_split_id}', ['as' => 'landingControl.abSplits.edit', 'uses' => 'ABSplitController@edit', 'middleware'=>'landingControl:create,ab-test']);
    Route::get('/ab-split/create', ['as' => 'landingControl.abSplits.create', 'uses' => 'ABSplitController@create', 'middleware'=>'landingControl:create,ab-test']);

    Route::get('/templates', ['as' => 'landingControl.templates', 'uses' => 'TemplateController@index', 'middleware'=>'landingControl:create,templates']);
    Route::get('/template/edit/{template_id}', ['as' => 'landingControl.templates.edit', 'uses' => 'TemplateController@edit', 'middleware'=>'landingControl:create,templates']);
    Route::get('/template/create', ['as' => 'landingControl.templates.create', 'uses' => 'TemplateController@create', 'middleware'=>'landingControl:create,templates']);

    Route::get('/links', ['as' => 'landingControl.links', 'uses' => 'LinkController@index', 'middleware'=>'landingControl:create,links']);
    //Route::get('/links/edit/{link_id}', ['as' => 'landingControl.links.edit', 'uses' => 'LinkController@edit']);
    Route::get('/links/constructor', ['as' => 'landingControl.links.create', 'uses' => 'LinkController@create', 'middleware'=>'landingControl:create,links']);
    Route::get('/links/multiplier', ['as' => 'landingControl.links.multiplier', 'uses' => 'LinkController@multiplier', 'middleware'=>'landingControl:create,links']);
    Route::get('/links/geodirect', ['as' => 'landingControl.links.geodirect', 'uses' => 'LinkController@geodirect', 'middleware'=>'landingControl:create,links']);

    Route::get('/utms', ['as' => 'landingControl.utms', 'uses' => 'UTMController@index']);
    Route::get('/utm/edit/{utm_id}', ['as' => 'landingControl.utms.edit', 'uses' => 'UTMController@edit']);
    Route::get('/utm/create', ['as' => 'landingControl.utms.create', 'uses' => 'UTMController@create']);

    Route::get('/domains', ['as' => 'landingControl.domains', 'uses' => 'DomainController@index', 'middleware'=>'landingControl:create,domains']);
    Route::post('/domains/postAjaxCreateDomain', ['as' => 'landingControl.domains.ajax.postAjaxCreateDomain', 'uses' => 'DomainController@postAjaxCreateDomain', 'middleware'=>'landingControl:create,domains']);
    Route::post('/domains/postAjaxCreateSubDomain', ['as' => 'landingControl.domains.ajax.postAjaxCreateSubDomain', 'uses' => 'DomainController@postAjaxCreateSubDomain', 'middleware'=>'landingControl:create,domains']);

    Route::get('/landingForms', ['as' => 'landingControl.landingForms', 'uses' => 'LandingFormController@index', 'middleware'=>'landingControl:create,forms']);
    Route::get('/landingForms/create', ['as' => 'landingControl.landingForms.create', 'uses' => 'LandingFormController@create', 'middleware'=>'landingControl:create,forms']);
    Route::get('/landingForms/edit/{form_id}', ['as' => 'landingControl.landingForms.update', 'uses' => 'LandingFormController@edit', 'middleware'=>'landingControl:create,forms']);

    Route::get('/filemanager', ['as' => 'landingControl.filemanager', 'uses' => 'ElfinderManagerController@showIndex', 'middleware'=>'landingControl:create,file-manager']);

    Route::group(['prefix' => 'ajax'], function () {

        // Замены параметров
        Route::group(['prefix' => 'replacementParams', 'middleware'=>'landingControl:multylanding,variables'], function () {
            Route::post('get', ['as' => 'ajax.replacementParams.get', 'uses' => 'ReplacementParamController@getParams']);
            Route::post('create', ['as' => 'ajax.replacementParams.create', 'uses' => 'ReplacementParamController@createParam']);
            Route::put('update', ['as' => 'ajax.replacementParams.update', 'uses' => 'ReplacementParamController@updateParam']);
            Route::post('delete', ['as' => 'ajax.replacementParams.delete', 'uses' => 'ReplacementParamController@deleteParam']);
        });

        // Замены времени
        Route::group(['prefix' => 'replacementTimes','middleware'=>'landingControl:multylanding,time-replacement'], function () {
            Route::post('get', ['as' => 'ajax.replacementTimes.get', 'uses' => 'ReplacementTimeController@getTimes']);
            Route::post('create', ['as' => 'ajax.replacementTimes.create', 'uses' => 'ReplacementTimeController@createTime']);
            Route::put('update', ['as' => 'ajax.replacementTimes.update', 'uses' => 'ReplacementTimeController@updateTime']);
            Route::post('delete', ['as' => 'ajax.replacementTimes.delete', 'uses' => 'ReplacementTimeController@deleteTime']);
        });

        // Линк замены
        Route::group(['prefix' => 'replacementLinks', 'middleware'=>'landingControl:multylanding,link-replacement'], function () {
            Route::post('get', ['as' => 'ajax.replacementLinks.get', 'uses' => 'ReplacementLinkController@getLinks']);
            Route::post('delete', ['as' => 'ajax.replacementLinks.delete', 'uses' => 'ReplacementLinkController@delete']);
            Route::post('getById', ['as' => 'ajax.replacementLinks.getById', 'uses' => 'ReplacementLinkController@getLinkById']);
        });

        // Товар-замены
        Route::group(['prefix' => 'replacementProducts', 'middleware'=>'landingControl:multylanding,product-replacement'], function () {
            Route::post('get', ['as' => 'ajax.replacementProducts.get', 'uses' => 'ReplacementProductController@getProducts']);
            Route::post('getById', ['as' => 'ajax.replacementProducts.getById', 'uses' => 'ReplacementProductController@getProductById']);
            Route::put('update', ['as' => 'ajax.replacementProducts.update', 'uses' => 'ReplacementProductController@updateReplacementProduct']);
            Route::post('create', ['as' => 'ajax.replacementProducts.create', 'uses' => 'ReplacementProductController@createReplacementProduct']);
            Route::post('delete', ['as' => 'ajax.replacementProducts.delete', 'uses' => 'ReplacementProductController@delete']);
        });

        // Линк вставки
        Route::group(['prefix' => 'insertLinks','middleware'=>'landingControl:multylanding,link-insert'], function () {
            Route::post('get', ['as' => 'ajax.insertLinks.get', 'uses' => 'InsertLinkController@getLinks']);
            Route::post('delete', ['as' => 'ajax.insertLinks.delete', 'uses' => 'InsertLinkController@delete']);
            Route::post('getById', ['as' => 'ajax.insertLinks.getById', 'uses' => 'InsertLinkController@getLinkById']);
        });

        // Гео замены
        Route::group(['prefix' => 'replacementGeo', 'middleware'=>'landingControl:multylanding,geo-replacement'], function () {
            Route::post('get', ['as' => 'ajax.replacementGeo.get', 'uses' => 'ReplacementGeoController@getGeoReplacements']);
        });

        // LandingForms
        Route::group(['prefix' => 'LandingForms', 'middleware'=>'landingControl:create,forms'], function () {
            Route::post('create', ['as' => 'ajax.LandingForms.create', 'uses' => 'LandingFormController@postAjaxCreateLandingForm']);
            Route::post('get', ['as' => 'ajax.LandingForms.get', 'uses' => 'LandingFormController@getLandingForms']);
            Route::put('update', ['as' => 'ajax.LandingForms.update', 'uses' => 'LandingFormController@postAjaxUpdateLandingForm']);
            Route::post('delete', ['as' => 'ajax.LandingForms.delete', 'uses' => 'LandingFormController@postAjaxDeleteLandingForm']);
        });

        // Гео сплит
        Route::group(['prefix' => 'geoSplits', 'middleware'=>'landingControl:create,geo-split'], function () {
            Route::post('get', ['as' => 'ajax.geoSplits.get', 'uses' => 'GeoSplitController@getGeoSplits']);
            Route::post('create', ['as' => 'ajax.geoSplits.create', 'uses' => 'GeoSplitController@createGeoSplit']);
            Route::put('update', ['as' => 'ajax.geoSplits.update', 'uses' => 'GeoSplitController@updateGeoSplit']);
            Route::post('delete', ['as' => 'ajax.geoSplits.delete', 'uses' => 'GeoSplitController@deleteGeoSplit']);
            Route::post('getById', ['as' => 'ajax.geoSplits.getById', 'uses' => 'GeoSplitController@getGeoSplitById']);
            Route::post('updateTmp', ['as' => 'ajax.geoSplits.updateTmp', 'uses' => 'GeoSplitController@updateTmpGeoSplit']);
            Route::post('saveTmp', ['as' => 'ajax.geoSplits.saveTmp', 'uses' => 'GeoSplitController@saveTmpGeoSplit']);
            Route::post('removeDirect', ['as' => 'ajax.geoSplits.removeDirect', 'uses' => 'GeoSplitController@removeDirect']);
        });

        // AB split
        Route::group(['prefix' => 'abSplits', 'middleware'=>'landingControl:create,ab-test'], function () {
            Route::post('get', ['as' => 'ajax.abSplits.get', 'uses' => 'ABSplitController@getABSplits']);
            Route::post('create', ['as' => 'ajax.abSplits.create', 'uses' => 'ABSplitController@createABSplit']);
            Route::put('update', ['as' => 'ajax.abSplits.update', 'uses' => 'ABSplitController@updateABSplit']);
            Route::post('delete', ['as' => 'ajax.abSplits.delete', 'uses' => 'ABSplitController@deleteABSplit']);
            Route::post('getById', ['as' => 'ajax.abSplits.getById', 'uses' => 'ABSplitController@getABSplitById']);
            Route::post('updateTmp', ['as' => 'ajax.abSplits.updateTmp', 'uses' => 'ABSplitController@updateTmpABSplit']);
            Route::post('saveTmp', ['as' => 'ajax.abSplits.saveTmp', 'uses' => 'ABSplitController@saveTmpABSplit']);
        });

        // Templates
        Route::group(['prefix' => 'templates', 'middleware'=>'landingControl:create,templates'], function () {
            Route::post('get', ['as' => 'ajax.templates.get', 'uses' => 'TemplateController@getTemplates']);
            Route::post('create', ['as' => 'ajax.templates.create', 'uses' => 'TemplateController@createTemplate']);
            Route::put('update', ['as' => 'ajax.templates.update', 'uses' => 'TemplateController@updateTemplate']);
            Route::post('delete', ['as' => 'ajax.templates.delete', 'uses' => 'TemplateController@deleteTemplate']);
            Route::post('getById', ['as' => 'ajax.templates.getById', 'uses' => 'TemplateController@getTemplateById']);
        });

        // Links
        Route::group(['prefix' => 'links', 'middleware'=>'landingControl:create,links'], function () {
            Route::post('get', ['as' => 'ajax.links.get', 'uses' => 'LinkController@getLinks']);
            Route::post('create', ['as' => 'ajax.links.create', 'uses' => 'LinkController@createLink']);
            Route::post('constructLink', ['as' => 'ajax.links.constructLink', 'uses' => 'LinkController@constructLink']);
            Route::post('attacheGeoDirection', ['as' => 'ajax.links.attacheGeoDirection', 'uses' => 'LinkController@attacheGeoDirection']);
            Route::put('update', ['as' => 'ajax.links.update', 'uses' => 'LinkController@updateLink']);
            Route::post('delete', ['as' => 'ajax.links.delete', 'uses' => 'LinkController@deleteLink']);
            Route::post('getByID', ['as' => 'ajax.links.getByID', 'uses' => 'LinkController@getByID']);
        });

        // UTM
        Route::group(['prefix' => 'utms'], function () {
            Route::post('get', ['as' => 'ajax.utms.get', 'uses' => 'UTMController@getUTMs']);
            Route::post('create', ['as' => 'ajax.utms.create', 'uses' => 'UTMController@createUTM']);
            Route::put('update', ['as' => 'ajax.utms.update', 'uses' => 'UTMController@updateUTM']);
            Route::post('delete', ['as' => 'ajax.utms.delete', 'uses' => 'UTMController@deleteUTM']);
        });
    });

});

Route::group([
    'prefix' => '{company_alias}/billing',
    'where' => ['company_alias' => '[0-9a-z]+'],
    'middleware' => ['auth', 'company'],
    'namespace' => 'Billing\Http\Controllers\App',
], function () {
    // Modules billing
    Route::group(['prefix' => 'modules'], function () {
        Route::get('/', ['as' => 'billing.modules', 'uses' => 'ModulesController@getIndex']);
        Route::group(['prefix' => 'ajax'], function () {
            Route::post('postAjaxCrmAddUser', [
                'as' => 'billing.modules.ajax.postAjaxCrmAddUser', 'uses' => 'ModulesController@postAjaxCrmAddUser'
            ]);
        });
    });
});


Route::group(['prefix' => 'billing', 'namespace' => 'Billing\Http\Controllers\App'], function () {
    // Profile user billing
    Route::get('profile', ['as' => 'billing.profile', 'uses' => 'ProfileController@getIndex']);

    // Ajax
    Route::group(['prefix' => 'ajax'], function () {

        Route::group(['prefix' => 'profile'], function () {
            Route::get('balance', ['as' => 'billing.ajax.balance', 'uses' => 'ProfileController@getAjaxBalance']);
            Route::get('expensesMonth', [
                'as' => 'billing.ajax.expensesMonth', 'uses' => 'ProfileController@getAjaxExpensesMonth'
            ]);
            Route::post('paymentTariff', [
                'as' => 'billing.ajax.paymentTariff',
                'uses' => 'ProfileController@ajaxPaymentTariff'
            ]);
        });
    });

});


Route::group(['prefix' => '/landing/scripts', 'namespace' => 'LandingControl\Http\Controllers\App'], function () {
    Route::get('timeLinks', ['as' => 'scripts.timeLinks', 'uses' => 'ReplacementTimeController@getLinks']);
});

Route::group(['prefix' => 'support', 'namespace' => 'Support\Http\Controllers\Api'], function () {
    Route::get('/', ['as' => 'support']);
    Route::get('/', ['as' => 'support.api']);
});

Route::group(['prefix' => 'help', 'namespace' => 'Support\Http\Controllers\Api'], function () {
    Route::get('/', ['as' => 'help']);
});

Route::group(['prefix' => 'docs', 'namespace' => 'Support\Http\Controllers\Api'], function () {
    Route::get('/', ['as' => 'docs']);
});

Route::group(['prefix' => 'saas', 'namespace' => 'Saas\Http\Controllers\App'], function () {
    Route::get('/settings', ['as' => 'saas.settings']);

    //Route::get('/users', ['as' => 'saas.users']);
    //Route::get('/users', ['as' => 'saas.users', 'uses' => 'UsersController@index']);
});

Route::group(['prefix' => '{company_alias}/saas',
    'where' => ['company_alias' => '[0-9a-z]+'],
    'middleware' => ['auth', 'company'],
    'namespace' => 'Saas\Http\Controllers\App'],
    function () {
        //Route::get('/users', ['as' => 'saas.users', 'uses' => 'UsersController@index']);
        Route::get('/forms', ['as' => 'saas.users', 'uses' => 'FormController@index', 'middleware' => 'crm:settings,fields']);
        Route::get('/forms/create', ['as' => 'saas.forms.create', 'uses' => 'FormController@add', 'middleware' => 'crm:settings,fields']);
        Route::get('/forms/edit/{form_id}', ['as' => 'saas.forms.edit', 'uses' => 'FormController@edit', 'middleware' => 'crm:settings,fields']);
        Route::get('/companySettings', ['as' => 'saas.company.edit', 'uses' => 'CompanyController@edit']);
        Route::get('/employees', ['as' => 'saas.employees', 'uses' => 'UsersController@index', 'middleware'=>'crm:employees,view']);
        Route::get('/role/edit/{role_id}', ['as' => 'saas.roles.edit', 'uses' => 'RoleController@edit', 'middleware'=>'crm:employees,roles']);

        Route::group(['prefix' => 'ajax'], function () {
            // Roles
            Route::group(['prefix' => 'roles', 'middleware'=>'crm:employees,roles'], function () {
                Route::post('get', ['as' => 'ajax.roles.get', 'uses' => 'RoleController@ajaxGetRoles']);
                Route::post('create', ['as' => 'ajax.roles.create', 'uses' => 'RoleController@ajaxStoreRole']);
                Route::put('update', ['as' => 'ajax.roles.update', 'uses' => 'RoleController@ajaxUpdateRole']);
                Route::post('delete', ['as' => 'ajax.roles.delete', 'uses' => 'RoleController@ajaxRemoveRole']);
            });

            // Пользователи.
            Route::group(['prefix' => 'users'], function () {
                Route::post('get', ['as' => 'ajax.users.get', 'uses' => 'UsersController@getUsers', 'middleware'=>'crm:employees,view']);
                Route::post('create', ['as' => 'ajax.users.create', 'uses' => 'UsersController@createUser', 'middleware'=>'crm:employees,edit']);
                Route::put('update', ['as' => 'ajax.users.update', 'uses' => 'UsersController@updateUser', 'middleware'=>'crm:employees,edit']);
                Route::post('delete', ['as' => 'ajax.users.delete', 'uses' => 'UsersController@deleteUser', 'middleware'=>'crm:employees,delete']);

                Route::post('checkAddUser', ['as' => 'ajax.users.checkAddUser', 'uses' => 'UsersController@ajaxCheckAddUser', 'middleware'=>'crm:employees,invite']);
                Route::post('checkEmailExists', ['as'=> 'ajax.users.checkEmailExists', 'uses' => 'UsersController@checkEmailExists', 'middleware'=>'crm:employees,invite']);
                Route::post('inviteUser', ['as'=> 'ajax.users.inviteUser', 'uses' => 'UsersController@ajaxSendInvite', 'middleware'=>'crm:employees,invite']);
            });

            // Дополнительные поля.
            Route::group(['prefix' => 'forms', 'middleware' => 'crm:settings,fields'], function () {
                Route::post('get', ['as' => 'ajax.forms.get', 'uses' => 'FormController@getForms']);
                Route::post('create', ['as' => 'ajax.forms.create', 'uses' => 'FormController@createForm']);
                Route::put('update', ['as' => 'ajax.forms.update', 'uses' => 'FormController@updateForm']);
                Route::post('delete', ['as' => 'ajax.forms.delete', 'uses' => 'FormController@deleteForm']);
            });

            // Company
            Route::group(['prefix' => 'company'], function () {
                Route::put('update', ['as' => 'ajax.company.update', 'uses' => 'CompanyController@updateCompany']);
            });
        });
    }
);

Route::get('/landing/index.html', function(){
    return view('landings.test.index');
});

Route::get('/modulcrm.ru.html', function() {
    return view('index.ssl');
});

