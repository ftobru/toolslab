<?php namespace Core\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
        'Illuminate\Cookie\Middleware\EncryptCookies',
        'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
        'Illuminate\Session\Middleware\StartSession',
        'Illuminate\View\Middleware\ShareErrorsFromSession',
        //'Core\Http\Middleware\VerifyCsrfToken',
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => 'Core\Http\Middleware\UserAuth',
        'notActiveUser' => 'Core\Http\Middleware\NotActiveUser',
        'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
        'guest' => 'Core\Http\Middleware\RedirectIfAuthenticated',
        'csrf' => 'Core\Http\Middleware\VerifyCsrfToken',
        'oauth' => 'LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware',
        'jwt.auth' => 'Tymon\JWTAuth\Middleware\GetUserFromToken',
        'jwt.refresh' => 'Tymon\JWTAuth\Middleware\RefreshToken',
        'ref' => 'Saas\Http\Middleware\RefererSystemMiddleware',
        'company' => 'Saas\Http\Middleware\CheckUserToCompany',
        'crm' => 'Crm\Http\Middleware\CrmPermissionMiddleware',
        'landingControl' => 'LandingControl\Http\Middleware\LandingControlPermissionMiddleware',
    ];

}
