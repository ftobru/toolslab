<?php namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Saas\MenuServiceProvider;

/**
 * Регистрация сервис провайдеров
 * Class CoreServiceProvider
 * @package Core\Providers
 */
class CoreServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        // Saas
        $this->app->register('Saas\ServiceProvider');
        // Billing
        $this->app->register('Billing\ServiceProvider');
        // Crm
        $this->app->register('Crm\ServiceProvider');
        $this->app->register('Crm\EventServiceProvider');
        // LandingControl
        $this->app->register('LandingControl\ServiceProvider');

        // Local system providers
        if ($this->app->environment() === 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');

        }
    }

}
