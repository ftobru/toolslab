<?php namespace Core\Providers;

use Illuminate\Support\ServiceProvider as ServiceProvider;

/**
 * Composer blade templates
 * Class ComposersServiceProvider
 * @package Core\Providers
 */
class ComposersServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->view->composer('company.my-companies', 'Saas\Composers\UserCompaniesComposer');
        // Cabinet company alias
        $this->app->view->composer('*', 'Saas\Composers\CompanyAliasComposer');
        $this->app->view->composer('partials.cabinet.menu', 'Saas\Composers\MenuComposer');
        $this->app->view->composer('layouts.master_simple', 'Saas\Composers\SocketComposer');
        $this->app->view->composer('landings.domains.index', 'Saas\Composers\SocketComposer');
    }
}