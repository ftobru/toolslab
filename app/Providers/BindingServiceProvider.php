<?php namespace Core\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class BindServiceProvider
 * @package Core\Providers
 */
class BindingServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Helpers
        $this->app->bind('response', 'Core\Http\Responses\ResponseFactory');

    }
}
