<?php namespace Core\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 * @package Core\Providers
 */
class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'auth.login' => [
            'Saas\Handlers\Events\LastActivityEventHandler@handle',
        ],
        // Заплатили по счету
        'Billing\Events\InvoicePayEvent' => [
            'Billing\Handlers\Events\RemovalBalanceHandler@handle',
            'Billing\Handler\Events\ActivateRateHandler@handle',
        ],
        // Подключили тариф
        'Billing\Events\ConnectRateEvent' => [
            'Billing\Handlers\Events\BillInvoiceHandler@handler'
        ],
        // Пополнили счет
        'Billing\Events\CreditedToTheAccountEvent' => [],
        // Отключили тариф
        'Billing\Events\DisconnectRateEvent' => [],
        // Заплатили за продление тарифа
        'Billing\Events\ExtendServiceEvent' => [
            'Billing\Handlers\Events\RemovalBalanceHandler@handle'
        ],
        // Активировали тариф
        'Billing\Events\ActivateRateEvent' => [
            'Billing\Handler\Event\RemovePrevRateHandler@handle'
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
    }

}
