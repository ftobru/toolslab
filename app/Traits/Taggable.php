<?php namespace Core\Traits;

use Crm\Models\Client;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Taggable
 * @package Core\Traits
 */
trait Taggable
{

    /**
     * @param $model
     * @param array $tags
     */
    public function attachTagsToModel(&$model, array $tags)
    {
        $model->tag(implode(",", $tags));
    }

    /**
     * @param $model
     * @param $tags
     */
    public function retagTagsToModel(&$model, array $tags)
    {
        $model->retag($tags);
    }

    /**
     * @param $collection
     * @param array $metaData
     * @param $class
     */
    public function filterByTags(&$collection, array $metaData, $class)
    {
        // Уточнение по тегам.
        if (isset($metaData['filter']['withAnyTag'])) {
            /** @var Collection $withAnyTag */
            $withAnyTag = call_user_func([$class, 'withAnyTag'], $metaData['filter']['withAnyTag'])->get();
            $collection = $withAnyTag->intersect($collection);
        }
        if (isset($metaData['filter']['withAllTags'])) {
            /** @var Collection $withAllTags */
            $withAllTags = call_user_func([$class, 'withAllTags'], $metaData['filter']['withAllTags'])->get();
            $collection = $withAllTags->intersect($collection);
        }
        if ($collection) {
            foreach ($collection as $elem) {
                $elem->tagged;
            }
        }
    }
}

