<?php namespace Core\Traits;

use Core\Repositories\Eloquent\Criteria\SortCriteria;
use Config;
use Ftob\Repositories\Criteria\CriteriaCollection;

trait Sortable
{
    /**
     * @param array $metaData
     * @return CriteriaCollection
     */
    public function makeSortCriteriaCollection(array $metaData)
    {
        $criteriaArray = [];

        foreach ($metaData as $key => $value) {
            $sortType = (isset($value['dir'])
                && in_array($value['dir'], Config::get('grid.sortTypes')))
                ? $value['dir'] : Config::get('grid.default.sortType');
            $criteriaArray[] = new SortCriteria($value['field'], $sortType);
        }
        return CriteriaCollection::make($criteriaArray);
    }

}
