<?php namespace Core\Traits;

use Illuminate\Database\Eloquent\Collection;

/**
 * Class Groupable
 * @package Core\Traits
 */
trait Groupable
{
    protected $groupOptions;
    protected $groupAggregates;
    protected $counter;

    /**
     * @param $items
     * @param $level
     * @return array
     */
    private function group($items, $level)
    {
        if (isset($this->groupOptions[$level])) {
            $field = $this->groupOptions[$level]['field'];
            $dir = $this->groupOptions[$level]['dir'];
            $a = new Collection();
            foreach ($items as $item) {
                $a->push($item);
            }
            $a = $a->groupBy($field);

            $keys = array_keys($a->toArray());
            if ($dir === 'desc') {
                arsort($keys);
            } else {
                asort($keys);
            }

            $groups_array = [];
            $level++;
            foreach ($keys as $key) {
                $items = $this->group($a[$key], $level);
                $counter = 0;
                if ($items instanceof Collection) {
                    $counter = sizeof($items);
                } else {
                    $this->counter = 0;
                    $this->getCounter($items);
                    $counter = $this->counter;
                }


                if ($items) {
                    $elem = [
                        //'aggregates' => ['field' => $field, 'dir' => $dir],
                        'aggregates' => [
                            $field => [
                                'count' => $counter,//sizeof($items),
                            ],
//                            'status' => [
//                                'count' => sizeof($items),
//                            ],
//                            'type' => [
//                                'count' => sizeof($items),
//                            ],
                        ],
                        'field' => $field,
                        'hasSubgroups' => sizeof($this->groupOptions) == $level ? false : true,
                        'items' => $items,
                        'value' => $key
                    ];
                    array_push($groups_array, $elem);
                };
            };

            return $groups_array;
        }
        return $items;
    }

    /**
     * @param $arr
     */
    private function getCounter($arr)
    {
        foreach ($arr as $val) {
            if (isset($val['hasSubgroups'])) {
                if ($val['hasSubgroups']) {
                    $this->getCounter($val['items']);
                } else {
                    $this->counter += sizeof($val['items']);
                }
            } else {
                $a = 1;
            }

        }
    }
}
