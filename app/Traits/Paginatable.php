<?php namespace Core\Traits;

use Config;

/**
 * Class Paginatable
 * @package Core\Traits
 */
trait Paginatable
{
    /**
     * @param array $metaData
     * @return bool|mixed
     */
    public function getPerPage(array $metaData)
    {
        if (isset($metaData['pageSize'])) {
            $perPage = (isset($metaData['pageSize'])
                    && in_array($metaData['pageSize'], Config::get('grid.perPage'))
                ? $metaData['pageSize'] : Config::get('grid.default.perPage'));
        } else {
            $perPage = Config::get('grid.default.perPage');
        }
        return $perPage;
    }
}
