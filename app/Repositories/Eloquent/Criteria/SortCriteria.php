<?php namespace Core\Repositories\Eloquent\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class SortCriteria extends Criteria
{
    protected $attribute;

    protected $type;

    public function __construct($attribute, $type)
    {
        $this->attribute = $attribute;
        $this->type = $type;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->orderBy($this->attribute, $this->type);
        return $query;
    }
}
