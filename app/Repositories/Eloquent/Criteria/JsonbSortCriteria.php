<?php namespace Core\Repositories\Eloquent\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class JsonbSortCriteria
 * @package Core\Repositories\Eloquent\Criteria
 */
class JsonbSortCriteria extends Criteria
{

    protected $column;
    protected $jsonField;
    protected $orderBy;

    /**
     * @param string $column
     * @param string $jsonField
     * @param string $orderBy
     */
    public function __construct($column, $jsonField, $orderBy = "ASC")
    {
        $this->column = $column;
        $this->jsonField = $jsonField;
        $this->orderBy = $orderBy;
    }

    /**
     * Set order and filter criteria
     *
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $fieldSelector = $this->column . "->>'" . $this->jsonField . "'";

        $query = $builder->orderByRaw(
            $fieldSelector
            . " "
            . $this->orderBy
        );
        return $query;
    }
}
