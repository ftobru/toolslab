<?php namespace Core\Repositories\Eloquent\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class JsonbFilterCriteria
 * @package Core\Repositories\Eloquent\Criteria
 */
class JsonbFilterCriteria extends Criteria
{

    protected $column;
    protected $jsonField;
    protected $operator;
    protected $value;

    /**
     * @param string $column
     * @param string $jsonField
     * @param string $operator
     * @param string $value
     */
    public function __construct($column, $jsonField, $operator, $value)
    {
        $this->column = $column;
        $this->jsonField = $jsonField;
        $this->operator = $operator;
        $this->value = $value;
    }

    /**
     * Set filter criteria
     *
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $fieldSelector = $this->column . "->>'" . $this->jsonField . "'";
        $query = $builder->whereRaw(
            $fieldSelector
            . " "
            . $this->operator
            . " '"
            . $this->value
            . "'"
        );

        return $query;
    }
}
