<?php namespace Core\Exceptions;

use App;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Response;

/**
 * Class Handler
 * @package Core\Exceptions
 */
class Handler extends ExceptionHandler
{

        /**
         * A list of the exception types that should not be reported.
         *
         * @var array
         */
        protected $dontReport = [
            'Symfony\Component\HttpKernel\Exception\HttpException'
        ];

        /**
         * Report or log an exception.
         *
         * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
         *
         * @param  \Exception  $e
         * @return void
         */
        public function report(Exception $e)
        {
            if (App::environment() === 'prod') {
                // Twiiter post
            }
            return parent::report($e);
        }

        /**
         * Render an exception into an HTTP response.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Exception  $e
         * @return \Illuminate\Http\Response
         */
        public function render($request, Exception $e)
        {
            if (!env('APP_DEBUG', false)) {
                $statusCode = $e->getCode();
                $statusArr = [401, 404, 503];

                if (in_array($statusCode, $statusArr)) {
                    return Response::view("errors." . $statusCode);
                } else {
                    return Response::view("errors.500");
                }
            }
            // If the request wants JSON (AJAX doesn't always want JSON)
            if ($request->wantsJson() || $request->ajax()) {
                // Define the response
                $response = [
                    'errors' => 'Sorry, something went wrong.'
                ];
                // If the app is in debug mode
                if (env('APP_DEBUG', false)) {
                    // Add the exception class name, message and stack trace to response
                    $response['exception'] = get_class($e); // Reflection might be better here
                    $response['message'] = $e->getMessage();
                    $response['trace'] = $e->getTrace();
                }

                // Default response of 500
                $status = 500;

                // If this exception is an instance of HttpException
                if ($this->isHttpException($e)) {
                    // Grab the HTTP status code from the Exception
                    $status = $e->getStatusCode();
                }
                // Return a JSON response with the response array and status code
                return Response::api($response, $status);
            }
            // Default to the parent class' implementation of handler
            return parent::render($request, $e);
        }

}
