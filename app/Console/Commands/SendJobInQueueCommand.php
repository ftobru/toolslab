<?php namespace Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Queue;

class SendJobInQueueCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'queue:send:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test queue';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment(PHP_EOL.'Send job in queue'.PHP_EOL);
        Queue::push('Core\Jobs\TestQueueJob', ['test']);
    }

}
