<?php namespace Core\Console\Commands;

use DB;
use Illuminate\Console\Command;
use IPGeoBase\IPGeoBase;

/**
 * Class UpdateGeoIp
 * @package Core\Console\Commands
 */
class UpdateGeoIp extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'geoip:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update geoip';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $downloadCmd = 'wget -P ' . storage_path() . '/app/ http://ipgeobase.ru/files/db/Main/geo_files.tar.gz';
        exec($downloadCmd);
        exec('tar xvzf ' . storage_path() . '/app/geo_files.tar.gz -C ' . storage_path() . '/app/');
        exec('rm ' . storage_path() . '/app/geo_files.tar.gz');
        DB::statement('delete from cities');
        /** @var IPGeoBase $geo */
        $geo = new IPGeoBase();
        $geo->fillCityTable();
    }

}
