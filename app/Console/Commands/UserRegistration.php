<?php

namespace Core\Console\Commands;

use Illuminate\Console\Command;
use Saas\Services\UserService;

/**
 * Class UserRegistration
 * @package Core\Console\Commands
 */
class UserRegistration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:registration {name} {phone} {email} {password=qwe<lp#!}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Registration user';
    /** @var  UserService */
    protected $userService;

    /**
     * Create a new command instance.
     *
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        parent::__construct();

        $this->userService = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //@todo Регистрация
    }
}
