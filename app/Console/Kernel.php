<?php namespace Core\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 * @package Core\Console
 */
class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'Core\Console\Commands\Inspire',
        'LandingControl\Console\Commands\CheckDNSCommand',
        'Core\Console\Commands\SendJobInQueueCommand',
        'Core\Console\Commands\UpdateGeoIp'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('dns:check')->everyMinute();
        $schedule->command('geoip:update')->daily();
    }

}
