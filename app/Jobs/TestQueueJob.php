<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 20.07.15
 * Time: 11:42
 */

namespace Core\Jobs;


use Illuminate\Contracts\Queue\Job;

class TestQueueJob
{
    /**
     * @param Job $job
     * @param $data
     */
    public function fire(Job $job, $data)
    {
        $job->delete();
    }
}
