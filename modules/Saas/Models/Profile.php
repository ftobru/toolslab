<?php namespace Saas\Models;

use Core\Models\Abstracts\CoreModel;

/**
 * Class Profile
 * @property string divisions
 * @property string company_id
 * @property string user_id
 * @package App
 */
class Profile extends CoreModel
{
    protected $table = 'profiles';

    protected $company = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'user_id',
        'divisions',
        'phones'
    ];
}
