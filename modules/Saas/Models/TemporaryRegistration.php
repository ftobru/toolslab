<?php namespace Saas\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Saas\Traits\CompaniesName;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TemporaryRegistration
 *
 * @property string email
 * @property string phone
 * @property string name
 * @package Saas
 */
class TemporaryRegistration extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'temporary_registrations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
    ];
}
