<?php namespace Saas\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ScopeInterface;
use Input;

/**
 * Scope по компаниям
 * Class CompanyScope
 * @package Core\Models\Scopes
 */
class CompaniesScope implements ScopeInterface
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if ($companyAlias = Input::get('company_alias', false)) {
            $builder->whereHas('companies', function ($q) use ($companyAlias) {
                $q->where('name', '=', $companyAlias);
            });
        }

    }

    /**
     * Remove the scope from the given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {

    }

}
