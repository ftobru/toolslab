<?php namespace Saas\Models;

use Zizaco\Entrust\EntrustPermission;

/**
 * Class Permission
 * @package App
 * @property $name;
 * @property $display_name;
 * @property $description
 */
class Permission extends EntrustPermission
{
    //
}
