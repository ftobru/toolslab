<?php namespace Saas\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Message
 * @package Saas\Models
 * @property $user_id
 * @property $text
 * @property $to
 * @property $from
 */
class Message extends Model
{
    protected $table = 'messages';

    protected $filable = ['user_id', 'text', 'to', 'from'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Saas\Models\User');
    }
}


