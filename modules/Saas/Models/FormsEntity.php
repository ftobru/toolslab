<?php namespace Saas\Models;

use Core\Models\Abstracts\CoreModel;

/**
 * Class FormsEntity
 * @property mixed company_id
 * @property mixed id
 * @package App
 */
class FormsEntity extends CoreModel
{
    protected $table = 'forms_entity';

    protected $company = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meaning',
        'form_id',
        'company_id',
        'en_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function forms()
    {
        return $this->belongsTo('Saas\Models\Form');
    }
}
