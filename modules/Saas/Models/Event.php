<?php namespace Saas\Models;

use Core\Models\Abstracts\CoreModel;

class Event extends CoreModel
{
    protected $table = 'events';

    protected $fillable = ['type', 'information', 'company_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}
