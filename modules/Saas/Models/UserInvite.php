<?php namespace Saas\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserInvite
 *
 * @property int user_id
 * @property int company_id
 * @property string invite_code
 * @package Saas
 */
class UserInvite extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_invites';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'company_id',
        'invite_code',
    ];
}
