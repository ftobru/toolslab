<?php namespace Saas\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Company
 * @package App
 * @property string name
 * @property mixed id
 * @property string display_name
 * @property mixed created_at
 */
class Company extends Model
{

    protected $table = 'companies';

    protected $fillable = ['name', 'display_name', 'user_id', 'divisions'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function owner()
    {
        return $this->belongsTo('Saas\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->hasMany('Crm\Models\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->hasMany('Crm\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->hasMany('Saas\Models\Clients');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Saas\Models\User')->withPivot('company_id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events()
    {
        return $this->hasMany('Saas\Models\Event');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany('Crm\Models\Task');
    }

}
