<?php namespace Saas\Models;

use Zizaco\Entrust\EntrustRole;

/**
 * Class Role
 * @property integer id
 * @property mixed company_id
 * @property mixed name
 * @package App
 */
class Role extends EntrustRole
{
    protected $fillable = [
        'name',
        'display_name',
        'description',
        'company_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}