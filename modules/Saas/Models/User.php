<?php namespace Saas\Models;

use Core\Models\Abstracts\CoreModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Saas\Traits\CompaniesName;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User
 * @property mixed activation_code
 * @property mixed registration_token
 * @property string email
 * @property mixed phone
 * @property mixed status
 * @property mixed id
 * @property string last_activity
 * @property string name
 * @property bool reg_complete
 * @property bool invited
 * @property string password
 * @package Saas
 */
class User extends CoreModel implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword, EntrustUserTrait, CompaniesName;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'patronymic',
        'last_name',
        'email',
        'password',
        'phone',
        'status',
        'registration_token',
        'activation_code',
        'last_activity',
        'invited',
        'reg_complete'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'activation_code'];

    /**
     * @return BelongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany('Saas\Models\Company')->withPivot('user_id', 'company_id');
    }

    /**
     * @return BelongsToMany
     */
    public function billingAccount()
    {
        return $this->belongsToMany('Billing\Models\BillingAccount')->withPivot('billing_account_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function billingInvoices()
    {
        return $this->belongsToMany('Billing\Models\BillingInvoice')->withPivot('billing_invoice_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function billingPayments()
    {
        return $this->belongsToMany('Billing\Models\BillingPayment')->withPivot('billing_payment_id', 'user_id');
    }
}
