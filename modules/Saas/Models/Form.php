<?php namespace Saas\Models;

use Core\Models\Abstracts\CoreModel;

/**
 * Class Form
 * @property mixed id
 * @package App
 */
class Form extends CoreModel
{

    protected $table = 'forms';

    protected $company = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'en_type',
        'form_type',
        'company_id',
        'default_value',
        'placeholder',
        'validator',
        'label',
        'name'
    ];


}
