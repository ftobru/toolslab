<?php namespace Saas\Handlers\Events;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;use Saas\Models\User;

/**
 * Class LastActivityEventHandler
 * @package Saas\Handlers\Events
 */
class LastActivityEventHandler
{

    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  User  $event
     * @return void
     */
    public function handle(User $event)
    {
        $event->last_activity = new \DateTime();
        $event->save();
    }

}
