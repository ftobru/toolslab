<?php namespace Saas\Traits;

use Saas\Models\Scopes\CompanyScope;

trait CompanyName
{

    public static function bootCompanyName()
    {
        static::addGlobalScope(new CompanyScope());
    }

}
