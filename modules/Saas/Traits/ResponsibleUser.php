<?php namespace Saas\Traits;

use Saas\Models\Scopes\ResponsibleUserScope;

/**
 * Class ResponsibleUser
 * @package Saas\Traits
 */
trait ResponsibleUser
{

    public static function bootResponsibleUser()
    {
        static::addGlobalScope(new ResponsibleUserScope());
    }

}
