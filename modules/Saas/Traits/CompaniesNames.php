<?php namespace Saas\Traits;

use Saas\Models\Scopes\CompaniesScope;

/**
 * Class CompanyNames
 * @package Saas\Traits
 */
trait CompaniesName
{

    public static function bootCompaniesName()
    {
        static::addGlobalScope(new CompaniesScope());
    }
}
