<?php namespace Saas\Repositories\Eloquent;

use Saas\Repositories\Interfaces\ProfileRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class ProductEloquentRepository
 * @package App\Repository\Eloquent
 */
class ProfileEloquentRepository extends Repository implements ProfileRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\Profile';
    }
}
