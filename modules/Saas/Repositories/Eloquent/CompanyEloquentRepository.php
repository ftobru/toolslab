<?php namespace Saas\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;

class CompanyEloquentRepository extends Repository implements CompanyRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\Company';
    }

}
