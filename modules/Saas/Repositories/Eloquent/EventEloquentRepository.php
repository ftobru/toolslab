<?php namespace Saas\Repositories\Eloquent;




use Ftob\Repositories\Eloquent\Repository;


/**
 * Class EventEloquentRepository
 * @package Core\Repository\Eloquent
 */
class EventEloquentRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\Event';
    }

}
