<?php namespace Saas\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;
use Saas\Repositories\Interfaces\TemporaryRegistrationRepositoryInterface;

/**
 * Class TemporaryRegistrationEloquentRepository
 * @package Saas\Repositories\Eloquent
 */
class TemporaryRegistrationEloquentRepository extends Repository implements TemporaryRegistrationRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\TemporaryRegistration';
    }

}
