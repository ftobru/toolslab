<?php namespace Saas\Repositories\Eloquent;


use Saas\Repositories\Interfaces\FormRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class FormEloquentRepository
 * @package Core\Repositories\Eloquent
 */
class FormEloquentRepository extends Repository implements FormRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\Form';
    }
}
