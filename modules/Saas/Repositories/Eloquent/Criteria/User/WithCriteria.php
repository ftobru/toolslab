<?php namespace Saas\Repositories\Eloquent\Criteria\User;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithCriteria
 * @package Saas\Repositories\Eloquent\Criteria\User;
 */
class WithCriteria extends Criteria
{
    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getModel()->with('companies');
    }
}
