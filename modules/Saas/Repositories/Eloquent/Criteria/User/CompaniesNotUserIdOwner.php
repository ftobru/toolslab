<?php namespace Saas\Repositories\Eloquent\Criteria\User;

use Doctrine\DBAL\Query\QueryBuilder;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Получить все компании в которых пользователь не является owner
 * Class CompanyNotUserIdOwner
 * @package Core\Repositories\Eloquent\Criteria\User
 */
class CompaniesNotUserIdOwner extends Criteria
{
    /** @var  int */
    protected $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }


    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->whereHas('companies', function ($builder) {
            $builder->where('companies.user_id', '<>', $this->userId);
        });
    }
}
