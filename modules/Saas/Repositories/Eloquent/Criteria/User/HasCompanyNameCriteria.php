<?php namespace Saas\Repositories\Eloquent\Criteria\User;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class HasCompanyNameCriteria
 * @package Saas\Repositories\Eloquent\Criteria\User
 */
class HasCompanyNameCriteria extends Criteria
{
    protected $companyName;

    /**
     * @param $companyName
     */
    public function __construct($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->whereHas('companies', function ($q) {
            $q->where('companies.name', $this->companyName);
        });
    }
}


