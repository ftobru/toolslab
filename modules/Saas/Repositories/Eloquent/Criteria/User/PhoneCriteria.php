<?php namespace Saas\Repositories\Eloquent\Criteria\User;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PhoneCriteria
 * @package Core\Repositories\Eloquent\Criteria\Company
 */
class PhoneCriteria extends Criteria
{
    protected $phone;


    public function __construct($phone)
    {
        $this->phone;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('phone', 'LIKE', $this->phone);
        return $query;
    }


}
