<?php namespace Saas\Repositories\Eloquent\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;


/**
 * Существует для поиска ползователя с определеными критериями
 * Class UserIdCriteria
 * @package Core\Repositories\Eloquent\Criteria
 */
class UserIdCriteria extends Criteria
{
    protected $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('id', '=', $this->userId);
    }


}
