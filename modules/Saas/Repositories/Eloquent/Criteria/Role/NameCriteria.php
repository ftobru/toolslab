<?php namespace Saas\Repositories\Eloquent\Criteria\Role;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class NameCriteria
 * @package Core\Repositories\Eloquent\Criteria\Role
 */
class NameCriteria extends Criteria
{
    protected $name;

    /**
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('name', 'LIKE', $this->name);
        return $query;
    }
}
