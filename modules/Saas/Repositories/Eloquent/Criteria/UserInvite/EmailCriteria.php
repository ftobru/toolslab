<?php namespace Saas\Repositories\Eloquent\Criteria\UserInvite;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EmailCriteria
 * @package Saas\Repositories\Eloquent\Criteria\UserInvite
 */
class EmailCriteria extends Criteria
{
    protected $email;

    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('email', 'LIKE', $this->email);
    }
}
