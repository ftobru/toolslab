<?php namespace Saas\Repositories\Eloquent\Criteria\UserInvite;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class InviteCodeCriteria
 * @package Saas\Repositories\Eloquent\Criteria\UserInvite
 */
class InviteCodeCriteria extends Criteria
{
    protected $inviteCode;

    public function __construct($invite_code)
    {
        $this->inviteCode = $invite_code;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('invite_code', 'LIKE', $this->inviteCode);
    }
}
