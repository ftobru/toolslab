<?php namespace Saas\Repositories\Eloquent\Criteria\UserInvite;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserIdCriteria
 * @package Saas\Repositories\Eloquent\Criteria
 */
class UserIdCriteria extends Criteria
{
    protected $userId;

    /**
     * @param int $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('user_id', '=', $this->userId);
    }
}
