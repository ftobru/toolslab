<?php namespace Saas\Repositories\Eloquent\Criteria\Forms;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithCriteria
 * @package  Saas\Repositories\Eloquent\Criteria\Forms
 */
class WithCriteria extends Criteria
{
    protected $with;

    /**
     * @param array $with
     */
    public function __construct(array $with)
    {
        $this->with = $with;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getModel()->with($this->with);
    }
}
