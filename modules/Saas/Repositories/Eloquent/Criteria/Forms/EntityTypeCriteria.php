<?php namespace Saas\Repositories\Eloquent\Criteria\Forms;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EntityTypeCriteria
 * @package Core\Repositories\Eloquent\Criteria\Forms
 */
class EntityTypeCriteria extends Criteria
{
    protected $entityType;

    /**
     * @param $entityType int Entity type
     */
    public function __construct($entityType)
    {
        $this->entityType = $entityType;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('en_type', '=', $this->entityType);
    }
}
