<?php namespace Saas\Repositories\Eloquent\Criteria\Forms;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FormLabelCriteria
 * @package App\Repositories\Eloquent\Criteria\Forms
 */
class FormLabelCriteria extends Criteria
{
    protected $formLabel;

    /**
     * @param $label int Entity type
     */
    public function __construct($label)
    {
        $this->formLabel = $label;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('label', 'LIKE', '%' . $this->formLabel . '%');
    }
}
