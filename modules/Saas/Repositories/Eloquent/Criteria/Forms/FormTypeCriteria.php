<?php namespace Saas\Repositories\Eloquent\Criteria\Forms;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FormTypeCriteria
 * @package App\Repositories\Eloquent\Criteria\Forms
 */
class FormTypeCriteria extends Criteria
{
    protected $formType;

    /**
     * @param $entityType int Entity type
     */
    public function __construct($entityType)
    {
        $this->formType = $entityType;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('form_type', '=', $this->formType);
    }
}
