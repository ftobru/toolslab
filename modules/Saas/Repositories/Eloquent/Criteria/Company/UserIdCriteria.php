<?php namespace Saas\Repositories\Eloquent\Criteria\Company;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserIdCriteria
 * @package Core\Repositories\Eloquent\Criteria\Company
 */
class UserIdCriteria extends  Criteria
{
    /** @var  int */
    protected $userId;

    /**
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId =$userId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('user_id', '=', $this->userId);
    }
}
