<?php namespace Saas\Repositories\Eloquent\Criteria\Company;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class DisplayNameCriteria extends Criteria
{
    protected $displayName;

    /**
     * @param $displayName
     */
    public function __construct($displayName)
    {
        $this->displayName = $displayName;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('display_name', 'LIKE', $this->displayName);
        return $query;
    }

}
