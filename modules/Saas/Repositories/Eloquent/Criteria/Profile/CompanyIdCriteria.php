<?php namespace Saas\Repositories\Eloquent\Criteria\Profile;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CompanyIdCriteria
 * @package App\Repositories\Eloquent\Criteria\Profile
 */
class CompanyIdCriteria extends Criteria
{
    protected $companyId;

    public function __construct($userId)
    {
        $this->companyId = $userId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('company_id', '=', $this->companyId);
    }
}
