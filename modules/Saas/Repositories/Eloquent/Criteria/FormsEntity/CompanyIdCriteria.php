<?php namespace Saas\Repositories\Eloquent\Criteria\FormsEntity;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CompanyIdCriteria
 * @package Core\Repositories\Eloquent\Criteria\FormsEntity
 */
class CompanyIdCriteria extends Criteria
{
    protected $companyId;

    /**
     * @param $companyId
     */
    public function __construct($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('company_id', '=', $this->companyId);
    }

}