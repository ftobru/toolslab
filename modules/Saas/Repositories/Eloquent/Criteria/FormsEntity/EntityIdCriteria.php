<?php namespace Saas\Repositories\Eloquent\Criteria\FormsEntity;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class EntityIdCriteria
 * @package App\Repositories\Eloquent\Criteria\FormsEntity
 */
class EntityIdCriteria extends Criteria
{
    protected $enId;

    /**
     * @param $companyId
     */
    public function __construct($companyId)
    {
        $this->enId = $companyId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('en_id', '=', $this->enId);
    }
}
