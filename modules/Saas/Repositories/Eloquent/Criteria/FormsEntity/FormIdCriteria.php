<?php namespace Saas\Repositories\Eloquent\Criteria\FormsEntity;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FormIdCriteria
 * @package App\Repositories\Eloquent\Criteria\FormsEntity
 */
class FormIdCriteria extends Criteria
{
    protected $formId;

    /**
     * @param $companyId
     */
    public function __construct($companyId)
    {
        $this->formId = $companyId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('form_id', '=', $this->formId);
    }

}