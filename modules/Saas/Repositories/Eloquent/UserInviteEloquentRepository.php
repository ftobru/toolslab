<?php namespace Saas\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use Saas\Repositories\Interfaces\UserInviteRepositoryInterface;

/**
 * Class UserInviteEloquentRepository
 * @package Core\Repository\Eloquent
 */
class UserInviteEloquentRepository extends Repository implements UserInviteRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\UserInvite';
    }
}
