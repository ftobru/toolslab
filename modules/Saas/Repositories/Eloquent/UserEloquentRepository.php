<?php namespace Saas\Repositories\Eloquent;

use Billing\Repositories\Eloquent\Criteria\StatusCriteria;
use Ftob\Repositories\Eloquent\Repository;
use Illuminate\Support\Str;
use Saas\Repositories\Eloquent\Criteria\User\CompanyIdCriteria;
use Saas\Repositories\Interfaces\UserRepositoryInterface;

/**
 * Class UserEloquentRepository
 * @package Core\Repository\Eloquent
 */
class UserEloquentRepository extends Repository implements UserRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\User';
    }

    /**
     * @param array $data
     * @return static
     */
    public function create(array $data)
    {
        return $this->builder->getModel()->create([
            'name' => array_get($data, 'name', null),
            'email' => array_get($data, 'email', null),
            'password' => bcrypt(array_get($data, 'password', null)),
            'phone' => array_get($data, 'phone', null),
            'activation_code' => rand(1, 9999),
            'registration_token' => Str::random()
        ]);
    }

    /**
     * @param $status
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByStatus($status)
    {
        return $this->getByCriteria(new StatusCriteria($status))->all();
    }

}
