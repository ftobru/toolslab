<?php namespace Saas\Repositories\Eloquent;

use Saas\Repositories\Interfaces\FormsEntityRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class FormsEntityEloquentRepository
 * @package Core\Repositories\Eloquent
 */
class FormsEntityEloquentRepository extends Repository implements FormsEntityRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\FormsEntity';
    }
}
