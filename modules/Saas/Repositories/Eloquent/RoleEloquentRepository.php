<?php namespace Saas\Repositories\Eloquent;

use Saas\Repositories\Interfaces\RoleRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;


/**
 * Class RoleEloquentRepository
 * @package Core\Repositories\Eloquent
 */
class RoleEloquentRepository extends Repository implements RoleRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Saas\Models\Role';
    }
}