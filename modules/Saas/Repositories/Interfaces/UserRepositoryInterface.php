<?php namespace Saas\Repositories\Interfaces;

/**
 * Interface UserRepository
 * @package Core\Repositories\Interfaces
 */
interface UserRepositoryInterface
{
    public function getByStatus($status);
}


