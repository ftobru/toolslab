<?php namespace Saas\Events\Observers\Interfaces;

interface ObserverInterface
{
    public function created();
}
