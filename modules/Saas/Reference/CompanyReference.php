<?php namespace Saas\References;

/**
 * Class CompanyReference
 * @package Core\Reference
 */
class CompanyReference
{
    const STATUS_BLOCK = -1;
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_NAME = 'Моя первая компания';

    const ERROR_NAME_UNIQUE = 303;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public static function errors()
    {
        return [
            self::ERROR_NAME_UNIQUE
        ];
    }

}
