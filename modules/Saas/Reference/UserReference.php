<?php namespace Saas\References;

/**
 * Class UserState
 * @package Core\Reference
 */
class UserReference
{
    /** Заблокированный пользователь */
    const STATUS_BLOCK = -1;
    /** Зарегистрированый но не потвежден */
    const STATUS_NO_ACTIVE = 0;
    /** Активный  */
    const STATUS_ACTIVE = 1;

    const ERROR_EMAIL_ALREADY_EXIST = 301;
    const ERROR_PHONE_ALREADY_EXIST = 302;
    const ERROR_INVALID_REGISTRATION_TOKEN = 303;

    /** Пол пользователя     */
    const GENDER_MALE = 0;
    const GENDER_FEMALE = 1;

    const OWNER_ROLE_NAME = 'owner';
    const OWNER_ROLE_DISPLAY_NAME = 'Владелец';

    const OWNER_PERMISSION_NAME = 'owner';

    /**
     * @return array
     */
    public static function status()
    {
        return [
            self::STATUS_BLOCK,
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public static function statusesInfo()
    {
        return [
            'Заблокирован' => self::STATUS_BLOCK,
            'Неактивный' => self::STATUS_NO_ACTIVE,
            'Активный' => self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public function errors()
    {
        return [
            self::ERROR_EMAIL_ALREADY_EXIST,
            self::ERROR_PHONE_ALREADY_EXIST
        ];
    }
}
