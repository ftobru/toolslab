<?php namespace Saas\References;

class SocketReference
{
    const CHANNEL_MESSAGE_PUSH = 'message.push';

    /**
     * @return array
     */
    public function channels()
    {
        return [
            self::CHANNEL_MESSAGE_PUSH
        ];
    }

}

