<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UserUpdateRequest
 * @package Core\Http\Requests\Rest
 */
class UpdateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en_type' => 'integer',
            'form_type' => 'integer',
            'default_value' => 'array',
            'label' => 'required|max:255',
            'placeholder' => 'max:255',
            'validator' => 'array',
            'id' => 'required|integer|exists:forms,id'
        ];
    }
}
