<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class AjaxUpdateCompanyRequest
 * @package Core\Http\Requests\Rest
 */
class AjaxUpdateCompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required|max:255|min:1',
            'name' => 'string|max:255|min:1|unique:companies,name|Regex:/^[0-9a-z]+$/'
        ];
    }
}