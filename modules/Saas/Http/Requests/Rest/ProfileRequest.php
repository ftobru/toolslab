<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

class ProfileRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'last_name' => 'max:255',
            'patronymic' => 'max:255',
            'password' => 'min:6|max:255',
            'phone' => 'required|max:255|min:6|unique:users',
            'email' => 'required|max:255|min:6|email|unique:users'
        ];
    }

}
