<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class GetCompanyRequest
 * @package App\Http\Requests
 */
class GetCompanyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'metaData' => 'array',
            'metaData.sort' => 'array',
            'metaData.filter' => 'array',
            'metaData.paginate' => 'array',
            'metaData.paginate.perPage' => 'integer',
            'page' => 'integer',
        ];
    }
}
