<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UserUpdateRequest
 * @package Core\Http\Requests\Rest
 */
class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:16|unique:users',
            'password' => 'min:6',
            'roles' => 'required|array',
            'companies' => 'required|integer'
        ];
    }

}


