<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class StoreUserRequest
 * @package Core\Http\Requests\Rest
 */
class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:32|unique:users',
            'password' => 'required|min:6',
            'roles' => 'required|array',
            'companies' => 'required|integer'
        ];
    }
}
