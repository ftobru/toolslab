<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UserUpdateRequest
 * @package Core\Http\Requests\Rest
 */
class UpdateFormEntityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form_id' => 'required|integer|exists:forms,id',
            'en_id' => 'required|integer',
            'meaning' => 'array',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }
}
