<?php namespace Saas\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateProductRequest
 * @package Core\Http\Requests\Rest
 */
class UpdateProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:128',
            'description' => 'max:2048',
            'article' => 'max:255',
            'new_price' => 'required|numeric',
            'old_price' => 'required|numeric',
            'profit' => 'required|numeric',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }
}
