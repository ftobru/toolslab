<?php namespace Saas\Http\Requests\App;

use Core\Http\Requests\Request;

class ChangeForgottenPasswordPequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password1' => 'min:6|max:255',
            'password2' => 'min:6|max:255'
        ];
    }

}