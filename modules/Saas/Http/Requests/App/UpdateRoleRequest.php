<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 15.07.15
 * Time: 13:50
 */

namespace Saas\Http\Requests;


use Core\Http\Requests\Request;

class UpdateRoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required',
            'attach' => 'array',
            'detach' => 'array'
        ];
    }


}