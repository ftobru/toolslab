<?php namespace Saas\Http\Requests\App;

use Core\Http\Requests\Request;

/**
 * Class DemoRegistrationRequest
 * @package Saas\Http\Requests\App
 */
class DemoRegistrationRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:5|max:255',
            'phone' => 'required|min:5|max:255',
            'name' => 'required|min:2|max:255'
        ];
    }
}