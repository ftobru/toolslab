<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 15.07.15
 * Time: 13:14
 */


namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

class RoleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255'
        ];
    }

}