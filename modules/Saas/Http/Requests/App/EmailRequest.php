<?php
namespace Saas\Http\Requests\App;

use Core\Http\Requests\Request;

/**
 * Class EmailRequest
 * @package Saas\Http\Requests\App
 */
class EmailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'company_id' => 'required|exists:companies,id'
        ];
    }
}