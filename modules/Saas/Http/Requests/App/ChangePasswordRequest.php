<?php namespace Saas\Http\Requests\App;
use Core\Http\Requests\Request;

/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 02.06.15
 * Time: 20:00
 */


class ChangePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'oldPassword' => 'required|min:5|max:255',
            'newPassword' => 'required|min:5|max:255'
        ];
    }

}