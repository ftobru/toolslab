<?php namespace Saas\Http\Requests\App;
use Core\Http\Requests\Request;

class ChangeEmailPhonerequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|min:5|max:255|unique:users',
            'phone' => 'min:5|max:30'
        ];
    }
}