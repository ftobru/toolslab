<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

/**
 * Class SignInRequest
 * @package Saas\Http\Requests
 */
class SignInRequest extends Request
{
    public function __construct()
    {
        parent::__construct();
        \Input::merge(['remember' => \Input::get('remember') === 'true'? true: false]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|max:255|required',
            'password' => 'required|min:3',
            'remember' => 'boolean'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
