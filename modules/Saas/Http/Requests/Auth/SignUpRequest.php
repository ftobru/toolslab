<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

class SignUpRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users.name' => 'required|max:255',
            'users.email' => 'required|email|max:255',
            'users.phone' => 'required|max:32',
            'users.password' => 'required|min:6',
        ];
    }
}
