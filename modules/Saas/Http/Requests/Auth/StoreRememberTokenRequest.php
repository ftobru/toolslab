<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

/**
 * Class StoreRememberTokenRequest
 * @package Saas\Http\Requests
 */
class StoreRememberTokenRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|max:255|required'
        ];
    }
}
