<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

/**
 * Class CheckCodeRequest
 * @package Saas\Http\Requests
 */
class AjaxCheckCodeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activation_code' => 'required|max:255',
            'user_id' => 'required|exists:users,id'
        ];
    }

}
