<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

/**
 * Class UpdateRegistrationDataRequest
 * @package Saas\Http\Requests
 */
class AjaxUpdateRegistrationDataRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_token' => 'required|max:32||exists:users',
            'email' => 'required|email|max:255',
            'phone' => 'required|Regex:/^\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}/',
            'user_id' => 'required|exists:users,id'
        ];
    }
}
