<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

class RememberTokenRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'remember_token' => 'required|max:255'
        ];
    }
}
