<?php namespace Saas\Http\Requests;

use Core\Http\Requests\Request;

/**
 * Class InviteCompleteRequest
 * @package Saas\Http\Requests
 */
class InviteCompleteRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'phone' => 'required|max:32',
            'password' => 'required|min:6',
            'invite_id' => 'required|exists:user_invites,id'
        ];
    }
}
