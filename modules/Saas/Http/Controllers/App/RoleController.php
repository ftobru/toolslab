<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 15.07.15
 * Time: 11:01
 */


namespace Saas\Http\Controllers\App;

//use Alexusmai\Ruslug\Slug;
use Alexusmai\Ruslug\Slug;
use Core\Http\Controllers\Controller;
use DB;
use Exception;
use Illuminate\Contracts\Queue\EntityNotFoundException;
use Illuminate\Database\Eloquent\Collection;
use Lang;
use Request;
use Response;
use Saas\Exceptions\CompanyNotFoundException;
use Saas\Http\Requests\RoleRequest;
use Saas\Http\Requests\RoleIdRequest;
use Saas\Http\Requests\UpdateRoleRequest;
use Saas\Models\Permission;
use Saas\Models\Role;
use Saas\Repositories\Eloquent\Criteria\User\CompanyIdCriteria;
use Saas\Repositories\Eloquent\RoleEloquentRepository;
use Saas\Repositories\Interfaces\RoleRepositoryInterface;
use Saas\Services\RoleService;
use Saas\Services\StateApplicationService;

/**
 * Class RoleController
 * @package Saas\Http\Controllers\App
 */
class RoleController extends Controller
{
    /** @var RoleRepositoryInterface|RoleEloquentRepository */
    protected $roleRepository;
    /** @var StateApplicationService */
    protected $appService;
    /** @var RoleService */
    protected $roleService;
    /** @var Slug */
    protected $slug;

    /**
     * @param RoleRepositoryInterface $roleRepositoryInterface
     * @param StateApplicationService $applicationService
     * @param RoleService $roleService
     * @param Slug $slug
     */
    public function __construct(
        RoleRepositoryInterface $roleRepositoryInterface,
        StateApplicationService $applicationService,
        RoleService $roleService,
        Slug $slug
    ) {
        $this->appService = $applicationService;
        $this->roleRepository = $roleRepositoryInterface;
        $this->roleService = $roleService;
        $this->slug = $slug;
    }

    /**
     * Вьюха настройки разрешений для ролей.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function edit(Request $request, $company_alias, $role_id)
    {
        /** @var Role $role */
        $role = $this->roleRepository->find(intval($role_id));
        if (!$role) {
            abort(404);
        }
        $company = $this->appService->getCurrentCompany();
        $viewData = $this->roleService->getDataForEditView($role, $company);
        return view('saas.roles.edit', $viewData);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws CompanyNotFoundException
     */
    public function ajaxGetRoles()
    {
        $company = $this->appService->getCurrentCompany();
        if (!$company) {
            throw new CompanyNotFoundException;
        }
        return Response::json([
            'roles' => $this->roleRepository->pushCriteria(new CompanyIdCriteria($company->id))->all()
        ]);
    }

    /**
     * @param RoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws CompanyNotFoundException
     */
    public function ajaxStoreRole(RoleRequest $request)
    {
        $company = $this->appService->getCurrentCompany();
        if (!$company) {
            throw new CompanyNotFoundException;
        }
        $role = $this->roleRepository->findBy('name', $request->get('name'), ['id']);
        if ($role) {
            return Response::json(['message' => 'Роль уже существует'], 422);
        }
        $role = $this->roleRepository->create([
            'name' => $this->slug->make($request->get('name')),
            'display_name' => $request->get('name'),
            'description' => $request->get('description'),
            'company_id' => $company->id
        ]);
        return Response::json(['role' => $role]);
    }

    /**
     * @param UpdateRoleRequest $request
     * @throws Exception
     */
    public function ajaxUpdateRole(UpdateRoleRequest $request)
    {
        /** @var Role $role */
        $role = $this->roleRepository->find($request->get('id'));
        if (!$role) {
            throw new EntityNotFoundException(Role::class, $request->get('id'));
        }
        $this->roleService->updateRolePermissions($request->all(), $role);
    }

    /**
     * @param RoleIdRequest $request
     * @return mixed
     */
    public function ajaxRemoveRole(RoleIdRequest $request)
    {
        $role = $this->roleRepository->find($request->get('id'));
        if (!$role) {
            throw new EntityNotFoundException(Role::class, $request->get('id'));
        }
        return $this->roleRepository->delete($request->get('id'));
    }
}