<?php namespace Saas\Http\Controllers\App;

use Auth;
use App;
use Illuminate\Support\Facades\Application as Application;
use Billing\Repositories\Interfaces\BillingAccountRepositoryInterface;
use Billing\Services\AccountService;
use Billing\Services\RateService;
use Core\Http\Controllers\Controller;
use Input;
use Redirect;
use Response;
use Saas\Http\Requests\AjaxResendCodeRequest;
use Saas\Http\Requests\InviteCompleteRequest;
use Saas\Http\Requests\InviteConfirmRequest;
use Saas\Models\UserInvite;
use Saas\References\CompanyReference;
use Saas\References\UserReference;
use Illuminate\Auth\Authenticatable;
use Saas\Http\Requests\AjaxCheckCodeRequest;
use Saas\Http\Requests\AjaxUpdateRegistrationDataRequest;
use Saas\Http\Requests\CheckCodeRequest;
use Saas\Http\Requests\SignInRequest;
use Saas\Http\Requests\SignUpRequest;
use Saas\Http\Requests\UpdateRegistrationDataRequest;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Repositories\Eloquent\Criteria\UserInvite\InviteCodeCriteria;
use Saas\Repositories\Eloquent\Criteria\UserInvite\UserIdCriteria;
use Saas\Repositories\Eloquent\UserInviteEloquentRepository;
use Saas\Repositories\Interfaces\UserInviteRepositoryInterface;
use Saas\Services\CompanyService;
use Saas\Services\InviteService;
use Saas\Services\UserService;
use SmscSender;

class AuthController extends Controller
{
    protected $userService;
    
    protected $companyService;

    protected $auth;
    /** @var  AccountService */
    protected $billingAccountService;
    /** @var  RateService */
    protected $rateServices;
    /** @var UserInviteEloquentRepository*/
    protected $userInviteRepository;
    /** @var InviteService */
    protected $inviteService;

    /**
     * @param Auth $auth
     * @param UserService $userService
     * @param CompanyService $companyService
     * @param AccountService $accountService
     * @param UserInviteRepositoryInterface $userInviteRepository
     * @param InviteService $inviteService
     * @param RateService $rateService
     */
    public function __construct(
        Auth $auth,
        UserService $userService,
        CompanyService $companyService,
        AccountService $accountService,
        UserInviteRepositoryInterface $userInviteRepository,
        InviteService $inviteService,
        RateService $rateService
    ) {
        $this->userService = $userService;
        $this->companyService = $companyService;
        $this->auth = $auth;
        $this->billingAccountService = $accountService;
        $this->rateServices = $rateService;
        $this->userInviteRepository = $userInviteRepository;
        $this->inviteService = $inviteService;
    }

    public function getLogin()
    {
        if (Auth::check()) {
            return redirect('/companies/my');
        }
        return view('auth.login');

    }

    public function getRegistration()
    {
        return view('auth.registration');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        if (Auth::user()) {
            $demo = Auth::getUser()->companies()->where('demo', '=', true)->first();
            if ($demo) {
                return redirect('/');
            }
            Auth::logout();
        }
        return redirect('/');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getInviteConfirmation()
    {
        $uid = intval(Input::get('uid', 0));
        $code = Input::get('code', null);

        /** @var User $user */
        $user = $this->userService->findById($uid);
        /** @var UserInvite $invite */
        $invite = $this->userInviteRepository
            ->pushCriteria(new UserIdCriteria($uid))
            ->pushCriteria(new InviteCodeCriteria($code))
            ->first();
        if ($invite) {
            $result = $this->inviteService->confirmInvite($invite, $user);
            if ($result['regComplete']) {
                return Redirect::to('/companies/my');
            } else {
                return view('auth.invite-confirmation', ['invite' => $invite]);
            }
        } else {
            abort(404);
        }
    }

    /**
     * Добивает данные о пользователе.
     *
     * @param InviteCompleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postInviteComplete(InviteCompleteRequest $request)
    {
        if ($userWithSamePhone = $this->userService->findUserByPhone($request->get('phone'))) {
                return Response::api(['error' => UserReference::ERROR_PHONE_ALREADY_EXIST], 422);
        };
        $result = $this->inviteService->completeInvite($request->all());
        return Response::api(['result' => $result]);
    }

    /**
     * @param SignInRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postLogin(SignInRequest $request)
    {
        $data = $request->all();
        $remember = array_get($data, 'remember', false);
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']], $remember)) {
            /** @var Response $response */
            return Response::api(['result' => true]);
        } else {
            return Response::api(['result' => false]);
        }
    }

    /**
     * @param SignUpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postRegistration(SignUpRequest $request)
    {
        $data = $request->get('users');
        $user = $this->userService->registration($data);
        if (isset($user['user'])) {
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                SmscSender::send([$user['user']->phone], $user['user']->activation_code);
                return Response::api(['result' => true, 'resp'=> $user]);
            } else {
                return Response::api(['result' => false , 'resp'=> $user]);
            }
        }
        return Response::api($user, 500);
    }

    /**
     * @param AjaxCheckCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postActivationCode(AjaxCheckCodeRequest $request)
    {
        $result = false;

        /** @var User $user */
        $user = $this->userService->findById($request->get('user_id'));

        $activationCode = App::isLocal() ? 'qwerty' : $user->activation_code;

        if (
        ($activationCode === $request->get('activation_code'))
            || ($activationCode === env('SMS_MASTER_KEY'))) {
            $user->update([
                'status' => UserReference::STATUS_ACTIVE,
                'registration_token' => '',
                'activation_code' => ''
            ]);
            // Создадим первую компанию пользователя.

            /** @var Company $company */
            $company = $this->companyService->create([
                'display_name' => CompanyReference::DEFAULT_NAME,
                'user_id' => $user->id,
            ]);

            $user->companies()->save($company);
            $account = $this->billingAccountService->createAccount();
            $user->billingAccount()->attach($account->id);

            $this->rateServices->connectDefaultRates($user->id, $company->id);
            //$rates = $this->rateServices->getRatesByCompanyId($company->id);
            $this->userService->addOwner($user->id, $company->id);
            $result = true;
        }

        return Response::api([
            'result' => $result,
        ]);
    }

    /**
     * Заглушка на повторную отправку кода активации на мобильный телефон.
     *
     * @param AjaxResendCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendActivationCode(AjaxResendCodeRequest $request)
    {
        $result = false;
        $activationCode = Auth::user()->activation_code;

        SmscSender::send(Auth::user()->phone, $activationCode);

        $result = true;
        return Response::api([
            'result' => $result,
        ]);
    }

    /**
     * @param AjaxUpdateRegistrationDataRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateRegistration(AjaxUpdateRegistrationDataRequest $request)
    {
        /** @var User $user */
        $user = $this->userService->findById($request->get('user_id'));

        if ($user->status === 0) {
            if ($userWithSamePhone = $this->userService->findUserByPhone($request->get('phone'))) {
                if ($userWithSamePhone->id !== $user->id) {
                    return Response::api(['error' => UserReference::ERROR_PHONE_ALREADY_EXIST], 422);
                }
            };
            if ($userWithSameEmail = $this->userService->findUserByEmail($request->get('email'))) {
                if ($userWithSameEmail->id !== $user->id) {
                    return Response::api(['error' => UserReference::ERROR_EMAIL_ALREADY_EXIST], 422);
                }
            }
            $result = $this->userService->updateRegistrationData($user->id, $request->all());

            return Response::api($result);
        }
    }
}
