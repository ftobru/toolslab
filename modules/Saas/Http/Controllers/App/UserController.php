<?php  namespace Saas\Http\Controllers\App;

use Auth;
use Hash;
use Mail;
use Response;
use Core\Http\Controllers\Controller;
//use Illuminate\Http\Response;
use Saas\Http\Requests\App\ChangeNameUserRequest;
use Saas\Http\Requests\App\ChangeEmailPhoneRequest;
use Saas\Http\Requests\App\ChangePasswordRequest;
use Saas\Http\Requests\App\CheckUserEmailRequest;
use Saas\Http\Requests\App\ChangeForgottenPasswordPequest;
use Saas\Http\Requests\App\DemoRegistrationRequest;
use Saas\Services\ProfileService;
use Saas\Services\UserService;

class UserController extends Controller
{
    /** @var UserService  */
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index()
    {
        return view('user.profile', ['user' => Auth::user()]);
    }

    /**
     * @param ChangeNameUserRequest $request
     * @return mixed
     */
    public function postAjaxChangeName(ChangeNameUserRequest $request)
    {
        return Response::json(['result' => $this->userService->changeName(
            Auth::user()->id,
            $request->get('name'),
            $request->get('last_name', null),
            $request->get('patronymic', null)
        ), 'url' => 'changeName']);
    }

    /**
     * @param ChangeEmailPhoneRequest $request
     * @return mixed
     */
    public function postAjaxChangeEmailPhone(ChangeEmailPhoneRequest $request)
    {

        return Response::json(['result'=>$this->userService->changeEmailPhone(
            Auth::user()->id,
            /*$request->get('email', null),
            $request->get('phone', null)*/
            \Input::all()
        ), 'url' => 'changeEmailPhone']);
    }

    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAjaxChangeDelivery($request)
    {
        return Response::json(['result' => $this->userService->changeDelivery(
            Auth::user()->id,
            $request->get('delivery')
        )
        ]);
    }

    /**
     * @param $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAjaxChangeGender($request)
    {
        return Response::json(['result' => $this->userService->changeGender(
            Auth::user()->id,
            $request->get('gender')
        )]);
    }

    /**
     * @param ChangePasswordRequest $request
     * @return bool
     */
    public function postAjaxChangePassword(ChangePasswordRequest $request)
    {
        if ($this->userService->changeUserPassword(
            Auth::user(),
            $request->get('newPassword'),
            $request->get('oldPassword')
        )) {
            return Response::json(['result' => true, 'url' => 'changePassword']);
        } else {
            return Response::json(['result' => false, 'url' => 'changePassword']);
        }
    }

    public function restorePassword()
    {
        return view('user.restorePassword');
    }

    /**
     * @param CheckUserEmailRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postAjaxCheckUserEmail(CheckUserEmailRequest $request)
    {
        $user = $this->userService->findUserByEmail($request->get('email'));
        if ($user == null) {
            return Response::json(["result" => false]);
        } else {
            Mail::send('emails.passwordRecovery', [
                'recoveryCode' => $user->remember_token,
                'hostName' => $request->getHttpHost()
            ], function ($message) use ($user) {
                $message->from('support@toolslab.com', 'toolslab');
                $message->to($user->email, '')->subject('Восстановление пароля');
            });
            return Response::json(["result" => true]);
        }
    }

    /**
     * Запоминает кто пришел
     *
     * @param DemoRegistrationRequest $request
     */
    public function postAjaxDemoRegistration(DemoRegistrationRequest $request)
    {
        $result = $this->userService->saveDemoRegistrationData($request->all());
        if ($result) {
            return Response::json($result);
        } else {
            abort(500);
        }
    }

    /**
     * @param ChangeForgottenPasswordPequest $request
     * @return \Illuminate\View\View
     */
    public function recoveryUserPassword(ChangeForgottenPasswordPequest $request)
    {
        $user = $this->userService->findUserByRememberToken($request->code);
        if ($user) {
            return view("user.setNewPassword", ['remember_token' => $user->remember_token]);
        } else {
            abort(500);
        }
    }

    /**
     * @param ChangeForgottenPasswordPequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAjaxSetNewPassword(ChangeForgottenPasswordPequest $request)
    {
        $user = $this->userService->findUserByRememberToken($request->remember_token);
        if ($user == null) {
            return Response::json(["result" => false]);
        } else {
            $this->userService->changePassword($user, $request->password1);
            //$this->userService->findUserByRememberToken($user);
            return Response::json(["result" => true]);
        }
    }
}

