<?php  namespace Saas\Http\Controllers\App;

use App;
use Billing\Reference\BillingRatesUserReference;
use Billing\Services\Rates\CrmAddUserRate;
use BillingControl;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Saas\Exceptions\CompanyNotFoundException;
use Saas\Http\Requests\App\CheckUserEmailRequest;
use Saas\Http\Requests\App\EmailRequest;
use Saas\Models\Company;
use Saas\References\UserReference;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;
use Saas\Services\CompanyService;
use Saas\Services\InviteService;
use Saas\Services\RoleService;
use Saas\Services\StateApplicationService;
use Saas\Services\UserService;
use Redirect;
use Response;

/**
 * Class UsersController
 * @package Saas\Http\Controllers\App
 */
class UsersController extends Controller
{
    /** @var UserService  */
    protected $userService;

    /** @var  CompanyService */
    protected $companyService;

    /** @var RoleService  */
    protected $roleService;

    /** @var  StateApplicationService */
    protected $applicationService;

    /** @var UserEloquentRepository */
    protected $userRepository;

    /**
     * @param UserService $userService
     * @param UserRepositoryInterface $userRepository
     * @param RoleService $roleService
     * @param CompanyService $companyService
     * @param StateApplicationService $applicationService
     */
    public function __construct(
        UserService $userService,
        UserRepositoryInterface $userRepository,
        RoleService $roleService,
        CompanyService $companyService,
        StateApplicationService $applicationService
    )
    {
        $this->userService = $userService;
        $this->companyService = $companyService;
        $this->roleService = $roleService;
        $this->applicationService = $applicationService;
        $this->userRepository = $userRepository;
    }

    /**
     * Users grid.
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function index($company_alias)
    {
        /** @var Company $company */
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $userAddRateInfo = App::make('BillingControl')->repository($this->userRepository)->check(CrmAddUserRate::IDENTIFIER, $company->id);
            return view('saas.users.index', [
                'company' => $company,
                'statuses' => UserReference::statusesInfo(),
                'roles' => $this->roleService->getByCompanyId($company->id)->toArray(),
                'UserAddRateInfo' => $userAddRateInfo
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * @param $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getUsers(AjaxCreateFirmRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->userService->getUsersForCompany($company->id, $request->all());
            return Response::json($result);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteUser(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['user_id'])) {

                $uid = intval($data['user_id']);
                $result = $this->companyService->deleteUserFromCompany($company->id, $uid);

                return Response::json($uid);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param EmailRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws CompanyNotFoundException
     * @throws \Exception
     */
    public function ajaxSendInvite(EmailRequest $request)
    {
        $company = $this->applicationService->getCurrentCompany();
        /** @var BillingControl $billingControl */
        $billingControl = App::make('BillingControl');
        $result = $billingControl->repository($this->userRepository)->check(CrmAddUserRate::IDENTIFIER, $company->id);
        $inviteResult = false;
        if($result['result']) {
            /** @var InviteService $inviteService */
            $inviteService = App::make('Saas\Services\InviteService');
            if (!$company) {
                throw new CompanyNotFoundException;
            }
            $inviteResult = $inviteService->inviteUser('', $request->get('email'), $company);
        } else {
            return Response::json([
                'allowToInvite' => false,
                'inviteSuccess' => false,
                'msg' => 'Ваш тарифный план не позволяет добавлять больше пользователей.'
            ]);
        }
        return Response::json([
            'allowToInvite' => $result['result'],
            'inviteSuccess' => $inviteResult
        ]);
    }

    /**
     * Проверяет есть ли у пользователя возможность пригласить нового пользователя.
     *
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxCheckAddUser($company_alias)
    {
        $company = $this->applicationService->getCurrentCompany();
        /** @var BillingControl $billingControl */
        $billingControl = App::make('BillingControl');
        $result = $billingControl->repository($this->userRepository)->check(CrmAddUserRate::IDENTIFIER, $company->id);
        return Response::json([
            'result' => $result['result'],
            'currentUsers' => $result['currentUsers'],
            'enabledUsers' => $result['enabledUsers'],
        ]);
    }


    /**
     * Проверяет существует ли пользователь с таким email в users.
     *
     * @param CheckUserEmailRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmailExists(CheckUserEmailRequest $request, $company_alias)
    {
        $company = $this->applicationService->getCurrentCompany();
        return Response::json([
            'result' => $this->userService->checkEmailExists($request->get('email'))
        ]);
    }
}

