<?php namespace Saas\Http\Controllers\App;

use Config;
use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Crm\Models\Client;
use Crm\Repositories\Eloquent\Criteria\Client\CompanyIdCriteria;
use Saas\Http\Requests\Rest\CreateFormRequest;
use Saas\Http\Requests\Rest\DeleteFormRequest;
use Saas\Http\Requests\Rest\GetFormRequest;
use Saas\Http\Requests\Rest\UpdateFormRequest;
use Response;
use Auth;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Repositories\Interfaces\FormRepositoryInterface;
use Saas\Services\CompanyService;
use Saas\Services\FormsService;
use Illuminate\Http\Request;

/**
 * Дополнительные поля.
 *
 * Class FormController
 * @package Saas\Http\Controllers\Api
 */
class FormController extends Controller
{
    /** @var CompanyService  */
    protected $companyService;

    /** @var FormsService  */
    protected $service;
    /**
     * @var FormEloquentRepository
     */
    protected $formRepository;

    /**
     * @param FormsService $formService
     * @param FormRepositoryInterface $formRepository
     * @param CompanyService $companyService
     */
    public function __construct(
        FormsService $formService,
        FormRepositoryInterface $formRepository,
        CompanyService $companyService
    ) {
        $this->service = $formService;
        $this->companyService = $companyService;
        $this->formRepository = $formRepository;
    }

    /**
     * Вьюха создания доп поля
     *
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function add($company_alias)
    {
        /** @var Company $company */
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();

            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    return view('saas.forms.create', [
                        'company' => $company
                    ]);
                }
            }
            abort(403);
        }
        abort(404);
    }

    /**
     * Вьюха редактирования доп поля
     *
     * @param $company_alias
     * @param $form_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($company_alias, $form_id)
    {
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    $form = $this->service->findById(intval($form_id));
                    if ($form) {
                        return view('saas.forms.edit', [
                            'company' => $company,
                            'form' => $form
                        ]);
                    }
                }
            }
            abort(403);
        }
        abort(404);
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetFormRequest $request
     * @return Response
     */
    public function getForms(GetFormRequest $request)
    {
        $forms = $this->service->findByEntityType($request->all());
        return Response::json(['forms' => $forms]);
    }

    /**
     * @param CreateFormRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param array $data
     */
    public function createForm(CreateFormRequest $request)
    {
        $data = $request->all();
        return Response::json(['form' => $this->service->store($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormRequest $request
     * @param $company_alias
     * @return Response
     * @internal param int $id
     */
    public function updateForm(UpdateFormRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                $id = $request->get('id');
                unset($data['id']);
                if ($c->id === $company->id) {
                    return Response::json(['form' => $this->service->update($id, $data)]);
                }
            }
        }
        return Response::json(['form' => $this->service->store($data)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteFormRequest $request
     * @return Response
     * @internal param int $id
     */
    public function deleteForm(DeleteFormRequest $request)
    {
        return Response::json(['form' => $this->service->delete($request->get('id'))]);
    }
}
