<?php namespace Saas\Http\Controllers\App;

use Core\Http\Controllers\Controller;

/**
 * Class DemoController
 * @package Saas\Http\Controllers\App
 */
class DemoController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('demo.cabinet-demo');
    }
}
