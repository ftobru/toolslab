<?php namespace Saas\Http\Controllers\App;

use Auth;
use Core\Http\Controllers\Controller;
use Redirect;
use Response;
use Saas\Http\Requests\Rest\AjaxCreateCompanyRequest;
use Saas\Http\Requests\Rest\AjaxUpdateCompanyRequest;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;
use Saas\Services\UserService;
use View;

/**
 * Class DemoController
 * @package Saas\Http\Controllers\App
 */
class CompanyController extends Controller
{
    /** @var  CompanyService */
    protected $companyService;

    /** @var  UserService */
    protected $userService;

    /**
     * @param CompanyService $companyService
     * @param UserService $userService
     */
    public function __construct(CompanyService $companyService, UserService $userService)
    {
        $this->companyService = $companyService;
        $this->userService = $userService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function my()
    {
        return view('company.my-companies');
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function edit($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    return view('saas.company.edit', [
                        'company' => $company,
                        'divisions' => json_decode($company->divisions)
                    ]);
                }
            }
            abort(403);
        }
        abort(404);
    }
    
    /**
     * @param AjaxCreateCompanyRequest $request
     * @return View
     */
    public function createCompany(AjaxCreateCompanyRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $company = $this->companyService->create([
            'display_name' => $request->get('display_name'),
            'user_id' => $user->id,
        ]);
        $user->companies()->save($company);
        $this->userService->addOwner($user->id, $company->id);
        return Response::json(['result' => true, 'company' => $company]);
    }



    /**
     * @param AjaxUpdateCompanyRequest $request
     * @return View
     */
    public function updateCompany(AjaxUpdateCompanyRequest $request, $company_alias)
    {
        /** @var Company $company */
        $company = $this->companyService->getByName($company_alias);
        $params = [
            'display_name' => $request->get('display_name'),
            'divisions' => json_encode($request->get('divisions'))
        ];
        $name = $request->get('name', null);
        if ($name !== null) {
            $params['name'] = $request->get('name');
        }

        $result = $this->companyService->updateCompanyWithCache($company, $params);

        if ($name === null) {
            $name = $company->name;
        }

        return Response::json(['result' => true, 'company' => $company, 'name' => $name]);
    }

}
