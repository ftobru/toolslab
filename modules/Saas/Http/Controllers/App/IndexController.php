<?php namespace Saas\Http\Controllers\App;

use Auth;
use Core\Http\Controllers\Controller;
use DB;
use Saas\Models\User;

/**
 * Class IndexController
 * @package Saas\Http\Controllers\App
 */
class IndexController extends Controller
{
    private function getParams()
    {
        $isDemoUser = false;

        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::getUser();
            $demo = DB::table('demo_data')->select()->where('user_id', '=', $user->id)->get();
            if (!empty($demo)) {
                $isDemoUser = true;
            }
        }

        return [
            'isLogin' => Auth::check(),
            'isDemoUser' => $isDemoUser
        ];
    }

    public function index()
    {
        return view('index.main', $this->getParams());
    }

    public function about()
    {
        return view('index.about_crm', $this->getParams());
    }

    public function analitycs()
    {
        return view('index.analitycs', $this->getParams());
    }

    public function multilanding()
    {
        return view('index.multilanding', $this->getParams());
    }

    public function cms()
    {
        return view('index.cms', $this->getParams());
    }

    public function traffic()
    {
        return view('index.traffic', $this->getParams());
    }

    public function linkRequest()
    {
        return view('index.linkrequest');
    }

}
