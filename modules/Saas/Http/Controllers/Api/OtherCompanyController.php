<?php namespace Saas\Http\Controllers\Api;

use Auth;
use Core\Http\Controllers\Controller;
use Core\Http\Requests;

use Saas\Services\CompanyService;
use Saas\Services\UserService;
use Response;

/**
 * Компании в которые тебя
 * Class OtherCompanyController
 * @package Core\Http\Controllers
 */
class OtherCompanyController extends Controller
{

    /** @var  CompanyService */
    protected $service;

    /**
     * @param UserService $companyService
     */
    public function __construct(UserService $companyService)
    {
        $this->service = $companyService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return ['company' => $this->service->getCompaniesNotOwner(Auth::user()->id)];
    }


}
