<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Saas\Services\CompanyService;
use Auth;
use Illuminate\Http\Request;
use Response;

/**
 * Свои компании
 * Class OwnCompanyController
 * @package Core\Http\Controllers
 */
class OwnCompanyController extends Controller
{

    /** @var  CompanyService */
    protected $service;

    /**
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->service = $companyService;
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return ['company' => $this->service->getByUserId(Auth::user()->id)];
    }
}
