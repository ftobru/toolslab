<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Saas\Http\Requests\Rest\StoreUserRequest;
use Saas\Http\Requests\Rest\UpdateUserRequest;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;
use Saas\Services\UserService;
use Illuminate\Http\Request;
use Response;

class UserController extends Controller
{
    /** @var UserService  */
    protected $service;

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $users = $this->service->activeRepository($request->all());

        return Response::api(['users' => $users]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return Response
     */
    public function store(StoreUserRequest $request)
    {
        return Response::api(['user' => $this->service->create($request->all())]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->service->findById($id);

        return Response::api(['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        return Response::api(['user' => $this->service->update($id, $request->all())]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['company' => $this->service->delete($id)]);
	}
}