<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;

use Saas\Http\Requests\CheckCodeRequest;
use Saas\Http\Requests\SignUpRequest;
use Saas\Services\CompanyService;
use Saas\Services\UserService;
use Log;
use Response;
use Core\Http\Controllers\Controller;

/**
 * Class CheckActivationCodeController
 * @package Core\Http\Controllers\Auth
 */
class CheckActivationCodeController extends Controller
{
    /** @var UserService  */
    protected $userService;
    /** @var CompanyService  */
    protected $companyServices;

    /**
     * @param UserService $userService
     * @param CompanyService $companyService
     */
    public function __construct(UserService $userService, CompanyService $companyService)
    {
        $this->userService = $userService;
    }

    /**
     * Check user activation code.
     *
     * @param CheckCodeRequest|SignUpRequest $request
     * @return Response
     */
    public function index(CheckCodeRequest $request)
    {
        $result = $this->userService->checkUserActivationCode($request->get('activation_code'));
        if ($result) {
            $this->userService->activateCurrentUser();
        }
        return Response::api([
            'result' => $result,
        ]);
    }
}
