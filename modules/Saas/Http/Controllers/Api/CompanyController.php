<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Saas\Http\Requests\Rest\GetCompanyRequest;
use Saas\Http\Requests\Rest\StoreCompanyRequest;
use Saas\Http\Requests\Rest\UpdateCompanyRequest;
use Saas\Services\CompanyService;
use Auth;
use Input;
use Request;
use Response;

/**
 * Class CompanyController
 * @package Core\Http\Api\Controllers
 */
class CompanyController extends Controller
{
    /** @var CompanyService  */
    protected $service;

    /**
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->service = $companyService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetCompanyRequest $request
     * @return Response
     */
    public function index(GetCompanyRequest $request)
    {
        
        return Response::api(
            [
                'companies' => $this->service->activeRepository($request->get('metaData', []))
            ]
        );
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCompanyRequest $request
     * @return Response
     */
    public function store(StoreCompanyRequest $request)
    {
        $company = $this->service->create(
            array_merge($request->all(),
                ['user_id' => Auth::user()->id]
            )
        );
        return Response::api(['company' => $company]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['company' => $this->service->show($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCompanyRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateCompanyRequest $request, $id)
    {
        return Response::api(['company' => $this->service->update($id, $request->all())]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['company' => $this->service->delete($id)]);
    }
}
