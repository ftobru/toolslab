<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Saas\Http\Requests\Rest\ProfileRequest;
use Saas\Services\UserService;
use Auth;
use Response;

class ProfileController extends Controller
{

    /** @var  UserService */
    protected $service;


    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::api(['user' => $this->service->show(Auth::user()->id)]);
    }

    /**
     * @param ProfileRequest $profileRequest
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(ProfileRequest $profileRequest)
    {
        return Response::api(['user' => $this->service->update(Auth::user()->id, $profileRequest->only([
            'name',
            'last_name',
            'patronymic',
            'password',
            'phone',
            'email']))]);
    }
}
