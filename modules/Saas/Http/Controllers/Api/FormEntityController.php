<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests\PaginationRequest;
use Saas\Http\Requests;
use Core\Http\Controllers\Controller;
use Saas\Http\Requests\Rest\GetFormEntityRequest;
use Saas\Http\Requests\Rest\UpdateFormEntityRequest;
use Response;
use Saas\Services\FormsEntityService;

/**
 * Class FormEntityController
 * @package Saas\Http\Controllers\Api
 */
class FormEntityController extends Controller
{
    /** @var FormsEntityService  */
    protected $service;

    /**
     * @param FormsEntityService $userService
     */
    public function __construct(FormsEntityService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param PaginationRequest $request
     * @return Response
     */
    public function index(PaginationRequest $request)
    {
        $formsEntities = $this->service->activeRepository($request->get('metaData', []));
        return Response::api(['formsEntities' => $formsEntities]);
    }

    /**
     * @param UpdateFormEntityRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param array $data
     */
    public function store(UpdateFormEntityRequest $request)
    {
        $data = $request->all();
        return Response::api(['formEntity' => $this->service->store($data)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $formEntity = $this->service->findById($id);
        return Response::api(['formEntity' => $formEntity]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormEntityRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateFormEntityRequest $request, $id)
    {
        return Response::api(['formEntity' => $this->service->update($id, $request->all())]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['formEntity' => $this->service->delete($id)]);
    }

}
