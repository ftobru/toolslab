<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Saas\Http\Requests\Rest\GetFormRequest;
use Saas\Http\Requests\Rest\UpdateFormRequest;
use Response;
use Saas\Services\FormsService;
use Illuminate\Http\Request;

/**
 * Class FormController
 * @package Saas\Http\Controllers\Api
 */
class FormController extends Controller
{

    /** @var FormsService  */
    protected $service;

    /**
     * @param FormsService $userService
     */
    public function __construct(FormsService $userService)
    {
        $this->service = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param GetFormRequest $request
     * @return Response
     */
    public function index(GetFormRequest $request)
    {
        $forms = $this->service->findByEntityType($request->all());
        return Response::api(['forms' => $forms]);
    }

    /**
     * @param UpdateFormRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param array $data
     */
    public function store(UpdateFormRequest $request)
    {
        $data = $request->all();
        return Response::api(['form' => $this->service->store($data)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $form = $this->service->findById($id);
        return Response::api(['form' => $form]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateFormRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateFormRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['form' => $this->service->update($id, $data)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['form' => $this->service->delete($id)]);
    }

}
