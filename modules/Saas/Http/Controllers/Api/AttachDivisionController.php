<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Saas\Http\Requests\Rest\StoreAttachDivisionRequest;
use Saas\Services\ProfileService;
use Response;

/**
 * Class AttachDivisionController
 * @package App\Http\Controllers\Api
 */
class AttachDivisionController extends Controller
{
    /** @var  ProfileService */
    protected $profileService;

    /**
     * @param ProfileService $companyService
     */
    public function __construct(ProfileService $companyService)
    {
        $this->profileService = $companyService;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAttachDivisionRequest $attachDivisionRequest
     * @return Response
     */
    public function store(StoreAttachDivisionRequest $attachDivisionRequest)
    {
        $request = $attachDivisionRequest->only(['company', 'user', 'divisions']);
        return Response::api(
            ['profile' => $this->profileService->attachUserToCompanyDivisions(
                $request['company']['id'],
                $request['user']['id'],
                $request['divisions']
            )]
        );
    }
}
