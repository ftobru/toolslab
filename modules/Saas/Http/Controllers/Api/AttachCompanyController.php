<?php namespace Saas\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Saas\Http\Requests\Rest\StoreAttachCompanyRequest;
use Saas\Services\CompanyService;
use Response;


/**
 * Class AttachCompanyController
 * @package Saas\Http\Controllers\Api
 */
class AttachCompanyController extends Controller
{
    /** @var  CompanyService */
    protected $companyService;

    /**
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAttachCompanyRequest $attachCompanyRequest
     * @return Response
     */
    public function store(StoreAttachCompanyRequest $attachCompanyRequest)
    {
        $request = $attachCompanyRequest->only(['company', 'user']);
        return Response::api(
            $this->companyService->attachUserToCompany($request['company']['id'], $request['user']['id'])
        );
    }
}
