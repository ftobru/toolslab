<?php namespace Saas\Http\Controllers\Service;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Authorizer;
use Illuminate\Http\Request;
use Response;


/**
 * Class OauthTokenController
 * @package Core\Http\Controllers\Service
 */
class OauthTokenController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        return Response::api(Authorizer::issueAccessToken());
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //@todo: надо научиться удалять токены
    }
}
