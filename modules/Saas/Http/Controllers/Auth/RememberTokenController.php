<?php namespace Saas\Http\Controllers\Auth;

use Core\Http\Controllers\Controller;
use Saas\Http\Requests\RememberTokenRequest;
use Saas\Http\Requests\StoreRememberTokenRequest;
use Saas\Services\UserService;
use Response;

/**
 * Class RememberTokenController
 * @package Saas\Http\Controllers\Auth
 */
class RememberTokenController extends Controller
{
    protected $service;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param RememberTokenRequest $request
     * @return array
     */
    public function index(RememberTokenRequest $request)
    {
        $user = $this->service->findUserByRememberToken($request->get('remember_token'));
        if (!$user) {
            return Response::api(['message' => 'User not found'], 405);
        }
        return Response::api(['user' => ['id' => $user->id]]);
    }

    /**
     * @param StoreRememberTokenRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(StoreRememberTokenRequest $request)
    {
        $user = $this->service->findUserByEmail($request->get('email'));
        if (!$user) {
            return Response::api(['message' => 'Email not registered'], 405);
        }
        $this->service->setRememberToken($user);
        return Response::api(['user' => ['id' => $user->id]]);
    }
}