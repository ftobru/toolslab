<?php namespace Saas\Http\Controllers\Auth;

use Core\Http\Controllers\Controller;
use Saas\Services\UserService;
use Auth;
use JWTAuth;
use Response;
use Saas\Http\Requests\SignInRequest;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * Class LoginController
 * @package Saas\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @var UserService
     */
    protected $service;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param SignInRequest $request
     * @return Response
     */
    public function store(SignInRequest $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return Response::api(['error' => 'Invalid credentials'], 401);
            }

        } catch (JWTException $e) {
            return Response::api(['error' => 'Could not create token'], 500);
        }

        return Response::api(['name' => Auth::user()->name, 'token' => $token, 'status' => Auth::user()->status]);
    }
}
