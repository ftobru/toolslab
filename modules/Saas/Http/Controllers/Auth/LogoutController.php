<?php namespace Saas\Http\Controllers\Auth;

use Core\Http\Controllers\Controller;
use Saas\Services\UserService;
use Auth;
use Response;

/**
 * Class LogoutController
 * @package Saas\Http\Controllers\Auth
 */
class LogoutController extends Controller
{
    /** @var UserService  */
    protected $service;

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
        $this->middleware('jwt.auth');
    }

    /**
     * @return array
     */
    public function index()
    {
//        if (Auth::user()->id) {
//            return Response::api(['logout' => $this->service->logout()]);
//        } else {
//            return Response::api(['logout' => false]);
//        }

    }
}
