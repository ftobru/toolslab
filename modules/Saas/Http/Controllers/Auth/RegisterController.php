<?php namespace Saas\Http\Controllers\Auth;

use Core\Http\Requests;

use Saas\Http\Requests\SignUpRequest;
use Saas\Http\Requests\UpdateRegistrationDataRequest;
use Saas\Models\User;
use Saas\Services\CompanyService;
use Saas\Services\UserService;
use JWTAuth;
use Response;
use Core\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /** @var UserService  */
    protected $userService;
    /** @var CompanyService  */
    protected $companyServices;

    public function __construct(UserService $userService, CompanyService $companyService)
    {
        $this->userService = $userService;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param SignUpRequest $request
     * @return Response
     * @throws \Exception
     */
    public function store(SignUpRequest $request)
    {
        $resultData = $this->userService->registration($request->get('users'));

        if (isset($resultData['user'])) {
            $token = JWTAuth::attempt(
                [
                    'email' => $resultData['user']['email'],
                    'password' => $request->get('users')['password']
                ]
            );

            return Response::api([
                'registration' => [
                    ['session' => ['token' => $token]],
                    $resultData
                ]
            ]);
        }
        return Response::api([$resultData]);
    }

    /**
     * @param UpdateRegistrationDataRequest $request
     * @param $id
     */
    public function update(UpdateRegistrationDataRequest $request, $id)
    {
        $result = $this->userService->updateRegistrationData($id, $request->all());
        return Response::api([$result]);
    }
}
