<?php namespace Saas\Http\Controllers\Auth;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Saas\Http\Requests\PasswordRequest;
use Saas\Services\UserService;
use Response;

/**
 * Class PasswordController
 * @package Saas\Http\Controllers\Auth
 */
class PasswordController extends Controller
{
    /** @var UserService  */
    protected $service;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param PasswordRequest $request
     * @return array
     */
    public function store(PasswordRequest $request)
    {
        $user = $this->service->findUserByRememberToken($request->get('remember_token'));
        if (!$user) {
            return Response::api(['message' => 'User not found'], 405);
        }
        $user = $this->service->changePassword($user, $request->get('password'));
        return Response::api(['user' => ['id' => $user->id]]);
    }
}
