<?php namespace Saas\Http\Middleware;

use Closure;
use Cookie;
use Illuminate\Http\Request;

/**
 * Class RefererSystemMiddleware
 * @package Saas\Http\Middleware
 */
class RefererSystemMiddleware
{
    /**
     * @param Request $request
     * @param callable $next
     */
    public function handle(Request $request, Closure $next)
    {
        if ($r = $request->get('r', false)) {
            Cookie::make('r', $r);
        }
        return $next($request);
    }
}
