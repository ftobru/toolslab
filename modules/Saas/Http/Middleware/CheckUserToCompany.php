<?php namespace Saas\Http\Middleware;

use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;
use Saas\Services\UserService;
use Auth;
use Closure;
use Response;

/**
 * Class CheckUserToCompany
 * @package Core\Http\Middleware
 */
class CheckUserToCompany
{
    /** @var UserService  */
    protected $service;

    /** @var  Auth */
    protected $auth;

    /**
     * @param Auth $auth
     * @param UserService $userService
     */
    public function __construct(AuthManager $auth, UserService $userService)
    {
        $this->service = $userService;
        $this->auth = $auth;
    }

    /**
     * @param Request $request
     * @param callable $next
     * @return \Illuminate\Contracts\Routing\ResponseFactory|
     * \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function handle(Request $request, Closure $next)
    {
        if ($companyName = $request->company_alias) {
            if (!$this->service->findUserToCompany($this->auth->id(), $companyName)) {
                if ($request->ajax()) {
                    return Response::api(['message' => 'You are not an employee of the company.'], 401);
                } else {
                    return redirect()->guest('auth/login');
                }
            }
        } else {
            return Response::api(['message' => 'Bad request. Please input param company'], 500);
        }
        return $next($request);
    }
}
