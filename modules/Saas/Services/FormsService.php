<?php namespace Saas\Services;

use App;
use Saas\Models\FormsEntity;
use Saas\Repositories\Eloquent\Criteria\Forms\CompanyIdCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\FormLabelCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\FormTypeCriteria;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Database\Eloquent\Collection;
use Saas\Repositories\Eloquent\Criteria\FormsEntity\FormIdCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Repositories\Eloquent\FormsEntityEloquentRepository;
use Saas\Repositories\Interfaces\FormRepositoryInterface;

/**
 * Class FormsService
 * @package Core\Services
 */
class FormsService
{
    use Sortable, Paginatable;

    // ID констант сущностей.
    const ENTITY_ID_USER    = 1;
    const ENTITY_ID_COMPANY = 2;

    /** @var FormEloquentRepository */
    protected $repository;

    /** @var FormsEntityEloquentRepository  */
    protected $formsEntityRepository;

    /**
     * @param FormRepositoryInterface $repository
     * @param FormsEntityEloquentRepository $formsEntityRepository
     */
    public function __construct(
        FormRepositoryInterface $repository,
        FormsEntityEloquentRepository $formsEntityRepository
    ) {
        $this->repository = $repository;
        $this->formsEntityRepository = $formsEntityRepository;
    }

    /**
     * @param $companyId
     * @return Collection
     */
    public function getAllFieldsByCompanyId($companyId)
    {
        return $this->repository->pushCriteria(new CompanyIdCriteria($companyId))->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $formEntities = $this->formsEntityRepository->pushCriteria(new FormIdCriteria($id))->all();
        /** @var FormsEntity $value */
        foreach ($formEntities as $value) {
            $value->delete();
        }
        return $this->repository->delete($id);
    }

    /**
     * @param array $data
     * @return static
     */
    public function store(array $data)
    {
        $this->prepareData($data);
        $form = $this->repository->create($data);
        $form->name = $this->generateCompanyName($form->id);
        $form->save();
        return $form;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $this->prepareData($data, true);
        return $this->repository->update($data, $id);
    }

    /**
     * @param array $data
     * @return static
     */
    public function create(array $data)
    {
        $this->prepareData($data);
        $field = $this->repository->create($data);
        return $field;
    }

    /**
     * @param $formID
     * @return mixed
     */
    public function findById($formID)
    {
        return $this->repository->find($formID);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllForms()
    {
        return $this->repository->all();
    }

    /**
     * @param $data array
     * @return \Illuminate\Support\Collection
     */
    public function findByEntityType(array $data)
    {
        $this->repository->applyCriteria(new EntityTypeCriteria($data['en_type']));
        return $this->activeRepository(array_get($data, 'metaData', []));
    }

    /**
     * @param array $data
     */
    private function prepareData(array &$data, $isUpdate = false)
    {
        if (!$isUpdate) {
            $validator = json_encode(array_get($data, 'validator', []));
            $data['validator'] = $validator;
            $default_value = json_encode(array_get($data, 'default_value', []));
            $data['default_value'] = $default_value;
        } else {
            if (isset($data['validator'])) {
                $validator = json_encode(array_get($data, 'validator', []));
                $data['validator'] = $validator;
            }
            if (isset($data['default_value'])) {
                $default_value = json_encode(array_get($data, 'default_value', []));
                $data['default_value'] = $default_value;
            }
        }
    }

    /**
     * Генерация имени формы
     * @param $formId
     * @return string
     */
    public function generateCompanyName($formId)
    {
        return 'f'. (int) $formId;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            if (isset($metaData['filter']['en_type'])) {
                $this->repository->pushCriteria(new EntityTypeCriteria($metaData['filter']['en_type']));
            }
            if (isset($metaData['filter']['form_type'])) {
                $this->repository->pushCriteria(new FormTypeCriteria($metaData['filter']['form_type']));
            }
            if (isset($metaData['filter']['company_id'])) {
                $this->repository->pushCriteria(new CompanyIdCriteria($metaData['filter']['company_id']));
            }
            if (isset($metaData['filter']['label'])) {
                if (isset($metaData['filter']['label']['term'])) {
                    $this->repository->pushCriteria(new FormLabelCriteria($metaData['filter']['label']['term']));
                }
            }
        }
        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginate($perPage);
        }
        return $this->repository->all();
    }
}
