<?php namespace Saas\Services;

use App;
use Saas\Repositories\Eloquent\Criteria\FormsEntity\CompanyIdCriteria;
use Saas\Repositories\Eloquent\Criteria\FormsEntity\EntityIdCriteria;
use Saas\Repositories\Eloquent\Criteria\FormsEntity\FormIdCriteria;

use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Database\Eloquent\Collection;
use Saas\Repositories\Eloquent\FormsEntityEloquentRepository;
use Saas\Repositories\Interfaces\FormsEntityRepositoryInterface;

/**
 * Class FormsEntityService
 * @package Core\Services
 */
class FormsEntityService
{

    use Sortable, Paginatable;

    /** @var FormsEntityEloquentRepository */
    protected $repository;

    /**
     * @param FormsEntityRepositoryInterface $repository
     */
    public function __construct(FormsEntityRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $companyId
     * @return Collection
     */
    public function getByCompanyId($companyId)
    {
        return $this->repository->findBy('company_id', $companyId);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $this->prepareData($data);
        return $this->repository->update($data, $id);
    }


    /**
     * @param array $data
     * @return static
     */
    public function create(array $data)
    {
        $this->prepareData($data);
        $field = $this->repository->create($data);
        return $field;
    }

    /**
     * @param $formEntityID
     * @return mixed
     */
    public function findById($formEntityID)
    {
        return $this->repository->find($formEntityID);
    }

    /**
     * @param $companyID
     * @return \Illuminate\Support\Collection
     */
    public function getFieldsByCompanyId($companyID)
    {
        return $this->repository->pushCriteria(new CompanyIdCriteria($companyID))->all();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllFormsEntities()
    {
        return $this->repository->all();
    }

    /**
     * @param $data
     * @return static
     */
    public function store($data)
    {
        $this->prepareData($data);
        return $this->repository->create($data);
    }

    /**
     * @param array $data
     */
    private function prepareData(array &$data)
    {
        $data['meaning'] = json_encode(array_get($data, 'meaning', []));
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            if (isset($metaData['filter']['en_id'])) {
                $this->repository->pushCriteria(new EntityIdCriteria($metaData['filter']['en_id']));
            }
            if (isset($metaData['filter']['form_id'])) {
                $this->repository->pushCriteria(new FormIdCriteria($metaData['filter']['form_id']));
            }
            if (isset($metaData['filter']['company_id'])) {
                $this->repository->pushCriteria(new CompanyIdCriteria($metaData['filter']['company_id']));
            }
        }
        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginate($perPage);
        }
        return $this->repository->all();
    }

}
