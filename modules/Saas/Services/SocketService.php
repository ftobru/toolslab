<?php namespace Saas\Services;

use Config;
use LRedis;
use Saas\Exceptions\UserNotFoundException;
use Saas\Models\User;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;

/**
 * Сервис по работе с сокетом
 * Class SocketService
 * @package Saas\Services
 */
class SocketService
{
    /** @var LRedis */
    protected $redis;
    /** @var  UserEloquentRepository|UserRepositoryInterface */
    protected $userRepository;

    /**
     * @param LRedis $redis
     * @param UserRepositoryInterface $userRepositoryInterface
     */
    public function __construct(LRedis $redis, UserRepositoryInterface $userRepositoryInterface)
    {
        $this->redis = $redis::connection();
        $this->userRepository = $userRepositoryInterface;
    }

    /**
     * Отправит сообщение на фронт
     * @param $channel
     * @param $userId
     * @param $text
     * @return int
     * @throws UserNotFoundException
     */
    public function push($channel, $userId, $text)
    {
        if (!($user = $this->userRepository->find($userId))) {
            throw new UserNotFoundException;
        }
        return $this->redis->publish($this->makeChannelName($channel, $user), $text);
    }

    /**
     * Сгенерировать название канала
     * @param $name
     * @param User $user
     * @return string
     */
    public function makeChannelName($name, User $user)
    {
        return $name . $this->generateSecHash($user);
    }

    /**
     * Сгенерировать секретный ключ
     * @param User $user
     * @return string
     */
    public function generateSecHash(User $user)
    {
        return md5(md5(Config::get('websocket.salt') . $user->id . $user->password . date('Y-m-d')));
    }


}