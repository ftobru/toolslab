<?php namespace Saas\Services;

use App;
use Auth;
use DB;
use Exception;
use Hash;
use Illuminate\Support\Str;
use Mail;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Models\UserInvite;
use Saas\References\UserReference;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Eloquent\Criteria\User\EmailCriteria;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Eloquent\UserInviteEloquentRepository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;
use Saas\Repositories\Interfaces\UserInviteRepositoryInterface;
use Saas\Repositories\Interfaces\UserRepositoryInterface;

class InviteService
{
    protected $subject = 'Вас пригласили в ModuleCRM';
    /** @var  UserEloquentRepository */
    protected $repository;
    /** @var UserInviteEloquentRepository */
    protected $userInviteRepository;
    /** @var CompanyEloquentRepository */
    protected $companyRepository;

    /**
     * @param UserRepositoryInterface $repositoryInterface
     * @param CompanyRepositoryInterface $companyRepository
     * @param UserInviteRepositoryInterface $userInviteRepository
     */
    public function __construct(
        UserRepositoryInterface $repositoryInterface,
        CompanyRepositoryInterface $companyRepository,
        UserInviteRepositoryInterface $userInviteRepository
    ) {
        $this->repository = $repositoryInterface;
        $this->userInviteRepository = $userInviteRepository;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @param $name
     * @param $email
     * @param Company $company
     * @return static
     * @throws Exception
     */
    public function inviteUser($name, $email, Company $company)
    {
        try {
            DB::beginTransaction();
            $inviteCode = $this->generateInviteCode();
            $user = $this->repository->pushCriteria(new EmailCriteria($email))->first();
            if ($user === null) {
                $user = $this->repository->create([
                    'email' => $email,
                    'name' => $name,
                    'password' => Hash::make('aSjsbxJbajkaj2e38asnNas7as4sf12ds3a16'),
                    'status' => UserReference::STATUS_NO_ACTIVE,
                    'company_id' => $company->id,
                    'reg_complete' => false,
                    'invited' => true
                ]);
            }

            $this->userInviteRepository->create([
                'user_id' => $user->id,
                'company_id' => $company->id,
                'invite_code' => $inviteCode
            ]);

            $this->sendInviteCode($inviteCode, $user, $company, $email, $name);
            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollBack();
            return false;
            //throw $e;
        }
    }

    /**
     * @param string $inviteCode
     * @param User $user
     * @param Company $company
     * @param string $email
     * @param $subject
     * @return bool
     */
    public function sendInviteCode($inviteCode, $user, $company, $email, $subject)
    {
        $link = App::make('url')->to('/auth/invite-confirmation?uid=') . $user->id . '&code=' . $inviteCode;
        Mail::queue('emails.invite', [
            'link' => $link,
            'user' => $user,
            'company' => $company,
            'name' => $subject
        ], function ($message) use ($email, $subject) {
            $message->to($email)->subject($subject);
        });
        return true;
    }

    /**
     * @param $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Генерация инвайт кода
     * @return string
     */
    protected function generateInviteCode()
    {
        return Str::random();
    }

    /**
     * @param UserInvite $invite
     * @param User $user
     * @return array
     */
    public function confirmInvite(UserInvite $invite, User $user)
    {
        /** @var Company $company */
        $company = $this->companyRepository->find($invite->company_id);
        if ($company) {
            if ($user->reg_complete === false) {
                // Пользаватель не зареган
                $company->users()->attach($user->id);
                return ['regComplete' => false];

            } else {
                // Пользаватель зареган
                if ($user->status === UserReference::STATUS_NO_ACTIVE) {
                    $user->status = UserReference::STATUS_ACTIVE;
                    $user->save();
                }
                $company->users()->attach($user->id);
                $invite->delete();
            }
        }
        return ['regComplete' => true];
    }

    /**
     * Добавляет данные о приглашенном пользователе.
     *
     * @param $all
     * @return bool
     */
    public function completeInvite($all)
    {
        $result=false;
        /** @var UserInvite $invite */
        $invite = $this->userInviteRepository->find(intval($all['invite_id']));
        if ($invite) {
            $data = [
                'name' => $all['invite_id'],
                'phone' => $all['phone'],
                'password' => Hash::make($all['password']),
                'reg_complete' => true,
                'status' => UserReference::STATUS_ACTIVE
            ];
            $this->repository->update($data, $invite->user_id);

            /** @var User $user */
            $user = $this->repository->find($invite->user_id);
            if (Auth::attempt(['email' => $user->email, 'password' => $user->password], true)) {
                Auth::user()->id;
            }
            $invite->delete();
            $result = true;
        }

        return $result;
    }
}