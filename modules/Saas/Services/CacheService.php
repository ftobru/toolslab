<?php namespace Saas\Services;

use App;
use Cache;
use Saas\Models\FormsEntity;
use Saas\Models\Role;
use Saas\Models\User;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Eloquent\Criteria\Forms\CompanyIdCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\FormLabelCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\FormTypeCriteria;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Database\Eloquent\Collection;
use Saas\Repositories\Eloquent\Criteria\FormsEntity\FormIdCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Repositories\Eloquent\FormsEntityEloquentRepository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;
use Saas\Repositories\Interfaces\FormRepositoryInterface;

/**
 * Class CacheService
 * @package Core\Services
 */
class CacheService
{
    // Cache keys
    const KEY_USER_ROLES    = 'roles_';
    const KEY_ROLE_PERMISSIONS = 'perms_';
    const KEY_COMPANY_NAME = 'company_';

    /** @var CompanyEloquentRepository */
    private $companyRepository;

    /**
     * @param CompanyRepositoryInterface $companyRepository
     */
    public function __construct(CompanyRepositoryInterface $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    // Company name cache

    /**
     * Clear cached company name key.
     *
     * @param string $company_name - 'name' field of Company
     */
    public function clearCompanyNameCache($company_name)
    {
        Cache::forget(self::KEY_COMPANY_NAME . $company_name);
    }

    /**
     * Get cached company by company name.
     *
     * @param string $company_name
     * @return mixed
     */
    public function getCompanyNameCache($company_name)
    {
        /** @var CompanyEloquentRepository $repository */
        $repository = $this->companyRepository;
        return Cache::rememberForever(
            self::KEY_COMPANY_NAME . $company_name,
            function () use ($company_name, $repository) {
                return $repository->findBy('name', $company_name);
            }
        );
    }

    // User roles

    /**
     * Clear cached roles for company user.
     *
     * @param int $company_id
     * @param int $user_id
     */
    public function clearUserRolesCache($company_id, $user_id)
    {
        Cache::forget(self::KEY_USER_ROLES . $company_id . '_' . $user_id);
    }

    /**
     * Get cached roles for company user. If it`s not exist - create cache with user roles.
     *
     * @param int $company_id
     * @param User $user
     * @return mixed
     */
    public function getUserRolesCache($company_id, User $user)
    {
        return Cache::rememberForever(
            self::KEY_USER_ROLES . $company_id . '_' . $user->id,
            function () use ($user, $company_id) {
                return $user->roles()->where('roles.company_id', '=', $company_id)->get();
            }
        );
    }

    /**
     * Get cached roles for company user. If it`s not exist - create cache with user roles.
     *
     * @param int $company_id
     * @param User $user
     */
    public function refreshUserRolesCache($company_id, User $user)
    {
        $this->clearUserRolesCache($company_id, $user->id);
        Cache::rememberForever(
            self::KEY_USER_ROLES . $company_id . '_' . $user->id,
            function () use ($user, $company_id) {
                return $user->roles()->where('roles.company_id', '=', $company_id)->get();
            }
        );
    }

    // Role permissions

    /**
     * Clear cached permissions for company role.
     *
     * @param int $company_id
     * @param int $role_id
     */
    public function clearRolePermissionsCache($company_id, $role_id)
    {
        Cache::forget(self::KEY_ROLE_PERMISSIONS . $company_id . '_' . $role_id);
    }

    /**
     * Get cached permissions for role. If it`s not exist - create cache for role.
     *
     * @param int $company_id
     * @param Role $role
     * @return mixed
     */
    public function getRolePermissionsCache($company_id, Role $role)
    {
        return Cache::rememberForever(
            self::KEY_ROLE_PERMISSIONS . $company_id . '_' . $role->id,
            function () use ($role) {
                return $role->perms()->get();
            }
        );
    }

    /**
     * Refresh cached permissions for company role.
     *
     * @param Role $role
     */
    public function refreshRolePermissionsCache(Role $role)
    {
        $this->clearRolePermissionsCache($role->company_id, $role->id);
        Cache::rememberForever(
            self::KEY_ROLE_PERMISSIONS . $role->company_id . '_' . $role->id,
            function () use ($role) {
                return $role->perms()->get();
            }
        );
    }
}
