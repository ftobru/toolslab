<?php namespace Saas\Services;

use Cache;
use Illuminate\Auth\Guard;
use Saas\Exceptions\CompanyNotFoundException;
use Saas\Exceptions\UserNotFoundException;
use Saas\Models\Role;
use Saas\Models\User;
use Saas\References\UserReference;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;

/**
 * Class AccessService
 * @package Saas\Services
 */
class AccessService
{
    /** @var Guard  */
    protected $auth;
    /** @var  StateApplicationService */
    protected $stateApp;
    /** @var UserEloquentRepository */
    protected $userRepository;
    /** @var CacheService */
    protected $cacheService;

    /**
     * @param Guard $guard
     * @param StateApplicationService $applicationService
     * @param CacheService $cacheService
     * @param UserRepositoryInterface $userRepositoryInterface
     */
    public function __construct(
        Guard $guard,
        StateApplicationService $applicationService,
        CacheService $cacheService,
        UserRepositoryInterface $userRepositoryInterface
    ) {
        $this->auth = $guard;
        $this->stateApp = $applicationService;
        $this->userRepository = $userRepositoryInterface;
        $this->cacheService = $cacheService;
    }

    /**
     * @param $permission
     * @param $companyId
     * @param null $userId
     * @return bool
     * @throws CompanyNotFoundException
     * @throws UserNotFoundException
     */
    public function canPermission($permission, $companyId = null, $userId = null)
    {
        /** @var User $user */
        if ($userId) {
            $user = $this->userRepository->find($userId);
        } else {
            $user = $this->auth->user();
        }

        if (!$user) {
            throw new UserNotFoundException;
        }

        if (!$companyId) {
            $company = $this->stateApp->getCurrentCompany();
            if (!$company) {
                throw new CompanyNotFoundException;
            }
            $companyId = $company->id;
        }

        // Роли юзера в текущей компании
        $roles = $this->cacheService->getUserRolesCache($companyId, $user);

        foreach ($roles as $role) {
            /** @var Role $role */
            $perms = $this->cacheService->getRolePermissionsCache($companyId, $role);
            foreach ($perms as $perm) {
                if ($perm->name === $permission || $perm->name === UserReference::OWNER_ROLE_NAME) {
                    return true;
                }
            }
        };
        return false;
    }

    /**
     * @param array $permissions
     * @param null $companyId
     * @param null $userId
     * @return bool
     * @throws CompanyNotFoundException
     * @throws UserNotFoundException
     */
    public function canOneOfPermissions(array $permissions, $companyId = null, $userId = null)
    {
        /** @var User $user */
        if ($userId) {
            $user = $this->userRepository->find($userId);
        } else {
            $user = $this->auth->user();
        }

        if (!$user) {
            throw new UserNotFoundException;
        }

        if (!$companyId) {
            $company = $this->stateApp->getCurrentCompany();
            if (!$company) {
                throw new CompanyNotFoundException;
            }
            $companyId = $company->id;
        }

        // Роли юзера в текущей компании
        $roles = $this->cacheService->getUserRolesCache($companyId, $user);
        foreach ($roles as $role) {
            /** @var Role $role */
            $perms = $this->cacheService->getRolePermissionsCache($companyId, $role);
            foreach ($perms as $perm) {
                if (in_array($perm->name, $permissions) || $perm->name === UserReference::OWNER_ROLE_NAME) {
                    return true;
                }
            }
        };
        return false;
    }
}
