<?php namespace Saas\Services;

use Illuminate\Mail\Mailer;
use Saas\Exceptions\UserNotFoundException;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;

/**
 * Class ConfirmEmailService
 * @package Saas\Services
 */
class ConfirmEmailService
{
    const SUBJECT = 'Пожалуйста потвердите ваш почтовый ящик';

    /** @var  UserEloquentRepository */
    protected $userRepository;

    /** @var  Mailer */
    protected $mailer;

    public function __construct(UserRepositoryInterface $interface, Mailer $mailer)
    {
        $this->userRepository = $interface;
        $this->mailer = $mailer;
    }

    /**
     * @param $to
     * @return mixed
     * @throws UserNotFoundException
     */
    public function send($to)
    {
        $user = $this->userRepository->findBy('email', $to);
        if (!$user) {
            throw new UserNotFoundException;
        }
        return $this->mailer->send('email.emailConfirm', $user, function ($message) use ($to, $user) {
            $message->to($to, $user->name)->subject(self::SUBJECT);
        });
    }

    public function confirm($code)
    {
        $user = $this->userRepository->findBy('email_confirm_code', $code);
        if (!$user) {
            throw new UserNotFoundException;
        }

    }
}
