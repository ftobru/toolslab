<?php namespace Saas\Services;

use Illuminate\Support\Facades\Facade;

class AccessFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'access';
    }

}
