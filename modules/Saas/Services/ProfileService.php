<?php namespace Saas\Services;

use App;
use Saas\Models\Profile;
use Saas\Repositories\Eloquent\Criteria\Profile\CompanyIdCriteria;
use Saas\Repositories\Eloquent\Criteria\Profile\UserIdCriteria;
use Saas\Repositories\Eloquent\ProfileEloquentRepository;
use Saas\Repositories\Interfaces\ProfileRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ProfileService
 * @package App\Services
 */
class ProfileService
{

    /** @var ProfileEloquentRepository */
    protected $repository;

    /**
     * @param ProfileRepositoryInterface $repository
     */
    public function __construct(ProfileRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $userId
     * @return Collection
     */
    public function getProfilesByUserId($userId)
    {
        return $this->repository->pushCriteria(new UserIdCriteria($userId))->all();
    }

    /**
     * @param $userId
     * @param $companyId
     * @return Collection
     */
    public function getProfile($userId, $companyId)
    {
        return $this->repository
            ->pushCriteria(new UserIdCriteria($userId))
            ->pushCriteria(new CompanyIdCriteria($companyId))
            ->first();
    }

    /**
     * @param $companyId
     * @return \Illuminate\Support\Collection
     */
    public function getProfilesByCompanyId($companyId)
    {
        return $this->repository->pushCriteria(new CompanyIdCriteria($companyId))->all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param array $data
     * @return static
     */
    public function store(array $data)
    {
        $this->prepareData($data);
        return $this->repository->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $this->prepareData($data);
        return $this->repository->update($data, $id);
    }

    /**
     * @param array $data
     * @return static
     */
    public function create(array $data)
    {
        $this->prepareData($data);
        return $this->repository->create($data);
    }

    /**
     * @param $formID
     * @return mixed
     */
    public function findById($formID)
    {
        return $this->repository->find($formID);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllProfiles()
    {
        return $this->repository->all();
    }

    /**
     * @param $companyId
     * @param $userId
     * @param $divisions
     * @return Profile
     */
    public function attachUserToCompanyDivisions($companyId, $userId, $divisions)
    {
        /** @var Profile $profile */
        $profile = $this->getProfile($userId, $companyId);

        if (!$profile) {
            $this->create(['user_id' => $userId, 'company_id' => $companyId, 'divisions' => $divisions]);
            return $this->getProfile($userId, $companyId);
        }

        $profile->divisions = json_encode($divisions);
        $profile->save();

        return $profile;
    }

    /**
     * @param $data array
     */
    private function prepareData(&$data)
    {
        $data['divisions'] = json_encode(array_get($data, 'divisions', []));
    }
}
