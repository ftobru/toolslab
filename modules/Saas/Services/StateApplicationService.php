<?php namespace Saas\Services;

use Cache;
use DB;
use Illuminate\Http\Request;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;

class StateApplicationService
{
    /** @var Request */
    protected $input;
    /** @var CompanyEloquentRepository  */
    protected $companyRepository;
    /** @var CacheService */
    private $cacheService;

    /**
     * @param Request $input
     * @param CompanyRepositoryInterface $repositoryInterface
     * @param CacheService $cacheService
     */
    public function __construct(
        Request $input,
        CompanyRepositoryInterface $repositoryInterface,
        CacheService $cacheService
    ) {
        $this->input = $input;
        $this->companyRepository = $repositoryInterface;
        $this->cacheService = $cacheService;
    }

    /**
     * @return mixed
     */
    public function getCompanyAlias()
    {
        return $this->input->company_alias;
    }

    /**
     * @return mixed|null
     */
    public function getCurrentCompany()
    {
        if ($name = $this->getCompanyAlias()) {
            return $this->cacheService->getCompanyNameCache($name);
        }
        return null;
    }
}
