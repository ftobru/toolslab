<?php namespace Saas\Services;

use App;
use File;
use Billing\Repositories\Eloquent\BillingAccountEloquentRepository;
use Billing\Repositories\Interfaces\BillingAccountRepositoryInterface;
use Saas\Exceptions\CompanyNotFoundException;
use Saas\Models\Company;

use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Eloquent\Criteria\Company\DisplayNameCriteria;
use Saas\Repositories\Eloquent\Criteria\Company\NameCriteria;
use Saas\Repositories\Eloquent\Criteria\Company\UserIdCriteria;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;

/**
 * Class CompanyService
 * @package Core\Services
 */
class CompanyService
{

    use Sortable, Paginatable;

    /** @var App  */
    protected $app;

    /** @var CompanyEloquentRepository  */
    protected $repository;

    /** @var BillingAccountEloquentRepository  */
    protected $billingRepository;
    /** @var CacheService */
    protected $cacheService;

    /**
     * @param App $app
     * @param CompanyRepositoryInterface $repositoryInterface
     * @param CacheService $cacheService
     * @param BillingAccountRepositoryInterface $accountRepositoryInterface
     */
    public function __construct(
        App $app,
        CompanyRepositoryInterface $repositoryInterface,
        CacheService $cacheService,
        BillingAccountRepositoryInterface $accountRepositoryInterface
    ) {

        $this->app = $app;
        $this->repository = $repositoryInterface;
        $this->billingRepository = $accountRepositoryInterface;
        $this->cacheService = $cacheService;
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        /** @var Company $company */
        $company = $this->repository->create($data);
        // Проверяем есть ли имя
        if (!$company->name) {
            $company->name = $this->generateCompanyName($company->id);
        }
        $company->save();
        return $company;
    }

    /**
     * Генерация названия компании
     * @param $companyId
     * @return string
     */
    public function generateCompanyName($companyId)
    {
        return 'c'. (int) $companyId;
    }

    /**
     * Генерация имени папки для компании.
     *
     * @param $companyId
     * @return string
     */
    public function generateCompanyDirName($companyId)
    {
        $company = $this->repository->find($companyId);
        return $this->makeCompanyFolderName($company);
    }

    /**
     * Генерация имени папки для компании.
     *
     * @param $alias
     * @return string
     */
    public function generateCompanyDirNameByAlias($alias)
    {
        $company = $this->repository->findBy('name', $alias);
        return $this->makeCompanyFolderName($company);
    }

    /**
     * Алгоритм генерации имени.
     *
     * @param Company $company
     * @return string
     */
    protected function makeCompanyFolderName(Company $company)
    {
        if ($company) {
            $folderName = $company->id . '_' . md5($company->created_at);
            $path = public_path() . '/files/' . $folderName;
            if (!File::exists($path)) {
                File::makeDirectory($path);
            }
            return $folderName;
        }
        abort(404);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }


    public function registry()
    {
        return $this->repository->all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->repository->update($data, $id);
    }

    /**
     * Reset cache for name attribute of company.
     *
     * @param Company $company
     * @param array $data
     * @return mixed
     * @throws CompanyNotFoundException
     */
    public function updateCompanyWithCache(Company $company, array $data)
    {
        if ($company) {
            if (isset($data['name']) && $data['name'] !== $company->name) {
                $this->cacheService->clearCompanyNameCache($company->name);
            }
            return $this->repository->update($data, $company->id);
        } else {
            throw new CompanyNotFoundException;
        }
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            if (isset($metaData['filter']['name'])) {
                $this->repository->pushCriteria(new NameCriteria($metaData['filter']['name']));

            }

            if (isset($metaData['filter']['display_name'])) {
                $this->repository->pushCriteria(new DisplayNameCriteria($metaData['filter']['display_name']));

            }
        }
        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginate($perPage);
        }
        return $this->repository->all();
    }

    /**
     * @param $companyId
     * @param $userId
     * @return array
     */
    public function attachUserToCompany($companyId, $userId)
    {
        /** @var Company $company */
        $company = $this->repository->find($companyId);
        $company->users()->attach($userId);
        return ['user' => $userId, 'company' => $company];
    }

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getByUserId($userId)
    {
        return $this->repository->getByCriteria(new UserIdCriteria($userId))->all();
    }

    /**
     * @param $companyName
     * @return \Illuminate\Support\Collection
     */
    public function getByName($companyName)
    {
        $stateApp = App::make('StateApplicationService');
        return $stateApp->getCurrentCompany();
        //return $this->repository->getByCriteria(new NameCriteria($companyName))->first();
    }



    /**
     * @param $cid
     * @param $uid
     */
    public function deleteUserFromCompany($cid, $uid)
    {
        Company::find($cid)->users()->detach($uid);
        return $uid;
    }
}