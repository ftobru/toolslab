<?php namespace Saas\Services;


use Alexusmai\Ruslug\Slug;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Lang;
use Saas\Models\Company;
use Saas\Models\Permission;
use Saas\Models\Role;
use Saas\References\UserReference;
use Saas\Repositories\Eloquent\Criteria\Role\CompanyIdCriteria;
use Saas\Repositories\Eloquent\Criteria\Role\NameCriteria;
use Saas\Repositories\Eloquent\RoleEloquentRepository;
use Saas\Repositories\Interfaces\RoleRepositoryInterface;

/**
 * Class RoleService
 * @package Core\Services
 */
class RoleService
{
    /** @var RoleEloquentRepository  */
    protected $repository;
    /** @var Slug */
    protected $slug;
    /** @var CacheService */
    private $cacheService;

    /**
     * @param RoleRepositoryInterface $roleRepositoryInterface
     * @param CacheService $cacheService
     * @param Slug $slug
     */
    public function __construct(
        RoleRepositoryInterface $roleRepositoryInterface,
        CacheService $cacheService,
        Slug $slug
    ) {
        $this->repository = $roleRepositoryInterface;
        $this->slug = $slug;
        $this->cacheService = $cacheService;
    }

    /**
     * All roles of company.
     *
     * @param $companyId
     * @return Collection
     */
    public function getByCompanyId($companyId)
    {
        return $this->repository->findAllBy('company_id', $companyId);
    }

    /**
     * Get owner role of company.
     *
     * @param $companyId
     * @return \Illuminate\Support\Collection
     */
    public function findOwnerRoleForCompany($companyId)
    {
        return $this->repository
            ->pushCriteria(new NameCriteria(UserReference::OWNER_ROLE_NAME))
            ->pushCriteria(new CompanyIdCriteria($companyId))
            ->all();
    }

    /**
     * @param Role $role
     * @param Company $company
     * @return array
     */
    public function getDataForEditView(Role $role, Company $company)
    {
        $checkbox_labels = [
            'sms' => 'SMS',
            'email' => 'email'
        ];
        $select_options = Lang::get('permissions-options', [], 'ru');
        //
        $permissions = $role->perms()->get();
        $perm_names = [];
        foreach ($permissions as $permission) {
            $perm_names[$permission->name] = 1;
        }

        $allPermissions = Lang::get('permissions', [], 'ru');
        $allPermissionsKeys = array_keys($allPermissions);

        $allPermissionsArray = [];
        foreach ($allPermissions as $perm_key => $userPermission) {
            $tmp_array = $userPermission;
            $tmp_array['perm_key'] = $perm_key;
            foreach (array_reverse(explode('.', $perm_key)) as $key) {
                $tmp_array = [$key => $tmp_array];
            }
            $allPermissionsArray = array_merge_recursive($allPermissionsArray, $tmp_array);
        }

        $permGroups = Lang::get('permissions-groups', [], 'ru');

        foreach ($permGroups as $module_name => $module_config) {
            foreach (array_get($allPermissionsArray, $module_name, []) as $key => $permissionGroup) {
                if (isset($permGroups[$module_name]['components'][$key]) &&
                    !isset($permGroups[$module_name]['components'][$key]['class'])
                ) {
                    foreach ($permissionGroup as $p_name => $p_option) {
                        $allPermissionsArray[$module_name][$key][$p_name]['option'] = 0;
                        if (isset($p_option['perm_key'])) {
                            if (isset($perm_names[$p_option['perm_key']])) {
                                $allPermissionsArray[$module_name][$key][$p_name]['option'] = 1;
                            }
                            $allPermissionsArray[$module_name][$key][$p_name]['options'] = $this
                                ->getOptions($p_option['perm_key']);
                        } else {
                            $group_perm_key = '';
                            foreach ($p_option as $var_p_option) {
                                if (empty($group_perm_key)) {
                                    $group_perm_key = preg_replace('/\\.[^.\\s]{1,20}$/', '', $var_p_option['perm_key']);
                                }
                                if (isset($perm_names[$var_p_option['perm_key']])) {
                                    //$allPermissionsArray[$module_name][$key][$p_name]['option'] = 0;
                                    $allPermissionsArray[$module_name][$key][$p_name]['option'] = $this
                                        ->getOptionValue($var_p_option['perm_key']);
                                }
                            }
                            if (!empty($group_perm_key)) {
                                $allPermissionsArray[$module_name][$key][$p_name]['options'] = $this
                                    ->getOptions($group_perm_key);
                            }
                        }
                    }
                } elseif (isset($permGroups[$module_name]['components'][$key]) &&
                    isset($permGroups[$module_name]['components'][$key]['class'])
                ) {
                    foreach ($permissionGroup as $p_name => $p_option) {
                        foreach ($p_option as $checkbox_key => $checkbox) {
                            $allPermissionsArray[$module_name][$key][$p_name][$checkbox_key]['option'] = '';
                            if (isset($perm_names[$checkbox['perm_key']])) {
                                $allPermissionsArray[$module_name][$key][$p_name][$checkbox_key]['option'] = 'checked';
                            }
                            if (isset($checkbox_labels[$checkbox_key])) {
                                $allPermissionsArray[$module_name][$key][$p_name][$checkbox_key]['label'] = $checkbox_labels[$checkbox_key];
                            }
                        }
                    }
                }
            }
        }

        return [
            'role' => $role,
            'company' => $company,
            'permissions' => $permissions,
            'allPermissionsArray' => $allPermissionsArray,
            'permissions_groups' => Lang::get('permissions-groups', [], 'ru'),
        ];
    }

    /**
     * @param array $data
     * @param Role $role
     * @throws Exception
     */
    public function updateRolePermissions(array $data, Role $role)
    {
        $select_options = Lang::get('permissions-options', [], 'ru');
        $permissions = $role->perms()->get();
        $perm_names = [];
        foreach ($permissions as $permission) {
            $perm_names[$permission->name] = $permission->name;
        }

        $attachPermissions = [];
        $detachPermissions = [];

        // Селекты CRM
        foreach ($data['crm_selects'] as $permission_key => $perm_info) {
            $perm_value = intval($perm_info['value']);
            $perm_count = intval($perm_info['count']);

            if ($perm_value === 0) {
                // Удаляем
                $matches = array_filter($perm_names, function ($haystack) use ($permission_key) {
                    if (strpos($haystack, $permission_key) === false) {
                        return false;
                    }
                    return true;
                });
                if (!empty($matches)) {
                    foreach ($matches as $k => $v) {
                        $detachPermissions[] = $v;
                    }
                }
            } else {
                $matches = array_filter($perm_names, function ($haystack) use ($permission_key) {
                    if (strpos($haystack, $permission_key) === false) {
                        return false;
                    }
                    return true;
                });
                if (!empty($matches)) {
                    foreach ($matches as $k => $v) {
                        $detachPermissions[] = $v;
                    }
                }
                $attachPermissions[$permission_key . $select_options[$perm_value]['postfix']] =
                    $permission_key . $select_options[$perm_value]['postfix'];
            }
        }

        // Селекты лендингов
        foreach ($data['landing_selects'] as $permission_key => $perm_info) {
            $perm_value = intval($perm_info['value']);

            if ($perm_value === 0) {
                // Удаляем
                $matches = array_filter($perm_names, function ($haystack) use ($permission_key) {
                    if (strpos($haystack, $permission_key) === false) {
                        return false;
                    }
                    return true;
                });
                if (!empty($matches)) {
                    foreach ($matches as $k => $v) {
                        $detachPermissions[] = $v;
                    }
                }
            } else {
                $matches = array_filter($perm_names, function ($haystack) use ($permission_key) {
                    if (strpos($haystack, $permission_key) === false) {
                        return false;
                    }
                    return true;
                });
                if (!empty($matches)) {
                    foreach ($matches as $k => $v) {
                        $detachPermissions[] = $v;
                    }
                }
                $attachPermissions[$permission_key . $select_options[$perm_value]['postfix']] = $permission_key . $select_options[$perm_value]['postfix'];
            }
        }

        // Чекбоксы CRM
        foreach ($data['crm_checkboxes'] as $permission_key => $perm_value) {
            if($perm_value === 'true') {
                $attachPermissions[$permission_key] = $permission_key;
            } else {
                $detachPermissions[$permission_key] = $permission_key;
            }
        }


        // Находим ids разрешений
        /** @var Collection $allPermissions */
        $allPermissions = Permission::all();
        foreach ($detachPermissions as $key => $attachePermission) {
            $filtered_collection = $allPermissions->filter(function ($item) use ($attachePermission) {
                if ($item->name === $attachePermission) {
                    return true;
                };
                return false;
            });
            if (!$filtered_collection->isEmpty()) {
                $detachPermissions[$key] = $filtered_collection->first()->id;
            }
        };
        foreach ($attachPermissions as $key => $attachePermission) {
            $filtered_collection = $allPermissions->filter(function ($item) use ($attachePermission) {
                if ($item->name === $attachePermission) {
                    return true;
                };
                return false;
            });
            if (!$filtered_collection->isEmpty()) {
                $attachPermissions[$key] = $filtered_collection->first()->id;
            }
        };

        try {
            DB::beginTransaction();

            $role->update([
                'name' => $this->slug->make(array_get($data, 'display_name')),
                'display_name' => array_get($data, 'display_name'),
                'description' => array_get($data, 'description')
            ]);
            if (!empty($detachPermissions)) {
                $role->perms()->detach($detachPermissions);
            }
            if (!empty($attachPermissions)) {
                $role->perms()->sync($attachPermissions);
            }
            DB::commit();
            $this->cacheService->refreshRolePermissionsCache($role);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * Проверка на начало строки.
     *
     * @param $haystack
     * @param $needle
     * @return bool
     */
    private function startsWith($haystack, $needle)
    {
        // search backwards starting from haystack length characters from the end
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    /**
     * @param string $perm_key
     * @return array
     */
    private function getOptions($perm_key)
    {
        //$perm_key = $p_option['perm_key'];
        $allPermissions = Lang::get('permissions', [], 'ru');
        $allPermissionsKeys = array_keys($allPermissions);
        $select_options = Lang::get('permissions-options', [], 'ru');

        // Получаем все права по маске
        $tmp_array = array_filter($allPermissionsKeys, function ($item) use ($perm_key) {
            return $this->startsWith($item, $perm_key);
        });
        // Получили массив постфиксы прав
        foreach ($tmp_array as $tmp_array_key => $tmp_array_value) {
            $tmp_array[$tmp_array_key] = str_replace($perm_key, '', $tmp_array_value);
        }
        $s_options = array_filter($select_options, function ($item) use ($tmp_array) {
            if (isset($item['postfix'])) {
                return in_array($item['postfix'], $tmp_array);
            }
            return false;
        });
        $s_options['0'] = $select_options[0];
        return $s_options;
    }

    /**
     * @param string $permissionKey
     * @return int
     */
    private function getOptionValue($permissionKey)
    {
        preg_match('/\.[^\.]+$/i', $permissionKey, $postfix);
        $select_options = Lang::get('permissions-options', [], 'ru');

        $tmp_array = array_filter($select_options, function ($item) use ($postfix) {
            if (isset($item['postfix'])) {
                return $item['postfix'] === $postfix[0];
            }
            return false;
        });

        if (!empty($tmp_array)) {
            return array_values($tmp_array)[0]['value'];
        }
        return 0;
    }
}
