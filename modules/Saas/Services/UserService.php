<?php namespace Saas\Services;

use App;
use Config;
use DB;
use Core\Traits\Groupable;
use Hash;
use Saas\Exceptions\UserNotFoundException;
use Saas\Models\Company;
use Saas\Models\Permission;
use Saas\Models\Role;
use Saas\References\UserReference;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use Saas\Models\User;
use Auth;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Support\Str;
use Saas\Repositories\Eloquent\Criteria\User\CompaniesNotUserIdOwner;
use Saas\Repositories\Eloquent\Criteria\User\EmailCriteria;
use Saas\Repositories\Eloquent\Criteria\User\HasCompanyNameCriteria;
use Saas\Repositories\Eloquent\Criteria\User\NameCriteria;
use Saas\Repositories\Eloquent\Criteria\User\PhoneCriteria;
use Saas\Repositories\Eloquent\Criteria\User\WithCriteria;
use Saas\Repositories\Eloquent\Criteria\UserIdCriteria;
use Saas\Repositories\Eloquent\RoleEloquentRepository;
use Saas\Repositories\Eloquent\TemporaryRegistrationEloquentRepository;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\RoleRepositoryInterface;
use Saas\Repositories\Interfaces\TemporaryRegistrationRepositoryInterface;
use Saas\Repositories\Interfaces\UserRepositoryInterface;
use Validator;

/**
 * Class UserService
 * @package Saas\Services
 */
class UserService
{
    use Sortable, Paginatable, Groupable;

    /** @var UserEloquentRepository  */
    protected $repository;

    /** @var App  */
    protected $app;

    /** @var CompanyService  */
    protected $companyService;

    /** @var RoleRepositoryInterface|RoleEloquentRepository  */
    protected $roleRepository;

    /** @var RoleService  */
    protected $roleService;
    /**
     * @var TemporaryRegistrationEloquentRepository
     */
    protected $temporaryRegistrationRepository;

    /**
     * @param App $app
     * @param UserRepositoryInterface $repository
     * @param CompanyService $companyService
     * @param RoleRepositoryInterface $repositoryInterface
     * @param TemporaryRegistrationRepositoryInterface $temporaryRegistrationRepository
     * @param RoleService $roleService
     */
    public function __construct(
        App $app,
        UserRepositoryInterface $repository,
        CompanyService $companyService,
        RoleRepositoryInterface $repositoryInterface,
        TemporaryRegistrationRepositoryInterface $temporaryRegistrationRepository,
        RoleService $roleService
    ) {
        $this->app = $app;
        $this->repository = $repository;
        $this->companyService = $companyService;
        $this->roleRepository = $repositoryInterface;
        $this->roleService = $roleService;
        $this->temporaryRegistrationRepository = $temporaryRegistrationRepository;
    }

    /**
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|max:16|unique:phone',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * @param $userData
     * @return array
     * @throws \Exception
     */
    public function registration($userData)
    {
        if ($this->findUserByPhone($userData['phone'])) {
            return ['error' => UserReference::ERROR_PHONE_ALREADY_EXIST];
        };
        if ($this->findUserByEmail($userData['email'])) {
            return ['error' => UserReference::ERROR_EMAIL_ALREADY_EXIST];
        }

        return ['user' => $this->repository->create($userData)];
    }

    /**
     * @param $userId
     * @param $userData
     * @return mixed
     */
    public function updateRegistrationData($userId, $userData)
    {
        /** @var User $user */
        $user = $this->repository->find($userId);
        if ($user->status === 0 && $user->registration_token === $userData['registration_token']) {
            $user->phone = $userData['phone'];
            $user->email = $userData['email'];
            $user->save();
            return ['user' => $user];
        }
        return ['error' => UserReference::ERROR_INVALID_REGISTRATION_TOKEN];
    }

    /**
     * @param $email
     * @param $password
     * @param bool $remember
     * @return bool|int|mixed
     */
    public function login($email, $password, $remember = false)
    {
        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            return Auth::user()->id;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function logout()
    {
        Auth::logout();
        return true;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->repository->update($data, $id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        /** @var User $user */
        $user = $this->repository->create($data);
        $user->roles()->sync($data['roles']);
        $user->companies()->attach($data['companies']);

        return $user;
    }

    /**
     * Фильтрация, пагинаци, сортировка.
     * @param array $metaData
     * @return RepositoryInterface
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        $statuses = UserReference::statusesInfo();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'statuses' => $statuses,
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'type') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new TypeCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'status') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new StatusCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new NameStartWithCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'types' => $types,
                'statuses' => $statuses,
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * @see CheckUserToCompany
     *
     * @param $userId
     * @param $companyName
     * @return \Illuminate\Support\Collection
     */
    public function findUserToCompany($userId, $companyName)
    {
        return $this->repository->pushCriteria(new UserIdCriteria($userId))
            ->pushCriteria(new HasCompanyNameCriteria($companyName))->all();
    }

    /**
     * @param $token
     * @return User
     */
    public function findUserByRememberToken($token)
    {
        return $this->repository->findBy('remember_token', (string)$token);
    }

    /**
     * Возвращает количество пользователей компании.
     *
     * @param $company_id
     * @return int
     */
    public function getAllUsersCountForCompany($company_id)
    {
        $owner_id = Company::find($company_id)->user_id;
        $companiesUsers = DB::table('company_user')
            ->join('users', 'users.id', '=', 'company_user.user_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', function ($join) use ($company_id) {
                $join->on('roles.id', '=', 'role_user.role_id')
                    ->where('roles.company_id', '=', $company_id);
            })
            ->select('*')
            ->where('company_user.company_id', '=', $company_id)
            ->where('company_user.user_id', '<>', $owner_id)
            ->select(
                'users.id',
                'users.name',
                'users.patronymic',
                'users.last_name',
                'users.email',
                'users.phone',
                'users.status',
                'roles.display_name',
                'roles.id AS role_id'
            )
            ->count();
        return $companiesUsers;

    }

    /**
     * Возвращает всех пользователей компании за исключением хозяина компании.
     *
     * @param $company_id
     * @return mixed
     */
    public function getUsersForCompany($company_id, $request) {
        $statuses = UserReference::statusesInfo();
        $owner_id = Company::find($company_id)->user_id;

        $companiesUsers = DB::table('company_user')
            ->join('users', 'users.id', '=', 'company_user.user_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', function ($join) use ($company_id) {
                $join->on('roles.id', '=', 'role_user.role_id')
                    ->where('roles.company_id', '=', $company_id);
            })
            ->select('*')
            ->where('company_user.company_id', '=', $company_id)
            ->where('company_user.user_id', '<>', $owner_id)
            ->select(
                'users.id',
                'users.name',
                'users.patronymic',
                'users.last_name',
                'users.email',
                'users.phone',
                'users.status',
                'roles.display_name',
                'roles.id AS role_id'
            )
            ->get();

        $collection = Collection::make($companiesUsers);

        // Filter it
        if (isset($request['options']['filter']['filters']) && is_array($request['options']['filter']['filters'])) {
            $filters = $request['options']['filter']['filters'];
            $collection = $collection->filter(function ($item) use ($filters) {
                $return = true;
                foreach ($filters as $filter) {
                    if (isset($filter['field']) && isset($filter['field']) && isset($filter['field'])) {
                        switch ($filter['field']) {
                            case 'status':
                                if ($item->status !== intval($filter['value'])) {
                                    $return = false;
                                }
                                break;
                            case 'role':
                                if ($item->role_id !== intval($filter['value'])) {
                                    $return = false;
                                }
                                break;
                            case 'name':
                                $needle = mb_strtolower($filter['value']);
                                if (!empty($needle)) {
                                    $haystack = mb_strtolower($item->name);
                                    if (strpos($haystack, $needle) === false) {
                                        $return = false;
                                    }
                                }
                                break;
                            case 'lastname':
                                $needle = mb_strtolower($filter['value']);
                                if (!empty($needle)) {
                                    $haystack = mb_strtolower($item->last_name);
                                    if (strpos($haystack, $needle) === false) {
                                        $return = false;
                                    }
                                }
                                break;
                            case 'patr':
                                $needle = mb_strtolower($filter['value']);
                                if (!empty($needle)) {
                                    $haystack = mb_strtolower($item->patronymic);
                                    if (strpos($haystack, $needle) === false) {
                                        $return = false;
                                    }
                                }
                                break;
                            case 'phone':
                                $needle = mb_strtolower($filter['value']);
                                if (!empty($needle)) {
                                    $haystack = mb_strtolower($item->phone);
                                    if (strpos($haystack, $needle) === false) {
                                        $return = false;
                                    }
                                }
                                break;
                            case 'email':
                                $needle = mb_strtolower($filter['value']);
                                if (!empty($needle)) {
                                    $haystack = mb_strtolower($item->email);
                                    if (strpos($haystack, $needle) === false) {
                                        $return = false;
                                    }
                                }
                                break;
                        }
                    }
                }
                return $return;
            });
        }

        $total = $collection->count();

        return [
            'data' => $collection->toArray(),
            'filters_data' => [
                'statuses' => $statuses,
            ],
            'total' => $total,
            'groups' => []
        ];
    }

    /**
     * @param $email
     * @return mixed
     */
    public function findUserByEmail($email)
    {
        return $this->repository->pushCriteria(new EmailCriteria($email))->first();
    }

    /**
     * @param $phone
     * @return mixed
     */
    public function findUserByPhone($phone)
    {
        return $this->repository->findBy('phone', (string)$phone);
    }

    /**
     * @param $code
     * @return bool
     */
    public function checkUserActivationCode($code)
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::getUser();
            if ($user->activation_code == $code) {
                return true;
            }
        }
        return false;
    }

    /**
     * Activate current user.
     */
    public function activateCurrentUser()
    {
        if (Auth::check()) {
            /** @var User $user */
            $user = Auth::getUser();
            $user->update([
                'status' => UserReference::STATUS_ACTIVE,
                'registration_token' => '',
                'activation_code' => ''
            ]);

        }
    }

    /**
     * @param User $user
     * @param $password
     * @return User
     */
    public function changePassword(User $user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();
        return $user;
    }

    /**
     * @param User $user
     * @param $newPassword
     * @param $oldPassword
     * @return bool|User
     */
    public function changeUserPassword(User $user, $newPassword, $oldPassword)
    {
        if (Hash::check($oldPassword, $user->password)) {
            $this->changePassword($user, $newPassword);
            Auth::logout();
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param $userId
     * @return mixed
     */
    public function findById($userId)
    {
        return $this->repository->find($userId);
    }

    /**
     * Attached with companies criteria.
     *
     * @param $userId
     * @return mixed
     */
    public function findUserByIdWithCompanies($userId)
    {
        return $this->repository
            ->pushCriteria(new WithCriteria())
            ->pushCriteria(new UserIdCriteria($userId))
            ->applyCriteria()
            ->first();
    }

    /**
     * Получит код активации аккаунта
     * @return string
     */
    public function generateActivationCode()
    {
        return Str::random();
    }

    /**
     * Получить все компании в которых пользователь состоит но не является owner
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getCompaniesNotOwner($userId)
    {
        return $this->repository->getByCriteria(new CompaniesNotUserIdOwner($userId))->all();
    }

    /**
     * @param User $user
     * @return string
     */
    public function setRememberToken(User $user)
    {
        $user->remember_token = Str::random(16);
        $user->save();
        return $user;
    }

    /**
     * @return Collection
     */
    public function getActiveUsers()
    {
        return $this->repository->getByStatus(UserReference::STATUS_ACTIVE);
    }

    /**
     * @param $userId
     * @param $name
     * @param null $lastName
     * @param null $patronymic
     * @return mixed
     * @internal param null $patronymic
     */
    public function changeName($userId, $name, $lastName = null, $patronymic = null)
    {
        return $this->repository->update([
            'name' => $name,
            'last_name' => $lastName,
            'patronymic' => $patronymic
        ], $userId);
    }

    /**
     * @param $userId
     * @param $delivery
     * @return mixed
     */
    public function changeDelivery($userId, $delivery)
    {
        return $this->repository->update([
            'delivery' => $delivery
        ], $userId);

    }

    /**
     * @param $userId
     * @param $gender
     * @return mixed
     */
    public function changeGender($userId, $gender)
    {
        return $this->repository->update([
            'gender' => $gender
        ], $userId);
    }

    public function changeEmailPhone($userId, $inputArr)
    {
        return $this->repository->update($inputArr, $userId);
    }

    /**
     * @param $userId
     * @param $companyId
     * @param $rates
     * @return mixed
     * @throws UserNotFoundException
     */
    public function addOwner($userId, $companyId)
    {

        $role = $this->roleService->findOwnerRoleForCompany($companyId);
        if ($role->isEmpty()) {
            $role = $this->createRoleOwner($companyId);
        }
        /** @var User $user */
        $user = $this->repository->find($userId);
        if (!$user) {
            throw new UserNotFoundException;
        }
        $user->roles()->attach($role->id);
        $user->save();
        return $role;
    }

    /**
     * @param $companyId
     * @return Role
     */
    public function createRoleOwner($companyId)
    {
        /** @var Role $role */
        $role = $this->roleRepository->create([
            'name' => UserReference::OWNER_ROLE_NAME,
            'display_name' => UserReference::OWNER_ROLE_DISPLAY_NAME,
            'company_id' => $companyId
        ]);
        $role->attachPermission(Permission::where('name', '=', UserReference::OWNER_PERMISSION_NAME)->get()->first());

        return $role;
    }


    /**
     * Записывает кто заполнил первую форму регистрации.
     *
     * @param array $all
     * @return mixed
     */
    public function saveDemoRegistrationData(array $all)
    {
        preg_match('/^\+7\s(\([0-9]{3}\)\s\d{3}-\d{4})$/', $all['phone'], $matches, PREG_OFFSET_CAPTURE);
        if (isset($matches[1][0])) {
            $this->temporaryRegistrationRepository->create([
                'phone' => $matches[1][0],
                'email' => $all['email'],
                'name' => $all['name'],
            ]);

            // Логиним демо-юзера.
            $email = Config::get('demo-user.credentials.email');
            $password = Config::get('demo-user.credentials.password');

            /** @var User $user */
            $user = Auth::user();
            if ($user) {
                if ($user->demo) {
                    return [
                        'success' => true,
                        'msg' => 'Успешно авторизован'
                    ];
                } else {
                    Auth::logout();
                }
            }
            $user = $this->login($email, $password);
            if ($user) {
                return [
                    'success' => true,
                    'msg' => 'Успешно авторизован'
                ];
            }
            return [
                'success' => false,
                'msg' => 'Ошибка авторизации'
            ];
        }

        return [
            'success' => false,
            'msg' => 'Неверный формат телефона'
        ];
    }

    /**
     * @param $email
     * @return array
     */
    public function checkEmailExists($email)
    {
        $user = $this->repository->pushCriteria(new EmailCriteria($email))->first();
        if ($user) {
            return ['exist' => true, 'email' => $email];
        }
        return ['exist' => false, 'email' => $email];
    }
}
