<?php namespace Saas\Exceptions;

use LucaDegasperi\OAuth2Server\Middleware\OAuthExceptionHandlerMiddleware;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class InvalidRememberTokenException
 * @package Core\Exceptions
 */
class InvalidRememberTokenException extends AuthenticationException
{
    /**
     * {@inheritdoc}
     */
    public function getMessageKey()
    {
        return 'Invalid Remember token.';
    }
}