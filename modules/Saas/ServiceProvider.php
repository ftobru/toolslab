<?php namespace Saas;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package Modules\LandingControll
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register('Tymon\JWTAuth\Providers\JWTAuthServiceProvider');

        // Register confide
        $this->app->register('Zizaco\Entrust\EntrustServiceProvider');

        // Tagging
        $this->app->register('Conner\Tagging\TaggingServiceProvider');

        // OAuth
        $this->app->register('LucaDegasperi\OAuth2Server\Storage\FluentStorageServiceProvider');
        $this->app->register('LucaDegasperi\OAuth2Server\OAuth2ServerServiceProvider');

        $this->app->register('Thujohn\Twitter\TwitterServiceProvider');

        $this->app->register('Skovmand\Mailchimp\MailchimpServiceProvider');

        $this->app->alias('Zizaco\Entrust\EntrustFacade', 'Entrust');

        $this->app->bind(
            'Saas\Repositories\Interfaces\FormRepositoryInterface',
            'Saas\Repositories\Eloquent\FormEloquentRepository'
        );

        $this->app->bind('FormsEntityRepositoryInterface', 'Saas\Repositories\Eloquent\FormsEntityEloquentRepository');

        $this->app->bind(
            'Saas\Repositories\Interfaces\UserRepositoryInterface',
            'Saas\Repositories\Eloquent\UserEloquentRepository'
        );

        $this->app->bind(
            'Saas\Repositories\Interfaces\UserInviteRepositoryInterface',
            'Saas\Repositories\Eloquent\UserInviteEloquentRepository'
        );

        $this->app->bind(
            'Saas\Repositories\Interfaces\CompanyRepositoryInterface',
            'Saas\Repositories\Eloquent\CompanyEloquentRepository'
        );

        $this->app->bind(
            'Saas\Repositories\Interfaces\FormsEntityRepositoryInterface',
            'Saas\Repositories\Eloquent\FormsEntityEloquentRepository'
        );

        $this->app->bind(
            'Saas\Repositories\Interfaces\RoleRepositoryInterface',
            'Saas\Repositories\Eloquent\RoleEloquentRepository'
        );

        $this->app->bind(
            'Saas\Repositories\Interfaces\ProfileRepositoryInterface',
            'Saas\Repositories\Eloquent\ProfileEloquentRepository'
        );

        $this->app->bind(
            'Saas\Repositories\Interfaces\TemporaryRegistrationRepositoryInterface',
            'Saas\Repositories\Eloquent\TemporaryRegistrationEloquentRepository'
        );

        $this->app->bind('SocketService', 'Saas\Services\SocketService');

        $this->app->bind('CacheService', 'Saas\Services\CacheService');

        // Services system
        $this->app->bind('UserService', 'Saas\Services\UserService');

        $this->app->bind('FormsEntityService', 'Saas\Services\FormsEntityService');

        $this->app->bind('ProductService', 'Saas\Services\ProductService');

        $this->app->bind('FormsService', 'Saas\Services\FormsService');

        $this->app->bind('CompanyService', 'Saas\Services\CompanyService');

        $this->app->bind('RoleService', 'Saas\Services\RoleService');

        $this->app->bind('StateApplicationService', 'Saas\Services\StateApplicationService');

        $this->app->bind('access', 'Saas\Services\AccessService');
    }
}
