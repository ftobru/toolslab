<?php namespace Saas\Composers;
use DB;
use Illuminate\View\View;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class UserCompaniesComposer
 * @package Saas\Composers
 */
class UserCompaniesComposer {

    /** @var  CompanyService */
    protected $companyService;

    /**
     * @param CompanyService $companyService
     */
    public function __construct(CompanyService $companyService)
    {
        $this->companyService = $companyService;

    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        /** @var User $user */
        $user = \Auth::getUser();
        $companies = $this->companyService->getByUserId($user->id);

        $companyIdList = [];
        foreach ($companies as $company) {
            array_push($companyIdList, $company->id);
        }

        $availableCompanies = DB::table('company_user')->select()
            ->join('companies', 'companies.id', '=', 'company_user.company_id')
            ->where('company_user.user_id', $user->id)
            ->whereNotIn('company_user.company_id', $companyIdList)
            ->get();


        $view->with('user_companies', $companies);
        $view->with('available_companies', $availableCompanies);
    }
}
