<?php namespace Saas\Composers;

use AccessService;
use Auth;
use Caffeinated\Menus\Builder;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\View\View;
use Lang;
use Menu;
use Route;
use Saas\Models\Company;
use Saas\Services\StateApplicationService;

/**
 * Class MenuComposer
 * @package Saas\Composers
 */
class MenuComposer
{
    /** @var StateApplicationService */
    protected $stateApp;
    /** @var Guard */
    protected $auth;

    public function __construct(StateApplicationService $applicationService, Guard $authManager)
    {
        $this->stateApp = $applicationService;
        $this->auth = $authManager;
    }

    public function compose(View $view)
    {
        $user = $this->auth->user();

        /** @var Company $company */
        $company = $this->stateApp->getCurrentCompany();
        if (!($user)) {
            return;
        }

        $menu = Menu::make('general', function (Builder $menu) use ($company) {

            $params = Route::getCurrentRequest()->all();
            $params_string = '';
            if(!empty($params)) {
                $params_string = '?' . http_build_query($params);
            }

            // Дашборд
            $menu->add(
                $this->iconWrapper(Lang::get('menu.dashboard', [], 'ru'), 'md-home'),
                route('crm.dashboard', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.dashboard.menu']);

            // Контакты
            $menu->add(
                $this->iconWrapper(Lang::get('menu.clients', [], 'ru'), 'md-people'),
                route('crm.clients', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', [
                'crm.contacts.view.all', 'crm.contacts.view.their', 'crm.firms.view.all', 'crm.firms.view.their'
            ]);

            // Задачи
            $menu->add(
                $this->iconWrapper(Lang::get('menu.tasks', [], 'ru'), 'md-format-list-numbered'),
                route('crm.tasks', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.tasks.view.all', 'crm.tasks.view.their']);

            // Сделки
            $menu->add(
                $this->iconWrapper(Lang::get('menu.orders', [], 'ru'), 'md-attach-money'),
                route('crm.deals', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.deals.view.all', 'crm.deals.view.their']);

            // CRM
            $crm = $menu->add(
                $this->iconWrapper(Lang::get('menu.crm', [], 'ru'), 'md-content-copy'),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->data('permissions', ['crm.products.view.all', 'crm.products.view.their'])->attribute(['class' => 'gui-folder']);

            $crm->add(
                $this->titleWrapper(Lang::get('menu.products', [], 'ru')),
                route('crm.products', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.products.view.all', 'crm.products.view.their']);
//            $crm->add(
//                $this->titleWrapper(Lang::get('menu.templates', [], 'ru')),
//                route('landingControl.templates')
//            )->data('permissions', ['templates']);
            // Лендинги
            $landings = $menu->add(
                $this->iconWrapper(Lang::get('menu.landings', [], 'ru'), 'md-view-quilt'),
                route(empty(Route::currentRouteName()) ? 'demo'
                    : Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->data('permissions', ['landings'])->attribute(['class' => 'gui-folder']);

            // Лендинги->Ссылки
            $landings->add(
                $this->titleWrapper(Lang::get('menu.landing-links', [], 'ru')),
                route('landingControl.links', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.create.links.edit', 'landingControl.create.links.view']);

            // Лендинги->Создание
            Route::current();
            $landings_create = $landings->add(
                $this->titleWrapper(Lang::get('menu.landing-create', [], 'ru')),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->data('permissions', ['landing-create'])->attribute(['class' => 'gui-folder']);
            // Лендинги->Создание->Шаблоны
            $landings_create->add(
                $this->titleWrapper(Lang::get('menu.landing-create-templates', [], 'ru')),
                route('landingControl.templates', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.create.templates.edit', 'landingControl.create.templates.view']);
            // Лендинги->Создание->Формы
            $landings_create->add(
                $this->titleWrapper(Lang::get('menu.landing-create-forms', [], 'ru')),
                route('landingControl.landingForms', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.create.forms.edit', 'landingControl.create.forms.view']);
            // Лендинги->Создание->АБ-тест
            $landings_create->add(
                $this->titleWrapper(Lang::get('menu.landing-create-abTest', [], 'ru')),
                route('landingControl.abSplits', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.create.ab-test.edit', 'landingControl.create.ab-test.view']);
            // Лендинги->Создание->Гео-сплит
            $landings_create->add(
                $this->titleWrapper(Lang::get('menu.landing-create-geoSplit', [], 'ru')),
                route('landingControl.geoSplits', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.create.geo-split.edit', 'landingControl.create.geo-split.view']);
            // Лендинги->Создание->Файловый менеджер
            $landings_create->add(
                $this->titleWrapper(Lang::get('menu.landing-create-fileManager', [], 'ru')),
                route('landingControl.filemanager', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['file-manager']);
            // Лендинги->Создание->Домены
            $landings_create->add(
                $this->titleWrapper(Lang::get('menu.landing-create-domains', [], 'ru')),
                route('landingControl.domains', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.create.domains']);

            // Лендинги->Мультилендинг
            $landings_multylanding = $landings->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding', [], 'ru')),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->attribute(['class' => 'gui-folder']);
            // Лендинги->Мультилендинг->Товар-замены
            $landings_multylanding->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding-products', [], 'ru')),
                route('landingControl.replacementProducts', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.multylanding.product-replacement']);
            // Лендинги->Мультилендинг->Переменные
            $landings_multylanding->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding-params', [], 'ru')),
                route('landingControl.replacementParams', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.multylanding.variables']);
            // Лендинги->Мультилендинг->Time-замены
            $landings_multylanding->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding-replacementTime', [], 'ru')),
                route('landingControl.replacementTimes', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.multylanding.time-replacement']);
            // Лендинги->Мультилендинг->Geo-замены
            $landings_multylanding->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding-replacementGeo', [], 'ru')),
                route('landingControl.replacementGeo', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.multylanding.geo-replacement']);
            // Лендинги->Мультилендинг->Link-вставки
            $landings_multylanding->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding-insertLinks', [], 'ru')),
                route('landingControl.insertLinks', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.multylanding.link-insert']);
            // Лендинги->Мультилендинг->Link-замены
            $landings_multylanding->add(
                $this->titleWrapper(Lang::get('menu.landing-multylanding-replacementLinks', [], 'ru')),
                route('landingControl.replacementLinks', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['landingControl.multylanding.link-replacement']);


            // Аналитика
            $stat = $menu->add(
                $this->iconWrapper(Lang::get('menu.stat', [], 'ru'), 'md-equalizer'),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->data('permissions', ['stat'])->attribute(['class' => 'gui-folder']);
            // Аналитика->CRM
            $crm_stat = $stat->add(
                $this->titleWrapper(Lang::get('menu.crm', [], 'ru')),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->attribute(['class' => 'gui-folder']);
            // Аналитика->CRM->Отчет по сотрудникам
            $crm_stat->add(
                $this->titleWrapper(Lang::get('menu.stat-crm-reportByUsers', [], 'ru')),
                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
            )->data('permissions', ['crm.analytics.employee']);
            // Аналитика->CRM->Отчет по продажам
            $crm_stat->add(
                $this->titleWrapper(Lang::get('menu.stat-crm-saleReport', [], 'ru')),
                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
            )->data('permissions', ['crm.analytics.deals']);
            // Аналитика->Лендинги
//            $landings_stat = $stat->add(
//                $this->titleWrapper(Lang::get('menu.landings', [], 'ru')),
//                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
//            )->attribute(['class' => 'gui-folder'])
//                ->data('permissions', ['stat']);
            // Аналитика->Лендинги->Пункты
//            $landings_stat->add(
//                $this->titleWrapper(Lang::get('menu.stat-landings-stat', [], 'ru')),
//                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
//            )->data('permissions', ['stat']);


            // Настройки
            $settings = $menu->add(
                $this->iconWrapper(Lang::get('menu.settings', [], 'ru'), 'md-settings'),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->attribute(['class' => 'gui-folder'])
                ->data('permissions', ['crm.settings.menu']);

            // Настройки->Автоплатеж
            $settings->add(
                $this->titleWrapper(Lang::get('menu.settings-invoices', [], 'ru')),
                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
            )->data('permissions', ['crm.settings.menu']);
            // Настройки->Настройки компаний
            $settings->add(
                $this->titleWrapper(Lang::get('menu.settings-companySettings', [], 'ru')),
                route('saas.company.edit', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.settings.menu']);
            // Настройки->Сотрудники
            $settings->add(
                $this->titleWrapper(Lang::get('menu.users', [], 'ru')),
                route('saas.employees', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.employees.view.all', 'crm.employees.view.their']);
            // Настройки->Модули
            $settings->add(
                $this->titleWrapper(Lang::get('menu.modules', [], 'ru')),
                route('billing.modules', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.settings.modules']);
            // Настройки->Дополнительные поля
            $settings->add(
                $this->titleWrapper(Lang::get('menu.settings-additionalFields', [], 'ru')),
                route('crm.firms.fields', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.settings.fields']);
            // Настройки->Справочники
//            $settings->add(
//                $this->titleWrapper(Lang::get('menu.settings-references', [], 'ru')),
//                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
//            );
            // ---- Со страрого шаблона:
//            $settings->add(
//                $this->titleWrapper(Lang::get('menu.general', [], 'ru')),
//                ['company_alias' => object_get($company, 'name', 'toolslab')]
//            );
            $settings->add(
                $this->titleWrapper(Lang::get('menu.tags', [], 'ru')),
                route('crm.statuses', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.settings.menu']);
            $settings->add(
                $this->titleWrapper(Lang::get('menu.api', [], 'ru')),
                route('support.api', ['company_alias' => object_get($company, 'name', 'toolslab')])
            )->data('permissions', ['crm.settings.api']);

            // Помощь
            $help = $menu->add(
                $this->iconWrapper(Lang::get('menu.help', [], 'ru'), 'md-help'),
                route(Route::currentRouteName(), Route::current()->parameters()) . $params_string . '#'
            )->data('permissions', [])->attribute(['class' => 'gui-folder']);

            $help->add(Lang::get('menu.support', [], 'ru'), route('support'));
            $help->add(Lang::get('menu.docs', [], 'ru'), route('docs'));
            $help->add(
                Lang::get('menu.faq', [], 'ru'),
                route(Route::currentRouteName(), ['company_alias' => object_get($company, 'name', 'toolslab')]) . '#'
            );
        })->filter(function ($item) use ($user) {
            return Auth::check() &&
            (!$item->data('permissions')
                || AccessService::canOneOfPermissions($item->data('permissions'))) ? true : false;
        });

        $view->with('menu_general', $menu);
    }


    /**
     * @param $text
     * @param $iconClass
     * @return string
     */
    protected function iconWrapper($text, $iconClass)
    {
        return '<div class="gui-icon"><i class="md ' . $iconClass . '"></i></div>' . $this->titleWrapper($text);
    }

    /**
     * @param $text
     * @return string
     */
    protected function titleWrapper($text)
    {
        return '<span class="title">' . $text . '</span>';
    }

}
