<?php namespace Saas\Composers;

use Config;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\View\View;
use Saas\Models\User;
use Saas\Services\SocketService;

class SocketComposer
{
    /** @var Guard */
    protected $auth;
    /** @var  SocketService */
    protected $socketService;

    /**
     * @param Guard $guard
     * @param SocketService $socketService
     */
    public function __construct(Guard $guard, SocketService $socketService)
    {
        $this->auth = $guard;
        $this->socketService = $socketService;
    }


    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        /** @var User $user */
        if ($user = $this->auth->user()) {
            $view->with('socketKey', $this->socketService->generateSecHash($user));

        } else {
            $view->with('socketKey', false);
        }
        $view->with('socketServer', Config::get('websocket.server') . ':' . Config::get('websocket.port'));
    }

}
