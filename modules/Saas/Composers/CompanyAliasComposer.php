<?php namespace Saas\Composers;


use Illuminate\View\View;
use Saas\Services\StateApplicationService;


class CompanyAliasComposer
{
    /** @var  StateApplicationService */
    protected $stateAppService;

    public function __construct(StateApplicationService $applicationService)
    {
        $this->stateAppService = $applicationService;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with('currentCompany', $this->stateAppService->getCurrentCompany());
    }
}
