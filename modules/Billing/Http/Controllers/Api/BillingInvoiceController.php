<?php namespace Billing\Http\Controllers\Api;

use Billing\Repositories\Eloquent\BillingInvoiceEloquentRepository;
use Billing\Repositories\Interfaces\BillingInvoiceRepositoryInterface;
use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Response;


/**
 * Class BillingInvoiceController
 * @package Core\Http\Controllers
 */
class BillingInvoiceController extends Controller
{
    /** @var BillingInvoiceEloquentRepository  */
    protected $repository;

    public function __construct(BillingInvoiceRepositoryInterface $invoiceRepositoryInterface)
    {
        $this->repository = $invoiceRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Response::api(['invoices' => $this->repository->all()]);
    }
}
