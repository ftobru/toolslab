<?php namespace Billing\Http\Controllers\Api;

use Billing\Repositories\Eloquent\BillingPaymentEloquentRepository;
use Billing\Repositories\Interfaces\BillingPaymentRepositoryInterface;
use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Response;

class BillingPaymentController extends Controller
{
    /** @var  BillingPaymentEloquentRepository */
    protected $repository;

    public function __construct(BillingPaymentRepositoryInterface $paymentRepositoryInterface)
    {
        $this->repository = $paymentRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        Response::api(['payments' => $this->repository->all()]);
    }

}
