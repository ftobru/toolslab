<?php namespace Billing\Http\Controllers\Api;

use Billing\Repositories\Interfaces\BillingAccountRepositoryInterface;
use Core\Http\Requests;
use Core\Http\Controllers\Controller;

use Auth;
use Illuminate\Http\Request;
use Response;

class BillingAccountController extends Controller
{

    protected $repository;

    /**
     * @param BillingAccountRepositoryInterface $accountRepositoryInterface
     */
    public function __construct(BillingAccountRepositoryInterface $accountRepositoryInterface)
    {
        $this->repository = $accountRepositoryInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::user()) {
            return Response::api(['billingAccount' => Auth::user()->billingAccount()->first()]);
        }
        return Response::api(['billingAccount' => false]);
    }
}
