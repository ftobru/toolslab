<?php namespace Billing\Http\Controllers\App;

use App;
use Auth;
use Billing\Http\Controllers\Requests\PaymentTariffRequest;
use Billing\Models\BillingPayment;
use Billing\Repositories\Eloquent\BillingPaymentEloquentRepository;
use Billing\Repositories\Interfaces\BillingPaymentRepositoryInterface;
use Billing\Services\AccountService;
use Billing\Services\InvoiceService;
use Billing\Services\RateService;
use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Illuminate\Auth\Guard;
use Illuminate\Support\Str;
use Modules\Billing\Reference\BillingPaymentReference;
use Response;
use Saas\Services\StateApplicationService;

/**
 * Class ProfileController
 * @package Billing\Http\Controllers\App
 */
class ProfileController extends Controller
{
    /** @var  AccountService */
    protected $accountService;

    /** @var  RateService */
    protected $rateService;

    /** @var  InvoiceService */
    protected $invoiceService;

    /** @var  BillingPaymentEloquentRepository */
    protected $paymentRepository;

    /** @var  Guard */
    protected $auth;

    /**
     * @param AccountService $accountService
     * @param RateService $rateService
     * @param InvoiceService $invoiceService
     * @param BillingPaymentRepositoryInterface $paymentRepositoryInterface
     * @param Guard $auth
     */
    public function __construct(
        AccountService $accountService,
        RateService $rateService,
        InvoiceService $invoiceService,
        BillingPaymentRepositoryInterface $paymentRepositoryInterface,
        Guard $auth
    ) {
        $this->accountService = $accountService;
        $this->rateService = $rateService;
        $this->invoiceService = $invoiceService;
        $this->paymentRepository = $paymentRepositoryInterface;
        $this->auth = $auth;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {

        $currentBalance = $this->accountService->getCurrentBalance($userId = $this->auth->user()->id);
        $invoiceCurrentMonth = $this->invoiceService->summaryInvoicesByDate(
            Carbon::now()->startOfMonth(),
            Carbon::now()->endOfMonth(),
            $userId
        );

        $invoicesNoPayment = $this->invoiceService->getNoPaymentInvoices($userId);
        $invoicesPayment = $this->invoiceService->getPaymentInvoices($userId);

        $rates = $this->rateService->getRatesByUserId($userId);

        /** @var BillingPayment $transaction */
        $transaction = $this->paymentRepository->create([
            'status' => BillingPaymentReference::STATUS_NOT_CONFIRM,
            'type' => BillingPaymentReference::TYPE_NORMAL,
            'amount' => 0,
            'last_status_update_at' => Carbon::now(),
            'transaction_id' => Str::random()
        ]);
        $user = Auth::user();
        $transaction->user()->attach($user->id);

        if (App::environment() === 'local') {
            if ($amount = \Input::get('MNT_AMOUNT', false)) {
                $transaction->status = BillingPaymentReference::STATUS_CONFIRM;
                $transaction->amount = $amount;
                $transaction->last_status_update_at = Carbon::now();
                $transaction->save();
                $billingAccount = $user->billingAccount()->first();
                $billingAccount->amount = $amount;
                $billingAccount->save();
            }
        }
        return view('billing.profile.index', [
            'currentBalance' => $currentBalance,
            'invoiceCurrentMonth' => $invoiceCurrentMonth,
            'invoicesNoPayment' => $invoicesNoPayment,
            'invoicesPayment' => $invoicesPayment,
            'rates' => $rates,
            'transaction' => $transaction
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAjaxBalance()
    {
        return Response::json([
            'currentBalance' => $this->accountService->getCurrentBalance($this->auth->id())
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAjaxExpensesMonth()
    {
        return Response::json([
            'invoiceBalance' => $this->invoiceService->summaryInvoicesByDate(
                Carbon::now()->startOfMonth(),
                Carbon::now()->endOfMonth(),
                $this->auth->id()
            )
        ]);
    }

    /**
     * @param PaymentTariffRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Billing\Exceptions\MonthCanNotBeNegativeException
     */
    public function ajaxPaymentTariff(PaymentTariffRequest $request)
    {
        $billingRateUser = $this->rateService->extendRateMonth(
            $request->get('company_id'),
            $request->get('rate_id'),
            $request->get('month', 1)
        );

        return Response::json($billingRateUser);
    }
}
