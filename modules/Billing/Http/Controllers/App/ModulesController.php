<?php namespace Billing\Http\Controllers\App;

use App;
use Auth;
use Billing\Exceptions\InsufficientFundsException;
use Billing\Exceptions\RateNotFoundException;
use Billing\Http\Controllers\Requests\ConnectRateRequest;
use Billing\Reference\BillingAccountReference;
use Billing\Reference\BillingRatesUserReference;
use Billing\Services\Rates\CrmAddUserRate;
use Billing\Services\RateService;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\CountUsersRequest;
use Illuminate\Auth\Guard;
use Illuminate\Support\Collection;
use Response;
use Saas\Services\CompanyService;
use Saas\Services\StateApplicationService;
use Saas\Services\UserService;

/**
 * Class ModulesController
 * @package Billing\Http\Controllers\App
 */
class ModulesController extends Controller
{
    /** @var  RateService */
    protected $rateService;
    /** @var Guard */
    protected $auth;
    /** @var  StateApplicationService */
    protected $stateAppService;
    /** @var CompanyService  */
    protected $companyService;

    /**
     * @param RateService $rateService
     * @param Guard $guard
     * @param StateApplicationService $stateApplicationService
     * @param CompanyService $companyService
     * @internal param UserService $userService
     */
    public function __construct(
        RateService $rateService,
        Guard $guard,
        StateApplicationService $stateApplicationService,
        CompanyService $companyService
    ) {
        $this->rateService = $rateService;
        $this->auth = $guard;
        $this->stateAppService = $stateApplicationService;
        $this->companyService = $companyService;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getIndex()
    {
        $company = $this->stateAppService->getCurrentCompany();
        $companyId = $company->id;
        $currentConnectRates = $this->rateService->getRatesByCompanyId($companyId);
        $rateEntities = Collection::make($this->rateService->getManager()->rates())->map(function ($rate) {
            return App::make($rate);
        });

        $amount = Auth::user()->billingAccount()->first()->amount;

        $countAddUserRates = $this->rateService->getAddUserRates(/*$companyId*/)->count();
        return view('billing.modules.index', [
            'currentConnectRates' => $currentConnectRates,
            'rateEntities' => $rateEntities,
            'countUsers' => $countAddUserRates,
            'amount' => $amount,
            'company' => App::make('StateApplicationService')->getCurrentCompany()
        ]);
    }


    /**
     * @param CountUsersRequest $request
     * @return mixed
     * @throws InsufficientFundsException
     */
    public function postAjaxCrmAddUser(CountUsersRequest $request)
    {
        if (($users = $request->get('users')) > 0 ) {
            for($i = 0; $i < $users; ++$i) {
                $this->rateService->connectRate(
                    Auth::user()->id,
                    $this->stateAppService->getCurrentCompany()->id,
                    CrmAddUserRate::IDENTIFIER
                );
            }
        }
        return $users;
    }

    public function postAjaxConnectLandingPro()
    {

    }

    public function postAjaxConnectAjaxDisk()
    {

    }

}
