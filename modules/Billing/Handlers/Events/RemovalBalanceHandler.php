<?php

namespace Billing\Handlers\Events;

use Billing\Models\BillingInvoice;

use Billing\Services\AccountService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Log;
use Saas\Exceptions\UserNotFoundException;

class RemovalBalanceHandler implements ShouldQueue
{
    use InteractsWithQueue;

    /** @var AccountService  */
    protected $accountService;

    /**
     * Create the event handler.
     *
     * @param AccountService $accountService
     */
    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * @param BillingInvoice $event
     * @throws UserNotFoundException
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     */
    public function handle(BillingInvoice $event)
    {
        Log::useDailyFiles(storage_path() . '/logs/billing_removal.log');
        if ($user = $event->user()->first()) {
            $this->accountService->removal($event->amount, $user->id);
        }

        Log::info(
            object_get($event->user(), 'id', 'Error (Invoice #' . $event->id . ')') . ' ---- ' . $event->amount
        );
    }
}