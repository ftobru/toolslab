<?php namespace Billing\Handlers\Events;

use Billing\Models\BillingRatesUser;
use Billing\Reference\BillingRatesUserReference;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Log;

/**
 * Class ActiveRateHandler
 * @package Billing\Handlers\Events
 */
class ActiveRateHandler implements ShouldQueue
{
    use InteractsWithQueue;

    public function handler(BillingRatesUser $billingRatesUser)
    {
        Log::useDailyFiles(storage_path() . '/logs/billing_removal.log');

        $billingRatesUser->status = BillingRatesUserReference::STATUS_ACTIVE;
        $billingRatesUser->save();

        \Event::fire('Billing\Events\ActivateRateEvent', $billingRatesUser);
    }
}
