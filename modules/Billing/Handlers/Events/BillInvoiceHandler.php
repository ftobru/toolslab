<?php

namespace Billing\Handlers\Events;

use Billing\Models\BillingRatesUser;
use Billing\Services\InvoiceService;

use Billing\Services\RateService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

/**
 * Class BillInvoiceHandler
 * @package Core\Handlers\Events
 */
class BillInvoiceHandler implements ShouldQueue
{

    use InteractsWithQueue;

    /** @var  InvoiceService */
    protected $serviceInvoice;

    /** @var  RateService */
    protected $rateService;

    /**
     * @param InvoiceService $invoiceService
     * @param RateService $rateService
     */
    public function __construct(InvoiceService $invoiceService, RateService $rateService)
    {
        $this->serviceInvoice = $invoiceService;
        $this->rateService = $rateService;
    }

    /**
     * Handle the event.
     *
     * @param  BillingRatesUser  $event
     * @return void
     */
    public function handler(BillingRatesUser $event)
    {
        $price = $this->rateService->getRateByIdentifier($event->rate_ident)->getPrice();
        $this->serviceInvoice->bill($event, $price);
    }
}
