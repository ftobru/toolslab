<?php namespace Billing\Handlers\Events;


use Billing\Models\BillingRatesUser;
use Billing\Models\Criteria\GroupIdCriteria;
use Billing\Repositories\Eloquent\Criteria\NoIdCriteria;
use Billing\Repositories\Eloquent\RateEloquentRepository;
use Billing\Repositories\Interfaces\RateRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RemovePreRateHandler implements ShouldQueue
{
    use InteractsWithQueue;

    /** @var RateEloquentRepository  */
    protected $rateRepository;

    /**
     * @param RateRepositoryInterface $repositoryInterface
     */
    protected function __construct(RateRepositoryInterface $repositoryInterface)
    {
        $this->rateRepository = $repositoryInterface;
    }

    /**
     * @param BillingRatesUser $billingRatesUser
     */
    public function handler(BillingRatesUser $billingRatesUser)
    {
        $rates = $this->rateRepository->pushCriteria(new GroupIdCriteria($billingRatesUser->group_id))
            ->pushCriteria(new NoIdCriteria($billingRatesUser->id))->all();
        if ($rates->count() > 0) {
            $rates->each(function ($rate) {
                /** @var BillingRatesUser $rate */
                $rate->delete();
            });
            unset($rates);
        }
    }
}
