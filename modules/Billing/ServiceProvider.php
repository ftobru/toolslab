<?php namespace Billing;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package Modules\Billing
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Repositories
        $this->app->bind(
            'Billing\Repositories\Interfaces\BillingAccountRepositoryInterface',
            'Billing\Repositories\Eloquent\BillingAccountEloquentRepository'
        );

        $this->app->bind(
            'Billing\Repositories\Interfaces\BillingPaymentRepositoryInterface',
            'Billing\Repositories\Eloquent\BillingPaymentEloquentRepository'
        );

        $this->app->bind(
            'Billing\Repositories\Interfaces\BillingInvoiceRepositoryInterface',
            'Billing\Repositories\Eloquent\BillingInvoiceEloquentRepository'
        );

        $this->app->bind(
            'Billing\Repositories\Interfaces\RateRepositoryInterface',
            'Billing\Repositories\Eloquent\RateEloquentRepository'
        );

        // Services
        $this->app->bind('AccountService', 'Billing\Services\AccountService');

        $this->app->bind('CollectorService', 'Billing\Services\CollectorService');

        $this->app->bind('RateService', 'Billing\Services\RateService');

        $this->app->bind('RateManager', 'Billing\Services\Rates\RateManager');

        $this->app->bind('BillingControl', 'Billing\Services\BillingControlService');
        $this->app->bind('InvoiceService', 'Billing\Services\InvoiceService');

    }
}