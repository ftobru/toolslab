<?php

namespace Billing\Events;

use Core\Events\Event;

use Illuminate\Queue\SerializesModels;

class InvoicePayEvent extends Event
{

    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

}
