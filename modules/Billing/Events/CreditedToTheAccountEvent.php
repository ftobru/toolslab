<?php namespace Billing\Events;

use Event;

/**
 * Событие при пополнение баланса
 * Class CreditedToTheAccountEvent
 * @package Billing\Events
 */
class CreditedToTheAccountEvent extends Event
{
    public function __construct()
    {
        //
    }
}