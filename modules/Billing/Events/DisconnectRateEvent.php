<?php namespace Billing\Events;

use Event;

/**
 * Отключение тарифа
 * Class DisconnectRateEvent
 * @package Billing\Events
 */
class DisconnectRateEvent extends Event
{
    public function __construct()
    {

    }
}