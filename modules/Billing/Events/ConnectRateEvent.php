<?php namespace Billing\Events;

use Core\Events\Event;

use Illuminate\Queue\SerializesModels;

class ConnectRateEvent extends Event
{

    use SerializesModels;

    /**
     * Create a new event instance.
     *
     */
    public function __construct()
    {
        //
    }
}
