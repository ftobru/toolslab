<?php namespace Billing\Exceptions;

use Exception;

class AmountCanNotBeNegativeException extends Exception
{
    //
}
