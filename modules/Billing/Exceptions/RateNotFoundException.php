<?php namespace Billing\Exceptions;

use Exception;

class RateNotFoundException extends Exception
{
}
