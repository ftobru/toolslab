<?php namespace Billing\Repositories\Eloquent;

use Billing\Models\Criteria\DateCriteria;
use Billing\Repositories\Eloquent\Criteria\StatusCriteria;
use Billing\Repositories\Eloquent\Criteria\UserIdCriteria;
use Billing\Repositories\Interfaces\BillingInvoiceRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BillingInvoiceEloquentRepository
 * @package Billing\Repositories\Eloquent
 */
class BillingInvoiceEloquentRepository extends Repository implements BillingInvoiceRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Billing\Models\BillingInvoice';
    }


    /**
     * @param $status
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getByStatusAndUserId($status, $userId)
    {
        return $this->resetScope()->skipCriteria()->pushCriteria(new StatusCriteria($status))
            ->pushCriteria(new UserIdCriteria($userId))->all();
    }

    /**
     * @param $startDate
     * @param $finishDate
     * @param $status
     * @param $userId
     * @return Collection
     */
    public function getByDateAndStatusAndUserId($startDate, $finishDate, $status, $userId)
    {
        return $this->resetScope()->pushCriteria(new DateCriteria($startDate, $finishDate))
            ->pushCriteria(new StatusCriteria($status))
            ->pushCriteria(new UserIdCriteria($userId))
            ->all();
    }


}
