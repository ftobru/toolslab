<?php namespace Billing\Repositories\Eloquent;

use Billing\Repositories\Eloquent\Criteria\UserIdCriteria;
use Billing\Repositories\Interfaces\BillingAccountRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class BillingEloquentRepository
 * @package Core\Repository\Eloquent
 */
class BillingAccountEloquentRepository extends Repository implements BillingAccountRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Billing\Models\BillingAccount';
    }

    public function getByUserId($userId)
    {
        return $this->getByCriteria(new UserIdCriteria($userId))->first();
    }


}
