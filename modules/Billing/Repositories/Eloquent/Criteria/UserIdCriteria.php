<?php namespace Billing\Repositories\Eloquent\Criteria;

use Doctrine\DBAL\Query\QueryBuilder;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UserIdCriteria
 * @package Billing\Repositories\Eloquent\Criteria
 */
class UserIdCriteria extends Criteria
{
    protected $userId;

    /**
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->whereHas('user', function ($q) {
            $q->where('id', $this->userId);
        });
    }
}