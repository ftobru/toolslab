<?php namespace Billing\Repositories\Eloquent\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class SumAmountSelectCriteria
 * @package Billing\Repositories\Eloquent\Criteria
 */
class SelectSumAmountCriteria extends Criteria
{
    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getQuery()->selectRaw('SUM(amount) as amount');
    }
}