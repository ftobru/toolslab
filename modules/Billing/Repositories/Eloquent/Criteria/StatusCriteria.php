<?php namespace Billing\Repositories\Eloquent\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class StatusCriteria extends Criteria
{
    /**
     * @var int
     */
    protected $status;

    /**
     * @param $status
     */
    public function __construct($status)
    {
        $this->$status = $status;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('status', '=', $this->status);
    }

}
