<?php namespace Billing\Repositories\Eloquent;

use Billing\Repositories\Eloquent\Criteria\StatusCriteria;
use Billing\Repositories\Eloquent\Criteria\UserIdCriteria;
use Billing\Repositories\Interfaces\BillingPaymentRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class BillingPaymentEloquentRepository
 * @package Core\Repositories\Eloquent
 */
class BillingPaymentEloquentRepository extends Repository implements BillingPaymentRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Billing\Models\BillingPayment';
    }

    /**
     * @param $status
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getByStatusAndUserId($status, $userId)
    {
        return $this->pushCriteria(new StatusCriteria($status))
            ->pushCriteria(new UserIdCriteria($userId))->all();
    }
}