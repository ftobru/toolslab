<?php namespace Billing\Repositories\Eloquent;


use Billing\Models\Criteria\GroupIdCriteria;
use Billing\Models\Criteria\RateIdentCriteria;
use Billing\Repositories\Eloquent\Criteria\UserIdTableCriteria;
use Billing\Repositories\Interfaces\RateRepositoryInterface;
use Crm\Repositories\Eloquent\Criteria\Task\CompanyIdCriteria;
use Ftob\Repositories\Eloquent\Repository;

class RateEloquentRepository extends Repository implements RateRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Billing\Models\BillingRatesUser';
    }

    /**
     * @param $companyId
     * @param $rateIdent
     * @return mixed
     */
    public function getByCompanyIdAndRateIdent($companyId, $rateIdent)
    {
        return $this->pushCriteria(new RateIdentCriteria($rateIdent))
            ->pushCriteria(new CompanyIdCriteria($companyId))->first();
    }

    /**
     * @param $groupId
     * @return mixed
     */
    public function removeByGroupId($groupId)
    {
        return $this->pushCriteria(new GroupIdCriteria($groupId))->destroy();
    }

    /**
     * @param $groupId
     * @return \Illuminate\Support\Collection
     */
    public function hasByGroupId($groupId)
    {
        return $this->pushCriteria(new GroupIdCriteria($groupId))->first(['id']);
    }

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getByUserId($userId)
    {
        return $this->pushCriteria(new UserIdTableCriteria($userId))->all();
    }
}