<?php namespace Billing\Repositories\Interfaces;

/**
 * Interface BillingAccountRepositoryInterface
 * @package Modules\Billing\Repositories\Interfaces
 */
interface BillingAccountRepositoryInterface
{
    public function getByUserId($userId);
}
