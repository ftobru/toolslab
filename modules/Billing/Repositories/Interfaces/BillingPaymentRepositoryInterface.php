<?php namespace Billing\Repositories\Interfaces;

/**
 * Interface BillingPaymentRepositoryInterface
 * @package Modules\Billing\Repositories\Interfaces
 */
interface BillingPaymentRepositoryInterface
{
    public function getByStatusAndUserId($status, $userId);
}
