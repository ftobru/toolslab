<?php namespace Billing\Models;

use App;
use Billing\Services\RateService;
use Carbon\Carbon;
use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\Model;
use Saas\Traits\CompaniesName;

/**
 * Class BillingRatesUser
 * @package Billing\Models
 * @property $rate_ident
 * @property $user_id
 * @property Carbon $date_start
 * @property Carbon $date_end
 * @property $company_id
 * @property integer $status
 */
class BillingRatesUser extends CoreModel
{

    /**
     * @var string
     */
    protected $table = 'billing_rates_user';

    protected $company = true;

    /**
     * @var array
     */
    protected $fillable = ['rate_ident', 'user_id', 'company_id', 'group_id', 'date_start', 'date_end', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('Saas\Models\User');
    }

    /**
     * @return \Billing\Services\Rates\Interfaces\RateInterface
     * @throws \Billing\Exceptions\RateNotFoundException
     */
    public function rate()
    {
        /** @var RateService $rateService */
        $rateService = App::make('RateService');
        return $rateService->getRateByIdentifier($this->rate_ident);
    }

    /**
     * @param $value
     * @return static
     */
    public function getDateStartAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value));
    }

    /**
     * @param $value
     * @return static
     */
    public function getDateEndAttribute($value)
    {
        return Carbon::createFromTimestamp(strtotime($value));
    }
}
