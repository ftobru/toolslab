<?php namespace Billing\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillingPayment
 * @package Billing\Models
 * @property $amount
 * @property $type
 * @property $status
 */
class BillingPayment extends Model
{
    /**
     * @var string
     */
    protected $table = 'billing_payments';

    /**
     * @var array
     */
    protected $fillable = ['transaction_id', 'status', 'type', 'amount'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('Saas\Models\User')->withPivot('user_id', 'billing_payment_id');
    }
}
