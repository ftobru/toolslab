<?php

namespace Billing\Models;

use App;
use Billing\Services\RateService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class BillingInvoice
 * @package Billing\Models
 * @property $status
 * @property $type
 * @property $amount
 */
class BillingInvoice extends Model
{
    /**
     * @var string
     */
    protected $table = 'billing_invoices';

    /**
     * @var array
     */
    protected $fillable = ['status', 'type', 'amount', 'company_id', 'rate_id'];

    /**
     * @return BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('Saas\Models\User')->withPivot('user_id', 'billing_invoice_id');
    }

    /**
     * @return \Billing\Services\Rates\Interfaces\RateInterface
     * @throws \Billing\Exceptions\RateNotFoundException
     */
    public function rate()
    {
        /** @var RateService $rateService */
        $rateService = App::make('RateService');
        return $rateService->getRateByIdentifier($this->rate_id);
    }
}
