<?php namespace Billing\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillingAccount
 * @package App
 * @property $amount
 */
class BillingAccount extends Model
{

    protected $table = 'billing_accounts';

    protected $fillable = ['amount', 'count_operations', 'status', 'type'];

    /**
     * Один к одному, мы же не банковскую систему проектируем ;)
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsToMany('Saas\Models\User')->withPivot('billing_account_id', 'user_id');
    }

}
