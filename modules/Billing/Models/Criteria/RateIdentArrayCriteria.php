<?php namespace Billing\Models\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class RateIdentArrayCriteria extends Criteria
{
    protected $rateIdentsArray;

    public function __construct(array $rateIdents)
    {
        $this->rateIdentsArray = $rateIdents;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->whereIn('rate_ident', $this->rateIdentsArray);
    }
}
