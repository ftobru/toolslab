<?php namespace Billing\Models\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class GroupIdCriteria
 * @package Billing\Models\Criteria
 */
class GroupIdCriteria extends Criteria
{
    protected $groupId;

    /**
     * @param $groupId
     */
    public function __construct($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('group_id', '=', $this->groupId);
    }
}
