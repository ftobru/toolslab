<?php namespace Billing\Models\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class RateIdentCriteria extends Criteria
{
    protected $rateIdent;

    public function __construct($rateIdent)
    {
        $this->rateIdent = $rateIdent;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('rate_ident', '=', $this->rateIdent);
    }


}
