<?php namespace Billing\Models\Criteria;


use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class DateCriteria extends  Criteria
{
    protected $startDate;

    protected $finishDate;

    public function __construct($startDate, $finishDate)
    {
        $this->startDate = $startDate;
        $this->finishDate = $finishDate;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->whereBetween('updated_at', [$this->startDate, $this->finishDate]);
    }


}
