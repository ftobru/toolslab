<?php namespace Billing\Services\ControlCallbacks;

use Billing\Services\ControlCallbacks\Interfaces\ControlCallbackInterface;
use Billing\Services\ControlCallbacks\Interfaces\CreatingInterface;
use Billing\Services\InvoiceService;
use Billing\Services\RateService;
use Saas\Exceptions\CompanyNotFoundException;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;

/**
 * Class CompanyCallback
 * @package Billing\Services\ControlCallbacks
 */
class CompanyCallback implements CreatingInterface, ControlCallbackInterface
{
    /** @var RateService  */
    protected $rateService;
    /** @var  CompanyEloquentRepository */
    protected $companyRepository;

    /** @var  InvoiceService */
    protected $invoiceService;

    /**
     * @param RateService $rateService
     * @param CompanyRepositoryInterface $companyRepositoryInterface
     * @param InvoiceService $invoiceService
     */
    public function __construct(
        RateService $rateService,
        CompanyRepositoryInterface $companyRepositoryInterface,
        InvoiceService $invoiceService
    ) {
        $this->rateService = $rateService;
        $this->companyRepository = $companyRepositoryInterface;
        $this->invoiceService = $invoiceService;
    }

    /**
     * @return string
     */
    public static function getModel()
    {
        return 'Saas\Models\Company';
    }

    /**
     * @param int $entityId
     * @return bool
     * @throws CompanyNotFoundException
     * @throws \Billing\Exceptions\RateNotFoundException
     */
    public function creating($entityId)
    {
        $company = $this->companyRepository->find($entityId);
        if (!$company) {
            throw new CompanyNotFoundException;
        }
        // Выставляем счет в случае наличия free периода
        $bill = $this->rateService->checkTheFreePeriod($company->user_id);
        return $this->rateService->connectDefaultRates($company->user_id, $entityId, $bill);
    }
}
