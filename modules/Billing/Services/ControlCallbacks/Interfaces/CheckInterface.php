<?php namespace Billing\Services\ControlCallbacks\Interfaces;

/**
 * Interface CheckInterface
 * @package Billing\Services\ControlCallbacks\Interfaces
 */
interface CheckInterface
{
    /**
     * @param integer $rateIdentifier
     * @param $company_id
     * @return mixed
     */
    public function check($rateIdentifier, $company_id);
}

