<?php namespace Billing\Services\ControlCallbacks\Interfaces;

/**
 * Interface UpdatingInterface
 * @package Billing\Services\ControlCallbacks\Interfaces
 */
interface UpdatingInterface
{
    /**
     * @param integer $companyId
     * @return mixed
     */
    public function updating($companyId);
}