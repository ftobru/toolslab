<?php namespace Billing\Services\ControlCallbacks\Interfaces;

/**
 * Interface CreatingInterface
 * @package Billing\Services\ControlCallbacks\Interfaces
 */
interface CreatingInterface
{
    /**
     * @param int $entityId
     * @return mixed
     */
    public function creating($entityId);
}

