<?php namespace Billing\Services\ControlCallbacks\Interfaces;

/**
 * Interface ControlCallbackInterface
 * @package Billing\Services\ControlCallbacks\Interfaces
 */
interface ControlCallbackInterface
{
    /**
     * @return string
     */
    public static function getModel();
}
