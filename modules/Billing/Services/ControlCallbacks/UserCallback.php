<?php namespace Billing\Services\ControlCallbacks;

use App;
use Billing\Exceptions\InsufficientFundsException;
use Billing\Models\BillingRatesUser;
use Billing\Services\ControlCallbacks\Interfaces\CheckInterface;
use Billing\Services\ControlCallbacks\Interfaces\ControlCallbackInterface;
use Billing\Services\ControlCallbacks\Interfaces\CreatingInterface;
use Billing\Services\Rates\CrmAddUserRate;
use Billing\Services\Rates\CrmUsersLiteRate;
use Billing\Services\RateService;
use Saas\Exceptions\CompanyNotFoundException;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;
use Saas\Services\UserService;

/**
 * Class UserCallback
 * @package Billing\Services\ControlCallbacks
 */
class UserCallback implements CreatingInterface, ControlCallbackInterface, CheckInterface
{
    /** @var RateService  */
    protected $rateService;
    /** @var UserService */
    protected $userService;
    /** @var CompanyRepositoryInterface */
    private $companyRepository;

    /**
     * @param RateService $rateService
     * @param UserService $userService
     * @param CompanyRepositoryInterface $companyRepository
     */
    public function __construct(
        RateService $rateService,
        UserService $userService,
        CompanyRepositoryInterface $companyRepository
    ) {
        $this->rateService = $rateService;
        $this->userService = $userService;
        $this->companyRepository = $companyRepository;
    }

    /**
     * @return string
     */
    public static function getModel()
    {
        return 'Saas\Models\User';
    }


    /**
     * @param integer $entityId
     * @return mixed
     * @throws CompanyNotFoundException
     * @throws InsufficientFundsException
     */
    public function creating($entityId)
    {
        $company = $this->companyRepository->find($entityId);
        if (!$company) {
            throw new CompanyNotFoundException;
        }
        $checkResult = $this->check(CrmAddUserRate::IDENTIFIER, $entityId);
        if ($checkResult['result']) {
            throw new InsufficientFundsException;
        }
    }

    /**
     * @param integer $rateIdentifier
     * @param $company_id
     * @return mixed
     */
    public function check($rateIdentifier, $company_id)
    {
        if ($rateIdentifier === CrmAddUserRate::IDENTIFIER) {
            $enabledUsersCount = 0;
            $this->rateService
                ->getAllAddUserAndLiteUserRates()->each(function ($rate) use (&$enabledUsersCount) {
                    /* @var BillingRatesUser $rate */
                    $enabledUsersCount += $rate->rate()->getValue();
                });
            $currentUsersCount = $this->userService->getAllUsersCountForCompany($company_id);
            if ($currentUsersCount < $enabledUsersCount) {
                return [
                    'result' => true,
                    'currentUsers' => $currentUsersCount,
                    'enabledUsers' => $enabledUsersCount
                ];
            }
            return [
                'result' => false,
                'currentUsers' => $currentUsersCount,
                'enabledUsers' => $enabledUsersCount
            ];
        }
        return [
            'result' => false
        ];
    }
}
