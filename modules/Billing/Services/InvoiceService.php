<?php namespace Billing\Services;

use Billing\Models\BillingInvoice;
use Billing\Models\BillingRatesUser;
use Billing\Reference\BillingInvoiceReference;
use Billing\Repositories\Eloquent\BillingInvoiceEloquentRepository;
use Billing\Repositories\Interfaces\BillingInvoiceRepositoryInterface;
use Event;
use Illuminate\Events\Dispatcher;

class InvoiceService
{
    /** @var BillingInvoiceEloquentRepository  */
    protected $invoiceRepository;

    /** @var  Event */
    protected $event;

    /**
     * @param BillingInvoiceRepositoryInterface $invoiceRepositoryInterface
     * @param Dispatcher $event
     */
    public function __construct(
        BillingInvoiceRepositoryInterface $invoiceRepositoryInterface,
        Dispatcher $event
    ) {
        $this->invoiceRepository = $invoiceRepositoryInterface;
        $this->event = $event;
    }

    /**
     * @param BillingRatesUser $billingRatesUser
     * @param $amount
     * @return BillingInvoice
     */
    public function bill(BillingRatesUser $billingRatesUser, $amount)
    {
        $invoice = [
            'status' => BillingInvoiceReference::STATUS_NO_PAY,
            'type' => BillingInvoiceReference::TYPE_NORMAL,
            'company_id' => $billingRatesUser->company_id,
            'rate_id' => $billingRatesUser->id,
            'amount' => $amount
        ];

        /** @var BillingInvoice $invoiceModel */
        $invoiceModel = $this->invoiceRepository->create($invoice);

        $invoiceModel->user()->attach($billingRatesUser->user_id);

        return $invoiceModel;
    }

    /**
     * @param BillingInvoice $billingInvoice
     * @return BillingInvoice
     */
    public function pay(BillingInvoice $billingInvoice)
    {
        $billingInvoice->status = BillingInvoiceReference::STATUS_PAY;
        $billingInvoice->save();

        // Запускаем мобытие подсчета баланса
        $this->event->fire('Billing\Events\InvoicePayEvent', $billingInvoice);
        return $billingInvoice;
    }

    /**
     * @param $startDate
     * @param $finishDate
     * @param $userId
     * @param int $status
     * @return mixed
     */
    public function summaryInvoicesByDate(
        $startDate,
        $finishDate,
        $userId,
        $status = BillingInvoiceReference::STATUS_PAY
    ) {
        return $this->invoiceRepository
            ->getByDateAndStatusAndUserId($startDate, $finishDate, $status, $userId)->sum('amount');

    }

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getNoPaymentInvoices($userId)
    {
        return $this->invoiceRepository->getByStatusAndUserId(BillingInvoiceReference::STATUS_NO_PAY, $userId);
    }

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getPaymentInvoices($userId)
    {
        return $this->invoiceRepository->getByStatusAndUserId(BillingInvoiceReference::STATUS_PAY, $userId);
    }
}
