<?php namespace Billing\Services;

use App;
use Billing\Services\ControlCallbacks\CompanyCallback;
use Billing\Services\ControlCallbacks\Interfaces\CreatingInterface;
use Billing\Services\ControlCallbacks\Interfaces\UpdatingInterface;
use Billing\Services\ControlCallbacks\UserCallback;
use DB;
use Exception;
use Ftob\Repositories\Eloquent\Repository;
use Illuminate\Support\Collection;

/**
 * Class ControlRepositoryService
 * @package Billing\Services
 */
class ControlRepositoryService
{
    /** @var Repository  */
    protected $repository;


    /**
     * Передаем сиды репозиторий для его инкапсуляции
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     *
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function create(array $data)
    {
        try {
            DB::beginTransaction();
            $entity = $this->repository->create($data);
            if ($callback = $this->getCreatingCallback($this->repository->model())) {
                $callback->creating($entity->id);
            }
            DB::commit();
            return $entity;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }

    }

    /**
     * @param $rateIdentifier
     * @param $company_id
     * @return mixed
     */
    public function check($rateIdentifier, $company_id)
    {
        if ($callback = $this->getCreatingCallback($this->repository->model())) {
            return $callback->check($rateIdentifier, $company_id);
        }
    }

    /**
     * @param array $data
     * @param $id
     * @return mixed
     * @throws Exception
     */
    public function update(array $data, $id)
    {
        try {
            DB::beginTransaction();
            $entity = $this->repository->update($data, $id);
            if ($callback = $this->getUpdatingCallback($this->repository->model())) {
                $callback->updating($entity->id);
            }
            DB::commit();
            return $entity;
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

    /**
     * @param $model
     * @return CreatingInterface|bool
     */
    private function getCreatingCallback($model)
    {
        if (($callback = $this->getCallbackByModel($model)) instanceof CreatingInterface) {
            return $callback;
        } else {
            return false;
        }
    }

    /**
     * @param $model
     * @return bool|UpdatingInterface
     */
    private function getUpdatingCallback($model)
    {
        if (($callback = $this->getCallbackByModel($model)) instanceof UpdatingInterface) {
            return $callback;
        } else {
            return false;
        }
    }

    /**
     * @param $model
     * @return mixed
     */
    private function getCallbackByModel($model)
    {
        if ($callback = $this->callbacks()->get($model)) {
            return App::make($callback);
        } else {
            return $callback;
        }
    }

    /**
     * @return Collection
     */
    private function callbacks()
    {
        return new Collection([
            CompanyCallback::getModel() => CompanyCallback::class,
            UserCallback::getModel() => UserCallback::class
        ]);
    }


}
