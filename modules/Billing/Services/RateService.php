<?php namespace Billing\Services;

use App;
use Billing\Exceptions\InsufficientFundsException;
use Billing\Exceptions\MonthCanNotBeNegativeException;
use Billing\Exceptions\RateIsAlreadyConnectedException;
use Billing\Exceptions\RateNotFoundException;
use Billing\Models\BillingRatesUser;
use Billing\Models\Criteria\RateIdentArrayCriteria;
use Billing\Models\Criteria\RateIdentCriteria;
use Billing\Models\Criteria\StatusCriteria;
use Billing\Reference\BillingRatesUserReference;
use Billing\Repositories\Eloquent\RateEloquentRepository;
use Billing\Repositories\Interfaces\RateRepositoryInterface;
use Billing\Services\Rates\CrmAddUserRate;
use Billing\Services\Rates\CrmUsersLiteRate;
use Billing\Services\Rates\Interfaces\RateInterface;
use Billing\Services\Rates\Interfaces\RepeatInterface;
use Billing\Services\Rates\Interfaces\WatchInterface;
use Billing\Services\Rates\RateManager;
use Cache;
use Carbon\Carbon;
use Crm\Repositories\Eloquent\Criteria\Client\CompanyIdCriteria;
use DB;
use Ftob\Repositories\Criteria\CriteriaCollection;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Collection;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;

/**
 * Считаем кол-во денег который должен платить клиент
 * Class RateService
 * @package Billing\Services
 */
class RateService
{

    private $cachePriceDefaultKey = 'rates.default.price';

    /** @var RateManager  */
    protected $manager;
    /** @var RateEloquentRepository  */
    protected $rateRepository;

    /** @var  Dispatcher */
    protected $event;

    /**
     * @param RateRepositoryInterface $repositoryInterface
     * @param RateManager $rateManager
     * @param Dispatcher $dispatcher
     */
    public function __construct(
        RateRepositoryInterface $repositoryInterface,
        RateManager $rateManager,
        Dispatcher $dispatcher
    ) {
        $this->rateRepository = $repositoryInterface;
        $this->manager = $rateManager;
        $this->event = $dispatcher;
    }

    /**
     * @param $identifier
     * @return bool
     */
    public function checkRateIdentifier($identifier)
    {
        return boolval(array_get($this->manager->rates(), $identifier, null));
    }

    /**
     * @return RateManager
     */
    public function getManager()
    {
        return $this->manager;
    }


    /**
     * @param $identifier
     * @return RateInterface
     * @throws RateNotFoundException
     */
    public function getRateByIdentifier($identifier)
    {
        if ($this->checkRateIdentifier($identifier)) {
            return App::make($rate = array_get($this->manager->rates(), $identifier));
        } else {
            throw new RateNotFoundException;
        }
    }

    /**
     * @param $userId
     * @param $companyId
     * @param $rateId
     * @return mixed
     * @throws InsufficientFundsException
     * @throws RateIsAlreadyConnectedException
     * @throws RateNotFoundException
     * @throws \Billing\Exceptions\AccountNotFoundException
     */
    public function connectRate($userId, $companyId, $rateId)
    {
        /** @var AccountService $accountService */
        $accountService = App::make('AccountService');
        $rateEntity = $this->getRateByIdentifier($rateId);

        if (!$accountService->checkBalance($rateEntity->getPrice(), $userId)) {
            throw new InsufficientFundsException('Не достаточно средст', 501);
        }
        $rate = $this->connectDefaultRate($userId, $companyId, $rateId, $rateEntity->getDays());
        $this->event->fire('Billing\Events\ConnectRateEvent', $rate);

        return $rate;
    }

    /**
     * @param $companyId
     * @param $rateId
     * @return bool|null
     * @throws RateNotFoundException
     * @throws \Exception
     */
    public function disconnectRate($companyId, $rateId)
    {
        /** @var BillingRatesUser $rate */
        $rate = $this->rateRepository->getByCompanyIdAndRateIdent($companyId, $rateId);

        if (!$rate) {
            throw new RateNotFoundException;
        }
        return $rate->delete();
    }

    /**
     * @param $price
     * @param $userId
     * @return mixed
     */
    public function checkBalance($price, $userId)
    {
        return App::make('AccountService')->checkBalance($price, $userId);

    }

    /**
     * @param $userId
     * @param $companyId
     * @param $rateId
     * @param int $days
     * @return mixed
     * @throws RateIsAlreadyConnectedException
     * @throws RateNotFoundException
     */
    public function connectDefaultRate($userId, $companyId, $rateId, $days = 17)
    {
        if (!$this->checkRateIdentifier($rateId)) {
            throw new RateNotFoundException;
        }
        if ($this->checkConnectionRate($companyId, $rateId)) {
            throw new RateIsAlreadyConnectedException;
        }

        if ($this->checkConnectedRateOfGroup($groupId = $this->getGroupIdentifierByRateIdentifier($rateId))) {
            $this->rateRepository->removeByGroupId($groupId);
        }

        return $this->rateRepository->create([
            'user_id' => $userId,
            'company_id' => $companyId,
            'rate_ident' => $rateId,
            'group_id' => $groupId,
            'date_start' => Carbon::now(),
            'date_end' => Carbon::now()->addDays($days)
        ]);

    }

    /**
     * Получаем идентификатор группы в которой состоит тариф
     * @param $rateIdentifier
     * @return integer
     */
    public function getGroupIdentifierByRateIdentifier($rateIdentifier)
    {
        return head(
            $this->manager->groupsRates()->filter(function ($group) use ($rateIdentifier) {
                return in_array($rateIdentifier, $group);
            })->keys()->toArray()
        );
    }

    public function checkConnectedRateOfGroup($groupIdentifier)
    {
        return boolval($this->rateRepository->hasByGroupId($groupIdentifier));
    }

    /**
     * @param $userId
     * @return bool
     */
    public function getSummaryOfAllRatesByUserId($userId)
    {
        return $this->getSummaryOfRatesByAttribute('user_id', $userId);
    }

    /**
     * @param $companyId
     * @return bool|int
     */
    public function getSummaryOfAllRatesByCompanyId($companyId)
    {
        return $this->getSummaryOfRatesByAttribute('company_id', $companyId);
    }

    /**
     * Получить сумму по тарифам (по умолчанию)
     * @return int
     */
    public function getSummaryOfDefaultRates()
    {
        if (Cache::has($this->cachePriceDefaultKey)) {
            return floatval(Cache::get($this->cachePriceDefaultKey));
        } else {
            $price = 0;
            $this->manager->defaultRates()->keys()->each(function ($identifier) use (&$price) {
                $price += $this->getRateByIdentifier($identifier)->getPrice();
            });
            Cache::put($this->cachePriceDefaultKey, $price, 1440);
            return $price;
        }
    }


    /**
     * @param $attribute
     * @param $value
     * @return bool|int
     */
    private function getSummaryOfRatesByAttribute($attribute, $value)
    {
        $rates = $this->rateRepository->findAllBy($attribute, $value);

        if ($rates) {
            $price = 0;
            $rates->each(function ($rate) use (&$price) {
                $price += $this->getRateByIdentifier($rate->rate_ident)->getPrice();

            });
            return $price;
        } else {
            return false;
        }
    }

    /**
     * Проверяем подключен ли free период у пользователя
     * Вернет true в случае если подключен
     * @param $userId
     * @return bool
     */
    public function checkTheFreePeriod($userId)
    {
        // Спросите меня как он это делает?
        return boolval($this->rateRepository->findBy('user_id', $userId, ['id']));
    }

    /**
     * Подключаем тарифы по умолчанию
     * @param $userId
     * @param $companyId
     * @param bool $bill Выставлять счет или нет
     * @return bool
     * @throws RateNotFoundException
     */
    public function connectDefaultRates($userId, $companyId, $bill = false)
    {
        try {
            DB::beginTransaction();
            $this->manager->defaultRates()->keys()->each(function ($identifier) use ($userId, $companyId, $bill) {
                $connectRate  = $this->connectDefaultRate($userId, $companyId, $identifier);
                $connectRate->status = BillingRatesUserReference::STATUS_ACTIVE;
                $connectRate->save();
            });
            DB::commit();

        } catch (RateNotFoundException $e) {
            DB::rollBack();
            throw new RateNotFoundException;
        }
        return true;
    }



    /**
     * @param $companyId
     * @param $identifier
     * @return mixed
     * @throws RateNotFoundException
     */
    public function checkConnectionRate($companyId, $identifier)
    {
        if ($this->checkRateIdentifier($identifier)) {
            return (boolval($this->rateRepository->getByCompanyIdAndRateIdent($companyId, $identifier))
                    && !($this->getRateByIdentifier($identifier) instanceof RepeatInterface));
        } else {
            throw new RateNotFoundException;
        }
    }

    /**
     * @param $userId
     * @return Collection
     */
    public function getRatesByUserId($userId)
    {
        return $this->rateRepository->getByUserId($userId);
    }

    /**
     * @param $companyId
     * @param $rateIdent
     * @return mixed
     */
    public function getRatesByCompanyIdAndRateIdentifier($companyId, $rateIdent)
    {
        return $this->rateRepository->getByCompanyIdAndRateIdent($companyId, $rateIdent);
    }

    /**
     * @param $companyId
     * @return mixed
     */
    public function getRatesByCompanyId($companyId)
    {
        return $this->rateRepository->findAllBy('company_id', $companyId);
    }

    /**
     * Возвращает активные тарифы на добавление пользователей для компании
     *
     * @return Collection
     */
    public function getAddUserRates()
    {
        return $this->rateRepository
            ->pushCriteria(new StatusCriteria(BillingRatesUserReference::STATUS_ACTIVE))
            ->pushCriteria(new RateIdentCriteria(CrmAddUserRate::IDENTIFIER))->all();
    }

    /**
     * Возвращает все тарифы активные тарифы на добавление пользователя к компании.
     *
     * @return Collection
     */
    public function getAllAddUserAndLiteUserRates()
    {
        return $this->rateRepository
            ->pushCriteria(new StatusCriteria(BillingRatesUserReference::STATUS_ACTIVE))
            ->pushCriteria(new RateIdentArrayCriteria([CrmUsersLiteRate::IDENTIFIER, CrmUsersLiteRate::IDENTIFIER]))
            ->all();
    }


    /**
     * Создаем коллекцию классов rate
     * @param Collection $rates
     * @return Collection
     */
    public function makeRatesCollection(Collection $rates)
    {
        $ratesCollection = new Collection([]);
        $rates->each(function ($rate) use ($ratesCollection) {
            $ratesCollection->push(array_get($this->manager->rates(), $rate->rate_ident, null));
        });
        return $ratesCollection;
    }


    /**
     * @param $companyId
     * @param $identifier
     * @param int $month
     * @return BillingRatesUser
     * @throws MonthCanNotBeNegativeException
     * @throws RateNotFoundException
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     * @throws \Billing\Exceptions\NotEnoughMoneyException
     */
    public function extendRateMonth($companyId, $identifier, $month = 1)
    {
        if ($month <= 0) {
            throw new MonthCanNotBeNegativeException;
        }

        /** @var BillingRatesUser $connectedRate */
        $connectedRate = $this->rateRepository->getByCompanyIdAndRateIdent($companyId, $identifier);
        if (!$connectedRate) {
            throw new RateNotFoundException;
        }

        $rate = $this->getRateByIdentifier($identifier);
        $totalPrice = 0;
        // Получаем всю сумму покупки
        for ($i = 0; $i <= $month; $i++) {
            $totalPrice += $rate->getPrice();
        }
        // Списываем с баланса
        //@todo заплатить денег
        // Продливаем тариф
        $connectedRate->date_end = $connectedRate->date_end->addMonth($month);
        $connectedRate->save();

        return $connectedRate;
    }

    /**
     * Подключить тариф и заплатить денежку
     * @param $userId
     * @param $companyId
     * @param $rateId
     * @return BillingRatesUser
     * @throws RateIsAlreadyConnectedException
     * @throws RateNotFoundException
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     * @throws \Billing\Exceptions\NotEnoughMoneyException
     */
    public function connectRateAndPay($userId, $companyId, $rateId)
    {
        $rate = $this->connectRate($userId, $companyId, $rateId);
        return $rate;
    }

    /**
     * Активация Rate
     * @param $id
     * @return BillingRatesUser
     */
    public function activateRate($id)
    {
        $rate = $this->rateRepository->find($id);
        $rate->status = BillingRatesUserReference::STATUS_ACTIVE;
        $rate->save();
        // Отключаем старые тарифы
        $this->event->fire('Billing\Events\ActivateRateEvent', $rate);

        return $rate;
    }

    /**
     * @param BillingRatesUser $billingRatesUser
     * @return float
     * @throws RateNotFoundException
     */
    public function getOddMoney(BillingRatesUser $billingRatesUser)
    {
        return ($this->getRateByIdentifier($billingRatesUser->rate_ident)->getPrice() / Carbon::now()->daysInMonth)
            * (Carbon::now()->daysInMonth - Carbon::now()->day);
    }
}
