<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Interfaces\CountInterface;
use Billing\Services\Rates\Interfaces\RateInterface;


class CrmUsersLiteRate implements RateInterface, CountInterface
{
    const DEFAULT_DAYS = 30;

    const DEFAULT_VALUE = 5;

    const IDENTIFIER = 101;

    const DEFAULT_PRICE = 1000;

    /**
     * @return int
     */
    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return 'Saas\Models\Users';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return int
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crm.lite';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'CRM Lite';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Возможность завести 5 пользователей';
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return UserGroupRates::IDENTIFIER;
    }


}