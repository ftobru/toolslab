<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\HostGroupRates;
use Billing\Services\Rates\Interfaces\LikenInterface;
use Billing\Services\Rates\Interfaces\RateInterface;

/**
 * Class LandingControlLiteRate
 * @package Billing\Service\Rates
 */
class LandingControlHostLiteRate implements RateInterface, LikenInterface
{
    const IDENTIFIER = 201;

    const DEFAULT_DAYS = 30;

    const DEFAULT_PRICE = 500;

    const DEFAULT_VALUE = 1000;

    /**
     * @return string
     */
    public function getEntity()
    {
        return 'LandingControl\Models\Link';
    }

    /**
     * @return string
     */
    public function getField()
    {
        return 'hosts';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'landingControl.host.lite';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Страницы - Lite';
    }

    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Кол-во посещений в месяц до 3000';
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return HostGroupRates::IDENTIFIER;
    }


}