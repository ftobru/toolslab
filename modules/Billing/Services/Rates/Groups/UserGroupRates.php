<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Interfaces\GroupRatesInterface;

/**
 * Class UserGroupRates
 * @package Billing\Services\Rates
 */
class UserGroupRates implements GroupRatesInterface
{
    const IDENTIFIER = 3;

    /**
     * @return integer
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }


    /**
     * @return array
     */
    public static function rates()
    {
        return [
            CrmUsersLiteRate::IDENTIFIER
        ];
    }
}
