<?php namespace Billing\Services\Rates\Groups;

use Billing\Services\Rates\CrmAddUserRate;
use Billing\Services\Rates\Interfaces\GroupRatesInterface;

/**
 * Class AddUserGroupRates
 * @package Billing\Services\Rates\Groups
 */
class AddUserGroupRates implements GroupRatesInterface
{
    const IDENTIFIER = 4;

    /**
     * @return integer
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return array
     */
    public static function rates()
    {
        return [
            CrmAddUserRate::IDENTIFIER
        ];
    }

}
