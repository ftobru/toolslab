<?php namespace Billing\Services\Rates\Groups;

use Billing\Services\Rates\Interfaces\GroupRatesInterface;
use Billing\Services\Rates\LandingControlDiskLiteRate;
use Billing\Services\Rates\LandingControlDiskNormalRate;
use Billing\Services\Rates\LandingControlDiskPrimaryRate;

/**
 * Class DiskGroupRates
 * @package Billing\Services\Rates\Groups
 */
class DiskGroupRates implements GroupRatesInterface
{
    const IDENTIFIER = 1;

    /**
     * @return integer
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return array
     */
    public static function rates()
    {
        return [
            LandingControlDiskLiteRate::IDENTIFIER,
            LandingControlDiskNormalRate::IDENTIFIER,
            LandingControlDiskPrimaryRate::IDENTIFIER
        ];
    }
}
