<?php namespace Billing\Services\Rates\Groups;

use Billing\Services\Rates\Interfaces\GroupRatesInterface;
use Billing\Services\Rates\LandingControlHostLiteRate;
use Billing\Services\Rates\LandingControlHostNormalRate;

class HostGroupRates implements GroupRatesInterface
{
    const IDENTIFIER = 2;

    /**
     * @return integer
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return array
     */
    public static function rates()
    {
        return [
            LandingControlHostLiteRate::IDENTIFIER,
            LandingControlHostNormalRate::IDENTIFIER
        ];
    }


}
