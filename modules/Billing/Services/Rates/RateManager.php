<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\AddUserGroupRates;
use Billing\Services\Rates\Groups\DiskGroupRates;
use Billing\Services\Rates\Groups\HostGroupRates;
use Illuminate\Support\Collection;
use Saas\Models\Company;
use Saas\Models\User;

/**
 * Class RateManager
 * @package Billing\Service\Rates
 */
class RateManager
{
    /**
     * @return array
     */
    public function rates()
    {
        return [
            CrmUsersLiteRate::IDENTIFIER => CrmUsersLiteRate::class,
            CrmAddUserRate::IDENTIFIER => CrmAddUserRate::class,
            LandingControlHostLiteRate::IDENTIFIER => LandingControlHostLiteRate::class,
            LandingControlHostNormalRate::IDENTIFIER => LandingControlHostNormalRate::class,
            LandingControlHostProRate::IDENTIFIER => LandingControlHostProRate::class,
            LandingControlDiskLiteRate::IDENTIFIER => LandingControlDiskLiteRate::class,
            LandingControlDiskNormalRate::IDENTIFIER => LandingControlDiskNormalRate::class,
            LandingControlDiskPrimaryRate::IDENTIFIER => LandingControlDiskPrimaryRate::class
        ];
    }

    /**
     * @return Collection
     */
    public function defaultRates()
    {
        return new Collection([
            CrmUsersLiteRate::IDENTIFIER => CrmUsersLiteRate::class,
            LandingControlDiskLiteRate::IDENTIFIER => LandingControlDiskLiteRate::class,
            LandingControlHostLiteRate::IDENTIFIER => LandingControlHostLiteRate::class
        ]);
    }

    /**
     * Группы тарифов
     * @return Collection
     */
    public function groupsRates()
    {
        return new Collection([
            UserGroupRates::IDENTIFIER => UserGroupRates::rates(),
            AddUserGroupRates::IDENTIFIER => AddUserGroupRates::rates(),
            DiskGroupRates::IDENTIFIER => DiskGroupRates::rates(),
            HostGroupRates::IDENTIFIER => HostGroupRates::rates()
        ]);
    }

}
