<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\DiskGroupRates;
use Billing\Services\Rates\Interfaces\LikenInterface;
use Billing\Services\Rates\Interfaces\RateInterface;

class LandingControlDiskPrimaryRate implements RateInterface, LikenInterface
{

    const IDENTIFIER = 205;

    const DEFAULT_VALUE = 10;

    const DEFAULT_PRICE = 500;

    const DEFAULT_DAYS = 30;

    /**
     * Сущность
     * @return mixed
     */
    public function getEntity()
    {
        return 'LandingControl\Models\Disk';
    }

    /**
     * Поле значение
     * @return mixed
     */
    public function getField()
    {
        return 'gb';
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'landingControl.disk.primary';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Страницы - Диск 10GB';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Дисковое пространство под ваши страницы более 10GB';
    }

    /**
     * @return integer
     */
    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return DiskGroupRates::IDENTIFIER;
    }


}
