<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\HostGroupRates;
use Billing\Services\Rates\Interfaces\LikenInterface;
use Billing\Services\Rates\Interfaces\RateInterface;

/**
 * Class LandingControlNormalRate
 * @package Billing\Service\Rates
 */
class LandingControlHostNormalRate implements LikenInterface, RateInterface
{
    const IDENTIFIER = 202;

    const DEFAULT_DAYS = 30;

    const DEFAULT_PRICE = 3000;

    const DEFAULT_VALUE = 2000;

    /**
     * @return string
     */
    public function getEntity()
    {
        return 'LandingControl\Models\Link';
    }

    /**
     * @return string
     */
    public function getField()
    {
        return 'hosts';
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'landingControl.host.normal';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Страницы - Normal';
    }

    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Кол-во посещений в месяц более 2000';
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return HostGroupRates::IDENTIFIER;
    }


}