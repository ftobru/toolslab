<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\DiskGroupRates;
use Billing\Services\Rates\Interfaces\RateInterface;

class LandingControlDiskNormalRate implements RateInterface
{
    const IDENTIFIER = 204;

    const DEFAULT_VALUE = 5;

    const DEFAULT_PRICE = 300;

    const DEFAULT_DAYS = 30;

    /**
     * Сущность
     * @return mixed
     */
    public function getEntity()
    {
        return 'LandingControl\Models\Disk';
    }

    /**
     * Поле значение
     * @return mixed
     */
    public function getField()
    {
        return 'gb';
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'landingControl.disk.normal';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Страницы - Диск 5GB';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Дисковое пространство под ваши страницы более 5GB';
    }

    /**
     * @return integer
     */
    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return DiskGroupRates::IDENTIFIER;
    }


}

