<?php namespace Billing\Services\Rates\Interfaces;

/**
 * Тариф
 * Interface RateInterface
 * @package Billing\Service\Rates\Interfaces
 */
interface RateInterface
{
    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * @return float
     */
    public function getPrice();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getDisplayName();

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @return integer
     */
    public function getDays();

    /**
     * @return integer
     */
    public static function getGroupIdentifier();

}
