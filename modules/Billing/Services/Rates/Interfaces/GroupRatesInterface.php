<?php namespace Billing\Services\Rates\Interfaces;

/**
 * Interface GroupRatesInterface
 * @package Billing\Services\Rates\Interfaces
 */
interface GroupRatesInterface
{
    /**
     * @return integer
     */
    public function getIdentifier();

    /**
     * @return array
     */
    public static function rates();
}
