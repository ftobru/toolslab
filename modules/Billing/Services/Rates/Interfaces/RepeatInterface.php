<?php namespace Billing\Services\Rates\Interfaces;

interface RepeatInterface
{
    public function isRepeat();
}
