<?php namespace Billing\Services\Rates\Interfaces;

/**
 * Равный парамметру
 * Interface LikenInterface
 * @package Billing\Service\Rates\Interfaces
 */
interface LikenInterface
{
    /**
     * Сущность
     * @return mixed
     */
    public function getEntity();

    /**
     * Поле значение
     * @return mixed
     */
    public function getField();

    /**
     * @return mixed
     */
    public function getValue();
}
