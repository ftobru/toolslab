<?php namespace Billing\Services\Rates\Interfaces;

/**
 * Interface CountInterface
 * @package Billing\Service\Rates\Interfaces
 */
interface CountInterface
{
    public function getEntity();

    public function getValue();
}