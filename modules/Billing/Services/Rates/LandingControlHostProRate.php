<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 17:06
 */

namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\HostGroupRates;
use Billing\Services\Rates\Interfaces\LikenInterface;
use Billing\Services\Rates\Interfaces\RateInterface;

class LandingControlHostProRate implements  LikenInterface, RateInterface
{

    const IDENTIFIER = 206;

    const DEFAULT_DAYS = 30;

    const DEFAULT_PRICE = 6000;

    const DEFAULT_VALUE = 3000;

    /**
     * Сущность
     * @return mixed
     */
    public function getEntity()
    {
        return 'LandingControl\Models\Link';
    }

    /**
     * Поле значение
     * @return mixed
     */
    public function getField()
    {
        return 'hosts';
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'landingControl.host.normal';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'Страницы - Normal';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Кол-во посещений в месяц более 3000';
    }

    /**
     * @return integer
     */
    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return HostGroupRates::IDENTIFIER;
    }

}