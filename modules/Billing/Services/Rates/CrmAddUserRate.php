<?php namespace Billing\Services\Rates;

use Billing\Services\Rates\Groups\AddUserGroupRates;
use Billing\Services\Rates\Interfaces\CountInterface;
use Billing\Services\Rates\Interfaces\RateInterface;
use Billing\Services\Rates\Interfaces\RepeatInterface;

class CrmAddUserRate implements RateInterface, CountInterface, RepeatInterface
{
    const DEFAULT_DAYS = 30;

    const DEFAULT_VALUE = 1;

    const IDENTIFIER = 102;

    const DEFAULT_PRICE = 300;


    public function isRepeat()
    {
        return 100;
    }

    public function getDays()
    {
        return self::DEFAULT_DAYS;
    }

    public function getEntity()
    {
        return 'Saas\Models\Users';
    }

    public function getValue()
    {
        return self::DEFAULT_VALUE;
    }

    /**
     * @return int
     */
    public function getIdentifier()
    {
        return self::IDENTIFIER;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return self::DEFAULT_PRICE;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'crm.addUser';
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return 'CRM - пользователь';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return 'Добавить еще одного пользователя';
    }

    /**
     * @return integer
     */
    public static function getGroupIdentifier()
    {
        return AddUserGroupRates::IDENTIFIER;
    }


}
