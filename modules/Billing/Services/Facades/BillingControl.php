<?php namespace Billing\Services\Facades;


use Illuminate\Support\Facades\Facade;

/**
 * Class BillingControl
 * @package Billing\Services\Facades
 */
class BillingControl extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'BillingControl';
    }

}
