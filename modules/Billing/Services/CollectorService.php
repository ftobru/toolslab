<?php namespace Billing\Services;

use Log;
use Saas\Services\UserService;

/**
 * Проверяем точность баланса
 * Class CollectorService
 * @package Billing\Services
 */
class CollectorService
{
    /** @var AccountService  */
    protected $accountService;
    /** @var  UserService */
    protected $userService;

    /**
     * @param AccountService $accountService
     * @param UserService $userService
     */
    public function __construct(AccountService $accountService, UserService $userService)
    {
        $this->accountService = $accountService;
        $this->userService = $userService;
    }


    public function revision()
    {
        $users = $this->userService->getActiveUsers();
        /**
         * Считаем бабло по пользователю
         */
        $users->each(function ($user) {
            $account = $this->accountService->getCurrentBalance($user->id);
            $currentBalance = $this->accountService->getDebitByUserId($user->id)->first()
                - $this->accountService->getCreditByUserId($user->id)->first();

            if ($account->amount != $currentBalance->amount) {
                $account->amount = $currentBalance->amount;
                $account->save();
                Log::useDailyFiles(storage_path() . 'logs/billing_collector.log', 0, 'warning');
                Log::warning('User #' . $user->id
                    . '. The amount is not correct (Account balance - '
                    . $account->amount . ' Current balance - ' . $currentBalance);
            }
        });
    }
}
