<?php namespace Billing\Services;

use Billing\Exceptions\AccountNotFoundException;
use Billing\Exceptions\AmountCanNotBeNegativeException;
use Billing\Exceptions\InsufficientFundsException;
use Billing\Exceptions\NotEnoughMoneyException;
use Billing\Models\BillingAccount;
use Billing\Reference\BillingAccountReference;
use Billing\Reference\BillingInvoiceReference;
use Billing\Repositories\Eloquent\BillingAccountEloquentRepository;
use Billing\Repositories\Eloquent\BillingInvoiceEloquentRepository;
use Billing\Repositories\Eloquent\BillingPaymentEloquentRepository;
use Billing\Repositories\Eloquent\Criteria\SelectSumAmountCriteria;
use Billing\Repositories\Eloquent\Criteria\UserIdCriteria;
use Billing\Repositories\Eloquent\Criteria\UserIdTableCriteria;
use Billing\Repositories\Interfaces\BillingAccountRepositoryInterface;
use Billing\Repositories\Interfaces\BillingInvoiceRepositoryInterface;
use Billing\Repositories\Interfaces\BillingPaymentRepositoryInterface;
use Modules\Billing\Reference\BillingPaymentReference;

/**
 * Class AccountService
 * @package Billing\Services
 */
class AccountService
{
    /** @var  BillingAccountEloquentRepository*/
    protected $accountRepository;
    /** @var  BillingInvoiceEloquentRepository */
    protected $invoiceRepository;
    /** @var  BillingPaymentEloquentRepository */
    protected $paymentRepository;

    public function __construct(
        BillingAccountRepositoryInterface $accountRepositoryInterface,
        BillingInvoiceRepositoryInterface $invoiceRepositoryInterface,
        BillingPaymentRepositoryInterface $paymentRepositoryInterface
    ) {
        $this->accountRepository = $accountRepositoryInterface;
        $this->invoiceRepository = $invoiceRepositoryInterface;
        $this->paymentRepository = $paymentRepositoryInterface;
    }

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getDebitByUserId($userId)
    {
        return $this->paymentRepository
            ->pushCriteria(new SelectSumAmountCriteria())
            ->getByStatusAndUserId(BillingPaymentReference::STATUS_CONFIRM, $userId);
    }

    /**
     * @param $userId
     * @return \Illuminate\Support\Collection
     */
    public function getCreditByUserId($userId)
    {
        return $this->invoiceRepository->pushCriteria(new SelectSumAmountCriteria())
            ->getByStatusAndUserId(BillingInvoiceReference::STATUS_PAY, $userId);
    }

    /**
     * @param $userId
     * @return BillingAccount|null
     */
    public function getCurrentBalance($userId)
    {
        return $this->accountRepository->getByCriteria(new UserIdCriteria($userId))->first();
    }

    /**
     * Снятие денег с баланса
     * @param $amount
     * @param $userId
     * @return BillingAccount|null
     * @throws AccountNotFoundException
     * @throws AmountCanNotBeNegativeException
     * @throws InsufficientFundsException
     * @throws NotEnoughMoneyException
     */
    public function removal($amount, $userId)
    {
        $account = $this->getCurrentBalance($userId);
        if (!$account) {
            throw new AccountNotFoundException;
        }

        if ($amount > $account->amount) {
            throw new NotEnoughMoneyException;
        }

        if (!($amount > 0)) {
            // Сумма не может быть отрицательной
            throw new AmountCanNotBeNegativeException;
        }

        if (floatval($account->amount) <= BillingAccountReference::getMinBalance()) {
            // На балансе не может быть 0 или меньше
            throw new InsufficientFundsException;
        }

        $account->amount = $account->amount - $amount;
        $account->save();

        return $account;
    }

    /**
     * Занисение денег на баланса
     * @param $amount
     * @param $userId
     * @return BillingAccount|null
     * @throws AccountNotFoundException
     * @throws AmountCanNotBeNegativeException
     */
    public function recharge($amount, $userId)
    {
        $account = $this->getCurrentBalance($userId);
        if (!$account) {
            throw new AccountNotFoundException;
        }

        if (!($amount > 0)) {
            // Сумма не может быть отрицательной
            throw new AmountCanNotBeNegativeException;
        }
        $account->amount = $account->amount + $amount;
        $account->save();
        return $account;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createAccount($data = [])
    {
        $data = array_merge($data, [
            'amount' => 0,
            'count_operations' => 0,
            'status' => BillingAccountReference::STATUS_ACTIVE,
            'type' => BillingInvoiceReference::TYPE_NORMAL,
        ]);
        return $this->accountRepository->create($data);
    }

    /**
     * @param $amount
     * @param $userId
     * @return bool
     * @throws AccountNotFoundException
     */
    public function checkBalance($amount, $userId)
    {
        /** @var BillingAccount $account */
        $account = $this->accountRepository->pushCriteria(new UserIdCriteria($userId))->first();

        if (!$account) {
            throw new AccountNotFoundException;
        }

        return $account->amount >= $amount;
    }
}
