<?php namespace Billing\Services;

use Billing\Services\Rates\RateManager;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class BillingControlService
 * @package Billing\Services
 */
class BillingControlService
{
    /** @var  RateManager */
    protected $manager;

    /** @var  Repository */
    protected $repository;

    /**
     * @param RateManager $manager
     */
    public function __construct(RateManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param \Ftob\Repositories\Eloquent\Repository $repository
     * @return ControlRepositoryService
     */
    public function repository(Repository $repository)
    {
        $this->repository = $repository;
        return new ControlRepositoryService($this->repository);
    }

}
