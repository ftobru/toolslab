<?php namespace Billing\Reference;

/**
 * Class BillingInvoiceReference
 * @package Billing\Reference
 */
class BillingInvoiceReference
{
    const STATUS_ABUSE = -1;
    const STATUS_NO_PAY = 0;
    const STATUS_PAY = 1;

    const TYPE_NORMAL = 1;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_ABUSE,
            self::STATUS_NO_PAY,
            self::STATUS_PAY
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_NORMAL
        ];
    }


}