<?php namespace Modules\Billing\Reference;

/**
 * Class BillingPaymentReference
 * @package Modules\Billing\Reference
 */
class BillingPaymentReference
{
    /** Платеж отклонен */
    const STATUS_REJECT = -1;
    /** Не потвержден */
    const STATUS_NOT_CONFIRM = 0;
    /** Successa. Бабло у нас! */
    const STATUS_CONFIRM = 1;
    const TYPE_NORMAL = 0;
    public static function statuses()
    {
        return [
            self::STATUS_REJECT,
            self::STATUS_NOT_CONFIRM,
            self::STATUS_CONFIRM
        ];
    }
}