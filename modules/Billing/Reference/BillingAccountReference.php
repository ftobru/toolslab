<?php namespace Billing\Reference;

/**
 * Справочник для Billing accounts
 * Class BillingAccount
 * @package Core\References
 */
class BillingAccountReference
{

    public function __construct()
    {

    }

    const STATUS_BLOCK = -1;
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /** Статус не найден */
    const ERROR_STATUS_NOT_FOUND = 101;
    /** Тип не найден */
    const ERROR_TYPE_NOT_FOUND = 102;
    /** Аккаунт не найден :( */
    const ERROR_ACCOUNT_NOT_FOUND = 103;
    /** Денег нет */
    const ERROR_NO_MONEY = 104;

    const TYPE_NORMAL = 1;

    const MIN_BALANCE = 0;

    /**
     * State account  billing
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_NORMAL
        ];
    }

    /**
     * @return array
     */
    public static function errors()
    {
        return [
            self::ERROR_STATUS_NOT_FOUND,
            self::ERROR_TYPE_NOT_FOUND,
            self::ERROR_ACCOUNT_NOT_FOUND
        ];
    }

    /**
     * @return int
     */
    public static function getMinBalance()
    {
        return self::MIN_BALANCE;
    }

}