<?php namespace Billing\Reference;

/**
 * Class BillingRatesUserReference
 * @package Billing\Reference
 */
class BillingRatesUserReference
{
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;


    const ERROR_RATE_NOT_FOUND = 901;

    public static function statuses()
    {
        return [
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }
}
