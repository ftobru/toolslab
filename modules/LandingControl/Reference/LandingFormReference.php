<?php namespace LandingControl\Reference;

/**
 * Class LandingFormReference
 * @package LandingControl\Reference
 */
class LandingFormReference
{
    const FORM_TYPE_POPUP = 0;
    const FORM_TYPE_INTEGRATED = 1;

    const CALLBACK_POPUP = 0;
    const CALLBACK_REDIRECT = 1;

    /**
     * Landing form types.
     *
     * @return array
     */
    public static function types()
    {
        return [
            self::FORM_TYPE_POPUP,
            self::FORM_TYPE_INTEGRATED,
        ];
    }

    /**
     * Form callback types.
     *
     * @return array
     */
    public static function callbacks()
    {
        return [
            self::CALLBACK_POPUP,
            self::CALLBACK_REDIRECT,
        ];
    }
}
