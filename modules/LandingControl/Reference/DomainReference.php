<?php namespace LandingControl\Reference;

/**
 * Справочник domain
 * Class DomainReference
 * @package LandingControl\Reference
 */
class DomainReference
{

    const STATUS_BLOCK = -1;
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /** Статус не найден */
    const ERROR_STATUS_NOT_FOUND = 401;
    /** Домен не найден :( */
    const ERROR_DOMAIN_NOT_FOUND = 403;

    /**
     * State domain
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_BLOCK,
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }


    /**
     * IP адреса наших серверов
     * @return array
     */
    public static function ips()
    {
        return [
            '148.251.186.11',
            '148.251.186.12'
        ];
    }


    /**
     * @return array
     */
    public static function errors()
    {
        return [
            self::ERROR_STATUS_NOT_FOUND,
            self::ERROR_DOMAIN_NOT_FOUND
        ];
    }
}
