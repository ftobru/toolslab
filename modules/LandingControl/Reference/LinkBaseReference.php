<?php namespace LandingControl\Reference;

/**
 * Class LinkBaseReference
 * @package LandingControl\Reference
 */
class LinkBaseReference
{
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_TEMPLATE = 0;
    const TYPE_AB_SPLIT = 1;
    const TYPE_GEO_SPLIT = 2;

    const PATH_NOT_EXIST = 0;
    const PATH_EXIST = 1;
    const PATH_NOT_SET = 2;

    /** Статус не найден */
    const ERROR_STATUS_NOT_FOUND = 401;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public static function pathStates()
    {
        return [
            self::PATH_EXIST,
            self::PATH_NOT_EXIST,
            self::PATH_NOT_SET
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_TEMPLATE,
            self::TYPE_AB_SPLIT,
            self::TYPE_GEO_SPLIT
        ];
    }

    /**
     * @return array
     */
    public static function errors()
    {
        return [
            self::ERROR_STATUS_NOT_FOUND
        ];
    }
}
