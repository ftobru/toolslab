<?php namespace LandingControl\Repositories\Eloquent;


use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface;

/**
 * Class ReplacementTimeEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class ReplacementTimeEloquentRepository extends Repository implements ReplacementTimeRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\ReplacementTime';
    }
}