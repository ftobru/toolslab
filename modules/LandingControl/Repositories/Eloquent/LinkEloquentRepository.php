<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Criteria\Link\WithCriteria;
use LandingControl\Repositories\Interfaces\LinkRepositoryInterface;

/**
 * Class LinkEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class LinkEloquentRepository extends Repository implements LinkRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\Link';
    }

    /**
     * @param array $entities
     * @return $this
     */
    public function getWith(array $entities)
    {
        return $this->pushCriteria(new WithCriteria($entities));
    }

}
