<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;

/**
 * Class DomainEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class DomainEloquentRepository extends Repository implements DomainRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\Domain';
    }
}
