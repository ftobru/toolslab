<?php
namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Criteria\LinkBase\WithDomainCriteria;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;

/**
 * Class LinkBaseEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class LinkBaseEloquentRepository extends Repository implements LinkBaseRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return \LandingControl\Models\LinkBase::class;
    }

    /**
     * @return $this
     */
    public function getWithDomain()
    {
        return $this->pushCriteria(new WithDomainCriteria());
    }
}
