<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface;

/**
 * Class ReplacementLinkEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class ReplacementLinkEloquentRepository extends Repository implements ReplacementLinkRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\ReplacementLink';
    }
}
