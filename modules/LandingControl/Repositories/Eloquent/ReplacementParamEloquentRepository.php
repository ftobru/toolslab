<?php namespace LandingControl\Repositories\Eloquent;


use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\ReplacementParamRepositoryInterface;

/**
 * Class ReplacementParamEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class ReplacementParamEloquentRepository extends Repository implements ReplacementParamRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\ReplacementParam';
    }
}
