<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Criteria\ReplacementProduct\WithCriteria;
use LandingControl\Repositories\Interfaces\ReplacementProductRepositoryInterface;

/**
 * Class ReplacementProductEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class ReplacementProductEloquentRepository extends Repository implements ReplacementProductRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\ReplacementProduct';
    }

    /**
     * @param array $entities
     * @return $this
     */
    public function getWith(array $entities)
    {
        return $this->pushCriteria(new WithCriteria($entities));
    }
}
