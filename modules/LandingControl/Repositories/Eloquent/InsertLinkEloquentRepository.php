<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\InsertLinkRepositoryInterface;

/**
 * Class InsertLinkEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class InsertLinkEloquentRepository extends Repository implements InsertLinkRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\InsertLink';
    }
}
