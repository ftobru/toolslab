<?php
namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Models\UTM;
use LandingControl\Repositories\Interfaces\UTMRepositoryInterface;

/**
 * Class UTMEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class UTMEloquentRepository extends Repository implements UTMRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return UTM::class;
    }
}
