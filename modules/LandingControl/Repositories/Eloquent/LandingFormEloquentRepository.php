<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use LandingControl\Repositories\Interfaces\LandingFormRepositoryInterface;

/**
 * Class LandingFormEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class LandingFormEloquentRepository extends Repository implements LandingFormRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\LandingForm';
    }
}
