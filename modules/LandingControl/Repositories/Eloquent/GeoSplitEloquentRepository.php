<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:16
 */

namespace LandingControl\Repositories\Eloquent;


use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Criteria\LandingForm\WithDomainCriteria;
use LandingControl\Repositories\Interfaces\GeoSplitRepositoryInterface;

class GeoSplitEloquentRepository extends Repository implements GeoSplitRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return \LandingControl\Models\GeoSplit::class;
    }

    /**
     * @return $this
     */
    public function getWithDomain()
    {
        return $this->pushCriteria(new WithDomainCriteria());
    }

}