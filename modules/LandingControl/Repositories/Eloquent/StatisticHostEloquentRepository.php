<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;

/**
 * Class UserEloquentRepository
 * @package Core\Repository\Eloquent
 */
class StatisticHostEloquentRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\StatisticHost';
    }
}
