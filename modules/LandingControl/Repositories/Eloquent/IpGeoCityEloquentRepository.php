<?php
namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\IpGeoCityRepositoryInterface;

/**
 * Class IpGeoCityEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class IpGeoCityEloquentRepository extends Repository implements IpGeoCityRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return \LandingControl\Models\IpGeoCity::class;
    }
}
