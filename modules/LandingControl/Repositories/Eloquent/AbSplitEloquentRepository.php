<?php
namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Criteria\LandingForm\WithDomainCriteria;
use LandingControl\Repositories\Interfaces\AbSplitRepositoryInterface;

/**
 * Class AbSplitEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class AbSplitEloquentRepository extends Repository implements AbSplitRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return \LandingControl\Models\AbSplit::class;
    }

    /**
     * @return $this
     */
    public function getWithDomain()
    {
        return $this->pushCriteria(new WithDomainCriteria());
    }

}