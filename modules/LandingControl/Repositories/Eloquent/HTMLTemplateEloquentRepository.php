<?php
namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\HTMLTemplateRepositoryInterface;

/**
 * Class HTMLTemplateEloquentRepository
 * @package LandingControl\Repositories\Eloquent
 */
class HTMLTemplateEloquentRepository extends Repository implements HTMLTemplateRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return \LandingControl\Models\HTMLTemplate::class;
    }
}
