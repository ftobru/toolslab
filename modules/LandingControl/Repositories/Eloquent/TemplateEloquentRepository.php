<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;

/**
 * Class TemplateEloquentRepository
 * @package Core\Repositories\Eloquent
 */
class TemplateEloquentRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\Template';
    }

}
