<?php namespace LandingControl\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use LandingControl\Repositories\Interfaces\ReplacementGeoRepositoryInterface;

class ReplacementGeoEloquentRepository extends Repository implements ReplacementGeoRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'LandingControl\Models\ReplacementGeo';
    }

}
