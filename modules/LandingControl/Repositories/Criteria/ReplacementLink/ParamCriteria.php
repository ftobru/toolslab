<?php namespace LandingControl\Repositories\Criteria\ReplacementLink;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CodeCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementLink
 */
class ParamCriteria extends Criteria
{
    protected $param;

    /**
     * @param $param
     */
    public function __construct($param)
    {
        $this->param = $param;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('param', 'LIKE', '%'.$this->param.'%');
        return $query;
    }
}
