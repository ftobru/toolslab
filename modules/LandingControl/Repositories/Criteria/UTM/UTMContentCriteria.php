<?php namespace LandingControl\Repositories\Criteria\UTM;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class UTMContentCriteria
 * @package LandingControl\Repositories\Criteria\UTM
 */
class UTMContentCriteria extends Criteria
{
    protected $value;

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('utm_content', 'LIKE', '%' . $this->value . '%');
        return $query;
    }
}
