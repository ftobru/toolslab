<?php namespace LandingControl\Repositories\Criteria\UTM;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class DescriptionCriteria
 * @package LandingControl\Repositories\Criteria\UTM
 */
class DescriptionCriteria extends Criteria
{
    protected $value;

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('description', 'LIKE', '%' . $this->value . '%');
        return $query;
    }
}
