<?php namespace LandingControl\Repositories\Criteria\ReplacementProduct;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class DescriptionCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementProduct
 */
class DescriptionCriteria extends Criteria
{
    protected $description;

    /**
     * @param $description
     */
    public function __construct($description)
    {
        $this->description = $description;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('description', 'LIKE', '%'.$this->description.'%');
        return $query;
    }
}
