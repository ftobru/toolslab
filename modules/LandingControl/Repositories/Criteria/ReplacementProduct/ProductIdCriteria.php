<?php namespace LandingControl\Repositories\Criteria\ReplacementProduct;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ProductIdCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementProduct
 */
class ProductIdCriteria extends Criteria
{
    protected $productId;

    /**
     * @param $productId
     */
    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('product_id', '=', $this->productId);
    }
}