<?php namespace LandingControl\Repositories\Criteria\ReplacementProduct;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class KeyCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementProduct
 */
class KeyCriteria extends Criteria
{
    protected $key;

    /**
     * @param $key
     */
    public function __construct($key)
    {
        $this->key = $key;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('key', 'LIKE', '%' . $this->key . '%');
        return $query;
    }
}
