<?php namespace LandingControl\Repositories\Criteria\Link;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ShortLinkCriteria
 * @package LandingControl\Repositories\Criteria\Link
 */
class ShortLinkCriteria extends Criteria
{
    protected $link;

    /**
     * @param $link
     */
    public function __construct($link)
    {
        $this->link = $link;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('short_link', 'LIKE', $this->link);
        return $query;
    }
}
