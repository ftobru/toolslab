<?php namespace LandingControl\Repositories\Criteria\LandingForm;

use Crm\Models\Task;
use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithDomainCriteria
 * @package LandingControl\Repositories\Criteria\LandingForm
 */
class WithDomainCriteria extends Criteria
{
    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getModel()->with('domain');
    }
}
