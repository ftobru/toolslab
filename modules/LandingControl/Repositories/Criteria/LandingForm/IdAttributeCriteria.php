<?php namespace LandingControl\Repositories\Criteria\LandingForm;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class IdAttributeCriteria
 * @package LandingControl\Repositories\Criteria\LandingForm
 */
class IdAttributeCriteria extends Criteria
{
    protected $value;

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('id_attr', 'LIKE', '%' . $this->value . '%');
        return $query;
    }
}
