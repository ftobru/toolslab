<?php namespace LandingControl\Repositories\Criteria\LinkBase;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithDomainCriteria
 * @package  LandingControl\Repositories\Criteria\LinkBase
 */
class WithDomainCriteria extends Criteria
{
    protected $with;

    public function __construct()
    {
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getModel()->with(['domain']);
    }
}
