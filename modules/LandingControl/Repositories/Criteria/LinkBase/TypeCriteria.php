<?php namespace LandingControl\Repositories\Criteria\LinkBase;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TypeCriteria
 * @package LandingControl\Repositories\Eloquent\Criteria\LinkBase
 */
class TypeCriteria extends Criteria
{
    protected $type;

    /**
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('type', '=', $this->type);
    }
}