<?php namespace LandingControl\Repositories\Criteria\ReplacementParam;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TimeCompareCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementParam
 */
class TimeCompareCriteria extends Criteria
{
    protected $value;
    protected $operator;
    protected $columnName;

    /**
     * @param $columnName
     * @param $value
     * @param $operator string
     */
    public function __construct($columnName, $value, $operator)
    {
        $this->columnName = $columnName;
        $this->value = $value;
        $this->operator = $operator;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->whereRaw($this->columnName . '::date ' . $this->operator . ' ?', [$this->value]);
    }

}
