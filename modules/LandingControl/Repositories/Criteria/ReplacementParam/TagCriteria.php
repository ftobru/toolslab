<?php namespace LandingControl\Repositories\Criteria\ReplacementParam;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TagCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementParam
 */
class TagCriteria extends Criteria
{
    protected $tag;

    /**
     * @param $tag
     */
    public function __construct($tag)
    {
        $this->tag = $tag;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('tag', 'LIKE', '%'.$this->tag.'%');
        return $query;
    }
}
