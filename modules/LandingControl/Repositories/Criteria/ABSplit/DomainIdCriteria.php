<?php namespace LandingControl\Repositories\Criteria\ABSplit;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class DomainIdCriteria
 * @package LandingControl\Repositories\Eloquent\Criteria\ABSplit
 */
class DomainIdCriteria extends Criteria
{
    protected $domainId;

    /**
     * @param $domainId
     */
    public function __construct($domainId)
    {
        $this->domainId = $domainId;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('domain_id', '=', $this->domainId);
    }
}