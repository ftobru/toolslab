<?php namespace LandingControl\Repositories\Criteria\InsertLink;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CodeCriteria
 * @package LandingControl\Repositories\Criteria\InsertLink
 */
class CodeCriteria extends Criteria
{
    protected $code;

    /**
     * @param $code
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('code', 'LIKE', '%'.$this->code.'%');
        return $query;
    }
}
