<?php namespace LandingControl\Repositories\Criteria\ReplacementTime;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class MaxStartTimeCriteria
 * @package LandingControl\Repositories\Eloquent\Criteria\ReplacementTime
 */
class MaxStartTimeCriteria extends Criteria
{
    protected $start_time;

    /**
     * @param $startTime
     */
    public function __construct($startTime)
    {
        $this->start_time = $startTime;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('start_time', '<=', $this->start_time);
    }
}