<?php namespace LandingControl\Repositories\Criteria\ReplacementTime;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class MinFinishTimeCriteria
 * @package LandingControl\Repositories\Eloquent\Criteria\ReplacementTime
 */
class MinFinishTimeCriteria extends Criteria
{
    protected $finish_time;

    /**
     * @param $finish_time
     */
    public function __construct($finish_time)
    {
        $this->finish_time = $finish_time;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('finish_time', '>=', $this->finish_time);
    }
}