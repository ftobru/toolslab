<?php namespace LandingControl\Repositories\Criteria\ReplacementTime;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class MeaningCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementTime
 */
class MeaningCriteria extends Criteria
{
    protected $meaning;

    /**
     * @param $meaning
     */
    public function __construct($meaning)
    {
        $this->meaning = $meaning;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('meaning', 'LIKE', '%' . $this->meaning . '%');
        return $query;
    }
}
