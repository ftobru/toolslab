<?php namespace LandingControl\Repositories\Criteria\ReplacementGeo;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class AddressCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementGeo
 */
class AddressCriteria extends Criteria
{
    protected $address;

    /**
     * @param $address
     */
    public function __construct($address)
    {
        $this->address = $address;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('phone', 'LIKE', '%'.$this->address.'%');
        return $query;
    }
}
