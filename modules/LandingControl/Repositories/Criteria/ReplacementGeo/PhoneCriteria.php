<?php namespace LandingControl\Repositories\Criteria\ReplacementGeo;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CodeCriteria
 * @package LandingControl\Repositories\Criteria\ReplacementGeo
 */
class PhoneCriteria extends Criteria
{
    protected $phone;

    /**
     * @param $phone
     */
    public function __construct($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('phone', 'LIKE', '%'.$this->phone.'%');
        return $query;
    }
}
