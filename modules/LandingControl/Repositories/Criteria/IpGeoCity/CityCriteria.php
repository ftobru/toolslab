<?php namespace LandingControl\Repositories\Criteria\IpGeoCity;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CityCriteria
 * @package LandingControl\Repositories\Criteria\IpGeoCity
 */
class CityCriteria extends Criteria
{
    protected $name;

    /**
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('city', 'LIKE', '%'.$this->name.'%');
        return $query;
    }
}
