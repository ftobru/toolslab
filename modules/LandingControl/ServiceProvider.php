<?php namespace LandingControl;


use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package Modules\LandingControll
 */
class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'LandingControl\Repositories\Interfaces\DomainRepositoryInterface',
            'LandingControl\Repositories\Eloquent\DomainEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\LandingFormRepositoryInterface',
            'LandingControl\Repositories\Eloquent\LandingFormEloquentRepository'
        );

        $this->app->bind('EventRepositoryInterface', 'Core\Repositories\Eloquent\EventEloquentRepository');

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\InsertLinkRepositoryInterface',
            'LandingControl\Repositories\Eloquent\InsertLinkEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\LinkRepositoryInterface',
            'LandingControl\Repositories\Eloquent\LinkEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\ReplacementGeoRepositoryInterface',
            'LandingControl\Repositories\Eloquent\ReplacementGeoEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface',
            'LandingControl\Repositories\Eloquent\ReplacementLinkEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\ReplacementProductRepositoryInterface',
            'LandingControl\Repositories\Eloquent\ReplacementProductEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\ReplacementParamRepositoryInterface',
            'LandingControl\Repositories\Eloquent\ReplacementParamEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface',
            'LandingControl\Repositories\Eloquent\ReplacementTimeEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\StatisticHostRepositoryInterface',
            'LandingControl\Repositories\Eloquent\StatisticHostEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\AbSplitRepositoryInterface',
            'LandingControl\Repositories\Eloquent\AbSplitEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\GeoSplitRepositoryInterface',
            'LandingControl\Repositories\Eloquent\GeoSplitEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\LinkRepositoryInterface',
            'LandingControl\Repositories\Eloquent\LinkEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\UTMRepositoryInterface',
            'LandingControl\Repositories\Eloquent\UTMEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface',
            'LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\IpGeoCityRepositoryInterface',
            'LandingControl\Repositories\Eloquent\IpGeoCityEloquentRepository'
        );

        $this->app->bind(
            'LandingControl\Repositories\Interfaces\HTMLTemplateRepositoryInterface',
            'LandingControl\Repositories\Eloquent\HTMLTemplateEloquentRepository'
        );
    }
}
