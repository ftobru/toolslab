<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class InsertLink
 * @package LandingControl\Models
 */
class InsertLink extends CoreModel
{
    use CompanyName;

    protected $table = 'insert_links';

    protected $company = true;

    protected $fillable = [
        'name',
        'code',
        'param',
        'key_value',
        'company_id'
    ];
}
