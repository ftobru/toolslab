<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class StatisticHost
 * @package LandingControl\Models
 */
class StatisticHost extends CoreModel
{
    use CompanyName;

    protected $table = 'statistic_hosts';

    protected $company = true;
}
