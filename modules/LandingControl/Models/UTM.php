<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

/**
 * Class Template
 * @package LandingControl\Models
 */
class UTM extends CoreModel
{
    use CompanyName, SoftDeletes;

    protected $company = true;

    protected $table = 'utms';

    protected $fillable = [
        'name',
        'description',
        'utm_source',
        'utm_compaign',
        'utm_medium',
        'utm_term',
        'utm_content',
        'company_id'
    ];
}
