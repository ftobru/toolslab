<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class LandingForm
 * @package App
 * @property $name
 */
class LandingForm extends CoreModel
{
    use CompanyName;

    protected $table = 'landing_forms';

    protected $company = true;

    protected $fillable = [
        'name',
        'id_attr',
        'form_type',
        'callback_type',
        'callback_url',
        'callback_text',
        'after_header_text',
        'header_text',
        'before_header_text',
        'after_btn_text',
        'btn_text',
        'before_btn_text',
        'btn_color',
        'fields',
        'base_fields',
        'tags',
        'company_id'
    ];
}
