<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class ReplacementGeo
 * @package LandingControl\Models
 */
class ReplacementGeo extends CoreModel
{
    use CompanyName;

    protected $table = 'replacement_geo';
    protected $company = true;

    protected $fillable = [
        'city',
        'phone',
        'company_id',
        'address'
    ];

    public function getCityAttribute($value)
    {
        return json_decode($value);
    }
}
