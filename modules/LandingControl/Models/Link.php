<?php namespace LandingControl\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

/**
 * Class Link
 * @package LandingControl\Models
 */
class Link extends CoreModel
{
    use TaggableTrait, CompanyName, SoftDeletes;

    protected $company = true;

    protected $table = 'links';

    protected $fillable = [
        'name',
        'link',
        'short_link',
        'status',
        'hosts',
        'hits',
        'count_leads',
        'convert',
        'company_id',
        'replacement_product',
        'replacement_time',
        'replacement_param',
        'replacement_geo',
        'replacement_link',
        'insert_link',
        'utm',
        'type',
        'domain_id',
        'link_base_id',
        'path',
        'url'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('LandingControl\Models\Domain');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function linkBase()
    {
        return $this->belongsTo('LandingControl\Models\LinkBase');
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getReplacementProductAttribute($value)
    {
        return $this->getEntityByJson('replacement_products', $value);
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getUtmAttribute($value)
    {
        return $this->getEntityByJson('utms', $value);
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getReplacementTimeAttribute($value)
    {
        return $this->getEntityByJson('replacement_time', $value);
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getReplacementGeoAttribute($value)
    {
        return $this->getEntityByJson('replacement_geo', $value);
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getReplacementLinkAttribute($value)
    {
        return $this->getEntityByJson('replacement_links', $value);
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getReplacementParamAttribute($value)
    {
        return $this->getEntityByJson('replacement_params', $value);
    }

    /**
     * @param $value
     * @return $this|Link|null
     */
    public function getInsertLinkAttribute($value)
    {
        return $this->getEntityByJson('insert_links', $value);
    }

    /**
     * @param $table
     * @param $value
     * @return $this|null
     */
    private function getEntityByJson($table, $value)
    {
        if ($value) {
            $json_arr = json_decode($value, true);
            if (!empty($json_arr)) {
                return \DB::table($table)->select()->where('id', '=', $json_arr[0])->get();
            }
            return null;
        }
        return $value;
    }
}
