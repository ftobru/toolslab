<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class Domain
 * @package App
 * @property $name
 * @property $status
 */
class Domain extends CoreModel
{
    use CompanyName;

    protected $table = 'domains';

    protected $company = true;

    protected $fillable = [
        'name',
        'status',
        'company_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}
