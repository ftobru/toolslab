<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class ReplacementLink
 * @package LandingControl\Models
 */
class ReplacementLink extends CoreModel
{
    use CompanyName;

    protected $table = 'replacement_links';

    protected $company = false;

    protected $fillable = [
        'name',
        'code',
        'param',
        'key_value',
        'company_id'
    ];
}
