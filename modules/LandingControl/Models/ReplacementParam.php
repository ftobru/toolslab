<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class ReplacementParam
 * @package LandingControl\Models
 */
class ReplacementParam extends CoreModel
{
    use CompanyName;

    /**
     * @var string
     */
    protected $table = 'replacement_params';
    /**
     * @var bool
     */
    protected $company = true;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'tag',
        'meaning',
        'company_id'
    ];

}
