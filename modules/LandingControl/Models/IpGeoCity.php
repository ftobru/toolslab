<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:08
 */

namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

class IpGeoCity extends CoreModel
{

    protected $table = 'cities';

    protected $fillable = ['idx', 'city', 'region', 'district', 'lng', 'lat'];

}