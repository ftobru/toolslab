<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class ReplacementProduct
 * @package LandingControl\Models
 */
class ReplacementProduct extends CoreModel
{
    use CompanyName;

    protected $table = 'replacement_products';

    protected $company = true;

    protected $fillable = [
        'name',
        'description',
        'key',
        'product_id',
        'company_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo('Crm\Models\Product');
    }
}
