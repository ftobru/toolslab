<?php namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class Disk
 * @package LandingControl\Models
 * @property integer gb
 * @property integer company_id
 */
class Disk extends CoreModel
{

    use CompanyName;

    protected $company = true;

    protected $table = 'disks';

    protected $fillable = ['gb'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}


