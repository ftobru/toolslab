<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:08
 */

namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

class AbSplit extends CoreModel
{
    use CompanyName, SoftDeletes;

    protected $company = true;

    protected $table = 'ab_splits';

    protected $fillable = ['name', 'description', 'company_id', 'open_url', 'domain_id', 'url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('LandingControl\Models\Domain');
    }

    /**
     * @return $this
     */
    public function getWithAllRelation()
    {
        return $this->pushCriteria(new WithDomainCriteria());
    }
}