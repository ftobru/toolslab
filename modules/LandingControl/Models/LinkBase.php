<?php
namespace LandingControl\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

/**
 * Class LinkBase
 * @property  int id
 * @property  int type
 * @property string link_ids
 * @property mixed cities_idxs
 * @package LandingControl\Models
 */
class LinkBase extends CoreModel
{
    use CompanyName, SoftDeletes, TaggableTrait;

    protected $company = true;

    protected $table = 'link_bases';

    protected $fillable = [
        'name',
        'description',
        'company_id',
        'type',
        'open_url',
        'domain_id',
        'url',
        'path',
        'google_analytics',
        'ya_metrika',
        'link_ids'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('LandingControl\Models\Domain');
    }

    public function getLinkIdsAttribute($value)
    {
        if ($value) {
            return json_decode($value);
        }
        return [];
    }
}
