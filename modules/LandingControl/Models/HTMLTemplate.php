<?php

namespace LandingControl\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

/**
 * Class HTMLTemplate
 * @package LandingControl\Models
 */
class HTMLTemplate extends CoreModel
{
    use CompanyName, SoftDeletes, TaggableTrait;

    protected $company = true;

    protected $table = 'html_templates';

    protected $fillable = ['name', 'description', 'company_id', 'path', 'google_analytics', 'ya_metrika'];
}
