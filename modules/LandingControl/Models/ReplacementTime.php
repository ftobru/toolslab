<?php namespace LandingControl\Models;

use Carbon\Carbon;
use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class ReplacementTime
 * @package LandingControl\Models
 * @property string name
 * @property string tag
 * @property string meaning
 * @property Carbon $start_time
 * @property Carbon $finish_time
 * @property $company_id
 */
class ReplacementTime extends CoreModel
{
    use CompanyName;

    protected $table = 'replacement_time';

    protected $company = true;

    protected $fillable = [
        'name',
        'tag',
        'meaning',
        'start_time',
        'finish_time',
        'company_id'
    ];

    /**
     * @return static
     */
    public function getCurrentAttribute($value)
    {
        return Carbon::now();
    }

}
