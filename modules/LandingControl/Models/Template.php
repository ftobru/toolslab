<?php namespace LandingControl\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Saas\Traits\CompanyName;

/**
 * Class Template
 * @package LandingControl\Models
 */
class Template extends CoreModel
{
    use CompanyName, TaggableTrait, SoftDeletes;

    protected $company = true;

    protected $table = 'templates';

    protected $fillable = [
        'type',
        'status',
        'name',
        'domain_id',
        'product_id',
        'url',
        'file',
        'title_page',
        'description_page',
        'description',
        'tags_clients',
        'tags_tasks',
        'tags_orders',
        'hits',
        'hosts',
        'count_leads',
        'convert',
        'company_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('LandingControl\Models\Domain');
    }
}
