<?php

namespace LandingControl\Models;

use Core\Models\Abstracts\CoreModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use LandingControl\Repositories\Criteria\LandingForm\WithDomainCriteria;
use Saas\Traits\CompanyName;

/**
 * Class GeoSplit
 * @package LandingControl\Models
 */
class GeoSplit extends CoreModel
{
    use CompanyName, SoftDeletes;

    protected $company = true;

    protected $table = 'geo_splits';

    protected $fillable = ['name', 'description', 'company_id', 'open_url', 'domain_id', 'url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domain()
    {
        return $this->belongsTo('LandingControl\Models\Domain');
    }

    /**
     * @return $this
     */
    public function getWithAllRelation()
    {
        return $this->pushCriteria(new WithDomainCriteria());
    }
}