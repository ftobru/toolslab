<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 17.07.15
 * Time: 9:54
 */

namespace LandingControl\Console\Commands;

use App;
use LandingControl\Models\Domain;
use LandingControl\Reference\DomainReference;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;

use Illuminate\Console\Command;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;
use Saas\Services\SocketService;

class CheckDNSCommand extends Command
{

    protected $name = 'dns:check';

    protected $description = 'Check domain';

    /** @var  DomainEloquentRepository */
    protected $repository;

    /** @var  SocketService */
    protected $socket;

    /** @var  UserEloquentRepository */
    protected $userRepository;

    public function __construct(
        DomainRepositoryInterface $domainRepositoryInterface,
        SocketService $server,
        UserRepositoryInterface $repositoryInterface
    ) {
        parent::__construct();
        $this->repository = $domainRepositoryInterface;
        $this->socket = $server;
        $this->userRepository = $repositoryInterface;
    }


    public function handle()
    {
        $this->repository = App::make('LandingControl\Repositories\Interfaces\DomainRepositoryInterface');
        $domains = $this->repository->findAllBy('status', DomainReference::STATUS_NO_ACTIVE);
        if ($domains) {
            $domains->each(function($domain){
                /** @var Domain $domain */
                $check = checkdnsrr($domain->name, 'A');
                $this->comment($domain->name);
                if($check) {
                    if (in_array(gethostbyname($domain->name), DomainReference::ips())) {
                        $domain->status = DomainReference::STATUS_ACTIVE;
                        $domain->save();
                        $this->socket->push('domain', $domain->company()->get()->first()->user_id, $domain->id);
                    }

                }
            });
        }
    }
}