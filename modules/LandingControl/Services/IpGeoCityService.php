<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use LandingControl\Repositories\Criteria\ABSplit\DomainIdCriteria;
use LandingControl\Repositories\Criteria\ABSplit\NameCriteria;
use LandingControl\Repositories\Criteria\ABSplit\TimeCompareCriteria;
use LandingControl\Repositories\Criteria\IpGeoCity\CityCriteria;
use LandingControl\Repositories\Eloquent\AbSplitEloquentRepository;
use LandingControl\Repositories\Eloquent\IpGeoCityEloquentRepository;
use LandingControl\Repositories\Interfaces\AbSplitRepositoryInterface;
use LandingControl\Repositories\Interfaces\IpGeoCityRepositoryInterface;

/**
 * Class IpGeoCityService
 * @package LandingControl\Services
 */
class IpGeoCityService
{

    use Sortable, Paginatable, Groupable;

    /** @var IpGeoCityEloquentRepository */
    protected $repository;

    /**
     * @param IpGeoCityRepositoryInterface $repository
     */
    public function __construct(IpGeoCityRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [],
                'total' => 0,
                'groups' => []
            ];
        }

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'city') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new CityCriteria($filter['value']);
                        $this->repository->pushCriteria(new CityCriteria($filter['value']));
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            foreach ($metaData['group'] as $key => $value) {
                if ($value['field'] === 'domain') {
                    $metaData['group'][$key]['field'] = 'domain_id';
                }
            }
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            //$groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            //$groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

//        if ($perPage = $this->getPerPage($metaData)) {
//            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
//        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [],
            'total' => $total,
            'groups' => $groups_array
        ];
    }
}
