<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use LandingControl\Models\HTMLTemplate;
use LandingControl\Repositories\Criteria\HTMLTemplate\NameCriteria;
use LandingControl\Repositories\Criteria\HTMLTemplate\StatusCriteria;
use LandingControl\Repositories\Criteria\HTMLTemplate\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\HTMLTemplateEloquentRepository;
use LandingControl\Repositories\Interfaces\HTMLTemplateRepositoryInterface;

/**
 * Class HTMLTemplateService
 * @package LandingControl\Services
 */
class HTMLTemplateService
{

    use Sortable, Paginatable, Groupable;

    /** @var HTMLTemplateEloquentRepository */
    protected $repository;

    /**
     * @param HTMLTemplateRepositoryInterface $repository
     */
    public function __construct(HTMLTemplateRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(new NameCriteria($filter['value']));
                    }
                }
                if ($filter['field'] === 'status') {
                    if ($filter['operator'] === 'eq') {
                        $criteriaList[] = new StatusCriteria($filter['value']);
                        $this->repository->pushCriteria(new StatusCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * @param $all
     * @return mixed
     */
    public function createHTMLTemplate($all)
    {
        $params = [
            'name' => $all['name'],
            'path' => $all['path']
        ];

        $nonRequiredParams = [
            'description',
            'google_analytics',
            'ya_metrika'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        $template = $this->repository->create($params);
        if ($template && !empty($all['tags'])) {
            $tags = explode(',', $all['tags']);
            $template->retag($tags);
        }
    }


    /**
     * @param array $all
     * @return mixed
     */
    public function updateHTMLTemplate(array $all)
    {
        $params = [
            'name' => $all['name'],
            'path' => $all['path']
        ];

        $nonRequiredParams = [
            'description',
            'google_analytics',
            'ya_metrika'
        ];

        $template = $this->repository->find($all['id']);
        if (!empty($all['tags'])) {
            $tags = explode(',', $all['tags']);
            $template->retag($tags);
        } else {
            $template->untag();
        }

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->update($params, $all['id']);
    }
}
