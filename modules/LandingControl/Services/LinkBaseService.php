<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use LandingControl\Models\LinkBase;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Criteria\LinkBase\NameCriteria;
use LandingControl\Repositories\Criteria\LinkBase\TimeCompareCriteria;
use LandingControl\Repositories\Criteria\LinkBase\TypeCriteria;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkEloquentRepository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkRepositoryInterface;
use Saas\Models\Company;

/**
 * Class LinkBaseService
 * @package LandingControl\Services
 */
class LinkBaseService
{

    use Sortable, Paginatable, Groupable;

    /** @var LinkBaseEloquentRepository */
    protected $repository;
    /**
     * @var LinkEloquentRepository
     */
    protected $linkRepository;
    /**
     * @var DomainEloquentRepository
     */
    private $domainRepository;

    /**
     * @param LinkBaseRepositoryInterface $repository
     * @param LinkRepositoryInterface $linkRepository
     * @param DomainRepositoryInterface $domainRepositoryInterface
     */
    public function __construct(
        LinkBaseRepositoryInterface $repository,
        LinkRepositoryInterface $linkRepository,
        DomainRepositoryInterface $domainRepositoryInterface
    )
    {
        $this->repository = $repository;
        $this->linkRepository = $linkRepository;
        $this->domainRepository = $domainRepositoryInterface;
    }

    /**
     * @param array $metaData
     * @param $type
     * @return Collection
     */
    public function activeRepository(array $metaData, $type)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        $criteriaList[] = new TypeCriteria(intval($type));
        $this->repository->pushCriteria(new TypeCriteria(intval($type)));

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(new NameCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'type') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new TypeCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        $this->repository->getWithDomain();
        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * @param $all
     * @param $type
     * @return mixed
     */
    public function createLinkBase($all, $type)
    {
        if (!in_array($type, LinkBaseReference::types())) {
            abort(422);
        }

        $params = [
            'name' => $all['name'],
            'type' => $type,
            'domain_id' => $all['domain_id'],
            'open_url' => $all['open_url'],
        ];

        $nonRequiredParams = [
            'description',
            'url',
            'path'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return $this->repository->create($params);
    }

    /**
     * @param $type
     * @return null
     */
    public function createTempEmptyLinkBase($type)
    {
        if (!in_array($type, LinkBaseReference::types())) {
            return null;
        }

        $params = [
            'name' => '',
            'type' => $type,
            'domain_id' => 0,
            'open_url' => false,
            'description' => ''
        ];

        /** @var LinkBase $temp */
        $temp = $this->repository->create($params);
        $id = $temp->id;
        $temp->delete();
        return $temp;
    }

    /**
     * @param $all
     * @param $type
     * @return mixed
     */
    public function createTemplate($all, $type)
    {
        if (!in_array($type, LinkBaseReference::types())) {
            abort(422);
        }

        $params = [
            'name' => $all['name'],
            'domain_id' => $all['domain_id'],
            'path' => $all['path'],
            'url' => $all['url'],
            'type' => $type
        ];

        $nonRequiredParams = [
            'description',
            'ya_metrika',
            'google_analytics'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }


        /** @var LinkBase $template */
        $template = $this->repository->create($params);

        if ($template && !empty($all['tags'])) {
            $tags = explode(',', $all['tags']);
            $template->retag($tags);
        }

        // create link
        $domain = $this->domainRepository->find($template->domain_id);
        $linkParams = [
            'name' => $template->name,
            'link' => $domain->name . '/' . $template->url,
            'type' => LinkBaseReference::TYPE_TEMPLATE,
            'status' => LinkBaseReference::STATUS_ACTIVE,
            'hosts' => 0,
            'url' => $template->url,
            'hits' => 0,
            'count_leads' => 0,
            'domain_id' => $domain->id,
            'link_base_id' => $template->id,
            'path' => $template->path
        ];
        $link = $this->linkRepository->create($linkParams);

        $template->link_ids = json_encode([$link->id]);
        $template->save();
        return $template;
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function updateLinkBase(array $all)
    {
        $nonRequiredParams = [
            'name',
            'domain_id',
            'open_url',
            'description',
            'url',
            'path',
            'link_ids'
        ];
        $params=[];
        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                if (is_array($all[$paramName])) {
                    $all[$paramName] = json_encode($all[$paramName]);
                }
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->update($params, $all['id']);
    }

    /**
     * Update temporary entity
     *
     * @param array $all
     * @return mixed
     */
    public function updateTmpLinkBase(array $all, $restore = false)
    {
        $nonRequiredParams = [
            'name',
            'domain_id',
            'open_url',
            'description',
            'url',
            'path',
            'link_ids'
        ];
        $params=[];
        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                if (is_array($all[$paramName])) {
                    $all[$paramName] = json_encode($all[$paramName]);
                }
                $params[$paramName] = $all[$paramName];
            }
        }

        /** @var LinkBase $temp */
        $temp = LinkBase::onlyTrashed()->find($all['id']);
        $temp->update($params);
        if ($restore) {
            $temp->restore();
        }
        return  $temp;
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function updateTemplate(array $all)
    {
        $params = [
            'name' => $all['name'],
            'domain_id' => $all['domain_id'],
            'path' => $all['path'],
            'url' => $all['url']
        ];

        $nonRequiredParams = [
            'description',
            'ya_metrika',
            'google_analytics'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        $template = $this->repository->find($all['id']);
        if (!empty($all['tags'])) {
            $tags = explode(',', $all['tags']);
            $template->retag($tags);
        } else {
            $template->untag();
        }

        return $this->repository->update($params, $all['id']);
    }

    /**
     * @param array $all
     * @param Company $company
     * @return bool
     */
    public function attacheGeoDirection(array $all, Company $company)
    {
        /** @var LinkBase $callback_geosplit */
        $callback_geosplit = LinkBase::withTrashed()->find(intval($all['callback_en_id']));
        /** @var LinkBase $link_base */
        $link_base = $this->repository->find(intval($all['link_base_id']));
        if ($link_base->type === intval($all['type']) && $callback_geosplit) {
            $callback_geosplit->link_ids = json_encode($link_base->link_ids);
            $cities_idx = [];
            foreach ($all['cities'] as $key => $city_idx) {
                $cities_idx[$city_idx] = $link_base->link_ids;
            }
            $current_cities = json_decode($callback_geosplit->cities_idxs, true);
            $direct_key = Str::random(8);

            $new_cities['name'] = $all['name'];
            $new_cities['cities'] = $cities_idx;
            if (isset($all['description'])) {
                $new_cities['description'] = $all['description'];
            }
            $current_cities[$direct_key] = $new_cities;

            $callback_geosplit->cities_idxs = json_encode($current_cities);
            $callback_geosplit->save();

            $redirectUrl = '/'.$company->name.'/landingControl/geo-split/';
            if ($callback_geosplit->deleted_at === null) {
                $redirectUrl .= 'edit/' . $callback_geosplit->id;
            } else {
                $redirectUrl .= 'create?tmp=' . $callback_geosplit->id;
            }
            return [
                'callback_en' => $callback_geosplit,
                'redirect' => $redirectUrl
            ];
        }
        abort(422);
    }

    /**
     * @param $all
     */
    public function removeDirection($all)
    {
        $link_id = intval($all['link_id']);
        $en_id = $all['en_id'];
        $key = $all['key'];

        /** @var LinkBase $geo_split */
        $geo_split = LinkBase::withTrashed()->find(intval($en_id));
        $city_list = json_decode($geo_split->cities_idxs, true);

        if(isset($city_list[$key])) {
            foreach($city_list[$key]['cities'] as $city_idx => $links) {
                foreach($links as $link_key=>$link) {
                    if(intval($link) === $link_id) {
                        unset($city_list[$key]['cities'][$city_idx][$link_key]);
                    }
                }
            }
        }

        $geo_split->cities_idxs = json_encode($city_list);
        $geo_split->save();
    }
}
