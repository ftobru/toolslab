<?php namespace LandingControl\Services;

use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use LandingControl\Repositories\Criteria\Domain\NameCriteria;
use LandingControl\Repositories\Criteria\Domain\StatusCriteria;
use LandingControl\Repositories\Criteria\Domain\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;

/**
 * Class DomainService
 * @package LandingControl\Services
 */
class DomainService
{

    use Sortable, Paginatable;

    /** @var DomainEloquentRepository */
    protected $repository;

    /**
     * @param DomainRepositoryInterface $repository
     */
    public function __construct(DomainRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            if (isset($metaData['filter']['name'])) {
                $this->repository->pushCriteria(new NameCriteria($metaData['filter']['name']));
            }
            if (isset($metaData['filter']['status'])) {
                $this->repository->pushCriteria(new StatusCriteria($metaData['filter']['status']));
            }
            if (isset($metaData['filter']['company_id'])) {
                $this->repository->pushCriteria(new CompanyIdCriteria($metaData['filter']['company_id']));
            }
        }
        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginate($perPage);
        }
        return $this->repository->all();
    }
}
