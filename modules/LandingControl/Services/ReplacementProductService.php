<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use LandingControl\Repositories\Criteria\ReplacementProduct\KeyCriteria;
use LandingControl\Repositories\Criteria\ReplacementProduct\NameCriteria;
use LandingControl\Repositories\Criteria\ReplacementProduct\ProductIdCriteria;
use LandingControl\Repositories\Criteria\ReplacementProduct\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\ReplacementProductEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementProductRepositoryInterface;

/**
 * Class ReplacementProductService
 * @package LandingControl\Services
 */
class ReplacementProductService
{

    use Sortable, Paginatable, Groupable;

    /** @var ReplacementProductEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementProductRepositoryInterface $repository
     */
    public function __construct(ReplacementProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];
        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $criteriaList[] = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $criteriaList[] = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(
                            new NameCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'key') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new KeyCriteria($filter['value']);
                        $this->repository->pushCriteria(
                            new KeyCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'product_id') {
                    if ($filter['operator'] === 'eq') {
                        $criteriaList[] = new ProductIdCriteria($filter['value']);
                        $this->repository->pushCriteria(
                            new ProductIdCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        $this->repository->getWith(['product']);
        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            foreach ($metaData['group'] as $key => $value) {
                if ($value['field'] === 'product') {
                    $metaData['group'][$key]['field'] = 'product_id';
                }
            }

            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }


        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total
        ];
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function createReplacementProduct(array $all)
    {
        $params = [
            'name' => $all['name'],
            'product_id' => $all['product_id'],
            'key' => $all['key']
        ];

        $nonRequiredParams = [
            'description'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->create($params);
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function updateReplacementProduct(array $all)
    {
        $params = [
            'name' => $all['name'],
            'product_id' => $all['product_id'],
            'key' => $all['key']
        ];

        $nonRequiredParams = [
            'description'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return $this->repository->update($params, $all['id']);
    }
}
