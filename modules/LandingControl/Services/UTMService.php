<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use LandingControl\Repositories\Criteria\UTM\DescriptionCriteria;
use LandingControl\Repositories\Criteria\UTM\NameCriteria;
use LandingControl\Repositories\Criteria\UTM\TimeCompareCriteria;
use LandingControl\Repositories\Criteria\UTM\UTMCompaignCriteria;
use LandingControl\Repositories\Criteria\UTM\UTMContentCriteria;
use LandingControl\Repositories\Criteria\UTM\UTMMediumCriteria;
use LandingControl\Repositories\Criteria\UTM\UTMSourceCriteria;
use LandingControl\Repositories\Criteria\UTM\UTMTermCriteria;
use LandingControl\Repositories\Eloquent\UTMEloquentRepository;
use LandingControl\Repositories\Interfaces\UTMRepositoryInterface;

/**
 * Class UTMService
 * @package LandingControl\Services
 */
class UTMService
{

    use Sortable, Paginatable, Groupable;

    /** @var UTMEloquentRepository */
    protected $repository;

    /**
     * @param UTMRepositoryInterface $repository
     */
    public function __construct(UTMRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(new NameCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'description') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new DescriptionCriteria($filter['value']);
                        $this->repository->pushCriteria(new DescriptionCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'utm_source') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new UTMSourceCriteria($filter['value']);
                        $this->repository->pushCriteria(new UTMSourceCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'utm_compaign') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new UTMCompaignCriteria($filter['value']);
                        $this->repository->pushCriteria(new UTMCompaignCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'utm_medium') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new UTMMediumCriteria($filter['value']);
                        $this->repository->pushCriteria(new UTMMediumCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'utm_term') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new UTMTermCriteria($filter['value']);
                        $this->repository->pushCriteria(new UTMTermCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'utm_content') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new UTMContentCriteria($filter['value']);
                        $this->repository->pushCriteria(new UTMContentCriteria($filter['value']));
                    }
                }

                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        //$this->repository->all();
        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1)) - 1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * @param $all
     * @return mixed
     */
    public function createUTM($all)
    {
        $params = [
            'name' => $all['name'],
        ];

        $nonRequiredParams = [
            'description',
            'utm_source',
            'utm_compaign',
            'utm_medium',
            'utm_term',
            'utm_content'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return $this->repository->create($params);
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function updateUTM(array $all)
    {
        $params = [
            'name' => $all['name']
        ];

        $nonRequiredParams = [
            'description',
            'utm_source',
            'utm_compaign',
            'utm_medium',
            'utm_term',
            'utm_content'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return $this->repository->update($params, $all['id']);
    }
}
