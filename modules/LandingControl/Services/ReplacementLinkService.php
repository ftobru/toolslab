<?php namespace LandingControl\Services;

use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use LandingControl\Repositories\Criteria\ReplacementLink\CodeCriteria;
use LandingControl\Repositories\Criteria\ReplacementLink\NameCriteria;
use LandingControl\Repositories\Criteria\ReplacementLink\ParamCriteria;
use LandingControl\Repositories\Criteria\ReplacementLink\CompanyIdCriteria;
use LandingControl\Repositories\Criteria\ReplacementLink\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\ReplacementLinkEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface;

/**
 * Class ReplacementLinkService
 * @package LandingControl\Services
 */
class ReplacementLinkService
{

    use Sortable, Paginatable;

    /** @var ReplacementLinkEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementLinkRepositoryInterface $repository
     */
    public function __construct(ReplacementLinkRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return static
     */
    public function store(array $data)
    {
        $this->prepareData($data);
        return $this->repository->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $this->prepareData($data);
        return $this->repository->update($data, $id);
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new NameCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'code') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new CodeCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'param') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new ParamCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems ->count();

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total
        ];
    }

    /**
     * @param $data
     */
    private function prepareData(&$data)
    {
            $json = json_encode($data['key_value']);
            $data['key_value'] = $json;
    }
}
