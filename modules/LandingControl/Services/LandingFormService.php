<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use LandingControl\Reference\LandingFormReference;
use LandingControl\Repositories\Criteria\LandingForm\CompanyIdCriteria;
use LandingControl\Repositories\Criteria\LandingForm\IdAttributeCriteria;
use LandingControl\Repositories\Criteria\LandingForm\NameCriteria;
use LandingControl\Repositories\Criteria\LandingForm\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\LandingFormEloquentRepository;
use LandingControl\Repositories\Interfaces\LandingFormRepositoryInterface;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Repositories\Interfaces\FormRepositoryInterface;

/**
 * Class LandingFormService
 * @package LandingControl\Services
 */
class LandingFormService
{

    use Sortable, Paginatable, Groupable;

    /** @var LandingFormEloquentRepository */
    protected $repository;
    /**
     * @var FormEloquentRepository
     */
    protected $formRepository;

    /**
     * @param LandingFormRepositoryInterface $repository
     * @param FormRepositoryInterface $formRepository
     */
    public function __construct(
        LandingFormRepositoryInterface $repository,
        FormRepositoryInterface $formRepository
    ) {
        $this->repository = $repository;
        $this->formRepository = $formRepository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }
        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }
        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(new NameCriteria($filter['value']));
                    }
                }
                if ($filter['field'] === 'company_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new CompanyIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field']=== 'id_attr') {
                    if ($filter['operator'] === 'startWith') {
                        $newCriteria = new IdAttributeCriteria($filter['value']);
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }


        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            foreach ($metaData['group'] as $key => $value) {
                if ($value['field'] === 'product') {
                    $metaData['group'][$key]['field'] = 'product_id';
                }
                if ($value['field'] === 'landing') {
                    $metaData['group'][$key]['field'] = 'landing_id';
                }
                if ($value['field'] === 'template') {
                    $metaData['group'][$key]['field'] = 'template_id';
                }
            }

            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $data = $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)))->all();
        } else {
            $data = $this->repository->all()->toArray();
        }

        return [
            'data' => $data, //$this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * Создание новой формы для лендинга.
     *
     * @param array $data
     * @return mixed
     */
    public function createForm(array $data)
    {
        $types = LandingFormReference::types();
        $callbacks = LandingFormReference::callbacks();
        if (in_array($data['form_type'], $types) && in_array($data['callback_type'], $callbacks)) {
            $fields = json_decode($data['fields'], true);
            foreach ($fields as $key => $field) {
                $add_field = $this->formRepository->find(intval($field['field_id']))->toArray();
                $fields[$key]['base'] = $add_field;
            }
            $data['fields'] = json_encode($fields);
            return $this->repository->create($data);
        }
        abort(422);
    }

    /**
     *  Редактирование формы для лендинга.
     *
     * @param array $data
     * @param $id
     * @return mixed
     */
    public function updateForm(array $data, $id)
    {
        $types = LandingFormReference::types();
        $callbacks = LandingFormReference::callbacks();
        if (in_array($data['form_type'], $types) && in_array($data['callback_type'], $callbacks)) {
            $fields = json_decode($data['fields'], true);
            foreach ($fields as $key => $field) {
                $add_field = $this->formRepository->find(intval($field['field_id']))->toArray();
                $fields[$key]['base'] = $add_field;
            }
            $data['fields'] = json_encode($fields);
            return $this->repository->update($data, intval($data['id']));
        }
        abort(422);
    }

    /**
     *  Удаление формы.
     *
     * @param $id
     */
    public function deleteForm($id)
    {

    }
}
