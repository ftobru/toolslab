<?php namespace LandingControl\Services;

use Core\Repositories\Eloquent\Criteria\JsonbFilterCriteria;
use Core\Repositories\Eloquent\Criteria\JsonbSortCriteria;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use LandingControl\Repositories\Criteria\ReplacementGeo\AddressCriteria;
use LandingControl\Repositories\Criteria\ReplacementGeo\PhoneCriteria;
use LandingControl\Repositories\Criteria\ReplacementGeo\CompanyIdCriteria;
use LandingControl\Repositories\Criteria\ReplacementGeo\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\ReplacementGeoEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementGeoRepositoryInterface;

/**
 * Class ReplacementGeoService
 * @package LandingControl\Services
 */
class ReplacementGeoService
{

    use Sortable, Paginatable;

    /** @var ReplacementGeoEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementGeoRepositoryInterface $repository
     */
    public function __construct(ReplacementGeoRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return static
     */
    public function store(array $data)
    {
        $this->prepareData($data);
        return $this->repository->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $this->prepareData($data);
        return $this->repository->update($data, $id);
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {

        $fullList = $this->repository->all();


        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);


        if (isset($metaData['sort'])) {
            $jsonbCriteriaParams = [];
            foreach ($metaData['sort'] as $key => $sort) {
                // Json sort
                if ($sort['field'] === 'city.city' ||
                    $sort['field'] === 'city.city_a' ||
                    $sort['field'] === 'city.city_e'
                ) {
                    $jsonbCriteriaParams[] = $sort;
                    unset($metaData['sort'][$key]);
                }
            }
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
            foreach ($jsonbCriteriaParams as $sort) {
                $this->addJsonbSortCriteria($sort);
            }
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $criteriaParam) {
                if ($criteriaParam['field'] === 'created_at') {
                    if ($criteriaParam['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $criteriaParam['value'], '>=')
                        );
                    }
                    if ($criteriaParam['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $criteriaParam['value'], '<=')
                        );
                    }
                }
                if ($criteriaParam['field'] === 'phone') {
                    if ($criteriaParam['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new PhoneCriteria($criteriaParam['value'])
                        );
                    }
                }
                if ($criteriaParam['field'] === 'address') {
                    if ($criteriaParam['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new AddressCriteria($criteriaParam['value'])
                        );
                    }
                }
                if ($criteriaParam['field'] === 'city') {
                    if ($criteriaParam['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new JsonbFilterCriteria('city', 'city', 'LIKE', '%' . $criteriaParam['value'] . '%')
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems ->count();

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total
        ];
    }

    /**
     * @param $data
     */
    private function prepareData(&$data)
    {
        $json = json_encode($data['city']);
        $data['city'] = $json;
    }

    /**
     * @param $jsonbCriteriaParams
     * @param $sort
     */
    private function addJsonbSort(&$jsonbCriteriaParams, $sort)
    {
        $jsonPath = explode('.', $sort['field']);

        $operator = 'LIKE';
        $value  = '%%';

        $jsonbCriteriaParams[$sort['field']] = [
            "column" => $jsonPath[0],
            "field" => $jsonPath[1],
            'order' => $sort['dir'],
            'value' => $value,
            'operator' => $operator
        ];
    }

    /**
     * @param $sort
     */
    private function addJsonbSortCriteria($sort)
    {
        $jsonPath = explode('.', $sort['field']);
        $this->repository->pushCriteria(
            new JsonbSortCriteria($jsonPath[0], $jsonPath[1], $sort['dir'])
        );
    }

}
