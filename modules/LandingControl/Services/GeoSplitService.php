<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Illuminate\Support\Collection;
use LandingControl\Repositories\Criteria\GeoSplit\DomainIdCriteria;
use LandingControl\Repositories\Criteria\GeoSplit\NameCriteria;
use LandingControl\Repositories\Criteria\GeoSplit\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\GeoSplitEloquentRepository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use LandingControl\Repositories\Interfaces\GeoSplitRepositoryInterface;

/**
 * Class GeoSplitService
 * @package LandingControl\Services
 */
class GeoSplitService
{

    use Sortable, Paginatable, Groupable;

    /** @var GeoSplitEloquentRepository */
    protected $repository;

    /**
     * @param GeoSplitRepositoryInterface $repository
     */
    public function __construct(GeoSplitRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(new NameCriteria($filter['value']));
                    }
                }
                if ($filter['field'] === 'domain_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new DomainIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        $this->repository->getWithDomain();
        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            foreach ($metaData['group'] as $key => $value) {
                if ($value['field'] === 'domain') {
                    $metaData['group'][$key]['field'] = 'domain_id';
                }
            }
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * @param $all
     * @return mixed
     */
    public function createSplit($all)
    {
        $params = [
            'name' => $all['name'],
            'domain_id' => $all['domain_id'],
        ];

        $nonRequiredParams = [
            'description',
            'url',
            'open_url'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->create($params);
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function updateSplit(array $all)
    {
        $params = [
            'name' => $all['name'],
            'domain_id' => $all['domain_id'],
        ];

        $nonRequiredParams = [
            'description',
            'url',
            'open_url'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->update($params, $all['id']);
    }


}
