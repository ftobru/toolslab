<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use LandingControl\Repositories\Criteria\ReplacementParam\TagCriteria;
use LandingControl\Repositories\Criteria\ReplacementParam\NameCriteria;
use LandingControl\Repositories\Criteria\ReplacementParam\MeaningCriteria;
use LandingControl\Repositories\Criteria\ReplacementParam\CompanyIdCriteria;
use LandingControl\Repositories\Criteria\ReplacementParam\TimeCompareCriteria;
use LandingControl\Repositories\Eloquent\ReplacementParamEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementParamRepositoryInterface;

/**
 * Class ReplacementParamService
 * @package LandingControl\Services
 */
class ReplacementParamService
{

    use Sortable, Paginatable, Groupable;

    /** @var ReplacementParamEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementParamRepositoryInterface $repository
     */
    public function __construct(ReplacementParamRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new NameCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'tag') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new TagCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'meaning') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new MeaningCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * Создает объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function createFromModels($models, $companyId)
    {
        $result = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                $newClient = $this->repository->create($this->kendoModelToArray($model, $companyId));
                array_push($result, $newClient);
            }
        }
        return $result;
    }

    /**
     * Обновляет объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function updateFromModels($models, $companyId)
    {
        $result = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                if (isset($model['id'])) {
                    $newClient = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                    array_push($result, $newClient);
                }
            }
        }
        return $result;
    }

    /**
     * Создает объект из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function createFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            $result = $this->repository->create($this->kendoModelToArray($model, $companyId));
            return $result;
        }
    }

    /**
     * Update объекта из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function updateFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            if (isset($model['id'])) {
                $result = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                return $result;
            }
        }
    }

    /**
     * @param $model
     * @return bool
     */
    private function checkKendoModel($model)
    {
        return
            isset($model['name']) &&
            isset($model['tag']) &&
            isset($model['meaning']);
    }

    /**
     * @param $model
     * @param $companyId
     * @return array
     */
    private function kendoModelToArray($model, $companyId)
    {
        return [
            'name' => $model['name'],
            'tag' => $model['tag'],
            'meaning' => $model['meaning'],
            'company_id' => intval($companyId)
        ];
    }
}
