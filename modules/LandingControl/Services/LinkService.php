<?php namespace LandingControl\Services;

use Core\Traits\Groupable;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use LandingControl\Models\Link;
use LandingControl\Models\LinkBase;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Criteria\ABSplit\DomainIdCriteria;
use LandingControl\Repositories\Criteria\ABSplit\NameCriteria;
use LandingControl\Repositories\Criteria\ABSplit\TimeCompareCriteria;
use LandingControl\Repositories\Criteria\Link\ShortLinkCriteria;
use LandingControl\Repositories\Criteria\Link\TagCriteria;
use LandingControl\Repositories\Eloquent\AbSplitEloquentRepository;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;
use LandingControl\Repositories\Eloquent\InsertLinkEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkEloquentRepository;
use LandingControl\Repositories\Eloquent\ReplacementLinkEloquentRepository;
use LandingControl\Repositories\Eloquent\ReplacementProductEloquentRepository;
use LandingControl\Repositories\Eloquent\UTMEloquentRepository;
use LandingControl\Repositories\Interfaces\AbSplitRepositoryInterface;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use LandingControl\Repositories\Interfaces\InsertLinkRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkRepositoryInterface;
use LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface;
use LandingControl\Repositories\Interfaces\ReplacementProductRepositoryInterface;
use LandingControl\Repositories\Interfaces\UTMRepositoryInterface;

/**
 * Class LinkService
 * @package LandingControl\Services
 */
class LinkService
{

    use Sortable, Paginatable, Groupable;

    /** @var LinkEloquentRepository */
    protected $repository;

    /** @var DomainEloquentRepository  */
    protected $domainRepository;
    /**
     * @var LinkBaseEloquentRepository
     */
    protected $linkBaseRepository;
    /**
     * @var UTMEloquentRepository
     */
    protected $UTMRepository;
    /**
     * @var ReplacementLinkEloquentRepository
     */
    protected $replacementLinkRepository;
    /**
     * @var ReplacementProductEloquentRepository
     */
    protected $replacementProductRepository;
    /**
     * @var InsertLinkEloquentRepository
     */
    private $insertLinkRepository;

    /**
     * @param LinkRepositoryInterface $repository
     * @param LinkBaseRepositoryInterface $linkBaseRepository
     * @param DomainRepositoryInterface $domainRepository
     * @param ReplacementLinkRepositoryInterface $linkRepository
     * @param ReplacementProductRepositoryInterface $replacementProductRepository
     * @param InsertLinkRepositoryInterface $insertLinkRepository
     * @param UTMRepositoryInterface $UTMRepository
     */
    public function __construct(
        LinkRepositoryInterface $repository,
        LinkBaseRepositoryInterface $linkBaseRepository,
        DomainRepositoryInterface $domainRepository,
        ReplacementLinkRepositoryInterface $linkRepository,
        ReplacementProductRepositoryInterface $replacementProductRepository,
        InsertLinkRepositoryInterface $insertLinkRepository,
        UTMRepositoryInterface $UTMRepository
    ) {
        $this->repository = $repository;
        $this->domainRepository = $domainRepository;
        $this->linkBaseRepository = $linkBaseRepository;
        $this->UTMRepository = $UTMRepository;


        $this->replacementLinkRepository = $linkRepository;
        $this->replacementProductRepository = $replacementProductRepository;
        $this->insertLinkRepository = $insertLinkRepository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new NameCriteria($filter['value']);
                        $this->repository->pushCriteria(new NameCriteria($filter['value']));
                    }
                }
                if ($filter['field'] === 'tag') {
                    if ($filter['operator'] === 'startWith') {
                        $criteriaList[] = new TagCriteria($filter['value']);
                        $this->repository->pushCriteria(new TagCriteria($filter['value']));
                    }
                }
                if ($filter['field'] === 'domain_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new DomainIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        $this->repository->getWith(['domain', 'linkBase']);
        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            foreach ($metaData['group'] as $key => $value) {
                if ($value['field'] === 'product') {
                    $metaData['group'][$key]['field'] = 'product_id';
                }
                if ($value['field'] === 'landing') {
                    $metaData['group'][$key]['field'] = 'landing_id';
                }
                if ($value['field'] === 'template') {
                    $metaData['group'][$key]['field'] = 'template_id';
                }
            }

            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $data = $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)))->all();
        } else {
            $data = $this->repository->all()->toArray();
        }

        return [
            'data' => $data, //$this->repository->all()->toArray(),
            'filters_data' => [
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * Конструктор ссылок ajax
     *
     * @param $all
     * @param $company_name
     * @return mixed
     */
    public function constructLink($all, $company_name)
    {

        /** @var LinkBase $callback_en */
        $callback_en = null;
        $c_links = [];
        if (isset($all['callback_en_id'])) {
            $callback_en = LinkBase::withTrashed()->where('id', intval($all['callback_en_id']))->get()->first();
            $c_links = $callback_en->link_ids;
        }

        $types = LinkBaseReference::types();
        if (in_array($all['type'], $types)) {
            $baseLink = $this->linkBaseRepository->find($all['link_base_id']);
            $tag = $all['tag'];
            $type = $all['type'];
            $domain = $this->domainRepository->find($baseLink->domain_id);

            $link_id_list = $baseLink->link_ids;

            $new_links = [];
            foreach ($link_id_list as $link_id) {
                $existed_link = $this->repository->find($link_id);

                /** @var Link $new_link */
                $new_link = $existed_link->replicate();
                $new_link->type = $type;
                $new_link->tag = $tag;
                $new_link->hosts = 0;
                $new_link->hits = 0;
                $new_link->count_leads = 0;
                $new_link->convert = 0;

                do {
                    $short_link = $domain->name . '/' . Str::random(8);
                    $result = $this->repository->pushCriteria(new ShortLinkCriteria($short_link))->all()->toArray();
                    $new_link->short_link =$short_link;
                } while (!empty($result));
                $new_link->save();
                $isFirstParam = true;
                if (isset($all['replacement_link_id'])) {
                    $id = intval($all['replacement_link_id']);
                    $replace = $this->replacementLinkRepository->find($id);
                    if ($replace) {
                        $new_link->replacement_link = json_encode([$id]);
                        $keys = array_keys(json_decode($replace->key_value, true));
                        if (!empty($keys)) {
                            $prefix = $isFirstParam ? '?' : '&';
                            $new_link->link .= $prefix;
                            $isFirstParam = false;
                            $new_link->link .= 'replacement_links_' . $replace->param . '=' . $keys[0];
                        }
                    }
                }
                if (isset($all['replacement_product_id'])) {
                    $id = intval($all['replacement_product_id']);
                    $replace = $this->replacementProductRepository->find($id);
                    if ($replace) {
                        $prefix = $isFirstParam ? '?' : '&';
                        $new_link->link .= $prefix;
                        $isFirstParam = false;
                        $new_link->link .= 'prod=' . $replace->key;
                    }
                }
                if (isset($all['insert_link_id'])) {
                    $id = intval($all['insert_link_id']);
                    $replace = $this->insertLinkRepository->find($id);
                    if ($replace) {
                        $new_link->insert_link = json_encode([$id]);
                        $keys = array_keys(json_decode($replace->key_value, true));
                        if (!empty($keys)) {
                            $prefix = $isFirstParam ? '?' : '&';
                            $new_link->link .= $prefix;
                            $isFirstParam = false;
                            $new_link->link .= 'insert_links_' . $replace->param . '=' . $keys[0];
                        }
                    }
                }
                if (isset($all['utm_id'])) {
                    $new_link->utm = json_encode([intval($all['utm_id'])]);
                    $prefix = $isFirstParam ? '?' : '&';
                    $new_link->link .= $prefix;
                    $isFirstParam = false;
                    $new_link->link .= 'utm_id' . '=' . $all['utm_id'];
                }

//                $prefix = $isFirstParam ? '?' : '&';
//                $new_link->link .= $prefix;
//                $isFirstParam = false;
//                $new_link->link .= 'link_id=' . $new_link->id;

                $same_links = DB::table('links')->select()->where('link', 'LIKE', $new_link->link . '%')->get();
                if (!empty($same_links)) {
                    $next =  true;
                    foreach ($same_links as $link) {
                        if($next) {
                            $parts = parse_url($link->link);
                            parse_str($parts['query'], $query);
                            $checked_params = $query;

                            $parts = parse_url($new_link->link);
                            parse_str($parts['query'], $query);
                            $new_params = $query;

                            $arr1 = array_diff($checked_params, $new_params);
                            if(sizeof($arr1) == 1 && isset($arr1['link_id'])){
                                //Похож на линк
                                $arr2 = array_diff($new_params, $checked_params);
                                if(empty($arr2)) {
                                    // Точно этот линк
                                    $same_link = $this->repository->find($link->id);
                                    $new_link->forceDelete();
                                    if ($callback_en) {
                                        $c_links[] = $same_link->id;
                                    }
                                    $new_links[] = $same_link;
                                    $next = false;
                                }
                            }
                        }
                    }
                } else {
                    $new_link->save();
                    if ($callback_en) {
                        $c_links[] = $new_link->id;
                    }
                    $new_links[] = $new_link;
                }
            }

            $response = ['links'=>$new_links];
            if ($callback_en) {
                $callback_en->link_ids = json_encode(array_unique($c_links));
                $callback_en->save();

                $url_en_type = '';
                switch($callback_en->type) {
                    case LinkBaseReference::TYPE_AB_SPLIT:
                        $url_en_type = 'ab-split';
                        break;
                    case LinkBaseReference::TYPE_GEO_SPLIT:
                        $url_en_type = 'geo-split';
                        break;
                }
                $operation = 'create';
                if ($callback_en->deleted_at === null) {
                    $operation = 'edit';
                }

                $redirect_link = '/' . $company_name . '/landingControl/' . $url_en_type . '/' . $operation;
                if ($operation === 'create') {
                    $redirect_link .= '?tmp=' . $callback_en->id;
                } else {
                    $redirect_link .= '/' . $callback_en->id;
                }
                $response['redirect_link'] = $redirect_link;
            }
            return $response;
        }
        abort(422);
    }

    /**
     * @param $all
     * @return mixed
     */
    public function createLink($all)
    {
        $params = [
            'name' => $all['name'],
            'domain_id' => $all['domain_id'],
            'url' => $all['url']
        ];

        $nonRequiredParams = [
            'link',
            'product_id',
            'template_id',
            'short_link',
            'status',
            'hosts',
            'hits',
            'count_leads',
            'convert',
            'replacement_product',
            'replacement_time',
            'replacement_param',
            'replacement_geo',
            'replacement_link',
            'insert_link'
        ];

        $domain = $this->domainRepository->find($all['domain_id']);
        $params['link'] = $domain->name . $all['url'];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->create($params);
    }

    /**
     * @param array $all
     * @return mixed
     */
    public function updateLink(array $all)
    {
        $params = [
            'name' => $all['name'],
            'link' => $all['link'],
            'product_id' => $all['product_id'],
        ];

        $nonRequiredParams = [
            'template_id',
            'short_link',
            'status',
            'hosts',
            'hits',
            'count_leads',
            'convert',
            'replacement_product',
            'replacement_time',
            'replacement_param',
            'replacement_geo',
            'replacement_link',
            'insert_link'
        ];

        foreach ($nonRequiredParams as $paramName) {
            if (isset($all[$paramName])) {
                $params[$paramName] = $all[$paramName];
            }
        }

        return  $this->repository->update($params, $all['id']);
    }


}
