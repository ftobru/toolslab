<?php namespace LandingControl\Http\Middleware;

use Saas\References\UserReference;
use Saas\Models\User;
use Saas\Services\UserService;
use Auth;
use AccessService;
use Closure;
use Lang;
use Illuminate\Http\Request as Request;
use Response;

/**
 * Class LandingControlPermissionMiddleware
 * @package LandingControl\Http\Middleware
 */
class LandingControlPermissionMiddleware
{
    /** @var UserService  */
    protected $service;

    protected $actions = [
        // Create
        'links', 'templates', 'forms', 'ab-test', 'geo-split', 'file-manager', 'domains',
        // Multylanding
        'time-replacement', 'product-replacement', 'link-replacement', 'geo-replacement', 'link-insert', 'variables',
    ];

    protected $components = [
        'create',
        'multylanding',
    ];

    protected $modulePrefix = 'landingControl';

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param Request $request
     * @param callable $next
     * @param $moduleComponent
     * @param $action
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $moduleComponent, $action)
    {
        if (in_array($moduleComponent, $this->components) && in_array($action, $this->actions)) {
            $permission_name = $this->modulePrefix . '.' . $moduleComponent . '.' . $action;
            if (AccessService::canPermission($permission_name) ||
                $this->checkPermGrades($permission_name)
            ) {
                return $next($request);
            }
        }
        abort(403, 'Недостаточно прав для просмотра страницы');
    }

    /**
     * Проверка вариаций прав.
     *
     * @param $permission_name
     * @return bool
     */
    private function checkPermGrades($permission_name)
    {
        $permissionGrades = Lang::get('permissions-options', [], 'ru');
        $postfix_values = [];
        foreach ($permissionGrades as $key => $val) {
            if (isset($val['postfix'])) {
                $postfix_values[] = $val['postfix'];
            }
        }

        foreach ($postfix_values as $grade) {
            if (AccessService::canPermission($permission_name . $grade)) {
                return true;
            }
        }
        return false;
    }
}
