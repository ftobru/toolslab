<?php namespace LandingControl\Http\Controllers\App;

use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxCreateProductRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\App\GetLinkRequest;
use LandingControl\Http\Controllers\Requests\InsertLink\GetInsertLinkByIDRequest;
use LandingControl\Models\InsertLink;
use LandingControl\Models\ReplacementTime;
use LandingControl\Repositories\Criteria\InsertLink\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\InsertLinkEloquentRepository;
use LandingControl\Repositories\Eloquent\ReplacementTimeEloquentRepository;
use LandingControl\Repositories\Interfaces\InsertLinkRepositoryInterface;
use LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface;
use LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface;
use LandingControl\Services\InsertLinkService;
use LandingControl\Services\ReplacementLinkService;
use LandingControl\Services\ReplacementTimeService;
use Response;
use Validator;
use Input;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class InsertLinkController
 * @package LandingControl\Http\Controllers\App
 */
class InsertLinkController extends Controller
{
    /** @var InsertLinkEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var InsertLinkService  */
    protected $insertLinkService;

    /**
     * @param InsertLinkEloquentRepository $repositoryInterface
     * @param CompanyService $companyService
     * @param InsertLinkService $insertLinkService
     */
    public function __construct(
        InsertLinkEloquentRepository $repositoryInterface,
        CompanyService $companyService,
        InsertLinkService $insertLinkService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->insertLinkService = $insertLinkService;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.insert-link.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $ins_link_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $link = $this->repository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($ins_link_id);
            if ($link) {
                $key = '';
                $value = '';
                if (!empty($link->key_value)) {
                    $encoded = json_decode($link->key_value, true);
                    foreach ($encoded as $k=>$v) {
                        $key = $k;
                        $value = $v;
                    }
                }
                return view('landings.insert-link.edit', [
                    'company' => $company,
                    'link' => $link,
                    'link_key' => $key,
                    'link_value' => $value
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * POST на редактирование ссылки.
     *
     * @param $company_alias
     * @param $ins_link_id
     * @return \Illuminate\View\View
     */
    public function postEdit($company_alias, $ins_link_id)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'code' => 'required|string|string',
            'param' => 'required|string|max:255',
            'key' => 'required|string|max:255',
            'value' => 'required|string|max:255'
        ];

        $params = Input::all();
        $validator = Validator::make($params, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/landingControl/insert-link/edit/' . $ins_link_id)->withErrors($messages);
        } else {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if ($c->id === $company->id) {
                        $key_value = json_encode([$params['key'] => $params['value']]);
                        $this->repository->update([
                            'name' => $params['name'],
                            'code' => $params['code'],
                            'param' => $params['param'],
                            'key_value' => $key_value
                        ], $ins_link_id);

                        return Redirect::to($company_alias . '/landingControl/insert-link/edit/' . $ins_link_id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            return Redirect::to('/404');
        }
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.insert-link.create', ['company' => $company]);
        }
        return Redirect::to('/404');
    }

    /**
     * POST на редактирование ссылки.
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function postCreate($company_alias)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'code' => 'required|string|string',
            'param' => 'required|string|max:255',
            'key' => 'required|string|max:255',
            'value' => 'required|string|max:255'
        ];

        $params = Input::all();
        $validator = Validator::make($params, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/landingControl/insert-link/create')->withErrors($messages);
        } else {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if ($c->id === $company->id) {
                        $key_value = json_encode([$params['key'] => $params['value']]);
                        /** @var InsertLink $newEnyity */
                        $newEnyity = $this->repository->create([
                            'name' => $params['name'],
                            'code' => $params['code'],
                            'param' => $params['param'],
                            'key_value' => $key_value,
                            'company_id' => $company->id
                        ]);

                        return Redirect::to($company_alias . '/landingControl/insert-link/edit/' . $newEnyity->id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            return Redirect::to('/404');
        }
    }

    /**
     * Get replacement times for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getLinks(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->insertLinkService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * Get replacement link by id
     *
     * @param GetInsertLinkByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getLinkById(GetInsertLinkByIDRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->repository->find($request->get('entity_id'));
            return Response::json($result);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function delete(AjaxCreateProductRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['entity_id'])) {
                $this->repository->delete($data['entity_id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }

}
