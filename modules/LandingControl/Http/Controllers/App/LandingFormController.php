<?php
namespace LandingControl\Http\Controllers\App;

use AccessService;
use App;
use Config;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\Models\Client;
use Crm\Repositories\Eloquent\Criteria\Client\CompanyIdCriteria;
use LandingControl\Http\Controllers\Requests\LandingForm\DeleteLandingFormRequest;
use LandingControl\Http\Controllers\Requests\LandingForm\GetLandingFormsRequest;
use LandingControl\Http\Controllers\Requests\LandingForm\StoreLandingFormRequest;
use LandingControl\Http\Controllers\Requests\LandingForm\UpdateLandingFormRequest;
use LandingControl\Http\Controllers\Requests\StoreDomainRequest;
use LandingControl\Reference\DomainReference;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;
use LandingControl\Repositories\Eloquent\LandingFormEloquentRepository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use LandingControl\Repositories\Interfaces\LandingFormRepositoryInterface;
use LandingControl\Services\LandingFormService;
use Redirect;
use Response;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Repositories\Interfaces\FormRepositoryInterface;
use Saas\Services\CompanyService;
use Saas\Services\StateApplicationService;

/**
 * Class LandingFormController
 * @package LandingControl\Http\Controllers\App
 */
class LandingFormController extends Controller
{
    /** @var  LandingFormEloquentRepository */
    protected $landingFormRepository;
    /** @var CompanyService  */
    protected $companyService;
    /** @var LandingFormService  */
    protected $service;
    /**
     * @var FormEloquentRepository
     */
    protected $formRepository;

    /**
     * @param LandingFormRepositoryInterface $landingFormRepositoryInterface
     * @param LandingFormService $service
     * @param FormRepositoryInterface $formRepository
     * @param CompanyService $companyService
     */
    public function __construct(
        LandingFormRepositoryInterface $landingFormRepositoryInterface,
        LandingFormService $service,
        FormRepositoryInterface $formRepository,
        CompanyService $companyService
    ) {
        $this->landingFormRepository = $landingFormRepositoryInterface;
        $this->companyService = $companyService;
        $this->service = $service;
        $this->formRepository = $formRepository;
    }

    /**
     * @param string $company_alias
     * @return \Illuminate\View\View
     */
    public function index($company_alias)
    {
        /** @var StateApplicationService $stateService */
        $stateApp = App::make('StateApplicationService');
        if ($company = $stateApp->getCurrentCompany()) {
            $forms = $this->landingFormRepository->all();
            return view('landings.landing-forms.index', [
                'forms' => $forms,
                'company' => $company,
                'editPermission' => AccessService::canPermission('landingControl.create.forms.edit')
            ]);
        }
        abort(404);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function edit($company_alias, $form_id)
    {
        if (AccessService::canPermission('landingControl.create.forms.edit')) {
            /** @var StateApplicationService $stateService */
            $stateApp = App::make('StateApplicationService');
            if ($company = $stateApp->getCurrentCompany()) {
                $form = $this->landingFormRepository->find(intval($form_id));
                $fields = $this->formRepository
                    ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Client::class)))
                    ->pushCriteria(new CompanyIdCriteria($company->id))
                    ->all();

                foreach ($fields as $field) {
                    if ($field->form_type === 1) {
                        $field->type = 'string';
                    } else {
                        $field->type = 'string';
                    }
                }

                $tags = json_decode($form->tags, true);
                $tagArr = [
                    'task_tags' => '',
                    'order_tags' => '',
                    'client_tags' => '',
                ];
                foreach ($tags as $key => $tag) {
                    if (isset($tag['task_tags'])) {
                        $tagArr['task_tags'] = $tag['task_tags'];
                    } elseif (isset($tag['order_tags'])) {
                        $tagArr['order_tags'] = $tag['order_tags'];
                    } elseif (isset($tag['client_tags'])) {
                        $tagArr['client_tags'] = $tag['client_tags'];
                    }
                }

                return view(
                    'landings.landing-forms.edit',
                    [
                        'form' => $form,
                        'fields' => $fields,
                        'company' => $company,
                    ]
                )
                    ->with('tag', $tagArr);
            }
            abort(404);
        }
        abort(403);
    }

    /**
     * View of landing form create page.
     *
     * @return \Illuminate\View\View
     */
    public function create($company_alias)
    {
        if (AccessService::canPermission('landingControl.create.forms.edit')) {
            /** @var StateApplicationService $stateService */
            $stateApp = App::make('StateApplicationService');
            if ($company = $stateApp->getCurrentCompany()) {
                $fields = $this->formRepository
                    ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Client::class)))
                    ->pushCriteria(new CompanyIdCriteria($company->id))
                    ->all();

                foreach ($fields as $field) {
                    if ($field->form_type === 1) {
                        $field->type = 'string';
                    } else {
                        $field->type = 'string';
                    }
                }

                return view('landings.landing-forms.create', [
                    'company' => $company,
                    'fields' => $fields
                ]);
            }
            abort(404);
        }
        abort(403);
    }

    /**
     * @param StoreLandingFormRequest $request
     * @return mixed
     */
    public function postAjaxCreateLandingForm(StoreLandingFormRequest $request)
    {
        return \Response::json(['result'=>$this->service->createForm($request->all())]) ;
    }

    /**
     * @param UpdateLandingFormRequest $request
     * @return mixed
     */
    public function postAjaxUpdateLandingForm(UpdateLandingFormRequest $request)
    {
        return \Response::json(['result'=>$this->service->updateForm($request->all(), $request->get('id'))]);
    }

    /**
     * @param DeleteLandingFormRequest $request
     * @return mixed
     */
    public function postAjaxDeleteLandingForm(DeleteLandingFormRequest $request)
    {
        return Response::json([$this->landingFormRepository->delete($request->get('id'))]) ;
    }

    /**
     * @param GetLandingFormsRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLandingForms(GetLandingFormsRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->service->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }
}