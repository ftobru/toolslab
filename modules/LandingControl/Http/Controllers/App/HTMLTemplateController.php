<?php namespace LandingControl\Http\Controllers\App;

use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\DeleteABSplitRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\StoreABSplitRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\UpdateABSplitRequest;
use LandingControl\Http\Controllers\Requests\HTMLTemplate\DeleteHTMLTemplateRequest;
use LandingControl\Http\Controllers\Requests\HTMLTemplate\StoreHTMLTemplateRequest;
use LandingControl\Http\Controllers\Requests\HTMLTemplate\UpdateHTMLTemplateRequest;
use LandingControl\Models\HTMLTemplate;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Criteria\InsertLink\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\AbSplitEloquentRepository;
use LandingControl\Repositories\Eloquent\HTMLTemplateEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Interfaces\AbSplitRepositoryInterface;
use LandingControl\Repositories\Interfaces\HTMLTemplateRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Services\ABSplitService;
use LandingControl\Services\DomainService;
use LandingControl\Services\HTMLTemplateService;
use LandingControl\Services\LinkBaseService;
use Response;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class HTMLTemplateController
 * @package LandingControl\Http\Controllers\App
 */
class HTMLTemplateController extends Controller
{
    /** @var HTMLTemplateEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var HTMLTemplateService  */
    protected $service;

    /**
     * @param HTMLTemplateRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param HTMLTemplateService $service
     */
    public function __construct(
        HTMLTemplateRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        HTMLTemplateService $service
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->service = $service;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.html-template.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $html_template_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $template = $this->repository->find($html_template_id);
            if ($template) {
                return view('landings.html-template.edit', [
                    'company' => $company,
                    'template' => $template,
                    'tags' => implode(',', $template->tagNames())
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.html-template.create', [
                'company' => $company
            ]);
        }
        return Redirect::to('/404');
    }

    /**
     * Get AbSplits for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getHTMLTemplates(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->service->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreHTMLTemplateRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createHTMLTemplate(StoreHTMLTemplateRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([
                $this->service->createHTMLTemplate($request->all())
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateHTMLTemplateRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateHTMLTemplate(UpdateHTMLTemplateRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->service->updateHTMLTemplate($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteHTMLTemplateRequest  $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteHTMLTemplate(DeleteHTMLTemplateRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['id'])) {
                $this->repository->delete(intval($data['id']));
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }

}
