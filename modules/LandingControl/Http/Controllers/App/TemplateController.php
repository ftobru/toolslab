<?php namespace LandingControl\Http\Controllers\App;

use AccessService;
use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\Template\DeleteTemplateRequest;
use LandingControl\Http\Controllers\Requests\Template\GetTemplateByIDRequest;
use LandingControl\Http\Controllers\Requests\Template\GetTemplatesRequest;
use LandingControl\Http\Controllers\Requests\Template\StoreTemplateRequest;
use LandingControl\Http\Controllers\Requests\Template\UpdateTemplateRequest;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Services\DomainService;
use LandingControl\Services\LinkBaseService;
use Response;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class TemplateController
 * @package LandingControl\Http\Controllers\App
 */
class TemplateController extends Controller
{
    /** @var LinkBaseEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var LinkBaseService  */
    protected $templateService;

    /**
     * @param LinkBaseRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param LinkBaseService $templateService
     * @param DomainService $domainService
     */
    public function __construct(
        LinkBaseRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        LinkBaseService $templateService,
        DomainService $domainService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->templateService = $templateService;
        $this->domainService = $domainService;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.template.index', [
                'company' => $company,
                'editPermission' => AccessService::canPermission('landingControl.create.templates.edit')
            ]);
        }
        abort(404, 'Company not found');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $template_id
     * @return \Illuminate\View\View
     */
    public function edit($company_alias, $template_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (AccessService::canPermission('landingControl.create.templates.edit')) {
                $template = $this->repository
                    ->find($template_id);
                if ($template && $template->type === LinkBaseReference::TYPE_TEMPLATE) {
                    $status = LinkBaseReference::PATH_NOT_SET;
                    if (!empty($template->path)) {
                        $exist = \File::exists(public_path() . '/files/' . $template->path);
                        if ($exist) {
                            $status = LinkBaseReference::PATH_EXIST;
                        }
                    } else {
                        $status = LinkBaseReference::PATH_NOT_SET;
                    }
                    return view('landings.template.edit', [
                        'company' => $company,
                        'domains' => $this->domainService->activeRepository([]),
                        'template' => $template,
                        'tags' => implode(',', $template->tagNames()),
                        'status' => $status
                    ]);
                }
            } else {
                abort(403);
            }
        }
        abort(404, 'Company not found');
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if (AccessService::canPermission('landingControl.create.templates.edit')) {
            if ($company) {
                return view('landings.template.create', [
                    'company' => $company,
                    'domains' => $this->domainService->activeRepository([])
                ]);
            }
            abort(404);
        }
        abort(403);
    }

    /**
     * Get templates for grid
     *
     * @param GetTemplatesRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getTemplates(GetTemplatesRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->templateService->
            activeRepository($request->get('options', []), LinkBaseReference::TYPE_TEMPLATE);

            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', []),
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param GetTemplateByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTemplateById(GetTemplateByIDRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $this->repository->getWithDomain();
            $result = $this->repository->find($request->get('entity_id'));
            $result->domain;
            return Response::json($result->toArray());
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreTemplateRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTemplate(StoreTemplateRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([
                $this->templateService->createTemplate($request->all(), LinkBaseReference::TYPE_TEMPLATE)
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateTemplateRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTemplate(UpdateTemplateRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->templateService->updateTemplate($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteTemplateRequest  $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteTemplate(DeleteTemplateRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['id'])) {
                $this->repository->delete(intval($data['id']));
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
