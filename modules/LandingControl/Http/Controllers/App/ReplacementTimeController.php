<?php namespace LandingControl\Http\Controllers\App;

use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxCreateProductRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\App\GetLinkRequest;
use LandingControl\Models\ReplacementTime;
use LandingControl\Repositories\Criteria\ReplacementTime\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\ReplacementTimeEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface;
use LandingControl\Services\ReplacementTimeService;
use Response;
use Saas\Models\Company;
use Saas\Models\User;
use Validator;
use Input;
use Redirect;
use Saas\Services\CompanyService;

/**
 * Class ReplacementTimeController
 * @package LandingControl\Http\Controllers\App
 */
class ReplacementTimeController extends Controller
{
    /** @var ReplacementTimeEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var ReplacementTimeService  */
    protected $replacementTimeService;

    /**
     * @param ReplacementTimeRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param ReplacementTimeService $replacementTimeService
     */
    public function __construct(
        ReplacementTimeRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        ReplacementTimeService $replacementTimeService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->replacementTimeService = $replacementTimeService;
    }

    /**
     * @param GetLinkRequest $getLinkRequest
     * @return mixed
     */
    public function getLinks(GetLinkRequest $getLinkRequest)
    {
        return Response::api([
            'replacementTime' => $this->repository->findIn($getLinkRequest->get('links')),
            'currentTime' => Carbon::now()
        ]);
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.replacement-time.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $replacement_time_id
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $replacement_time_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company && (bool)intval($replacement_time_id)) {
            $time = $this->repository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($replacement_time_id);
            if ($time) {
                return view('landings.replacement-time.edit', [
                    'company' => $company,
                    'time' => $time
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * POST на редактирование ссылки.
     *
     * @param $company_alias
     * @param $replacement_time_id
     * @return \Illuminate\View\View
     */
    public function postEdit($company_alias, $replacement_time_id)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'start_time' => 'required|regex:/^[0-9]{2}:[0-9]{2}$/',
            'finish_time' => 'required|regex:/^[0-9]{2}:[0-9]{2}$/',
            'tag' => 'required|string|max:255',
            'meaning' => 'required|string|max:255'
        ];

        $params = Input::all();
        $validator = Validator::make($params, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/landingControl/replacement-time/edit/' . $replacement_time_id)
                ->withErrors($messages);
        } else {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if ($c->id === $company->id) {
                        $this->repository->update([
                            'name' => $params['name'],
                            'start_time' => $params['start_time'],
                            'finish_time' => $params['finish_time'],
                            'tag' => $params['tag'],
                            'meaning' => $params['meaning'],
                        ], $replacement_time_id);

                        return Redirect::to($company_alias . '/landingControl/replacement-time/edit/' . $replacement_time_id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            return Redirect::to('/404');
        }
    }


    /**
     * Get replacement times for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getTimes(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->replacementTimeService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createTime(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($data['models'])) {
                return Response::json([
                    'data' => $this->replacementTimeService->createFromModels($data['models'], $company->id)
                ]);
            }
            if (isset($data['model'])) {
                return Response::json([
                    'data' => $this->replacementTimeService->createFromModel($data['model'], $company->id)
                ]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateTime(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                return Response::json([
                    'data' => $this->replacementTimeService->updateFromModels($data['models'], $company->id)
                ]);
            }
            if (isset($data['model'])) {
                return Response::json([
                    'data' => $this->replacementTimeService->updateFromModel($data['model'], $company->id)
                ]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteTime(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['entity_id'])) {
                $this->repository->delete($data['entity_id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
