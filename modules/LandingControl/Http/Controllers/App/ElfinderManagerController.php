<?php namespace LandingControl\Http\Controllers\App;

use Barryvdh\Elfinder\Connector;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Application;
use Illuminate\Filesystem\FilesystemAdapter;
use Auth;
use Saas\Services\CompanyService;

class ElfinderManagerController extends Controller
{
    protected $package = 'elfinder';

    /**
     * The application instance.
     *
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    /** @var CompanyService  */
    protected $companyService;

    public function __construct(Application $app, CompanyService $companyService)
    {
        $this->app = $app;
        $this->companyService = $companyService;
    }
        
    public function showIndex($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.filemanager.index', ['company' => $company, 'company_alias' => $company_alias])
                ->with($this->getViewVars());
        }
        return \Redirect::to('/404');

//        return $this->app['view']
//            ->make($this->package . '::elfinder')
//            ->with($this->getViewVars());
    }

    public function showTinyMCE()
    {
        return $this->app['view']
            ->make($this->package . '::tinymce')
            ->with($this->getViewVars());
    }

    public function showTinyMCE4()
    {
        return $this->app['view']
            ->make($this->package . '::tinymce4')
            ->with($this->getViewVars());
    }

    public function showCKeditor4()
    {
        return $this->app['view']
            ->make($this->package . '::ckeditor4')
            ->with($this->getViewVars());
    }

    public function showPopup($company_alias, $input_id)
    {
        return $this->app['view']
            ->make($this->package . '::standalonepopup')
            ->with($this->getViewVars())
            ->with(compact('company_alias'))
            ->with(compact('input_id'));
    }

    public function showConnector($company_alias)
    {
        $roots = $this->app->config->get('elfinder.roots', []);
        if (empty($roots)) {
            $dirs = (array) $this->app['config']->get('elfinder.dir', []);
            foreach ($dirs as $dir) {
                $user = Auth::user();
                $company = $this->companyService->getByName($company_alias);

                if ($user && $company) {
                    $dir = $dir . '/' . $this->companyService->generateCompanyDirNameByAlias($company_alias);
                    $roots[] = [
                        'driver' => 'LocalFileSystem', // driver for accessing file system (REQUIRED)
                        'path' => public_path($dir), // path to files (REQUIRED)
                        'URL' => url($dir), // URL to files (REQUIRED)
                        'accessControl' => $this->app->config->get('elfinder.access') // filter callback (OPTIONAL)
                    ];
                }



            }

            $disks = (array) $this->app['config']->get('elfinder.disks', []);
            foreach ($disks as $key => $root) {
                if (is_string($root)) {
                    $key = $root;
                    $root = [];
                }
                $disk = app('filesystem')->disk($key);
                if ($disk instanceof FilesystemAdapter) {
                    $defaults = [
                        'driver' => 'Flysystem',
                        'filesystem' => $disk->getDriver(),
                        'alias' => $key,
                    ];
                    $roots[] = array_merge($defaults, $root);
                }
            }
        }

        $opts = $this->app->config->get('elfinder.options', array());
        $opts = array_merge(['roots' => $roots], $opts);

        // run elFinder
        $connector = new Connector(new \elFinder($opts));
        $connector->run();
        return $connector->getResponse();
    }

    protected function getViewVars()
    {
        $dir = 'packages/barryvdh/' . $this->package;
        $locale = $this->app->config->get('app.locale');
        if (!file_exists($this->app['path.public'] . "/$dir/js/i18n/elfinder.$locale.js")) {
            $locale = false;
        }
        $csrf = true;
        return compact('dir', 'locale', 'csrf');
    }
}
