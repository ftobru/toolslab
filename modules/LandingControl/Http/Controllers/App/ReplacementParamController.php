<?php namespace LandingControl\Http\Controllers\App;

use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\App\GetLinkRequest;
use LandingControl\Repositories\Criteria\ReplacementParam\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\ReplacementParamEloquentRepository;
use LandingControl\Repositories\Eloquent\ReplacementTimeEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface;
use LandingControl\Services\ReplacementParamService;
use Response;
use Saas\Models\Company;
use Saas\Models\User;
use Validator;
use Input;
use Redirect;
use Saas\Services\CompanyService;

/**
 * Class ReplacementParamController
 * @package LandingControl\Http\Controllers\App
 */
class ReplacementParamController extends Controller
{
    /** @var ReplacementTimeEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var ReplacementParamService  */
    protected $replacementParamService;

    /** @var ReplacementParamEloquentRepository  */
    protected $replacementParamEloquentRepository;


    /**
     * @param ReplacementTimeRepositoryInterface $repositoryInterface
     * @param ReplacementParamService $replacementParamService
     * @param ReplacementParamEloquentRepository $replacementParamEloquentRepository
     * @param CompanyService $companyService
     */
    public function __construct(
        ReplacementTimeRepositoryInterface $repositoryInterface,
        ReplacementParamService $replacementParamService,
        ReplacementParamEloquentRepository $replacementParamEloquentRepository,
        CompanyService $companyService)
    {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->replacementParamService = $replacementParamService;
        $this->replacementParamEloquentRepository = $replacementParamEloquentRepository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.replacement-param.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $replacement_param_id
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $replacement_param_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company && (bool)intval($replacement_param_id)) {
            $parameter = $this->replacementParamEloquentRepository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($replacement_param_id);
            if ($parameter) {
                return view('landings.replacement-param.edit', [
                    'company' => $company,
                    'parameter' => $parameter
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * POST на редактирование.
     *
     * @param $company_alias
     * @param $replacement_param_id
     * @return \Illuminate\View\View
     */
    public function postEdit($company_alias, $replacement_param_id)
    {
        $rules = [
            'name' => 'required|string|max:255',
            'tag' => 'required|string|max:255',
            'meaning' => 'required|string|max:255'
        ];

        $params = Input::all();
        $validator = Validator::make($params, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/landingControl/replacement-param/edit/' . $replacement_param_id)
                ->withErrors($messages);
        } else {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if ($c->id === $company->id) {
                        $this->replacementParamEloquentRepository->update([
                            'name' => $params['name'],
                            'tag' => $params['tag'],
                            'meaning' => $params['meaning'],
                        ], $replacement_param_id);

                        return Redirect::to($company_alias . '/landingControl/replacement-param/edit/' . $replacement_param_id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            return Redirect::to('/404');
        }
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getParams(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->replacementParamService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createParam(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($data['models'])) {
                return Response::json(['data' => $this->replacementParamService->createFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->replacementParamService->createFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateParam(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                return Response::json(['data' => $this->replacementParamService->updateFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->replacementParamService->updateFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteParam(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->replacementParamEloquentRepository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->replacementParamEloquentRepository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['entity_id'])) {
                $this->replacementParamEloquentRepository->delete($data['entity_id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
