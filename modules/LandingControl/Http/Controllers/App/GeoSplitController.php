<?php namespace LandingControl\Http\Controllers\App;

use AccessService;
use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\DeleteGeoSplitRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\GetGeoSplitByIDRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\GetGeoSplitsRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\RemoveDirectionRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\SaveTmpGeoSplitRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\StoreGeoSplitRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\UpdateGeoSplitRequest;
use LandingControl\Http\Controllers\Requests\GeoSplit\UpdateTmpGeoSplitRequest;
use LandingControl\Models\LinkBase;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Criteria\InsertLink\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\GeoSplitEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkEloquentRepository;
use LandingControl\Repositories\Interfaces\GeoSplitRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkRepositoryInterface;
use LandingControl\Services\DomainService;
use LandingControl\Services\GeoSplitService;
use LandingControl\Services\IpGeoCityService;
use LandingControl\Services\LinkBaseService;
use Response;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;
use URL;

/**
 * Class GeoSplitController
 * @package LandingControl\Http\Controllers\App
 */
class GeoSplitController extends Controller
{
    /** @var LinkBaseEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var LinkBaseService  */
    protected $geoSplitService;

    /** @var IpGeoCityService  */
    protected $ipGeoCityService;
    /**
     * @var LinkEloquentRepository
     */
    private $linkRepository;

    /**
     * @param LinkBaseRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param LinkBaseService $geoSplitService
     * @param IpGeoCityService $ipGeoCityService
     * @param LinkRepositoryInterface $linkRepository
     * @param DomainService $domainService
     */
    public function __construct(
        LinkBaseRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        LinkBaseService $geoSplitService,
        IpGeoCityService $ipGeoCityService,
        LinkRepositoryInterface $linkRepository,
        DomainService $domainService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->geoSplitService = $geoSplitService;
        $this->domainService = $domainService;
        $this->ipGeoCityService = $ipGeoCityService;
        $this->linkRepository = $linkRepository;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.geo-split.index', [
                'company' => $company,
                'editPermission' => AccessService::canPermission('landingControl.create.geo-split.edit')
            ]);
        }
        abort(404);
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $geo_split_id
     * @return \Illuminate\View\View
     */
    public function edit($company_alias, $geo_split_id)
    {
        if (AccessService::canPermission('landingControl.create.geo-split.edit')) {
            $company = $this->companyService->getByName($company_alias);
            if ($company) {
                $split = $this->repository
                    ->pushCriteria(new CompanyIdCriteria($company->id))
                    ->find($geo_split_id);
                $ab_splits = $this->geoSplitService->activeRepository([], LinkBaseReference::TYPE_AB_SPLIT);
                $templates = $this->geoSplitService->activeRepository([], LinkBaseReference::TYPE_TEMPLATE);

                $cities = json_decode($split->cities_idxs, true);
                if ($cities) {
                    foreach ($cities as $key => $city) {
                        foreach ($city['cities'] as $city_key => $city_ids) {
                            foreach ($city_ids as $city_id) {
                                $cities[$key]['links'][] = $links = $this->linkRepository->find($city_id)->toArray();
                            }
                        }
                    }
                }

                if ($split && $split->type === LinkBaseReference::TYPE_GEO_SPLIT) {
                    return view('landings.geo-split.edit', [
                        'company' => $company,
                        'domains' => $this->domainService->activeRepository([]),
                        'split' => $split,
                        'ab_splits' => $ab_splits,
                        'templates' => $templates,
                        'cities_directions' => $cities
                    ]);
                }
            } else {
                abort(404);
            }
        } else {
            abort(403);
        }
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        if (AccessService::canPermission('landingControl.create.geo-split.edit')) {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            if ($company) {
                /** @var LinkBase $temp */
                $temp = null;
                if (isset($_GET['tmp'])) {
                    $temp = LinkBase::onlyTrashed()->find($_GET['tmp']);
                    if ($temp === null) {
                        $temp = $this->geoSplitService->createTempEmptyLinkBase(LinkBaseReference::TYPE_GEO_SPLIT);
                        $url = URL::route('landingControl.geoSplits.create', [$company->name, 'tmp=' . $temp->id]);
                        return Redirect::to($url);
                    }

                    $cities = json_decode($temp->cities_idxs, true);
                    if ($cities) {
                        foreach ($cities as $key => $city) {
                            foreach ($city['cities'] as $city_key => $city_ids) {
                                foreach ($city_ids as $city_id) {
                                    $cities[$key]['links'][] = $links = $this->linkRepository
                                        ->find($city_id)->toArray();
                                }
                            }
                        }
                    }

                    return view('landings.geo-split.create', [
                        'company' => $company,
                        'domains' => $this->domainService->activeRepository([]),
                        'temp' => $temp,
                        'cities_directions' => $cities
                    ]);
                } else {
                    $temp = $this->geoSplitService->createTempEmptyLinkBase(LinkBaseReference::TYPE_GEO_SPLIT);
                    $url = URL::route('landingControl.geoSplits.create', [$company->name, 'tmp=' . $temp->id]);
                    return Redirect::to($url);
                }
            } else {
                abort(404);
            }
        } else {
            abort(403);
        }
    }

    /**
     * @param RemoveDirectionRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeDirect(RemoveDirectionRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->geoSplitService
                ->removeDirection($request->all());
            return Response::json($result);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * Get GeoSplits for grid
     *
     * @param GetGeoSplitsRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getGeoSplits(GetGeoSplitsRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->geoSplitService
                ->activeRepository($request->get('options', []), LinkBaseReference::TYPE_GEO_SPLIT);
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param GetGeoSplitByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGeoSplitById(GetGeoSplitByIDRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $this->repository->getWithDomain();
            $result = $this->repository->find($request->get('entity_id'));
            $result->domain;
            return Response::json($result->toArray());
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreGeoSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createGeoSplit(StoreGeoSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->geoSplitService
                ->createLinkBase($request->all(), LinkBaseReference::TYPE_GEO_SPLIT)]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateGeoSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateGeoSplit(UpdateGeoSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->geoSplitService->updateLinkBase($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateTmpGeoSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTmpGeoSplit(UpdateTmpGeoSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->geoSplitService->updateTmpLinkBase($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param SaveTmpGeoSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTmpGeoSplit(SaveTmpGeoSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->geoSplitService->updateTmpLinkBase($request->all(), true)]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteGeoSplitRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteGeoSplit(DeleteGeoSplitRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['id'])) {
                $this->repository->delete(intval($data['id']));
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
