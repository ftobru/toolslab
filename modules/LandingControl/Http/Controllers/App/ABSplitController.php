<?php namespace LandingControl\Http\Controllers\App;

use AccessService;
use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use LandingControl\Http\Controllers\Requests\ABSplit\DeleteABSplitRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\GetABSplitByIDRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\GetABSplitsRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\SaveTmpABSplitRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\StoreABSplitRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\UpdateABSplitRequest;
use LandingControl\Http\Controllers\Requests\ABSplit\UpdateTmpABSplitRequest;
use LandingControl\Models\LinkBase;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Eloquent\AbSplitEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkEloquentRepository;
use LandingControl\Repositories\Interfaces\AbSplitRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkRepositoryInterface;
use LandingControl\Services\ABSplitService;
use LandingControl\Services\DomainService;
use LandingControl\Services\LinkBaseService;
use Response;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;
use URL;

/**
 * Class ABSplitController
 * @package LandingControl\Http\Controllers\App
 */
class ABSplitController extends Controller
{
    /** @var LinkBaseEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var LinkBaseService  */
    protected $abSplitService;

    /** @var LinkEloquentRepository */
    protected $linkRepository;

    /**
     * @param LinkBaseRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param LinkBaseService $abSplitService
     * @param LinkRepositoryInterface $linkRepository
     * @param DomainService $domainService
     */
    public function __construct(
        LinkBaseRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        LinkBaseService $abSplitService,
        LinkRepositoryInterface $linkRepository,
        DomainService $domainService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->abSplitService = $abSplitService;
        $this->domainService = $domainService;
        $this->linkRepository = $linkRepository;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.ab-split.index', [
                'company' => $company,
                'editPermission' => AccessService::canPermission('landingControl.create.ab-test.edit')
            ]);
        }
        abort(404);
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $ab_split_id)
    {
        if (AccessService::canPermission('landingControl.create.ab-test.edit')) {
            $company = $this->companyService->getByName($company_alias);
            if ($company) {
                $split = $this->repository->find($ab_split_id);
                if ($split && $split->type === LinkBaseReference::TYPE_AB_SPLIT) {
                    $links = [];
                    if (!empty($split->link_ids)) {
                        $link_id_list = $split->link_ids;
                        $links = $this->linkRepository->find($link_id_list)->toArray();
                    };
                    return view('landings.ab-split.edit', [
                        'company' => $company,
                        'domains' => $this->domainService->activeRepository([]),
                        'split' => $split,
                        'links' => $links
                    ]);
                } else {
                    abort(403);
                }
            } else {
                abort(404);
            }
        }
        abort(403);
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        if (AccessService::canPermission('landingControl.create.ab-test.edit')) {
            $company = $this->companyService->getByName($company_alias);
            if ($company) {
                /** @var LinkBase $temp */
                $temp = null;
                if (isset($_GET['tmp'])) {
                    $temp = LinkBase::onlyTrashed()->find($_GET['tmp']);
                    if ($temp === null) {
                        $temp = $this->abSplitService->createTempEmptyLinkBase(LinkBaseReference::TYPE_AB_SPLIT);
                        $url = URL::route('landingControl.abSplits.create', [$company->name, 'tmp=' . $temp->id]);
                        return Redirect::to($url);
                    }
                    $links = [];
                    if (!empty($temp->link_ids)) {
                        $link_id_list = $temp->link_ids;
                        $links = $this->linkRepository->find($link_id_list)->toArray();
                    };
                    return view('landings.ab-split.create', [
                        'company' => $company,
                        'domains' => $this->domainService->activeRepository([]),
                        'temp' => $temp,
                        'links' => $links
                    ]);
                } else {
                    $temp = $this->abSplitService->createTempEmptyLinkBase(LinkBaseReference::TYPE_AB_SPLIT);
                    $url = URL::route('landingControl.abSplits.create', [$company->name, 'tmp=' . $temp->id]);
                    return Redirect::to($url);
                }
            } else {
                abort(404);
            }
        }
        abort(403);
    }

    /**
     * Get AbSplits for grid
     *
     * @param GetABSplitsRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getABSplits(GetABSplitsRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->abSplitService->
            activeRepository($request->get('options', []), LinkBaseReference::TYPE_AB_SPLIT);

            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param GetABSplitByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getABSplitById(GetABSplitByIDRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $this->repository->getWithDomain();
            $result = $this->repository->find($request->get('entity_id'));
            $result->domain;
            return Response::json($result->toArray());
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreABSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createABSplit(StoreABSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([
                $this->abSplitService->createLinkBase($request->all(), LinkBaseReference::TYPE_AB_SPLIT)
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateABSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateABSplit(UpdateABSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->abSplitService->updateLinkBase($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateTmpABSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTmpABSplit(UpdateTmpABSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->abSplitService->updateTmpLinkBase($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param SaveTmpABSplitRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveTmpABSplit(SaveTmpABSplitRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->abSplitService->updateTmpLinkBase($request->all(), true)]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteABSplitRequest  $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteABSplit(DeleteABSplitRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['id'])) {
                $this->repository->delete(intval($data['id']));
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
