<?php namespace LandingControl\Http\Controllers\App;

use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\UTM\DeleteUTMRequest;
use LandingControl\Http\Controllers\Requests\UTM\StoreUTMRequest;
use LandingControl\Http\Controllers\Requests\UTM\UpdateUTMRequest;
use LandingControl\Repositories\Criteria\UTM\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\UTMEloquentRepository;
use LandingControl\Repositories\Interfaces\UTMRepositoryInterface;
use LandingControl\Services\UTMService;
use Response;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class UTMController
 * @package LandingControl\Http\Controllers\App
 */
class UTMController extends Controller
{
    /** @var UTMEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var UTMService  */
    protected $service;

    /**
     * @param UTMRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param UTMService $utmService
     */
    public function __construct(
        UTMRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        UTMService $utmService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->service = $utmService;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.utm.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $utm_id
     * @return \Illuminate\View\View
     */
    public function edit($company_alias, $utm_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $utm = $this->repository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($utm_id);
            if ($utm) {
                return view('landings.utm.edit', [
                    'company' => $company,
                    'utm' => $utm
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.utm.create', [
                'company' => $company
            ]);
        }
        return Redirect::to('/404');
    }

    /**
     * Get utms for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getUTMs(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->service->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreUTMRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createUTM(StoreUTMRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->service->createUTM($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateUTMRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUTM(UpdateUTMRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->service->updateUTM($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteUTMRequest  $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteUTM(DeleteUTMRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['id'])) {
                $this->repository->delete($data['id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }

}
