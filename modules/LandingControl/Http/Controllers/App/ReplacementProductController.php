<?php namespace LandingControl\Http\Controllers\App;

use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Core\Services\ProductService;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxCreateProductRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\Repositories\Eloquent\ProductEloquentRepository;
use Crm\Repositories\Interfaces\ProductRepositoryInterface;
use LandingControl\Http\Controllers\Requests\App\GetLinkRequest;
use LandingControl\Http\Controllers\Requests\ReplacementProduct\DeleteReplacementProductRequest;
use LandingControl\Http\Controllers\Requests\ReplacementProduct\GetReplacementProductByIDRequest;
use LandingControl\Http\Controllers\Requests\ReplacementProduct\StoreReplacementProductRequest;
use LandingControl\Http\Controllers\Requests\ReplacementProduct\UpdateReplacementProductRequest;
use LandingControl\Models\ReplacementLink;
use LandingControl\Models\ReplacementTime;
use LandingControl\Repositories\Criteria\ReplacementLink\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\ReplacementLinkEloquentRepository;
use LandingControl\Repositories\Eloquent\ReplacementProductEloquentRepository;
use LandingControl\Repositories\Eloquent\ReplacementTimeEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface;
use LandingControl\Repositories\Interfaces\ReplacementProductRepositoryInterface;
use LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface;
use LandingControl\Services\ReplacementLinkService;
use LandingControl\Services\ReplacementProductService;
use LandingControl\Services\ReplacementTimeService;
use Response;
use Redirect;
use Validator;
use Input;
use Saas\Services\CompanyService;

/**
 * Class ReplacementProductController
 * @package LandingControl\Http\Controllers\App
 */
class ReplacementProductController extends Controller
{
    /** @var ReplacementProductEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var ReplacementProductService  */
    protected $replacementProductService;

    /** @var  ProductEloquentRepository */
    protected $productRepository;

    /**
     * @param ReplacementProductEloquentRepository $repositoryInterface
     * @param CompanyService $companyService
     * @param ProductRepositoryInterface $productRepository
     * @param ReplacementProductService $service
     */
    public function __construct(
        ReplacementProductEloquentRepository $repositoryInterface,
        CompanyService $companyService,
        ProductRepositoryInterface $productRepository,
        ReplacementProductService $service
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->replacementProductService = $service;
        $this->productRepository = $productRepository;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.replacement-product.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $replacement_product_id
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $replacement_product_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $product_replace = $this->repository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($replacement_product_id);
            if ($product_replace) {
                $product = $this->productRepository->find($product_replace->product_id);
                return view('landings.replacement-product.edit', [
                    'company' => $company,
                    'product_replace' => $product_replace,
                    'product' => $product,
                    'products' =>$this->productRepository->all()
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.replacement-product.create', [
                'company' => $company,
                'products' =>$this->productRepository->all()
            ]);
        }
        return Redirect::to('/404');
    }

    /**
     * Get replacement times for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getProducts(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->replacementProductService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * Get replacement times for grid
     *
     * @param GetReplacementProductByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getProductById(GetReplacementProductByIDRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->repository->find($request->get('entity_id'));
            return Response::json($result);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreReplacementProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createReplacementProduct(StoreReplacementProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->replacementProductService->createReplacementProduct($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateReplacementProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateReplacementProduct(UpdateReplacementProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->replacementProductService->updateReplacementProduct($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function delete(DeleteReplacementProductRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['entity_id'])) {
                $this->repository->delete($data['entity_id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
