<?php namespace LandingControl\Http\Controllers\App;

use AccessService;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Http\Controllers\Requests\Link\AttacheGeoDirectionRequest;
use LandingControl\Http\Controllers\Requests\Link\ConstructLinkRequest;
use LandingControl\Http\Controllers\Requests\Link\DeleteLinkRequest;
use LandingControl\Http\Controllers\Requests\Link\GetLinkByIDRequest;
use LandingControl\Http\Controllers\Requests\Link\StoreLinkRequest;
use LandingControl\Http\Controllers\Requests\Link\UpdateLinkRequest;
use LandingControl\Models\LinkBase;
use LandingControl\Models\ReplacementProduct;
use LandingControl\Reference\LinkBaseReference;
use LandingControl\Repositories\Criteria\InsertLink\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\LinkBaseEloquentRepository;
use LandingControl\Repositories\Eloquent\LinkEloquentRepository;
use LandingControl\Repositories\Interfaces\LinkBaseRepositoryInterface;
use LandingControl\Repositories\Interfaces\LinkRepositoryInterface;
use LandingControl\Services\ABSplitService;
use LandingControl\Services\GeoSplitService;
use LandingControl\Services\HTMLTemplateService;
use LandingControl\Services\InsertLinkService;
use LandingControl\Services\IpGeoCityService;
use LandingControl\Services\LandingFormService;
use LandingControl\Services\LinkBaseService;
use LandingControl\Services\LinkService;
use LandingControl\Services\ReplacementLinkService;
use LandingControl\Services\ReplacementProductService;
use LandingControl\Services\UTMService;
use Response;
use Redirect;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class LinkController
 * @package LandingControl\Http\Controllers\App
 */
class LinkController extends Controller
{
    /** @var LinkEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var LinkService  */
    protected $linkService;

    /** @var GeoSplitService */
    protected $geoSplitService;

    /** @var ABSplitService  */
    protected $abSplitService;

    /** @var IpGeoCityService  */
    protected $ipGeoCityService;

    /** @var  LinkBaseService */
    protected $linkBaseService;
    /**
     * @var ReplacementProductService
     */
    protected $replacementProductService;
    /**
     * @var ReplacementLinkService
     */
    protected $replacementLinkService;
    /**
     * @var InsertLinkService
     */
    protected $insertLinkService;
    /**
     * @var UTMService
     */
    protected $UTMService;
    /**
     * @var HTMLTemplateService
     */
    protected $HTMLTemplateService;
    /**
     * @var LandingFormService
     */
    protected $landingFormService;
    /**
     * @var LinkBaseEloquentRepository
     */
    protected $linkBaseRepository;

    /**
     * @param LinkRepositoryInterface $repositoryInterface
     * @param CompanyService $companyService
     * @param LinkService $linkService
     * @param GeoSplitService $geoSplitService
     * @param IpGeoCityService $ipGeoCityService
     * @param LinkBaseService $linkBaseService
     * @param LinkBaseRepositoryInterface $linkBaseRepository
     * @param ReplacementProductService $replacementProductService
     * @param ReplacementLinkService $replacementLinkService
     * @param InsertLinkService $insertLinkService
     * @param UTMService $UTMService
     * @param HTMLTemplateService $HTMLTemplateService
     * @param LandingFormService $landingFormService
     * @param ABSplitService $ABSplitService
     */
    public function __construct(
        LinkRepositoryInterface $repositoryInterface,
        CompanyService $companyService,
        LinkService $linkService,
        GeoSplitService $geoSplitService,
        IpGeoCityService $ipGeoCityService,
        LinkBaseService $linkBaseService,
        LinkBaseRepositoryInterface $linkBaseRepository,
        ReplacementProductService $replacementProductService,
        ReplacementLinkService $replacementLinkService,
        InsertLinkService $insertLinkService,
        UTMService $UTMService,
        HTMLTemplateService $HTMLTemplateService,
        LandingFormService $landingFormService,
        ABSplitService $ABSplitService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->linkService = $linkService;
        $this->geoSplitService = $geoSplitService;
        $this->abSplitService = $ABSplitService;
        $this->ipGeoCityService = $ipGeoCityService;
        $this->linkBaseService = $linkBaseService;
        $this->replacementProductService = $replacementProductService;
        $this->replacementLinkService = $replacementLinkService;
        $this->insertLinkService = $insertLinkService;
        $this->UTMService = $UTMService;
        $this->HTMLTemplateService = $HTMLTemplateService;
        $this->landingFormService = $landingFormService;
        $this->linkBaseRepository = $linkBaseRepository;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        $editPermission = false;
        if (AccessService::canPermission('landingControl.create.links.edit')) {
            $editPermission = true;
        }

        if ($company) {
            return view('landings.link.index', [
                'company' => $company,
                'editPermission' => $editPermission
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $link_id
     * @return \Illuminate\View\View
     */
    public function edit($company_alias, $link_id)
    {
        if (!AccessService::canPermission('landingControl.create.links.edit')) {
            abort(403, 'Недостаточно прав');
        }

        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $split = $this->repository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($link_id);
            if ($split) {
                return view('landings.link.edit', [
                    'company' => $company,
                ]);
            }
        }
        abort(404, 'Компания не существует');
    }

    /**
     * Constructor
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        if (!AccessService::canPermission('landingControl.create.links.edit')) {
            abort(403, 'Недостаточно прав');
        }

        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $viewParams = [
                'company' => $company,
                'templates' => $this->linkBaseService->activeRepository([], LinkBaseReference::TYPE_TEMPLATE)['data'],
                'ab_tests' => $this->linkBaseService->activeRepository([], LinkBaseReference::TYPE_AB_SPLIT)['data'],
                'geo_splits' => $this->linkBaseService->activeRepository([], LinkBaseReference::TYPE_GEO_SPLIT)['data'],
                'replacement_product' => $this->replacementProductService->activeRepository([])['data'],
                'replacement_link' => $this->replacementLinkService->activeRepository([])['data'],
                'insert_link' => $this->insertLinkService->activeRepository([])['data'],
                'utm' => $this->UTMService->activeRepository([])['data'],
                'HTMLs' => $this->HTMLTemplateService->activeRepository([])['data'],
                'forms' => $this->landingFormService->activeRepository([])['data'],
                'callback_en_id' => null
            ];

            if (isset($_GET['tmp'])) {
                $viewParams['callback_en_id'] = intval($_GET['tmp']);
            }

            return view('landings.link.constructor', $viewParams);
        }
        return Redirect::to('/404');
    }

    /**\
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function multiplier($company_alias)
    {
        if (!AccessService::canPermission('landingControl.create.links.edit')) {
            abort(403, 'Недостаточно прав');
        }

        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.link.multiplier', [
                'company' => $company
            ]);
        }
        return Redirect::to('/404');
    }

    /**
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function geodirect($company_alias)
    {
        if (!AccessService::canPermission('landingControl.create.links.edit')) {
            abort(403, 'Недостаточно прав');
        }

        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($_GET['tmp'])) {
                /** @var LinkBase $callback_en */
                $callback_en = LinkBase::withTrashed()->where('id', intval($_GET['tmp']))->get()->first();
                $cities = $this->ipGeoCityService
                    ->activeRepository(['group' => [['field' => 'region', 'dir' => 'asc']]]);
                $ab_splits = $this->linkBaseService->activeRepository([], LinkBaseReference::TYPE_AB_SPLIT);
                $templates = $this->linkBaseService->activeRepository([], LinkBaseReference::TYPE_TEMPLATE);
                if ($callback_en) {
                    return view('landings.link.geodirect', [
                        'company' => $company,
                        'cities' => $cities,
                        'ab_splits' => $ab_splits,
                        'templates' => $templates,
                        'temp' => $callback_en
                    ]);
                }
            }
        }
        abort(404, 'Недостаточно прав');
    }

    /**
     * @param AttacheGeoDirectionRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function attacheGeoDirection(AttacheGeoDirectionRequest $request, $company_alias)
    {
        if (!AccessService::canPermission('landingControl.create.links.edit')) {
            abort(403, 'Недостаточно прав');
        }

        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->linkBaseService->attacheGeoDirection($request->all(), $company);
            return Response::json($result);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * Get links for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getLinks(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->linkService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * Получить линки с id из массива id-ов
     *
     * @param GetLinkByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getByID(GetLinkByIDRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->repository->find($request->get('id'));
            return Response::json($result);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param StoreLinkRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function createLink(StoreLinkRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->linkService->createLink($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param ConstructLinkRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function constructLink(ConstructLinkRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->linkService->constructLink($request->all(), $company_alias)]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateLinkRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateLink(UpdateLinkRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return Response::json([$this->linkService->updateLink($request->all())]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteLinkRequest  $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteLink(DeleteLinkRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->repository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->repository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['id'])) {
                $this->repository->delete($data['id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }

}
