<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:21
 */


namespace LandingControl\Http\Controllers\App;


use App;
use Core\Http\Controllers\Controller;
use LandingControl\Http\Controllers\Requests\StoreDomainRequest;
use LandingControl\Reference\DomainReference;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use Saas\Services\StateApplicationService;

class DomainController extends Controller
{
    /** @var  DomainEloquentRepository */
    protected $domainRepository;

    /**
     * @param DomainRepositoryInterface $domainRepositoryInterface
     */
    public function __construct(DomainRepositoryInterface $domainRepositoryInterface)
    {
        $this->domainRepository = $domainRepositoryInterface;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $domains = $this->domainRepository->all();
        return view('landings.domains.index', [
            'domains' => $domains,
            'company' => App::make('StateApplicationService')->getCurrentCompany()
        ]);
    }

    /**
     * @param StoreDomainRequest $request
     * @return mixed
     */
    public function postAjaxCreateSubDomain(StoreDomainRequest $request)
    {

        return $this->domainRepository->create([
            'name' => $request->get('name'),
            'status' => DomainReference::STATUS_NO_ACTIVE,
            'company_id' => App::make('StateApplicationService')->getCurrentCompany()->id
        ]);
    }

    /**
     * @param StoreDomainRequest $request
     * @return mixed
     */
    public function postAjaxCreateDomain(StoreDomainRequest $request)
    {
        return $this->domainRepository->create([
            'name' => $request->get('name'),
            'status' => DomainReference::STATUS_NO_ACTIVE,
            'company_id' => App::make('StateApplicationService')->getCurrentCompany()->id
        ]);
    }
}