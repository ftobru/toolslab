<?php namespace LandingControl\Http\Controllers\App;

use Carbon\Carbon;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use LandingControl\Models\ReplacementGeo;
use LandingControl\Repositories\Criteria\ReplacementGeo\CompanyIdCriteria;
use LandingControl\Repositories\Eloquent\ReplacementGeoEloquentRepository;
use LandingControl\Services\ReplacementGeoService;
use Response;
use Redirect;
use Validator;
use Input;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Services\CompanyService;

/**
 * Class ReplacementGeoController
 * @package LandingControl\Http\Controllers\App
 */
class ReplacementGeoController extends Controller
{
    /** @var ReplacementGeoEloquentRepository */
    protected $repository;

    /** @var CompanyService  */
    protected $companyService;

    /** @var ReplacementGeoService */
    protected $replacementGeoService;

    /**
     * @param ReplacementGeoEloquentRepository $repositoryInterface
     * @param CompanyService $companyService
     * @param ReplacementGeoService $replacementGeoService
     */
    public function __construct(
        ReplacementGeoEloquentRepository $repositoryInterface,
        CompanyService $companyService,
        ReplacementGeoService $replacementGeoService
    ) {
        $this->repository = $repositoryInterface;
        $this->companyService = $companyService;
        $this->replacementGeoService = $replacementGeoService;
    }

    /**
     * Grid
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.replacement-geo.index', [
                'company' => $company,
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * Edit
     *
     * @param $company_alias
     * @param $replacement_geo_id
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function edit($company_alias, $replacement_geo_id)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $geo = $this->repository
                ->pushCriteria(new CompanyIdCriteria($company->id))
                ->find($replacement_geo_id);
            if ($geo) {
                return view('landings.replacement-geo.edit', [
                    'company' => $company,
                    'geo' => $geo
                ]);
            }
        }
        return Redirect::to('/404');
    }

    /**
     * POST на редактирование.  TODO: доделать
     *
     * @param $company_alias
     * @param $replacement_geo_id
     * @return \Illuminate\View\View
     */
    public function postEdit($company_alias, $replacement_geo_id)
    {
        $rules = [
            'city' => 'required|string|string',
            'city_e' => 'string|string',
            'city_a' => 'string|string',
            'phone' => 'string|max:255',
            'address' => 'string|max:255',
        ];

        $params = Input::all();
        $validator = Validator::make($params, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/landingControl/replacement-geo/edit/' . $replacement_geo_id)->withErrors($messages);
        } else {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if ($c->id === $company->id) {
                        $city = json_encode([
                            'city' => array_get($params, 'city', ''),
                            'city_a' => array_get($params, 'city_a', ''),
                            'city_e' => array_get($params, 'city_e', ''),
                        ]);

                        $this->repository->update([
                            'city' => $city,
                            'phone' => array_get($params, 'phone', ''),
                            'address' => array_get($params, 'address', ''),
                            'company_id' => $company->id
                        ], $replacement_geo_id);

                        return Redirect::to($company_alias . '/landingControl/replacement-geo/edit/' . $replacement_geo_id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            return Redirect::to('/404');
        }
    }

    /**
     * Create
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function create($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('landings.replacement-geo.create', ['company' => $company]);
        }
        return Redirect::to('/404');
    }

    /**
     * POST на coздание TODO: доделать
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function postCreate($company_alias)
    {
        $rules = [
            'city' => 'required|string|string',
            'city_e' => 'string|string',
            'city_a' => 'string|string',
            'phone' => 'string|max:255',
            'address' => 'string|max:255',
        ];

        $params = Input::all();
        $validator = Validator::make($params, $rules);

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/landingControl/replacement-geo/create')->withErrors($messages);
        } else {
            /** @var Company $company */
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if ($c->id === $company->id) {
                        $city = json_encode([
                            'city' => array_get($params, 'city', ''),
                            'city_a' => array_get($params, 'city_a', ''),
                            'city_e' => array_get($params, 'city_e', ''),
                        ]);

                        /** @var ReplacementGeo $newEnyity */
                        $newEnyity = $this->repository->create([
                            'city' => $city,
                            'phone' => array_get($params, 'phone', ''),
                            'address' => array_get($params, 'address', ''),
                            'company_id' => $company->id
                        ]);

                        return Redirect::to($company_alias . '/landingControl/replacement-geo/edit/' . $newEnyity->id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            return Redirect::to('/404');
        }
    }

    /**
     * Get replacement times for grid
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getGeoReplacements(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->replacementGeoService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

}
