<?php namespace LandingControl\Http\Controllers\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateInsertLinkRequest
 * @package LandingControl\Http\Controllers\Requests\Rest
 */
class UpdateInsertLinkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'code' => 'required|max:255',
            'param' => 'required|max:255',
            'key_value' => 'array',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }
}
