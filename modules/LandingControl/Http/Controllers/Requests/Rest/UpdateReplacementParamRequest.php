<?php namespace LandingControl\Http\Controllers\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateReplacementParamRequest
 * @package LandingControl\Http\Controllers\Requests\Rest
 */
class UpdateReplacementParamRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'tag' => 'required|max:255',
            'meaning' => 'required|max:255',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }
}
