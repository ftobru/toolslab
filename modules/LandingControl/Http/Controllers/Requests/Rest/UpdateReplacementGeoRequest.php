<?php namespace LandingControl\Http\Controllers\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateReplacementGeoRequest
 * @package LandingControl\Http\Controllers\Requests\Rest
 */
class UpdateReplacementGeoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|max:255',
            'city' => 'array',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }
}
