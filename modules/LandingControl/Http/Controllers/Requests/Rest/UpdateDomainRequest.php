<?php namespace LandingControl\Http\Controllers\Requests\Rest;

use Core\Http\Requests\Request;
use LandingControl\Reference\DomainReference;

/**
 * Class UpdateDomainRequest
 * @package LandingControl\Http\Controllers\Requests\Rest
 */
class UpdateDomainRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|domainName|url',
            'status' => 'required|integer|domainStatus',
            'company_id' => 'required|integer|exists:companies,id',
        ];
    }

    /**
     * @return \Illuminate\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();

        $validator->addImplicitExtension('domainStatus', function ($attribute, $value, $parameters) {
            if (is_int($value) && in_array($value, DomainReference::statuses())) {
                return true;
            }
            return false;
        });

        $validator->addImplicitExtension('domainName', function ($attribute, $value, $parameters) {
            if (is_string($value)) {
                // Check all domain name rules
                $result = (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $value) //chars check
                    && preg_match("/^.{1,253}$/", $value) //overall length check
                    && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $value));
                return $result;
            }
            return false;
        });

        return $validator;
    }
}
