<?php

namespace LandingControl\Http\Controllers\Requests\ReplacementLink;

use Core\Http\Requests\Request;

/**
 * Class GetReplacementLinkByIDRequest
 * @package LandingControl\Http\Controllers\Requests\ReplacementLink
 */
class GetReplacementLinkByIDRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity_id' => 'required|exists:replacement_links,id'
        ];
    }

}