<?php

namespace LandingControl\Http\Controllers\Requests\GeoSplit;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class RemoveDirectionRequest
 * @package LandingControl\Http\Controllers\Requests\GeoSplit
 */
class RemoveDirectionRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.geo-split.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en_id' => 'required|exists:link_bases,id',
            'key' => 'required|string|max:8',
            'link_id' => 'required|numeric|exists:links,id',
        ];
    }

}