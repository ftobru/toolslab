<?php

namespace LandingControl\Http\Controllers\Requests\App;


use Core\Http\Requests\Request;

class GetLinkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return ['links' => 'required|array'];
    }

}