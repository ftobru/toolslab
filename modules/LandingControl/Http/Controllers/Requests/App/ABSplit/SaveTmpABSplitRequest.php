<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:32
 */

namespace LandingControl\Http\Controllers\Requests\ABSplit;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class SaveTmpABSplitRequest
 * @package LandingControl\Http\Controllers\Requests\ABSplit
 */
class SaveTmpABSplitRequest extends Request
{

    public function __construct()
    {
        parent::__construct();
        \Input::merge(['open_url' => \Input::get('open_url') === 'true'? true: false]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.ab-test.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:link_bases,id',
            'name' => 'required|string|max:255',
            'domain_id' => 'required|numeric|exists:domains,id',
            'description' => 'string|max:255',
            'url' => 'required|string|max:255',
            'open_url' => 'required|boolean',
            'link_ids' => 'array'
        ];
    }

}