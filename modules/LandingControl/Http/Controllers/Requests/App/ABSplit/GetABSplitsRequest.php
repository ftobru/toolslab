<?php

namespace LandingControl\Http\Controllers\Requests\ABSplit;

use Core\Http\Requests\Request;

/**
 * Class GetABSplitsRequest
 * @package LandingControl\Http\Controllers\Requests\ABSplit
 */
class GetABSplitsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'options' => 'required|array',
            'options.take' => 'required|integer',
            'options.skip' => 'required|integer',
            'options.page' => 'required|integer',
            'options.pageSize' => 'required|integer'
        ];
    }

}