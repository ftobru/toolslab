<?php

namespace LandingControl\Http\Controllers\Requests\Template;

use Core\Http\Requests\Request;

/**
 * Class GetTemplateByIDRequest
 * @package LandingControl\Http\Controllers\Requests\Template
 */
class GetTemplateByIDRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity_id' => 'required|exists:link_bases,id'
        ];
    }

}