<?php

namespace LandingControl\Http\Controllers\Requests\Template;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class UpdateTemplateRequest
 * @package LandingControl\Http\Controllers\Requests\Template
 */
class UpdateTemplateRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return  AccessService::canPermission('landingControl.create.templates.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:link_bases,id',
            'name' => 'required|string|max:255',
            'domain_id' => 'required|numeric|exists:domains,id',
            'description' => 'string|max:255',
            'url' => 'string|max:255',
            'path' => 'required|string|max:1024',
            'tags' => 'string|max:256',
            'google_analytics' => 'string|max:1024',
            'ya_metrika' => 'string|max:1024'
        ];
    }
}
