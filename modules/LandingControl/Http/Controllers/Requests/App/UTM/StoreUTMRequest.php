<?php

namespace LandingControl\Http\Controllers\Requests\UTM;

use Core\Http\Requests\Request;

/**
 * Class StoreUTMRequest
 * @package LandingControl\Http\Controllers\Requests\UTM
 */
class StoreUTMRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'utm_source' => 'string|max:255',
            'utm_compaign' => 'string|max:255',
            'utm_medium' => 'string|max:255',
            'utm_term' => 'string|max:255',
            'utm_content' => 'string|max:255'
        ];
    }
}
