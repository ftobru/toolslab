<?php

namespace LandingControl\Http\Controllers\Requests\ReplacementProduct;

use Core\Http\Requests\Request;

/**
 * Class GetReplacementProductByIDRequest
 * @package LandingControl\Http\Controllers\Requests\ReplacementProduct
 */
class GetReplacementProductByIDRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity_id' => 'required|exists:replacement_products,id'
        ];
    }

}