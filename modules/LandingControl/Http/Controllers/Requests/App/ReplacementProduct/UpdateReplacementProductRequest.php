<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:32
 */

namespace LandingControl\Http\Controllers\Requests\ReplacementProduct;

use Core\Http\Requests\Request;

/**
 * Class UpdateReplacementProductRequest
 * @package LandingControl\Http\Controllers\Requests\ReplacementProduct
 */
class UpdateReplacementProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'key' => 'required|string|max:255',
            'product_id' => 'required|exists:products,id'
        ];
    }

}