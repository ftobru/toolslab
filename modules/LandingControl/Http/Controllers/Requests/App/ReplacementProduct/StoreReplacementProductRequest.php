<?php

namespace LandingControl\Http\Controllers\Requests\ReplacementProduct;

use Core\Http\Requests\Request;

/**
 * Class StoreReplacementProductRequest
 * @package LandingControl\Http\Controllers\Requests\ReplacementProduct
 */
class StoreReplacementProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'string|max:255',
            'key' => 'required|string|max:255',
            'product_id' => 'required|exists:products,id'
        ];
    }
}
