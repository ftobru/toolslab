<?php

namespace LandingControl\Http\Controllers\Requests\HTMLTemplate;

use Core\Http\Requests\Request;

/**
 * Class StoreHTMLTemplateRequest
 * @package LandingControl\Http\Controllers\Requests\HTMLTemplate
 */
class StoreHTMLTemplateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'path' => 'required|string|max:1024',
            'description' => 'string|max:255',
            'google_analytics' => 'string|max:1024',
            'ya_metrika' => 'string|max:1024'
        ];
    }

}