<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:32
 */

namespace LandingControl\Http\Controllers\Requests\LandingForm;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class UpdateLandingFormRequest
 * @package LandingControl\Http\Controllers\Requests\LandingForm
 */
class UpdateLandingFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.forms.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'exists:landing_forms,id',
            'name' => 'required|string|max:255',
            'id_attr' =>'required|string|max:255',
            'form_type' =>'required|numeric',
            'callback_type' =>'required|numeric',
            'btn_text' =>'required|string',
            'btn_color' =>'required|string|max:32',
            'fields' =>'required|string',
            'tags' => 'required|string',
            'callback_text' => 'string|max:255',
            'callback_url' => 'url',
            'after_header_text' => 'string|max:255',
            'header_text' => 'string|max:255',
            'before_header_text' => 'string|max:255',
            'after_btn_text' => 'string|max:255',
            'before_btn_text' => 'string|max:255',
        ];
    }

}