<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 07.07.15
 * Time: 3:32
 */

namespace LandingControl\Http\Controllers\Requests\LandingForm;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class DeleteLandingFormRequest
 * @package LandingControl\Http\Controllers\Requests\LandingForm
 */
class DeleteLandingFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.forms.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:landing_forms,id',
        ];
    }

}