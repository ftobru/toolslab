<?php

namespace LandingControl\Http\Controllers\Requests\Link;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class ConstructLinkRequest
 * @package LandingControl\Http\Controllers\Requests\Link
 */
class ConstructLinkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.links.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|numeric',
            'link_base_id' => 'required|exists:link_bases,id',
            'description' => 'string|max:255',
            'tag' => 'string|max:255',
            'utm_id' => 'numeric|exists:utms,id',
            'insert_link_id' => 'numeric|exists:insert_links,id',
            'replacement_link_id' => 'numeric|exists:replacement_links,id',
            'replacement_product_id' => 'numeric|exists:replacement_products,id',
            'callback_en_id' => 'exists:link_bases,id'
        ];
    }

}