<?php

namespace LandingControl\Http\Controllers\Requests\Link;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class StoreLinkRequest
 * @package LandingControl\Http\Controllers\Requests\Link
 */
class StoreLinkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.links.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'link' => 'string|max:255',
            'type' => 'numeric',
            'product_id' => 'exists:products,id',
            'template_id' => 'exists:templates,id',
            'status' => 'numeric',
            'hosts' => 'numeric',
            'hits' => 'numeric',
            'count_leads' => 'numeric',
            'convert'=> 'numeric'
        ];
    }

}