<?php

namespace LandingControl\Http\Controllers\Requests\Link;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class AttacheGeoDirectionRequest
 * @package LandingControl\Http\Controllers\Requests\Link
 */
class AttacheGeoDirectionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.links.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|numeric',
            'link_base_id' => 'required|exists:link_bases,id',
            'description' => 'string|max:255',
            'name' => 'required|string|max:255',
            'tag' => 'string|max:255',
            'cities' => 'array',
            'callback_en_id' => 'required|exists:link_bases,id'
        ];
    }

}