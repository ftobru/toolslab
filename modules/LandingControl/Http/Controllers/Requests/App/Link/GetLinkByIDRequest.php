<?php

namespace LandingControl\Http\Controllers\Requests\Link;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class GetLinkByIDRequest
 * @package LandingControl\Http\Controllers\Requests\Link
 */
class GetLinkByIDRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.links.edit') ||
        AccessService::canPermission('landingControl.create.links.view');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|array'
        ];
    }

}