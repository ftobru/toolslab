<?php

namespace LandingControl\Http\Controllers\Requests\Link;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Class DeleteABSplitRequest
 * @package LandingControl\Http\Controllers\Requests\Link
 */
class DeleteLinkRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('landingControl.create.links.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:links,id'
        ];
    }
}
