<?php

namespace LandingControl\Http\Controllers\Requests\InsertLink;

use Core\Http\Requests\Request;

/**
 * Class GetInsertLinkByIDRequest
 * @package LandingControl\Http\Controllers\Requests\InsertLink
 */
class GetInsertLinkByIDRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'entity_id' => 'required|exists:insert_links,id'
        ];
    }

}