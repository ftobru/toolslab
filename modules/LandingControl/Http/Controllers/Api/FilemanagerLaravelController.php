<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;
use Pqb\FilemanagerLaravel\FilemanagerLaravelEx;

/**
 * Class FilemanagerLaravelController
 * @package LandingControl\Http\Controllers
 */
class FilemanagerLaravelController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function getShow()
    {
        return view('filemanager-laravel::filemanager.index');
    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    public function getConnectors(Request $request)
    {
        $isDirAccessibleForUser = FilemanagerLaravelEx::checkAccessToDir($request);
        if ($isDirAccessibleForUser['isEnabled']) {
            $f = FilemanagerLaravelEx::Filemanager($request);
            return $f->run();
        }
        $result = json_encode(['Error:' => $isDirAccessibleForUser['msg']]);
        return Response::api(['result' => $result]);
    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    public function postConnectors(Request $request)
    {
        $isDirAccessibleForUser = FilemanagerLaravelEx::checkAccessToDir($request);
        if ($isDirAccessibleForUser['isEnabled']) {
            $f = FilemanagerLaravelEx::Filemanager($request);
            return $f->run();
        }
        $result = json_encode(['Error:' => $isDirAccessibleForUser['msg']]);
        return Response::api(['result' => $result]);
    }
}
