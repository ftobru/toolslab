<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use LandingControl\Http\Controllers\Requests\Rest\UpdateReplacementLinkRequest;
use LandingControl\Repositories\Eloquent\ReplacementLinkEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementLinkRepositoryInterface;
use LandingControl\Services\ReplacementLinkService;
use Response;
use Illuminate\Http\Request;

/**
 * Class ReplacementLinkController
 * @package LandingControl\Http\Controllers\Api
 */
class ReplacementLinkController extends Controller
{

    /** @var ReplacementLinkService  */
    protected $service;
    /** @var  ReplacementLinkEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementLinkService $service
     * @param ReplacementLinkEloquentRepository $repository
     */
    public function __construct(ReplacementLinkService $service, ReplacementLinkEloquentRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        return Response::api([
            'replacementLinks' =>  $this->service->activeRepository($request->get('metaData', []))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['replacementLink' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateReplacementLinkRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateReplacementLinkRequest $request)
    {
        $data = $request->all();
        return Response::api(['replacementLink' => $this->service->store($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateReplacementLinkRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateReplacementLinkRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['replacementLink' => $this->service->update($id, $data)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['replacementLink' => $this->repository->delete($id)]);
    }
}
