<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use LandingControl\Http\Controllers\Requests\Rest\UpdateReplacementGeoRequest;
use LandingControl\Repositories\Eloquent\ReplacementGeoEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementGeoRepositoryInterface;
use LandingControl\Services\ReplacementGeoService;
use Response;
use Illuminate\Http\Request;

/**
 * Class ReplacementGeoController
 * @package LandingControl\Http\Controllers\Api
 */
class ReplacementGeoController extends Controller
{

    /** @var ReplacementGeoService  */
    protected $service;

    /** @var ReplacementGeoEloquentRepository  */
    protected $repository;

    /**
     * @param ReplacementGeoService $service
     * @param ReplacementGeoRepositoryInterface $repository
     */
    public function __construct(ReplacementGeoService $service, ReplacementGeoRepositoryInterface $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        return Response::api([
            'replacementGeo' => $this->service->activeRepository($request->get('metaData', []))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['replacementGeo' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateReplacementGeoRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateReplacementGeoRequest $request)
    {
        $data = $request->all();
        return Response::api(['replacementGeo' => $this->service->store($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateReplacementGeoRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateReplacementGeoRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['replacementGeo' => $this->service->update($id, $data)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['replacementGeo' => $this->repository->delete($id)]);
    }
}
