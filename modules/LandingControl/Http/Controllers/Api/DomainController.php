<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use LandingControl\Http\Controllers\Requests\Rest\UpdateDomainRequest;
use LandingControl\Repositories\Eloquent\DomainEloquentRepository;
use LandingControl\Repositories\Interfaces\DomainRepositoryInterface;
use LandingControl\Services\DomainService;
use Response;
use Illuminate\Http\Request;

/**
 * Class DomainController
 * @package LandingControl\Http\Controllers\Api
 */
class DomainController extends Controller
{

    /** @var DomainService  */
    protected $service;

    /** @var DomainEloquentRepository */
    protected $repository;

    /**
     * @param DomainService $service
     * @param DomainRepositoryInterface $repository
     */
    public function __construct(DomainService $service, DomainRepositoryInterface $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        return Response::api([
            'domains' => $this->service->activeRepository($request->get('metaData', []))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['domain' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateDomainRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateDomainRequest $request)
    {
        $data = $request->all();
        return Response::api(['domain' => $this->repository->create($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDomainRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateDomainRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['domain' => $this->repository->update($data, $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['domain' => $this->repository->delete($id)]);
    }
}
