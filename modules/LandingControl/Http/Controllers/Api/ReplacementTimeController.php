<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use LandingControl\Http\Controllers\Requests\Rest\UpdateReplacementTimeRequest;
use LandingControl\Repositories\Eloquent\ReplacementTimeEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementTimeRepositoryInterface;
use LandingControl\Services\ReplacementTimeService;
use Response;
use Illuminate\Http\Request;

/**
 * Class ReplacementTimeController
 * @package LandingControl\Http\Controllers\Api
 */
class ReplacementTimeController extends Controller
{

    /** @var ReplacementTimeService  */
    protected $service;

    /** @var  ReplacementTimeEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementTimeService $service
     * @param ReplacementTimeRepositoryInterface $repository
     */
    public function __construct(ReplacementTimeService $service, ReplacementTimeRepositoryInterface $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        return Response::api([
            'replacementTimes' =>   $this->service->activeRepository($request->get('metaData', []))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['replacementTime' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateReplacementTimeRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateReplacementTimeRequest $request)
    {
        $data = $request->all();
        return Response::api(['replacementTime' => $this->repository->create($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateReplacementTimeRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateReplacementTimeRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['replacementTime' => $this->repository->update($data, $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['replacementTime' => $this->repository->delete($id)]);
    }
}
