<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use LandingControl\Http\Controllers\Requests\Rest\UpdateInsertLinkRequest;
use LandingControl\Repositories\Eloquent\InsertLinkEloquentRepository;
use LandingControl\Repositories\Interfaces\InsertLinkRepositoryInterface;
use LandingControl\Services\InsertLinkService;
use Response;
use Illuminate\Http\Request;

/**
 * Class InsertLinkController
 * @package LandingControl\Http\Controllers\Api
 */
class InsertLinkController extends Controller
{

    /** @var InsertLinkService  */
    protected $service;

    /** @var InsertLinkEloquentRepository  */
    protected $repository;

    /**
     * @param InsertLinkService $service
     * @param InsertLinkRepositoryInterface $repository
     */
    public function __construct(InsertLinkService $service, InsertLinkRepositoryInterface $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        return Response::api([
            'insertLinks' => $this->service->activeRepository($request->get('metaData', []))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['insertLink' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateInsertLinkRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateInsertLinkRequest $request)
    {
        $data = $request->all();
        return Response::api(['insertLink' => $this->service->store($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateInsertLinkRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateInsertLinkRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['insertLink' => $this->service->update($id, $data)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['insertLink' => $this->repository->delete($id)]);
    }
}
