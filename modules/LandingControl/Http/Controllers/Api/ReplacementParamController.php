<?php namespace LandingControl\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use LandingControl\Http\Controllers\Requests\Rest\UpdateReplacementParamRequest;
use LandingControl\Repositories\Eloquent\ReplacementParamEloquentRepository;
use LandingControl\Repositories\Interfaces\ReplacementParamRepositoryInterface;
use LandingControl\Services\ReplacementParamService;
use Response;
use Illuminate\Http\Request;

/**
 * Class ReplacementParamController
 * @package LandingControl\Http\Controllers\Api
 */
class ReplacementParamController extends Controller
{

    /** @var ReplacementParamService  */
    protected $service;

    /** @var  ReplacementParamEloquentRepository */
    protected $repository;

    /**
     * @param ReplacementParamService $service
     * @param ReplacementParamRepositoryInterface $repository
     */
    public function __construct(ReplacementParamService $service, ReplacementParamRepositoryInterface $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        return Response::api([
            'replacementParams' =>  $this->service->activeRepository($request->get('metaData', []))
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['replacementParam' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateReplacementParamRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateReplacementParamRequest $request)
    {
        $data = $request->all();
        return Response::api(['replacementParam' => $this->repository->create($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateReplacementParamRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateReplacementParamRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['replacementParam' => $this->repository->update($data, $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['replacementParam' => $this->repository->delete($id)]);
    }
}
