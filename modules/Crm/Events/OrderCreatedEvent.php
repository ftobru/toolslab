<?php namespace Crm\Events;

use Core\Events\Event;

use Crm\Models\Order;
use Illuminate\Queue\SerializesModels;

class OrderCreatedEvent extends Event
{

    use SerializesModels;

    /** @var Order  */
    protected $entity;

    /**
     * Create a new event instance.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->entity = $order;
    }

    /**
     * @return Order
     */
    public function getEntity()
    {
        return $this->entity;
    }

}
