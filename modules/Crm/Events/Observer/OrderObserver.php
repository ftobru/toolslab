<?php namespace Crm\Events\Observer;

use Crm\Events\OrderCreatedEvent;
use Crm\Models\Order;
use Event;

/**
 * Class OrderObserver
 * @package Crm\Events\Observer
 */
class OrderObserver
{
    /**
     * @param Order $model
     */
    public function created($model)
    {
        \Log::warning('Order created -------->');
        Event::fire(new OrderCreatedEvent($model));
    }
}
