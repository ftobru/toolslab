<?php namespace Crm\Repositories\Interfaces;

/**
 * Interface TaskRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface TaskRepositoryInterface
{
    public function getWithAllRelation();
}

