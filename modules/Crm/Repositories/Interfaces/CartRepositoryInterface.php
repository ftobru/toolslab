<?php namespace Crm\Repositories\Interfaces;

/**
 * Interface CartRepositoryInterface
 * @package Crm\Repositories\Interfaces
 */
interface CartRepositoryInterface
{
    public function getWithOrder();
}
