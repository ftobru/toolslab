<?php namespace Crm\Repositories\Interfaces;

/**
 * Interface OrderRepositoryInterface
 * @package App\Repositories\Interfaces
 */
interface OrderRepositoryInterface
{
    public function getWithClient();

    public function getWithFirm();

    public function getWithCarts();

    public function getWith(array $with);

}
