<?php namespace Crm\Repositories\Eloquent\Criteria\Client;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class StatusCriteria
 * @package App\Repositories\Eloquent\Criteria\Client
 */
class StatusCriteria extends Criteria
{
    protected $status;

    /**
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('status', '=', $this->status);
    }
}
