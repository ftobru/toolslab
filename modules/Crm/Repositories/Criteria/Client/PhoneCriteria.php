<?php namespace Crm\Repositories\Eloquent\Criteria\Client;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class PhoneCriteria
 * @package Core\Repositories\Eloquent\Criteria\Client
 */
class PhoneCriteria extends Criteria
{
    protected $value;

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('phone', 'LIKE', '%' . $this->value . '%');
        return $query;
    }

}
