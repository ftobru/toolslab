<?php namespace Crm\Repositories\Eloquent\Criteria\Client;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class TypeCriteria
 * @package App\Repositories\Eloquent\Criteria\Client
 */
class TypeCriteria extends Criteria
{
    protected $type;

    /**
     * @param $type
     */
    public function __construct($type)
    {
        $this->type = $type;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('type', '=', $this->type);
    }
}
