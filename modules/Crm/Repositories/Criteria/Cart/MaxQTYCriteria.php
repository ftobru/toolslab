<?php namespace Crm\Repositories\Eloquent\Criteria\Cart;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class MaxQTYCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Cart
 */
class MaxQTYCriteria extends Criteria
{
    protected $maxQTY;

    /**
     * @param $maxCount
     */
    public function __construct($maxCount)
    {
        $this->maxQTY = $maxCount;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('qty', '<', $this->maxQTY);
    }
}