<?php namespace Crm\Repositories\Eloquent\Criteria\Cart;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class QTYCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Cart
 */
class QTYCriteria extends Criteria
{
    protected $qty;

    /**
     * @param $count
     */
    public function __construct($count)
    {
        $this->qty = $count;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('qty', '=', $this->qty);
    }
}