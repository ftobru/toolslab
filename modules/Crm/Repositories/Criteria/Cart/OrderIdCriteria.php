<?php namespace Crm\Repositories\Eloquent\Criteria\Cart;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class OrderIdCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Cart
 */
class OrderIdCriteria extends Criteria
{
    protected $orderId;

    /**
     * @param $orderId
     */
    public function __construct($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('order_id', '=', $this->orderId);
    }
}