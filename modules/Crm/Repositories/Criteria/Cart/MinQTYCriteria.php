<?php namespace Crm\Repositories\Eloquent\Criteria\Cart;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class MinQTYCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Cart
 */
class MinQTYCriteria extends Criteria
{
    protected $minQTY;

    /**
     * @param $minCount
     */
    public function __construct($minCount)
    {
        $this->minQTY = $minCount;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('qty', '>', $this->minQTY);
    }
}