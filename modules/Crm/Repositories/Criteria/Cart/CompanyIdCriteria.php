<?php namespace Crm\Repositories\Eloquent\Criteria\Cart;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CompanyIdCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Cart
 */
class CompanyIdCriteria extends Criteria
{
    protected $companyId;

    /**
     * @param $companyId
     */
    public function __construct($companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        return $model->where('company_id', '=', $this->companyId);
    }
}