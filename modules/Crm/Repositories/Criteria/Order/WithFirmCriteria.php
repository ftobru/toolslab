<?php namespace Crm\Repositories\Criteria\Order;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithFirmCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Order
 */
class WithFirmCriteria extends Criteria
{
    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getModel()->with('firm');
    }
}
