<?php namespace Crm\Repositories\Eloquent\Criteria\Order;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ClientIdCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Order
 */
class ClientIdCriteria extends Criteria
{
    protected $value;
    protected $field = 'client_id';

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where($this->field, '=', $this->value);
    }
}
