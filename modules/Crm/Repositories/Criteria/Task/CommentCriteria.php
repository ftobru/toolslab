<?php namespace Crm\Repositories\Eloquent\Criteria\Task;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class CommentCriteria
 * @package Core\Repositories\Eloquent\Criteria\Task
 */
class CommentCriteria extends Criteria
{
    protected $comment;

    /**
     * @param $comment_part
     */
    public function __construct($comment_part)
    {
        $this->comment = $comment_part;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $query = $builder->where('comment', 'LIKE', '%'.$this->comment.'%');
        return $query;
    }

}
