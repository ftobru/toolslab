<?php namespace Crm\Repositories\Eloquent\Criteria\Task;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class OrderIdCriteria
 * @package App\Repositories\Eloquent\Criteria\Task
 */
class OrderIdCriteria extends Criteria
{
    protected $value;
    protected $field = 'order_id';

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where($this->field, '=', $this->value);
    }
}
