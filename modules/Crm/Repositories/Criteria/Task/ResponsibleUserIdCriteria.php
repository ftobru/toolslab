<?php namespace Crm\Repositories\Eloquent\Criteria\Task;

use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class ResponsibleUserIdCriteria
 * @package App\Repositories\Eloquent\Criteria\Task
 */
class ResponsibleUserIdCriteria extends Criteria
{
    protected $value;
    protected $field = 'responsible_user_id';

    /**
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where($this->field, '=', $this->value);
    }
}
