<?php namespace Crm\Repositories\Criteria\Task;

use Crm\Models\Task;
use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class WithCriteria
 * @package Crm\Repositories\Eloquent\Criteria\Task
 */
class WithAllRelationCriteria extends Criteria
{
    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->getModel()->with('responsibleUser', 'performerUser', 'order', 'client', 'firm');
    }
}
