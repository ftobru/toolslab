<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 30.06.15
 * Time: 13:10
 */

namespace Crm\Repositories\Criteria\Card;

use DB;
use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class CurrentDataMoreCreatedAt extends Criteria
{
    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where(DB::raw('created_at < NOW()'));
    }
}