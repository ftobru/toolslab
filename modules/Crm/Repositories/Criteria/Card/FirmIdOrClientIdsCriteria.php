<?php namespace Crm\Repositories\Criteria\Card;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class FirmIdOrClientIdsCriteria
 * @package Crm\Repositories\Criteria\Card
 */
class FirmIdOrClientIdsCriteria extends Criteria
{
    /** @var  array */
    protected $clientIds;
    /** @var  int */
    protected $firmId;

    /**
     * @param array $clientIds
     * @param $firmId
     */
    public function __construct(array $clientIds, $firmId = null)
    {
        $this->clientIds = $clientIds;
        $this->firmId = $firmId;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        $builder = $builder->orWhereIn('client_id', $this->clientIds);
        if ($this->firmId) {
            $builder = $builder->orWhere('firm_id', $this->firmId);
        }
        return $builder;
    }
}
