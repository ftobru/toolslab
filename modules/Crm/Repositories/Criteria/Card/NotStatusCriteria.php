<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 30.06.15
 * Time: 13:37
 */

namespace Crm\Repositories\Criteria\Card;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

class NotStatusCriteria extends Criteria
{
    protected $status;

    /**
     * @param $status
     */
    public function __construct($status)
    {
        $this->status = $status;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->where('status', '<>', $this->status);
    }
}