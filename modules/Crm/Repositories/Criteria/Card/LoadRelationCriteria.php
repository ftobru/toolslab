<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 16.07.15
 * Time: 14:28
 */

namespace Crm\Repositories\Criteria\Card;


use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class LoadRelationCriteria
 * @package Crm\Repositories\Criteria\Card
 */
class LoadRelationCriteria extends Criteria
{
    protected $relation;

    public function __construct($relation)
    {
        $this->relation = $relation;
    }

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $builder, Repository $repository)
    {
        return $builder->with($this->relation);
    }

}