<?php namespace Crm\Repositories\Eloquent\Criteria\Product;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\Criteria;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class NewPriceCompareCriteria
 * @package App\Repositories\Eloquent\Criteria\Product
 */
class NewPriceCompareCriteria extends Criteria
{
    protected $value;
    protected $operator;

    /**
     * @param $value
     * @param $operator string
     */
    public function __construct($value, $operator)
    {
        $this->value = $value;
        $this->operator = $operator;
    }

    /**
     * @param Builder $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply(Builder $model, Repository $repository)
    {
        $query = $model->where('new_price', $this->operator, $this->value);
        return $query;
    }

}
