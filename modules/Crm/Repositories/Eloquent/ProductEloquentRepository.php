<?php namespace Crm\Repositories\Eloquent;

use Crm\Repositories\Interfaces\ProductRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;


/**
 * Class ProductEloquentRepository
 * @package Core\Repository\Eloquent
 */
class ProductEloquentRepository extends Repository implements ProductRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\Product';
    }

}
