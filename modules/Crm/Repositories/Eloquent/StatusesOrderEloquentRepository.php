<?php namespace Crm\Repositories\Eloquent;

use Ftob\Repositories\Eloquent\Repository;
use Crm\Repositories\Interfaces\StatusesOrderRepositoryInterface;

class StatusesOrderEloquentRepository extends Repository implements StatusesOrderRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\StatusesOrder';
    }
}
