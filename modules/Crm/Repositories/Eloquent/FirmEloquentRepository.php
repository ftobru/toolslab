<?php namespace Crm\Repositories\Eloquent;

use Crm\Repositories\Interfaces\FirmRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class FirmEloquentRepository
 * @package Crm\Repositories\Eloquent
 */
class FirmEloquentRepository extends Repository implements FirmRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\Firm';
    }

}
