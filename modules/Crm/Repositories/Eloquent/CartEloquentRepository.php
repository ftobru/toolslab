<?php namespace Crm\Repositories\Eloquent;

use Crm\References\CartReference;
use Crm\Repositories\Criteria\Card\WithOrderCriteria;
use Crm\Repositories\Interfaces\CartRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class CartEloquentRepository
 * @package Crm\Repositories\Eloquent
 */
class CartEloquentRepository extends Repository implements CartRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\Cart';
    }

    /**
     * @param array $data
     * @return static
     */
    public function create(array $data)
    {
        if (!isset($data['content'])) {
            return ['error' => CartReference::ERROR_PRODUCTS_NOT_SET];
        }
        return parent::create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update(array $data, $id)
    {
        if (!isset($data['content'])) {
            return ['error' => CartReference::ERROR_PRODUCTS_NOT_SET];
        }
        return parent::update($data, $id);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getWithOrder()
    {
        return $this->pushCriteria(new WithOrderCriteria());
    }
}
