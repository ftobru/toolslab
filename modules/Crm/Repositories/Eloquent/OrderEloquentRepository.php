<?php namespace Crm\Repositories\Eloquent;

use Crm\Repositories\Criteria\Order\WithCartsCriteria;
use Crm\Repositories\Criteria\Order\WithClientCriteria;
use Crm\Repositories\Criteria\Order\WithCriteria;
use Crm\Repositories\Criteria\Order\WithFirmCriteria;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class OrderEloquentRepository
 * @package Core\Repository\Eloquent
 */
class OrderEloquentRepository extends Repository implements OrderRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\Order';
    }

    /**
     * @return $this
     */
    public function getWithClient()
    {
        return $this->pushCriteria(new WithClientCriteria());
    }

    /**
     * @return $this
     */
    public function getWithFirm()
    {
        return $this->pushCriteria(new WithFirmCriteria());
    }

    /**
     * @return $this
     */
    public function getWithCarts()
    {
        return $this->pushCriteria(new WithCartsCriteria());
    }

    /**
     * @param array $with
     * @return $this
     */
    public function getWith(array $with)
    {
        return $this->pushCriteria(new WithCriteria($with));
    }
}
