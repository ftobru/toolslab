<?php namespace Crm\Repositories\Eloquent;

use Crm\Repositories\Criteria\Task\WithAllRelationCriteria;
use Crm\Repositories\Interfaces\TaskRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

class TaskEloquentRepository extends Repository implements TaskRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\Task';
    }

    /**
     * @return $this
     */
    public function getWithAllRelation()
    {
        return $this->pushCriteria(new WithAllRelationCriteria());
    }

}