<?php namespace Crm\Repositories\Eloquent;


use Crm\Repositories\Interfaces\StatusesTaskRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

class StatusesTaskEloquentRepository extends Repository implements StatusesTaskRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\StatusesTask';
    }
}
