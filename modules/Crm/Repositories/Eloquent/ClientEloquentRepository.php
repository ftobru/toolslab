<?php namespace Crm\Repositories\Eloquent;

use Crm\Repositories\Interfaces\ClientRepositoryInterface;
use Ftob\Repositories\Eloquent\Repository;

/**
 * Class ClientEloquentRepository
 * @package App\Repositories\Eloquent
 */
class ClientEloquentRepository extends Repository implements ClientRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return 'Crm\Models\Client';
    }


}
