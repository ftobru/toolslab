<?php

namespace Crm\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class Comment
 * @package Crm\Models
 * @property int $message
 * @property int company_id
 */
class Comment extends CoreModel
{
    use CompanyName;

    protected $company = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}
