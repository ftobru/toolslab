<?php namespace Crm\Models\Interfaces;

/**
 * Interface ResponsiveUserInterface
 * @package Crm\Models\Interfaces
 */
interface ResponsiveUserInterface
{
    public function getScopeLimitPermission();
}
