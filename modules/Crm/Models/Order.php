<?php namespace Crm\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Crm\Models\Interfaces\ResponsiveUserInterface;
use Saas\Traits\CompanyName;
use Saas\Traits\ResponsibleUser;

/**
 * Class Order
 * @property mixed tagged
 * @package Crm\Models
 */
class Order extends CoreModel implements ResponsiveUserInterface
{
    use TaggableTrait, CompanyName, ResponsibleUser;

    protected $table = 'orders';

    protected $company = true;
    protected $responsible_user = true;
    /** Имя права на ограничение видимости сущностей. */
    protected $scope_limit_permission = 'crm.deals.view.all';

    /**
     * @var array
     */
    protected $fillable = [
        'client_id',
        'status',
        'company_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('Crm\Models\Client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function firm()
    {
        return $this->belongsTo('Crm\Models\Firm');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsible_user()
    {
        return $this->belongsTo('Saas\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carts()
    {
        return $this->hasMany('Crm\Models\Cart', 'order_id');
    }


    /**
     * @return string
     */
    public function getScopeLimitPermission()
    {
        return $this->scope_limit_permission;
    }
}
