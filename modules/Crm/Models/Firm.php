<?php namespace Crm\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Core\Traits\Taggable;
use Crm\Models\Interfaces\ResponsiveUserInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Saas\Traits\CompanyName;
use Saas\Traits\ResponsibleUser;

/**
 * Class Firm
 * @package Crm\Models
 *
 * @property $responsible_user_id
 */
class Firm extends CoreModel implements ResponsiveUserInterface
{
    use CompanyName, TaggableTrait, ResponsibleUser;

    /** @var string  */
    protected $table = 'firms';
    /** @var array  */
    protected $fillable = [
        'status',
        'name',
        'description',
        'company_id',
        'performer_user_id',
        'responsible_user_id',
        'type',
        'email',
        'phone'
    ];

    protected $company = true;

    protected $responsible_user = true;
    /** Имя права на ограничение видимости сущностей. */
    protected $scope_limit_permission = 'crm.firms.view.all';

    protected $containCustomField = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function performerUser()
    {
        return $this->belongsTo('Saas\Models\User', 'performer_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function responsibleUser()
    {
        return $this->belongsTo('Saas\Models\User', 'responsible_user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany('Crm\Models\Client')->withPivot('firm_id', 'client_id');
    }

    /**
     * Get permission name.
     *
     * @return string
     */
    public function getScopeLimitPermission()
    {
        return $this->scope_limit_permission;
    }
}
