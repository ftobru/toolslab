<?php namespace Crm\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class StatusesTask
 * @package Saas\Models
 */
class StatusesTask extends CoreModel
{
    use CompanyName;

    /** @var string  */
    protected $table = 'statuses_tasks';

    /** @var array  */
    protected $fillable = ['company_id', 'statuses'];

    protected $company = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}
