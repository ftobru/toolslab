<?php namespace Crm\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class Product
 * @package Crm\Models
 * @property $name
 * @property $new_price
 * @property $old_price
 * @property $profit
 * @property $description
 * @property $article
 * @property $company_id
 */
class Product extends CoreModel
{
    use TaggableTrait, CompanyName;

    protected $table = 'products';

    protected $containCustomField = true;

    protected $fillable = ['name', 'new_price', 'old_price', 'discount', 'description', 'article',
        'company_id', 'profit', 'prime_cost', 'photos', 'min_price'];

    protected $company = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}
