<?php namespace Crm\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Crm\Models\Interfaces\ResponsiveUserInterface;
use Illuminate\Database\Eloquent\Model;
use Saas\Traits\CompanyName;

/**
 * Class Task
 *
 * @package Crm\Models
 * @property $client_id
 * @property $order_id
 * @property $type
 * @property $date_perf
 * @property $responsible_user_id
 * @property $comment
 * @property $state
 * @property $company_id
 * @property $final_comment
 */
class Task extends CoreModel implements ResponsiveUserInterface
{
    use TaggableTrait, CompanyName;

    protected $table = 'tasks';

    protected $fillable = [
        'client_id',
        'order_id',
        'type',
        'date_perf',
        'responsible_user_id',
        'performer_user_id',
        'comment',
        'state',
        'company_id',
        'final_comment'
    ];

    /** @var bool  */
    protected $company = true;
    protected $responsible_user = true;
    /** Имя права на ограничение видимости сущностей. */
    protected $scope_limit_permission = 'crm.tasks.view.all';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function order()
    {
        return $this->belongsTo('Crm\Models\Order', 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->belongsTo('Crm\Models\Client', 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firm()
    {
        return $this->belongsTo('Crm\Models\Firm', 'firm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function responsibleUser()
    {
        return $this->belongsTo('Saas\Models\User', 'responsible_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function performerUser()
    {
        return $this->belongsTo('Saas\Models\User', 'performer_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }

    public function getScopeLimitPermission()
    {
        return $this->scope_limit_permission;
    }
}
