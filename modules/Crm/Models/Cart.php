<?php namespace Crm\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

/**
 * Class Cart
 * @package Crm\Models
 */
class Cart extends CoreModel
{

    use CompanyName;

    protected $table = 'cart';

    protected $company = true;

    protected $fillable = [
        'order_id',
        'qty',
        'company_id',
        'order_id',
        'content'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function order()
    {
        return $this->belongsTo('Crm\Models\Order', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo('Saas\Models\Company');
    }
}
