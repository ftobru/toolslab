<?php namespace Crm\Models;

use Core\Models\Abstracts\CoreModel;
use Saas\Traits\CompanyName;

class StatusesOrder extends CoreModel
{
    use CompanyName;

    protected $table = 'statuses_orders';

    protected $fillable = [];

    protected $guarded = ['id'];

    protected $company = true;

    /**
     * @param $value
     * @return mixed
     */
    public function getStatusesAttribute($value)
    {
        return $this->attributes['statuses'] = json_decode($value, true);
    }

    /**
     * @param $value
     * @return mixed|string
     */
    public function setStatusesAttribute($value)
    {
        return $this->attributes['statuses'] = json_encode($value);
    }
}
