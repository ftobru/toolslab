<?php namespace Crm\Models;

use Conner\Tagging\TaggableTrait;
use Core\Models\Abstracts\CoreModel;
use Crm\Models\Interfaces\ResponsiveUserInterface;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Saas\Traits\CompanyName;
use Saas\Traits\ResponsibleUser;

/**
 * Class Client
 * @property mixed tagged
 * @package App
 * @property $type
 * @property $status
 * @property $company_id
 * @property $responsible_user_id
 */
class Client extends CoreModel implements ResponsiveUserInterface
{
    use TaggableTrait, CompanyName, ResponsibleUser;

    protected $table = 'clients';

    protected $fillable = ['status', 'type', 'company_id', 'name', 'email', 'phone', 'description',
        'responsible_user_id'];

    protected $company = true;
    protected $responsible_user = true;
    /** Имя права на ограничение видимости сущностей. */
    protected $scope_limit_permission = 'crm.contacts.view.all';

    protected $containCustomField = true;

    /**
     * Один ко многим
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany('Crm\Models\Order');
    }

    /**
     * @return BelongsToMany
     */
    public function firms()
    {
        return $this->belongsToMany('Crm\Models\Firm')->withPivot('firm_id', 'client_id');
    }

    public function tasks()
    {
        return $this->hasMany('Crm\Models\Task');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function responsibleUser()
    {
        return $this->belongsTo('Saas\Models\User', 'responsible_user_id');
    }

    public function getScopeLimitPermission()
    {
        return $this->scope_limit_permission;
    }
}
