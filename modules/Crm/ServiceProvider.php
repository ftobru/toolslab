<?php namespace Crm;

use Crm\Events\Observer\OrderObserver;
use Crm\Models\Order;
use Crm\Services\CardService;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

/**
 * Class ServiceProvider
 * @package Crm
 */
class ServiceProvider extends BaseServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        // Repository
        $this->app->bind(
            'Crm\Repositories\Interfaces\OrderRepositoryInterface',
            'Crm\Repositories\Eloquent\OrderEloquentRepository'
        );

        $this->app->bind(
            'Core\Repositories\Interfaces\BillingAccountRepositoryInterface',
            'Core\Repositories\Eloquent\BillingAccountEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\CartRepositoryInterface',
            'Crm\Repositories\Eloquent\CartEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\ClientRepositoryInterface',
            'Crm\Repositories\Eloquent\ClientEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\TaskRepositoryInterface',
            'Crm\Repositories\Eloquent\TaskEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\ProductRepositoryInterface',
            'Crm\Repositories\Eloquent\ProductEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\FirmRepositoryInterface',
            'Crm\Repositories\Eloquent\FirmEloquentRepository'
        );

        $this->app->bind(
            'StatusOrderService',
            'Crm\Services\StatusOrderService'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\StatusesOrderRepositoryInterface',
            'Crm\Repositories\Eloquent\StatusesOrderEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\StatusesTaskRepositoryInterface',
            'Crm\Repositories\Eloquent\StatusesTaskEloquentRepository'
        );

        $this->app->bind(
            'Crm\Repositories\Interfaces\FirmRepositoryInterface',
            'Crm\Repositories\Eloquent\FirmEloquentRepository'
        );


        $this->app->bind('CardService', 'Crm\Services\CardService');

        // Events
        Order::observe(new OrderObserver());
    }
}