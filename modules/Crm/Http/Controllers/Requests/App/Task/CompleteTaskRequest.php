<?php namespace Crm\Http\Requests\App\Task;

use AccessService;
use Auth;
use Core\Http\Requests\Request;
use Crm\Repositories\Eloquent\TaskEloquentRepository;
use Crm\Repositories\Interfaces\TaskRepositoryInterface;
use Input;
use Saas\Models\User;

/**
 * Class CompleteTaskRequest
 * @package Crm\Http\Requests\App\Task
 */
class CompleteTaskRequest extends Request
{
    /**
     * @var TaskEloquentRepository
     */
    protected $taskRepository;

    /**
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (AccessService::canPermission('crm.tasks.edit.all')) {
            return true;
        } elseif (AccessService::canPermission('crm.tasks.edit.their')) {
            $models = Input::get('models', []);
            /** @var User $user */
            $user = Auth::getUser();
            foreach ($models as $model) {
                if ($this->taskRepository->find(intval($model['id']))->responsible_user_id !== $user->id) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:tasks,id',
            'final_comment' => 'string|max:255'
        ];
    }
}
