<?php namespace Crm\Http\Requests\App\Task;

use AccessService;
use Auth;
use Core\Http\Requests\Request;
use Crm\Repositories\Eloquent\TaskEloquentRepository;
use Crm\Repositories\Interfaces\TaskRepositoryInterface;
use Input;
use Saas\Models\User;

/**
 * Class DeleteTaskRequest
 * @package Crm\Http\Requests\App\Task
 */
class DeleteTaskRequest extends Request
{
    /**
     * @var TaskEloquentRepository
     */
    protected $taskRepository;

    /**
     * @param TaskRepositoryInterface $taskRepository
     */
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (AccessService::canPermission('crm.tasks.delete.all')) {
            return true;
        } elseif (AccessService::canPermission('crm.tasks.delete.their')) {
            $models = Input::get('models', []);
            /** @var User $user */
            $user = Auth::getUser();
            foreach ($models as $model) {
                if ($this->taskRepository->find(intval($model['id']))->responsible_user_id !== $user->id) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:tasks,id'
        ];
    }
}
