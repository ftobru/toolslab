<?php namespace Crm\Http\Requests\App\Client;

use AccessService;
use Core\Http\Requests\Request;
use Crm\Repositories\Eloquent\ClientEloquentRepository;
use Crm\Repositories\Interfaces\ClientRepositoryInterface;
use Input;
use Saas\Models\User;

/**
 * Class DeleteClientRequest
 * @package Crm\Http\Requests\App\Client
 */
class DeleteClientRequest extends Request
{
    /**
     * @var ClientEloquentRepository
     */
    protected $clientRepository;

    /**
     * @param ClientRepositoryInterface $clientRepository
     */
    public function __construct(ClientRepositoryInterface $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (AccessService::canPermission('crm.contacts.delete.all')) {
            return true;
        } elseif (AccessService::canPermission('crm.contacts.delete.their')) {
            $models = Input::get('models', []);
            /** @var User $user */
            $user = \Auth::getUser();
            foreach ($models as $model) {
                if ($this->clientRepository->find(intval($model['id']))->responsible_user_id !== $user->id) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'models.id' => 'exists:clients,id'
        ];
    }
}
