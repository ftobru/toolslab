<?php namespace Crm\Http\Requests\App\Product;

use AccessService;
use Core\Http\Requests\Request;

/**
 * Delete product via grid.
 *
 * Class DeleteProductRequest
 * @package Crm\Http\Requests\App\Product
 */
class DeleteProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model.name' => 'min:1|max:128',
            'model.new_price' => 'numeric',
            'model.old_price' => 'numeric',
            'model.profit' => 'numeric',
        ];
    }
}
