<?php namespace Crm\Http\Requests\App\Firm;

use AccessService;
use Core\Http\Requests\Request;
use Crm\Models\Firm;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Repositories\Interfaces\FirmRepositoryInterface;
use DB;
use Input;
use Saas\Models\User;

/**
 * Class DeleteFirmRequest
 * @package Crm\Http\Requests\App\Firm
 */
class DeleteFirmRequest extends Request
{
    /**
     * @var FirmEloquentRepository
     */
    protected $firmRepository;

    /**
     * @param FirmRepositoryInterface $firmRepository
     */
    public function __construct(FirmRepositoryInterface $firmRepository)
    {
        $this->firmRepository = $firmRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (AccessService::canPermission('crm.firms.delete.all')) {
            return true;
        } elseif (AccessService::canPermission('crm.firms.delete.their')) {
            $models = Input::get('models', []);
            /** @var User $user */
            $user = \Auth::getUser();
            foreach ($models as $model) {
                if ($this->firmRepository->find(intval($model['id']))->responsible_user_id !== $user->id) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'models.id' => 'exists:firms,id'
        ];
    }
}
