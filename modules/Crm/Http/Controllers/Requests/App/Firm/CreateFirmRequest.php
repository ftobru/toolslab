<?php namespace Crm\Http\Requests\App\Firm;

use AccessService;
use Core\Http\Requests\Request;
use Input;

/**
 * Class CreateFirmRequest
 * @package Crm\Http\Requests\App\Firm
 */
class CreateFirmRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return AccessService::canPermission('crm.firms.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'models.name' => 'min:1|max:255',
            'model.email' => 'max:255',
            'model.phone' => 'min:1|max:255',
            'model.type' => 'numeric',
            'model.description' => 'max:255',
            'model.status' => 'numeric',
        ];
    }
}
