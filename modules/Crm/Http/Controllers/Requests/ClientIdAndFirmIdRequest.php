<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 02.07.15
 * Time: 20:00
 */

namespace Crm\Http\Requests;


use Core\Http\Requests\Request;

class ClientIdAndFirmIdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clientId' => 'required|exists:clients,id',
            'firmId' => 'required|exists:firms,id',
        ];
    }

}