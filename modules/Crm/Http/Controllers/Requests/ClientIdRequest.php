<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 02.07.15
 * Time: 21:06
 */


namespace Core\Http\Requests;

class ClientIdRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:clients,id'
        ];
    }

}