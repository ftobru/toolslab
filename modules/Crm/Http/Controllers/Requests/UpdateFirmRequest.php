<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 02.07.15
 * Time: 20:25
 */

namespace Crm\Http\Requests;


use Core\Http\Requests\Request;

class UpdateFirmRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:firms,id',
            'name' => 'required|max:255',
            'description' => 'max:255',
            'phone' => 'max:20',
            'other_phone' => 'max:20',
            'tags' => 'array'
        ];
    }

}