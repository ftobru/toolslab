<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 02.07.15
 * Time: 19:56
 */

namespace Crm\Http\Requests;

use Core\Http\Requests\Request;

class ClientIdsAndFirmIdRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clientIds' => 'required|array',
            'firmId' => 'required|exists:firms,id'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

    }

}