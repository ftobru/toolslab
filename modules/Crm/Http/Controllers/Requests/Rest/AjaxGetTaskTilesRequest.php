<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class AjaxGetTaskTilesRequest
 * @package Crm\Http\Requests\Rest
 */
class AjaxGetTaskTilesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'options' => 'array',
            'options.take' => 'integer',
            'options.skip' => 'integer',
            'options.page' => 'integer',
            'options.pageSize' => 'integer'
        ];
    }
}
