<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateTaskRequest
 * @package App\Http\Requests\Rest
 */
class UpdateTaskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|integer|exists:clients,id',
            'order_id' => 'required|integer|exists:orders,id',
            'company_id' => 'required|integer|exists:companies,id',
            'type' => 'required|integer',
            'responsible_user_id' => 'required|integer|exists:users,id',
            'performer_user_id' => 'required|integer|exists:users,id',
            'comment' => 'max:255',
            'state' => 'integer',
            'tags' => 'required|array',
        ];
    }
}
