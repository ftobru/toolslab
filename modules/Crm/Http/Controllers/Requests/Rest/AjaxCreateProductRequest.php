<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class AjaxCreateProductRequest
 * @package Crm\Http\Requests\Rest
 */
class AjaxCreateProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'model.name' => 'min:1|max:128',
            'model.new_price' => 'numeric',
            'model.old_price' => 'numeric',
            'model.profit' => 'numeric',
        ];
    }
}
