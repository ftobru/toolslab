<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class PostEditProductRequest
 * @package Crm\Http\Requests\Rest
 */
class PostEditProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'article' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'tags' => 'required|string',
            'description' => 'required|string|max:255',
            'new_price' => 'required|numeric',
            'old_price' => 'required|numeric',
            'min_price' => 'required|numeric',
            'prime_cost' => 'required|numeric',
        ];
    }
}
