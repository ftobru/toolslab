<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateOrderRequest
 * @package App\Http\Requests\Rest
 */
class UpdateOrderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required|integer|exists:clients,id',
            'company_id' => 'required|integer|exists:companies,id',
            'status' => 'integer',
            'tags' => 'required|array',
        ];
    }
}
