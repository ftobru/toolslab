<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateClientRequest
 * @package App\Http\Requests\Rest
 */
class UpdateClientRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'integer',
            'type' => 'integer',
            'company_id' => 'required|exists:companies,id',
            'tags' => 'required|array',
        ];
    }
}
