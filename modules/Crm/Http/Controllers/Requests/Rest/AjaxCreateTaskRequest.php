<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class AjaxFirmRequest
 * @package Crm\Http\Requests\Rest
 */
class AjaxCreateTaskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'responsible_user_id' => 'required|exists:users,id',
            'performer_user_id' => 'required|exists:users,id',
            'type' => 'required|integer',
            'state' => 'required|integer',
            'comment' => 'string|max:255'
        ];
    }
}
