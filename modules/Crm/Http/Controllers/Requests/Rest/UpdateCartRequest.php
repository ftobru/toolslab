<?php namespace Crm\Http\Requests\Rest;

use Core\Http\Requests\Request;

/**
 * Class UpdateCartRequest
 * @package Crm\Http\Requests\Rest
 */
class UpdateCartRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id' => 'required|integer|exists:orders,id',
            'company_id' => 'required|integer|exists:companies,id',
            'content' => 'required|array',
            'content.products' => 'required|array',
            'qty' => 'integer',
        ];
    }
}
