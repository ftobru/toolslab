<?php namespace Crm\Http\Controllers\App;

use AccessService;
use App;
use Core\Http\Controllers\Controller;
use Core\Services\ProductService;
use Crm\Http\Requests\App\Product\CreateProductRequest;
use Crm\Http\Requests\App\Product\DeleteProductRequest;
use Crm\Http\Requests\App\Product\GetProductsRequest;
use Crm\Http\Requests\App\Product\UpdateProductRequest;
use Crm\Models\Product;
use Crm\Repositories\Eloquent\Criteria\Product\CompanyIdCriteria;
use Crm\Repositories\Eloquent\ProductEloquentRepository;
use Illuminate\Foundation\Application;
use Saas\Models\Company;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Repositories\Eloquent\FormsEntityEloquentRepository;
use Saas\Repositories\Interfaces\FormsEntityRepositoryInterface;
use Validator;
use Config;
use Redirect;
use Response;
use Saas\Models\User;
use Saas\Services\CompanyService;
use Saas\Services\StateApplicationService;
use Input;

/**
 * Class ProductController
 * @package Crm\Http\Controllers\App
 */
class ProductController extends Controller
{
    /** @var  ProductService */
    protected $productService;

    /** @var  ProductEloquentRepository */
    protected $productRepository;

    /** @var  CompanyService */
    protected $companyService;

    /** @var  StateApplicationService */
    protected $stateAppService;

    /** @var  FormEloquentRepository */
    protected $formRepository;

    /** @var FormsEntityEloquentRepository */
    protected $formsEntityRepository;

    /**
     * @param Application $app
     * @param CompanyService $companyService
     * @param ProductService $productService
     * @param ProductEloquentRepository $productRepository
     * @param StateApplicationService $stateAppService
     * @param FormsEntityRepositoryInterface $formsEntityRepository
     * @param FormEloquentRepository $formRepository
     */
    public function __construct(
        Application $app,
        CompanyService $companyService,
        ProductService $productService,
        ProductEloquentRepository $productRepository,
        StateApplicationService $stateAppService,
        FormsEntityRepositoryInterface $formsEntityRepository,
        FormEloquentRepository $formRepository
    ) {
        $this->companyService = $companyService;
        $this->productService = $productService;
        $this->productRepository = $productRepository;
        $this->stateAppService = $stateAppService;
        $this->app = $app;
        $this->formRepository = $formRepository;
        $this->formsEntityRepository = $formsEntityRepository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function index($company_alias)
    {
        /** @var StateApplicationService $stateService */
        $stateApp = App::make('StateApplicationService');

        /** @var User $user */
        $user = \Auth::getUser();
        if ($company = $stateApp->getCurrentCompany()) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    $products = $this->productRepository->applyCriteria(new CompanyIdCriteria($company->id))->all();
                    $fields = $this->getAdditionalFields();
                    return view('crm.products.index', [
                        'company' => $company,
                        'products' => json_encode($products),
                        'customFields' => $fields,
                        'primeCost' =>  AccessService::canPermission('crm.products.prime'),
                    ]);
                }
            }
            return Redirect::to('/401');
        }
        return Redirect::to('/404');
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function fields($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    $fields = $this->formRepository
                        ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Product::class)))
                        ->pushCriteria(new CompanyIdCriteria($company->id))
                        ->all();
                    foreach ($fields as $field) {
                        if ($field->form_type === 1) { // TODO
                            $field->type = 'string';
                            $field->form_type_label = 'Текстовое поле';
                        } else {
                            $field->type = 'string';
                            $field->form_type_label = 'Текстовый поле';
                        }
                    }
                    return view('crm.products.fields', [
                        'company' => $company,
                        'customFields' => $fields,
                    ]);
                }
            }
            return Redirect::to('/401');
        }
        return Redirect::to('/404');
    }

    /**
     * @param $company_alias
     * @param $product_id
     * @return \Illuminate\View\View
     */
    public function edit($company_alias, $product_id)
    {
        /** @var Company $company */
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    $product = $this->productRepository
                        ->pushCriteria(new CompanyIdCriteria($company->id))
                        ->find($product_id);
                    if ($product) {
                        $tags_array = $product->tagged->toArray();
                        $tags = [];
                        foreach ($tags_array as $tag) {
                            $tags[] = $tag['tag_name'];
                        }
                        if ($product) {
                            return view('crm.products.edit', [
                                'company' => $company,
                                'company_folder' => $this->companyService->generateCompanyDirName($company->id),
                                'product' => $product->toArray(),
                                'company_alias' => $company_alias,
                                'locale' => $this->app->config->get('app.locale'),
                                'tags' => implode(',', $tags),
                                'fields' => $this->getAdditionalFields(),
                                'primeCost' => AccessService::canPermission('crm.products.prime'),
                            ]);
                        }
                    }
                    abort(404, 'Товар не найден');
                }
            }
            abort(401, 'Вы не состоите в текущей компании');
        }
        abort(404, 'Компания не существует');
    }

    /**
     * POST на редактирование товара.
     *
     * @param $company_alias
     * @param $product_id
     * @return \Illuminate\View\View
     */
    public function postEdit($company_alias, $product_id)
    {
        $rules = [
            'article' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            'tags' => 'string',
            'description' => 'string|max:255',
            'new_price' => 'required|numeric',
            'old_price' => 'required|numeric',
            'min_price' => 'numeric',
            'prime_cost' => 'numeric',
        ];

        $params = Input::all();
        /** @var Validator $validator */
        $validator = Validator::make($params, $rules);

        if (!AccessService::canPermission('crm.products.edit.all')) {
            abort(403, 'Нет прав на редактирование товара');
        }

        if ($validator->fails()) {
            $messages = $validator->messages();
            return Redirect::to($company_alias . '/crm/product/edit/' . $product_id)->withErrors($messages);
        } else {
            $company = $this->companyService->getByName($company_alias);
            /** @var User $user */
            $user = \Auth::getUser();
            if ($company) {
                $userCompanies = $user->companies()->get();
                foreach ($userCompanies as $c) {
                    if (empty($params['fileLinks'])) {
                        $params['fileLinks'] = null;
                    }
                    if ($c->id === $company->id) {
                        if (!isset($params['prime_cost'])) {
                            $params['prime_cost'] = 0;
                        }

                        $updated_product = [
                            'article' => $params['article'],
                            'name' => $params['name'],
                            'description' => $params['description'],
                            'new_price' => $params['new_price'],
                            'min_price' => $params['min_price'],
                            'old_price' => $params['old_price'],
                            'prime_cost' => $params['prime_cost'],
                            'profit' => $params['new_price'] - $params['prime_cost'],
                            'photos' => $params['fileLinks']
                        ];

                        // Additional fields
                        $fields = $this->getAdditionalFields();
                        foreach ($fields as $field) {
                            if (isset($params[$field->name])) {
                                $updated_product[$field->name] = $params[$field->name];
                            }
                        }

                        $this->productRepository->update($updated_product, $product_id);

                        if (!empty($params['tags'])) {
                            $tags = explode(',', $params['tags']);
                            $product = $this->productRepository->find($product_id);
                            $product->retag($tags);
                        }

                        return Redirect::to($company_alias . '/crm/product/edit/' . $product_id)
                            ->withSuccess('Успешно обновлено');
                    }
                }
                return Redirect::to('/401');
            }
            abort(404, 'Company not found');
        }
    }

    /**
     * @param GetProductsRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getProducts(GetProductsRequest $request, $company_alias)
    {
        /** @var StateApplicationService $stateService */
        $stateApp = App::make('StateApplicationService');
        if ($company = $stateApp->getCurrentCompany()) {
            $this->productRepository->pushCriteria(new CompanyIdCriteria($company->id));
            $products = $this->productService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $products['data'],
                'total' => $products['total'],
                'filters_data' => $products['filters_data'],
                'groups' => array_get($products, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param CreateProductRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createProduct(CreateProductRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if ($this->isIsSetBaseParamFields($model)
                    ) {
                        $new_product = $this->getEntityModel($model);
                        $this->attachAdditionalParams($new_product, $model);
                        $newProduct = $this->productRepository->create($new_product);
                        array_push($products, $newProduct);
                    }
                }
                return Response::json(['data' => $products]);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if ($this->isIsSetBaseParamFields($model)
                ) {
                    $new_product = $this->getEntityModel($model);
                    $this->attachAdditionalParams($new_product, $model);
                    $newProduct = $this->productRepository->create($new_product);
                    return Response::json(['data' => $newProduct]);
                }
                return Response::json(['bad params'], 500);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateProduct(UpdateProductRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if ($this->isIsSetBaseParamFields($model) && isset($model['id'])
                    ) {
                        $updated_product = $this->getEntityModel($model);
                        $this->attachAdditionalParams($updated_product, $model);
                        $newProduct = $this->productRepository->update($updated_product, intval($model['id']));
                        array_push($products, $newProduct);
                    }
                }
                return Response::json(['data' => $products]);
            }

            if (isset($data['model'])) {
                $model = $data['model'];
                if ($this->isIsSetBaseParamFields($model) && isset($model['id'])
                ) {
                    $updated_product = $this->getEntityModel($model);
                    $this->attachAdditionalParams($updated_product, $model);
                    $newProduct = $this->productRepository->update($updated_product, intval($model['id']));
                    array_push($products, $newProduct);
                    return Response::json(['data' => $products]);
                }
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteProduct(DeleteProductRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->productRepository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->productRepository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
            if (isset($data['product_id'])) {
                $this->productRepository->delete($data['product_id']);
                return Response::json(true);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param $entityArray
     * @param $model
     */
    private function attachAdditionalParams(&$entityArray, $model)
    {
        // Additional fields
        $prod = new Product();
        $fields = $prod->getCustomAttribute();
        foreach ($fields as $field) {
            if (isset($model[$field->name])) {
                $entityArray[$field->name] = $model[$field->name];
            }
        }
    }

    /**
     * @param $model
     * @return array
     */
    private function getEntityModel($model)
    {
        $vars = ['description', 'article', 'photos'];
        $entityArray = [
            'name' => $model['name'],
            'new_price' => $model['new_price'],
            'old_price' => $model['old_price'],
            'min_price' => $model['min_price'],
            'profit' => $model['profit'],
            'prime_cost' => $model['prime_cost'],
        ];
        foreach ($vars as $var) {
            if (isset($model[$var])) {
                $entityArray[$var] = $model[$var];
            }
        }
        if (isset($entityArray['photos']) && empty($entityArray['photos'])) {
            $entityArray['photos'] = json_encode([]);
        }
        return $entityArray;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getAdditionalFields()
    {
        $fields = $this->formRepository
            ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Product::class)))
            ->all();
        foreach ($fields as $field) {
            if ($field->form_type === Config::get('custom-fields.form_types.text')) { // TODO
                $field->type = 'string';
            } else {
                $field->type = 'string';
            }
        }
        return $fields;
    }

    /**
     * @param $model
     * @return bool
     */
    private function isIsSetBaseParamFields(&$model)
    {
        if (!isset($model['prime_cost'])) {
            $model['prime_cost'] = 0;
        }
        if (!isset($model['profit'])) {
            $model['profit'] = $model['new_price'] - $model['prime_cost'];
        }

        return isset($model['name']) &&
        isset($model['new_price']) &&
        isset($model['old_price']) &&
        isset($model['min_price']);
    }
}
