<?php namespace Crm\Http\Controllers\App;

use Core\Http\Controllers\Controller;
use Crm\Http\Requests\App\Task\CompleteTaskRequest;
use Crm\Http\Requests\App\Task\CreateTaskRequest;
use Crm\Http\Requests\App\Task\DeleteTaskRequest;
use Crm\Http\Requests\App\Task\UpdateTaskRequest;
use Crm\Http\Requests\Rest\AjaxCompleteTaskRequest;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxCreateTaskRequest;
use Crm\Http\Requests\Rest\AjaxDeleteTaskRequest;
use Crm\Http\Requests\Rest\AjaxGetTaskByIDRequest;
use Crm\Http\Requests\Rest\AjaxGetTaskTilesRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\Http\Requests\Rest\AjaxUpdateTaskRequest;
use Crm\References\FirmReference;
use Crm\References\OrderReference;
use Crm\References\TaskReference;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Eloquent\TaskEloquentRepository;
use Crm\Services\ClientService;
use Crm\Services\FirmService;
use Crm\Services\OrderService;
use Crm\Services\TaskService;
use Redirect;
use Response;
use Saas\Models\Company;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Services\CompanyService;
use Saas\Services\UserService;

/**
 * Class TaskController
 * @package Crm\Http\Controllers\App
 */
class TaskController extends Controller
{

    /** @var  CompanyService */
    protected $companyService;

    /** @var OrderService  */
    protected $orderService;

    /** @var  ClientService */
    protected $clientService;

    /** @var  FirmService */
    protected $firmRepository;

    /** @var  UserService */
    protected $userRepository;

    /** @var  OrderEloquentRepository */
    protected $orderRepository;

    /** @var  TaskService */
    protected $taskService;

    /** @var  TaskEloquentRepository */
    protected $taskRepository;

    /**
     * @param OrderService $orderService
     * @param CompanyService $companyService
     * @param ClientService $clientService
     * @param FirmEloquentRepository $firmRepository
     * @param UserEloquentRepository $userRepository
     * @param TaskService $taskService
     * @param TaskEloquentRepository $taskEloquentRepository
     * @param OrderEloquentRepository $orderRepository
     */
    public function __construct(
        OrderService $orderService,
        CompanyService $companyService,
        ClientService $clientService,
        FirmEloquentRepository $firmRepository,
        UserEloquentRepository $userRepository,
        TaskService $taskService,
        TaskEloquentRepository $taskEloquentRepository,
        OrderEloquentRepository $orderRepository
    ) {
        $this->orderService = $orderService;
        $this->companyService = $companyService;
        $this->orderRepository = $orderRepository;
        $this->firmRepository = $firmRepository;
        $this->clientService = $clientService;
        $this->userRepository = $userRepository;
        $this->taskService = $taskService;
        $this->taskRepository = $taskEloquentRepository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function index($company_alias)
    {
        /** @var Company $company */
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('crm.tasks.index', [
                'company' => $company,
                'states' => TaskReference::statesInfo(),
                'clients' => $this->clientService->getAllClients(),
                'firms' => $this->firmRepository->all(),
                'users' => $company->users()->get(),
                'types' => TaskReference::typesInfo()
            ]);
        }
        abort(404);
    }

    /**
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function indexTiles($company_alias)
    {
        /** @var Company $company */
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('crm.tasks.tasklist-tileview', [
                'company' => $company,
                'states' => TaskReference::statesInfo(),
                'clients' => $this->clientService->getAllClients(),
                'firms' => $this->firmRepository->all(),
                'users' => $company->users()->get(),
                'tasks' => $this->taskService->getTilesData([]),
                'types' => TaskReference::typesInfo(),
                'newOrders' => $this->orderService->getNewOrders()
            ]);
        }
        abort(404);
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getTasks(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->taskService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxGetTaskTilesRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getTaskTiles(AjaxGetTaskTilesRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->taskService->getTilesData($request->all());
            return Response::json(['tasks' => $result]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param CreateTaskRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createTask(CreateTaskRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->taskService->createNewTask($request->all());
            return Response::json(['task'=>$result]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateTaskRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateTask(UpdateTaskRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->taskService->updateSomeTask($request->all());
            return Response::json(['task'=>$result]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param CompleteTaskRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function completeTask(CompleteTaskRequest $request, $company_alias)
    {
        $result = $this->taskService->completeTask($request->all());
        return Response::json(['task'=>$result]);
    }

    /**
     * @param AjaxGetTaskByIDRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTaskByID(AjaxGetTaskByIDRequest $request, $company_alias)
    {
        $result = $this->taskService->getTaskByID($request->get('id'));
        return Response::json(['task'=>$result]);
    }

    /**
     * @param DeleteTaskRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteTask(DeleteTaskRequest $request, $company_alias)
    {
        $result = $this->taskRepository->delete(intval($request->get('id')));
        return Response::json(['result' => $result]);
    }
}
