<?php namespace Crm\Http\Controllers\App;

use Core\Http\Controllers\Controller;
use Core\Http\Requests\ClientIdRequest;
use Crm\Exceptions\TypeNotFoundException;
use Crm\Http\Requests\ClientIdAndFirmIdRequest;
use Crm\Http\Requests\ClientIdsAndFirmIdRequest;
use Crm\Http\Requests\CreateClientRequest;
use Crm\Http\Requests\CreateFirmRequest;
use Crm\Http\Requests\FirmIdRequest;
use Crm\Http\Requests\ShowCardRequest;
use Crm\Http\Requests\UpdateClientRequest;
use Crm\Http\Requests\UpdateFirmRequest;
use Crm\Models\Client;
use Crm\Models\Firm;
use Crm\References\CardReference;
use Crm\Services\CardService;
use Response;

/**
 * Class CardController
 * @package Crm\Http\Controllers\App
 */
class CardController extends Controller
{
    /** @var CardService */
    protected $cardService;

    public function __construct(
        CardService $cardService
    )
    {
        $this->cardService = $cardService;
    }

    public function getShow($companyAlias, $type, $id)
    {
        if (!in_array($type, CardReference::types())) {
            throw new TypeNotFoundException;
        }
        $typeId = [
            'clientId' => null,
            'firmId' => null
        ];
        if (array_get($this->cardService->types(), $type) === 'client') {
            $typeId['clientId'] = $id;
        } else {
            $typeId['firmId'] = $id;
        }

        $card = [
            'users' => $this->cardService->getUserRepository()->all(),
            'firms' => $this->cardService->getFirmRepository()->all(),
            'clients' => $this->cardService->getClientRepository()->all(),

        ];
        $card = array_merge($card, $typeId);

        return view('crm.card.index', $card);
    }

    public function ajaxAttachFirm(ClientIdsAndFirmIdRequest $request)
    {
        /** @var Firm $firm */
        $firm = $this->cardService->getFirmRepository()->find($request->get('firmId'));
        $firm->clients()->sync($request->get('clientIds'));

        return Response::json(['clients' => $firm->clients()->get()]);
    }

    /**
     * @param ClientIdAndFirmIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxAttachClient(ClientIdAndFirmIdRequest $request)
    {
        /** @var Client $client */
        $client = $this->cardService->getClientRepository()->find($request->get('clientId'));
        $client->firms()->attach($request->get('firmId'));

        return Response::json(['client' => $client]);
    }

    /**
     * @param CreateClientRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxCreateClient(CreateClientRequest $request)
    {
        /** @var Client $client */
        $client = $this->cardService->getClientRepository()->create($request->except('firmId'));
        $client->firms()->attach($request->get('firmId'));
        return Response::json(['client' => $client]);
    }

    /**
     * @param CreateFirmRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxCreateFirm(CreateFirmRequest $request)
    {
        /** @var Firm $firm */
        $firm = $this->cardService->getFirmRepository()->create($request->except('clientIds'));
        $firm->clients()->sync($request->get('clientIds'));
        return Response::json(['firm' => $firm]);
    }

    /**
     * @param FirmIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Crm\Exceptions\ClientOrFirmNotFoundException
     */
    public function ajaxGetOrdersByFirm(FirmIdRequest $request)
    {
        $orders = $this->cardService->getOrdersByClientsOrFirm($request->get('firmId'));
        return Response::json(['orders' => $orders]);
    }

    /**
     * @param ClientIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetOrdersByClientId(ClientIdRequest $request)
    {
        $orders = $this->cardService->getOrdersByClient($request->get('clientId'));
        return Response::json(['orders' => $orders]);
    }

    /**
     * @param ClientIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Crm\Exceptions\ClientOrFirmNotFoundException
     */
    public function ajaxGetTasksByClientId(ClientIdRequest $request)
    {
        $tasks = $this->cardService->getTasksByClientsOrFirm($request->get('clientId'));
        return Response::json($tasks);
    }

    /**
     * @param FirmIdRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxGetTasksByFirm(FirmIdRequest $request)
    {
        $tasks = $this->cardService->getTasksByFirm($request->get('firmId'));
        return Response::json($tasks);
    }

    public function ajaxGetStatisticsByFirm(FirmIdRequest $request)
    {
        // @TODO Statistics
    }

    /**
     * @param UpdateClientRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxUpdateClient(UpdateClientRequest $request)
    {
        /** @var Client $client */
        $client = $this->cardService->getClientRepository()->find($request->get('id'));
        $client->fill($request->except('id'));
        $client->save();
        return Response::json($client);
    }

    /**
     * @param UpdateFirmRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxUpdateFirm(UpdateFirmRequest $request)
    {
        $firm = $this->cardService->getFirmRepository()->find($request->get('id'));
        $firm->fill($request->except('id'));
        $firm->save();

        return Response::json($firm);
    }

    /**
     * @param $company_alias
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Crm\Exceptions\ClientOrFirmNotFoundException
     */
    public function ajaxGetClient($company_alias, $id)
    {
        return Response::json($this->cardService->getClientById($id));
    }
}
