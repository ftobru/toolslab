<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 14.07.15
 * Time: 10:53
 */

namespace Crm\Http\Controllers;


use Core\Http\Controllers\Controller;

/**
 * Class AnalyticsUsersController
 * @package Crm\Http\Controllers
 */
class AnalyticsUsersController extends Controller
{

    public function orders()
    {
        return view('crm.analytics.users.orders');
    }

    public function contacts()
    {
        return view('crm.analytics.users.contacts');
    }

    public function tasks()
    {
        return view('crm.analytics.users.tasks');
    }

}