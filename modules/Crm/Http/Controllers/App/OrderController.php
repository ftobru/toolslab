<?php namespace Crm\Http\Controllers\App;

use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\References\FirmReference;
use Crm\References\OrderReference;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Services\ClientService;
use Crm\Services\FirmService;
use Crm\Services\OrderService;
use Redirect;
use Response;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Services\CompanyService;
use Saas\Services\UserService;

/**
 * Class OrderController
 * @package Crm\Http\Controllers\App
 */
class OrderController extends Controller
{

    /** @var  CompanyService */
    protected $companyService;

    /** @var OrderService  */
    protected $orderService;

    /** @var  ClientService */
    protected $clientService;

    /** @var  FirmService */
    protected $firmRepository;

    /** @var  UserService */
    protected $userRepository;

    /** @var  OrderEloquentRepository */
    protected $orderRepository;

    /**
     * @param OrderService $orderService
     * @param CompanyService $companyService
     * @param OrderEloquentRepository $orderRepository
     */
    public function __construct(
        OrderService $orderService,
        CompanyService $companyService,
        ClientService $clientService,
        FirmEloquentRepository $firmRepository,
        UserEloquentRepository $userRepository,
        OrderEloquentRepository $orderRepository)
    {
        $this->orderService = $orderService;
        $this->companyService = $companyService;
        $this->orderRepository = $orderRepository;
        $this->firmRepository = $firmRepository;
        $this->clientService = $clientService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('crm.orders.index', [
                'company' => $company,
                'statuses' => OrderReference::statusesInfo(),
                'clients' => $this->clientService->getAllClients(),
                'firms' => $this->firmRepository->all(),
                'users' => $this->userRepository->all()
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getOrders(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->orderService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createOrder(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($data['models'])) {
                return Response::json(['data' => $this->orderService->createFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->orderService->createFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateOrder(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                return Response::json(['data' => $this->orderService->updateFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->orderService->updateFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteOrder(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->orderRepository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->orderRepository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
        }
        return Response::json(['Company not found'], 404);
    }


}
