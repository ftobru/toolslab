<?php namespace Crm\Http\Controllers\App;

use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\References\FirmReference;
use Crm\References\OrderReference;
use Crm\Repositories\Eloquent\CartEloquentRepository;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Services\CartService;
use Crm\Services\FirmService;
use Crm\Services\OrderService;
use Redirect;
use Response;
use Saas\Services\CompanyService;

/**
 * Class CartController
 * @package Crm\Http\Controllers\App
 */
class CartController extends Controller
{

    /** @var  CompanyService */
    protected $companyService;

    /** @var CartService  */
    protected $cartService;

    /** @var  CartEloquentRepository */
    protected $cartRepository;

    /**
     * @param CartService $cartService
     * @param CompanyService $companyService
     * @param CartEloquentRepository $cartRepository
     */
    public function __construct(CartService $cartService, CompanyService $companyService, CartEloquentRepository $cartRepository)
    {
        $this->cartService = $cartService;
        $this->companyService = $companyService;
        $this->cartRepository = $cartRepository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            return view('crm.cart.index', [
                'company' => $company,
                'statuses' => OrderReference::statusesInfo(),
            ]);
        }
        return Redirect::route('/404');
    }

    /**
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getCarts(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->cartService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createCart(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($data['models'])) {
                return Response::json(['data' => $this->cartService->createFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->cartService->createFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateCart(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                return Response::json(['data' => $this->cartService->updateFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->cartService->updateFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param AjaxCreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteCart(AjaxCreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->cartRepository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->cartRepository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
        }
        return Response::json(['Company not found'], 404);
    }


}
