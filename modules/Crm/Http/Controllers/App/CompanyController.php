<?php namespace Crm\Http\Controllers\App;

use App;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Redirect;
use Response;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Eloquent\Criteria\Company\NameCriteria;
use Saas\Services\CompanyService;
use Saas\Services\StateApplicationService;

/**
 * Class DashboardController
 * @package Crm\Http\Controllers\App
 */
class CompanyController extends Controller
{
    /** @var  CompanyService */
    protected $companyService;

    /** @var  CompanyEloquentRepository */
    protected $companyRepository;


    /**
     * @param CompanyService $companyService
     * @param CompanyEloquentRepository $repository
     */
    public function __construct(CompanyService $companyService, CompanyEloquentRepository $repository)
    {
        $this->companyService = $companyService;
        $this->companyRepository = $repository;
    }



    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function employees($company_alias)
    {
        /** @var StateApplicationService $stateService */
        $stateApp = App::make('StateApplicationService');
        if ($company = $stateApp->getCurrentCompany()) {
            return view('saas.users.index', [
                'company' => $company,
                'statuses' => [],
            ]);
        }
    }

    public function postAjaxSendInvitionEmail()
    {

    }
}