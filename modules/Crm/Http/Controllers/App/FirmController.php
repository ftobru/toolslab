<?php namespace Crm\Http\Controllers\App;

use AccessService;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\App\Firm\CreateFirmRequest;
use Crm\Http\Requests\App\Firm\DeleteFirmRequest;
use Crm\Http\Requests\App\Firm\UpdateFirmRequest;
use Crm\Http\Requests\Rest\AjaxCreateFirmRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\Models\Firm;
use Crm\References\FirmReference;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Services\FirmService;
use Redirect;
use Response;
use Config;
use Saas\Models\User;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\Criteria\FormsEntity\CompanyIdCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Services\CompanyService;

/**
 * Class FirmController
 * @package Crm\Http\Controllers\App
 */
class FirmController extends Controller
{

    /** @var  CompanyService */
    protected $companyService;

    /** @var FirmService  */
    protected $firmService;

    /** @var  FirmEloquentRepository */
    protected $firmRepository;

    /** @var  FormEloquentRepository */
    protected $formRepository;

    /**
     * @param FirmService $firmService
     * @param CompanyService $companyService
     * @param FirmEloquentRepository $firmRepository
     * @param FormEloquentRepository $formRepository
     */
    public function __construct(
        FirmService $firmService,
        CompanyService $companyService,
        FirmEloquentRepository $firmRepository,
        FormEloquentRepository $formRepository
    ) {
        $this->firmService = $firmService;
        $this->companyService = $companyService;
        $this->firmRepository = $firmRepository;
        $this->formRepository = $formRepository;
    }

    /**
     * Firms grid.
     *
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        if (AccessService::canPermission('crm.firms.view.all') ||
            AccessService::canPermission('crm.firms.view.their')
        ) {
            $company = $this->companyService->getByName($company_alias);
            if ($company) {
                $fields = $this->formRepository
                    ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Firm::class)))
                    ->pushCriteria(new CompanyIdCriteria($company->id))
                    ->all();

                foreach ($fields as $field) {
                    if ($field->form_type === 1) {
                        $field->type = 'string';
                    } else {
                        $field->type = 'string';
                    }
                }

                return view('crm.firms.index', [
                    'company' => $company,
                    'types' => FirmReference::typesInfo(),
                    'statuses' => FirmReference::statusesInfo(),
                    'customFields' => $fields,
                ]);
            }
            abort(404);
        } else {
            abort(403);
        }
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function fields($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    $fields = $this->formRepository
                        ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Firm::class)))
                        ->pushCriteria(new CompanyIdCriteria($company->id))
                        ->all();
                    foreach ($fields as $field) {
                        if ($field->form_type === 1) { // TODO
                            $field->type = 'string';
                            $field->form_type_label = 'Текстовое поле';
                        } else {
                            $field->type = 'string';
                            $field->form_type_label = 'Текстовый поле';
                        }
                    }
                    return view('crm.firms.fields', [
                        'company' => $company,
                        'customFields' => $fields,
                    ]);
                }
            }
            return Redirect::to('/401');
        }
        return Redirect::to('/404');
    }

    /**
     * Права через глобал скоуп.
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getFirms(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->firmService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param CreateFirmRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createFirm(CreateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($data['models'])) {
                return Response::json(['data' => $this->firmService->createFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->firmService->createFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateFirm(UpdateFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                return Response::json(['data' => $this->firmService->updateFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->firmService->updateFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param DeleteFirmRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteFirm(DeleteFirmRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->firmRepository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->firmRepository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
