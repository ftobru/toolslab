<?php namespace Crm\Http\Controllers\App;

use App;
use Core\Http\Controllers\Controller;
use Redirect;
use Saas\Models\User;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Eloquent\Criteria\Company\NameCriteria;
use Saas\Services\CompanyService;
use Saas\Services\StateApplicationService;

/**
 * Class DashboardController
 * @package Crm\Http\Controllers\App
 */
class DashboardController extends Controller
{
    /** @var  CompanyService */
    protected $companyService;

    /** @var  CompanyEloquentRepository */
    protected $companyRepository;


    /**
     * @param CompanyService $companyService
     * @param CompanyEloquentRepository $repository
     */
    public function __construct(CompanyService $companyService, CompanyEloquentRepository $repository)
    {
        $this->companyService = $companyService;
        $this->companyRepository = $repository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function index($company_alias)
    {
        /** @var StateApplicationService $stateService */
        $stateApp = App::make('StateApplicationService');

        /** @var User $user */
        $user = \Auth::getUser();
        if ($company = $stateApp->getCurrentCompany()) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    return view('dashboard.dashboard', ['company' => $company]);
                }
            }
            return Redirect::route('401');
        }
        return Redirect::route('404');
    }
}
