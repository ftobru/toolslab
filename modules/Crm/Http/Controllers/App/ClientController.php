<?php namespace Crm\Http\Controllers\App;

use AccessService;
use Core\Http\Controllers\Controller;
use Crm\Http\Requests\App\Client\CreateClientRequest;
use Crm\Http\Requests\App\Client\DeleteClientRequest;
use Crm\Http\Requests\App\Client\UpdateClientRequest;
use Crm\Http\Requests\Rest\AjaxProductRequest;
use Crm\Models\Client;
use Crm\References\ClientReference;
use Crm\Repositories\Eloquent\ClientEloquentRepository;
use Crm\Services\ClientService;
use Redirect;
use Response;
use Config;
use Saas\Models\User;
use Saas\Repositories\Eloquent\Criteria\Forms\CompanyIdCriteria;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;
use Saas\Services\CompanyService;
use View;

/**
 * Class ClientController
 * @package Crm\Http\Controllers\App
 */
class ClientController extends Controller
{

    /** @var  CompanyService */
    protected $companyService;

    /** @var ClientService  */
    protected $clientService;

    /** @var  ClientEloquentRepository */
    protected $clientRepository;

    /** @var  FormEloquentRepository */
    protected $formRepository;

    /**
     * @param ClientService $clientService
     * @param CompanyService $companyService
     * @param ClientEloquentRepository $clientRepository
     * @param FormEloquentRepository $formRepository
     */
    public function __construct(
        ClientService $clientService,
        CompanyService $companyService,
        ClientEloquentRepository $clientRepository,
        FormEloquentRepository $formRepository
    ) {
        $this->clientService = $clientService;
        $this->companyService = $companyService;
        $this->clientRepository = $clientRepository;
        $this->formRepository = $formRepository;
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     * @internal param AjaxFirmRequest $request
     */
    public function index($company_alias)
    {
        if (AccessService::canPermission('crm.contacts.view.all') ||
            AccessService::canPermission('crm.contacts.view.their')
        ) {
            $company = $this->companyService->getByName($company_alias);
            if ($company) {
                $fields = $this->formRepository
                    ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Client::class)))
                    ->pushCriteria(new CompanyIdCriteria($company->id))
                    ->all();

                foreach ($fields as $field) {
                    if ($field->form_type === 1) {
                        $field->type = 'string';
                    } else {
                        $field->type = 'string';
                    }
                }
                return view('crm.clients.index', [
                    'company' => $company,
                    'types' => ClientReference::typesInfo(),
                    'statuses' => ClientReference::statusesInfo(),
                    'customFields' => $fields,
                ]);
            }
            abort(404);
        }
        abort(403);
    }

    /**
     * Права через глобал скоуп.
     *
     * @param AjaxProductRequest $request
     * @param $company_alias
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getClients(AjaxProductRequest $request, $company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            $result = $this->clientService->activeRepository($request->get('options', []));
            return Response::json([
                'data' => $result['data'],
                'total' => $result['total'],
                'filters_data' => $result['filters_data'],
                'groups' => array_get($result, 'groups', [])
            ]);
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param CreateClientRequest $request
     * @param $company_alias
     * @return mixed
     * @internal param $company_alias
     */
    public function createClient(CreateClientRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);
        if ($company) {
            if (isset($data['models'])) {
                return Response::json(['data' => $this->clientService->createFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->clientService->createFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param UpdateClientRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function updateClient(UpdateClientRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            $products = [];
            if (isset($data['models'])) {
                return Response::json(['data' => $this->clientService->updateFromModels($data['models'], $company->id)]);
            }
            if (isset($data['model'])) {
                return Response::json(['data' => $this->clientService->updateFromModel($data['model'], $company->id)]);
            }
        }
        return Response::json(['Company not found'], 404);
    }

    /**
     * @param $company_alias
     * @return \Illuminate\View\View
     */
    public function fields($company_alias)
    {
        $company = $this->companyService->getByName($company_alias);
        /** @var User $user */
        $user = \Auth::getUser();
        if ($company) {
            $userCompanies = $user->companies()->get();
            foreach ($userCompanies as $c) {
                if ($c->id === $company->id) {
                    $fields = $this->formRepository
                        ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Client::class)))
                        ->pushCriteria(new CompanyIdCriteria($company->id))
                        ->all();
                    foreach ($fields as $field) {
                        if ($field->form_type === 1) { // TODO
                            $field->type = 'string';
                            $field->form_type_label = 'Текстовое поле';
                        } else {
                            $field->type = 'string';
                            $field->form_type_label = 'Текстовый поле';
                        }
                    }
                    return view('crm.clients.fields', [
                        'company' => $company,
                        'customFields' => $fields,
                    ]);
                }
            }
            return Redirect::to('/401');
        }
        return Redirect::to('/404');
    }

    /**
     * @param DeleteClientRequest $request
     * @param $company_alias
     * @return mixed
     */
    public function deleteClient(DeleteClientRequest $request, $company_alias)
    {
        $data = $request->all();
        $company = $this->companyService->getByName($company_alias);

        if ($company) {
            if (isset($data['models'])) {
                foreach ($data['models'] as $model) {
                    if (isset($model['id'])) {
                        $this->clientRepository->delete(intval($model['id']));
                    }
                }
                return Response::json($data['models']);
            }
            if (isset($data['model'])) {
                $model = $data['model'];
                if (isset($model['id'])) {
                    $this->clientRepository->delete(intval($model['id']));
                }
                return Response::json($data['model']);
            }
        }
        return Response::json(['Company not found'], 404);
    }
}
