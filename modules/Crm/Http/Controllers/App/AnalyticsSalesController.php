<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 14.07.15
 * Time: 10:55
 */

namespace Crm\Http\Controllers\App;


use Core\Http\Controllers\Controller;

/**
 * Class AnalyticsSalesController
 * @package Crm\Http\Controllers\App
 */
class AnalyticsSalesController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
        return view('crm.analytics.sales.index');
    }
}