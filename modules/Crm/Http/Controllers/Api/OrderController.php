<?php namespace Crm\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use Crm\Http\Requests\Rest\CompanyIdOrderRequest;
use Crm\Http\Requests\Rest\UpdateOrderRequest;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Crm\Services\OrderService;
use Response;
use Illuminate\Http\Request;

/**
 * Class OrderController
 * @package Crm\Http\Controllers\Api
 */
class OrderController extends Controller
{

    /** @var OrderService  */
    protected $service;

    /** @var OrderEloquentRepository */
    protected $repository;


    /**
     * @param OrderService $orderService
     * @param OrderRepositoryInterface $repository
     */
    public function __construct(OrderService $orderService, OrderRepositoryInterface $repository)
    {
        $this->service = $orderService;
        $this->repository = $repository;

    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        $orders = $this->service->activeRepository($request->all());
        return Response::api(['orders' => $orders]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['order' => $this->service->getOrderWithCriteria($id)]);
    }

    /**
     * @param UpdateOrderRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateOrderRequest $request)
    {
        return Response::api(['order' => $this->repository->create($request->all())]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateOrderRequest $request, $id)
    {
        return Response::api(['order' => $this->repository->update($request->all(), $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['order' => $this->repository->delete($id)]);
    }
}
