<?php namespace Crm\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use Crm\Http\Requests\Rest\UpdateClientRequest;
use Crm\Repositories\Eloquent\ClientEloquentRepository;
use Crm\Repositories\Interfaces\ClientRepositoryInterface;
use Crm\Services\ClientService;
use Input;
use Response;

/**
 * Class ClientController
 * @package App\Http\Controllers\Api
 */
class ClientController extends Controller
{

    /** @var ClientService  */
    protected $service;
    /** @var  ClientEloquentRepository */
    protected $repository;

    /**
     * @param ClientService $userService
     * @param ClientRepositoryInterface $repositoryInterface
     */
    public function __construct(ClientService $userService, ClientRepositoryInterface $repositoryInterface)
    {
        $this->service = $userService;
        $this->repository = $repositoryInterface;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        $clients = $this->service->activeRepository($request->get('metaData', []));
        return Response::api(['clients' => $clients]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $client = $this->repository->find($id);
        return Response::api(['client' => $client]);
    }

    /**
     * @param UpdateClientRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateClientRequest $request)
    {
        $data = $request->all();
        return Response::api(['client' => $this->repository->create($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateClientRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateClientRequest $request, $id)
    {
        $data = $request->all();
        return Response::api(['client' => $this->repository->update($data, $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['client' => $this->repository->delete($id)]);
    }
}
