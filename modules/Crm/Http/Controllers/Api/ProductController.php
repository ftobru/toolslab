<?php namespace Crm\Http\Controllers\Api;


use Core\Http\Requests\PaginationRequest;
use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Crm\Repositories\Eloquent\ProductEloquentRepository;
use Crm\Repositories\Interfaces\ProductRepositoryInterface;
use Saas\Http\Requests\Rest\UpdateProductRequest;
use Core\Services\ProductService;
use Response;

/**
 * Class ProductController
 * @package Core\Http\Controllers\Api
 */
class ProductController extends Controller
{

    /** @var ProductService  */
    protected $service;
    /** @var ProductEloquentRepository  */
    protected $repository;

    /**
     * @param ProductService $userService
     * @param ProductRepositoryInterface $productEloquentRepository
     */
    public function __construct(ProductService $userService, ProductRepositoryInterface $productEloquentRepository)
    {
        $this->service = $userService;
        $this->repository = $productEloquentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param PaginationRequest $request
     * @return Response
     */
    public function index(PaginationRequest $request)
    {
        $products = $this->service->activeRepository($request->get('metaData', []));
        return Response::api(['products' => $products]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $product = $this->repository->find($id);
        return Response::api(['product' => $product]);
    }

    /**
     * @param UpdateProductRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateProductRequest $request)
    {
        return Response::api(['product' => $this->repository->create($request->all())]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        return Response::api(['product' => $this->repository->update($request->all(), $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['product' => $this->repository->delete($id)]);
    }

}
