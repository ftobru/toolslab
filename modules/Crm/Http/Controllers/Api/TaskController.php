<?php namespace Crm\Http\Controllers\Api;

use Core\Http\Requests;
use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use Crm\Http\Requests\Rest\CompanyIdTaskRequest;
use Crm\Http\Requests\Rest\UpdateTaskRequest;
use Crm\References\TaskReference;
use Crm\Repositories\Eloquent\TaskEloquentRepository;
use Crm\Repositories\Interfaces\TaskRepositoryInterface;
use Crm\Services\TaskService;
use Response;
use Illuminate\Http\Request;

/**
 * Class TaskController
 * @package App\Http\Controllers\Api
 */
class TaskController extends Controller
{

    /** @var TaskService  */
    protected $service;

    /** @var  TaskEloquentRepository */
    protected $repository;

    /**
     * @param TaskService $taskService
     * @param TaskRepositoryInterface $repositoryInterface
     */
    public function __construct(TaskService $taskService, TaskRepositoryInterface $repositoryInterface)
    {
        $this->service = $taskService;
        $this->repository = $repositoryInterface;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        $tasks = $this->service->activeRepository($request->get('metaData', []));
        return Response::api(['tasks' => $tasks]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['task' => $this->service->getTaskWithCriteria($id)]);
    }

    /**
     * @param UpdateTaskRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateTaskRequest $request)
    {
        $data = $request->all();
        $result = $this->checkRequestParams($data);

        if ($result['result']) {
            return Response::api(['task' => $this->repository->create($data)]);
        }
        return Response::api(['task' => $result]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTaskRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateTaskRequest $request, $id)
    {
        $data = $request->all();
        $result = $this->checkRequestParams($data);
        if ($result['result']) {
            return Response::api(['task' => $this->repository->update($data, $id)]);
        }
        return Response::api(['task' => $result]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['task' => $this->repository->delete($id)]);
    }

    /**
     * Проверка корректности входных данных.
     *
     * @param $data array
     * @return array
     */
    private function checkRequestParams(array $data)
    {
        $states = TaskReference::states();
        if (!in_array($data['state'], $states)) {
            return ['result' => false, 'error' => TaskReference::ERROR_STATE_NOT_EXIST];
        }
        return ['result' => true];
    }
}
