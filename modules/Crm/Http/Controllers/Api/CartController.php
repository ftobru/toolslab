<?php namespace Crm\Http\Controllers\Api;

use Core\Http\Controllers\Controller;
use Core\Http\Requests\PaginationRequest;
use Crm\Http\Requests\Rest\UpdateCartRequest;
use Crm\Repositories\Eloquent\CartEloquentRepository;
use Crm\Repositories\Interfaces\CartRepositoryInterface;
use Crm\Services\CartService;
use Input;
use Response;

/**
 * Class CartController
 * @package Crm\Http\Controllers\Api
 */
class CartController extends Controller
{

    /** @var  CartEloquentRepository */
    protected $repository;

    protected $service;

    public function __construct(CartRepositoryInterface $cartRepositoryInterface, CartService $cartService)
    {
        $this->repository = $cartRepositoryInterface;
        $this->service = $cartService;
    }

    /**
     * @param PaginationRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PaginationRequest $request)
    {
        $carts = $this->service->activeRepository($request->get('metaData', []));
        return Response::api(['carts' => $carts]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Response::api(['cart' => $this->repository->find($id)]);
    }

    /**
     * @param UpdateCartRequest $request
     * @return Response
     * @internal param array $data
     */
    public function store(UpdateCartRequest $request)
    {
        $data = $request->all();
        $this->service->prepareData($data);
        return Response::api(['cart' => $this->repository->create($data)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCartRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateCartRequest $request, $id)
    {
        $data = $request->all();
        $this->service->prepareData($data);
        return Response::api(['cart' => $this->repository->update($data, $id)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        return Response::api(['cart' => $this->repository->delete($id)]);
    }
}
