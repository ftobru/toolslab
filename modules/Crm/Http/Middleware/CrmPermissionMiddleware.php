<?php namespace Crm\Http\Middleware;

use Saas\References\UserReference;
use Saas\Models\User;
use Saas\Services\UserService;
use Auth;
use AccessService;
use Closure;
use Illuminate\Http\Request as Request;
use Response;
use Lang;

/**
 * Class CrmPermissionMiddleware
 * @package Crm\Http\Middleware
 */
class CrmPermissionMiddleware
{
    /** @var UserService  */
    protected $service;

    protected $actions = [
        'view', 'create', 'edit', 'delete',
        'menu', 'employee', 'deals', 'fields',
        'roles', 'tags-statuses', 'modules',
        'api', 'invite'
    ];

    protected $components = [
        'products',
        'contacts',
        'firms',
        'deals',
        'tasks',
        'settings',
        'analytics',
        'employees',
        'dashboard'
    ];

    protected $permissionGrades = ['all', 'their'];

    protected $modulePrefix = 'crm';

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param Request $request
     * @param callable $next
     * @param $moduleComponent
     * @param $action
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $moduleComponent, $action)
    {
        if (in_array($moduleComponent, $this->components) && in_array($action, $this->actions)) {
            $permission_name = $this->modulePrefix . '.' . $moduleComponent . '.' . $action;
            if (AccessService::canPermission($permission_name) ||
                $this->checkPermGrades($permission_name)
            ) {
                return $next($request);
            }
        }
        abort(403);
    }

    /**
     * Проверка вариаций прав.
     *
     * @param $permission_name
     * @return bool
     */
    private function checkPermGrades($permission_name)
    {
        $permissionGrades = Lang::get('permissions-options', [], 'ru');
        $postfix_values = [];
        foreach ($permissionGrades as $key => $val) {
            if (isset($val['postfix'])) {
                $postfix_values[] = $val['postfix'];
            }
        }

        foreach ($postfix_values as $grade) {
            if (AccessService::canPermission($permission_name . $grade)) {
                return true;
            }
        }
        return false;
    }
}
