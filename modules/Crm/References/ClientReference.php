<?php namespace Crm\References;

/**
 * Class ClientReference
 * @package Crm\References
 */
class ClientReference
{
    const STATUS_BLOCK = -1;
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_NORMAL = 0;
    const TYPE_NEW = 1;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_BLOCK,
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public static function statusesInfo()
    {
        return [
            'Заблокированый' => self::STATUS_BLOCK,
            'Неактивный' => self::STATUS_NO_ACTIVE,
            'Активный' => self::STATUS_ACTIVE,
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_NORMAL,
            self::TYPE_NEW
        ];
    }

    /**
     * @return array
     */
    public static function typesInfo()
    {
        return [
            'Обычный' => self::TYPE_NORMAL,
            'Новый' => self::TYPE_NEW,
        ];
    }
}
