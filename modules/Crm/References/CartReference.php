<?php namespace Crm\References;

/**
 * Class CartReference
 * @package Crm\References
 */
class CartReference
{

    const ERROR_PRODUCTS_NOT_SET = 202;

    /**
     * @return array
     */
    public function errors()
    {
        return [
            self::ERROR_PRODUCTS_NOT_SET
        ];
    }
}
