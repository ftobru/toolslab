<?php namespace Crm\References;
use Crm\Repositories\Eloquent\StatusesOrderEloquentRepository;
use Crm\Repositories\Interfaces\StatusesOrderRepositoryInterface;

/**
 * Class OrderReference
 * @package Crm\References
 */
class OrderReference
{
    // Все кастомные статусы начинаются с этого числа
    const MIN_CUSTOM_STATUS = 1000;

    const STATUS_NEW = 0;
    const STATUS_DELETE = 1;
    const STATUS_CLOSE = 2;
    /** @var  StatusesOrderEloquentRepository */
    protected $statusesRepository;

    /**
     * @param \Crm\Repositories\Interfaces\StatusesOrderRepositoryInterface $statusesOrderRepositoryInterface
     */
    public function __construct(StatusesOrderRepositoryInterface $statusesOrderRepositoryInterface)
    {
        $this->statusesRepository = $statusesOrderRepositoryInterface;
    }

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_DELETE,
            self::STATUS_CLOSE
        ];
    }

    public static function statusesLabels()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_DELETE => 'Удален',
            self::STATUS_CLOSE => 'Закрыт'
        ];
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        $customStatus = $this->statusesRepository->all()->first();
        return array_merge(self::statusesLabels(), object_get($customStatus, 'statuses', []));
    }

    /**
     * @return array
     */
    public static function statusesInfo()
    {
        return [
            'Новый' => self::STATUS_NEW,
            'Удален' => self::STATUS_DELETE,
            'Закрыт' => self::STATUS_CLOSE
        ];
    }
}
