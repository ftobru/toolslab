<?php namespace Crm\References;

/**
 * Class FirmReference
 * @package Crm\References
 */
class FirmReference
{
    const STATUS_NO_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const TYPE_NORMAL = 0;
    const TYPE_NEW = 1;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }

    /**
     * @return array
     */
    public static function statusesInfo()
    {
        return [
            'Неактивный' => self::STATUS_NO_ACTIVE,
            'Активный' => self::STATUS_ACTIVE,
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_NORMAL,
            self::TYPE_NEW
        ];
    }

    /**
     * @return array
     */
    public static function typesInfo()
    {
        return [
            'Обычная' => self::TYPE_NORMAL,
            'Новая' => self::TYPE_NEW,
        ];
    }
}
