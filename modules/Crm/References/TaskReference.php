<?php namespace Crm\References;

/**
 * Class TaskReference
 * @package Crm\Reference
 */
class TaskReference
{
    /** Новая задача */
    const STATE_NEW = 1;
    /** Задача выполнена */
    const STATE_DONE = 2;
    /** Задача удалена */
    const STATE_OVERDUE = 0;

    const TYPE_NORMAL = 0;

    const ERROR_STATE_NOT_EXIST = 201;

    /**
     * Состояния задач.
     *
     * @return array
     */
    public static function states()
    {
        return [
            self::STATE_NEW,
            self::STATE_DONE,
            self::STATE_OVERDUE
        ];
    }

    /**
     *  Состояния задач.
     *
     * @return array
     */
    public static function statesInfo()
    {
        return [
            'Новая' => self::STATE_NEW,
            'Завершена' => self::STATE_DONE,
            'Просрочена' => self::STATE_OVERDUE
        ];
    }

    /**
     * @return array
     */
    public static function typesInfo()
    {
        return [
            'Нормальный' => self::TYPE_NORMAL
        ];
    }


    /**
     * @return array
     */
    public function errors()
    {
        return [
            self::ERROR_STATE_NOT_EXIST
        ];
    }

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_NORMAL
        ];
    }
}
