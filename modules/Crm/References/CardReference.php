<?php namespace Crm\References;
/**
 * Created by PhpStorm.
 * User: office
 * Date: 30.06.15
 * Time: 11:16
 */


/**
 * Class CardReference
 */
class CardReference
{
    const TYPE_CLIENT = 0;

    const TYPE_FIRM = 1;

    public static $factoryType = 1;

    /**
     * @return array
     */
    public static function types()
    {
        return [
            self::TYPE_CLIENT,
            self::TYPE_FIRM
        ];
    }
}