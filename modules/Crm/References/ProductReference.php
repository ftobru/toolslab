<?php namespace Crm\References;

/**
 * Class ProductReference
 * @package Crm\References
 */
class ProductReference
{
    const STATUS_BLOCK = -1;

    const STATUS_NO_ACTIVE = 0;

    const STATUS_ACTIVE = 1;

    /**
     * @return array
     */
    public static function statuses()
    {
        return [
            self::STATUS_BLOCK,
            self::STATUS_NO_ACTIVE,
            self::STATUS_ACTIVE
        ];
    }
}
