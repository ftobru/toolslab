<?php namespace Crm;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as EvServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;

class EventServiceProvider extends EvServiceProvider
{
    protected $listen = [
        'Crm\Events\OrderCreatedEvent' => [
            'Crm\Handlers\Events\CreateTaskWasCreatedOrderHandler@handle'
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  Dispatcher  $events
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        parent::boot($events);

        //
    }
}
