<?php namespace Crm\Handlers\Events;

use Crm\Events\OrderCreatedEvent;

use Crm\References\TaskReference;
use Crm\Services\TaskService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class CreateTaskWasCreatedOrderHandler implements ShouldBeQueued
{
    use InteractsWithQueue;

    protected $taskService;

    /**
     * Create the event handler.
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreatedEvent  $event
     * @return void
     */
    public function handle(OrderCreatedEvent $event)
    {
        \Log::debug('Create order handle ---->');
        $order = $event->getEntity();
        $data = [
            'order_id' => $order->id,
            'client_id' => $order->client_id,
            'state' => TaskReference::STATE_NEW,
            'date_perf' => new \DateTime(),
            'responsible_user_id' => $order->company()->user_id,
            'performer_user_id' => $order->company()->user_id,
            'company_id' => $order->company_id
        ];

        $this->taskService->create($data);
    }
}
