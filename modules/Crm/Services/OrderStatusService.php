<?php namespace Crm\Services;

use Crm\Exceptions\StatusIdentConflictException;
use Crm\References\OrderReference;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Eloquent\StatusesOrderEloquentRepository;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Crm\Repositories\Interfaces\StatusesOrderRepositoryInterface;
use Crm\Exceptions\ExistOrderToThisStatusException;
use Saas\Services\StateApplicationService;

/**
 * Class OrderStatusService
 * @package Crm\Services
 */
class OrderStatusService
{
    /** @var StatusesOrderEloquentRepository  */
    protected $repository;

    /** @var  OrderEloquentRepository */
    protected $orderRepository;

    /** @var array|mixed|null  */
    protected $currentAvailableStatuses = [];

    /** @var  StateApplicationService */
    protected $stateApplication;
    /**
     * @param StatusesOrderRepositoryInterface $repositoryInterface
     * @param OrderRepositoryInterface $orderEloquentRepository
     * @param StateApplicationService $applicationService
     */
    public function __construct(
        StatusesOrderRepositoryInterface $repositoryInterface,
        OrderRepositoryInterface $orderEloquentRepository,
        StateApplicationService $applicationService
    ) {
        /** @var StatusesOrderEloquentRepository repository */
        $this->repository = $repositoryInterface;
        /** @var OrderEloquentRepository orderRepository */
        $this->orderRepository = $orderEloquentRepository;

        /** @var  stateApplication */
        $this->stateApplication = $applicationService;
    }

    /**
     * @return array|mixed|null
     */
    public function getAvailableStatuses()
    {
        if($record = last($this->repository->all())) {
            return $this->currentAvailableStatuses = $record;
        } else {
            return $this->currentAvailableStatuses = $this->repository->create([

            ]);
        }
    }


    /**
     * Статусы с лабелами
     * @return array
     */
    public function getStatuses()
    {
        $customStatuses = $this->getAvailableStatuses()->statuses;
        $statuses = OrderReference::statusesLabels();
        foreach ($customStatuses as $key => $value) {
            $statuses = array_add($statuses, $key, $value);
        }
        return $statuses;
    }

    /**
     * @param $statusId
     * @return bool|mixed
     * @throws ExistOrderToThisStatusException
     * @throws StatusIdentConflictException
     */
    public function deleteStatuesByStatusId($statusId)
    {
        /** */
        if ($this->isAvailableStatus($statusId) && $this->isExistOrderByStatus($statusId)) {
            // Тут все не просто так, сделано это именно так из-за ошибки "Indirect notification of overloaded"
            $statusesEntity = $this->getAvailableStatuses();
            $statuses = $statusesEntity->statuses;
            unset($statuses[$statusId]);
            $statusesEntity->statuses = $statuses;
            return $this->repository->update(
                $statusesEntity->toArray(),
                $statusesEntity->id
            );
        } else {
            return false;
        }
    }

    /**
     * @param $statusId
     * @return bool
     * @throws StatusIdentConflictException
     */
    private function isAvailableStatus($statusId)
    {
        // Минимальный идентификатор для идентификатора статуса
        if ($statusId < OrderReference::MIN_CUSTOM_STATUS) {
            throw new StatusIdentConflictException;
        }
        return true;
    }

    /**
     * Добавить статус
     * @param $label
     * @return mixed
     */
    public function addStatus($label)
    {
        $key = (empty($this->getAvailableStatuses()->statuses)
            ? OrderReference::MIN_CUSTOM_STATUS
            : max($this->getAvailableStatuses()->statuses));

        $this->currentAvailableStatuses->statuses = array_add(
            $this->getAvailableStatuses()->statuses,
            $key,
            $label
        );

        return $this->repository->update(
            $this->currentAvailableStatuses->toArray(),
            $this->getAvailableStatuses()->id
        );
    }

    /**
     * @param $statusId
     * @return bool
     * @throws ExistOrderToThisStatusException
     */
    private function isExistOrderByStatus($statusId)
    {
        $order = $this->orderRepository->findBy('status', $statusId);
        if ($order) {
            // Если заказ с таким статусом уже есть, бросаем исключение
            throw new ExistOrderToThisStatusException;
        }
        return true;
    }
}