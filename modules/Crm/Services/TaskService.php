<?php namespace Crm\Services;

use App;
use Auth;
use Billing\Repositories\Eloquent\Criteria\StatusCriteria;
use Carbon\Carbon;
use Crm\References\OrderReference;
use Crm\References\TaskReference;
use Crm\Repositories\Criteria\Task\WithAllRelationCriteria;
use Crm\Models\Task;
use Crm\Repositories\Eloquent\Criteria\Task\ClientIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\CommentCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\CompanyIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\FirmIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\OrderIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\PerformerUserIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\PerfUserIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\ResponsibleUserIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\RespUserIdIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\StateCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\TimeCompareCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\TypeCriteria;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Eloquent\TaskEloquentRepository;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Crm\Repositories\Interfaces\TaskRepositoryInterface;
use Ftob\Repositories\Criteria\CriteriaCollection;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Core\Traits\Taggable;

/**
 * Class TaskService
 * @package App\Services
 */
class TaskService
{

    use Sortable, Paginatable, Taggable;

    /** @var TaskEloquentRepository */
    protected $repository;
    /** @var OrderEloquentRepository */
    protected $orderRepository;

    /**
     * @param TaskRepositoryInterface $repository
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        TaskRepositoryInterface $repository,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->repository = $repository;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param $taskId
     * @return mixed
     */
    public function getTaskWithCriteria($taskId)
    {
        $this->repository->find($taskId);
        $this->repository->pushCriteria(new WithAllRelationCriteria());
        return $this->repository->all();
    }


    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        //        $this->filterByTags($orders, $metaData, 'Crm\Models\Order');

        $fullList = $this->repository->all();
        $states = TaskReference::statesInfo();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'states' => $states,
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'client_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new ClientIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'firm_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new FirmIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'resp_user_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new RespUserIdIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'perf_user_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new PerfUserIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'order_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new OrderIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'date_perf') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('date_perf', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('date_perf', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'state') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new StateCriteria($filter['value']);
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'comment') {
                    if ($filter['operator'] === 'startWith') {
                        $newCriteria = new CommentCriteria($filter['value']);
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems->count();

        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1)) - 1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        $this->repository->getWithAllRelation();

        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'states' => $states,
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * My tasks for tile view.
     *
     * @param $metaData
     * @return array
     */
    public function getTilesData($metaData)
    {
        $criteriaList = [];

        $newCriteria = new PerfUserIdCriteria(Auth::getUser()->id);
        $criteriaList[] = $newCriteria;

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'client_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new ClientIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                    }
                }
                if ($filter['field'] === 'firm_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new FirmIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                    }
                }
                if ($filter['field'] === 'resp_user_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new RespUserIdIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                    }
                }
                if ($filter['field'] === 'order_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new OrderIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                    }
                }
                if ($filter['field'] === 'date_perf') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('date_perf', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('date_perf', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                    }
                }
                if ($filter['field'] === 'comment') {
                    if ($filter['operator'] === 'startWith') {
                        $newCriteria = new CommentCriteria($filter['value']);
                        $criteriaList[] = $newCriteria;
                    }
                }
            }
        }

        $this->repository->getWithAllRelation();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $overdue = $this->repository->pushCriteria(new StateCriteria(TaskReference::STATE_OVERDUE))
            ->all()->toArray();

        $this->repository->getWithAllRelation();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $active = $this->repository->pushCriteria(new StateCriteria(TaskReference::STATE_NEW))
            ->all()->toArray();

        $this->repository->getWithAllRelation();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $completed = $this->repository->pushCriteria(new StateCriteria(TaskReference::STATE_DONE))
            ->all()->toArray();

        foreach ($overdue as $key => $value) {
            $date_perf = new Carbon($value['date_perf']);
            Carbon::setLocale('ru');
            $now = Carbon::now();
            $difference = $now->diffForHumans($date_perf);
            $overdue[$key]['head_date'] = $difference;
            $overdue[$key]['formatted_date'] = date("d.m.Y", strtotime($overdue[$key]['date_perf']))
                . ' в '
                . date("G:i", strtotime($overdue[$key]['date_perf']));
        }

        foreach ($active as $key => $value) {
            $date_perf = new Carbon($value['date_perf']);
            Carbon::setLocale('ru');
            $now = Carbon::now();
            $difference = $now->diffForHumans($date_perf);
            $active[$key]['head_date'] = $difference;
            $active[$key]['formatted_date'] = date("d.m.Y", strtotime($active[$key]['date_perf']))
                . ' в '
                . date("G:i", strtotime($active[$key]['date_perf']));
        }

        foreach ($completed as $key => $value) {
            $date_perf = new Carbon($value['date_perf']);
            Carbon::setLocale('ru');
            $now = Carbon::now();
            $difference = $now->diffForHumans($date_perf);
            $completed[$key]['head_date'] = $difference;
            $completed[$key]['formatted_date'] = date("d.m.Y", strtotime($completed[$key]['updated_at']))
                . ' в '
                . date("G:i", strtotime($completed[$key]['updated_at']));
        }

        return ['overdue' => $overdue, 'active' => $active, 'completed' => $completed];
    }

    /**
     * создание таска по AJAX
     *
     * @param array $all
     * @return mixed
     */
    public function createNewTask(array $all)
    {
        $order = $this->orderRepository->find($all['order_id']);
        if (in_array($all['type'], TaskReference::types()) &&
            in_array($all['state'], TaskReference::states())
        ) {
            $data = [
                'order_id' => intval($all['order_id']),
                'type' => intval($all['type']),
                'state' => intval($all['state']),
                'date_perf' => new \DateTime($all['date_perf']),
                'performer_user_id' => intval($all['performer_user_id']),
                'responsible_user_id' => intval($all['responsible_user_id']),
                'comment' => $all['comment'],
                'client_id' => $order->client_id ? intval($order->client_id) : null,
                'firm_id' => $order->firm_id ? intval($order->firm_id) : null
            ];
            return $this->repository->create($data);
        } else {
            abort(422);
        }
    }

    /**
     * Update task via AJAX
     *
     * @param array $all
     * @return mixed
     */
    public function updateSomeTask(array $all)
    {
        $order = $this->orderRepository->find($all['order_id']);
        if (in_array($all['type'], TaskReference::types()) &&
            in_array($all['state'], TaskReference::states())
        ) {
            $data = [
                'order_id' => intval($all['order_id']),
                'type' => intval($all['type']),
                'state' => intval($all['state']),
                'date_perf' => new \DateTime($all['date_perf']),
                'performer_user_id' => intval($all['performer_user_id']),
                'responsible_user_id' => intval($all['responsible_user_id']),
                'comment' => $all['comment'],
                'client_id' => $order->client_id ? intval($order->client_id) : null,
                'firm_id' => $order->firm_id ? intval($order->firm_id) : null
            ];
            return $this->repository->update($data, intval($all['id']));
        } else {
            abort(422);
        }
    }

    /**
     * Ajax - завершение задачи.
     *
     * @param $all
     * @return mixed
     */
    public function completeTask($all)
    {
        $task = $this->repository->find(intval($all['id']));
        $order = $this->orderRepository->find($task->order_id);
        if ($task && $order) {
            $data = [
                'final_comment' => $all['final_comment'],
                'state' => TaskReference::STATE_DONE
            ];
            $order->status = OrderReference::STATUS_CLOSE;
            return $this->repository->update($data, intval($all['id']));
        } else {
            abort(422);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getTaskByID($id)
    {
        $this->repository->pushCriteria(new WithAllRelationCriteria());
        return $this->repository->find(intval($id));
    }
}
