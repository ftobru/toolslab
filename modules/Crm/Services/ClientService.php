<?php namespace Crm\Services;

use App;
use Config;
use Core\Traits\Groupable;
use Crm\Models\Client;
use Crm\Models\Firm;
use Crm\References\ClientReference;
use Crm\Repositories\Eloquent\ClientEloquentRepository;
use Crm\Repositories\Eloquent\Criteria\Client\CompanyIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Client\EmailCriteria;
use Crm\Repositories\Eloquent\Criteria\Client\NameStartWithCriteria;
use Crm\Repositories\Eloquent\Criteria\Client\PhoneCriteria;
use Crm\Repositories\Eloquent\Criteria\Client\StatusCriteria;
use Crm\Repositories\Eloquent\Criteria\Client\TimeCompareCriteria;
use Crm\Repositories\Eloquent\Criteria\Client\TypeCriteria;
use Crm\Repositories\Interfaces\ClientRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Core\Traits\Taggable;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;

/**
 * Class ClientService
 * @package App\Services
 */
class ClientService implements CardInterface
{

    use Sortable, Paginatable, Taggable, Groupable;

    /** @var ClientEloquentRepository */
    protected $repository;

    /** @var  FormEloquentRepository */
    protected $formRepository;


    /**
     * @param ClientRepositoryInterface $repository
     * @param FormEloquentRepository $formRepository
     */
    public function __construct(ClientRepositoryInterface $repository, FormEloquentRepository $formRepository)
    {
        $this->repository = $repository;
        $this->formRepository = $formRepository;
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function getAllCompanyClients(array $data)
    {
        $this->repository->pushCriteria(new CompanyIdCriteria($data['company_id']));
        return $this->activeRepository(array_get($data, 'metaData', []));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param array $data
     * @return static
     */
    public function store(array $data)
    {
        return $this->repository->create($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        return $this->repository->update($data, $id);
    }

    /**
     * @param array $data
     * @return static
     */
    public function create(array $data)
    {
        $client = $this->repository->create($data);
        return $client;
    }

    /**
     * @param $formID
     * @return mixed
     */
    public function findById($formID)
    {
        return $this->repository->find($formID);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllClients()
    {
        return $this->repository->all();
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        //$this->filterByTags($clients, $metaData, 'Crm\Models\Client');

        $fullList = $this->repository->all();
        $statuses = ClientReference::statusesInfo();
        $types = ClientReference::typesInfo();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'types' => $types,
                    'statuses' => $statuses,
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'type') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new TypeCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'status') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new StatusCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new NameStartWithCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'phone') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new PhoneCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'email') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new EmailCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'types' => $types,
                'statuses' => $statuses,
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * Создает объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function createFromModels($models, $companyId)
    {
        $clients = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                $newClient = $this->repository->create($this->kendoModelToArray($model, $companyId));
                array_push($clients, $newClient);
            }
        }
        return $clients;
    }

    /**
     * Обновляет объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function updateFromModels($models, $companyId)
    {
        $clients = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                if (isset($model['id'])) {
                    $newClient = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                    array_push($clients, $newClient);
                }
            }
        }
        return $clients;
    }

    /**
     * Создает объект из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function createFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            $newClient = $this->repository->create($this->kendoModelToArray($model, $companyId));
            return $newClient;
        }
    }

    /**
     * Update объекта из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function updateFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            if (isset($model['id'])) {
                $newClient = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                return $newClient;
            }
        }
    }

    /**
     * @param $model
     * @return bool
     */
    private function checkKendoModel($model)
    {
        return
            isset($model['name']) &&
            isset($model['type']) &&
            isset($model['status']) &&
            isset($model['description']) &&
            isset($model['phone']) &&
            isset($model['email']) &&
            in_array(intval($model['status']), ClientReference::statuses()) &&
            in_array(intval($model['type']), ClientReference::types());
    }

    /**
     * @param $model
     * @param $companyId
     * @return array
     */
    private function kendoModelToArray($model, $companyId)
    {
        $array = [
            'name' => $model['name'],
            'type' => intval($model['type']),
            'status' => intval($model['status']),
            'description' => $model['description'],
            'phone' => $model['phone'],
            'email' => $model['email'],
            'company_id' => $companyId
        ];
        $this->attachAdditionalParams($array, $model);
        return $array;
    }

    /**
     * @param $entityArray
     * @param $model
     */
    private function attachAdditionalParams(&$entityArray, $model)
    {
        $fields = $this->formRepository
            ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Client::class)))
            ->all();
        foreach ($fields as $field) {
            if (isset($model[$field->name])) {
                $entityArray[$field->name] = $model[$field->name];
            }
        }
    }

    /**
     * @param $id
     * @param $toFirmId
     * @return bool|void
     */
    public function attach($id, $toFirmId)
    {
        $client = $this->repository->find($id);
        /** @var Firm $firm */
        $firm = App::make('FirmRepositoryInterface')->find($toFirmId);

        if (!($client && $firm)) {
            return false;
        }
        $firm->clients()->attach($client->id);
        return true;
    }

    /**
     * @param $id
     * @return int
     */
    public function detach($id)
    {
        /** @var Client $client */
        $client = $this->repository->find($id);
        return $client->firms()->detach();
    }

    /**
     * @param $id
     * @return Client
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

}
