<?php namespace Crm\Services;

use Core\Traits\Groupable;
use Crm\Models\Order;
use Crm\References\OrderReference;
use Crm\Repositories\Criteria\Order\WithClientCriteria;
use Crm\Repositories\Eloquent\Criteria\Order\DescriptionCriteria;
use Crm\Repositories\Eloquent\Criteria\Order\ClientIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Order\FirmIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Order\RespUserIdIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Order\StatusCriteria;
use Crm\Repositories\Eloquent\Criteria\Order\TimeCompareCriteria;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Core\Traits\Taggable;

/**
 * Class OrderService
 * @package Crm\Services
 */
class OrderService
{

    use Sortable, Paginatable, Taggable, Groupable;

    /** @var OrderEloquentRepository */
    protected $repository;

    /**
     * @param OrderRepositoryInterface $repository
     */
    public function __construct(OrderRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return Collection
     */
    public function getAllCompanyOrders(array $data)
    {
        $this->activeRepository(array_get($data, 'metaData', []));
        return $this->repository->all();
    }

    /**
     * @param $orderId
     * @return mixed
     */
    public function getOrderWithCriteria($orderId)
    {
        $this->repository->find($orderId);
        $this->repository->pushCriteria(new WithClientCriteria());
        $orders = $this->repository->all();
        foreach ($orders as $order) {
            $order->tagged;
        }
        return $orders;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {

//        $this->filterByTags($orders, $metaData, 'Crm\Models\Order');

        $fullList = $this->repository->all();
        $statuses = OrderReference::statusesInfo();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'statuses' => $statuses,
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        $criteriaList = [];

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'status') {
                    if ($filter['operator'] === 'eq') {
                        $criteriaList[] = new StatusCriteria(intval($filter['value']));
                        $this->repository->pushCriteria(new StatusCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'client_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new ClientIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'firm_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new FirmIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'resp_user_id') {
                    if ($filter['operator'] === 'eq') {
                        $newCriteria = new RespUserIdIdCriteria(intval($filter['value']));
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '>=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                    if ($filter['operator'] === 'end') {
                        $newCriteria = new TimeCompareCriteria('created_at', $filter['value'], '<=');
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
                if ($filter['field'] === 'description') {
                    if ($filter['operator'] === 'startWith') {
                        $newCriteria = new DescriptionCriteria($filter['value']);
                        $criteriaList[] = $newCriteria;
                        $this->repository->pushCriteria($newCriteria);
                    }
                }
            }
        }

        $this->repository->getWith(['firm', 'client', 'carts', 'responsible_user']);
        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        foreach ($criteriaList as $criteria) {
            $this->repository->pushCriteria($criteria);
        }
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];

        if (isset($metaData['group'])) {
            // prepare some data
            foreach ($metaData['group'] as $key => $value) {
                if ($value['field'] === 'client') {
                    $metaData['group'][$key]['field'] = 'client_id';
                }
                if ($value['field'] === 'firm') {
                    $metaData['group'][$key]['field'] = 'firm_id';
                }
            }

            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->toArray(),
            'filters_data' => [
                'statuses' => $statuses,
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * Создает объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function createFromModels($models, $companyId)
    {
        $result = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                $newOrder = $this->repository->create($this->kendoModelToArray($model, $companyId));
                array_push($result, $newOrder);
            }
        }
        return $result;
    }

    /**
     * Обновляет объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function updateFromModels($models, $companyId)
    {
        $result = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                if (isset($model['id'])) {
                    $newOrder = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                    array_push($result, $newOrder);
                }
            }
        }
        return $result;
    }

    /**
     * Создает объект из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function createFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            $newOrder = $this->repository->create($this->kendoModelToArray($model, $companyId));
            return $newOrder;
        }
    }

    /**
     * Update объекта из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function updateFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            if (isset($model['id'])) {
                $newOrder = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                return $newOrder;
            }
        }
    }

    /**
     * @param $model
     * @return bool
     */
    private function checkKendoModel($model)
    {
        return
            isset($model['client_id']) &&
            isset($model['status']) &&
            in_array(intval($model['status']), OrderReference::statuses());
    }

    /**
     * @param $model
     * @param $companyId
     * @return array
     */
    private function kendoModelToArray($model, $companyId)
    {
        return [
            'client_id' => $model['client_id'],
            'status' => intval($model['status']),
            'company_id' => $companyId
        ];
    }

    /**
     * Возвращает новые заказы.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getNewOrders()
    {
        $this->repository->getWith(['firm', 'client', 'carts', 'responsible_user']);
        return $this->repository->pushCriteria(new StatusCriteria(OrderReference::STATUS_NEW))->all();
    }
}
