<?php namespace Crm\Services;

use Carbon\Carbon;
use Crm\Exceptions\ClientOrFirmNotFoundException;
use Crm\Exceptions\TypeNotFoundException;
use Crm\Models\Client;
use Crm\Models\Firm;
use Crm\References\CardReference;
use Crm\References\OrderReference;
use Crm\References\TaskReference;
use Crm\Repositories\Criteria\Card\CurrentDataMoreCreatedAt;
use Crm\Repositories\Criteria\Card\FirmIdOrClientIdsCriteria;
use Crm\Repositories\Criteria\Card\LoadRelationCriteria;
use Crm\Repositories\Criteria\Card\NotStateCriteria;
use Crm\Repositories\Criteria\Card\NotStatusCriteria;
use Crm\Repositories\Eloquent\ClientEloquentRepository;
use Crm\Repositories\Eloquent\Criteria\Order\ClientIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Task\FirmIdCriteria;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Eloquent\TaskEloquentRepository;
use Crm\Repositories\Interfaces\ClientRepositoryInterface;
use Crm\Repositories\Interfaces\FirmRepositoryInterface;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Crm\Repositories\Interfaces\TaskRepositoryInterface;
use Illuminate\Support\Collection;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\UserRepositoryInterface;

/**
 * Class CardService
 * @package Crm\Services
 */
class CardService
{

    /** @var ClientEloquentRepository */
    protected $clientRepository;
    /** @var FirmEloquentRepository */
    protected $firmRepository;
    /** @var  TaskEloquentRepository */
    protected $taskRepository;
    /** @var  OrderEloquentRepository */
    protected $orderRepository;
    /** @var UserEloquentRepository  */
    protected $userRepository;

    /**
     * @param ClientRepositoryInterface $clientRepositoryInterface
     * @param FirmRepositoryInterface $eloquentRepository
     * @param TaskRepositoryInterface $taskRepositoryInterface
     * @param OrderRepositoryInterface $orderRepositoryInterface
     * @param UserRepositoryInterface $userRepositoryInterface
     */
    public function __construct(
        ClientRepositoryInterface $clientRepositoryInterface,
        FirmRepositoryInterface $eloquentRepository,
        TaskRepositoryInterface $taskRepositoryInterface,
        OrderRepositoryInterface $orderRepositoryInterface,
        UserRepositoryInterface $userRepositoryInterface
    ) {
        $this->clientRepository = $clientRepositoryInterface;
        $this->firmRepository = $eloquentRepository;
        $this->orderRepository = $orderRepositoryInterface;
        $this->taskRepository = $taskRepositoryInterface;
        $this->userRepository = $userRepositoryInterface;
    }

    /**
     * @param $clientId
     * @param $firmId
     * @return bool
     * @throws ClientOrFirmNotFoundException
     */
    public function attachClientToFirm($clientId, $firmId)
    {
        /** @var Client $client */
        $client = $this->clientRepository->find($clientId);
        /** @var Firm $firm */
        $firm = $this->firmRepository->find($firmId);

        if (!($firm && $client)) {
            throw new ClientOrFirmNotFoundException;
        }
        $client->firms()->attach($firmId);
        return true;
    }

    /**
     * @param Firm $firm
     * @param array $clientIds
     * @return $this
     * @internal param $firmId
     */
    public function attachFirmToClients(Firm $firm, array $clientIds)
    {
        return Collection::make($clientIds)->each(function ($clientId) use ($firm) {
            $firm->clients()->attach($clientId);
        });
    }

    /**
     * @param $id
     * @return Firm|null
     */
    public function getFirmById($id)
    {
        $firm = $this->firmRepository->find($id);
        $clients = $firm->clients()->get();
        $tasks = $this->taskRepository->findAllBy('firm_id', $firm->id);
        $orders = $this->orderRepository->findAllBy('firm_id', $firm->id);

        $actionOrders = $this->getActionOrders($firm->id);
        $dueTasks = $this->getDueTasks($firm->id);
        $allFirms = $this->firmRepository->all();

        return [
            'firm' => $firm,
            'clients' => $clients,
            'tasks' => $tasks,
            'orders' => $orders,
            'dueTasks' => $dueTasks,
            'history' => null,
            'actionOrders' => $actionOrders,
            'allFirms' => $allFirms
        ];
    }

    /**
     * @return UserEloquentRepository|UserRepositoryInterface
     */
    public function getUserRepository()
    {
        return $this->userRepository;
    }

    /**
     * @param $firmId
     * @return Collection
     */
    public function getDueTasks($firmId)
    {
        return $dueTasks = $this->taskRepository->pushCriteria(new CurrentDataMoreCreatedAt())
            ->pushCriteria(new NotStateCriteria(TaskReference::STATE_DONE))
            ->findAllBy('firm_id', $firmId);
    }

    /**
     * @param $firmId
     * @return Collection
     */
    public function getActionOrders($firmId)
    {
        return $this->orderRepository->pushCriteria(new NotStatusCriteria(OrderReference::STATUS_CLOSE))
            ->pushCriteria(new NotStatusCriteria(OrderReference::STATUS_DELETE))
            ->findAllBy('firm_id', $firmId);
    }

    /**
     * @param $id
     * @return array
     * @throws ClientOrFirmNotFoundException
     */
    public function getClientById($id)
    {
        $client = $this->clientRepository->pushCriteria(new LoadRelationCriteria('firm'))->find($id);
        if (!$client) {
            throw new ClientOrFirmNotFoundException;
        }
        /** @var Firm $firm */
        $firm = $client->firms()->get()->first();

        $clients = ($firm ? $firm->clients()->get() : Collection::make([$client]));
        $result = [
            'firm' => $firm,
            'clients' => $clients,
        ];

        return $result;
    }


    /**
     * @param $clientIds
     * @param $firmId
     * @return Collection
     */
    public function getActiveOrderByClientIds($clientIds, $firmId)
    {
        return $this->orderRepository->pushCriteria(new NotStatusCriteria(OrderReference::STATUS_CLOSE))
            ->pushCriteria(new NotStatusCriteria(OrderReference::STATUS_DELETE))
            ->pushCriteria(new FirmIdOrClientIdsCriteria($clientIds, $firmId))->all();
    }

    /**
     * @param $clientsIds
     * @param null $firmId
     * @return Collection
     */
    public function getDueTasksByClientsOrFirm($clientsIds, $firmId = null)
    {
        return $this->taskRepository->pushCriteria(new FirmIdOrClientIdsCriteria($clientsIds, $firmId))
            ->pushCriteria(new NotStateCriteria(TaskReference::STATE_DONE))->all();
    }

    /**
     * @param $clientIds
     * @param null $firmId
     * @return Collection
     * @throws ClientOrFirmNotFoundException
     */
    public function getOrdersByClientsOrFirm($clientIds, $firmId = null)
    {
        if ($clientIds) {
            $this->orderRepository->pushCriteria(new FirmIdOrClientIdsCriteria($clientIds, $firmId));
            return $this->taskRepository->all();
        } else {
            throw new ClientOrFirmNotFoundException;
        }
    }

    /**
     * @param $clientId
     * @return $this
     */
    public function getOrdersByClient($clientId)
    {
        return $this->orderRepository->getByCriteria(new ClientIdCriteria($clientId));
    }

    /**
     * @param $firmId
     * @return $this
     */
    public function getOrdersByFirmId($firmId)
    {
        return $this->orderRepository->getByCriteria(new FirmIdCriteria($firmId));
    }

    /**
     * @param $clientIds
     * @param null $firmId
     * @return Collection
     * @throws ClientOrFirmNotFoundException
     */
    public function getTasksByClientsOrFirm($clientIds, $firmId = null)
    {
        if ($clientIds) {

            $this->taskRepository->pushCriteria(new FirmIdOrClientIdsCriteria($clientIds, $firmId));

            return $this->taskRepository->all();
        } else {
            throw new ClientOrFirmNotFoundException;
        }
    }

    /**
     * @param $clientId
     * @return $this
     */
    public function getTasksByClient($clientId)
    {
        return $this->taskRepository->getByCriteria(new ClientIdCriteria($clientId));
    }

    /**
     * @param $firmId
     * @return $this
     */
    public function getTasksByFirm($firmId)
    {
        return $this->taskRepository->getByCriteria(new FirmIdCriteria($firmId));
    }

    /**
     * @param $id
     * @return bool
     * @throws ClientOrFirmNotFoundException
     * @throws TypeNotFoundException
     */
    public function get($id)
    {

        if (CardReference::$factoryType === CardReference::TYPE_CLIENT) {
            $result = $this->getClientById($id);
        } elseif (CardReference::$factoryType === CardReference::TYPE_FIRM) {
            $result = $this->getFirmById($id);
        } else {
            throw new TypeNotFoundException;
        }
        $result = array_merge($result, ['users' => $this->userRepository->all()->toArray()]);
        $result = array_merge($result, ['firms' => $this->firmRepository->all()->toArray()]);
        $result = array_merge($result, ['allClients' => $this->clientRepository->all()->toArray()]);
        return $result;
    }

    /**
     * Создать фирму
     * @param array $data
     * @return Firm
     */
    public function createFirm(array $data)
    {
        /** @var Firm $firm */
        $firm = $this->firmRepository->create($data);
        if (is_array(array_get($data, 'clientIds', false))) {
            return $this->attachFirmToClients($firm, $data['clientIds']);
        }
        return $firm;
    }

    /**
     * Создать контакт
     * @param array $data
     * @return mixed
     */
    public function createClient(array $data)
    {
        return $this->clientRepository->create($data);
    }


    public function getHistoryByType($id, $type = 'client')
    {
        if ($this->checkType($type)) {
            //
        }
    }

    /**
     * Проверить тип карточки
     * @param $type
     * @return bool
     */
    protected function checkType($type)
    {
        return in_array($type, $this->types());
    }

    /**
     * Типы карточек
     * @return array
     */
    public function types()
    {
        return ['client', 'firm'];
    }

    /**
     * @param $type
     * @param $id
     * @return Collection
     * @throws ClientOrFirmNotFoundException
     */
    public function getTasksByType($type, $id)
    {
        if ($this->checkType($type)) {
            if ($type === $this->types()[0]) {
                return $this->getTasksByTypeClient($id);
            } elseif ($type === $this->types()[1]) {
                return $this->getTasksByTypeFirm($id);
            }
        }
    }

    /**
     * @param $id
     * @return Collection
     */
    protected function getTasksByTypeClient($id)
    {
        return $this->taskRepository->findAllBy('client_id', $id);
    }

    /**
     * @param $id
     * @return Collection
     * @throws ClientOrFirmNotFoundException
     */
    protected function getTasksByTypeFirm($id)
    {
        /** @var Firm $firm */
        $firm = $this->firmRepository->find($id);
        if (!$firm) {
            throw new ClientOrFirmNotFoundException;
        }

        return Collection::make($this->taskRepository->pushCriteria(
            new FirmIdOrClientIdsCriteria($firm->clients()->get()->lists('id')->toArray(), $firm->id)
        )->all());
    }

    /**
     * @param $type
     * @param $id
     * @return bool|Collection
     * @throws ClientOrFirmNotFoundException
     */
    public function getOrderByType($type, $id)
    {
        if ($this->checkType($type)) {
            if ($type === $this->types()[0]) {
                return $this->getOrderByTypeClient($id);
            } elseif ($type == $this->types()[1]) {
                return $this->getOrderByTypeFirm($id);
            }
            return false;
        }
    }

    /**
     * @param $id
     * @return Collection
     */
    protected function getOrderByTypeClient($id)
    {
        return $this->orderRepository->findAllBy('client_id', $id);
    }


    /**
     *
     * @param $id
     * @return Collection
     * @throws ClientOrFirmNotFoundException
     */
    protected function getOrderByTypeFirm($id)
    {
        /** @var Firm $firm */
        $firm = $this->firmRepository->find($id);
        if (!$firm) {
            throw new ClientOrFirmNotFoundException;
        }

        return Collection::make($this->orderRepository->pushCriteria(
            new FirmIdOrClientIdsCriteria($firm->clients()->get()->lists('id')->toArray(), $firm->id)
        )->all());
    }

    /**
     * @return ClientEloquentRepository|ClientRepositoryInterface
     */
    public function getClientRepository()
    {
        return $this->clientRepository;
    }

    /**
     * @return FirmEloquentRepository|FirmRepositoryInterface
     */
    public function getFirmRepository()
    {
        return $this->firmRepository;
    }

}
