<?php namespace Crm\Services;

use Crm\Models\Product;
use Crm\References\OrderReference;
use Crm\Repositories\Eloquent\CartEloquentRepository;
use Crm\Repositories\Eloquent\Criteria\Cart\CompanyIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Cart\MinQTYCriteria;
use Crm\Repositories\Eloquent\Criteria\Cart\OrderIdCriteria;
use Crm\Repositories\Eloquent\Criteria\Cart\QTYCriteria;
use Crm\Repositories\Eloquent\Criteria\Cart\TimeCompareCriteria;
use Crm\Repositories\Eloquent\ProductEloquentRepository;
use Crm\Repositories\Interfaces\CartRepositoryInterface;
use Crm\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;

/**
 * Class CartService
 * @package Crm\Services
 */
class CartService
{

    use Sortable, Paginatable;

    /** @var CartEloquentRepository */
    protected $repository;

    /** @var ProductEloquentRepository */
    protected $productRepository;

    /**
     * @param CartRepositoryInterface $repository
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(CartRepositoryInterface $repository, ProductRepositoryInterface $productRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
//        if (isset($metaData['sort'])) {
//            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
//        }
//
//        if (isset($metaData['filter'])) {
//            if (isset($metaData['filter']['order_id'])) {
//                $this->repository->pushCriteria(new OrderIdCriteria($metaData['filter']['order_id']));
//            }
//            if (isset($metaData['filter']['qty'])) {
//                $this->repository->pushCriteria(new QTYCriteria($metaData['filter']['qty']));
//            }
//            if (isset($metaData['filter']['min_qty'])) {
//                $this->repository->pushCriteria(new MinQTYCriteria($metaData['filter']['min_qty']));
//            }
//            if (isset($metaData['filter']['max_qty'])) {
//                $this->repository->pushCriteria(new MinQTYCriteria($metaData['filter']['max_qty']));
//            }
//        }
//        if ($perPage = $this->getPerPage($metaData)) {
//            $this->repository->paginate($perPage);
//        }
//        return $this->repository->all();
        $fullList = $this->repository->all();
        $statuses = OrderReference::statusesInfo();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'statuses' => $statuses,
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();

        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->getWithOrder()->all()->toArray(),
            'filters_data' => [
                'statuses' => $statuses,
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * @param array $data
     */
    public function prepareData(array &$data)
    {
        if (isset($data['content'])) {
            $productList = [];
            $summary = 0;
            $qty = 0;
            foreach ($data['content']['products'] as $productInfo) {
                /** @var Product $product */
                $product = $this->productRepository->find($productInfo['id']);
                if ($product) {
                    $productArray = $product->toArray();
                    $productArray['qty'] = intval(array_get($productInfo, 'qty', 1));
                    $productArray['summary_price'] =
                        intval($productArray['qty']) * round(doubleval($productArray['new_price']), 2);
                    $summary+= $productArray['summary_price'];
                    $qty+=$productArray['qty'];
                    array_push($productList, $productArray);
                }
            }
            $data['content'] = json_encode([
                'products' => $productList,
                'summary_price' => $summary,
                'qty' => $qty
            ]);
        } else {
            $data['content'] = null;
        }
    }

    /**
     * @param array $data
     * @return static
     */
    public function saveCart(array $data)
    {
        $this->prepareData($data);
        return $this->repository->create($data);
    }
}
