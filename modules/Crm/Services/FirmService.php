<?php namespace Crm\Services;

use App;
use Config;
use Core\Traits\Groupable;
use Crm\Models\Firm;
use Crm\References\FirmReference;
use Crm\Repositories\Eloquent\Criteria\Firm\EmailCriteria;
use Crm\Repositories\Eloquent\Criteria\Firm\PhoneCriteria;
use Crm\Repositories\Eloquent\Criteria\Firm\StatusCriteria;
use Crm\Repositories\Eloquent\Criteria\Firm\TypeCriteria;
use Crm\Repositories\Eloquent\Criteria\Firm\NameStartWithCriteria;
use Crm\Repositories\Eloquent\Criteria\Firm\TimeCompareCriteria;
use Crm\Repositories\Eloquent\FirmEloquentRepository;
use Crm\Repositories\Interfaces\FirmRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;
use Core\Traits\Taggable;
use Saas\Repositories\Eloquent\Criteria\Forms\EntityTypeCriteria;
use Saas\Repositories\Eloquent\FormEloquentRepository;

/**
 * Class FirmService
 * @package App\Services
 */
class FirmService
{

    use Sortable, Paginatable, Taggable, Groupable;

    /** @var FirmEloquentRepository */
    protected $repository;

    /** @var  FormEloquentRepository */
    protected $formRepository;

    /**
     * @param FirmRepositoryInterface $repository
     * @param FormEloquentRepository $formEloquentRepository
     */
    public function __construct(FirmRepositoryInterface $repository, FormEloquentRepository $formEloquentRepository)
    {
        $this->repository = $repository;
        $this->formRepository = $formEloquentRepository;
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullList = $this->repository->all();
        $statuses = FirmReference::statusesInfo();
        $types = FirmReference::typesInfo();

        // Если нет записей
        if ($fullList->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'types' => $types,
                    'statuses' => $statuses,
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $minCreatedAt = date('Y/m/d', $fullList->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullList->max('created_at')->timestamp);
        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'type') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new TypeCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'status') {
                    if ($filter['operator'] === 'eq') {
                        $this->repository->pushCriteria(new StatusCriteria(intval($filter['value'])));
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new NameStartWithCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'phone') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new PhoneCriteria($filter['value'])
                        );
                    }
                }
                if ($filter['field'] === 'email') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new EmailCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        /** @var Collection $fullList */
        $filteredItems = $this->repository->all();
        $total = $filteredItems ->count();

        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredItems->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }

        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'types' => $types,
                'statuses' => $statuses,
                'created_at' => [
                    'min' => $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /**
     * Создает объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function createFromModels($models, $companyId)
    {
        $firms = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                $newFirm = $this->repository->create($this->kendoModelToArray($model, $companyId));
                array_push($firms, $newFirm);
            }
        }
        return $firms;
    }

    /**
     * Обновляет объекты из моделей KendoGrid.
     *
     * @param $models
     * @param $companyId
     * @return array
     */
    public function updateFromModels($models, $companyId)
    {
        $firms = [];
        foreach ($models as $model) {
            if ($this->checkKendoModel($model)) {
                if (isset($model['id'])) {
                    $newFirm = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                    array_push($firms, $newFirm);
                }
            }
        }
        return $firms;
    }


    /**
     * Создает объект из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function createFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            $newFirm = $this->repository->create($this->kendoModelToArray($model, $companyId));
            return $newFirm;
        }
    }

    /**
     * Update объекта из модели KendoGrid.
     *
     * @param $model
     * @param $companyId
     * @return array
     */
    public function updateFromModel($model, $companyId)
    {
        if ($this->checkKendoModel($model)) {
            if (isset($model['id'])) {
                $newFirm = $this->repository->update($this->kendoModelToArray($model, $companyId), $model['id']);
                return $newFirm;
            }
        }
    }

    /**
     * @param $model
     * @return bool
     */
    private function checkKendoModel($model)
    {
        return
            isset($model['name']) &&
            isset($model['type']) &&
            isset($model['status']) &&
            isset($model['description']) &&
            isset($model['phone']) &&
            isset($model['email']) &&
            in_array(intval($model['status']), FirmReference::statuses()) &&
            in_array(intval($model['type']), FirmReference::types());
    }

    /**
     * @param $model
     * @param $companyId
     * @return array
     */
    private function kendoModelToArray($model, $companyId)
    {
        $array = [
            'name' => $model['name'],
            'type' => intval($model['type']),
            'status' => intval($model['status']),
            'description' => $model['description'],
            'phone' => $model['phone'],
            'email' => $model['email'],
            'company_id' => $companyId
        ];
        $this->attachAdditionalParams($array, $model);
        return $array;
    }

    /**
     * @param $entityArray
     * @param $model
     */
    private function attachAdditionalParams(&$entityArray, $model)
    {
        $fields = $this->formRepository
            ->pushCriteria(new EntityTypeCriteria(Config::get('custom-fields.en_types.' . Firm::class)))
            ->all();
        foreach ($fields as $field) {
            if (isset($model[$field->name])) {
                $entityArray[$field->name] = $model[$field->name];
            }
        }
    }
}
