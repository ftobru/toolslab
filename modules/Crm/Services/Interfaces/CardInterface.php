<?php namespace Crm\Services;

/**
 * Interface CardInterface
 * @package Crm\Services
 */
interface CardInterface
{
    public function get($id);

    public function create(array $data);

    public function update($id, array $data);

    public function detach($id);

    public function attach($id, $toId);
}
