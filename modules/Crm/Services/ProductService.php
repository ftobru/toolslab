<?php namespace Core\Services;

use App;
use Core\Traits\Groupable;
use Crm\Http\Requests\Rest\GetProductsRequest;
use Crm\Repositories\Criteria\Product\NameStartWithCriteria;
use Crm\Repositories\Eloquent\Criteria\Product\TimeCompareCriteria;
use Crm\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Core\Traits\Paginatable;
use Core\Traits\Sortable;

use Crm\Repositories\Eloquent\Criteria\Product\NewPriceCompareCriteria;
use Crm\Repositories\Eloquent\Criteria\Product\OldPriceCompareCriteria;
use Crm\Repositories\Eloquent\Criteria\Product\ProfitCompareCriteria;

/**
 * Class ProductService
 * @package Core\Services
 */
class ProductService
{

    use Sortable, Paginatable, Groupable;

    /** @var \Crm\Repositories\Eloquent\ProductEloquentRepository */
    protected $repository;

    /**
     * @param ProductRepositoryInterface $repository
     */
    public function __construct(ProductRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param GetProductsRequest $request
     * @return Collection
     */
    public function getAllCompanyProducts(GetProductsRequest $request)
    {
        if ($request->has('metaData')) {
            return $this->activeRepository($request->get('metaData'));
        }
        return $this->repository->all();
    }

    /**
     * @param array $metaData
     * @return \Illuminate\Support\Collection
     */
    public function activeRepository(array $metaData)
    {
        $fullProducts = $this->repository->all();

        // Если нет записей
        if ($fullProducts->isEmpty()) {
            return [
                'data' => [],
                'filters_data' => [
                    'old_price' => [
                        'min' =>  0,
                        'max' => 100
                    ],
                    'new_price' => [
                        'min' =>  0,
                        'max' => 100
                    ],
                    'profit' => [
                        'min' =>  0,
                        'max' => 100
                    ],
                    'created_at' => [
                        'min' => date('Y/m/d'),
                        'max' => date('Y/m/d')
                    ]
                ],
                'total' => 0,
                'groups' => []
            ];
        }

        $maxPrice = $fullProducts->max('new_price');
        $minPrice = $fullProducts->min('new_price');
        $maxOldPrice = $fullProducts-> max('old_price');
        $minOldPrice = $fullProducts-> min('old_price');
        $maxProfit = $fullProducts-> max('profit');
        $minProfit = $fullProducts-> min('profit');

        $minCreatedAt = date('Y/m/d', $fullProducts->min('created_at')->timestamp);
        $maxCreatedAt = date('Y/m/d', $fullProducts->max('created_at')->timestamp);

        if (isset($metaData['sort'])) {
            $this->repository->setCriteriaCollection($this->makeSortCriteriaCollection($metaData['sort']));
        }

        if (isset($metaData['filter'])) {
            foreach ($metaData['filter']['filters'] as $filter) {
                if ($filter['field'] === 'old_price') {
                    if ($filter['operator'] === 'max') {
                        $this->repository->pushCriteria(
                            new OldPriceCompareCriteria(intval($filter['value']), '<=')
                        );
                    }
                    if ($filter['operator'] === 'min') {
                        $this->repository->pushCriteria(
                            new OldPriceCompareCriteria(intval($filter['value']), '>=')
                        );
                    }
                }
                if ($filter['field'] === 'new_price') {
                    if ($filter['operator'] === 'max') {
                        $this->repository->pushCriteria(
                            new NewPriceCompareCriteria(intval($filter['value']), '<=')
                        );
                    }
                    if ($filter['operator'] === 'min') {
                        $this->repository->pushCriteria(
                            new NewPriceCompareCriteria(intval($filter['value']), '>=')
                        );
                    }
                }
                if ($filter['field'] === 'profit') {
                    if ($filter['operator'] === 'max') {
                        $this->repository->pushCriteria(
                            new ProfitCompareCriteria(intval($filter['value']), '<=')
                        );
                    }
                    if ($filter['operator'] === 'min') {
                        $this->repository->pushCriteria(
                            new ProfitCompareCriteria(intval($filter['value']), '>=')
                        );
                    }
                }
                if ($filter['field'] === 'created_at') {
                    if ($filter['operator'] === 'start') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '>=')
                        );
                    }
                    if ($filter['operator'] === 'end') {
                        $this->repository->pushCriteria(
                            new TimeCompareCriteria('created_at', $filter['value'], '<=')
                        );
                    }
                }
                if ($filter['field'] === 'name') {
                    if ($filter['operator'] === 'startWith') {
                        $this->repository->pushCriteria(
                            new NameStartWithCriteria($filter['value'])
                        );
                    }
                }
            }
        }

        /** @var Collection $fullProducts */
        $filteredProducts = $this->repository->all();
        $total = $filteredProducts ->count();



        $groups = null;
        $groups_array = [];
        if (isset($metaData['group'])) {
            $this->groupOptions = $metaData['group'];
            $groups_array = $this->group($filteredProducts->toArray(), 0);
            $total = sizeof($groups_array);
            $groups_array = array_chunk($groups_array, $perPage = $this->getPerPage($metaData));
            $groups_array = $groups_array[intval(array_get($metaData, 'page', 1))-1];
        }



        if ($perPage = $this->getPerPage($metaData)) {
            $this->repository->paginateToPage($perPage, intval(array_get($metaData, 'page', 1)));
        }

        return [
            'data' => $this->repository->all()->all(),
            'filters_data' => [
                'old_price' => [
                    'min' =>  $minOldPrice,
                    'max' => $maxOldPrice
                ],
                'new_price' => [
                    'min' =>  $minPrice,
                    'max' => $maxPrice
                ],
                'profit' => [
                    'min' =>  $minProfit,
                    'max' => $maxProfit
                ],
                'created_at' => [
                    'min' =>  $minCreatedAt,
                    'max' => $maxCreatedAt
                ]
            ],
            'total' => $total,
            'groups' => $groups_array
        ];
    }

    /*protected $groupOptions;

    private function group($items, $level)
    {
        if (isset($this->groupOptions[$level])) {
            $field = $this->groupOptions[$level]['field'];
            $dir = $this->groupOptions[$level]['dir'];
            $a = new Collection();
            foreach ($items as $item) {
                $a->push($item);
            }
            $a = $a->groupBy($field);

            $keys = array_keys($a->toArray());
            if ($dir === 'desc') {
                arsort($keys);
            } else {
                asort($keys);
            }

            $groups_array = [];
            $level++;
            foreach ($keys as $key) {
                $items = $this->group($a[$key], $level);
                if ($items) {
                    $elem = [
                        'aggregates' => ['field' => $field, 'dir' => $dir],
                        'field' => $field,
                        'hasSubgroups' => sizeof($this->groupOptions) == $level ? false : true,
                        'items' => $items,
                        'value' => $key
                    ];
                    array_push($groups_array, $elem);
                };
            };

            return $groups_array;
        }
        return $items;
    }*/
}
