/**
 * Created by nikita on 21.05.15.
 */

var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');

var port = 8890;


server.listen(port);

io.on('connection', function(socket) {
    console.log("New connection client");

    var redisClient = redis.createClient();
    redisClient.psubscribe("message*");
    redisClient.psubscribe("domain*");
    redisClient.on("pmessage", function(pattern, channel, message) {
        console.log("new message in queue " + message + " channel");
        socket.emit(channel, message);
    });


    socket.on('disconnect', function() {
        redisClient.quit();
    });
});