<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace Core\Models\Abstracts{
/**
 * Class CoreModel
 *
 * @package Core\Models\Abstracts
 * @extends Illuminate\Database\Eloquent\Model
 * @property-read mixed $form_entity_and_clear 
 * @property-read mixed $exclude_custom 
 * @property-read mixed $custom 
 */
	class CoreModel {}
}

