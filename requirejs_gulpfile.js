var elixir = require('laravel-elixir');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less(['bootstrap.less', 'materialadmin.less', 'libs/bootstrap-datepicker/datepicker3.less']);

    mix.copy('resources/assets/css', 'public/css');
    mix.copy('resources/assets/js/libs/jquery-ui/themes/smoothness/jquery-ui.min.css', 'public/css/jquery-ui.min.css');
    mix.copy('resources/assets/css/toolslab.jquery-ui.css', 'public/css/toolslab.jquery-ui.css')
    mix.copy('resources/assets/js/libs/select2/dist/css/select2.min.css', 'public/css/select2.min.css');
    mix.copy('resources/assets/js/libs/multiselect/css/multi-select.css','public/css/multi-select.css');
    mix.copy('resources/assets/css/toastr.css','public/css/toastr.css');
    mix.copy('resources/assets/js/libs/bootstrap-tagsinput/dist/bootstrap-tagsinput.css','public/css/bootstrap-tagsinput.css');

    mix.copy('resources/assets/js/libs/respond/dest/respond.min.js', 'public/js/respond.min.js');
    mix.copy('resources/assets/js/libs/html5shiv/dist/html5shiv.min.js', 'public/js/html5shiv.min.js');

    mix.copy('resources/assets/libs/toastr/toastr.min.js');
    mix.copy('resources/assets/libs/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js');
    mix.copy('resources/assets/libs/jquery-ui/jquery-ui.min.js');
    mix.copy('resources/assets/libs/jquery-validation/dist/jquery.validate.min.js');
    mix.copy('resources/assets/libs/jquery.cookie/jquery.cookie.js');
    mix.copy('resources/assets/libs/socket.io-client/socket.io.js');
    mix.copy('resources/assets/libs/select2/dist/js/select2.full.min.js');
    mix.copy('resources/assets/libs/multiselect/js/jquery.multi-select.js');
    mix.copy('resources/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
    mix.copy('resources/assets/libs/jquery.inputmask/dist/jquery.inputmask.bundle.min.js');
    mix.copy('resources/assets/libs/jquery.truncate.js');
    mix.copy('resources/assets/libs/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js');
    mix.copy('resources/assets/libs/bootstrap/dist/js/bootstrap.min.js');
    mix.copy('resources/assets/js/libs/handlebars/handlebars.min.js', 'public/js/handlebars.min.js');
    mix.copy('resources/assets/js/script-card.js','public/js/script-card.js');
    mix.copy('resources/assets/js/jamur.js', 'public/js/jamur.js');

    mix.copy(
        'resources/assets/js/libs/jquery.fancybox/fancybox/jquery.fancybox.pack-1.3.4.js',
        'public/js/jquery.fancybox-1.3.4.pack.js'
    );

    mix.scripts([
        'core/source/App.js',
        'core/source/AppNavigation.js',
        'core/source/AppOffcanvas.js',
        'core/source/AppCard.js',
        'core/source/AppForm.js',
        'core/source/AppNavSearch.js',
        'core/source/AppVendor.js',
        'core/source/slick.js',
        'libs/spinjs/spin.js',
        'libs/autosize/dist/autosize.min.js'
    ], 'public/vendor/material.pack.min.js').scripts([
        'js/kendo/messages/kendo.messages.ru-RU.min.js',
        'js/kendo/cultures/kendo.culture.ru-RU.min.js',
        'libs/jszip/dist/jszip.min.js',
        'kendo/pako_deflate.min.js',
        'libs/underscore/underscore-min.js',
        'kendo/kendo.toolslab-grid.base.js',
        'js/kendo/kendo.all.min.js'
    ], 'public/vendor/kendo.pack.min.js');




    mix.stylesIn("public/css");
    mix.version([
        'css/all.css'
    ]);

    mix.copy('resources/assets/css/kendo.css', 'public/css/kendo.css');
    

    mix.copy(
        'resources/assets/js/libs/jquery.fancybox/fancybox/jquery.fancybox-1.3.4.css',
        'public/css/jquery.fancybox.css'
    );
    mix.copy('resources/assets/css/jquery-ui-theme.css', 'public/css/jquery-ui-theme.css');
    mix.copy('resources/assets/css-kendo/kendo.dataviz.min.css', 'public/css/kendo.dataviz.min.css');
    mix.copy('resources/assets/css-kendo/kendo.default.min.css', 'public/css/kendo.default.min.css');
    mix.copy('resources/assets/css-kendo/kendo.dataviz.default.min.css', 'public/css/kendo.dataviz.default.min.css');
    mix.copy('resources/assets/js/jamur.js', 'public/js/jamur.js');
    mix.copy('resources/assets/css/jquery-ui-filemanager-theme.css', 'public/css/jquery-ui-filemanager-theme.css');
});
