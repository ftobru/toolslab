<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersRespUserAndDescription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $blueprint) {
            $blueprint->integer('responsible_user_id')->nullable();
            $blueprint->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $blueprint) {
            $blueprint->dropColumn('responsible_user_id');
            $blueprint->dropColumn('description');
        });
    }
}
