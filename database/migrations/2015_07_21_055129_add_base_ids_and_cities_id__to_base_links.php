<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBaseIdsAndCitiesIdToBaseLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('link_bases', function (Blueprint $table) {
            $table->jsonb('link_ids')->nullable(); // список id
            $table->jsonb('cities_idxs')->nullable(); // idx с таблиц городов
            $table->string('path', 1024)->nullable(); // путь к лендингу
            $table->string('google_analytics', 1024)->nullable();
            $table->string('ya_metrika', 1024)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_bases', function (Blueprint $table) {
            $table->dropColumn('link_ids'); // список id
            $table->dropColumn('cities_idxs'); // idx с таблиц городов
            $table->dropColumn('path'); // путь к лендингу
            $table->dropColumn('google_analytics');
            $table->dropColumn('ya_metrika');
        });
    }
}
