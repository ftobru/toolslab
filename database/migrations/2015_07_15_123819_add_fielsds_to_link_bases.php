<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFielsdsToLinkBases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('link_bases', function (Blueprint $table) {
            $table->integer('domain_id');
            $table->string('url')->nullable();
            $table->boolean('open_url')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_bases', function (Blueprint $table) {
            $table->dropColumn('domain_id');
            $table->dropColumn('url');
            $table->dropColumn('open_url')->default(false);
        });
    }
}
