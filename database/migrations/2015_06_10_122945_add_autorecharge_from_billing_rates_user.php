<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutorechargeFromBillingRatesUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_rates_user', function (Blueprint $blueprint) {
            $blueprint->boolean('autorecharge')->default(true)->after('rate_ident');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_rates_user', function (Blueprint $blueprint) {
            $blueprint->dropColumn('autorecharge');
        });
    }
}
