<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToClientsAndFirms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('firms', function (Blueprint $blueprint) {
            $blueprint->string('phone')->nullable();
        });
        Schema::table('clients', function (Blueprint $blueprint) {
            $blueprint->string('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('firms', function (Blueprint $blueprint) {
            $blueprint->dropColumn('phone');
        });
        Schema::table('clients', function (Blueprint $blueprint) {
            $blueprint->dropColumn('description');
        });
    }
}
