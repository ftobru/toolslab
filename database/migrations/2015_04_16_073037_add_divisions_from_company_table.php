<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDivisionsFromCompanyTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $blueprint) {
            $blueprint->string('divisions')->nullable();
        });

        DB::statement('alter table companies alter column divisions type jsonb using divisions::text::jsonb;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $blueprint) {
            $blueprint->dropColumn('divisions');
        });
    }
}
