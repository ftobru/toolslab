<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddTrialsFromBillingAccount
 */
class AddTrialsFromBillingAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_accounts', function (Blueprint $blueprint) {
            $blueprint->jsonb('trials')->default(json_encode([]));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_accounts', function (Blueprint $blueprint) {
            $blueprint->dropColumn('trials');
        });
    }
}
