<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('links', function (Blueprint $blueprint) {
            $blueprint->dropColumn('landing_id');
        });

        Schema::table('links', function (Blueprint $blueprint) {
            $blueprint->jsonb('insert_link')->nullable();
            $blueprint->jsonb('replacement_link')->nullable();
            $blueprint->jsonb('replacement_geo')->nullable();
            $blueprint->jsonb('replacement_param')->nullable();
            $blueprint->jsonb('replacement_time')->nullable();
            $blueprint->jsonb('replacement_product')->nullable();
            $blueprint->smallInteger('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('links', function (Blueprint $blueprint) {
            $blueprint->dropColumn('insert_link');
            $blueprint->dropColumn('replacement_link');
            $blueprint->dropColumn('replacement_geo');
            $blueprint->dropColumn('replacement_param');
            $blueprint->dropColumn('replacement_time');
            $blueprint->dropColumn('replacement_product');
            $blueprint->dropColumn('type');
        });

        Schema::table('links', function (Blueprint $blueprint) {
            $blueprint->integer('landing_id')->nullable();
        });
    }
}
