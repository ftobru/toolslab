<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeys extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('users', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');

        });

        Schema::table('orders', function(Blueprint $table){
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('clients', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('products', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('cart', function(Blueprint $table){
            $table->foreign('order_id')->references('id')->on('orders');
        });

        Schema::table('companies', function(Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('billing_account_id')->references('id')->on('billing_accounts');
        });

        Schema::table('tasks', function(Blueprint $table){
            $table->foreign('client_id')->references('id')->on('clients');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('responsible_user_id')->references('id')->on('users');
            $table->foreign('performer_user_id')->references('id')->on('users');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('domains', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('links', function(Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('template_id')->references('id')->on('templates');

            $table->foreign('company_id')->references('id')->on('companies');

        });

        Schema::table('templates', function(Blueprint $table){
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('statistic_hosts', function(Blueprint $table){
            $table->foreign('replacement_link_id')->references('id')->on('replacement_links');
            $table->foreign('insert_link_id')->references('id')->on('insert_links');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('client_id')->references('id')->on('clients');
        });

        Schema::table('replacement_links', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('insert_links', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('replacement_geo', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('replacement_time', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('replacement_params', function(Blueprint $table){
            $table->foreign('company_id')->references('id')->on('companies');
        });
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
