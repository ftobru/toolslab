<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsEntityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forms_entity', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('form_id');
            $table->foreign('form_id')->references('id')->on('forms');
            $table->integer('en_id');
            $table->string('meaning')->nullable();
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('alter table forms_entity alter column meaning type jsonb using meaning::text::jsonb;');
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('forms_entity');
	}

}
