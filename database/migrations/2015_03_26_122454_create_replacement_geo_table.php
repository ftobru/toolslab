<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplacementGeoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('replacement_geo', function(Blueprint $table) {
            $table->increments('id');
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('alter table replacement_geo alter column city type jsonb using city::text::jsonb;');
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('replacement_geo');
	}

}
