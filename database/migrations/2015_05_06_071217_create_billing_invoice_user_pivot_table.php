<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingInvoiceUserPivotTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('billing_invoice_company');
        Schema::create('billing_invoice_user', function (Blueprint $table)
        {
            $table->integer('billing_invoice_id')->unsigned()->index();
            $table->foreign('billing_invoice_id')->references('id')->on('billing_invoices')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billing_invoice_user');
    }

}
