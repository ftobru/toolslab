<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('order_id');
            $table->integer('type');
            $table->datetime('date_perf')->nullable();
            $table->integer('responsible_user_id');
            $table->integer('performer_user_id')->nullale();
            $table->string('comment')->nullable();
            $table->tinyInteger('state')->default(0);
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tasks');
	}

}
