<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateFirmsTable
 */
class CreateFirmsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('firms', function(Blueprint $table)
		{
			$table->increments('id');
            $table->tinyInteger('status');
            $table->string('name');
            $table->string('description');
            $table->integer('company_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('firms');
	}

}
