<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingInvocesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_invoices', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('status')->default(0);
            $table->integer('type');
            $table->double('amount')->default(0.00);
            $table->dateTime('last_status_update_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('billing_invoices');
	}

}
