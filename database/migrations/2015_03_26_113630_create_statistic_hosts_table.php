<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticHostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('statistic_hosts', function(Blueprint $table) {
            $table->increments('id');
            $table->string('step0')->nullable();
            $table->string('step1')->nullable();
            $table->string('step2')->nullable();
            $table->string('step3')->nullable();
            $table->boolean('order_page')->default(false);
            $table->integer('product_id');
            $table->integer('replacement_link_id');
            $table->integer('insert_link_id');
            $table->integer('form_id');
            $table->string('buttons');
            $table->string('utm')->nullable();
            $table->string('ya_utm')->nullable();
            $table->string('google_utm')->nullable();
            $table->string('vk_utm')->nullable();
            $table->string('other_utm')->nullable();
            $table->string('refferer')->nullable();
            $table->string('sid')->nullable();
            $table->integer('uid')->default(1);
            $table->string('city')->default();
            $table->string('ip')->nullable();
            $table->string('user_agent')->nullable();
            $table->string('os')->nullable();
            $table->datetime('start_datetime_at')->nullable();
            $table->datetime('finish_datetime_at')->nullable();
            $table->string('session')->nullable();
            $table->integer('client_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });


        DB::statement('alter table statistic_hosts alter column vk_utm type jsonb using vk_utm::text::jsonb;');
        DB::statement('alter table statistic_hosts alter column other_utm type jsonb using other_utm::text::jsonb;');
        DB::statement('alter table statistic_hosts alter column google_utm type jsonb using google_utm::text::jsonb;');
        DB::statement('alter table statistic_hosts alter column ya_utm type jsonb using ya_utm::text::jsonb;');


    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('statistic_hosts');
	}

}
