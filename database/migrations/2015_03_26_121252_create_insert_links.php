<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertLinks extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('insert_links', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->string('param');
            $table->string('key_value')->nullable();
            $table->integer('company_id');
            $table->timestamps();
        });

        DB::statement('alter table insert_links alter column key_value type jsonb using key_value::text::jsonb;');
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('drop table insert_links CASCADE');
	}

}
