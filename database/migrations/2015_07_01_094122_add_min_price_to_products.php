<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinPriceToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $blueprint) {
            $blueprint->float('min_price')->default(0);
            $blueprint->float('prime_cost')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $blueprint) {
            $blueprint->dropColumn('min_price');
            $blueprint->dropColumn('prime_cost');
        });
    }
}
