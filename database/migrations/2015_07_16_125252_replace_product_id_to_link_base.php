<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReplaceProductIdToLinkBase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("links", function (Blueprint $table) {
            $table->dropColumn('product_id');
            $table->dropColumn('template_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("links", function (Blueprint $table) {
            $table->integer('product_id')->nullable();
            $table->integer('template_id')->nullable();
        });
    }
}
