<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCartTable
 */
class CreateCartTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cart', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id');
            $table->string('content', 2048);
            $table->integer('qty')->default(1);
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('alter table cart alter column content type jsonb using content::text::jsonb;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cart');
	}

}
