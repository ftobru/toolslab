<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('forms', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('en_type');
            $table->integer('form_type');
            $table->string('default_value')->nullable();
            $table->string('label')->nullable();
            $table->string('placeholder')->nullable();
            $table->string('validator');
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('alter table forms alter column default_value type jsonb using default_value::text::jsonb;');
        DB::statement('alter table forms alter column validator type jsonb using validator::text::jsonb;');

    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('forms');
	}

}
