<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplacementTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('replacement_time', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('tag');
            $table->time('start_time');
            $table->time('finish_time');
            $table->string('meaning');
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('replacement_time');
	}

}
