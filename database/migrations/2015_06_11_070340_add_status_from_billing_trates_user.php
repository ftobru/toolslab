<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusFromBillingTratesUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('billing_rates_user', function (Blueprint $blueprint) {
            $blueprint->tinyInteger('status')->after('id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('billing_rates_user', function (Blueprint $blueprint){
            $blueprint->dropColumn('status');
        });
    }
}
