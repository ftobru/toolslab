<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('links', function(Blueprint $table) {
            $table->increments('id');
            $table->string ('name');
            $table->string('link');
            $table->string('short_link')->nullable();
            $table->integer('template_id')->nullable();
            $table->integer('landing_id');
            $table->integer('product_id');
            $table->integer('status')->default(0);
            $table->integer('hosts')->unsigned()->default(0);
            $table->integer('hits')->unsigned()->default(0);
            $table->integer('count_leads')->unsigned()->default(0);
            $table->float('convert')->default(0);
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('links');
	}

}
