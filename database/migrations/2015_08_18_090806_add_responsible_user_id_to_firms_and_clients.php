<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResponsibleUserIdToFirmsAndClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('firms', function (Blueprint $table) {
            $table->integer('responsible_user_id')->nullable();
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->integer('responsible_user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('firms', function (Blueprint $table) {
            $table->dropColumn('responsible_user_id');
        });
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('responsible_user_id');
        });
    }
}
