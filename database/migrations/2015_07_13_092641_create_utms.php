<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utms', function ($table) {
            $table->increments('id');
            $table->string("name");
            $table->string("description")->nullable();
            $table->string("utm_source")->nullable();
            $table->string("utm_compaign")->nullable();
            $table->string("utm_medium")->nullable();
            $table->string("utm_term")->nullable();
            $table->string("utm_content")->nullable();
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('utms');
    }
}
