<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LandingForms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('id_attr');
            $table->smallInteger('form_type');
            $table->smallInteger('callback_type');
            $table->string('callback_url')->nullable();
            $table->string('callback_text')->nullable();
            $table->string('after_header_text')->nullable();
            $table->string('header_text')->nullable();
            $table->string('before_header_text')->nullable();
            $table->string('after_btn_text')->nullable();
            $table->string('btn_text');
            $table->string('before_btn_text')->nullable();
            $table->string('btn_color');
            $table->integer('company_id');
            $table->jsonb('fields');
            $table->jsonb('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_forms');
    }
}
