<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToLinks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::raw("ALTER TABLE links ALTER COLUMN \"type\" SET DEFAULT 0;");
//        Schema::table('links', function (Blueprint $table) {
//            $table->smallInteger('type')->default(0);
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        ///
    }
}
