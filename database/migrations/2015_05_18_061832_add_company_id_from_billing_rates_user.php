<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdFromBillingRatesUser extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('billing_rates_user', function(Blueprint $blueprint) {
            $blueprint->integer('company_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('billing_rates_user', function(Blueprint $blueprint) {
            $blueprint->dropColumn('company_id');
        });
	}

}
