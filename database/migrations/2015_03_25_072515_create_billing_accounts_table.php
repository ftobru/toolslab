<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('billing_accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->double('amount')->default(0);
            $table->integer('count_operations')->unsigned()->default(0);
            $table->integer('status')->default(0);
            $table->integer('type')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('companies', function(Blueprint $table){
            $table->dropColumn('billing_account_id');
        });
		Schema::dropIfExists('billing_accounts');
	}

}
