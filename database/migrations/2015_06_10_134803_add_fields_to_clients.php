<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $blueprint) {
            $blueprint->string('name', 255)->nullable();
            $blueprint->string('email', 255)->nullable();
            $blueprint->string('phone', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $blueprint) {
            $blueprint->dropColumn('name');
            $blueprint->dropColumn('email');
            $blueprint->dropColumn('phone');
        });
    }
}
