<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkBases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->smallInteger('type');
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('links', function (Blueprint $table) {
            $table->integer('domain_id');
            $table->integer('link_base_id');
            $table->string('path');
            $table->string('url');
            $table->boolean('open_url')->default(false);
        });


        Schema::drop('ab_splits');
        Schema::drop('geo_splits');

        DB::statement('DROP TABLE templates CASCADE;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('link_bases');

        Schema::table('links', function (Blueprint $table) {
            $table->dropColumn('domain_id');
            $table->dropColumn('link_base_id');
        });
    }
}
