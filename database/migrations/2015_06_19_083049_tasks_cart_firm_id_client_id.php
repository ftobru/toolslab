<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TasksCartFirmIdClientId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $blueprint) {
            $blueprint->integer('firm_id')->nullable()->change();
            $blueprint->integer('client_id')->nullable()->change();
        });
        Schema::table('tasks', function (Blueprint $blueprint) {
            $blueprint->integer('firm_id')->nullable()->change();
            $blueprint->integer('client_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $blueprint) {
            $blueprint->integer('firm_id')->change();
            $blueprint->integer('client_id')->change();
        });
        Schema::table('tasks', function (Blueprint $blueprint) {
            $blueprint->integer('firm_id')->change();
            $blueprint->integer('client_id')->change();
        });
    }
}
