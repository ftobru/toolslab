<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTamplatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('templates', function(Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->string('name');
            $table->integer('domain_id');
            $table->integer('product_id');
            $table->foreign('domain_id')->references('id')->on('domains');
            $table->string('url')->default('/');
            $table->string('file');
            $table->string('title_page')->nullable();
            $table->string('description_page')->nullable();
            $table->string('description')->nullable();
            $table->string('tags_clients')->nullable();
            $table->string('tags_tasks')->nullable();
            $table->string('tags_orders')->nullable();
            $table->integer('hits')->unsigned()->default(0);
            $table->integer('hosts')->unsigned()->default(0);
            $table->integer('count_leads')->unsigned()->default(0);
            $table->float('convert')->nullable();
            $table->integer('company_id');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::statement('alter table templates alter column tags_clients type jsonb using tags_clients::text::jsonb;');
        DB::statement('alter table templates alter column tags_tasks type jsonb using tags_tasks::text::jsonb;');
        DB::statement('alter table templates alter column tags_orders type jsonb using tags_tasks::text::jsonb;');
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('templates');
	}

}
