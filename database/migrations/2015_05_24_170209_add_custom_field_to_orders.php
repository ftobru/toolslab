<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomFieldToOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('statuses_orders', function (Blueprint $blueprint){
            $blueprint->increments('id');
            $blueprint->integer('company_id');
            $blueprint->jsonb('statuses');
            $blueprint->timestamps();
            $blueprint->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('statuses_orders');
	}

}
