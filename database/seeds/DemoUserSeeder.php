<?php

use Crm\References\ClientReference;
use Crm\References\FirmReference;
use Crm\References\OrderReference;
use Crm\References\TaskReference;
use Crm\Repositories\Eloquent\OrderEloquentRepository;
use Crm\Repositories\Eloquent\ProductEloquentRepository;
use Crm\Repositories\Interfaces\OrderRepositoryInterface;
use Crm\Repositories\Interfaces\ProductRepositoryInterface;
use Crm\Services\CartService;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Envoy\SSH;
use League\Csv\Reader;
use IPGeoBase\IPGeoBase;
use Faker\Factory as Faker;
use Saas\Models\Company;
use Saas\Models\User;
use Saas\Repositories\Eloquent\CompanyEloquentRepository;
use Saas\Repositories\Eloquent\UserEloquentRepository;
use Saas\Repositories\Interfaces\CompanyRepositoryInterface;
use Saas\Repositories\Interfaces\UserRepositoryInterface;
use Saas\Services\CompanyService;
use Saas\Services\UserService;

/**
 * Class DemoUserSeeder
 */
class DemoUserSeeder extends Seeder {
    /**
     * @var UserEloquentRepository
     */
    protected $userRepository;
    /**
     * @var CompanyEloquentRepository
     */
    protected $companyRepository;
    /**
     * @var UserService
     */
    protected $userService;
    /**
     * @var CompanyService
     */
    protected $companyService;

    /** @var \Faker\Generator  */
    protected $faker;
    /**
     * @var CartService
     */
    protected $cartService;
    /**
     * @var ProductEloquentRepository
     */
    protected $productRepository;
    /**
     * @var OrderEloquentRepository
     */
    protected $orderRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param UserService $userService
     * @param CompanyRepositoryInterface $companyRepository
     * @param CompanyService $companyService
     * @param CartService $cartService
     * @param ProductRepositoryInterface $productRepository
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        UserService $userService,
        CompanyRepositoryInterface $companyRepository,
        CompanyService $companyService,
        CartService $cartService,
        ProductRepositoryInterface $productRepository,
        OrderRepositoryInterface $orderRepository

    ) {
        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
        $this->userService = $userService;
        $this->companyService = $companyService;
        $this->faker = Faker::create();
        $this->cartService = $cartService;
        $this->productRepository = $productRepository;

        $this->orderRepository = $orderRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->writeDemoData();
    }

    private function writeDemoData()
    {
        $demo_data = DB::table('demo_data')->select()->get();
        if (!empty($demo_data)) {
            $this->deleteDemoData();
            $this->refreshDemoData($demo_data);

        } else {
            $this->createDemoData();
        }
    }

    private function deleteDemoData()
    {
        $demo_user = null;
        $demo_company = null;

        $demo_data = DB::table('demo_data')->select()->get();
        if (!empty($demo_data)) {
            $uid = $demo_data[0]->user_id;
            $cid = $demo_data[0]->company_id;

            /** @var Company $demo_company */
            $demo_company = $this->companyRepository->find($cid);
            if (Cache::has('company_' . $demo_company->name)) {
                Cache::forget('company_' . $demo_company->name);
            }

            DB::statement('delete from products where company_id=' . $cid);
            DB::statement('delete from tasks where company_id=' . $cid);
            DB::statement('delete from cart where company_id=' . $cid);
            DB::statement('delete from orders where company_id=' . $cid);
            DB::statement('delete from clients where company_id=' . $cid);
            DB::statement('delete from firms where company_id=' . $cid);



            //DB::statement('delete from roles CASCADE where company_id=' . $cid);
            DB::statement('delete from companies CASCADE where user_id=' . $uid . ' AND id!= ' . $cid);

            DB::table('companies')->select()->where('id', '=', $cid)->update([
                'name' => Config::get('demo-company.name'),
                'status' => Config::get('demo-company.status'),
                'user_id' => $uid,
                'demo' => true,
                'display_name' => Config::get('demo-company.display_name'),
                'divisions' => Config::get('demo-company.divisions'),
                'updated_at' => 'NOW'
            ]);

            DB::table('users')->select()->where('id', '=', $uid)->update([
                    'name' => Config::get('demo-user.name'),
                    'status' => Config::get('demo-user.status'),
                    'email' => Config::get('demo-user.credentials.email'),
                    'password' => bcrypt(Config::get('demo-user.credentials.password')),
                    'phone' => Config::get('demo-user.phone'),
                    'patronymic' => Config::get('demo-user.patronymic'),
                    'last_name' => Config::get('demo-user.last_name'),
                    'updated_at' => 'NOW',
                ]);
        }
    }

    private function createDemoData()
    {
        DB::transaction(function () use (&$demo_user, &$demo_company) {
            // create demo user
            $uid = DB::table('users')->insertGetId([
                'name' => Config::get('demo-user.name'),
                'email' => Config::get('demo-user.credentials.email'),
                'password' => bcrypt(Config::get('demo-user.credentials.password')),
                'status' => Config::get('demo-user.status'),
                'phone' => Config::get('demo-user.phone'),
                'patronymic' => Config::get('demo-user.patronymic'),
                'last_name' => Config::get('demo-user.last_name'),
                'created_at' => 'NOW',
                'updated_at' => 'NOW',
            ]);
            // create demo company
            $cid = DB::table('companies')->insertGetId([
                'name' => Config::get('demo-company.name'),
                'status' => Config::get('demo-company.status'),
                'user_id' => $uid,
                'display_name' => Config::get('demo-company.display_name'),
                'divisions' => Config::get('demo-company.divisions'),
                'demo' => true,
                'created_at' => 'NOW',
                'updated_at' => 'NOW',
            ]);


            /** @var User $demo_user */
            $demo_user = $this->userRepository->find($uid);
            /** @var Company $demo_company */
            $demo_company = $this->companyRepository->find($cid);
            $demo_company->name = Config::get('demo-company.name');
            $demo_company->save();

            if (Cache::has('company_' . $demo_company->name)) {
                Cache::forget('company_' . $demo_company->name);
            }
            Cache::add('company_' . $demo_company->name, $demo_company, 60);

            //$demo_user->companies()->attach($cid);
            $this->companyService->attachUserToCompany($cid, $uid);
            $this->userService->addOwner($uid, $cid);

            $demo_data = DB::table('demo_data')->insert([
                'company_id' => $cid,
                'user_id' => $uid,
                'created_at' => 'NOW',
                'updated_at' => 'NOW',
            ]);

            $this->createDemoEntities($uid, $cid);
        });
    }

    /**
     * @param array $demo_data
     */
    private function refreshDemoData(array $demo_data)
    {
        $uid = $demo_data[0]->user_id;
        $cid = $demo_data[0]->company_id;
        $this->createDemoEntities($uid, $cid);
    }

    /**
     * @param $uid int Demo user ID
     * @param $cid int Demo Company ID
     */
    private function createDemoEntities($uid, $cid)
    {
        DB::transaction(function () use ($uid, $cid) {

            $products_ids = [];
            $client_ids = [];
            $firms_ids = [];
            $order_ids = [];
            $cart_ids = [];
            $task_ids = [];

            // Наполняем продукты
            $demo_products = Config::get('demo-entities.products');
            if (!empty($demo_products)) {
                foreach ($demo_products as $demo_product) {
                    $products_ids[] = DB::table('products')->insertGetId([
                        'name' => $demo_product['name'],
                        'description' => $demo_product['description'],
                        'article' => $demo_product['article'],
                        'new_price' => $demo_product['new_price'],
                        'prime_cost' => $demo_product['prime_cost'],
                        'profit' => $demo_product['profit'],
                        'old_price' => $demo_product['old_price'],
                        'min_price' => $demo_product['min_price'],
                        'company_id' => $cid,
                        'created_at' => 'NOW',
                        'updated_at' => 'NOW',
                    ]);
                }
            }

            // Заполняем клиентов
            for ($i = 0; $i < 50; $i++) {
                $status = ClientReference::statuses()[array_rand(ClientReference::statuses())];
                $type = ClientReference::types()[array_rand(ClientReference::types())];
                $client_ids[] = DB::table('clients')->insertGetId([
                    'status' => $status,
                    'type' => $type,
                    'company_id' => $cid,
                    'name' => $this->faker->name,
                    'email' => $this->faker->email,
                    'phone' => $this->faker->phoneNumber,
                    'created_at' => new DateTime(),
                    'updated_at' => new  DateTime(),
                ]);
            }
            // Заполняем фирмы
            for ($i = 0; $i < 50; $i++) {
                $status = FirmReference::statuses()[array_rand(FirmReference::statuses())];
                $type = FirmReference::types()[array_rand(FirmReference::types())];
                $firms_ids[] = DB::table('firms')->insertGetId([
                    'status' => $status,
                    'type' => $type,
                    'company_id' => $cid,
                    'name' => $this->faker->company,
                    'description' => $this->faker->companySuffix,
                    'email' => $this->faker->companyEmail,
                    'phone' => $this->faker->phoneNumber,
                    'created_at' => new DateTime(),
                    'updated_at' => new  DateTime(),
                ]);
            }
            // Создаем пользователей компании TODO:

            // Создаем заказы с фирмами
            for ($i = 0; $i < 10; $i++) {
                $status = OrderReference::statuses()[array_rand(OrderReference::statuses())];
                $firm_id = $firms_ids[array_rand($firms_ids)];
                $order_ids[] = DB::table('orders')->insertGetId([
                    'status' => $status,
                    'company_id' => $cid,
                    'firm_id' => $firm_id,
                    'responsible_user_id' => $uid,
                    'description' => $this->faker->text(200),
                    'created_at' => new DateTime(),
                    'updated_at' => new  DateTime(),
                ]);
            }
            // Создаем заказы с клиентами
            for ($i = 0; $i < 20; $i++) {
                $status = OrderReference::statuses()[array_rand(OrderReference::statuses())];
                $client_id = $client_ids[array_rand($client_ids)];
                $order_ids[] = DB::table('orders')->insertGetId([
                    'status' => $status,
                    'company_id' => $cid,
                    'client_id' => $client_id,
                    'responsible_user_id' => $uid,
                    'description' => $this->faker->text(200),
                    'created_at' => new DateTime(),
                    'updated_at' => new  DateTime(),
                ]);
            }
            // Создаем корзины и таски
            $demo_products_list = $this->productRepository->find($products_ids)->toArray();
            foreach ($order_ids as $order_id) {
                $random_products = [];
                $random_products_ids = [];
                $random_products_ids = array_rand($demo_products_list, rand(1, 5));

                if (is_array($random_products_ids)) {
                    foreach ($random_products_ids as $random_products_id) {
                        $random_products[$random_products_id] = $demo_products_list[$random_products_id];
                        $random_products[$random_products_id]['qty'] = rand(1, 10);
                    }
                } else {
                    $random_products[0] = $demo_products_list[$random_products_ids];
                    $random_products[0]['qty'] = rand(1, 10);
                }

                $data = [
                    'content' => [
                        'products' => $random_products
                    ],
                    'qty' => 1,
                    'order_id' => $order_id,
                    'company_id' => $cid,
                    'created_at' => new DateTime(),
                    'updated_at' => new  DateTime(),
                ];
                $this->cartService->prepareData($data);
                $cart_ids[] = DB::table('cart')->insertGetId($data);

                // task
                $order = $this->orderRepository->find($order_id);
                $data = [
                    'order_id' => $order_id,
                    'type' => TaskReference::types()[array_rand(TaskReference::types())],
                    'state' => TaskReference::states()[array_rand(TaskReference::states())],
                    'date_perf' => $this->faker->dateTimeBetween('now', '10 days'),
                    'responsible_user_id' => $uid,
                    'performer_user_id' => $uid,
                    'comment' => $this->faker->text(50),
                    'company_id' => $cid,
                    'created_at' => new DateTime(),
                    'updated_at' => new  DateTime(),
                ];
                if ($order->client_id) {
                    $data['client_id'] = $order->client_id;
                };
                if ($order->firm_id) {
                    $data['firm_id'] = $order->firm_id;
                };
                $task_ids[] = DB::table('tasks')->insertGetId($data);
            }


        });
    }
}
