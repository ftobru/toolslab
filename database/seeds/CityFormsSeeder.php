<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Envoy\SSH;
use League\Csv\Reader;
use IPGeoBase\IPGeoBase;

class CityFormsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("TRUNCATE TABLE city_forms CASCADE");
        $csvFile = storage_path().'/cities_forms.csv';
        $areas = $this->csvToArray($csvFile);
        foreach ($areas as $key => $area) {
            $city = DB::selectOne("SELECT idx FROM cities where city ILIKE '" . $area['name'] . "'");
            if ($city) {
                $areas[$key]['idx'] = $city->idx;
            } else {
                $areas[$key]['idx'] = null;
            }
            unset($areas[$key]['status']);
            unset($areas[$key]['rating']);
        }
        DB::table('city_forms')->insert($areas);
    }

    /**
     * @param string $filename
     * @param string $delimiter
     * @return array|bool
     */
    private function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    if(sizeof($header)===sizeof($row)){
                        $data[] = array_combine($header, $row);
                    }
                }
            }
            fclose($handle);
        }
        return $data;
    }
}
