<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laravel\Envoy\SSH;
use League\Csv\Reader;
use IPGeoBase\IPGeoBase;

class CitySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $downloadCmd = 'wget -P ' . storage_path() . '/app/ http://ipgeobase.ru/files/db/Main/geo_files.tar.gz';
        exec($downloadCmd);
        exec('tar xvzf ' . storage_path() . '/app/geo_files.tar.gz -C ' . storage_path() . '/app/');
        exec('rm ' . storage_path() . '/app/geo_files.tar.gz');
        DB::statement('delete from cities');
        /** @var IPGeoBase $geo */
        $geo = new IPGeoBase();
        $geo->fillCityTable();
    }
}
