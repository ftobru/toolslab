<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Illuminate\Support\Str;
use Laracasts\TestDummy\Factory as TestDummy;

class OauthClientTableSeeder extends Seeder
{

    public function run()
    {
        /** @var \LucaDegasperi\OAuth2Server\Storage\FluentClient $repository */
        $repository = App::make('LucaDegasperi\OAuth2Server\Storage\FluentClient');
        $repository->create('front', Str::random(), Str::random(32));
    }

}