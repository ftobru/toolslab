<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Collection;
use Saas\Models\Permission;

/**
 * Class PermissionTableSeeder
 */
class PermissionTableSeeder extends Seeder
{

    /**
     *
     */
    public function run()
    {
        DB::statement("TRUNCATE TABLE permissions CASCADE");
        Collection::make(Lang::get('permissions', [], 'ru'))->each(function ($attributes, $name) {
            $permission = new Permission();
            $permission->name = $name;
            $permission->display_name = $attributes['display_name'];
            $permission->description = $attributes['description'];
            $permission->save();
        });
    }
}
