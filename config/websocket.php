<?php

return [
    'server'   => env('SOCKET_SERVER', 'http://localhost'),
    'client'   => env('SOCKET_CLIENT', 'http://localhost'),
    'port'  => env('SOCKET_PORT', '8890'),
    'debug' => env('SOCKET_DEBUG', false),
    'salt' => env('SOCKET_SALT', 'IamSocketSalt')
];