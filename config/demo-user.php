<?php

use Saas\References\UserReference;

return [
    'credentials' => [
        'email' => 'demo@demo.ru',
        'password' => '1js2Eds3c3fs3dcKLj9kLl'
    ],
    'name' => 'Иван',
    'last_name' => 'Иванов',
    'patronymic' => 'Иванович',
    'phone' => '(000) 000-0000',
    'status' => UserReference::STATUS_ACTIVE
];
