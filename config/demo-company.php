<?php

use Saas\References\CompanyReference;

return [
    'name' => 'demo',
    'display_name' => 'ModulCRM',
    'status' => CompanyReference::STATUS_ACTIVE,
    'divisions' => '"отдел продаж, маркетинг, разработка"'
];
