<?php

use Crm\Models\Client;
use Crm\Models\Firm;
use Crm\Models\Product;

return [
    'en_types' => [
        Product::class => 1,
        Client::class => 2,
        Firm::class => 3
    ],
    'form_types' => [
        'text' => 1,
        'select' => 2
    ]
];
