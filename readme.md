# ToolsLab CRM

* * *

## Требования (Server Requirements)

* PHP 5.6
* php5-mcrypt
* php5-redis (Predis\Client)
* php5-json
* git-core
* Composer 
* pdo-pgsql
* PostgreSQL 9.4
* Redis-Sever
* Node package manager (npm)
* Bower

* * *

## Установка / Installation

Для функционирования приложения  и частности framework [Laravel]: http://laravel.com/docs/5.0 вам необходимо установить и 
настроить на сервере PHP 5.6 и такие расширения для него как mcrypt, predis, pdo-pgsql, php5-json.  

После чего вам потребуется [Composer]: https://getcomposer.org/download/ и [Git]: http://git-scm.com. Клонируйте проект
 в ваш рабочий каталог и в папке с проектом сделайте `$ composer update`.
 
Для подгрузки javascript зависимостей используйте [Bower]: http://bower.io , `$ bower install`

* * *


## База данных / Database

Хотел бы обратить внимание на то что у нас используется PostgreSQL именно 9.4, связано это с использованием JSONB и JSQUERY. Для того что бы залить дам необходимо 
выполнить две команды из рабочего каталога `$ php artisan migrate` и `$ php artisan db:seed`

* * *

## API

Практически все сущности в проекте доступны по REST протоколу и имеют вид (/api/v1/{название_сущности}).
В случае событий (events) реализация перенесена на сторону web-socket server'а [Ratchet]: http://socketo.me/ ,
 на стороне клиента работа с сокетом реализована  при помощи библиотеки [Brain-socket]: https://github.com/BrainBoxLabs/brain-socket-js.
 Для запуска сервера необходимо выполнить команду `$ php artisan socket:start`
 
* * *

### Фоновые процессы (Supervisor) / Background process (Supervisor)

Для настройки supervisor'а необходимо прокинуть sin-link на файл конфигураций расположенный в каталоге проекта 
- `/config/supervisor.d/saas.conf`. Подробнее об настройки supervisor смотреть тут 

* * *


* * *

### SMS - Master key

6JWgLbAWOS4CVmbgVNwEoYBBit2ScYq7Ew1VJE9v

* * *
