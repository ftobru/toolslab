#!/usr/bin/env bash


docker build -t registry.tubecj.com/stock-web:latest-base docker/base/nginx
docker push registry.tubecj.com/stock-web:latest-base

docker build -t registry.tubecj.com/stock-app:latest-base docker/base/php
docker push registry.tubecj.com/stock-app:latest-base

docker build -t registry.tubecj.com/stock-data:latest-base docker/base/data
docker push registry.tubecj.com/stock-data:latest-base

docker build -t registry.tubecj.com/stock-web:latest-dev docker/dev/nginx
docker push registry.tubecj.com/stock-web:latest-dev

docker build -t registry.tubecj.com/stock-app:latest-dev docker/dev/php
docker push registry.tubecj.com/stock-app:latest-dev

docker build -t registry.tubecj.com/stock-data:latest-dev docker/dev/data
docker push registry.tubecj.com/stock-data:latest-dev

docker build -t registry.tubecj.com/stock-web:latest-prod docker/prod/nginx
docker push registry.tubecj.com/stock-web:latest-prod

docker build -t registry.tubecj.com/stock-app:latest-prod docker/prod/php
docker push registry.tubecj.com/stock-app:latest-prod

docker build -t registry.tubecj.com/stock-data:latest-prod .
docker push registry.tubecj.com/stock-data:latest-prod