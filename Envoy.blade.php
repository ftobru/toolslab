@servers(['web' => 'toolslab_admin@148.251.186.11'])

@task('backend:migrate')
    cd /var/www/backend
    php artisan migrate --force
@endtask

@task('backend:seed')
    cd /var/www/backend
    php artisan db:seed --force

@endtask
@task('backend:down')
    cd /var/www/backend
    php artisan down
    {{--sudo supervisorctl stop saas-socket--}}
@endtask

@task('backend:update')
    cd /var/www/backend
    chmod -R 777 /var/www/backend/storage
    git fetch --all
    git reset --hard origin/develop
    git pull origin develop
    composer update
    node node_modules/gulp/bin/gulp.js
    chmod -R 777 /var/www/backend/storage
    chmod +x /var/www/backend/artisan
@endtask

@task('backend:up')
    cd /var/www/backend
    php artisan up

    {{--sudo supervisorctl start saas-socket--}}
@endtask

@macro('backend:deploy')
    backend:down
    backend:update
    backend:migrate
    backend:up
@endmacro