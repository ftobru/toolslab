<?php

namespace Ftob\WebSocketClient;

use App;
use \WebSocket\Client as WClient;

class WebSocketClient 
{
    protected $uri = 'ws://localhost';
    protected $port = 8080; //by default
    protected $transport;
    
    public function __construct($uri, $port)
    {
        $this->uri = $uri;
        $this->port = $port;
        $this->transport = new WClient($this->uri . ":". $this->port);
    }

    
    public function emit($event, $data)
    {
        $this->transport->send(json_encode(['client'=>['event'=>$event, 'data'=>$data]]));
        
        return null; //$this->transport->receive();
    }
    
    public function message($package) {
        $this->transport->send(json_encode(['client'=>$package]));
        return null;// $this->transport->receive();
    }

}

