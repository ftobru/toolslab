<?php namespace Ftob\WebSocketClient;

use Illuminate\Support\ServiceProvider;

use Illuminate\Contracts\Config;

class WebSocketClientServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app['WebSocketClient'] = $this->app->share(function($app)
		{
		    return new WebSocketClient(Config::get( 'websocket.server.uri'), Config::get('websocket.port'));
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}
//
//	public function boot()
//	{
//	    $this->('ftob/web-socket-client');
//	}

}
