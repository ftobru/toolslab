<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:50
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на удаление делегированного доступа к счету.
 * Request for deletion of access delegation.
 *
 */
class MonetaDeleteAccountRelationRequest
{

    /**
     * Номер счета в системе МОНЕТА.РУ.
     * Account number.
     *
     *
     * @var long
     */
    public $accountId = null;

    /**
     * Email пользователя.
     * User's Email.
     *
     *
     * @var string
     */
    public $principalEmail = null;

}