<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:27
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий делегированный доступ к счету.
 * Type describing access to delegated account.
 *
 */
class MonetaAccountRelation
{

    /**
     * Номер счета в системе МОНЕТА.РУ.
     * Account number.
     *
     *
     * @var long
     */
    public $accountId = null;

    /**
     * Email пользователя, которому предоставляется делегированный доступ к счету.
     * Email of user which has to be provided with delegated account access.
     *
     *
     * @var string
     */
    public $principalEmail = null;

    /**
     * Просмотр счета.
     * View account.
     *
     *
     * @var boolean
     */
    public $canViewAccount = null;

    /**
     * Редактирование счета.
     * Edit account.
     *
     *
     * @var boolean
     */
    public $canEditAccount = null;

    /**
     * Проведение операций.
     * Process operations.
     *
     *
     * @var boolean
     */
    public $canProcessOperation = null;

}