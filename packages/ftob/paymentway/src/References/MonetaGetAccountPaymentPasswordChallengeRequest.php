<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:10
 */


namespace Ftob\Paymentway\References;


/**
 * Получить запрос для платежного пароля
 * Receive challenge for payment password
 *
 */
class MonetaGetAccountPaymentPasswordChallengeRequest
{

    /**
     * Номер счета
     * Account number
     *
     *
     * @var long
     */
    public $accountId = null;

}