<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:30
 */


namespace Ftob\Paymentway\References;


/**
 * Запрос на перевод денежных средств в пакетном режиме.
 * Money Transfer Request in batch processing mode.
 *
 */
class MonetaTransferBatchRequest extends MonetaTransactionBatchRequestType
{

}