<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:57
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на завершение (подтверждение) операции.
 * Transaction confirmation request.
 *
 */
class MonetaConfirmTransactionRequest extends MonetaConfirmTransactionRequestType
{

}