<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 18:58
 */

namespace Ftob\Paymentway\References;

/**
 * Тип, описывающий статусы счетов в системе МОНЕТА.РУ.
 * Счет может быть либо активным, либо заблокированным.
 * Account status in MONETA.RU.
 * May be either active or inactive.
 *
 */
class MonetaAccountStatus
{

    /**
     * Активный счет
     * Active account
     *
     *
     * @var int
     */
    const AccountStatus1 = '1';

    /**
     * Заблокированный счет
     * Inactive (blocked) account
     *
     *
     * @var int
     */
    const AccountStatus2 = '2';

}
