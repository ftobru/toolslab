<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:22
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий доступ к счету.
 * Account access description type.
 *
 */
class MonetaAccountAccessInfo
{

    /**
     * Доступ на изменение свойств счета
     * Account attributes modification access
     *
     *
     * @var boolean
     */
    public $accessToWrite = null;

    /**
     * Доступ на снятие средств со счета
     * Account debiting access
     *
     *
     * @var boolean
     */
    public $accessToTakenOut = null;

    /**
     * Доступ на зачисление средств на счет
     * Account crediting access
     *
     *
     * @var boolean
     */
    public $accessToTakenIn = null;

}