<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:11
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на редактирование счета.
 * У счета можно изменить свойства "псевдоним" и "платежный пароль".
 * Account modification request.
 * It's allowed to change alias and payment password for account.
 *
 */
class MonetaEditAccountRequest
{

    /**
     * Идентификатор редактируемого счета.
     * Modifying account's number.
     *
     *
     * @var long
     */
    public $id = null;

    /**
     * Новый псевдоним счета.
     * Псевдоним счета должен быть уникальным среди счетов пользователя.
     * Необязательное поле.
     * New alias of account.
     * Must be unique among accounts of given user.
     * Optional.
     *
     *
     * @var string
     */
    public $alias = null;

    /**
     * Новый платежный пароль. Для того чтобы изменить платежный
     * пароль, необходимо указать предыдущий платежный пароль. Необязательное поле.
     * New payment password for account.
     * oldPaymentPassword is required to set new payment password.
     * Optional.
     *
     *
     * @var string
     */
    public $paymentPassword = null;

    /**
     * Текущий платежный пароль. Для того чтобы изменить платежный
     * пароль на новый, необходимо указать текущий платежный пароль. Необязательное поле.
     * Current payment password.
     * Used for authorization of changing payment password to new value.
     * Required only if newPaymentPassword is specified.
     * Optional.
     *
     *
     * @var string
     */
    public $oldPaymentPassword = null;

    /**
     * URL после списания средств. Необязательное поле.
     * URL on debiting. Optional.
     *
     *
     * @var string
     */
    public $onSuccessfulDebitUrl = null;

    /**
     * URL после зачисления средств. Необязательное поле.
     * URL on crediting. Optional.
     *
     *
     * @var string
     */
    public $onSuccessfulCreditUrl = null;

    /**
     * Код проверки целостности данных. Необязательное поле.
     * Mandatory payment form signature. Optional.
     *
     *
     * @var string
     */
    public $signature = null;

    /**
     * Если баланс счета меньше данного значения, то раз в сутки уходит уведомление об этом событии. Необязательное поле.
     * Daily notifications if balance is less than threshold. Optional.
     *
     *
     * @var decimal
     */
    public $lowBalanceThreshold = null;

    /**
     * Если баланс счета больше данного значения, то раз в сутки уходит уведомление об этом событии. Необязательное поле.
     * Daily notifications if balance is greater than threshold. Optional.
     *
     *
     * @var decimal
     */
    public $highBalanceThreshold = null;

    /**
     * Счет-прототип с которого берутся свойства "по умолчанию". Необязательное поле.
     * Prototype account for default properties values. Optional.
     *
     *
     * @var long
     */
    public $prototypeAccountId = null;

    /**
     * Запрос для платежного пароля
     * Payment password challenge
     *
     *
     * @var string
     */
    public $oldPaymentPasswordChallenge = null;

    /**
     * URL после отмены списания средств. Необязательное поле.
     * URL on cancelled debit. Optional.
     *
     *
     * @var string
     */
    public $onCancelledDebitUrl = null;

    /**
     * URL после отмены зачисления средств. Необязательное поле.
     * URL on cancelled credit. Optional.
     *
     *
     * @var string
     */
    public $onCancelledCreditUrl = null;

}