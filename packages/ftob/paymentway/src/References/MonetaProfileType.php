<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:03
 */


namespace Ftob\Paymentway\References;


/**
 * Тип данных пользователя.
 * User profile information type.
 *
 */
class MonetaProfileType
{

    /**
     * Тип данных "Организация". В данных такого типа присутствуют поля,
     * характерные для организаций, например: "Название организации", "ФИО директора" и т.п.
     * "Organization" profile type containing organization related information.
     *
     *
     * @var string
     */
    const organization = 'organization';

    /**
     * Тип данных "Клиент". В данных такого типа присутствуют поля,
     * характерные для обычных пользователей, например: "фамилия", "имя", "отчество" и т.д.
     * "Client" profile type containing person related information.
     *
     *
     * @var string
     */
    const client = 'client';

}