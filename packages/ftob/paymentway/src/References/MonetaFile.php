<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:26
 */


namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий данные в виде бинарного файла.
 * Binary data type.
 *
 */
class MonetaFile
{

    /**
     * ID документа, которому принадлежит данный файл
     * Document ID, which file belongs to
     *
     *
     * @var long
     */
    public $documentId = null;

    /**
     * Данные файла. При передаче/получении данных используйте MTOM (Message Transmission Optimization Mechanism).
     * File contents. Use MTOM (Message Transmission Optimization Mechanism).
     *
     *
     * @var base64Binary
     */
    public $blob = null;

    /**
     * Проверен или нет данный файл
     * Is file approved or not
     *
     *
     * @var boolean
     */
    public $approved = null;

    /**
     * ID файла
     * File Id
     *
     *
     * @var long
     */
    public $fileId = null;

    /**
     * Mime type файла (например: image/jpeg)
     * File mime type (example: image/jpeg)
     *
     *
     * @var string
     */
    public $mimeType = null;

    /**
     * Имя файла или описание
     * File name or description
     *
     *
     * @var string
     */
    public $title = null;

}