<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:25
 */

namespace Ftob\Paymentway\References;



/**
 * Поиск пользователя в системе МОНЕТА.РУ по заданному фильтру.
 * В результате возвратится список пользователей, разбитый на страницы
 * Profile searching by filter parameters.
 * Result contains paged profile list.
 *
 */
class MonetaFindProfileInfoRequest extends MonetaEntity
{

    /**
     * Настройки страницы данных. Необязательное поле.
     * Paging settings. Optional.
     *
     *
     * @var MonetaPager
     */
    public $pager = null;

    /**
     * Фильтр, по которому ищем данные
     * Searching filter
     *
     *
     * @var MonetaFindProfileInfoRequestFilter
     */
    public $filter = null;

}