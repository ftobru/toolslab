<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:21
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на получение данных пользователя по номеру счета.
 * Если данные не найдены, возникает Exception.
 * Request profile information by account number.
 * Exception is thrown if no data found.
 *
 */
class MonetaFindProfileInfoByAccountIdRequest
{

}