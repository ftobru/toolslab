<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:23
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, который позволяет работать с сущностями типа "ключ-значение".
 * Key-value pairs type.
 *
 */
class MonetaKeyValueAttribute
{

    /**
     *
     *
     * @var string
     */
    public $key = null;

    /**
     *
     *
     * @var string
     */
    public $value = null;

}