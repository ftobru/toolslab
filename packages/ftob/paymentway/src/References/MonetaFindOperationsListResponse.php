<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:20
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос FindOperationsListRequest.
 * В результате возвращается список операций, разбитый по страницам.
 * Transaction searching response.
 * Result contains paged transactions list.
 *
 */
class MonetaFindOperationsListResponse extends MonetaOperationInfoList
{

}