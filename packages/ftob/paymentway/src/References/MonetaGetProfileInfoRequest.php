<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:23
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на получение данных пользователя по ID пользователя.
 * Если данные не найдены, возникает Exception.
 * Request for profile information by structure ID.
 * Exception is thrown if no data found.
 *
 */
class MonetaGetProfileInfoRequest extends MonetaEntity
{

    /**
     * ID пользователя в системе МОНЕТА.РУ.
     * Если это поле не задано, то используется текущий пользователь.
     * Необязательное поле.
     * Structure ID.
     * If omitted authenticated user's structure is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

}