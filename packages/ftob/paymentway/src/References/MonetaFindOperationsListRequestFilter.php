<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:19
 */

namespace Ftob\Paymentway\References;


/**
 * Фильтр, по которому ищем операции.
 * Transactions filter.
 *
 */
class MonetaFindOperationsListRequestFilter
{

    /**
     * ID пользователя в системе МОНЕТА.РУ
     * Structure identificator in MONETA.RU
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * Номер счета
     * Account number
     *
     *
     * @var long
     */
    public $accountId = null;

    /**
     * Дата начала периода
     * Period's start
     *
     *
     * @var dateTime
     */
    public $dateFrom = null;

    /**
     * Дата конца периода
     * Period's end
     *
     *
     * @var dateTime
     */
    public $dateTo = null;

    /**
     * Номер операции в системе МОНЕТА.РУ
     * Transaction ID in MONETA.RU
     *
     *
     * @var long
     */
    public $operationId = null;

    /**
     * Сумма от... Ищутся все операции, которые проходили
     * на большую или равную сумму. Если указано это поле, то обязательно надо
     * заполнить Currency
     * Minimal transaction amount.
     * If specified currencyCode filter parameter is required.
     *
     *
     * @var decimal
     */
    public $amountFrom = null;

    /**
     * Сумма до... Ищутся все операции, которые проходили
     * на меньшую или равную сумму. Если указано это поле, то обязательно надо
     * заполнить Currency
     * Maximal transaction amount.
     * If specified currencyCode filter parameter is required.
     *
     *
     * @var decimal
     */
    public $amountTo = null;

    /**
     * Валюта, по которой проходили операции
     * Currency of transaction account.
     *
     *
     * @var string
     */
    public $currencyCode = null;

    /**
     * Корреспонденский номер счета в системе МОНЕТА.РУ
     * Correspondent account number in MONETA.RU
     *
     *
     * @var long
     */
    public $targetAccountId = null;

    /**
     * Статус операции
     * Transaction status
     *
     *
     * @var string
     */
    public $operationStatus = null;

    /**
     * Внешний номер операции
     * External transaction ID
     *
     *
     * @var string
     */
    public $clientTransaction = null;

    /**
     * 1 - все операции;
     * 2 - операции, где деньги были получены (сумма > 0);
     * 3 - операции, где деньги были потрачены (сумма < 0).
     * 1 - all transactions;
     * 2 - credit transactions;
     * 3 - debit transactions.
     *
     *
     * @var int
     */
    public $operationAmountTypeId = null;

    /**
     * Название свойства операции. Может принимать следующие значения:
     * YANDEXACCOUNT - счет в системе Яндекс.Деньги;
     * WEBMONEYWMID - WebMoney WMID;
     * WEBMONEYPURSE - WebMoney кошелек;
     * CONTACTTRANSFERORDERNUMBER - Номер перевода в системе Contact;
     * WIRETRANSFERORDERNUMBER - Номер платежного поручения в банковском переводе;
     * Значение свойства указывается в поле propertyValue.
     * The transaction property name may take following values:
     * YANDEXACCOUNT - user's account number in Yandex.Money;
     * WEBMONEYWMID - user's WMID in WebMoney;
     * WEBMONEYPURSE - user's purse number in WebMoney;
     * CONTACTTRANSFERORDERNUMBER - transfer idenitification number in Contact;
     * WIRETRANSFERORDERNUMBER - bank transfer identification number.
     * The value of property has to be specified in "propertyValue" element.
     *
     *
     * @var string
     */
    public $propertyName = null;

    /**
     * Значение свойства операции.
     * Поиск происходит по прямому совпадению. Для задания маски
     * можно указать спец-символы - "*" или "?".
     * Transaction property value.
     * Wildcards "*" and "?" may be used.
     *
     *
     * @var string
     */
    public $propertyValue = null;

    /**
     * 1 - Ввод средств;
     * 2 - Вывод средств;
     * 3 - Внутренний перевод;
     * 4 - Товары и услуги.
     * 1 - Deposit;
     * 2 - Withdrawal;
     * 3 - Transfer;
     * 4 - Goods and services.
     *
     *
     * @var long
     */
    public $operationCategoryId = null;

}