<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 18:55
 */

namespace Ftob\Paymentway\References;



/**
 * Список версий, который используется в MONETA.MerchantAPI.
 * Если функциональность зависит от конкретной версии, то в описании запроса это будет указано дополнительно.
 * Если версия не указана, то по умолчанию используется VERSION_1.
 * The version enumeration which is used in MONETA.MerchantAPI.
 * If functionality depends on certain version it will be described for each request type.
 * VERSION_1 is considered by default.
 *
 */
class MonetaVersion
{

    /**
     *
     *
     * @var string
     */
    const VersionVERSION_1 = 'VERSION_1';

    /**
     *
     *
     * @var string
     */
    const VersionVERSION_2 = 'VERSION_2';

}

