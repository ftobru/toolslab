<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:12
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ, который приходит на запрос EditAccountRequest.
 * В ответе нет никаких данных.
 * Если в процессе сохранения счета произошла ошибка, то возникнет Exception.
 * Если Exception не возник - значит счет гарантированно сохранен.
 * Account modification response.
 * Response contains no data unless some error has occured while modification.
 * If reponse has no Exception then account is modified successfully.
 *
 */
class MonetaEditAccountResponse
{

}