<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:25
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, позволяющий задать необходимую страницу при отображении длинных списков.
 * Long list retrieval type.
 *
 */
class MonetaPager
{

    /**
     * Номер страницы, которую нужно получить. Минимальное значение равно 1.
     * Значение по умолчанию равно 1.
     * Page number to retrieve. Minimal value is 1.
     * By default is 1.
     *
     *
     * @var int
     */
    public $pageNumber = null;

    /**
     * Сколько записей необходимо получить на одной странице. Минимальное
     * значение равно 1. Максимальное значение равно 100. Значение по умолчанию равно 10.
     * Transactions per page. Minimal value is 1.
     * Maximal value is 100.
     * By default is 10.
     *
     *
     * @var int
     */
    public $pageSize = null;

}