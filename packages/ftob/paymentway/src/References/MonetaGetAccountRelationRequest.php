<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:42
 */


namespace Ftob\Paymentway\References;


/**
 * Запрос на получение данных по делегированному доступу.
 * Request for delegated access details.
 *
 */
class MonetaGetAccountRelationRequest
{

    /**
     * Номер счета в системе МОНЕТА.РУ.
     * Account number.
     *
     *
     * @var long
     */
    public $accountId = null;

    /**
     * Email пользователя.
     * User's Email.
     *
     *
     * @var string
     */
    public $principalEmail = null;

}