<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:32
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос создания/редактирования файла документа
 * Если в процессе сохранения файла произошла ошибка, то возникнет Exception.
 * Если Exception не возник - значит файл гарантированно сохранен.
 * Response for file adding/modification.
 * Response contains no data unless some error has occured while modification.
 * If reponse has no Exception then file is uploaded successfully.
 *
 */
class MonetaUploadProfileDocumentFileResponse
{

}