<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:42
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос FindAccountRelationsRequest.
 * Response containing the list of delegated users.
 *
 */
class MonetaFindAccountRelationsResponse
{

    /**
     * Список найденных пользователей, которые имеют делегированный доступ к указанному счету. Если пользователей нет - список пустой.
     * List of found users having delegated account access. The list is empty if no users are found.
     *
     *
     * @var MonetaAccountRelation
     */
    public $accountRelation = null;

    /**
     * Список найденных пользователей, которые имеют делегированный доступ к указанному счету. Если пользователей нет - список пустой.
     * List of found users having delegated account access. The list is empty if no users are found.
     *
     *
     * @param MonetaAccountRelation
     *
     * @return void
     */
    public function addAccountRelation(MonetaAccountRelation $item)
    {
        $this->accountRelation[] = $item;
    }

}