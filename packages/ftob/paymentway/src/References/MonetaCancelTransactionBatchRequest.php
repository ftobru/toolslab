<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:01
 */


namespace Ftob\Paymentway\References;



/**
 * Запрос на отмену операций в пакетном режиме.
 * Transactions cancelation request in batch processing mode.
 *
 */
class MonetaCancelTransactionBatchRequest extends MonetaCancelTransactionBatchRequestType
{

}