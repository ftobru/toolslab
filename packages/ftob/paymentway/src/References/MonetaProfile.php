<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:25
 */


namespace Ftob\Paymentway\References;

/**
 * Данные пользователя в системе МОНЕТА.РУ.
 * Данные представляются в виде "ключ-значение" и признака подтвержденности.
 * Можно получить доступ либо к собственным, либо к публично доступным данным
 * (то есть к тем данным, которые пользователь сам разрешил для просмотра).
 * Возможные ключи для пользователей с типом "client":
 * unitid - ID пользователя в системе МОНЕТА.РУ;
 * last_name - фамилия;
 * first_name - имя;
 * middle_initial_name - отчество;
 * alias - псевдоним;
 * country - страна;
 * state - область, республика, штат;
 * city - город;
 * zip - индекс;
 * address - адрес;
 * email_for_notifications - адрес электронной почты;
 * phone - номер телефона;
 * cell_phone - номер сотового телефона;
 * url - url сайта;
 * sex - пол. MALE - мужской, FEMALE - женский;
 * date_of_birth - дата рождения (формат - yyyy-mm-dd);
 * inn - номер ИНН;
 * timezone - часовой пояс пользователя;
 * ui_language - язык интерфейса пользователя. RU - руский, EN - английский;
 * customfield:* - произвольный набор значений.
 * В данных пользователя их может быть несколько.
 * Полный ключ атрибута состоит из префикса ("customfield:") и тэга (32 символа).
 * Например, "customfield:name".
 * Возможные ключи для пользователей с типом "organization":
 * unitid - ID пользователя в системе МОНЕТА.РУ;
 * organization_name - название организации;
 * alias - псевдоним;
 * fio_director - ФИО директора;
 * phone_director - телефон директора;
 * fio_accountant - ФИО бухгалтера;
 * phone_accountant - телефон бухгалтера;
 * legal_address - юридический адрес;
 * post_address - почтовый адрес;
 * actual_address - фактический адрес;
 * url - url сайта;
 * contact_info - контактная информация;
 * contact_email - контактный e-mail;
 * business_activity - вид деятельности;
 * inn - номер ИНН;
 * contract_number - номер договора (только для чтения);
 * contract_date - дата договора (только для чтения);
 * timezone - часовой пояс пользователя;
 * ui_language - язык интерфейса пользователя. RU - руский, EN - английский;
 * email_for_notifications - адрес электронной почты;
 * customfield:* - произвольный набор значений.
 * В данных пользователя их может быть несколько.
 * Полный ключ атрибута состоит из префикса ("customfield:") и тэга (32 символа).
 * Например, "customfield:name".
 * User profile information.
 * Data is presented as key-value pairs list.
 * Returned attributes are either owned by authenticated user
 * or marked as publicly available by profile owner.
 * Possilble keys for "client" profiles:
 * unitid - structure identificator in MONETA.RU;
 * last_name;
 * first_name;
 * middle_initial_name;
 * alias;
 * country;
 * state;
 * city;
 * zip;
 * address;
 * email_for_notifications;
 * phone;
 * cell_phone;
 * url;
 * sex (MALE|FEMALE);
 * date_of_birth (yyyy-mm-dd format);
 * inn - Tax ID;
 * timezone;
 * ui_language (RU|EN);
 * customfield:* - custom list of values.
 * User profile may contain several attributes with different keys.
 * The full attribute's key consists of prefix ("customfield:") and tag (32 characters).
 * For example, "customfield:name".
 * Possible keys for "organization" profile:
 * unitid - structure identificator in MONETA.RU;
 * organization_name;
 * alias;
 * fio_director - Director's Fullname ;
 * phone_director - Director's phone;
 * fio_accountant - Accountant's fullname;
 * phone_accountant - Accountant's phone;
 * legal_address - official organization address;
 * post_address - post address;
 * actual_address - actual address;
 * url - site url;
 * contact_info - contact info;
 * contact_email - contact e-mail;
 * business_activity - business activity;
 * inn - Tax ID;
 * contract_number - contract number;
 * contract_date - contract date;
 * timezone - user's time zone;
 * ui_language (RU|EN);
 * email_for_notifications;
 * customfield:* - custom list of values.
 * Organization profile may contain several attributes with different keys.
 * The full attribute's key consists of prefix ("customfield:") and tag (32 characters).
 * For example, "customfield:name".
 * Список идентификаторов временных зон, которые используются в системе, доступен на странице wikipedia.org
 * Timezone identificators supported in MONETA.RU available as TZ name on wikipedia.org
 *
 */
class MonetaProfile
{

    /**
     *
     *
     * @var MonetaKeyValueAttribute
     */
    public $attribute = null;

    /**
     *
     *
     * @param MonetaKeyValueAttribute
     *
     * @return void
     */
    public function addAttribute(MonetaKeyValueAttribute $item)
    {
        $this->attribute[] = $item;
    }

}