<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:06
 */


namespace Ftob\Paymentway\References;


/**
 * Запрос возможности провести операцию в системе МОНЕТА.РУ.
 * Transaction verification request.
 *
 */
class MonetaVerifyTransferRequest extends MonetaTransactionRequestType
{

}