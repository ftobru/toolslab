<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:23
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ, который приходит на запрос EditProfileRequest.
 * В ответе нет никаких данных.
 * Если в процессе сохранения пользователя произошла ошибка, то возникнет Exception.
 * Если Exception не возник - значит пользователь гарантированно сохранен.
 * Profile modification response.
 * Response contains no data unless error has occured while saving information
 * and Exception returned as a result.
 *
 */
class MonetaEditProfileResponse
{

}