<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:59
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос завершения операций в пакетном режиме.
 * Transactions confirmation response in batch processing mode.
 *
 */
class MonetaConfirmTransactionBatchResponse
{

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в ConfirmTransactionBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @var MonetaOperationInfoBatchResponseType
     */
    public $transaction = null;

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в ConfirmTransactionBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @param MonetaOperationInfoBatchResponseType
     *
     * @return void
     */
    public function addTransaction(MonetaOperationInfoBatchResponseType $item)
    {
        $this->transaction[] = $item;
    }

}