<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:22
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос FindProfileInfoByAccountIdRequest.
 * Profile information by account number searching response.
 *
 */
class MonetaFindProfileInfoByAccountIdResponse
{

    /**
     * Номер счета пользователя в системе МОНЕТА.РУ
     * Account number in MONETA.RU
     *
     *
     * @var long
     */
    public $accountId = null;

    /**
     * Валюта счета
     * Currency of account
     *
     *
     * @var string
     */
    public $currency = null;

    /**
     * Данные пользователя в системе МОНЕТА.РУ
     * User profile information in MONETA.RU
     *
     *
     * @var MonetaProfile
     */
    public $profile = null;

}