<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:01
 */


namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос CancelTransactionBatchRequest.
 * Transactions cancelation response in batch processing mode.
 *
 */
class MonetaCancelTransactionBatchResponse
{

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в CancelTransactionBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @var MonetaCancelTransactionBatchResponseType
     */
    public $transaction = null;

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в CancelTransactionBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @param MonetaCancelTransactionBatchResponseType
     *
     * @return void
     */
    public function addTransaction(MonetaCancelTransactionBatchResponseType $item)
    {
        $this->transaction[] = $item;
    }

}