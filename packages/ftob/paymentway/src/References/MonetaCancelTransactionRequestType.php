<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:14
 */


namespace Ftob\Paymentway\References;


/**
 * Тип для запроса на отмену операции.
 * Transaction cancelation request type.
 *
 */
class MonetaCancelTransactionRequestType
{

    /**
     * Номер операции
     * Transaction ID
     *
     *
     * @var long
     */
    public $transactionId = null;

    /**
     * Код протекции
     * Protection code
     *
     *
     * @var string
     */
    public $protectionCode = null;

    /**
     * Описание операции
     * Transaction description or comments
     *
     *
     * @var normalizedString
     */
    public $description = null;

}