<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:27
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ, который приходит на запрос FindProfileInfoRequest.
 * Данные в ответе разбиты по страницам.
 * Если данные не найдены, то size в ответе равен 0.
 * Profile searching response.
 * Result is paged.
 * Response element "size" is 0 if no data found.
 *
 */
class MonetaFindProfileInfoResponse
{

    /**
     * Количество пользователей, возвращаемых в результате запроса.
     * Profiles per page.
     *
     *
     * @var long
     */
    public $pageSize = null;

    /**
     * Номер текущей страницы. Нумерация начинается с 1
     * Current page number starting from 1.
     *
     *
     * @var long
     */
    public $pageNumber = null;

    /**
     * Сколько всего страниц с пользователями возможно по данному
     * запросу
     * Total count of pages.
     *
     *
     * @var long
     */
    public $pagesCount = null;

    /**
     * Размер текущей страницы. Он всегда <= pageSize. На последней
     * странице может быть < pageSize
     * Profiles count in current page.
     *
     *
     * @var long
     */
    public $size = null;

    /**
     * Общее количество пользователей, которое можно получить в данной
     * выборке
     * Total profiles count matched by searching filter.
     *
     *
     * @var long
     */
    public $totalSize = null;

    /**
     * Список пользователей
     * Profile list
     *
     *
     * @var MonetaProfile
     */
    public $profile = null;

    /**
     * Список пользователей
     * Profile list
     *
     *
     * @param MonetaProfile
     *
     * @return void
     */
    public function addProfile(MonetaProfile $item)
    {
        $this->profile[] = $item;
    }

}
