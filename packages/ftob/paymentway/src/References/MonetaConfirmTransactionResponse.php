<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:58
 */


namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос завершения операции.
 * Transaction confirmation response.
 *
 */
class MonetaConfirmTransactionResponse extends MonetaOperationInfo
{

}
