<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:58
 */

namespace Ftob\Paymentway\References;

/**
 * Запрос на завершение (подтверждение) операций в пакетном режиме.
 * Transactions confirmation request in batch processing mode.
 *
 */
class MonetaConfirmTransactionBatchRequest extends MonetaConfirmTransactionBatchRequestType
{

}
