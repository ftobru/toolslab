<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:36
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на создание документа пользователя.
 * Profile's document registration request.
 *
 */
class MonetaCreateProfileDocumentRequest extends MonetaDocument
{

    /**
     * Пользователь, которому будет принадлежать данный документ.
     * Если это поле не задано, то документ создается для текущего пользователя.
     * Необязательное поле.
     * Structure element, where the document belongs to.
     * If omitted authenticated user's structure is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

}