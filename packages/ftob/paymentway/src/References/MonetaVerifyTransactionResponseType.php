<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:20
 */


namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос проверки проведения операции в системе МОНЕТА.РУ
 * Transaction verification response.
 *
 */
class MonetaVerifyTransactionResponseType extends MonetaVerifyTransferResponseType
{

    /**
     * Идентификатор операции в системе МОНЕТА.РУ.
     * The identifier of transaction.
     *
     *
     * @var long
     */
    public $transactionId = null;

    /**
     * Статус операции в системе МОНЕТА.РУ.
     * Transaction status type in MONETA.RU
     *
     *
     * @var string
     */
    public $operationStatus = null;

}