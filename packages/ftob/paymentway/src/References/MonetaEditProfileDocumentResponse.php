<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:38
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ, который приходит на запрос EditProfileDocumentRequest.
 * В ответе нет никаких данных.
 * Если в процессе сохранения документа произошла ошибка, то возникнет Exception.
 * Если Exception не возник - значит документ гарантированно сохранен.
 * Document modification response.
 * Response contains no data unless some error has occured while modification.
 * If reponse has no Exception then document is modified successfully.
 *
 */
class MonetaEditProfileDocumentResponse
{

}