<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:09
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ, который содержит список счетов.
 * Accounts searching response containing the list of accounts.
 *
 */
class MonetaFindAccountsListResponse
{

    /**
     *
     *
     * @var MonetaAccountInfo
     */
    public $account = null;

    /**
     *
     *
     * @param MonetaAccountInfo
     *
     * @return void
     */
    public function addAccount(MonetaAccountInfo $item)
    {
        $this->account[] = $item;
    }

}