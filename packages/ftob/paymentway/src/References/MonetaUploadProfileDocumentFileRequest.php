<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:32
 */


namespace Ftob\Paymentway\References;


/**
 * Запрос на создание/редактирование файла документа
 * Request for file adding/modification.
 *
 */
class MonetaUploadProfileDocumentFileRequest
{

    /**
     * Если указано поле fileId - файл редактируется, иначе создается новый файл.
     * Поле approved в данном запросе не используется.
     * Если не указан mimeType, но в запросе есть имя файла (поле title), то mimeType будет вычисляться по расширению файла
     * Specify "fileId" to modify file, omit "fileId" to add new file.
     * Approved is not used.
     * If "mimeType" omitted and "title" is specified, "mimeType" will be resolved by filename extension.
     *
     *
     * @var MonetaFile
     */
    public $file = null;

}