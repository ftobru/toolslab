<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:35
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос FindProfileDocumentsRequest.
 * Profile's documents retrieval response.
 *
 */
class MonetaFindProfileDocumentsResponse
{

    /**
     * Список найденных документов. Если документов нет - список пустой.
     * Found documents list. Empty if no documents are found.
     *
     *
     * @var MonetaDocument
     */
    public $document = null;

    /**
     * Список найденных документов. Если документов нет - список пустой.
     * Found documents list. Empty if no documents are found.
     *
     *
     * @param MonetaDocument
     *
     * @return void
     */
    public function addDocument(MonetaDocument $item)
    {
        $this->document[] = $item;
    }

}