<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:18
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий параметры операции для выставления счета к оплате.
 * Transaction parameters type for making new invoice (request for payment).
 *
 */
class MonetaInvoiceRequestType
{

    /**
     * Номер счета плательщика
     * Payer account number
     *
     *
     * @var long
     */
    public $payer = null;

    /**
     * Номер счета получателя
     * Payee account number
     *
     *
     * @var long
     */
    public $payee = null;

    /**
     * Сумма
     * Amount
     *
     *
     * @var decimal
     */
    public $amount = null;

    /**
     * Внешний номер операции
     * external transaction identificator
     *
     *
     * @var string
     */
    public $clientTransaction = null;

    /**
     * Описание операции
     * Transaction description or comments
     *
     *
     * @var normalizedString
     */
    public $description = null;

    /**
     * Произвольный параметр
     * Custom parameter
     *
     *
     * @var string
     */
    public $mnt_custom1 = null;

    /**
     * Произвольный параметр
     * Custom parameter
     *
     *
     * @var string
     */
    public $mnt_custom2 = null;

    /**
     * Произвольный параметр
     * Custom parameter
     *
     *
     * @var string
     */
    public $mnt_custom3 = null;

    /**
     * Набор полей, которые необходимо сохранить в качестве атрибутов
     * операции. Значения дат в формате dd.MM.yyyy HH:mm:ss
     * Key-value pairs that will be saved as a transaction attributes. Use
     * format dd.MM.yyyy HH:mm:ss for date values
     *
     *
     * @var MonetaOperationInfo
     */
    public $operationInfo = null;

}