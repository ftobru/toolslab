<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:31
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ, содержащий бинарные данные, ассоциированные с указанным в запросе документом.
 * Response containing binary data associated with document given in request.
 *
 */
class MonetaFindProfileDocumentFilesResponse
{

    /**
     *
     *
     * @var MonetaFile
     */
    public $file = null;

    /**
     *
     *
     * @param MonetaFile
     *
     * @return void
     */
    public function addFile(MonetaFile $item)
    {
        $this->file[] = $item;
    }

}