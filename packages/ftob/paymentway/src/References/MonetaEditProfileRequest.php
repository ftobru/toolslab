<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:23
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на редактирование данных пользователя в системе МОНЕТА.РУ.
 * Profile information modification request.
 *
 */
class MonetaEditProfileRequest
{

    /**
     * ID пользователя в системе МОНЕТА.РУ, данные которого надо отредактировать.
     * Structure ID of profile for modification.
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * Данные редактируемого пользователя. Данные представлены в виде "ключ-значение".
     * Ключи данных зависят от типа редактируемого пользователя (client или organization)
     * User profile information in key-value pairs list.
     * Supported keys depend on ProfileType.
     *
     *
     * @var MonetaProfile
     */
    public $profile = null;

}