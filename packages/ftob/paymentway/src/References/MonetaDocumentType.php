<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:04
 */


namespace Ftob\Paymentway\References;


/**
 * Типы документов, поддерживаемые в системе МОНЕТА.РУ.
 * Document type supported in MONETA.RU.
 *
 */
class MonetaDocumentType
{

    /**
     * Паспорт
     * Civil passport
     *
     *
     * @var string
     */
    const PASSPORT = 'PASSPORT';

    /**
     * Водительское удостоверение
     * User's driving licence
     *
     *
     * @var string
     */
    const DRIVING_LICENCE = 'DRIVING_LICENCE';

    /**
     * Военный билет
     * Military ID
     *
     *
     * @var string
     */
    const MILITARY_ID = 'MILITARY_ID';

    /**
     * Другой документ
     * Other document
     *
     *
     * @var string
     */
    const OTHER = 'OTHER';

}