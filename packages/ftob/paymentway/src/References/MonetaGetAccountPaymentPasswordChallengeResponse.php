<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:10
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ содержит строку запроса для платежного пароля
 * Response contains of challenge string for payment password
 *
 */
class MonetaGetAccountPaymentPasswordChallengeResponse
{

    /**
     * Запрос для платежного пароля
     * Challenge of payment password
     *
     *
     * @var string
     */
    public $paymentPasswordChallenge = null;

}