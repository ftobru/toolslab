<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:20
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на получение списка последних операций.
 * Если данные не найдены, то size в ответе равен 0.
 * Last transactions searching.
 * Response element "size" is 0 if no data found.
 *
 */
class MonetaFindLastOperationsListRequest
{

    /**
     * ID пользователя в системе МОНЕТА.РУ.
     * Если это поле не задано, то используется текущий пользователь.
     * Необязательное поле.
     * Structure identificator in MONETA.RU.
     * If omitted authenticated user's structure is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * Количество операций. Необязательное поле.
     * Transactions quantity. Optional.
     *
     *
     * @var int
     */
    public $transactionsQuantity = null;

}