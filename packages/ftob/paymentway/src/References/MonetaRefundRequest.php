<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:04
 */


namespace Ftob\Paymentway\References;

/**
 * Возврат средств по указанной операции.
 * Refund given transaction.
 *
 */
class MonetaRefundRequest
{

    /**
     * Номер операции в системе МОНЕТА.РУ, по которой необходимо вернуть деньги
     * Transaction ID in Moneta.ru
     *
     *
     * @var long
     */
    public $transactionId = null;

    /**
     * Сумма, которую необходимо возвратить.
     * Если сумма не указана, то сумма для возврата берется из указанной операции.
     * Необязательное поле.
     * Amount to refund in source transaction payee's currency.
     * If omitted the source transaction payees' amount is used.
     * Optional.
     *
     *
     * @var decimal
     */
    public $amount = null;

    /**
     * Платежный пароль
     * Payment password for payer account
     *
     *
     * @var normalizedString
     */
    public $paymentPassword = null;

    /**
     * Внешний номер операции
     * External transaction ID
     *
     *
     * @var string
     */
    public $clientTransaction = null;

    /**
     * Описание операции
     * Transaction description or comments
     *
     *
     * @var normalizedString
     */
    public $description = null;

    /**
     * Набор полей, которые необходимо сохранить в качестве атрибутов операции. Значения дат в формате dd.MM.yyyy HH:mm:ss
     * Key-value pairs that will be saved as a transaction attributes. Use format dd.MM.yyyy HH:mm:ss for date values
     *
     *
     * @var MonetaOperationInfo
     */
    public $operationInfo = null;

    /**
     * Запрос для платежного пароля
     * Payment password challenge
     *
     *
     * @var string
     */
    public $paymentPasswordChallenge = null;

}
