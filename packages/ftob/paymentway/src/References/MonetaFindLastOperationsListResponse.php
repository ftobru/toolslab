<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:21
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос FindLastOperationsListRequest.
 * В результате возвращается список последних операций.
 * Transaction searching response.
 * Result contains last transactions list.
 *
 */
class MonetaFindLastOperationsListResponse extends MonetaOperationInfoList
{

}