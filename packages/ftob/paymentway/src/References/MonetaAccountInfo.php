<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:22
 */


namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий атрибуты счета в системе МОНЕТА.РУ.
 * Account information container type.
 *
 */
class MonetaAccountInfo
{

    /**
     * Номер счета в системе МОНЕТА.РУ
     * Account number
     *
     *
     * @var long
     */
    public $id = null;

    /**
     * Валюта данного счета
     * Currency of account
     *
     *
     * @var string
     */
    public $currency = null;

    /**
     * Баланс на данном счете
     * Current balance of account
     *
     *
     * @var decimal
     */
    public $balance = null;

    /**
     * Доступный баланс на данном счете
     * Available balance of account
     *
     *
     * @var decimal
     */
    public $availableBalance = null;

    /**
     * Тип счета в системе МОНЕТА.РУ
     * Type of account
     *
     *
     * @var int
     */
    public $type = null;

    /**
     * Статус счета в системе МОНЕТА.РУ
     * Status of account
     *
     *
     * @var int
     */
    public $status = null;

    /**
     * Название счета в системе МОНЕТА.РУ. Необязательное поле
     * Alias of account. Optional.
     *
     *
     * @var string
     */
    public $alias = null;

    /**
     * URL после списания средств. Необязательное поле.
     * URL on debiting. Optional.
     *
     *
     * @var string
     */
    public $onSuccessfulDebitUrl = null;

    /**
     * URL после зачисления средств. Необязательное поле.
     * URL on crediting. Optional.
     *
     *
     * @var string
     */
    public $onSuccessfulCreditUrl = null;

    /**
     * Код проверки целостности данных. Необязательное поле.
     * Payment form data integrity signature. Optional.
     *
     *
     * @var string
     */
    public $signature = null;

    /**
     * Если баланс счета меньше данного значения, то раз в сутки уходит уведомление об этом событии. Необязательное поле.
     * Daily notifications if balance is less than threshold. Optional.
     *
     *
     * @var decimal
     */
    public $lowBalanceThreshold = null;

    /**
     * Если баланс счета больше данного значения, то раз в сутки уходит уведомление об этом событии. Необязательное поле.
     * Daily notifications if balance is greater than threshold. Optional.
     *
     *
     * @var decimal
     */
    public $highBalanceThreshold = null;

    /**
     * Информация о доступе к счету. Информация отдается, если счет является делегированным. Необязательное поле.
     * Account access information. Available if the account is delegated, otherwise omitted. Optional.
     *
     *
     * @var MonetaAccountAccessInfo
     */
    public $accountAccess = null;

    /**
     * Счет-прототип с которого берутся свойства "по умолчанию". Необязательное поле.
     * Prototype account for default properties values. Optional.
     *
     *
     * @var long
     */
    public $prototypeAccountId = null;

    /**
     * URL после отмены списания средств. Необязательное поле.
     * URL on cancelled debit. Optional.
     *
     *
     * @var string
     */
    public $onCancelledDebitUrl = null;

    /**
     * URL после отмены зачисления средств. Необязательное поле.
     * URL on cancelled credit. Optional.
     *
     *
     * @var string
     */
    public $onCancelledCreditUrl = null;

}
