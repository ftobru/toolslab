<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:38
 */

namespace Ftob\Paymentway\References;

/**
 * Запрос на получение списка пользователей, которые имеют делегированный доступ к указанному счету.
 * Request to find delegated users for specified account ID.
 *
 */
class MonetaFindAccountRelationsRequest
{

    /**
     * Номер счета в системе МОНЕТА.РУ.
     * Account number.
     *
     *
     * @var long
     */
    public $accountId = null;

}