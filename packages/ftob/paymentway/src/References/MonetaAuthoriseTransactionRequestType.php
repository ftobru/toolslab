<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:11
 */


namespace Ftob\Paymentway\References;


/**
 * Тип для запроса на регистрацию операции с блокировкой средств на счете плательщика.
 * Transaction registration and debiting funds from payer's account request type.
 *
 */
class MonetaAuthoriseTransactionRequestType extends MonetaTransactionRequestType
{

    /**
     * Номер операции. Необязательное поле.
     * Transaction ID. Optional.
     *
     *
     * @var long
     */
    public $transactionId = null;

}