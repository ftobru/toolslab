<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:30
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос перевода денежных средств.
 * Money transfer registration response.
 *
 */
class MonetaTransferResponse extends MonetaTransactionResponseType
{

}