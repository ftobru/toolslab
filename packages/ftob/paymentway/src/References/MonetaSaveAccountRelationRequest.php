<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:45
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на сохранение делегированного доступа к счету.
 * Request for saving detailed information of delegated access.
 *
 */
class MonetaSaveAccountRelationRequest extends MonetaAccountRelation
{

    /**
     * Платежный пароль.
     * Payment password for account.
     *
     *
     * @var normalizedString
     */
    public $paymentPassword = null;

    /**
     * Запрос для платежного пароля
     * Payment password challenge
     *
     *
     * @var string
     */
    public $paymentPasswordChallenge = null;

}