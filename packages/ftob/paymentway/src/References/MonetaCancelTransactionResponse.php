<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:01
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос CancelTransactionRequest.
 * Transaction cancelation response.
 *
 */
class MonetaCancelTransactionResponse extends MonetaCancelTransactionResponseType
{

}