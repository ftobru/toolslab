<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:05
 */


namespace Ftob\Paymentway\References;

/**
 * Запрос предварительного расчета сумм и комиссий по параметрам операции.
 * Request for amount and fee preliminary calculation by transaction parameters.
 *
 */
class MonetaForecastTransactionRequest extends MonetaTransactionRequestType
{

}