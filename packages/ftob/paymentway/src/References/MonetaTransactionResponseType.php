<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:08
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий атрибуты операции в ответах.
 * Transaction attributes type for responses.
 *
 */
class MonetaTransactionResponseType
{

    /**
     * Текущий статус операции
     * Current transaction status
     *
     *
     * @var string
     */
    public $status = null;

    /**
     * Время последней модификации операции
     * Transaction modification timestamp
     *
     *
     * @var dateTime
     */
    public $dateTime = null;

    /**
     * Номер операции
     * Transaction ID in MONETA.RU
     *
     *
     * @var long
     */
    public $transaction = null;

    /**
     * Внешний номер операции
     * External transaction ID
     *
     *
     * @var string
     */
    public $clientTransaction = null;

}