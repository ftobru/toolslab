<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:05
 */

namespace Ftob\Paymentway\References;

/**
 * Базовый тип, содержащий атрибут "version".
 * Base type containing "version" attribute.
 *
 */
class MonetaEntity
{

    /**
     *
     *
     * @var string
     */
    public $version = null;

}
