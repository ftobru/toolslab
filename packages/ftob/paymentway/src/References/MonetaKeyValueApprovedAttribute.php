<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:24
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, расширяющий KeyValueAttribute.
 * Поле approved показывает проверено или нет данное свойство. Поле approved - не обязательное.
 * Поле published показывает доступно или нет чтение свойства для всех пользователей. Поле published - не обязательное.
 * Extended KeyValueAttribute Type.
 * Approved element shows if attribute value is approved or not. Approved element is optional.
 * Published element shows if attribute value is available for reading for all users. Published element is optional.
 *
 */
class MonetaKeyValueApprovedAttribute extends MonetaKeyValueAttribute
{

    /**
     *
     *
     * @var boolean
     */
    public $approved = null;

    /**
     * Для чтения этого атрибута в запросе необходимо выставлять атрибут version равный или больше VERSION_2.
     * Для записи version можно не указывать.
     * The value "VERSION_2" of request's attribute "version" is required for reading of this attribute.
     * The request's attribute "version" for writing is optional.
     *
     *
     * @var boolean
     */
    public $published = null;

}