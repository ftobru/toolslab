<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:15
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий атрибуты при ответе на запрос отмены операции.
 * Transaction cancelation response type.
 *
 */
class MonetaCancelTransactionResponseType
{

    /**
     * Номер операции
     * Transaction ID
     *
     *
     * @var long
     */
    public $transactionId = null;

    /**
     * Статус операции
     * Transaction status
     *
     *
     * @var string
     */
    public $operationStatus = null;

}