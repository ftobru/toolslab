<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:36
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос создания документа в профайле.
 * Profile's document registration response.
 * $
 *
 */
class MonetaCreateProfileDocumentResponse
{

    /**
     * Идентификатор документа в системе МОНЕТА.РУ
     * Document identificator in MONETA.RU
     *
     *
     * @var long
     */
    public $id = null;

}