<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:24
 */


namespace Ftob\Paymentway\References;



/**
 * Тип, представляющий список операций.
 * Содержит разбиение по страницам для отображения длинных списков.
 * Transaction list type.
 * Contains paged structure for long list presentation.
 *
 */
class MonetaOperationInfoList
{

    /**
     * Количество операций, возвращаемых в результате запроса.
     * Transaction per page within one request.
     *
     *
     * @var long
     */
    public $pageSize = null;

    /**
     * Номер текущей страницы. Нумерация начинается с 1
     * Current page number starting with 1.
     *
     *
     * @var long
     */
    public $pageNumber = null;

    /**
     * Максимальное количество страниц с операциями по данному запросу
     * Total page count for given request.
     *
     *
     * @var long
     */
    public $pagesCount = null;

    /**
     * Количество операций на текущей странице.
     * Меньше или равно pageSize.
     * Последняя страница может содержать операций меньше, чем pageSize.
     * Transactions count in current page.
     * Less or equal to pageSize.
     * Last page may contain less than pageSize transactions.
     *
     *
     * @var long
     */
    public $size = null;

    /**
     * Общее количество операций, которое можно получить в данной
     * выборке
     * Total transactions count for given request.
     *
     *
     * @var long
     */
    public $totalSize = null;

    /**
     * Список операций
     * Transaction list
     *
     *
     * @var MonetaOperationInfo
     */
    public $operation = null;

    /**
     * Список операций
     * Transaction list
     *
     *
     * @param MonetaOperationInfo
     *
     * @return void
     */
    public function addOperation(MonetaOperationInfo $item)
    {
        $this->operation[] = $item;
    }

}