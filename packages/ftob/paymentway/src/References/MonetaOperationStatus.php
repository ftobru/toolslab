<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 18:56
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий статусы операций в системе МОНЕТА.РУ.
 * Данный тип может иметь только определенные значения, описанные ниже.
 * Transaction status type in MONETA.RU.
 * This type may have enumerated values.
 *
 */
class MonetaOperationStatus
{

    /**
     * В обработке
     * Transaction processing is in progress
     *
     *
     * @var string
     */
    const INPROGRESS = 'INPROGRESS';

    /**
     * Операция обработана
     * Transaction processing completed successfully
     *
     *
     * @var string
     */
    const SUCCEED = 'SUCCEED';

    /**
     * Операция отменена
     * Transaction processing is canceled
     *
     *
     * @var string
     */
    const CANCELED = 'CANCELED';

    /**
     * Уведомление по операции не отправлено (или не принято)
     * External notification is not sent (or not accepted)
     *
     *
     * @var string
     */
    const TAKENIN_NOTSENT = 'TAKENIN_NOTSENT';

    /**
     * Операция создана
     * Transaction is registered
     *
     *
     * @var string
     */
    const CREATED = 'CREATED';

}