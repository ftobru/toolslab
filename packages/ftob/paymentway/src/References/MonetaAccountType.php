<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:02
 */

namespace Ftob\Paymentway;

/**
 * Типы счетов в системе МОНЕТА.РУ.
 * Account types in MONETA.RU.
 *
 */
class MonetaAccountType
{

    /**
     * Обычный счет
     * Standard account
     *
     *
     * @var int
     */
    const AccountType1 = '1';

    /**
     * Расширенный счет
     * Advanced account
     *
     *
     * @var int
     */
    const AccountType2 = '2';

    /**
     * Бонус-счет
     * Bonus account
     *
     *
     * @var int
     */
    const AccountType3 = '3';

}