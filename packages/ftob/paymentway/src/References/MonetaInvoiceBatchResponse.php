<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:28
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ на выставление счета к оплате в пакетном режиме.
 * Invoice registration response in batch processing mode.
 *
 */
class MonetaInvoiceBatchResponse
{

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в InvoiceBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @var MonetaTransactionBatchResponseType
     */
    public $transaction = null;

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в InvoiceBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @param MonetaTransactionBatchResponseType
     *
     * @return void
     */
    public function addTransaction(MonetaTransactionBatchResponseType $item)
    {
        $this->transaction[] = $item;
    }

}