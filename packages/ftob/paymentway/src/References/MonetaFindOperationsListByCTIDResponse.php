<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:13
 */

namespace Ftob\Paymentway\References;
/**
 * Ответ на запрос FindOperationsListByCTIDRequest.
 * В результате возвращается список операций, разбитый по страницам.
 * Transaction searching by external transaction ID response.
 * Result contains paged transaction list.
 *
 */
class MonetaFindOperationsListByCTIDResponse extends MonetaOperationInfoList
{

}