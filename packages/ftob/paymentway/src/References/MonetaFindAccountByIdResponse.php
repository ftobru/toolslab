<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:08
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ, который приходит на запрос FindAccountByIdRequest.
 * В ответе содержится информация по счету.
 * Account searching by account number response.
 * Response contains Account information.
 *
 */
class MonetaFindAccountByIdResponse
{

    /**
     *
     *
     * @var MonetaAccountInfo
     */
    public $account = null;

}
