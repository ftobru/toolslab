<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 18:57
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий валюты, используемые в системе МОНЕТА.РУ.
 * Данный тип может иметь только строго определенные значения, описанные ниже.
 * Currency type in MONETA.RU.
 * Must be one of values listed below.
 *
 */
class MonetaCurrency
{

    /**
     *
     *
     * @var string
     */
    const RUB = 'RUB';

    /**
     *
     *
     * @var string
     */
    const USD = 'USD';

    /**
     *
     *
     * @var string
     */
    const EUR = 'EUR';

    /**
     *
     *
     * @var string
     */
    const GBP = 'GBP';

}