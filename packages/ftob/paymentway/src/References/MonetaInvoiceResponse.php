<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:29
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на выставление счета к оплате.
 * Invoice registration response.
 *
 */
class MonetaInvoiceResponse extends MonetaTransactionResponseType
{

}