<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:56
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос регистрации операции.
 * Transaction authorisation response.
 *
 */
class MonetaAuthoriseTransactionResponse extends MonetaOperationInfo
{
}