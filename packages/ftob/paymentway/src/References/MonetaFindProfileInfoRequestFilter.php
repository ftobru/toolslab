<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:24
 */

namespace Ftob\Paymentway\References;


/**
 * Фильтр, по которому ищем данные
 * Searching filter
 *
 */
class MonetaFindProfileInfoRequestFilter
{

    /**
     * ID пользователя в системе МОНЕТА.РУ. Необязательное поле. ID пользователя "от которого"
     * начинается поиск. Если это поле не задано, то ищем пользователей, начиная
     * от идентифицированного пользователя.
     * Structure ID of root where searching starts from.
     * If ommited then authenticated user's structure ID is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * Имя, по которому происходит поиск. Необязательное поле. Если поле задано,
     * то оно должно быть не меньше 2-х символов.
     * Поиск происходит по прямому совпадению. Для задания маски
     * можно указать спец-символы - "*" или "?".
     * Для пользователей с типом "organization" поиск происходит по
     * полю "organization_name". Для пользователей с типом "client" -
     * по общему полю "last_name first_name middle_initial_name".
     * То есть, если нужно найти пользователя "Иванов Андрей Сергеевич",
     * то следует ввести имя для поиска именно так. Если нужно найти всех
     * пользователей с фамилией "Иванов", то запрос должен быть "Иванов*".
     * Если нужно найти всех пользователей с именем "Андрей", то запрос
     * должен быть "*Андрей*".
     * Name to search for. Optional.
     * If specified must be not shorter than 2 symbols.
     * Wildcards "*" and "?" may be used.
     * For "organization" profiles "organization_name" is matched.
     * For "client" profiles "last_name first_name middle_initial_name" is matched.
     *
     *
     * @var string
     */
    public $name = null;

    /**
     * Email, по которому происходит поиск. Необязательное поле. Если поле задано,
     * то оно должно быть не меньше 2-х символов.
     * Поиск происходит по прямому совпадению. Для задания маски
     * можно указать спец-символы - "*" или "?".
     * Email to search for. Optional.
     * If specified must be not shorted than 2 symbols.
     * Wildcards "*" and "?" may be used.
     *
     *
     * @var string
     */
    public $email = null;

    /**
     * Возвращаются пользователи у которых есть счета с балансом
     * большим или равным указанному значению.
     * Необязательное поле.
     * Only profiles with greater or equal account's balance are returned.
     * Optional.
     *
     *
     * @var decimal
     */
    public $accountBalanceFrom = null;

    /**
     * Возвращаются пользователи у которых есть счета с балансом
     * меньшим или равным указанному значению.
     * Необязательное поле.
     * Only profiles with less or equal account's balance are returned.
     * Optional.
     *
     *
     * @var decimal
     */
    public $accountBalanceTo = null;

    /**
     * При указании границ баланса в поиске можно уточнить валюту счета.
     * Необязательное поле.
     * If balance limits are specified only accounts with given currency will be analysed.
     * Optional.
     *
     *
     * @var string
     */
    public $accountCurrencyCode = null;

}