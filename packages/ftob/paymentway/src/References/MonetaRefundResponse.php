<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:04
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ по запросу возврата средств, содержащий информацию по возвратной операции.
 * Response containing refund transaction information.
 *
 */
class MonetaRefundResponse extends MonetaOperationInfo
{

}