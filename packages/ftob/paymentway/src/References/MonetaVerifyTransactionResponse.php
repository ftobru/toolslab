<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:06
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос проверки проведения операции в системе МОНЕТА.РУ
 * Transaction verification response.
 *
 */
class MonetaVerifyTransactionResponse extends MonetaVerifyTransactionResponseType
{

}