<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:19
 */


namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий суммы и комиссии в предварительном расчете операции.
 * Type describes amount and fees for preliminary calculation of transaction.
 *
 */
class MonetaForecastTransactionResponseType
{

    /**
     * Номер счета плательщика
     * Payer account number
     *
     *
     * @var long
     */
    public $payer = null;

    /**
     * Валюта счета плательщика
     * Currency of payer account
     *
     *
     * @var string
     */
    public $payerCurrency = null;

    /**
     * Сумма к списанию
     * Debit amount
     *
     *
     * @var decimal
     */
    public $payerAmount = null;

    /**
     * Комиссия списания средств
     * Debit fee
     *
     *
     * @var decimal
     */
    public $payerFee = null;

    /**
     * Номер счета получателя
     * Payee account number
     *
     *
     * @var long
     */
    public $payee = null;

    /**
     * Валюта счета получателя
     * Currency of payee account
     *
     *
     * @var string
     */
    public $payeeCurrency = null;

    /**
     * Сумма к зачислению
     * Credit amount
     *
     *
     * @var decimal
     */
    public $payeeAmount = null;

    /**
     * Комиссия зачисления средств
     * Credit fee
     *
     *
     * @var decimal
     */
    public $payeeFee = null;

    /**
     * Название счета плательщика
     * Payer account alias
     *
     *
     * @var string
     */
    public $payerAlias = null;

    /**
     * Название счета получателя
     * Payee account alias
     *
     *
     * @var string
     */
    public $payeeAlias = null;

}