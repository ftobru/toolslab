<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:09
 */


namespace Ftob\Paymentway\References;

/**
 * Запрос на получение списка счетов по заданным условиям поиска.
 * Accounts searching by given filter.
 *
 */
class MonetaFindAccountsListRequest
{

    /**
     * Пользователь, которому принадлежат счета.
     * Если это поле не задано, то используется текущий пользователь.
     * Необязательное поле.
     * Structure element, where accounts belong to.
     * If omitted authenticated user's structure is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * Название счета. Поиск происходит по прямому совпадению.
     * Для задания маски можно указать спец-символы "*" или "?".
     * Необязательное поле.
     * Alias of account. Wildcards "*" and "?" may be used.
     * Optional.
     *
     *
     * @var string
     */
    public $alias = null;

    /**
     * Валюта счета. Необязательное поле.
     * Currency of account. Optional.
     *
     *
     * @var string
     */
    public $currency = null;

    /**
     * Является ли счет делегированным. Необязательное поле.
     * - поле не указано - выбрать все счета
     * - false - выбрать только неделегированные счета
     * - true - выбрать только делегированные счета
     * The boolean flag indicating if account is deligated. Optional.
     * - select all accounts if omitted
     * - false - select only non-delegated (owned) accounts
     * - true - select only delegated accounts
     *
     *
     * @var boolean
     */
    public $isDelegatedAccount = null;

}