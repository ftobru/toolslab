<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:57
 */


namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос регистрации операций в пакетном режиме.
 * Transactions authorisation response in batch processing mode.
 *
 */
class MonetaAuthoriseTransactionBatchResponse
{

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в AuthoriseTransactionBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @var MonetaOperationInfoBatchResponseType
     */
    public $transaction = null;

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в AuthoriseTransactionBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @param MonetaOperationInfoBatchResponseType
     *
     * @return void
     */
    public function addTransaction(MonetaOperationInfoBatchResponseType $item)
    {
        $this->transaction[] = $item;
    }

}