<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:43
 */
namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос GetAccountRelationRequest.
 * Response containing detailed information for delegated access.
 *
 */
class MonetaGetAccountRelationResponse
{

    /**
     *
     *
     * @var MonetaAccountRelation
     */
    public $accountRelation = null;

}