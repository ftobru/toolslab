<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:07
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос проверки проведения операции в системе МОНЕТА.РУ
 * Transaction verification response.
 *
 */
class MonetaVerifyTransferResponse extends MonetaVerifyTransferResponseType
{

}