<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:37
 */


namespace Ftob\Paymentway\References;

/**
 * Запрос на редактирование документа пользователя.
 * Profile's document modification request.
 *
 */
class MonetaEditProfileDocumentRequest extends MonetaDocument
{

    /**
     * Пользователь, которому принадлежит данный документ.
     * Если это поле не задано, то документ ищется для текущего пользователя.
     * Необязательное поле.
     * Structure element, where the document belongs to.
     * If omitted authenticated user's structure is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

}