<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:19
 */

namespace Ftob\Paymentway\References;

/**
 * Запрос на получение списка операций по заданному фильтру.
 * Обязательными в фильтре являются только поля с датами периода.
 * Все остальные поля в фильтре необязательные.
 * Если данные не найдены, то size в ответе равен 0.
 * Transaction searching by given filter.
 * Filter requires date period values to be set.
 * Other parameters are optional.
 * Response element "size" is 0 if no data found.
 *
 */
class MonetaFindOperationsListRequest
{

    /**
     * Настройки страницы данных. Необязательное поле.
     * Paging settings. Optional.
     *
     *
     * @var MonetaPager
     */
    public $pager = null;

    /**
     * Фильтр, по которому ищем операции.
     * Transactions filter.
     *
     *
     * @var MonetaFindOperationsListRequestFilter
     */
    public $filter = null;

}