<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:26
 */

namespace Ftob\Paymentway\References;


/**
 * Тип, описывающий документ в системе МОНЕТА.РУ
 * Document information type
 *
 */
class MonetaDocument
{

    /**
     * Идентификатор документа в системе МОНЕТА.РУ
     * Document identificator in MONETA.RU
     *
     *
     * @var long
     */
    public $id = null;

    /**
     * Тип документа
     * Document type
     *
     *
     * @var string
     */
    public $type = null;

    /**
     * Поля документа в системе МОНЕТА.РУ.
     * Данные представляются в виде "ключ-значение" и признака подтвержденности.
     * В зависимости от типа документа возвращаются следующие поля:
     * Для документов типа PASSPORT, MILITARY_ID:
     * SERIES - серия документа
     * NUMBER - номер документа
     * ISSUER - кем выдан документ
     * ISSUED - когда выдан документ
     * COMMENTS - комментарии (необязательное поле)
     * Для документов типа DRIVING_LICENCE:
     * SERIES - серия документа
     * NUMBER - номер документа
     * ISSUER - кем выдан документ
     * ISSUED - когда выдан документ
     * EXPIRATIONDATE - срок действия
     * COMMENTS - комментарии (необязательное поле)
     * Для OTHER:
     * COMMENTS - комментарии, пояснения, описание
     * Для всех типов документов:
     * customfield:* - произвольный набор значений.
     * В документе их может быть несколько.
     * Полный ключ атрибута состоит из префикса ("customfield:") и тэга (32 символа).
     * Например, "customfield:name".
     * MODIFICATIONDATE - последняя дата редактирования документа
     * Document attributes in MONETA.RU.
     * Information is represented by key-value pairs list.
     * Supported keys depending on document type as follows.
     * PASSPORT, MILITARY_ID document type:
     * SERIES - document series
     * NUMBER - document number
     * ISSUER - document issuer
     * ISSUED - document issuance date
     * COMMENTS - optional description
     * DRIVING_LICENCE document type:
     * SERIES - document series
     * NUMBER - document number
     * ISSUER - document issuer
     * ISSUED - document issuance date
     * EXPIRATIONDATE - expiration date
     * COMMENTS - optional description
     * OTHER document type:
     * COMMENTS - name, description, comments or explanation
     * all document types:
     * customfield:* - custom list of values.
     * The document may contain several attributes with different keys.
     * The full attribute's key consists of prefix ("customfield:") and tag (32 characters).
     * For example, "customfield:name".
     * MODIFICATIONDATE - document last modification date
     *
     *
     * @var MonetaKeyValueAttribute
     */
    public $attribute = null;

    /**
     * Поля документа в системе МОНЕТА.РУ.
     * Данные представляются в виде "ключ-значение" и признака подтвержденности.
     * В зависимости от типа документа возвращаются следующие поля:
     * Для документов типа PASSPORT, MILITARY_ID:
     * SERIES - серия документа
     * NUMBER - номер документа
     * ISSUER - кем выдан документ
     * ISSUED - когда выдан документ
     * COMMENTS - комментарии (необязательное поле)
     * Для документов типа DRIVING_LICENCE:
     * SERIES - серия документа
     * NUMBER - номер документа
     * ISSUER - кем выдан документ
     * ISSUED - когда выдан документ
     * EXPIRATIONDATE - срок действия
     * COMMENTS - комментарии (необязательное поле)
     * Для OTHER:
     * COMMENTS - комментарии, пояснения, описание
     * Для всех типов документов:
     * customfield:* - произвольный набор значений.
     * В документе их может быть несколько.
     * Полный ключ атрибута состоит из префикса ("customfield:") и тэга (32 символа).
     * Например, "customfield:name".
     * MODIFICATIONDATE - последняя дата редактирования документа
     * Document attributes in MONETA.RU.
     * Information is represented by key-value pairs list.
     * Supported keys depending on document type as follows.
     * PASSPORT, MILITARY_ID document type:
     * SERIES - document series
     * NUMBER - document number
     * ISSUER - document issuer
     * ISSUED - document issuance date
     * COMMENTS - optional description
     * DRIVING_LICENCE document type:
     * SERIES - document series
     * NUMBER - document number
     * ISSUER - document issuer
     * ISSUED - document issuance date
     * EXPIRATIONDATE - expiration date
     * COMMENTS - optional description
     * OTHER document type:
     * COMMENTS - name, description, comments or explanation
     * all document types:
     * customfield:* - custom list of values.
     * The document may contain several attributes with different keys.
     * The full attribute's key consists of prefix ("customfield:") and tag (32 characters).
     * For example, "customfield:name".
     * MODIFICATIONDATE - document last modification date
     *
     *
     * @param MonetaKeyValueAttribute
     *
     * @return void
     */
    public function addAttribute(MonetaKeyValueAttribute $item)
    {
        $this->attribute[] = $item;
    }

    /**
     * Имеет ли документ прикрепленные файлы.
     * Для получения прикрепленных файлов используйте вызов FindProfileDocumentFilesRequest
     * If the document has attachments or not.
     * For attachment fetching see FindProfileDocumentFilesRequest
     *
     *
     * @var boolean
     */
    public $hasAttachedFiles = null;

}