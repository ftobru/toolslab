<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:54
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос перевода денежных средств.
 * Money transfer registration response.
 *
 */
class MonetaPaymentResponse extends MonetaOperationInfo
{

}