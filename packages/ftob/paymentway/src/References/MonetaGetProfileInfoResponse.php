<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:24
 */

namespace Ftob\Paymentway\References;


/**
 * Ответ на запрос GetProfileInfoRequest.
 * Profile information by structure ID searching response.
 *
 */
class MonetaGetProfileInfoResponse extends MonetaProfile
{

}