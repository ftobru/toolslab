<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:11
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на создание счета.
 * Счет создается для указанного пользователя или,
 * если это поле не указано, - для текущего пользователя.
 * Account creation request.
 * Account is created for given structure element (unitId)
 * or for authenticated user's structure if unitId is omitted.
 *
 */
class MonetaCreateAccountRequest
{

    /**
     * Валюта счета
     * Currency of account
     *
     *
     * @var string
     */
    public $currency = null;

    /**
     * Название счета.
     * Название счета должно быть уникальным среди счетов одного пользователя.
     * Необязательное поле.
     * Alias of account.
     * Must be unique among accounts of given user.
     * Optional.
     *
     *
     * @var string
     */
    public $alias = null;

    /**
     * Платежный пароль. Минимальная длина - 5 символов
     * Payment password. Minimum length is 5 symbols.
     *
     *
     * @var string
     */
    public $paymentPassword = null;

    /**
     * Пользователь, которому будет принадлежать данный счет.
     * Если это поле не задано, то счет создается для текущего пользователя.
     * Необязательное поле.
     * Structure element, where the account belongs to.
     * If omitted authenticated user's structure is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * URL после списания средств. Необязательное поле.
     * URL on debiting. Optional.
     *
     *
     * @var string
     */
    public $onSuccessfulDebitUrl = null;

    /**
     * URL после зачисления средств. Необязательное поле.
     * URL on crediting. Optional.
     *
     *
     * @var string
     */
    public $onSuccessfulCreditUrl = null;

    /**
     * Код проверки целостности данных. Необязательное поле.
     * Mandatory payment form signature. Optional.
     *
     *
     * @var string
     */
    public $signature = null;

    /**
     * Если баланс счета меньше данного значения, то раз в сутки уходит уведомление об этом событии. Необязательное поле.
     * Daily notifications if balance is less than threshold. Optional.
     *
     *
     * @var decimal
     */
    public $lowBalanceThreshold = null;

    /**
     * Если баланс счета больше данного значения, то раз в сутки уходит уведомление об этом событии. Необязательное поле.
     * Daily notifications if balance is greater than threshold. Optional.
     *
     *
     * @var decimal
     */
    public $highBalanceThreshold = null;

    /**
     * Счет-прототип с которого берутся свойства "по умолчанию". Необязательное поле.
     * Prototype account for default properties values. Optional.
     *
     *
     * @var long
     */
    public $prototypeAccountId = null;

    /**
     * URL после отмены списания средств. Необязательное поле.
     * URL on cancelled debit. Optional.
     *
     *
     * @var string
     */
    public $onCancelledDebitUrl = null;

    /**
     * URL после отмены зачисления средств. Необязательное поле.
     * URL on cancelled credit. Optional.
     *
     *
     * @var string
     */
    public $onCancelledCreditUrl = null;

}