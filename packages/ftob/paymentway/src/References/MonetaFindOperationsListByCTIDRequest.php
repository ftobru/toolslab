<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:13
 */


namespace Ftob\Paymentway\References;

/**
 * Запрос на получение данных по внешнему номеру операции
 * (номеру не в системе МОНЕТА.РУ).
 * По внешнему номеру операции может быть найдено несколько операций
 * в системе МОНЕТА.РУ. Поэтому операции возвращаются списком,
 * разбитым на страницы. Размером страницы можно управлять через
 * поле pager.
 * Если данные не найдены, то size в ответе равен 0.
 * Transaction searching by external transaction ID.
 * As a result more than one transaction may be found.
 * Transaction list is paged.
 * Response element "size" is 0 if no data found.
 *
 */
class MonetaFindOperationsListByCTIDRequest
{

    /**
     * Настройки страницы данных.
     * Необязательное поле.
     * Long list retrieval settings.
     * Optional.
     *
     *
     * @var MonetaPager
     */
    public $pager = null;

    /**
     * Номер счета в системе МОНЕТА.РУ
     * Account number in MONETA.RU
     *
     *
     * @var long
     */
    public $accountId = null;

    /**
     * Внешний номер операции
     * External transaction ID
     *
     *
     * @var string
     */
    public $clientTransaction = null;

}