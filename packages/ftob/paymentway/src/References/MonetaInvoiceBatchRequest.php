<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:28
 */


namespace Ftob\Paymentway\References;


/**
 * Выставление счета к оплате в пакетном режиме.
 * Making new invoice (request for payment) in batch processing mode.
 *
 */
class MonetaInvoiceBatchRequest extends MonetaInvoiceBatchRequestType
{

}