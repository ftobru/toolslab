<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:53
 */


namespace Ftob\Paymentway\References;
use MonetaTransactionBatchResponseType;


/**
 * Ответ на запрос перевода денежных средств в пакетном режиме.
 * Money transfer registration response in batch processing mode.
 *
 */
class MonetaTransferBatchResponse
{

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в TransferBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @var MonetaTransactionBatchResponseType
     */
    public $transaction = null;

    /**
     * Детали проведенных операций, либо описание ошибок, если операция не проведена.
     * Порядок соответствует набору операций, переданных в TransferBatchRequest.
     * Either transaction details or error description in order of appearance in corresponding request.
     *
     *
     * @param MonetaTransactionBatchResponseType
     *
     * @return void
     */
    public function addTransaction(MonetaTransactionBatchResponseType $item)
    {
        $this->transaction[] = $item;
    }

}