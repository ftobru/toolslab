<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 19:57
 */


namespace Ftob\Paymentway\References;


/**
 * Запрос на регистрацию операции с блокировкой средств на счете плательщика в пакетном режиме.
 * Request for transactions registration and debiting funds from payer's account in batch processing mode.
 *
 */
class MonetaAuthoriseTransactionBatchRequest extends MonetaAuthoriseTransactionBatchRequestType
{

}