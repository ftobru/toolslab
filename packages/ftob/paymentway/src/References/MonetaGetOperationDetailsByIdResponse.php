<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:12
 */


namespace Ftob\Paymentway\References;

/**
 * Ответ на запрос получения информации по операции.
 * Transaction information response.
 *
 */
class MonetaGetOperationDetailsByIdResponse
{

    /**
     *
     *
     * @var MonetaOperationInfo
     */
    public $operation = null;

}