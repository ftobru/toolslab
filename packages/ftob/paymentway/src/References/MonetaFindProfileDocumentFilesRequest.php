<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:28
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на получение бинарных данных, ассоциированных с указанным документом.
 * Request for binary data associated with given document.
 *
 */
class MonetaFindProfileDocumentFilesRequest
{

    /**
     *
     *
     * @var long
     */
    public $documentId = null;

}