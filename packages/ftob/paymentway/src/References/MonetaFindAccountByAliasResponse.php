<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:09
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ, который приходит на запрос FindAccountByAliasRequest.
 * В ответе содержится информация по счету.
 * Account searching by account alias response.
 * Response contains Account information.
 *
 */
class MonetaFindAccountByAliasResponse
{

    /**
     *
     *
     * @var MonetaAccountInfo
     */
    public $account = null;

}