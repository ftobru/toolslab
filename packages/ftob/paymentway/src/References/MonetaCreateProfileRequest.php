<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:22
 */

namespace Ftob\Paymentway\References;


/**
 * Запрос на создание пользователя в системе МОНЕТА.РУ.
 * New user registration request.
 *
 */
class MonetaCreateProfileRequest
{

    /**
     * ID родительского пользователя в системе МОНЕТА.РУ, к которому будет принадлежать
     * создаваемый пользователь. Необязательное поле. Если поле не задано,
     * то пользователь будет принадлежать тому пользователю, от имени
     * которого работает web service.
     * Structure ID in MONETA.RU.
     * If ommited then authenticated user's structure ID is used.
     * Optional.
     *
     *
     * @var long
     */
    public $unitId = null;

    /**
     * Тип пользователя - "client" или "organization"
     * Profile type. See also ProfileType description.
     *
     *
     * @var string
     */
    public $profileType = null;

    /**
     * Данные создаваемого пользователя. Данные представлены в виде "ключ-значение".
     * Ключи данных зависят от типа пользователя (ProfileType)
     * User profile information in key-value pairs list.
     * Supported keys depend on ProfileType.
     *
     *
     * @var MonetaProfile
     */
    public $profile = null;

}