<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:50
 */

namespace Ftob\Paymentway\References;

/**
 * Ответ, который приходит на запрос DeleteAccountRelationRequest.
 * В ответе нет никаких данных.
 * Если в процессе сохранения счета произошла ошибка, то возникнет Exception.
 * Если Exception не возник - значит делегированный доступ гарантированно удален.
 * Delete account relation response.
 * Response contains no data unless some error has occured while modification.
 * If reponse has no Exception then delegated access is deleted successfully.
 *
 */
class MonetaDeleteAccountRelationResponse
{

}