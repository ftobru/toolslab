<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 06.07.15
 * Time: 20:35
 */

namespace Ftob\Paymentway\References;

/**
 * Запрос на получение документов пользователя.
 * Profile's documents retrieval request.
 *
 */
class MonetaFindProfileDocumentsRequest
{

    /**
     * ID пользователя, документы которого запрашиваются.
     * Structure ID containing documents.
     *
     *
     * @var long
     */
    public $unitId = null;

}