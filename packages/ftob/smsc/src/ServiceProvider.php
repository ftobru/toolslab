<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 02.07.15
 * Time: 9:07
 */

namespace Ftob\Smsc;

use Illuminate\Support\ServiceProvider as BaseProvider;

class ServiceProvider extends BaseProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SmscSender', 'Ftob\Smsc\Sender');
    }

}