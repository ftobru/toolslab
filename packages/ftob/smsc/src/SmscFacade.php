<?php
/**
 * Created by PhpStorm.
 * User: office
 * Date: 02.07.15
 * Time: 9:10
 */

namespace Ftob\Smsc;

use Illuminate\Support\Facades\Facade;

class SmscFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'SmscSender';
    }

}