<?php namespace Ftob\Socket;

use Config;
use Illuminate\Support\ServiceProvider;

/**
 * Class WebSocketClientServiceProvider
 * @package Ftob\Socket
 */
class WebSocketClientServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        $this->app['web_socket'] = $this->app->share(function ($app)
        {
            return new WebSocketClient(Config::get('websocket.server.uri'), Config::get('websocket.port'));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('web_socket');
    }

    public function boot()
    {
    }

}
