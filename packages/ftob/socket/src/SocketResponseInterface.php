<?php namespace Ftob\Socket;

interface SocketResponseInterface{

	public function make($msg);

}