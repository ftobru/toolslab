<?php namespace Ftob\Socket;


use Illuminate\Support\Facades\Facade;

class SocketFacade extends Facade {

	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() { return 'stc_socket'; }

}