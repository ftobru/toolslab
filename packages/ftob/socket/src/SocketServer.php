<?php namespace Ftob\Socket;


use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\Http\HttpServer;

class SocketServer{
	protected $server;

	public function start($port){
		$this->server = IoServer::factory(
			new HttpServer(
				new WsServer(
					new SocketEventListener(
						new SocketResponse(new LaravelEventPublisher())
					)
				)
			)
			, $port
		);
	}

	public function run(){
		$this->server->run();
	}

}