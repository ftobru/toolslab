<?php namespace Ftob\Socket;

/**
 * Interface EventPublisherInterface
 * @package Ftob\Socket
 */
interface EventPublisherInterface
{
    public function fire($event, $data = array(), $stop_if_data_returned = true);
}