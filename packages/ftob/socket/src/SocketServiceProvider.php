<?php namespace Ftob\Socket;

use Illuminate\Support\ServiceProvider;

class SocketServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		$this->app['stc_socket'] = $this->app->share(function($app){
			return new SocketAppResponse();
		});

		$this->app['command.brainsocket.start'] = $this->app->share(function($app)
		{
			return new Socket();
		});
		$this->commands('command.brainsocket.start');
	}


	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('brain_socket','command.brainsocket.start');
	}

}