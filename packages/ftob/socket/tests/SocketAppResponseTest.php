<?php
use Ftob\Socket\SocketAppResponse;


/**
 * Class BrainSocketAppResponseTest
 */
class SocketAppResponseTest extends PHPUnit_Framework_TestCase {

	protected function tearDown()
    {
		Mockery::close();
	}


	public function testMessageReturnsAnObject()
    {
		$AppResponse = new SocketAppResponse();

		$response = $AppResponse->message('some.event');

		$this->assertObjectHasAttribute('data',$response);
		return $this->assertObjectHasAttribute('event',$response);
	}

}