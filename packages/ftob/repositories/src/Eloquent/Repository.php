<?php namespace Ftob\Repositories\Eloquent;

use Cache;
use Core\Models\Abstracts\CoreModel;
use Crm\Models\Product;
use Crm\Repositories\Eloquent\Criteria\Cart\CompanyIdCriteria;
use Debugbar;
use Ftob\Repositories\Contracts\CriteriaInterface;
use Ftob\Repositories\Criteria\Criteria;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Ftob\Repositories\Criteria\CriteriaCollection;
use Ftob\Repositories\Exceptions\RepositoryException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Container\Container as App;
use Saas\Services\StateApplicationService;
use Schema;

/**
 * Class Repository
 * @package Ftob\Repositories\Eloquent
 */
abstract class Repository implements RepositoryInterface, CriteriaInterface
{

    /**
     * @var App
     */
    private $app;

    /**
     * @var Builder
     */
    protected $builder;

    /**
     * @var Collection
     */
    protected $criteria;

    /**
     * @var bool
     */
    protected $skipCriteria = false;

    /**
     * @param App $app
     * @param Collection $collection
     * @throws \Ftob\Repositories\Exceptions\RepositoryException
     */
    public function __construct(App $app, Collection $collection)
    {
        $this->app = $app;
        $this->criteria = $collection;
        $this->resetScope();
        $this->makeModel();
    }

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    abstract public function model();

    /**
     * @param array $columns
     * @return Collection
     */
    public function all($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->builder->get($columns);
    }

    /**
     * @param array $columns
     * @return mixed|null
     */
    public function first($columns = array('*'))
    {
        $this->applyCriteria();
        return $this->builder->get($columns)->first();
    }

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 1, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->builder->paginate($perPage, $columns);
    }

    /**
     * Пагинатор, принимающий номер страницы.
     *
     * @param int $perPage Сколько элементов на странице
     * @param int $currentPage На какую страницу
     * @param array $columns
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateToPage($perPage = 1, $currentPage = 1, $columns = array('*'))
    {
        $this->applyCriteria();
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        return $this->builder->paginate($perPage, $columns);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->builder->getModel()->create($data);
    }

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = 'id')
    {
        return $this->builder->find($id)->fill($data)->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->builder->getModel()->destroy($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->builder->getModel()->find($id, $columns);
    }

    /**
     * Execute the query and get the first result.
     *
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->builder->getModel()->where($attribute, '=', $value)->first($columns);
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return Collection
     */
    public function findAllBy($attribute, $value, $columns = array('*'))
    {
        $this->applyCriteria();
        return $this->builder->where($attribute, '=', $value)->get($columns);
    }

    /**
     * @return string
     */
    public function toSql()
    {
        $this->applyCriteria();
        return $this->builder->getQuery()->toSql();
    }


    /**
     * @return Model
     * @throws RepositoryException
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());
        if (!$model instanceof Model) {
            throw new RepositoryException(
                "Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model"
            );
        }
        $newModel = new $model;
        $this->builder = $newModel->newQuery();
        $this->builder->setModel($newModel);

        // Company id check --------------------------->
        $tableName = with($newModel)->getTable();
        $keyName = $tableName . CoreModel::COMPANY;
        $hasCompanyId = (Cache::has($keyName)
            ? Cache::get($keyName) : Schema::hasColumn(with($newModel)->getTable(), 'company_id'));
        if($hasCompanyId) {

            /** @var StateApplicationService $appState */
            $appState = $this->app->make('StateApplicationService');

            if ($company = $appState->getCurrentCompany()) {
                $this->builder->where(CoreModel::COMPANY, $company->id);
            }

        }
        Cache::add($keyName, $hasCompanyId, 100000);

        // ------------------------------------------------->
        return $this->builder->getModel();
    }

    /**
     * @return $this
     */
    public function resetScope()
    {
        $this->skipCriteria(false);
        return $this;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function getByCriteria(Criteria $criteria)
    {
        $this->builder = $criteria->apply($this->builder, $this);
        return $this;
    }

    /**
     * @param Criteria $criteria
     * @return $this
     */
    public function pushCriteria(Criteria $criteria)
    {
        $this->criteria->push($criteria);
        return $this;
    }

    /**
     * @return $this
     */
    public function applyCriteria()
    {
        if ($this->skipCriteria === true) {
            return $this;
        }
        foreach ($this->getCriteria() as $criteria) {
            if ($criteria instanceof Criteria) {
                $this->builder = $criteria->apply($this->builder, $this);
            }
        }
        return $this;
    }

    /**
     * @param CriteriaCollection $criteriaCollection
     * @return $this
     */
    public function setCriteriaCollection(CriteriaCollection $criteriaCollection)
    {
        $this->criteria = $criteriaCollection;
        return $this;
    }

    /**
     * @return mixed
     */
    public function destroy()
    {
        $this->applyCriteria();
        return $this->builder->delete();

    }

    /**
     * @param array $value
     * @param string $attribute
     * @return Collection
     */
    public function findIn(array $value, $attribute = 'id')
    {
        $this->applyCriteria();
        return $this->builder->getQuery()->whereIn($attribute, array_values($value))->get();
    }
}