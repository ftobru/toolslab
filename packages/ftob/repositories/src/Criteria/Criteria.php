<?php namespace Ftob\Repositories\Criteria;

use Ftob\Repositories\Contracts\RepositoryInterface as Repository;
use Ftob\Repositories\Contracts\RepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Criteria
 * @package Ftob\Repositories\Criteria
 */
abstract class Criteria
{

    /**
     * @param Builder $builder
     * @param RepositoryInterface $repository
     * @return mixed
     */
    abstract public function apply(Builder $builder, Repository $repository);

}