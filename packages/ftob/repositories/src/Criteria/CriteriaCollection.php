<?php namespace Ftob\Repositories\Criteria;

use Ftob\Repositories\Exceptions\IsNotInstanceCriteriaException;
use Illuminate\Support\Collection;

/**
 * Class CriteriaCollection
 * @package Ftob\Repositories\Criteria
 */
class CriteriaCollection extends Collection
{
    /**
     * @param array $items
     * @throws IsNotInstanceCriteriaException
     */
    public function __construct(array $items = [])
    {
        foreach ($items as $item) {
            if (!($item instanceof Criteria)) {
                throw new IsNotInstanceCriteriaException;
            }
        }

        parent::__construct($items);
    }
}
