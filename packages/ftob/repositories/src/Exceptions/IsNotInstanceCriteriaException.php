<?php namespace Ftob\Repositories\Exceptions;

use Exception;

class IsNotInstanceCriteriaException extends Exception
{
    // Exception criteria collection
}
