<?php namespace Ftob\Repositories\Exceptions;

use Exception;

/**
 * Class RepositoryException
 * @package Bosnadev\Repositories\Exceptions
 */
class RepositoryException extends Exception {}