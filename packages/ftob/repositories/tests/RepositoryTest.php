<?php namespace Ftob\Repositories\Tests;


use Illuminate\Database\Eloquent\Model;
use \Mockery as m;
use \PHPUnit_Framework_TestCase as TestCase;

/**
 * Class RepositoryTest
 * @package Ftob\Tests\Repositories
 */
class RepositoryTest extends TestCase {

    protected $mock;
    protected $repository;

    public function setUp() {
        $this->mock = m::mock('Illuminate\Database\Eloquent\Model');
    }

    public function testInstance()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Model', $this->mock);
    }
}