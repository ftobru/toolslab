<?php

namespace IPGeoBase;
use DB;

/**
 * http://ipgeobase.ru/
 *
 *  Архив для базы можно скачать тут:
 *  http://ipgeobase.ru/files/db/Main/geo_files.tar.gz
 *
 *  Файлы распокавать в /storage/app/
 *
 *  Запустить CitySeeder чтобы обновить города
 *
 * Class IPGeoBase
 */
class IPGeoBase
{
    private $fhandleCIDR;
    private $fhandleCities;
    private $fSizeCIDR;
    private $fsizeCities;

    /**
     * @param bool $CIDRFile файл базы диапазонов IP (cidr_optim.txt)
     * @param bool $CitiesFile файл базы городов (cities.txt)
     */
    public function __construct($CIDRFile = false, $CitiesFile = false)
    {
        if (!$CIDRFile) {
            $CIDRFile = base_path() . '/storage/app/cidr_optim.txt';
        }
        if (!$CitiesFile) {
            $CitiesFile = base_path() . '/storage/app/cities.txt';
        }
        $this->fhandleCIDR = fopen($CIDRFile, 'r') or die("Cannot open $CIDRFile");
        $this->fhandleCities = fopen($CitiesFile, 'r') or die("Cannot open $CitiesFile");
        $this->fSizeCIDR = filesize($CIDRFile);
        $this->fsizeCities = filesize($CitiesFile);
    }

    /*
     * @brief Получение информации о городе по индексу
     * @param idx индекс города
     * @return массив или false, если не найдено
     */
    public function getCityByIdx($idx)
    {
        $city = DB::table('cities')->select()->where('idx', '=', $idx)->first();

        if($city) {
            return [
                'city' => $city->city,
                'region' => $city->region,
                'district' => $city->district,
                'lat' => $city->lat,
                'lng' => $city->lng
            ];
        }

        rewind($this->fhandleCities);
        while (!feof($this->fhandleCities)) {
            $str = fgets($this->fhandleCities);
            $arRecord = explode("\t", trim($str));
            if ($arRecord[0] == $idx) {
                return [
                    'city' => $arRecord[1],
                    'region' => $arRecord[2],
                    'district' => $arRecord[3],
                    'lat' => $arRecord[4],
                    'lng' => $arRecord[5]
                ];
            }
        }
        return false;
    }

    /*
     * @brief Получение гео-информации по IP
     * @param ip IPv4-адрес
     * @return массив или false, если не найдено
     */
    public function getRecord($ip)
    {
        $ip = sprintf('%u', ip2long($ip));

        rewind($this->fhandleCIDR);
        $rad = floor($this->fSizeCIDR / 2);
        $pos = $rad;
        while (fseek($this->fhandleCIDR, $pos, SEEK_SET) != -1) {
            if ($rad) {
                $str = fgets($this->fhandleCIDR);
            } else {
                rewind($this->fhandleCIDR);
            }

            $str = fgets($this->fhandleCIDR);

            if (!$str) {
                return false;
            }

            $arRecord = explode("\t", trim($str));
            $rad = floor($rad / 2);
            if (!$rad && ($ip < $arRecord[0] || $ip > $arRecord[1])) {
                return false;
            }

            if ($ip < $arRecord[0]) {
                $pos -= $rad;
            } elseif ($ip > $arRecord[1]) {
                $pos += $rad;
            } else {
                $result = [
                    'range' => $arRecord[2],
                    'cc' => $arRecord[3]
                ];

                if ($arRecord[4] != '-' && $cityResult = $this->getCityByIdx($arRecord[4])) {
                    $result += $cityResult;
                }

                return $result;
            }
        }
        return false;
    }


    /**
     * Заполняет таблицу городов из текстового файла.
     */
    public function fillCityTable()
    {
        if ($this->fhandleCities) {
            while (($buffer = fgets($this->fhandleCities, 4096)) !== false) {
                $buffer = iconv("cp1251", "UTF-8", $buffer);
                $buffer = str_replace("\n", "", $buffer);
                $buffer = explode("\t", $buffer);
                $city = [
                    'idx' => intval($buffer[0]),
                    'city' => $buffer[1],
                    'region' => $buffer[2],
                    'district' => $buffer[3],
                    'lat' => $buffer[4],
                    'lng' => $buffer[5]
                ];
                DB::table('cities')->insert($city);
            }
            if (!feof($this->fhandleCities)) {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($this->fhandleCities);
        }
    }
}