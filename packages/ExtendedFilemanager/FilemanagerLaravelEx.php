<?php namespace Pqb\FilemanagerLaravel;

use Auth;
use Illuminate\Http\Request;
use Saas\Services\CompanyService;
use Saas\Services\UserService;

/**
 * Class FilemanagerLaravelEx
 * @package Pqb\FilemanagerLaravel
 */
class FilemanagerLaravelEx
{
    const ROOT_FOLDER_NAME = 'FileStorage';

    /**
     * @param Request $request
     * @return FilemanagerEx
     */
    public static function Filemanager(Request $request)
    {
        return new FilemanagerEx($request);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return bool
     */
    public static function checkAccessToDir(Request $request)
    {
        $path_params = ['path', 'old', 'currentpath', 'newfilepath'];
        $isEnabled = $isEnabled = [
            'isEnabled' => false,
            'msg' => ''
        ];
        $params = $request->all();
        foreach ($params as $key => $value) {
            if (in_array($key, $path_params) && !is_int($key)) {
                $chunks = explode('/', $value);
                $baseDirKey = array_search(self::ROOT_FOLDER_NAME, $chunks);
                if (is_int($baseDirKey)) {
                    if (isset($chunks[$key + 1])) {
                        $rootFolderName = $chunks[$baseDirKey + 1];

                        $companyId = intval(substr($rootFolderName, 0, -32));
                        $userId = Auth::getUser()->id;

                        /** @var UserService $userService */
                        $userService = \App::make('UserService');

                        if ($rootFolderName != self::getFolderNameByCompanyId($companyId)) {
                            return [
                                'isEnabled' => false,
                                'msg' => 'Error: folder name not allowed.'
                            ];
                        }

                        $user = $userService->findUserByIdWithCompanies($userId);

                        if ($user && $user->companies) {
                            $userCompanies = $user->companies;
                            foreach ($userCompanies as $company) {
                                if ($company->id === $companyId) {
                                    $isEnabled = [
                                        'isEnabled' => true,
                                        'msg' => 'OK'
                                    ];
                                }
                            }
                        } else {
                            return [
                                'isEnabled' => false,
                                'msg' => 'Error: you have not shared folders.'
                            ];
                        }
                    } else {
                        return [
                            'isEnabled' => false,
                            'msg' => 'Error: you can not write to ' . self::ROOT_FOLDER_NAME . 'folder.'
                        ];
                    }
                } else {
                    return [
                        'isEnabled' => false,
                        'msg' => 'Error: you can write only to ' . self::ROOT_FOLDER_NAME . 'folder.'
                    ];
                }
            };
        }

        return $isEnabled;
    }

    /**
     * @param $id
     * @return string
     */
    private static function getFolderNameByCompanyId($id)
    {
        /** @var CompanyService $userService */
        $userService = \App::make('CompanyService');
        return $userService->generateCompanyDirName($id);
    }
}
