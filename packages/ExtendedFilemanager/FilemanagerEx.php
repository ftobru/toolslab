<?php
namespace Pqb\FilemanagerLaravel;

use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Psy\Exception\ErrorException;
use Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FilemanagerEx
 * @package Pqb\FilemanagerLaravel
 */
class FilemanagerEx extends Filemanager
{
    protected $request;

    protected $params;

    /**
     * @param $var
     * @param null $preserve
     * @return bool
     */
    public function getvar($var, $preserve = null)
    {
        if (!isset($this->params[$var]) || $this->params[$var] == '') {
            $this->error(sprintf($this->lang('INVALID_VAR'), $var));
        } else {
            $this->get[$var] = $this->sanitize($this->params[$var], $preserve);
            return true;
        }
    }

    /**
     * @param $var
     * @param bool $sanitize
     * @return bool
     */
    public function postvar($var, $sanitize = true)
    {
        if (!isset($this->params[$var]) || ($var != 'content' && $this->params[$var] == '')) {
            $this->error(sprintf($this->lang('INVALID_VAR'), $var));
        } else {
            if ($sanitize) {
                $this->post[$var] = $this->sanitize($this->params[$var]);
            } else {
                $this->post[$var] = $this->params[$var];
            }
            return true;
        }
    }

    /**
     * @return array
     */
    public function getinfo()
    {
        $this->item = [];
        $this->item['properties'] = $this->properties;
        if (file_exists(storage_path() . '/data/' .$this->get['path'])) {
            $this->get_file_info('', false);
            // handle path when set dynamically with $fm->setFileRoot() method
            if ($this->dynamic_fileroot != '') {
                $path = $this->dynamic_fileroot . $this->get['path'];
                $path = preg_replace('~/+~', '/', $path); // remove multiple slashes
            } else {
                $path = $this->get['path'];
            }
            $array = [
                'Path' => $path,
                'Filename' => $this->item['filename'],
                'File Type' => $this->item['filetype'],
                'Preview' => $this->item['preview'],
                'Properties' => $this->item['properties'],
                'Error' => "",
                'Code' => 0
            ];
            return Response::api(['result' => json_encode($array)]);
        } else {
            $error = $this->error("File does not exist.");
            return Response::api(['result' => json_encode($error)]);
        }
    }

    public function getfolder()
    {
        $array = array();
        $filesDir = array();
        $current_path = $this->getFullPath();
        $this->__log(__METHOD__ . ' - getFullPath ' . $current_path);
        if (!$this->isValidPath($current_path)) {
            $error = $this->error("No way.");
            return Response::api(['result' => json_encode($error)]);
        }
        if (!is_dir($current_path)) {
            $error = $this->error(sprintf($this->lang('DIRECTORY_NOT_EXIST'), $this->get['path']));
            return Response::api(['result' => json_encode($error)]);
        }
        if (!$handle = opendir($current_path)) {
            $error = $this->error(sprintf($this->lang('UNABLE_TO_OPEN_DIRECTORY'), $this->get['path']));
            return Response::api(['result' => json_encode($error)]);
        } else {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    array_push($filesDir, $file);
                }
            }
            closedir($handle);
            // By default
            // Sorting files by name ('default' or 'NAME_DESC' cases from $this->config['options']['fileSorting']
            natcasesort($filesDir);
            foreach ($filesDir as $file) {
                if (is_dir($current_path . $file)) {
                    if (!in_array($file, $this->config['exclude']['unallowed_dirs']) && !preg_match($this->config['exclude']['unallowed_dirs_REGEXP'], $file)) {
                        $array[$this->get['path'] . $file . '/'] = [
                            'Path' => $this->get['path'] . $file . '/',
                            'Filename' => $file,
                            'File Type' => 'dir',
                            'Preview' => $this->config['icons']['path'] . $this->config['icons']['directory'],
                            'Properties' => [
                                'Date Created' => date($this->config['options']['dateFormat'], filectime($this->getFullPath($this->get['path'] . $file . '/'))),
                                'Date Modified' => date($this->config['options']['dateFormat'], filemtime($this->getFullPath($this->get['path'] . $file . '/'))),
                                'filemtime' => filemtime($this->getFullPath($this->get['path'] . $file . '/')),
                                'Height' => null,
                                'Width' => null,
                                'Size' => null
                            ],
                            'Error' => "",
                            'Code' => 0
                        ];
                    }
                } else if (!in_array($file, $this->config['exclude']['unallowed_files']) && !preg_match($this->config['exclude']['unallowed_files_REGEXP'], $file)) {
                    $this->item = array();
                    $this->item['properties'] = $this->properties;
                    $this->get_file_info($this->get['path'] . $file, true);
                    if (!isset($this->params['type']) || (isset($this->params['type']) && strtolower($this->params['type']) == 'images' && in_array(strtolower($this->item['filetype']), array_map('strtolower', $this->config['images']['imagesExt'])))) {
                        if ($this->config['upload']['imagesOnly'] == false || ($this->config['upload']['imagesOnly'] == true && in_array(strtolower($this->item['filetype']), array_map('strtolower', $this->config['images']['imagesExt'])))) {
                            $array[$this->get['path'] . $file] = array(
                                'Path' => $this->get['path'] . $file,
                                'Filename' => $this->item['filename'],
                                'File Type' => $this->item['filetype'],
                                'Preview' => $this->item['preview'],
                                'Properties' => $this->item['properties'],
                                'Error' => "",
                                'Code' => 0
                            );
                        }
                    }
                }
            }
        }
        $array = $this->sortFiles($array);
        return Response::api(['result' => json_encode($array)]);
    }

    public function editfile()
    {
        $current_path = $this->getFullPath();
        if (!$this->has_permission('edit') || !$this->isValidPath($current_path) || !$this->isEditable($current_path)) {
            $error = $this->error("No way.");
            return Response::api(['result' => json_encode($error)]);
        }
        $this->__log(__METHOD__ . ' - editing file ' . $current_path);
        $content = file_get_contents($current_path);
        $content = htmlspecialchars($content);
        if ($content === false) {
            $error = $this->error(sprintf($this->lang('ERROR_OPENING_FILE')));
            return Response::api(['result' => json_encode($error)]);
        }
        $array = array(
            'Error' => "",
            'Code' => 0,
            'Path' => $this->get['path'],
            'Content' => $this->formatPath($content)
        );
        return Response::api(['result' => json_encode($array)]);
    }

    /**
     * Зачем он нужен?
     * @return mixed
     */
    public function savefile()
    {
        $current_path = $this->getFullPath($this->post['path']);
        if (!$this->has_permission('edit') || !$this->isValidPath($current_path) || !$this->isEditable($current_path)) {
            $error = $this->error("No way.");
            return Response::api(['result' => json_encode($error)]);
        }
        if (!is_writable($current_path)) {
            $error = $this->error(sprintf($this->lang('ERROR_WRITING_PERM')));
            return Response::api(['result' => json_encode($error)]);
        }
        $this->__log(__METHOD__ . ' - saving file ' . $current_path);
        $content = htmlspecialchars_decode($this->post['content']);
        $r = file_put_contents($current_path, $content, LOCK_EX);
        if (!is_numeric($r)) {
            $error = $this->error(sprintf($this->lang('ERROR_SAVING_FILE')));
            return Response::api(['result' => json_encode($error)]);
        }
        $array = array(
            'Error' => "",
            'Code' => 0,
            'Path' => $this->formatPath($this->post['path'])
        );
        return Response::api(['result' => json_encode($array)]);
    }

    /**
     * @return mixed
     */
    public function rename()
    {
        $suffix = '';

        if (substr($this->get['old'], -1, 1) == '/') {
            $this->get['old'] = substr($this->get['old'], 0, (strlen($this->get['old']) - 1));
            $suffix = '/';
        }
        $tmp = explode('/', $this->get['old']);
        $filename = $tmp[(sizeof($tmp) - 1)];
        $path = str_replace('/' . $filename, '', $this->get['old']);
        $new_file = $this->getFullPath($path . '/' . $this->get['new']) . $suffix;
        $old_file = $this->getFullPath($this->get['old']) . $suffix;
        if (!$this->has_permission('rename') || !$this->isValidPath($old_file)) {
            $error = $this->error("No way.");
            return Response::api(['result' => json_encode($error)]);
        }
        // For file only - we check if the new given extension is allowed regarding the security Policy settings
        if (is_file($old_file) &&
            $this->config['security']['allowChangeExtensions'] &&
            !$this->isAllowedFileType($new_file)
        ) {
            $error = $this->error(sprintf($this->lang('INVALID_FILE_TYPE')));
            return Response::api(['result' => json_encode($error)]);
        }
        $this->__log(__METHOD__ . ' - renaming ' . $old_file . ' to ' . $new_file);
        if (file_exists($new_file)) {
            if ($suffix == '/' && is_dir($new_file)) {
                $error = $this->error(sprintf($this->lang('DIRECTORY_ALREADY_EXISTS'), $this->get['new']));
                return Response::api(['result' => json_encode($error)]);
            }
            if ($suffix == '' && is_file($new_file)) {
                $error = $this->error(sprintf($this->lang('FILE_ALREADY_EXISTS'), $this->get['new']));
                return Response::api(['result' => json_encode($error)]);
            }
        }
        if(!file_exists($old_file)) {
            return Response::api(['result' => json_encode($this->error('File does not exist!'))]);
        }
        if (!rename($old_file, $new_file)) {
            if (is_dir($old_file)) {
                $error = $this->error(sprintf($this->lang('ERROR_RENAMING_DIRECTORY'), $filename, $this->get['new']));
                return Response::api(['result' => json_encode($error)]);
            } else {
                $error = $this->error(sprintf($this->lang('ERROR_RENAMING_FILE'), $filename, $this->get['new']));
                return Response::api(['result' => json_encode($error)]);
            }
        }
        $array = [
            'Error' => "",
            'Code' => 0,
            'Old Path' => $this->get['old'],
            'Old Name' => $filename,
            'New Path' => $path . '/' . $this->get['new'] . $suffix,
            'New Name' => $this->get['new']
        ];
        return Response::api(['result' => json_encode($array)]);
    }

    /**
     * @return mixed
     */
    public function move()
    {
        // dynamic fileroot dir must be used when enabled
        $rootDir = $this->dynamic_fileroot;
        if (empty($rootDir)) {
            $rootDir = $this->get['root'];
        }
        $rootDir = str_replace('//', '/', $rootDir);
        $oldPath = $this->getFullPath($this->get['old']);
        // old path
        $tmp = explode('/', trim($this->get['old'], '/'));
        $fileName = array_pop($tmp); // file name or new dir name
        $path = '/' . implode('/', $tmp) . '/';
        // new path
        if (substr($this->get['new'], 0, 1) != "/") {
            // make path relative from old dir
            $newPath = $path . '/' . $this->get['new'] . '/';
        } else {
            $newPath = $rootDir . '/' . $this->get['new'] . '/';
        }
        $newPath = preg_replace('#/+#', '/', $newPath);
        $newPath = $this->expandPath($newPath, true);
        //!important! check that we are still under ROOT dir
        if (strncasecmp($newPath, $rootDir, strlen($rootDir))) {
            $error = $this->error(sprintf($this->lang('INVALID_DIRECTORY_OR_FILE'), $this->get['new']));
            return Response::api(['result' => json_encode($error)]);
        }
        if (!$this->has_permission('move') || !$this->isValidPath($oldPath)) {
            $error = $this->error("No way.");
            return Response::api(['result' => json_encode($error)]);
        }
        $newRelativePath = $newPath;
        $newPath = $this->getFullPath($newPath);
        // check if file already exists
        if (file_exists($newPath . $fileName)) {
            if (is_dir($newPath . $fileName)) {
                $error = $this->error(
                    sprintf($this->lang('DIRECTORY_ALREADY_EXISTS'), rtrim($this->get['new'], '/') . '/' . $fileName)
                );
                return Response::api(['result' => json_encode($error)]);
            } else {
                $error = $this->error(
                    sprintf($this->lang('FILE_ALREADY_EXISTS'), rtrim($this->get['new'], '/') . '/' . $fileName)
                );
                return Response::api(['result' => json_encode($error)]);
            }
        }
        // create dir if not exists
        if (!file_exists($newPath)) {
            if (!mkdir($newPath, 0755, true)) {
                $error = $this->error(sprintf($this->lang('UNABLE_TO_CREATE_DIRECTORY'), $newPath));
                return Response::api(['result' => json_encode($error)]);
            }
        }
        // move
        $this->__log(__METHOD__ . ' - moving ' . $oldPath . ' to directory ' . $newPath);
        try {
            if (!rename($oldPath, $newPath . $fileName)) {
                if (is_dir($oldPath)) {
                    $error = $this->error(sprintf($this->lang('ERROR_RENAMING_DIRECTORY'), $path, $this->get['new']));
                    return Response::api(['result' => json_encode($error)]);
                } else {
                    $error = $this->error(
                        sprintf($this->lang('ERROR_RENAMING_FILE'), $path . $fileName, $this->get['new'])
                    );
                    return Response::api(['result' => json_encode($error)]);
                }
            }
        } catch (\Exception $ex) {
            return Response::api(['result' => json_encode($this->error('No such file or directory.'))]);
        }

        $array = [
            'Error' => "",
            'Code' => 0,
            'Old Path' => $path,
            'Old Name' => $fileName,
            'New Path' => $this->formatPath($newRelativePath),
            'New Name' => $fileName,
        ];
        return Response::api(['result' => json_encode($array)]);
    }

    /**
     * @return array
     */
    public function delete()
    {
        $current_path = $this->getFullPath();
        $thumbnail_path = $this->get_thumbnail_path($current_path);
        if (!$this->has_permission('delete') || !$this->isValidPath($current_path)) {
            $error = $this->error("No way.");
            return Response::api(['result' => json_encode($error)]);
        }
        if (is_dir($current_path)) {
            $this->unlinkRecursive($current_path);
            // we remove thumbnails if needed
            $this->__log(__METHOD__ . ' - deleting thumbnails folder ' . $thumbnail_path);
            $this->unlinkRecursive($thumbnail_path);
            $array = [
                'Error' => "",
                'Code' => 0,
                'Path' => $this->formatPath($this->get['path'])
            ];
            $this->__log(__METHOD__ . ' - deleting folder ' . $current_path);
            return $array;
        } elseif (file_exists($current_path)) {
            unlink($current_path);
            // delete thumbail if exists
            $this->__log(__METHOD__ . ' - deleting thumbnail file ' . $thumbnail_path);
            if (file_exists($thumbnail_path)) {
                unlink($thumbnail_path);
            };
            $array = [
                'Error' => "",
                'Code' => 0,
                'Path' => $this->formatPath($this->get['path'])
            ];
            $this->__log(__METHOD__ . ' - deleting file ' . $current_path);
            return Response::api(['result' => json_encode($array)]);
        } else {
            return Response::api([
                'result' => json_encode($this->error(sprintf($this->lang('INVALID_DIRECTORY_OR_FILE'))))
            ]);
        }
    }

    public function replace()
    {
        $this->setParams($this->request->all());

        /** @var UploadedFile $file */
        $files = $this->request->file();
        if (isset($files[0])) {
            $file = $files[0];
            $size = $file->getClientSize();

            // we determine max upload size if not set
            if ($this->config['upload']['fileSizeLimit'] == 'auto') {
                $this->config['upload']['fileSizeLimit'] = $this->getMaxUploadFileSize();
            }
            if ($size > ($this->config['upload']['fileSizeLimit'] * 1024 * 1024)) {
                $error = $this->error(
                    sprintf(
                        $this->lang('UPLOAD_FILES_SMALLER_THAN'),
                        $this->config['upload']['fileSizeLimit'] . $this->lang('mb')
                    ),
                    true
                );
                return Response::api(['result' => json_encode($error)]);
            }

            $extension = $file->getClientOriginalExtension();
            $fileName = $this->cleanString($file->getClientOriginalName(), ['.', '-']);

            if (strtolower(pathinfo($fileName, PATHINFO_EXTENSION)) != strtolower(pathinfo($this->post['newfilepath'], PATHINFO_EXTENSION))) {
                $error = $this->error(
                    sprintf(
                        $this->lang('ERROR_REPLACING_FILE') . ' ' . pathinfo(
                            $this->post['newfilepath'],
                            PATHINFO_EXTENSION
                        )
                    ),
                    true
                );
                return Response::api(['result' => json_encode($error)]);
            }

            if (!$this->isAllowedFileType($fileName)) {
                $error = $this->error(sprintf($this->lang('INVALID_FILE_TYPE')), true);
                return Response::api(['result' => json_encode($error)]);
            }

            $current_path = $this->getFullPath($this->post['newfilepath']);


            if (!$this->has_permission('replace') || !$this->isValidPath($current_path)) {
                $error = $this->error("No way.");
                return Response::api(['result' => json_encode($error)]);
            }

            if (!$this->isValidPath($current_path)) {
                $error = $this->error("No way.");
                return Response::api(['result' => json_encode($error)]);
            }

            //write file to storage.
            $destinationPath = storage_path() . '/data/' . $this->request->newfilepath;
            $destinationPath = str_replace("//", "/", $destinationPath);
            $destinationPath = pathinfo($destinationPath);
            $file->move($destinationPath['dirname'], $fileName);

            try {
                if ($this->is_image($current_path) && file_exists($this->get_thumbnail($current_path))) {
                    unlink($this->get_thumbnail($current_path));
                }

                $this->get_thumbnail($current_path);

                // automatically resize image if it's too big
                $imagePath = $current_path;
                if ($this->is_image($imagePath) && $this->config['images']['resize']['enabled']) {
                    $size = @getimagesize($imagePath);
                    if ($size) {
                        if ($size[0] > $this->config['images']['resize']['maxWidth'] ||
                            $size[1] > $this->config['images']['resize']['maxHeight']
                        ) {
                            $image = Image::make($imagePath);
                            $resized = $image->resize(
                                $this->config['images']['resize']['maxWidth'],
                                $this->config['images']['resize']['maxHeight'],
                                function ($constraint) {
                                    $constraint->aspectRatio();
                                }
                            );
                            $resized->save($imagePath);
                            $this->__log(__METHOD__ . ' - resizing image : ' . $current_path);
                        }
                    }
                }
                chmod($current_path, 0644);
                $response = [
                    'Path' => dirname($this->post['newfilepath']),
                    'Name' => basename($this->post['newfilepath']),
                    'Error' => "",
                    'Code' => 0
                ];
                $this->__log(__METHOD__ . ' - replacing file ' . $current_path);
                return Response::api(['result' => json_encode($response)]);
            } catch (\ErrorException $ex) {
                return Response::api(['result' => json_encode($this->error('Image file is damaged!'))]);
            }
        }
        return Response::api(['result' => json_encode($this->error('Not files attached.'))]);
    }

    public function add()
    {
        $this->setParams($this->request->all());

        /** @var UploadedFile $file */
        $files = $this->request->file();
        if (isset($files[0])) {
            $file = $files[0];
            $size = $file->getClientSize();

            // we determine max upload size if not set
            if ($this->config['upload']['fileSizeLimit'] == 'auto') {
                $this->config['upload']['fileSizeLimit'] = $this->getMaxUploadFileSize();
            }
            if ($size > ($this->config['upload']['fileSizeLimit'] * 1024 * 1024)) {
                $error = $this->error(
                    sprintf(
                        $this->lang('UPLOAD_FILES_SMALLER_THAN'),
                        $this->config['upload']['fileSizeLimit'] . $this->lang('mb')
                    ),
                    true
                );
                return Response::api(['result' => json_encode($error)]);
            }

            // we check if extension is allowed regarding the security Policy settings
            if (!$this->isAllowedFileType($file->getClientOriginalName())) {
                $error = $this->error(sprintf($this->lang('INVALID_FILE_TYPE')), true);
                return Response::api(['result' => json_encode($error)]);
            }

            // we check if only images are allowed
            if ($this->config['upload']['imagesOnly'] ||
                (isset($this->params['type']) && strtolower($this->params['type']) == 'images')
            ) {
                if (!($size = @getimagesize($file->getPathname()))) {
                    $error = $this->error(sprintf($this->lang('UPLOAD_IMAGES_ONLY')), true);
                    return Response::api(['result' => json_encode($error)]);
                }
                if (!in_array($size[2], [1, 2, 3, 7, 8])) {
                    $error = $this->error(sprintf($this->lang('UPLOAD_IMAGES_TYPE_JPEG_GIF_PNG')), true);
                    return Response::api(['result' => json_encode($error)]);
                }
            }


            $extension = $file->getClientOriginalExtension();
            $fileName = $this->cleanString($file->getClientOriginalName(), ['.', '-']);
            $current_path = $this->getFullPath($this->post['currentpath']);

            if (!$this->isValidPath($current_path)) {
                $error = $this->error("No way.");
                return Response::api(['result' => json_encode($error)]);
            }
            if (!$this->config['upload']['overwrite']) {
                $fileName = $this->checkFilename($current_path, $fileName);
            }

            //write file to storage.
            $destinationPath = storage_path() . '/data/' . $this->request->currentpath;
            $destinationPath = str_replace("//", "/", $destinationPath);

            $file->move($destinationPath, $fileName);

            // Generate some get_thumbnail.
            try {
                if ($this->is_image($destinationPath.$fileName) &&
                    file_exists($this->get_thumbnail($destinationPath.$fileName))
                ) {
                    unlink($this->get_thumbnail($destinationPath.$fileName));
                }
                $this->get_thumbnail($destinationPath.$fileName);
            } catch (ErrorException $ex) {
                return Response::api(['result' => $ex->getMessage()]);
            }



        }
        $response = [
            'Path' => $current_path,
            'Name' => $fileName,
            'Error' => "",
            'Code' => 0
        ];
        $this->__log(__METHOD__ . ' - adding file ' . $fileName . ' into ' . $current_path);
        return Response::api(['result' => json_encode($response)]);
    }

    /**
     * @return array
     */
    public function addfolder()
    {
        $current_path = $this->getFullPath();
        if (!$this->isValidPath($current_path)) {
            return Response::api(['result' => json_encode($this->error("No way."))]);
        }
        if (is_dir($current_path . $this->get['name'])) {
            return Response::api([
                'result' => json_encode(
                    $this->error(sprintf($this->lang('DIRECTORY_ALREADY_EXISTS'), $this->get['name']))
                )
            ]);
        }
        $newdir = $this->cleanString($this->get['name']);
        if (!mkdir($current_path . $newdir, 0755)) {
            return Response::api([
                'result' => json_encode(
                    $this->error(sprintf($this->lang('UNABLE_TO_CREATE_DIRECTORY'), $newdir))
                )
            ]);
        }
        $array = [
            'Parent' => $this->get['path'],
            'Name' => $this->get['name'],
            'Error' => "",
            'Code' => 0
        ];
        $this->__log(__METHOD__ . ' - adding folder ' . $current_path . $newdir);
        return Response::api(['result' => json_encode($array)]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download()
    {
        $current_path = $this->getFullPath();
        if (!$this->has_permission('download') || !$this->isValidPath($current_path)) {
            return Response::api(['result' => json_encode($this->error("No way."))]);
        }
        // we check if extension is allowed regarding the security Policy settings
        if (!$this->isAllowedFileType(basename($current_path))) {
            return Response::api([
                'result' => json_encode($this->error(sprintf($this->lang('INVALID_FILE_TYPE')), true))
            ]);
        }
        if (isset($this->get['path']) && file_exists($current_path)) {
            $headers = [
                'Content-type: application/force-download',
                'Content-Disposition: inline; filename="' . basename($current_path) . '"',
                'Content-Transfer-Encoding: Binary',
                'Content-length: ' . filesize($current_path),
                'Content-Type: application/octet-stream',
                'Content-Disposition: attachment; filename="' . basename($current_path) . '"',
            ];

            $this->__log(__METHOD__ . ' - downloading ' . $current_path);

            return Response::download($current_path, basename($current_path), $headers);
        } else {
            return Response::api([
                'result' => $this->error(sprintf($this->lang('FILE_DOES_NOT_EXIST'), $current_path))
            ]);
        }
    }

    /**
     * @param $thumbnail
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|void
     */
    public function preview($thumbnail)
    {
        $current_path = $this->getFullPath();
        if (isset($this->get['path']) && file_exists($current_path)) {
            // if $thumbnail is set to true we return the thumbnail
            if ($this->config['options']['generateThumbnails'] == true && $thumbnail == true) {
                // get thumbnail (and create it if needed)
                $returned_path = $this->get_thumbnail($current_path);
            } else {
                $returned_path = $current_path;
            }
            $headers = [
                "Content-type: image/" . strtolower(pathinfo($returned_path, PATHINFO_EXTENSION)),
                "Content-Transfer-Encoding: Binary",
                "Content-length: " . filesize($returned_path),
                'Content-Disposition: inline; filename="' . basename($returned_path) . '"',
                'Content-Type: application/octet-stream',
                'Content-Disposition: attachment; filename="' . basename($current_path) . '"',
            ];

            return Response::download($current_path, basename($current_path), $headers);

        } else {
            return Response::api([
                'result' => json_encode($this->error(sprintf($this->lang('FILE_DOES_NOT_EXIST'), $current_path)))
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function getMaxUploadFileSize()
    {
        $max_upload = (int)ini_get('upload_max_filesize');
        $max_post = (int)ini_get('post_max_size');
        $memory_limit = (int)ini_get('memory_limit');
        $upload_mb = min($max_upload, $max_post, $memory_limit);
        $this->__log(__METHOD__ . ' - max upload file size is ' . $upload_mb . 'Mb');
        return $upload_mb;
    }

    /**
     * @param \Illuminate\Http\Request|string $request
     * @param string $extraConfig
     */
    public function __construct(Request $request, $extraConfig = '')
    {
        $this->request = $request;
        // getting default config file
        $content = file_get_contents(
            public_path() . $this->dir_filemanager . "/filemanager/scripts/filemanager.config.js.default"
        );
        $config_default = json_decode($content, true);
        // getting user config file
        $content = file_get_contents(
            public_path() . $this->dir_filemanager . "/filemanager/scripts/filemanager.config.js"
        );
        $config = json_decode($content, true);
        $this->config = array_replace_recursive($config_default, $config);
        // override config options if needed
        if (!empty($extraConfig)) {
            $this->setup($extraConfig);
        }
        // $this->root = dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR;
        $this->root = public_path() . DIRECTORY_SEPARATOR;
        // var_dump($this->root);
        $this->properties = [
            'Date Created' => null,
            'Date Modified' => null,
            'Height' => null,
            'Width' => null,
            'Size' => null
        ];
        // Log actions or not?
        if ($this->config['options']['logger'] == true) {
            if (isset($this->config['options']['logfile'])) {
                $this->logfile = $this->config['options']['logfile'];
            }
            $this->enableLog();
        }
        // if fileRoot is set manually, $this->doc_root takes fileRoot value
        // for security check in isValidPath() method
        // else it takes $_SERVER['DOCUMENT_ROOT'] default value
        if ($this->config['options']['fileRoot'] !== false) {
            if ($this->config['options']['serverRoot'] === true) {
                $this->doc_root = $_SERVER['DOCUMENT_ROOT'];
                $this->separator = basename($this->config['options']['fileRoot']);
            } else {
                $this->doc_root = $this->config['options']['fileRoot'];
                $this->separator = basename($this->config['options']['fileRoot']);
            }
        } else {
            $this->doc_root = $_SERVER['DOCUMENT_ROOT'];
        }
        $this->__log(__METHOD__ . ' $this->doc_root value ' . $this->doc_root);
        $this->__log(__METHOD__ . ' $this->separator value ' . $this->separator);
        $this->setParams($request->all());

        $this->setPermissions();
        $this->availableLanguages();
        $this->loadLanguageFile();
    }

    /**
     * Return Thumbnail path from given path
     * works for both file and dir path
     * @param string $path
     * @return string
     */
    private function get_thumbnail_path($path)
    {
        $a = explode($this->separator, $path);
        $path_parts = pathinfo($path);
        $thumbnail_path = $path_parts['dirname']. '/' . $this->cachefolder;
        $thumbnail_name = $path_parts['filename'] . '_' . $this->thumbnail_width . 'x' . $this->thumbnail_height . 'px.' . $path_parts['extension'];
        if (is_dir($path)) {
            $thumbnail_fullpath = $thumbnail_path;
        } else {
            $thumbnail_fullpath = $thumbnail_path . $thumbnail_name;
        }
        return $thumbnail_fullpath;
    }

    /**
     * @param $string
     * @param array $allowed
     * @return array|mixed
     */
    private function cleanString($string, $allowed = [])
    {
        $allow = null;
        if (!empty($allowed)) {
            foreach ($allowed as $value) {
                $allow .= "\\$value";
            }
        }
        $mapping = [
            'Š' => 'S', 'š' => 's', 'Đ' => 'Dj', 'đ' => 'dj', 'Ž' => 'Z', 'ž' => 'z', 'Č' => 'C', 'č' => 'c',
            'Ć' => 'C', 'ć' => 'c', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A',
            'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O', 'Ø' => 'O',
            'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a',
            'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o',
            'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'ű' => 'u',
            'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'Ŕ' => 'R', 'ŕ' => 'r', ' ' => '_',
            "'" => '_', '/' => ''
        ];
        if (is_array($string)) {
            $cleaned = [];
            foreach ($string as $key => $clean) {
                $clean = strtr($clean, $mapping);
                if ($this->config['options']['chars_only_latin'] == true) {
                    $clean = preg_replace("/[^{$allow}_a-zA-Z0-9]/u", '', $clean);
                    // $clean = preg_replace("/[^{$allow}_a-zA-Z0-9\x{0430}-\x{044F}\x{0410}-\x{042F}]/u", '', $clean); // allow only latin alphabet with cyrillic
                }
                $cleaned[$key] = preg_replace('/[_]+/', '_', $clean); // remove double underscore
            }
        } else {
            $string = strtr($string, $mapping);
            if ($this->config['options']['chars_only_latin'] == true) {
                $clean = preg_replace("/[^{$allow}_a-zA-Z0-9]/u", '', $string);
                // $clean = preg_replace("/[^{$allow}_a-zA-Z0-9\x{0430}-\x{044F}\x{0410}-\x{042F}]/u", '', $string); // allow only latin alphabet with cyrillic
            }
            $cleaned = preg_replace('/[_]+/', '_', $string); // remove double underscore
        }
        return $cleaned;
    }

    /**
     * format path regarding the initial configuration
     * @param string $path
     * @return mixed|string
     */
    private function formatPath($path)
    {
        if ($this->dynamic_fileroot != '') {
            $a = explode($this->separator, $path);
            return end($a);
        } else {
            return $path;
        }
    }

    /**
     * For debugging just call
     * the direct URL http://localhost/Filemanager/connectors
     * ?mode=preview&path=%2FFilemanager%2Fuserfiles%2FMy%20folder3%2Fblanches_neiges.jPg&thumbnail=true
     * and echo vars below
     * @param string $path
     * @return string
     */
    private function get_thumbnail($path)
    {
        $thumbnail_fullpath = $this->get_thumbnail_path($path);
        // echo $thumbnail_fullpath.'<br>';
        // if thumbnail does not exist we generate it
        if (!file_exists($thumbnail_fullpath)) {
            // create folder if it does not exist
            if (!file_exists(dirname($thumbnail_fullpath))) {
                mkdir(dirname($thumbnail_fullpath), 0755, true);
            }
            $image = Image::make($path);
            $resized = $image->fit($this->thumbnail_width, $this->thumbnail_height);
            $resized->save($thumbnail_fullpath);
            $this->__log(__METHOD__ . ' - generating thumbnail :  ' . $thumbnail_fullpath);
        }
        return $thumbnail_fullpath;
    }

    /**
     * @param $path
     * @return bool
     */
    private function isValidPath($path)
    {
        // @todo remove debug message
        // $this->__log('compare : ' .$this->getFullPath(). '($this->getFullPath())  and ' . $path . '(path)');
        // $this->__log('strncmp() retruned value : ' .strncmp($path, $this->getFullPath(), strlen($this->getFullPath())));
        return !strncmp($path, $this->getFullPath(), strlen($this->getFullPath()));
    }

    /**
     * Checking if permission is set or not for a given action
     * @param string $action
     * @return boolean
     */
    private function has_permission($action)
    {
        if (in_array($action, $this->allowed_actions))
            return true;
        return false;
    }

    /**
     * @param $url_params
     */
    private function setParams($url_params)
    {
        $params = [];
        if (is_array($url_params)) {
            foreach ($url_params as $key => $value) {
                $params[$key] = $value;
            }
        }
        $this->params = $params;
    }

    /**
     * @param $array
     * @return array
     */
    private function sortFiles($array)
    {
        // handle 'NAME_ASC'
        if ($this->config['options']['fileSorting'] == 'NAME_ASC') {
            $array = array_reverse($array);
        }
        // handle 'TYPE_ASC' and 'TYPE_DESC'
        if (strpos($this->config['options']['fileSorting'], 'TYPE_') !== false || $this->config['options']['fileSorting'] == 'default') {
            $a = [];
            $b = [];
            foreach ($array as $key => $item) {
                if (strcmp($item["File Type"], "dir") == 0) {
                    $a[$key] = $item;
                } else {
                    $b[$key] = $item;
                }
            }
            if ($this->config['options']['fileSorting'] == 'TYPE_ASC') {
                $array = array_merge($a, $b);
            }
            if ($this->config['options']['fileSorting'] == 'TYPE_DESC' || $this->config['options']['fileSorting'] == 'default') {
                $array = array_merge($b, $a);
            }
        }
        // handle 'MODIFIED_ASC' and 'MODIFIED_DESC'
        if (strpos($this->config['options']['fileSorting'], 'MODIFIED_') !== false) {
            $modified_order_array = array();  // new array as a column to sort collector
            foreach ($array as $item) {
                $modified_order_array[] = $item['Properties']['filemtime'];
            }
            if ($this->config['options']['fileSorting'] == 'MODIFIED_ASC') {
                array_multisort($modified_order_array, SORT_ASC, $array);
            }
            if ($this->config['options']['fileSorting'] == 'MODIFIED_DESC') {
                array_multisort($modified_order_array, SORT_DESC, $array);
            }
            return $array;
        }
        return $array;
    }

    /**
     * isAllowedFile()
     * check if extension is allowed regarding the security Policy / Restrictions settings
     * @param string $file
     * @return bool
     */
    private function isAllowedFileType($file)
    {
        $path_parts = pathinfo($file);
        // if there is no extension AND no extension file are not allowed
        if (!isset($path_parts['extension']) && $this->config['security']['allowNoExtension'] == false) {
            return false;
        } else {
            return true;
        }
        $exts = array_map('strtolower', $this->config['security']['uploadRestrictions']);
        if ($this->config['security']['uploadPolicy'] == 'DISALLOW_ALL') {
            if (!in_array(strtolower($path_parts['extension']), $exts))
                return false;
        }
        if ($this->config['security']['uploadPolicy'] == 'ALLOW_ALL') {
            if (in_array(strtolower($path_parts['extension']), $exts))
                return false;
        }
        return true;
    }

    /**
     * @param string $path
     * @param bool $thumbnail
     */
    private function get_file_info($path = '', $thumbnail = false)
    {
        // DO NOT  rawurlencode() since $current_path it
        // is used for displaying name file
        if ($path == '') {
            $current_path = $this->get['path'];
        } else {
            $current_path = $path;
        }
        $tmp = explode('/', $current_path);
        $this->item['filename'] = $tmp[(sizeof($tmp) - 1)];
        $tmp = explode('.', $this->item['filename']);
        $this->item['filetype'] = $tmp[(sizeof($tmp) - 1)];
        $this->item['filemtime'] = filemtime($this->getFullPath($current_path));
        $this->item['filectime'] = filectime($this->getFullPath($current_path));
        $this->item['preview'] = $this->config['icons']['path'] . $this->config['icons']['default'];
        if (is_dir($current_path)) {
            $this->item['preview'] = $this->config['icons']['path'] . $this->config['icons']['directory'];
        } else if (in_array(strtolower($this->item['filetype']), array_map('strtolower', $this->config['images']['imagesExt']))) {
            // svg should not be previewed as raster formats images
            if ($this->item['filetype'] == 'svg') {
                $this->item['preview'] = $current_path;
            } else {
                $this->item['preview'] = $this->connector_url . '?mode=preview&path=' . rawurlencode($current_path) . '&' . time();
                if ($thumbnail) $this->item['preview'] .= '&thumbnail=true';
            }
            //if(isset($get['getsize']) && $get['getsize']=='true') {
            $this->item['properties']['Size'] = filesize($this->getFullPath($current_path));
            if ($this->item['properties']['Size']) {
                list($width, $height, $type, $attr) = getimagesize($this->getFullPath($current_path));
            } else {
                $this->item['properties']['Size'] = 0;
                list($width, $height) = [0, 0];
            }
            $this->item['properties']['Height'] = $height;
            $this->item['properties']['Width'] = $width;
            $this->item['properties']['Size'] = filesize($this->getFullPath($current_path));
            //}
        } else if (file_exists($this->root . $this->config['icons']['path'] . strtolower($this->item['filetype']) . '.png')) {
            $this->item['preview'] = $this->config['icons']['path'] . strtolower($this->item['filetype']) . '.png';
            $this->item['properties']['Size'] = filesize($this->getFullPath($current_path));
            if (!$this->item['properties']['Size']) $this->item['properties']['Size'] = 0;
        }
        $this->item['properties']['Date Modified'] = date($this->config['options']['dateFormat'], $this->item['filemtime']);
        $this->item['properties']['filemtime'] = filemtime($this->getFullPath($current_path));
        //$return['properties']['Date Created'] = $this->config['options']['dateFormat'], $return['filectime']); // PHP cannot get create timestamp
    }

    public function run()
    {
        if (!isset($this->params)) {
            return Response::api(['result' => json_encode($this->error($this->lang('INVALID_ACTION')))]);
        } else {
            if (isset($this->params['mode']) && $this->params['mode'] != '' && $this->request->method() == 'GET') {
                switch ($this->params['mode']) {
                    default:
                        $this->error($this->lang('MODE_ERROR'));
                        break;
                    case 'getinfo': // done
                        if ($this->getvar('path')) {
                            $response = $this->getinfo();
                        }
                        break;
                    case 'getfolder':
                        if ($this->getvar('path')) {
                            $response = $this->getfolder();
                        }
                        break;
                    case 'rename':
                        if ($this->getvar('old') && $this->getvar('new')) {
                            $response = $this->rename();
                        }
                        break;
                    case 'move':
                        // allow "../"
                        if ($this->getvar('old') && $this->getvar('new', 'parent_dir') && $this->getvar('root')) {
                            $response = $this->move();
                        }
                        break;
                    case 'editfile':
                        if ($this->getvar('path')) {
                            $response = $this->editfile();
                        }
                        break;
                    case 'delete':
                        if ($this->getvar('path')) {
                            $response = $this->delete();
                        }
                        break;
                    case 'addfolder':
                        if ($this->getvar('path') && $this->getvar('name')) {
                            $response = $this->addfolder();
                        }
                        break;
                    case 'download':
                        if ($this->getvar('path')) {
                            $response = $this->download();
                        }
                        break;
                    case 'preview':
                        if ($this->getvar('path')) {
                            if (isset($this->params['thumbnail'])) {
                                $thumbnail = true;
                            } else {
                                $thumbnail = false;
                            }
                            $response = $this->preview($thumbnail);
                        }
                        break;
                    case 'maxuploadfilesize':
                        $response = Response::api(['result' => $this->getMaxUploadFileSize()]);
                        break;
                }
            } elseif ($this->request->method() == 'POST') {
                switch ($this->params['mode']) {
                    default:
                        $response = Response::api(['result' => $this->error($this->lang('MODE_ERROR'))]);
                        break;
                    case 'add':
                        if ($this->postvar('currentpath')) {
                            $response = $this->add();
                        }
                        break;
                    case 'replace':
                        if ($this->postvar('newfilepath')) {
                            $response = $this->replace();
                        }
                        break;
                    case 'savefile':
                        if ($this->postvar('content', false) && $this->postvar('path')) {
                            $response = $this->savefile();
                        }
                        break;
                }
            }
        }
        //$result = json_encode($response);
        return $response;
    }

    private function sanitize($var, $preserve = null)
    {
        $sanitized = strip_tags($var);
        $sanitized = str_replace('http://', '', $sanitized);
        $sanitized = str_replace('https://', '', $sanitized);
        if ($preserve != 'parent_dir') {
            $sanitized = str_replace('../', '', $sanitized);
        }
        return $sanitized;
    }

    private function setPermissions()
    {
        $this->allowed_actions = $this->config['options']['capabilities'];
        if ($this->config['edit']['enabled']) array_push($this->allowed_actions, 'edit');
    }

    /**
     * @param string $path
     * @return mixed|string
     */
    private function getFullPath($path = '')
    {
        if ($path == '') {
            if (isset($this->get['path'])) $path = $this->get['path'];
        }
        if ($this->config['options']['fileRoot'] !== false) {
            $full_path = $this->doc_root . rawurldecode(str_replace($this->doc_root, '', $path));
            if ($this->dynamic_fileroot != '') {
                $full_path = $this->doc_root . rawurldecode(str_replace($this->dynamic_fileroot, '', $path));
            }
        } else {

            $full_path = storage_path() . '/data/' . rawurldecode($path);
        }
        $full_path = str_replace("//", "/", $full_path);
        $this->__log(" returned path : " . $full_path);
        return $full_path;
    }

    /**
     * @param $dir
     * @param bool $deleteRootToo
     */
    private function unlinkRecursive($dir, $deleteRootToo = true)
    {
        if (!$dh = @opendir($dir)) {
            return;
        }
        while (false !== ($obj = readdir($dh))) {
            if ($obj == '.' || $obj == '..') {
                continue;
            }
            if (!@unlink($dir . '/' . $obj)) {
                $this->unlinkRecursive($dir . '/' . $obj, true);
            }
        }
        closedir($dh);
        if ($deleteRootToo) {
            @rmdir($dir);
        }
        return;
    }

    /**
     * @param $path
     * @param $filename
     * @param string $i
     * @return mixed
     */
    private function checkFilename($path, $filename, $i = '')
    {
        if (!file_exists($path . $filename)) {
            return $filename;
        } else {
            $_i = $i;
            $tmp = explode(/*$this->config['upload']['suffix'] . */
                $i . '.', $filename);
            if ($i == '') {
                $i = 1;
            } else {
                $i++;
            }
            $filename = str_replace($_i . '.' . $tmp[(sizeof($tmp) - 1)], $i . '.' . $tmp[(sizeof($tmp) - 1)], $filename);
            return $this->checkFilename($path, $filename, $i);
        }
    }

    private function loadLanguageFile()
    {
        // we load langCode var passed into URL if present and if exists
        // else, we use default configuration var
        $lang = $this->config['options']['culture'];
        if (isset($this->params['langCode']) && in_array($this->params['langCode'], $this->languages)) $lang = $this->params['langCode'];
        if (file_exists($this->root . 'filemanager/scripts/languages/' . $lang . '.js')) {
            $stream = file_get_contents($this->root . 'filemanager/scripts/languages/' . $lang . '.js');
            $this->language = json_decode($stream, true);
        } else {
            $stream = file_get_contents($this->root . 'filemanager/scripts/languages/' . $lang . '.js');
            $this->language = json_decode($stream, true);
        }
    }

    private function availableLanguages()
    {
        if ($handle = opendir($this->root . 'filemanager/scripts/languages/')) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    array_push($this->languages, pathinfo($file, PATHINFO_FILENAME));
                }
            }
            closedir($handle);
        }
    }

    /**
     * @param $path
     * @return bool
     */
    private function is_image($path)
    {
        $a = getimagesize($path);
        $image_type = $a[2];
        if (in_array($image_type, array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP))) {
            return true;
        }
        return false;
    }

    /**
     * @param $file
     * @return bool
     */
    private function isEditable($file)
    {
        $path_parts = pathinfo($file);
        $exts = array_map('strtolower', $this->config['edit']['editExt']);
        if (in_array($path_parts['extension'], $exts)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $msg
     */
    private function __log($msg)
    {
        if ($this->logger == true) {
            $fp = fopen($this->logfile, "a");
            $str = "[" . date("d/m/Y h:i:s", time()) . "]#" . $this->getUserIP() . "#" . $msg;
            fwrite($fp, $str . PHP_EOL);
            fclose($fp);
        }
    }

    public function error($string, $textarea = false)
    {
        $array = array(
            'Error' => $string,
            'Code' => '-1',
            'Properties' => $this->properties
        );
        $this->__log(__METHOD__ . ' - error message : ' . $string);
        if ($textarea) {
            return '<textarea>' . json_encode($array) . '</textarea>';
        }
        return json_encode($array);
    }
}
