window.onload(getJSONFromServer());

// парсим url
function parseQueryString(strQuery)
{
    var strSearch   = strQuery.substr(1),
        strPattern  = /([^=]+)=([^&]+)&?/ig,
        arrMatch    = strPattern.exec(strSearch),
        objRes      = {};
    while (arrMatch != null) {
        objRes[arrMatch[1]] = arrMatch[2];
        arrMatch = strPattern.exec(strSearch);
    }
    return objRes;
}

// полуаем кросбраузерный объект XmlHttpRequest
function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

// получаем данные с сервера
function getJSONFromServer()
{
    var req   = getXmlHttp();
    var links = getTimeline(window.location.search);

    req.onreadystatechange = function() {
        if (req.readyState == 4) {

            if (req.status == 200) {
                // пришел ответ
                var returnArr = JSON.parse(req.responseText);

                replaceTag(returnArr.replacementTime, returnArr.currentTime);
            } else {
                // ошибки
                console.log("Ошибка ответа");
            }
        }
    };

    req.open('GET', 'http://localhost:8000/landing/scripts/timeLinks?'+links, false);

    req.send();

}

// получаем строчку timeline
function getTimeline(timeline)
{
    var queryStr = parseQueryString(timeline);
    var linksArr = queryStr['timeline'].split(",");

    if ( linksArr.length > 0 ) {
        var links = [];
        for ( var i = 0; i < linksArr.length; i++ ) {
            links.push("links[]="+linksArr[i]);
        }
    }

    return links.join("&");
}

// заменяем тэги в соответствии с временными метками
function replaceTag(arrOfLinkReplace, currentTime)
{
    var currentTags,
        meaning,
        start_time,
        end_time,
        currTime;

    currTime = new Date(currentTime.date);

    for (var i = 0; i < arrOfLinkReplace.length; i++) {
        currentTags = document.getElementsByTagName(arrOfLinkReplace[i].tag);
        meaning = arrOfLinkReplace[i].meaning;
        start_time  = +new Date(currTime.getFullYear()+"-"+(currTime.getMonth()+1)+"-"+currTime.getDate()+" "+arrOfLinkReplace[i].start_time);
        end_time    = +new Date(currTime.getFullYear()+"-"+(currTime.getMonth()+1)+"-"+currTime.getDate()+" "+arrOfLinkReplace[i].finish_time);

        // если попадаем во временной промежуток то заменяем
        if ((+currTime > start_time) && (+currTime < end_time)) {
            for (var j = 0; j < currentTags.length; j++) {
                currentTags[j].outerHTML = meaning;
            }
        }
    }
}