<?php
/*
 * Группы разрешений для вывода на странице
 * редактирования ролей.
 *
*/
return [
    '0' => [
        'color' => 'red',
        'value' => '0',
        'label' => 'Запрещено'
    ],
    '1' => [
        'color' => 'green',
        'value' => '1',
        'label' => 'Разрешено',
        'postfix' => ''
    ],
    '2' => [
        'color' => 'green',
        'value' => '2',
        'label' => 'Разрешено', // .all
        'postfix' => '.all'
    ],
    '3' => [
        'color' => 'blue',
        'value' => '3',
        'label' => 'Свои', // .their
        'postfix' => '.their'
    ],
    '4' => [
        'color' => 'green',
        'value' => '4',
        'label' => 'Редактирование',
        'postfix' => '.edit'
    ],
    '5' => [
        'color' => 'blue',
        'value' => '5',
        'label' => 'Просмотр',
        'postfix' => '.view'
    ],
];
