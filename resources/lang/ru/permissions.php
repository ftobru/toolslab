<?php

return [

    'owner' => [
        'display_name' => 'Владелец',
        'description' => 'Все права для данной компании'
    ],

    // -------- CRM -------------
    //Contacts permissions
    'crm.contacts.create' => [
        'display_name' => 'Создание',
        'description' => 'Возможность создавать новые контакты'
    ],
    'crm.contacts.view.all' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать контакты'
    ],
    'crm.contacts.view.their' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать контакты, если ответственный'
    ],
    'crm.contacts.edit.all' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать контакт'
    ],
    'crm.contacts.edit.their' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать только контакты, за которые ответственный'
    ],
    'crm.contacts.delete.all' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять контакты'
    ],
    'crm.contacts.delete.their' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять контакты, если ответственный'
    ],
    'crm.contacts.export.all' => [
        'display_name' => 'Экспорт',
        'description' => 'Возможность экспорта данных о контактах'
    ],

    // Firms
    'crm.firms.create' => [
        'display_name' => 'Создание',
        'description' => 'Возможность создавать новые контакты фирм'
    ],
    'crm.firms.view.all' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать контакты фирм'
    ],
    'crm.firms.view.their' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать контакты фирм, если ответственный'
    ],
    'crm.firms.edit.all' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать контакт фирмы'
    ],
    'crm.firms.edit.their' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать только контакты фирм, за которые ответственный'
    ],
    'crm.firms.delete.all' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять контакты'
    ],
    'crm.firms.delete.their' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять контакты фирм, если ответственный'
    ],
    'crm.firms.export.all' => [
        'display_name' => 'Экспорт',
        'description' => 'Возможность экспорта данных о контактах фирм'
    ],

    // Deals - заказы
    'crm.deals.create' => [
        'display_name' => 'Создание',
        'description' => 'Возможность создавать новые сделки'
    ],
    'crm.deals.view.all' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать сделки'
    ],
    'crm.deals.view.their' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать сделки, если ответственный'
    ],
    'crm.deals.edit.all' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать сделки'
    ],
    'crm.deals.edit.their' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать только сделки, за которые ответственный'
    ],
    'crm.deals.delete.all' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять сделки'
    ],
    'crm.deals.delete.their' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять сделки, если ответственный'
    ],
    'crm.deals.export.all' => [
        'display_name' => 'Экспорт',
        'description' => 'Возможность экспорта данных о сделках'
    ],

    // Tasks
    'crm.tasks.create' => [
        'display_name' => 'Создание',
        'description' => 'Возможность создавать новые задачи'
    ],
    'crm.tasks.view.all' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать сделки'
    ],
    'crm.tasks.view.their' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать задачи, если ответственный'
    ],
    'crm.tasks.edit.all' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать задачи'
    ],
    'crm.tasks.edit.their' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать только задачи, за которые ответственный'
    ],
    'crm.tasks.delete.all' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять задачи'
    ],
    'crm.tasks.delete.their' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять задачи, если ответственный'
    ],
    'crm.tasks.export.all' => [
        'display_name' => 'Экспорт',
        'description' => 'Возможность экспорта данных о задачах'
    ],

    // Products
    'crm.products.create' => [
        'display_name' => 'Создание',
        'description' => 'Возможность добавлять новые товары'
    ],
    'crm.products.view.all' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать товары'
    ],
    'crm.products.edit.all' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактировать товары'
    ],
    'crm.products.delete.all' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять товары'
    ],
    'crm.products.export.all' => [
        'display_name' => 'Экспорт',
        'description' => 'Возможность экспорта данных о товарах'
    ],
    'crm.products.prime' => [
        'display_name' => 'Выгода',
        'description' => 'Возможность просматривать себестоимость, выгоду товаров'
    ],

    // Users
    'crm.employees.view.all' => [
        'display_name' => 'Просмотр',
        'description' => 'Возможность просматривать список сотрудников'
    ],
    'crm.employees.invite' => [
        'display_name' => 'Приглашение',
        'description' => 'Возможность приглашать новых сотрудников'
    ],
    'crm.employees.edit' => [
        'display_name' => 'Правка',
        'description' => 'Возможность редактирования информации сотрудников'
    ],
    'crm.employees.delete' => [
        'display_name' => 'Удаление',
        'description' => 'Возможность удалять сотрудников из компании'
    ],
    'crm.employees.roles' => [
        'display_name' => 'Доступ к ролям',
        'description' => 'Возможность управлять ролями'
    ],

    // Settings
    'crm.settings.fields' => [
        'display_name' => 'Доп. поля',
        'description' => 'Возможность управлять доп. полями'
    ],
    'crm.settings.tags-statuses' => [
        'display_name' => 'Теги и статусы',
        'description' => 'Возможность управлять тегами и статусами'
    ],
    'crm.settings.modules' => [
        'display_name' => 'Модули',
        'description' => 'Возможность управлять модулями'
    ],
    'crm.settings.api' => [
        'display_name' => 'API',
        'description' => 'Доступ к API'
    ],
    'crm.settings.menu' => [
        'display_name' => 'Меню настроек',
        'description' => 'Доступ к меню настроек'
    ],

    // Analytics
    'crm.analytics.employee' => [
        'display_name' => 'По сотрудникам',
        'description' => 'Просмотр аналитики по сотрудникам'
    ],
    'crm.analytics.deals' => [
        'display_name' => 'По продажам',
        'description' => 'Просмотр аналитики по сотрудникам'
    ],
    'crm.analytics.menu' => [
        'display_name' => 'Меню аналитики',
        'description' => 'Отображение меню аналитики'
    ],

    // Dashboard
    'crm.dashboard.menu' => [
        'display_name' => 'Просмотр',
        'description' => 'Просмотр сводной информации о компании'
    ],

    // notifications
    'crm.notifications.new_lead.sms' => [
        'display_name' => 'Новый лид',
        'description' => 'Получение уведомлений о новых лидах по SMS'
    ],
    'crm.notifications.new_lead.email' => [
        'display_name' => 'Новый лид',
        'description' => 'Получение уведомлений о новых лидах по e-mail'
    ],
    'crm.notifications.new_resp.email' => [
        'display_name' => 'Назначен ответственный',
        'description' => 'Получение уведомлений о назначении ответственным по e-mail'
    ],
    'crm.notifications.overdue_task.sms' => [
        'display_name' => 'Просрочена задача',
        'description' => 'Получение уведомлений о просроченных задачах по SMS'
    ],
    // ------------- end of CRM ---------------

    // ------------- landingControl -----------

    // Creation
    'landingControl.create.links.edit' => [
        'display_name' => 'Ссылки',
        'description' => 'Редактирование ссылок'
    ],
    'landingControl.create.links.view' => [
        'display_name' => 'Ссылки',
        'description' => 'Просмотр ссылок'
    ],
    'landingControl.create.templates.edit' => [
        'display_name' => 'Шаблоны',
        'description' => 'Редактирование шаблонов'
    ],
    'landingControl.create.templates.view' => [
        'display_name' => 'Шаблоны',
        'description' => 'Просмотр шаблонов'
    ],
    'landingControl.create.forms.edit' => [
        'display_name' => 'Формы',
        'description' => 'Редактирование форм'
    ],
    'landingControl.create.forms.view' => [
        'display_name' => 'Формы',
        'description' => 'Просмотр форм'
    ],
    'landingControl.create.ab-test.edit' => [
        'display_name' => 'AB тесты',
        'description' => 'Редактирование тестов'
    ],
    'landingControl.create.ab-test.view' => [
        'display_name' => 'AB тесты',
        'description' => 'Просмотр AB тестов'
    ],
    'landingControl.create.geo-split.edit' => [
        'display_name' => 'GEO сплиты',
        'description' => 'Редактирование geo сплитов'
    ],
    'landingControl.create.geo-split.view' => [
        'display_name' => 'GEO сплиты',
        'description' => 'Просмотр geo сплитов'
    ],
    'landingControl.create.file-manager' => [
        'display_name' => 'Файловый менеджер',
        'description' => 'Доступ к файловому менеджеру'
    ],
    'landingControl.create.domains' => [
        'display_name' => 'Домены',
        'description' => 'Управление доменами'
    ],

    // Multylanding
    'landingControl.multylanding.product-replacement' => [
        'display_name' => 'Товар-замены',
        'description' => 'Управление товар-заменами'
    ],
    'landingControl.multylanding.time-replacement' => [
        'display_name' => 'Time-замены',
        'description' => 'Управление time-заменами'
    ],
    'landingControl.multylanding.geo-replacement' => [
        'display_name' => 'Geo-замены',
        'description' => 'Управление geo-заменами'
    ],
    'landingControl.multylanding.link-replacement' => [
        'display_name' => 'Link-замены',
        'description' => 'Управление link-заменами'
    ],
    'landingControl.multylanding.link-insert' => [
        'display_name' => 'Link-вставки',
        'description' => 'Управление link-вставками'
    ],
    'landingControl.multylanding.variables' => [
        'display_name' => 'Переменные',
        'description' => 'Управление переменными'
    ],

    // Аналитика


    // ----------end of landingControl --------

];
