<?php
/*
 * Группы разрешений для вывода на странице
 * редактирования ролей.
 *
*/
return [
    'crm' => [
        'display_name' => 'CRM',
        'module_key' => 'crm',

        'components' => [
            'products'=> [
                'label' => 'Товары'
            ],
            'firms' => [
                'label' => 'Компании'
            ],
            'contacts' => [
                'label' => 'Контакты'
            ],
            'deals' => [
                'label' => 'Заказы'
            ],
            'tasks' => [
                'label' => 'Задачи'
            ],
            'employees' => [
                'label' => 'Сотрудники'
            ],
            'settings' => [
                'label' => 'Настройки'
            ],
            'analytics' => [
                'label' => 'Аналитика'
            ],
            'dashboard' => [
                'label' => 'Dashboard'
            ],
            'notifications' => [
                'label' => 'Уведомления',
                'class' => 'notice'
            ],
        ]
    ],
    'landingControl' => [
        'display_name' => 'Страницы',
        'module_key' => 'landingControl',

        'components' => [
            'create' => [
                'label' => 'Создание'
            ],
            'multylanding' => [
                'label' => 'Мультилендинг'
            ]
        ]
    ]
];
