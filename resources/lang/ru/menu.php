<?php

return [
    'dashboard' => 'Dashboard',
    'orders' => 'Сделки',
    'tasks' => 'Задачи',
    'clients' => 'Контакты',


    'crm' => 'CRM',
    'products' => 'Товары',
    'templates' => 'Шаблоны',


    'landings' => 'Лендинги',

    'landing-links' => 'Ссылки',
    'landing-htmltemplates' => 'HTML шаблоны',

    'landing-create' => 'Создание',
    'landing-create-templates' => 'Шаблоны',
    'landing-create-forms' => 'Формы',
    'landing-create-abTest' => 'АБ-тест',
    'landing-create-geoSplit' => 'Гео-сплит',
    'landing-create-chain' => 'Цепочки',
    'landing-create-fileManager' => 'Файловый менеджер',
    'landing-create-domains' => 'Домены',

    'landing-multylanding' => 'Мультилендинг',
    'landing-multylanding-products' => 'Товар-замены',
    'landing-multylanding-params' => 'Переменные',
    'landing-multylanding-replacementTime' => 'Time-замены',
    'landing-multylanding-replacementGeo' => 'Geo-замены',
    'landing-multylanding-insertLinks' => 'Link-вставки',
    'landing-multylanding-replacementLinks' => 'Link-замены',
    'landing-multylanding-replacementProducts' => 'Продукт-замены',


    'stat' => 'Аналитика',

    'stat-crm-reportByUsers' => 'Отчет по сотрудникам',
    'stat-crm-saleReport' => 'Отчет по продажам',
    'stat-landings-stat' => 'Пункты',

    'settings-invoices' => 'Автоплатеж',
    'settings-companySettings' => 'Настройки компании',
    'settings-users' => 'Сотрудники',
    'settings-modules' => 'Модули',
    'settings-additionalFields' => 'Дополнительные поля',
    'settings-references' => 'Справочники',


    'replace' => 'Замены',
    'scripts' => 'Скрипты',
    'help' => 'Помощь',
    'docs' => 'База знаний',
    'faq' => 'FAQ и статьи',
    'support' => 'Тех поддержка',
    'settings' => 'Настройки',
    'general' => 'Общие настройки',
    'fields' => 'Доп поля',
    'tags' => 'Теги и статусы',
    'users' => 'Пользователи',
    'modules' => 'Модули',
    'api' => 'API'
];
