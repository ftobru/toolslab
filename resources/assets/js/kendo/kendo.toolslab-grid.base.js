var GridController = {};
var ResizeGrid = true;

function resizeGrid() {
    if (ResizeGrid) {
        setTimeout(function (e) {
            $('#grid').data("kendoGrid").resize();
        }, 400);
    }
}

// Копирование строки
function addNewCustomRow(e) {
    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));
    var idx = this.dataSource.indexOf(item);
    var data = {};
    $.each(GridConfig.copyFields, function (key, value) {
        data[value] = item[value];
    })
    var newRow = this.dataSource.insert(idx, data);
    this.editRow($('tr[data-uid=' + newRow.uid + ']'));
}

// Карточка сущности.
function gotoEntityCard(e) {
    e.preventDefault();
    var item = this.dataItem($(e.currentTarget).closest("tr"));
    if(!item) {
        item = this.dataItem($(GridController.selectedRows[0]));
    }
    var idx = this.dataSource.indexOf(item);
    var entityID = item.id;
    window.location.href = window.location.protocol + '//' + window.location.host + GridConfig.baseCardUrl + entityID;
}

if(GridConfig.editable) {
    GridConfig.columns.push({
        command: [
            {name: "edit", text: "", width: "32px", className: "md md-mode-edit"},
            {name: "destroy", text: "", width: "32px", className: "md md-delete"},
            {
                name: "copy",
                text: "",
                click: addNewCustomRow,
                className: "md md-content-copy",
                width: "32px"
            },
            {name: "card", text: "", click: gotoEntityCard, className: "md md-list", width: "32px"}
        ],
        title: "&nbsp;",
        width: "170px",
        attributes: {"class": "button-column"}
    });
}


Filters['firstInitFilters'] = function (data) {
    this.limits = data;
    this.initSlidersMaxMin();
    this.initDatesMaxMin();
    this.initDropDownListDataSource();
    var savedValues = FilterBuilder.getSavedFilterValues();
    if (savedValues) {
        this.setValuesBySavedData(savedValues);
    } else {
        this.setSliderValuesByLimits();
    }
    this.initSlidersAmount();
};

GridController = {
    initializedHiddenColumns: null,
    customGridToolsPrevState: 0, // 0-нет кнопок, 1-все, 2 - удалить, 3-ok,cancel
    autoSync: false,
    containerSelector: '.k-grid-container',
    container: null,
    headers: null,
    draggable: null,
    elem: '#grid',
    droptargetHiddenColumns: '#droptarget-hidden-columns',
    sortable: {
        mode: "multiple",
        allowUnsort: true
    },
    grid: {
        factory: {
            transport: {},
            dataSource: {}
        },
        dragColumn: {},
        defaults: {}
    },
    filter: [],

    // Шаблоны доп. кнопок
    editBtnTemplate: '<a id="custom-grid-tools-edit"  style="display: none;" class="k-button k-button-icontext md md-mode-edit k-grid-edit" href="#"><span class="k-icon k-edit"></span></a>',
    deleteBtnTemplete: '<a id="custom-grid-tools-delete"  style="display: none;" class="k-button k-button-icontext md md-delete k-grid-delete" href="#"><span class="k-icon k-delete"></span></a>',
    copyBtnTemplete: '<a id="custom-grid-tools-copy" style="display: none;" class="k-button k-button-icontext md md-content-copy k-grid-copy" href="#"><span class=" "></span></a>',
    cardBtnTemplete: '<a id="custom-grid-tools-card" style="display: none;" class="k-button k-button-icontext md md-list k-grid-card" href="#"><span class=" "></span></a>',

    okBtnTemplete: '<a id="custom-grid-tools-ok" style="display: none;" class="k-button k-button-icontext md md-check k-grid-card" href="#"><span class=" "></span></a>',
    cancelBtnTemplete: '<a id="custom-grid-tools-cancel" style="display: none;" class="k-button k-button-icontext md md-cancel k-grid-card" href="#"><span class=" "></span></a>',

    // id доп. кнопок
    customGridTools: [
        '#custom-grid-tools-edit',
        '#custom-grid-tools-delete',
        '#custom-grid-tools-copy',
        '#custom-grid-tools-card'
    ],

    buildFilterBuilder: function() {
        FilterBuilder['filters'] = {};
        FilterBuilder.filter_keys = GridConfig.filter_keys;
        FilterBuilder.filter_keys_hash = GridConfig.filter_keys_hash;
        FilterBuilder['addFilter'] = function (key, filter) {
            if ($.inArray(key, this.filter_keys) >= 0) {
                this.filters[key] = filter;
            }
        };
        FilterBuilder.removeFilter = function (key) {
            if ($.inArray(key, this.filter_keys) >= 0) {
                delete this.filters[key];
            }
        };
        FilterBuilder.applyFilters = function () {
            var filters = [];
            for (var j = 0; j < this.filter_keys.length; j++) {
                var key = this.filter_keys[j];
                if (this.filters.hasOwnProperty(key)) {
                    filters.push(this.filters[key]);
                }
            }
            GridController.filter = filters;
            GridController.redraw();
            localStorage["kendo-grid-filters" + GridConfig.storage_postfix] = kendo.stringify(filters);
        };
        FilterBuilder.getSavedFilterValues = function () {
            var hashedFilters = this.tryToLoadFiltersFromHash();
            if (hashedFilters != false) {
                return hashedFilters;
            }
            var filtersValues = localStorage["kendo-grid-filters" + GridConfig.storage_postfix];
            if (filtersValues) {
                return JSON.parse(filtersValues);
            }
            return false;
        };
        FilterBuilder.tryToLoadFiltersFromHash = function () {
            var self = this;
            var filters = {};
            if (window.location.hash != '') {
                try {
                    var encoded = JSON.parse(decodeURIComponent(window.location.hash.substr(1)));
                    if ('filters' in encoded) {
                        $.each(encoded.filters, function (key, value) {
                            if ($.inArray(value.field, GridConfig.filter_keys_hash) >= 0) {
                                filters[self.keyGenerator(value)] = value;
                            }
                        });
                        return filters;
                    }
                } catch (e) {
                    console.log(e.name);
                }
            }
            return false;
        };
    },

    init: function () {
        this.tplColumn = _.template('<span class="primary-bright" data-field="<%=field%>" style="padding: 3px; border: 1px solid #c5c5c5; border-radius: 4px; cursor: pointer;"><i class="md md-view-column"></i> <%=(title || field)%></span>');
        if (localStorage["kendo-grid-ver"] && parseInt(localStorage["kendo-grid-ver"]) < GridConfig.version) {
            var last_postfix = GridConfig.postfixPath + parseInt(localStorage["kendo-grid-ver"]);
            localStorage.removeItem("kendo-grid-filters" + last_postfix);
            localStorage.removeItem("kendo-grid-options" + last_postfix);
        }
        GridConfig.storage_postfix = GridConfig.postfixPath + GridConfig.version;
        localStorage["kendo-grid-ver"] = GridConfig.version;
        this.buildFilterBuilder();
        Filters.initFilters();
    },

    initGrid: function () {
        var self = this;

        var serverPaging = true;
        var serverSorting = true;

        if(typeof GridConfig.noServerPaging !== 'undefined' && GridConfig.noServerPaging == true ){
            serverPaging = false;
        }

        if(typeof GridConfig.noServerSorting !== 'undefined' && GridConfig.noServerSorting == true ){
            serverSorting = false;
        }

        var dataSource = {
            serverPaging: serverPaging,
            serverSorting: serverSorting,
            serverFiltering: true,
            serverGrouping: GridConfig.groupable,
            allowUnsort: true,
            type: "json",
            transport: {
                read: function (e) {
                    self.selectedRows = [];
                    self.setControlButtonsState();
                    GridConfig.gridPagerConfig.page = $(GridController.elem)
                        .data("kendoGrid")
                        .dataSource.page();
                    if (e.data.filter) {
                        window.location.hash = encodeURIComponent(JSON.stringify(e.data.filter));
                    } else {
                        window.location.href.split('#')[0];
                    }
                    $.ajax({
                        url: GridConfig.baseGridCrudUrl + "get",
                        method: "POST",
                        dataType: "json",
                        data: {
                            options: e.data
                        },
                        context: this,
                        success: function (result, status, xhr) {
                            self.setControlButtonsState();
                            if (!Filters.isInit) {
                                Filters.firstInitFilters(result.filters_data);
                                Filters.isInit = true;
                            } else {
                                Filters.updateSliderLimits(result.filters_data);
                            }
                            e.success(JSON.parse(JSON.stringify(result)));
                        },
                        error: function (xhr, status, error) {
                            e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                        }
                    });
                },
                update: function (e) {
                    var data;
                    if (GridController.autoSync) {
                        data = {model: e.data};
                    } else {
                        data = {models: e.data.models};
                    }
                    $.ajax({
                        url: GridConfig.baseGridCrudUrl + "update",
                        method: "PUT",
                        data: data,
                        context: this,
                        success: function (result, status, xhr) {
                            self.setControlButtonsState();
                            e.success(JSON.parse(JSON.stringify(result)));
                        },
                        error: function (xhr, status, error) {
                            e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                        }
                    });
                },
                destroy: function (e) {
                    var data;
                    if (GridController.autoSync) {
                        data = {model: e.data};
                    } else {
                        data = {models: e.data.models};
                    }
                    $.ajax({
                        url: GridConfig.baseGridCrudUrl + "delete",
                        method: "POST",
                        data: data,
                        context: this,
                        success: function (result, status, xhr) {
                            self.setControlButtonsState();
                            e.success(JSON.parse(JSON.stringify(result)));
                        },
                        error: function (xhr, status, error) {
                            e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                        }
                    });
                },
                create: function (e) {
                    var data;
                    if (GridController.autoSync) {
                        data = {model: e.data};
                    } else {
                        data = {models: e.data.models};
                    }
                    $.ajax({
                        url: GridConfig.baseGridCrudUrl + "create",
                        method: "POST",
                        data: data,
                        context: this,
                        success: function (result, status, xhr) {
                            self.setControlButtonsState();
                            e.success(JSON.parse(JSON.stringify(result)));
                        },
                        error: function (xhr, status, error) {
                            e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                        }
                    });
                }
            },
            batch: true,
            schema: {
                data: 'data',
                total: "total",
                groups: "groups",
                model: GridConfig.model
            },
            aggregate: GridConfig.aggregateFields,
            filter: this.filter,
            pageSize: GridConfig.gridPagerConfig.perPage,
            page: GridConfig.gridPagerConfig.page
        };
        var toolbar;
        var editMode;
        if(GridConfig.editable) {
            toolbar = [
                "create",
                "excel",
                "pdf",
                {name: "print", text: "Печать"}
            ];
            editMode = 'inline';
        } else {
            toolbar = [
                "excel",
                "pdf",
                {name: "print", text: "Печать"}
            ];
            editMode = false;
        }

        var grid = $(this.elem).kendoGrid({
            mobile: true,
            dataSource: dataSource,
            groupable: GridConfig.groupable,
            selectable: "multiple cell",
            columnMenu: true,
            allowCopy: true,
            toolbar: toolbar,
            pdf: {
                allPages: true,
                fileName: "toolslab-" + GridConfig.gridName + ".pdf",
                proxyURL: "http://demos.telerik.com/kendo-ui/service/export",
                author: "ToolsLab CRM",
                date: new Date(),
                keywords: "toolslab, " + GridConfig.gridName,
                landscape: false,
                margin: {
                    left: "1in",
                    right: "2cm",
                    top: "1in",
                    bottom: "4cm"
                },
                paperSize: "A4"
            },
            height: 650,
            scrollable: true,
            sortable: this.sortable,
            reorderable: true,
            editable: editMode, // true - for in-grid edit
            resizable: true,
            columnResize: function (e) {
                self.saveColumnsState();
            },
            columnReorder: function (e) {
                self.saveColumnsState();
            },
            change: function (e) {
                var selectedRows = this.select();
                var allSelectedRows = $(selectedRows).closest("tr");
                if (selectedRows.length > 1) {
                    $.each(self.selectedRows, function (index, value) {
                        $(value).removeClass('k-state-selected');
                        $('tr[data-uid="' + value.data().uid + '"]').removeClass('k-state-selected');
                    });
                    self.selectedRows = [];
                }
                self.setControlButtonsState();
            },
            cancel: function (e) {
                self.selectedRows = [];
                self.setControlButtonsState();
                this.refresh();
            },
            pageable: {
                input: true,
                numeric: true,
                pageSize: GridConfig.gridPagerConfig.perPage,
                pageSizes: [25, 100, 250]
            },
            columns: this.getColumnsState(),
            columnMenuInit: function (e) {
                var columnsItem = $(e.container[0]).find('li.k-columns-item');
                $(columnsItem).next().remove();
                $(columnsItem).remove();
            }
        }).data("kendoGrid");
        $('.k-button.k-button-icontext.k-grid-add').click(function() {
            self.showConfirmButtons();
        });
        $(grid.tbody).on("click", "td", function (e) {
            var row = $(this).closest("tr");
            if ($(row).attr('data-role') == 'editable') {
                self.showConfirmButtons();
                return;
            }
            var grid = $('#grid').data('kendoGrid');
            var unselect = false;
            $.each(self.selectedRows, function (key, value) {
                if (value && value.is(row)) {
                    self.selectedRows.splice(key, 1);
                    $(row).removeClass('k-state-selected');
                    $('tr[data-uid="' + row.data().uid + '"]').removeClass('k-state-selected');
                    unselect = true;
                }
            });
            if (unselect) {
                $(this).removeClass('k-state-selected');
                $('tr[data-uid="' + row.data().uid + '"]').removeClass('k-state-selected');
                self.setControlButtonsState();
                return;
            }
            if (!e.ctrlKey) {
                $.each(self.selectedRows, function (index, value) {
                    $(value).removeClass('k-state-selected');
                    $('tr[data-uid="' + value.data().uid + '"]').removeClass('k-state-selected');
                });
                grid.clearSelection();
                self.selectedRows = [];
            }
            self.selectedRows.push(row);
            grid.select(row);
            $(this).removeClass('k-state-selected');
            grid.clearSelection();
            var rowIdx = $("tr", grid.tbody).index(row);
            var colIdx = $("td", row).index(this);
            var item = grid.dataItem(row);
            var id = item.Id;
            self.setControlButtonsState();
        });
        if (!$('#grid tbody').first().is($(grid.tbody))) {
            $('#grid tbody').first().on("click", "td", function (e) {
                var row = $(this).closest("tr");
                if ($(row).attr('data-role') == 'editable') {
                    self.showConfirmButtons();
                    return;
                }
                var grid = $('#grid').data('kendoGrid');
                var unselect = false;
                $.each(self.selectedRows, function (key, value) {
                    if (value && value.is(row)) {
                        self.selectedRows.splice(key, 1);
                        $(row).removeClass('k-state-selected');
                        $('tr[data-uid="' + row.data().uid + '"]').removeClass('k-state-selected');
                        unselect = true;
                    }
                });
                if (unselect) {
                    $(this).removeClass('k-state-selected');
                    $('tr[data-uid="' + row.data().uid + '"]').removeClass('k-state-selected');
                    self.setControlButtonsState();
                    return;
                }
                if (!e.ctrlKey) {
                    $.each(self.selectedRows, function (index, value) {
                        $(value).removeClass('k-state-selected');
                        $('tr[data-uid="' + value.data().uid + '"]').removeClass('k-state-selected');
                    });
                    grid.clearSelection();
                    self.selectedRows = [];
                }
                self.selectedRows.push(row);
                grid.select(row);
                $(this).removeClass('k-state-selected');
                grid.clearSelection();
                var rowIdx = $("tr", grid.tbody).index(row);
                var colIdx = $("td", row).index(this);
                var item = grid.dataItem(row);
                var id = item.Id;
                self.setControlButtonsState();
            });
        }
        this.grid.grid = grid;
        $('body').on('grid:drag:start', this.onGridDragStart);
        this.render();
        this.initDraggable();
        self.columnsDrop.html('');
        $.each(this.initializedHiddenColumns, function (index, value) {
            var col = $(self.tplColumn(value));
            self.columnsDrop.append(col);
            col.kendoDraggable({
                group: self.grid.draggableGroup,
                hint: function (element) {
                    return element.clone();
                }
            });
        });
        this.initPrintButton();
    },

    showConfirmButtons: function () {
        if (!GridConfig.editable) return;
        var self = this;
        if (this.customGridToolsPrevState != 3) {
            if (!$('#custom-grid-tools').length) {
                $(".k-header.k-grid-toolbar").append('<div id="custom-grid-tools"></div>')
            }
            var toolsDiv = $('#custom-grid-tools');
            toolsDiv.html('')
                .append(this.okBtnTemplete)
                .append(this.cancelBtnTemplete)
            $('#custom-grid-tools-cancel').show('slow');
            $('#custom-grid-tools-ok').show('slow');


            $('#custom-grid-tools-cancel').on('click', function (e) {
                $('#grid').data("kendoGrid").cancelChanges();
                self.selectedRows = [];
                self.setControlButtonsState();
            });

            $('#custom-grid-tools-ok').on('click', function (e) {
                $('#grid').data('kendoGrid').dataSource.sync();
                self.selectedRows = [];
                self.setControlButtonsState();
            });
            this.customGridToolsPrevState = 3;
        }
    },

    setControlButtonsState: function () {
        if (!GridConfig.editable) return;
        var self = this;
        var grid = $('#grid').data('kendoGrid');
        var row = this.selectedRows[0];
        var rowIdx = $("tr", grid.tbody).index(row);
        var colIdx = $("td", row).index(this);
        var item = grid.dataItem(row);

        if (!$('#custom-grid-tools').length) {
            $(".k-header.k-grid-toolbar").append('<div id="custom-grid-tools"></div>')
        }

        if (this.selectedRows.length == 1) {

            if (this.customGridToolsPrevState != 1) {
                var toolsDiv = $('#custom-grid-tools');
                toolsDiv.html('')
                    .append(this.editBtnTemplate)
                    .append(this.deleteBtnTemplete)
                    .append(this.copyBtnTemplete)
                    .append(this.cardBtnTemplete);

                $.each(this.customGridTools, function (key, value) {
                    $(value).show('slow');
                });
            }
            $.each(this.customGridTools, function (key, value) {
                $(value).unbind('click');
            });

            $('#custom-grid-tools-delete').on('click', function (e) {
                grid.removeRow(row);
                self.selectedRows = [];
                $.each(self.customGridTools, function (key, value) {
                    $(value).hide('fast');
                });
            });

            $('#custom-grid-tools-edit').on('click', function (e) {
                grid.editRow(row);
                self.showConfirmButtons();
            });

            $('#custom-grid-tools-card').on('click', function (e) {
                var entityID = item.id;
                window.location.href = window.location.protocol + '//' + window.location.host + GridConfig.baseCardUrl + entityID;
            });

            $('#custom-grid-tools-copy').on('click', function (e) {
                var a = {};
                var data = {};
                $.each(GridConfig.copyFields, function (key, value) {
                    data[value] = item[value];
                });
                $.each(self.customGridTools, function (key, value) {
                    $(value).hide('fast');
                    $(value).removeClass('k-state-selected');
                });
                var newRow = grid.dataSource.insert(rowIdx, data);
                var some = $("#grid").data("kendoGrid").table.find('tr[data-uid="' + newRow.uid + '"]');
                $("#grid").data("kendoGrid").select(some);
                self.selectedRows = [some];
                grid.editRow($('tr[data-uid=' + newRow.uid + ']'));
                self.showConfirmButtons();
            });
            this.customGridToolsPrevState = 1;
        }
        if (this.selectedRows.length == 0) {
            $('#custom-grid-tools').html('');
            this.customGridToolsPrevState = 0;
        }
        if (this.selectedRows.length > 1) {
            if (this.customGridToolsPrevState != 2) {
                var toolsDiv = $('#custom-grid-tools');
                toolsDiv.html('')
                    .append(this.deleteBtnTemplete);
                $('#custom-grid-tools-delete').show('slow');
            }
            $('#custom-grid-tools-delete').unbind('click');
            $('#custom-grid-tools-delete').on('click', function (e) {
                $.each(self.selectedRows, function (key, value) {
                    grid.removeRow(value);
                });
                self.selectedRows = [];
                $.each(self.customGridTools, function (key, value) {
                    $(value).hide('fast');
                });
            });
            this.customGridToolsPrevState = 2;
        }
    },

    initPrintButton: function () {
        var self = this;
        $('.k-header .k-button.k-grid-print').click(function (e) {
            self.printGrid();
        });
    },

    redraw: function () {
        var ds = $("#grid").data("kendoGrid").dataSource;
        ds.filter(this.filter);
        $('#grid').data('kendoGrid').dataSource.read();
    },

    onGridDragStart: function (data, elem) {
        if (!!GridController.grid.dragColumn) {
            delete GridController.grid.dragColumn;
        }
        GridController.grid.dragColumn = elem;
    },

    render: function () {
        this.container = $(this.containerSelector);
        this.columnsDrop = $('.columns-drop');
        this.grid.grid = this.container.data('kendoGrid');
        this.grid.draggable = this.container.data('kendoDraggable');
        this.grid.headers = $('.k-grid-container th');
        this.grid.draggableGroup = $(this.grid.headers.get(0)).data('kendoDropTarget').options.group;
        return this;
    },

    printGrid: function () {
        var gridElement = $(this.elem);
        printableContent = '';
        win = window.open('', '', 'width=800, height=500');
        doc = win.document.open();

        var htmlStart =
            '<!DOCTYPE html>' +
            '<html>' +
            '<head>' +
            '<meta charset="utf-8" />' +
            '<title>Kendo UI Grid</title>' +
            '<link href="http://cdn.kendostatic.com/' + kendo.version + '/styles/kendo.common.min.css" rel="stylesheet" /> ' +
            '<style>' +
            'html { font: 11pt sans-serif; }' +
            '.k-grid { border-top-width: 0; }' +
            '.k-grid, .k-grid-content { height: auto !important; }' +
            '.k-grid-content { overflow: visible !important; }' +
            'div.k-grid table { table-layout: auto; width: 100% !important; }' +
            '.k-grid .k-grid-header th { border-top: 1px solid; }' +
            '.k-grid-toolbar, .k-grouping-header, .k-grid-pager > .k-link { display: none; }' +
            '.k-pager-wrap.k-grid-pager.k-widget { display: none; }' +
            'td.button-column {display: none;}' +
            'th.k-header:not([role]) {display: none;}' +
            'td a.k-button {display: none;}' +
            '</style>' +
            '</head>' +
            '<body>';

        var htmlEnd =
            '</body>' +
            '</html>';

        var gridHeader = gridElement.children('.k-grid-header');
        if (gridHeader[0]) {
            var thead = gridHeader.find('thead').clone().addClass('k-grid-header');
            printableContent = gridElement
                .clone()
                .children('.k-grid-header').remove()
                .end()
                .children('.k-grid-content')
                .find('table')
                .first()
                .children('tbody').before(thead)
                .end()
                .end()
                .end()
                .end()[0].outerHTML;
        } else {
            printableContent = gridElement.clone()[0].outerHTML;
        }

        doc.write(htmlStart + printableContent + htmlEnd);
        doc.close();
        setTimeout(
            function () {
                win.print();
            }, 1500);

    },

    initDraggable: function () {
        var self = this;
        this.initColumnsDropArea(self.grid.draggableGroup);
        this.container.kendoDropTarget({
            group: this.grid.draggableGroup,
            drop: function (e) {
                var $field = $(e.draggable.element);
                var fieldName = $field.data('field');

                if (!!fieldName) {
                    self.grid.grid.showColumn(fieldName);
                    $field.remove();
                    self.saveColumnsState();
                }
            }
        });

        $("#grid").data("kendoDraggable").bind('dragstart', function (e) {
            var data = $(e.currentTarget).data();
            if (data.field) {
                $('body').trigger('grid:drag:start', {
                    element: e.currentTarget,
                    field: data.field,
                    index: data.index,
                    title: data.title
                });
            }
        });
    },

    getColumnsState: function () {
        var options = localStorage["kendo-grid-options" + GridConfig.storage_postfix];
        if (options) {
            var opt = JSON.parse(options);
            options = this.merge_options_arrays(GridConfig.columns, opt.columns);
        } else {
            options = GridConfig.columns;
        }
        this.fillHiddenColumns(options);
        return options;
    },

    merge_options_arrays: function (arr1, arr2) {
        if (GridConfig.existedFields) {
            $.each(arr2, function (key, value) {
                if(value && value.hasOwnProperty('field')){
                    if ($.inArray(value.field, GridConfig.existedFields) === -1) {
                        arr2.splice(key , 1);
                    }
                }
            });
        }
        var arr3 = [];
        for (var i in arr1) {
            var shared = false;
            for (var j in arr2)
                if (arr2[j].field == arr1[i].field) {
                    if(arr1[i].hasOwnProperty('command')) {
                        arr3.push(arr1[i]);
                    } else {
                        var merged_obj = this.merge_options(arr1[i], arr2[j]);
                        arr3.push(merged_obj);
                    }
                    break;
                }
        }
        return arr3;
    },

    merge_options: function (obj1, obj2) {
        var obj3 = {};
        for (var attrname in obj1) {
            obj3[attrname] = obj1[attrname];
        }
        for (var attrname in obj2) {
            obj3[attrname] = obj2[attrname];
        }
        return obj3;
    },

    fillHiddenColumns: function (columns) {
        var self = this;
        self.columnsDrop = $('.columns-drop');
        this.initializedHiddenColumns = [];
        $.each(columns, function (index, value) {
            if (value.hasOwnProperty('hidden') && value.hidden) {
                if (value.hasOwnProperty('field')) {
                    var $column = $(self.tplColumn(value));
                    self.initializedHiddenColumns.push(value);
                }
            }
        });
    },

    saveColumnsState: function () {
        setTimeout(function () {
            localStorage["kendo-grid-options" + GridConfig.storage_postfix] = kendo.stringify(GridController.grid.grid.getOptions());
        }, 1);
    },

    initColumnsDropArea: function (group) {
        var self = this;

        this.columnsDrop.kendoDropTarget({
            group: group,
            drop: function (e) {
                if (!!self.grid.dragColumn && !$.isEmptyObject(self.grid.dragColumn)) {
                    var arr = $('#grid').data('kendoGrid').columns;
                    arr = $.grep(arr, function (a) {
                        return a.field == self.grid.dragColumn.field;
                    });
                    if (arr.length === 1) {
                        var columnFromTable = arr[0];
                        if (!columnFromTable.hidden && columnFromTable.hasOwnProperty('field')) {
                            self.grid.grid.hideColumn(self.grid.dragColumn.field);
                            var $column = $(self.tplColumn(self.grid.dragColumn));
                            self.initColumnDrag.call(self, $column);
                            self.saveColumnsState();
                        }
                    }
                }
            },
            dragenter: function (e) {
                if (self.grid.dragColumn.field) {
                    $(e.toElement).children('span').removeClass('k-denied').addClass('k-add');
                }
            },
            dragleave: function (e) {
                $(e.toElement).children('span').removeClass('k-add').addClass('k-denied');
            }
        });

        _.chain(GridConfig.columns)
            .filter(function (col) {
                return _.has(col, 'hidden') && col.hidden;
            })
            .each(function (col) {
                var $column = $(this.tplColumn(col));
                this.initColumnDrag.call(this, $column);
            }, this);
    },

    initColumnDrag: function (column) {
        this.columnsDrop.append(column);
        column.kendoDraggable({
            group: this.grid.draggableGroup,
            hint: function (element) {
                return element.clone();
            }
        });
    }
};

var StateManager = {
    stateMenu: false,
    width: null,
    stateFilter: null,
    stateContent: null,
    currentMenuWidth: 0,
    currentFilterLeft: 0,
    currentContentPaddingLeft: 0,
    isContentMoveable: false,
    isMobile: false,


    filterWidth: 320,
    // Левое меню - ПК  большой экран
    closeMenuWidthPCBig: 60,
    openMenuWidthPCBig: 240,
    // Левое меню - ПК   маленький экран
    closeMenuWidthPCSmall: 0,
    openMenuWidthPCSmall: 240,
    // Левое меню - Mobile  большой экран
    closeMenuWidthMobBig: 60,
    openMenuWidthMobBig: 240,
    // Левое меню - Mobile  min экран
    closeMenuWidthMobSmall: 0,
    openMenuWidthMobSmall: 240,

    prevState: null,


    setStateMenu: function () {
        resizeGrid();
        this.stateMenu ? this.openMenu() : this.closeMenu();
    },

    setWidthWindow: function (width) {

    },

    setFilterState: function () {
        resizeGrid();
        this.stateFilter ? this.openFilter() : this.closeFilter();
    },

    setContentState: function () {
        if (this.isContentMoveable) {
            //this.stateContent ? this.openContent() : this.closeContent();
            var padding = 0;// this.currentMenuWidth;
            if (this.stateFilter) {
                padding += this.filterWidth;
            }
            $('#content section.style-default-bright').animate({paddingLeft: padding + "px"}, 200);
        } else {
            $('#content section.style-default-bright').animate({paddingLeft: "0px"}, 200);
        }
    },

    initialDefaultState: function (mobile) {
        if (mobile == null) {
            this.isMobile = false;
        } else {
            if (this.isBigDisplay()) {
                $('#content section.style-default-bright').addClass('mob-max-content');
                $('#off-canvas1').addClass('mob-max-filter');
            } else {
                //$('#content section.style-default-bright').addClass('mob-min-content');
                //$('#off-canvas1').addClass('mob-min-filter');
            }


            this.isMobile = true;
        }

        var state;

        if (!this.isMobile) {
            if (this.isBigDisplay()) {
                this.stateMenu = false;
                this.stateFilter = true;
                this.stateContent = true;
                this.isContentMoveable = true;

                state = 0;

            } else {
                this.stateMenu = false;
                this.stateFilter = false;
                this.stateContent = false;
                this.isContentMoveable = false;

                state = 1;

            }
        } else {
            if (this.isBigDisplay()) {
                this.stateMenu = false;
                this.stateFilter = false;
                this.stateContent = false;
                this.isContentMoveable = false;

                state = 2;

            } else {
                this.stateMenu = false;
                this.stateFilter = false;
                this.stateContent = false;
                this.isContentMoveable = false;

                state = 3;

            }

            if (state != this.prevState) {
                switch (state) {
                    case 0:

                        break;
                    case 1:
                        this.closeFilter();
                        this.closeMenu();
                        break;
                }
            }
            this.prevState = state;
        }

        if (!this.isMobile) {
            if (this.isBigDisplay()) {
                this.currentMenuWidth = this.openMenuWidthPCBig;
            } else {
                this.currentMenuWidth = this.openMenuWidthPCSmall;
            }
        } else {
            if (this.isBigDisplay()) {
                this.currentMenuWidth = this.closeMenuWidthMobBig;
            } else {
                this.currentMenuWidth = this.openMenuWidthMobSmall;
            }
        }
        this.init();
    },

    isBigDisplay: function () {
        return $(window).width() > 768;
    },

    openMenu: function () {
        this.currentMenuWidth = this.closeMenuWidthPCBig;
        if (!this.isMobile) {
            if (this.isBigDisplay()) {
                this.currentMenuWidth = this.openMenuWidthPCBig;
            } else {
                this.currentMenuWidth = this.openMenuWidthPCSmall;
            }
        } else {
            if (this.isBigDisplay()) {
                this.currentMenuWidth = this.closeMenuWidthMobBig;
            } else {
                this.currentMenuWidth = this.openMenuWidthMobSmall;
            }
        }

        this.stateMenu = true;
    },

    closeMenu: function () {
        this.currentMenuWidth = this.openMenuWidthPCBig;
        if (!this.isMobile) {
            if (this.isBigDisplay()) {
                this.currentMenuWidth = this.closeMenuWidthPCBig;
            } else {
                this.currentMenuWidth = this.closeMenuWidthPCSmall;
            }
        } else {
            if (this.isBigDisplay()) {
                this.currentMenuWidth = this.closeMenuWidthMobBig;
            } else {
                this.currentMenuWidth = this.closeMenuWidthMobSmall;
            }
        }

        this.stateMenu = false;
        // animate
    },

    closeFilter: function () {
        this.currentFilterLeft = 0;
        this.currentFilterLeft = -320;
        $('#off-canvas1').animate({left: this.currentFilterLeft + "px"}, 200);
        this.stateFilter = false;
    },

    openFilter: function () {
        this.currentFilterLeft = 0;
        if (!this.isMobile) {
            if (this.isBigDisplay()) {
                this.currentFilterLeft = this.currentMenuWidth;
            } else {
                this.currentFilterLeft = 0;
            }
        } else {
            if (this.isBigDisplay()) {
                this.currentFilterLeft = this.currentMenuWidth;
            } else {
                this.currentFilterLeft = this.closeMenuWidthMobSmall;
            }
        }

        $('#off-canvas1').animate({left: this.currentFilterLeft + "px"}, 0);
        this.stateFilter = true;
    },

    init: function () {
        this.setStateMenu();
        this.setFilterState();
        this.setContentState();
        resizeGrid();
    }
};
var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

StateManager.initialDefaultState(isMobile.any());

$('#trigger').on('click', function () {
    StateManager.stateFilter = (!StateManager.stateFilter);
    StateManager.init();
});

$('a[data-toggle="menubar"]').on('click', function () {
    StateManager.stateMenu = (!StateManager.stateMenu);
    StateManager.init();
});

$(window).resize(function () {
    var windowSize = $(window).width();
    StateManager.width = windowSize;
    StateManager.initialDefaultState(isMobile.any());
    StateManager.init();
    resizeGrid();
});

//# sourceURL=grid.js