/**
 * Created by office on 03.07.15.
 */


(function( global ) {
    var CardModule = (function(){
        var endpoint = '/crm/card/',
            self = this;
        return {
            attachFirm: function(clientIds, firmId) {
                $.ajax({
                    url: endpoint + 'ajaxAttachFirm',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        clientIds: clientIds,
                        firmId: firmId
                    }
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            attachClient: function(clientId, firmId) {
                $.ajax({
                    url: endpoint + 'ajaxAttachClient',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        clientId: clientId,
                        firmId: firmId
                    }
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            createClient: function(data) {
                $.ajax({
                    url: endpoint + 'ajaxCreateClient',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        data: data
                    }
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            createFirm: function(data) {
                $.ajax({
                    url: endpoint + 'ajaxCreateFirm',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        data: data
                    }
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            getOrdersByFirmId: function(id) {
                $.ajax({
                    url: endpoint + 'ajaxGetOrdersByFirm/' + id,
                    type: 'GET',
                    dataType: 'json'
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            getOrdersByClientId: function(id) {
                $.ajax({
                    url: endpoint + 'ajaxGetOrdersByClientId/' + id,
                    type: 'GET',
                    dataType: 'json'
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            getTasksByClientId: function(id) {
                $.ajax({
                    url: endpoint + 'ajaxGetTasksByClientId/' + id,
                    type: 'GET',
                    dataType: 'json'
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            getTasksByFirmId: function(id) {
                $.ajax({
                    url: endpoint + 'ajaxGetTasksByFirm/' + id,
                    type: 'GET',
                    dataType: 'json'
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            getStatisticsByFirmId: function(id) {
                $.ajax({
                    url: endpoint + 'ajaxGetStatisticsByFirm/' + id,
                    type: 'GET',
                    dataType: 'json'
                }).error(function(e) {
                    console.log(e);
                }).success(function(response){
                    console.log(response)
                });
            },

            updateClient: function(id, data) {
                $.ajax({
                    url: endpoint + 'ajaxUpdateClient/' + id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        data: data
                    }
                }).error(function(e){
                    console.log(e);
                }).success(function(response) {
                    console.log(response);
                });
            },

            updateFirm: function(id, data) {
                $.ajax({
                    url: endpoint + 'ajaxUpdateFirm/' + id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        data: data
                    }
                }).error(function(e){
                    console.log(e);
                }).success(function(response){
                    console.log(response);
                });
            },

            updateOrder: function(id, data) {
                $.ajax({
                    url: endpoint + 'ajaxUpdateOrder/' + id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        data: data
                    }
                }).error(function(e){
                    console.log(e);
                }).success(function(response){
                    console.log(response);
                });
            },

            updatesTask: function(id, data) {
                $.ajax({
                    url: endpoint + 'ajaxUpdateTask/' + id,
                    type: 'PUT',
                    dataType: 'json',
                    data: {
                        data: data
                    }
                })
            }
        };

    })();

    global.CardModule = CardModule;
})( this );

(function( global ){
    var StateManager = (function() {
        return {
            stateMenu: false,
            width: null,
            stateFilter: null,
            stateContent: null,
            currentMenuWidth: 0,
            currentFilterLeft: 0,
            currentContentPaddingLeft:  0,
            isContentMoveable: false,
            isMobile: false,



            filterWidth: 320,
            // Левое меню - ПК  большой экран
            closeMenuWidthPCBig: 60,
            openMenuWidthPCBig: 240,
            // Левое меню - ПК   маленький экран
            closeMenuWidthPCSmall: 0,
            openMenuWidthPCSmall: 240,
            // Левое меню - Mobile  большой экран
            closeMenuWidthMobBig: 60,
            openMenuWidthMobBig: 240,
            // Левое меню - Mobile  min экран
            closeMenuWidthMobSmall: 0,
            openMenuWidthMobSmall: 240,

            prevState : null,



            setStateMenu: function() {
                this.stateMenu ? this.openMenu() : this.closeMenu();
            },

            setWidthWindow : function(width) {

            },

            setFilterState: function() {
                this.stateFilter ? this.openFilter() : this.closeFilter();
            },

            setContentState: function() {
                if (this.isContentMoveable) {
                    //this.stateContent ? this.openContent() : this.closeContent();
                    var padding = 0;// this.currentMenuWidth;
                    if (this.stateFilter) {
                        padding += this.filterWidth;

                    }
                    console.log(padding);
                    $('#content section.style-default-bright').animate({paddingLeft: padding + "px"}, 200);

                }
            },

            initialDefaultState: function(mobile) {
                if(mobile == null) {
                    this.isMobile = false;
                } else {
                    if(this.isBigDisplay()) {
                        $('#content section.style-default-bright').addClass('mob-max-content');
                        $('#off-canvas1').addClass('mob-max-filter');
                    } else {
                        //$('#content section.style-default-bright').addClass('mob-min-content');
                        //$('#off-canvas1').addClass('mob-min-filter');
                    }


                    this.isMobile = true;
                }

                var state;

                if(!this.isMobile){
                    if(this.isBigDisplay()) {
                        this.stateMenu = false;
                        this.stateFilter = true;
                        this.stateContent = true;
                        this.isContentMoveable =true;

                        state = 0;

                    } else {
                        this.stateMenu = false;
                        this.stateFilter = false;
                        this.stateContent = false;
                        this.isContentMoveable =false;

                        state = 1;

                    }
                } else {
                    if(this.isBigDisplay()) {
                        this.stateMenu = false;
                        this.stateFilter = false;
                        this.stateContent = false;
                        this.isContentMoveable =false;

                        state = 2;

                    } else {
                        this.stateMenu = false;
                        this.stateFilter = false;
                        this.stateContent = false;
                        this.isContentMoveable =false;

                        state = 3;

                    }

                    if(state!=this.prevState) {
                        switch (state) {
                            case 0:

                                break;
                            case 1:
                                this.closeFilter();
                                this.closeMenu();
                                break;
                        };
                    }

                    console.log("prev state" + this.prevState);
                    console.log("new state " + state);

                    this.prevState = state;
                }

                if(!this.isMobile){
                    if(this.isBigDisplay()) {
                        this.currentMenuWidth =this.openMenuWidthPCBig;
                    } else {
                        this.currentMenuWidth = this.openMenuWidthPCSmall;
                    }
                } else {
                    if(this.isBigDisplay()) {
                        this.currentMenuWidth =this.closeMenuWidthMobBig;
                    } else {
                        this.currentMenuWidth = this.openMenuWidthMobSmall;
                    }
                }
            },

            isBigDisplay: function() {
                return $(window).width() > 768;
            },

            openMenu:  function() {
                this.currentMenuWidth =this.closeMenuWidthPCBig;
                if(!this.isMobile){
                    if(this.isBigDisplay()) {
                        this.currentMenuWidth =this.openMenuWidthPCBig;
                    } else {
                        this.currentMenuWidth = this.openMenuWidthPCSmall;
                    }
                } else {
                    if(this.isBigDisplay()) {
                        this.currentMenuWidth =this.closeMenuWidthMobBig;
                    } else {
                        this.currentMenuWidth = this.openMenuWidthMobSmall;
                    }
                }

                this.stateMenu =true;
            },

            closeMenu:  function() {
                this.currentMenuWidth =this.openMenuWidthPCBig;
                if(!this.isMobile){
                    if(this.isBigDisplay()) {
                        this.currentMenuWidth =this.closeMenuWidthPCBig;
                    } else {
                        this.currentMenuWidth =this.closeMenuWidthPCSmall;
                    }
                } else {
                    if(this.isBigDisplay()) {
                        this.currentMenuWidth =this.closeMenuWidthMobBig;
                    } else {
                        this.currentMenuWidth =this.closeMenuWidthMobSmall;
                    }
                }

                this.stateMenu=false;
                // animate
            },

            closeFilter: function() {
                this.currentFilterLeft = 0;
                this.currentFilterLeft = -320;
                $('#off-canvas1').animate({left: this.currentFilterLeft + "px"}, 200);
                this.stateFilter = false;
            },

            openFilter: function() {
                this.currentFilterLeft =0;
                if(!this.isMobile){
                    if(this.isBigDisplay()){
                        this.currentFilterLeft = this.currentMenuWidth;
                    } else {
                        this.currentFilterLeft = 0;
                    }
                } else {
                    if(this.isBigDisplay()) {
                        this.currentFilterLeft = this.currentMenuWidth;
                    } else {
                        this.currentFilterLeft = this.closeMenuWidthMobSmall;
                    }
                }

                $('#off-canvas1').animate({left:this.currentFilterLeft+"px"},200);
                this.stateFilter = true;
            },

            init: function() {
                this.setStateMenu();
                this.setFilterState();
                this.setContentState();
            }
        }

    })();

    global.StateManager = StateManager;
})( this );
console.log(StateManager);
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

StateManager.initialDefaultState(isMobile.any());

$('#trigger').on('click', function(){
    StateManager.stateFilter = (!StateManager.stateFilter);
    StateManager.init();
});

$('a[data-toggle="menubar"]').on('click', function(){
    StateManager.stateMenu = (!StateManager.stateMenu);
    StateManager.init();
});

$(window).resize(function(){
    var windowSize = $(window).width();
    StateManager.width = windowSize;
    //StateManager.initialDefaultState(isMobile.any());
    //StateManager.init();
});


