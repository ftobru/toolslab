var lock = false;

function goToCabinet() {
    window.location.href = window.location.protocol + '//' + window.location.host + '/companies/my';
}

function userLogout() {
    window.location.href = window.location.protocol + '//' + window.location.host + '/auth/logout';
}

$(document).ready(function () {
    // registration
    $("#phone2").inputmask();
    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Номер телефона введен неправильно."
    );
    $("#registration-form-step1").validate({
        rules: {
            "name1": {
                required: true,
                minlength: 2,
                maxlength: 255
            },
            "phone2": {
                required: true,
                regex: "^\\+7\\s\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}"
            },
            "email1": {
                required: true,
                email: true
            }
        },
        messages: {
            "name1": {
                required: "Введите имя",
                minlength: "Минимальная длинна имени 2 символa",
                maxlength: "Максимальная длинна имени 255 символов"
            },
            "phone1": {
                required: "Введите номер телефона",
                digits: "Номер должен состоять только из цифр",
                minlength: "Номер должен состоять из 10 цифр",
                maxlength: "Номер должен состоять из 10 цифр"
            },
            "email1": {
                required: "Введите email",
                email: "Введите корректный email"
            }
        },
        submitHandler: function () {
            $('#resp-error1').hide('fast');
            $.cookie("name", $('#name1').val());
            $.cookie("email", $('#email1').val());
            $.cookie("phone", $('#phone2').val().replace(/\D/g,'').substr(1));

            var data = {
                name: $('#name1').val(),
                email: $('#email1').val(),
                phone: $('#phone2').val()
            };


            //window.location.href = '/demo';
        }
    });
    $('#btn-modal-register').click(function () {
        if(lock === false) {
            $('#resp-error1').hide('fast');
            $.cookie("name", $('#name1').val());
            $.cookie("email", $('#email1').val());
            $.cookie("phone", $('#phone2').val().replace(/\D/g,'').substr(1));

            var data = {
                name: $('#name1').val(),
                email: $('#email1').val(),
                phone: $('#phone2').val()
            };

            lock = true;
            $.ajax({
                url: "/user/ajax/demoRegistration",
                method: "POST",
                data: data,
                success: function (result) {
                    if(result.success === true) {
                        $.cookie("registration_token", null);
                        window.location.href = window.location.protocol + '//' + window.location.host + '/companies/my';
                    }
                    lock = false;
                },
                error: function (result) {
                    lock = false;
                }
            });
        }

    });
    // login
    $('#login-form').validate({
        rules: {
            "password1": {
                required: true,
                minlength: 5
            },
            "email10": {
                required: true,
                email: true
            }
        },
        messages: {
            "password1": {
                required: "Введите пароль",
                minlength: "Минимальная длинна пароля 5 символов",
            },
            "email10": {
                required: "Введите email",
                email:  "Введите корректный email"
            }
        },
        submitHandler: function() {
            $('#login-error').hide('fast');

            var email = $('#email10').val();
            var password = $('#password1').val();
            var remember =  $("#remember_me").is(":checked");

            var data = {
                "email": email,
                "password": password,
                "remember":  Boolean(remember)
            };
            $.ajax({
                url: "/auth/ajax/login",
                method: "POST",
                data: data,

                success: function (result, status, xhr) {
                    if(result.result) {
                        window.location.href = '/companies/my';
                    } else {
                        $('#login-error').html('<span class="help-block">Неправильный логин или пароль.</span>');
                    }
                    $('#login-error').show('slow');
                },
                error: function (result) {
                    $('#login-error').html('<span class="help-block">Непредвиденная ошибка</span>');
                    $('#login-error').show('slow');
                }
            });
        }
    });
});