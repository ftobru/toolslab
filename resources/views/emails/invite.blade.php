<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Новое приглашение</title>
</head>
<body>
<p>Вы получили новое приглашение от компании "{{{$company['display_name']}}}".</p>

<p>Для подтверждения перейдите по ссылке
    <a href="{{$link}}">Принять приглашение</a>
</p>
</body>
</html>
