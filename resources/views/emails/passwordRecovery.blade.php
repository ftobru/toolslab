<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Восстановление пароля</title>
</head>
<body>
    Для восстановления пароля перейдите по ссылке
    <a href="http://{{ $hostName }}/recovery_password?code={{ $recoveryCode }}">восстановить пароль</a>
</body>
</html>
