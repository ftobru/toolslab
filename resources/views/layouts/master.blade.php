<!DOCTYPE html>
<html lang="en">
<head>
    <title>ToolsLab - @yield('title')</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    @if(Request::secure())
        <link href='https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    @else
        <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    @endif

    <link type="text/css" rel="stylesheet" href="{{ elixir("css/all.css") }}" />
    {{--<link type="text/css" rel="stylesheet" href="css/bootstrap-tagsinput.css" />--}}

    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/js/html5shiv.js"></script>
    <script type="text/javascript" src="/js/respond.min.js"></script>
    <![endif]-->
</head>
    @yield('layout')
</html>
