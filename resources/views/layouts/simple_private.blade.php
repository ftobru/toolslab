
@extends('layouts.master_simple')

@section('header')
    @yield('demo-header')
    <div class="headerbar">
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                @yield('left-header-buttons')
            </ul>
        </div>
        <ul class="header-nav">
            <li class="prev">
                <button type="button" class="btn ink-reaction btn-flat btn-primary" onclick="history.back();return false;">
                    <i class="md md-keyboard-backspace"></i> Вернуться назад
                </button>
            </li>
        </ul>
        <div class="offcanvas"></div>
        <div class="card-head tables-tabs">
            @yield('switcher')
        </div>
        <div class="headerbar-right add">
            @yield('right-header-button')
            <ul class="header-nav header-nav-toggle">
                @yield('buttons')
            </ul>
            <ul class="header-nav header-nav-options">
                <li>
                    <!-- Search form -->
                    <form class="navbar-search" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" name="headerSearch" placeholder="Вы ищите...">
                        </div>
                        <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                    </form>
                </li>
                <li class="dropdown message-wrap">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-bell"></i><sup id="count-message" class="badge style-danger">0</sup>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header">Уведомления</li>
                        {{--<li class="alert alert-callout alert-warning alert-primary-bright">--}}
                            {{--<p><small>Входящий вызов от <span>+788877766667</span></small></p>--}}
                            {{--<a href="#"><strong>БОСС. Рога и копыта</strong></a>--}}
                        {{--</li>--}}
                        <li class="more-message"><a href="#">Посмотреть все сообщения<span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
                <li class="dropdown cabinet">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <i class="md md-person"></i>
                    </a>
                    <ul class="dropdown-menu animation-expand width-7">
                        <li class="dropdown-header">Анна Михеева</li>
                        <li><a href="#">Мой профиль</a></li>
                        <li><a href="{{URL::route('my')}}">В мои компании</a></li>
                        <li><a href="#">Мои настройки</a></li>
                        <li class="divider"></li>
                        @if(isset($company) && $company->demo)
                            <li><a href="/"><i class="fa fa-fw fa-power-off text-danger"></i> Выйти</a></li>
                        @else
                            <li><a href="{{URL::route('logout')}}"><i class="fa fa-fw fa-power-off text-danger"></i> Выйти</a></li>
                        @endif
                    </ul>
                </li><!--end .dropdown -->
                @if(isset($company) && $company->demo)
                    <li>
                        <button class="btn btn-default btn-danger" id="registration" data-toggle="modal"
                                data-target="#reg_ToolsLab"><span class="hidden-xs">Регистрация</span></span><span
                                    class="visible-xs">Попробуй!</span></button>
                    </li>
                @endif
            </ul>
        </div>
    </div>
@stop