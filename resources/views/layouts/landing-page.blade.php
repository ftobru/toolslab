<!doctype html>
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 10]>    <html class="ie10" > <![endif]-->
<!--[if IE 11]>    <html class="ie11" > <![endif]-->
<!--[if (gt IE 11)|!(IE)]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ModulCRM</title>

    <link rel="stylesheet" href="demo/css/bootstrap.css">
    <link rel="stylesheet" href="demo/css/style.css">
    <link rel="stylesheet" href="demo/css/new_style.css">
    <link rel="stylesheet" href="demo/css/responsive.css">
    <link rel="stylesheet" href="demo/css/animations.css">
    <link rel="stylesheet" href="demo/css/animate.css">
    <link rel="stylesheet" href="demo/css/material-design-iconic-font.css">
    <link rel="stylesheet" href="demo/css/font-awesome.css">
    <link rel="stylesheet" href="demo/css/swiper.css">
    <link rel="shortcut icon" href="demo/images/favicon.png">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

</head>
<body>
<div id="top"></div>
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-xs-4 logo">
                <a href="/"><img src="demo/images/LOGO.png" alt="ModulCrm"></a>
            </div>
            <button  class="toggle-canvas"><i class="md md-menu"></i></button>
            <div class="nav offset-canvas">
                <ul id="top-menu">
                    <li><a href="/about">Что это?</a></li>
                    <li><span id="opportunity">Возможности</span></li>
                </ul>
            </div>

            @if(Auth::getUser())
                @if($isDemoUser)
                    <button class="btn btn-2 btn-2a pull-right" onclick="goToCabinet()" style="margin-right: 10px; background-color:rgb(8,​ 134,​ 126); ">демо-кабинет</button>
                @else
                    <button class="btn btn-2 btn-2a pull-right" onclick="goToCabinet()" style="margin-right: 10px; background-color:rgb(8,​ 134,​ 126); ">Мой кабинет</button>
                    <button class="btn btn-2 btn-2a pull-right" onclick="userLogout()" style="margin-right: 10px; background-color:rgb(8,​ 134,​ 126);">Выход</button>
                @endif
            @else
                <button class="btn btn-2 btn-2a pull-right" data-toggle="modal" data-target="#modal1">Регистрация</button>
                <button class="btn btn-2 btn-2a pull-right" data-toggle="modal" style="margin-right: 10px; background-color:rgb(8,​ 134,​ 126); " data-target="#modal-login">Вход</button>
            @endif
        </div>
    </div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-inline">
                        <li><a href="/about">CRM</a></li>
                        <li><a href="/cms">CMS</a></li>
                        <li><a href="/multilanding">Мультилендинги</a></li>
                        <li><a href="/traffic">Распределение траффика</a></li>
                        <li><a href="/analitycs">Аналитика</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<div class="modal fade" id="modal1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title text-primary">Попробуйте ModulCRM сейчас!</h3>
            </div>
            <div class="card-body">
                <form id="registration-form-step1" class="form form-validate floating-label reg-forma" role="form" novalidate="novalidate">
                    <div class="form-group">
                        <div class="group">
                            <input type="text" required id="name1" name="name1">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="name1">Имя</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="group">
                            <input data-inputmask="'mask': '+7 (999) 999-9999'"  required type="text" id="phone2" name="phone2" class="phone-ru">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="phone2">Телефон</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="group">
                            <input type="email" required  id="email1" name="email1">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="email1">E-mail</label>
                        </div>
                    </div>
                    <div class="form-group floating-label has-error" id="resp-error1" style="display:none;"></div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn-modal-register" class="btn btn-2 btn-2a">Попробовать бесплатно сейчас</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-login">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title text-primary">Вход в личный кабинет</h3>
            </div>
            <div class="card-body">
                <form class="form floating-label reg-forma" id="login-form">

                    <div class="form-group">
                        <div class="group">
                            <input type="email" id="email10" name="email10">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="email0">E-mail</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="group">
                            <input type="password" id="password1" name="password1">
                            <span class="highlight"></span>
                            <span class="bar"></span>
                            <label for="password1">Пароль</label>
                        </div>
                    </div>


                    <div class="form-group">
                        <p class="help-block"><a href="{{URL::route('user.restorePasssword')}}">Забыли пароль?</a></p>
                    </div>
                    <div class="form-group floating-label has-error" id="login-error" style="display:none;"></div>


                    <div class="modal-footer enter-bottom">
                        <div class="col-xs-7 text-left remind">
                            <div class="checkbox-wrap">
                                <label>
                                    <input type="checkbox" id="remember_me" name="remember_me">
                                    <span>Запомнить меня</span>
                                </label>
                            </div>
                        </div>
                        <div class="col-xs-5 text-right">
                            <button type="submit" class="btn btn-primary btn-raised">Вход</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="demo/js/jquery-2.1.4.min.js"></script>
<script src="demo/js/bootstrap.min.js"></script>
<script src="demo/js/jquery.carouFredSel-6.1.0-packed.js"></script>
<script src="demo/js/jquery-ui-1.10.3.custom.js"></script>
<script src="demo/js/jquery.cycle.all.js"></script>
<script src="demo/js/jquery.appear.js"></script>
<script src="demo/js/jquery.nav.js"></script>
<script src="demo/js/jquery.scrollTo.js"></script>
<script src="demo/js/jquery.sticky.js"></script>
<script src="demo/js/jquery.form.js"></script>
<script src="demo/js/jquery.validate.js"></script>
<script src="demo/js/wow.js"></script>
<script src="demo/js/jquery.scrollme.js"  type="text/javascript"></script>
<script src="demo/js/typed.js" type="text/javascript"></script>
<script src="demo/js/swiper.js"  type="text/javascript"></script>
<script src="demo/js/script.js" type="text/javascript"></script>
<script src="demo/js/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="demo/js/jquery.cookie.js" type="text/javascript"></script>

<script src="demo/js/demo-auth.js" type="text/javascript"></script>

</body>
</html>