@extends('layouts.master')

@section('layout')
    <body class="header-fixed @yield('first', '')">
    <header id="header">
        @yield('header')
    </header>
    <div id="base">
        <div id="content">
            @yield('content')
        </div>
    </div>

    @yield('menu')

    @yield('modals')

    @if(isset($company) && $company->demo)
        <div class="modal fade" id="reg_ToolsLab">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel">Регистрация в ToolsLab</h3>
                    </div>
                    <div class="card-body">
                        <form class="form floating-label" id="registration-form-step2">

                            <div class="form-group floating-label">
                                <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="name2" name="name2">
                                        <label for="name2">Имя</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group floating-label">
                                <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-phone fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="text" id="phone2" data-inputmask="'mask': '(999) 999-9999'"
                                               name="phone2" class="form-control phone-ru">
                                        <span class="reg-phone"> +7</span>
                                        <label for="phone2"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group floating-label">
                                <div class="input-group">
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-envelope fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="email" class="form-control" id="email2" name="email2">
                                        <label for="email2">E-mail</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group floating-label">
                                <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="password" class="form-control" id="password21" name="password21">
                                        <label for="password21">Пароль</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group floating-label">
                                <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="password" class="form-control" id="password22" name="password22">
                                        <label for="password22">Подтвердить пароль</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group floating-label has-error" id="resp-error2" style="display:none;">

                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="btn-register-modal-post" data-loading-text="Loading..."
                                        class="btn btn-danger">Создать аккаунт! 17 дней
                                </button>
                                <p><i class="md md-lock"></i> Ваши данные надежно защищены</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title">Подтвердите свой номер телефона</h3>
                    </div>
                    <div class="card-body verification">
                        <p>Введите код подтверждения, присланный на указанный Вами номер телефона.</p>

                        <form class="form" id="registration-confirm-form">
                            <div class="form-group floating-label">
                                <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-phone fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="activation-code"
                                               name="activation-code">
                                        <label for="activation-code">Код подтверждения</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group floating-label has-error" id="resp-activate-error"
                                 style="display:none;">

                            </div>
                            <div class="modal-footer">
                                <button type="submit" data-loading-text="Loading..." class="btn btn-danger">
                                    Подтвердить
                                </button>
                                <p class="sms">Не получили смс? <span>Отправить повторно через <time
                                                id="resend_countdown">
                                            00:00
                                        </time></span> <a href="#">Отправить повторно</a></p>
                                <a href="#" id="open-edit-reg-data">Указать другой номер Телефона</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="updateRegData_ToolsLab">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header text-center">
                        <button type="button" id="closeUpdate" class="close" data-dismiss="modal"
                                aria-hidden="true">&times;</button>
                        <h3 class="modal-title" id="myModalLabel">Изменить регистрационные данные</h3>
                    </div>
                    <div class="card-body">
                        <form class="form floating-label" id="registration-form-update">

                            <div class="form-group floating-label">
                                <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-phone fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="text" id="new_phone" data-inputmask="'mask': '(999) 999-9999'"
                                               name="new_phone" class="form-control phone-ru">
                                        <span class="reg-phone"> +7</span>
                                        <label for="new_phone"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group floating-label">
                                <div class="input-group">
                                    <span class="input-group-addon"><span
                                                class="glyphicon glyphicon-envelope fa-lg"></span></span>

                                    <div class="input-group-content">
                                        <input type="email" class="form-control" id="new_email" name="new_email">
                                        <label for="new_email">E-mail</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group floating-label has-error" id="resp-update-error"
                                 style="display:none;">
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Изменить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <script src="{{ elixir("js/all.js") }}"></script>
    <div id="scripts">
        @yield('scripts')
        @if($socketKey)
            <script type="text/javascript">
                $(document).ready(function () {
                    var socket = io.connect('{!! $socketServer !!}');
                    socket.on('message{!! $socketKey !!}', function (data) {
                        console.log(data);
                    });
                });
            </script>
        @endif
        @if(isset($company) && $company->demo)
            <script type="text/javascript">
                $(document).ready(function () {

                    var Timer = {
                        state: false,

                        interval: 300000,
                        loopId: 0,

                        countdown: null,
                        sendBtn: null,

                        target_date: 0,

                        locStorageKey: 'resendSMSTimer',

                        ms_step: 500,

                        init: function () {
                            this.countdown = $("#resend_countdown");
                            this.sendBtn = $(".sms a");
                            this.target_date = this.getTargetDate();
                            $('#modal2').on('shown.bs.modal', function () {
                                Timer.start();
                            });
                        },
                        getTargetDate: function () {
                            if (localStorage[this.locStorageKey]) {
                                return parseInt(localStorage[this.locStorageKey]);
                            } else {
                                var time = new Date(new Date().getTime() + this.interval).getTime();
                                localStorage[this.locStorageKey] = time;
                                return time;
                            }
                        },
                        start: function () {
                            var self = this;

                            var current_date = new Date().getTime();

                            if (this.target_date > current_date) {
                                if (!this.state) {
                                    this.loopId = setInterval(function () {
                                        current_date = new Date().getTime();
                                        var seconds_left = (self.target_date - current_date) / 1000;
                                        seconds_left = seconds_left % 86400;
                                        seconds_left = seconds_left % 3600;
                                        var min = parseInt(seconds_left / 60);
                                        var sec = parseInt(seconds_left % 60);
                                        if (min < 0 || sec < 0) {
                                            self.stop();
                                        } else {
                                            var f_min = Math.abs(min) < 10 ? '0' + Math.abs(min) : Math.abs(min);
                                            var f_sec = Math.abs(sec) < 10 ? '0' + Math.abs(sec) : Math.abs(sec);
                                            $(self.countdown).html(f_min + ':' + f_sec);
                                        }
                                    }, this.ms_step);
                                    this.state = true;
                                }
                            } else {
                                this.state = true;
                                this.stop();
                            }
                        },
                        stop: function () {
                            var self = this;
                            if (this.state) {
                                localStorage.removeItem(this.locStorageKey);
                                this.sendBtn.unbind("click");
                                this.sendBtn.on('click', function (e) {
                                    if (!self.state) {
                                        self.sendSMS();
                                    }
                                });
                                this.sendBtn.show('fast');
                                clearInterval(this.loopId);
                                this.state = false;
                            }
                        },
                        sendSMS: function () {
                            var self = this;
                            $.ajax({
                                url: "/auth/ajax/resendActivationCode",
                                method: "POST",
                                success: function (result) {
                                    if (result.result === true) {
                                        toastr["success"]("Код активации упешно отправлен на указанный номер.")
                                        self.target_date = new Date().getTime() + self.interval;
                                        localStorage[self.locStorageKey] = self.target_date;
                                        self.start();
                                        self.sendBtn.hide('fast');
                                    } else {
                                        toastr["error"]("Произошла непредвиденная ошибка! Повторите попытку позже.")
                                    }
                                },
                                error: function (result) {
                                    toastr["error"]("Произошла непредвиденная ошибка! Повторите попытку позже.")
                                }
                            });
                        }
                    };
                    Timer.init();

                    if ($.cookie("registration_token") &&   $.cookie("registration_token") !== 'null') {
                        $('#registration').attr('data-target', '#modal2');
                    }

                    $("#phone2").inputmask();
                    $("#new_phone").inputmask();

                    $.validator.addMethod(
                            "regex",
                            function (value, element, regexp) {
                                var re = new RegExp(regexp);
                                return this.optional(element) || re.test(value);
                            },
                            "Номер телефона введен неправильно."
                    );

                    //set button action

                    var openButton = $('#btn_open_dialog');
                    if ($.cookie('registration_token')) {
                        openButton.html('<span class="hidden-xs">Активировать аккаунт!</span><span class="visible-xs">Попробуй!</span>');
                    } else {
                        openButton.html('<span class="hidden-xs">Сделай свою компанию эффективной! <span>17 дней бесплатно!</span></span><span class="visible-xs">Попробуй!</span>');
                    }
                    openButton.click(function () {
                        if ($.cookie('registration_token')) {
                            $('#modal2').modal('toggle');
                        } else {
                            $('#reg_ToolsLab').modal('toggle');
                        }
                    });

                    // Fill reg fields with data from cookies
                    $('.btn.btn-default.btn-danger').click(function () {
                        var name = $.cookie("name");
                        var email = $.cookie("email");
                        var phone = $.cookie("phone");
                        if (name) {
                            $('#name2').val(name).trigger("change");
                        }
                        if (email) {
                            $('#email2').val(email).trigger("change");
                            $('#new_email').val(email).trigger("change");
                        }
                        if (phone) {
                            $('#phone2').val(phone).trigger("change");
                            $('#new_phone').val(phone).trigger("change");
                        }
                    });

                    // Open change reg data window
                    $('#open-edit-reg-data').click(function () {
                        var email = $.cookie("email");
                        var phone = $.cookie("phone");

                        if (email) {
                            $('#new_email').val(email).trigger("change");
                        }
                        if (phone) {
                            $('#new_phone').val(phone).trigger("change");
                        }

                        $('#modal2').modal('hide');
                        $('#updateRegData_ToolsLab').modal('toggle');
                        $('#updateRegData_ToolsLab').on('hidden.bs.modal', function () {
                            $('#modal2').modal('show');
                        })
                    });

                    //validation of registration
                    $("#registration-form-step2").validate({
                        rules: {
                            "name2": {
                                required: true,
                                minlength: 2
                            },
                            "phone2": {
                                required: true,
                                regex: "^\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}"
                            },
                            "password21": {
                                required: true,
                                minlength: 5
                            },
                            "password22": {
                                required: true,
                                minlength: 5,
                                equalTo: "#password21"
                            },
                            "email2": {
                                required: true,
                                email: true
                            }
                        },
                        messages: {
                            "name2": {
                                required: "Введите имя",
                                minlength: "Минимальная длинна имени 2 символов"
                            },
                            "phone2": {
                                required: "Введите номер телефона",
                                minlength: "Номер должен состоять из 10 цифр",
                                maxlength: "Номер должен состоять из 10 цифр"
                            },
                            "password21": {
                                required: "Введите пароль",
                                minlength: "Минимальная длинна пароля 5 символов",
                            },
                            "password22": {
                                required: "Повторите пароль",
                                minlength: "Минимальная длинна пароля 5 символов",
                                equalTo: "Пароли не совпадают"
                            },
                            "email2": {
                                required: "Введите email",
                                email: "Введите корректный email"
                            }
                        },
                        submitHandler: function () {
                            $('#resp-error2').hide('fast');

                            var name = $('#name2').val();
                            var email = $('#email2').val();
                            var phone = $('#phone2').val();
                            var pass1 = $('#password21').val();
                            var pass2 = $('#password22').val();

                            var data = {
                                "users": {
                                    "name": name,
                                    "email": email,
                                    "password": pass1,
                                    "phone": phone
                                }
                            };

                            $.ajax({
                                url: "/auth/ajax/registration",
                                method: "POST",
                                data: data,

                                success: function (result) {
                                    if (result.result === true) {
                                        var registration_token = result.resp.user.registration_token;
                                        $.cookie("registration_token", registration_token);
                                        $.cookie("user_id", result.resp.user.id);
                                        $('#reg_ToolsLab').modal('hide');
                                        $('#modal2').modal('toggle');
                                        $('#registration').attr('data-target', '#modal2');
                                    }
                                },

                                error: function (result) {
                                    var obj = JSON.parse(result.responseText);
                                    if (obj.error) {
                                        var errorBlock = $('#resp-error2');
                                        switch (obj.error) {
                                            case 301:
                                                errorBlock.html('<span class="help-block">Такой email уже зарегистрирован</span>');
                                                break;
                                            case 302:
                                                errorBlock.html('<span class="help-block">Такой телефон уже зарегистрирован</span>');
                                                break;
                                            default:
                                                errorBlock.html('<span class="help-block">Непредвиденная ошибка</span>');
                                                break;
                                        }
                                        errorBlock.show('slow');
                                    }
                                }
                            });
                        }
                    });

                    //validation of update registration
                    $("#registration-form-update").validate({
                        rules: {
                            "new_phone": {
                                required: true,
                                regex: "^\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}"
                            },
                            "new_email": {
                                required: true,
                                email: true
                            }
                        },
                        messages: {
                            "new_phone": {
                                required: "Введите номер телефона",
                                digits: "Номер должен состоять только из цифр",
                                minlength: "Номер должен состоять из 10 цифр",
                                maxlength: "Номер должен состоять из 10 цифр"
                            },
                            "new_email": {
                                required: "Введите email",
                                email: "Введите корректный email"
                            }
                        },
                        submitHandler: function () {
                            $('#resp-update-error').hide('fast');

                            var reg_token = $.cookie("registration_token");

                            var email = $('#new_email').val();
                            var phone = $('#new_phone').val();
                            var uid = $.cookie("user_id");


                            var data = {
                                "registration_token": reg_token,
                                "email": email,
                                "phone": phone,
                                "user_id": uid
                            };

                            $.ajax({
                                url: "/auth/ajax/updateRegistration",
                                method: "POST",
                                data: data,
                                context: {email: email, phone: phone},

                                success: function (result) {
                                    if (result.user) {

                                        $.cookie("email", this.email);
                                        $.cookie("phone", this.phone);
                                        $('#updateRegData_ToolsLab').modal('hide');
                                        Timer.sendSMS();
                                        $('#modal2').modal('show');
                                    }
                                },

                                error: function (result) {
                                    var obj = JSON.parse(result.responseText);
                                    if (obj.error) {
                                        var errorBlock = $('#resp-update-error');
                                        switch (obj.error) {
                                            case 301:
                                                errorBlock.html('<span class="help-block">Такой email уже зарегистрирован</span>');
                                                break;
                                            case 302:
                                                errorBlock.html('<span class="help-block">Такой телефон уже зарегистрирован</span>');
                                                break;
                                            default:
                                                errorBlock.html('<span class="help-block">Непредвиденная ошибка</span>');
                                                break;
                                        }
                                        errorBlock.show('slow');
                                    }
                                }
                            });
                        }
                    });
                    $('#closeUpdate').click(function () {
                        $('#updateRegData_ToolsLab').modal('hide');
                        // TODO update info at modal2
                        $('#modal2').modal('show');
                    });
                    //validation of send activation code
                    $('#registration-confirm-form').validate({
                        rules: {
                            "activation-code": {
                                required: true
                            }
                        },
                        messages: {
                            "activation-code": {
                                required: "Введите код активации"
                            }
                        },
                        submitHandler: function () {

                            $('#resp-activate-error').hide('fast');

                            //start_countdown()

                            var code = $('#activation-code').val();
                            var user_id = $.cookie("user_id");

                            var data = {
                                activation_code: code,
                                user_id: user_id
                            };

                            var reg_token = $.cookie("registration_token");
                            $.ajax({
                                url: "/auth/ajax/activateAccount",
                                method: "POST",
                                data: data,

                                success: function (result) {
                                    if (result.result === true) {
                                        $.removeCookie("registration_token");
                                        $.removeCookie("email");
                                        $.removeCookie("phone");
                                        $.removeCookie("name");
                                        $('#modal2').modal('hide');
                                        window.location.href = '/companies/my';
                                    } else {
                                        var errorBlock = $('#resp-activate-error');
                                        errorBlock.html('<span class="help-block">Неверный код активации.</span>');
                                        errorBlock.show('slow');
                                    }
                                },

                                error: function (result) {
                                    var errorBlock = $('#resp-activate-error');
                                    errorBlock.html('<span class="help-block">На сервере возникла ошибка.</span>');
                                    errorBlock.show('slow');
                                }
                            });
                        }
                    });
                });
            </script>
        @endif

    </div>
    <footer>
        @yield('footer')
    </footer>
    </body>
@stop