
<!doctype html>
<!--[if IE 9]>    <html class="ie9" > <![endif]-->
<!--[if IE 10]>    <html class="ie10" > <![endif]-->
<!--[if IE 11]>    <html class="ie11" > <![endif]-->
<!--[if (gt IE 11)|!(IE)]><!--> <html lang="en-US"> <!--<![endif]-->
<head>

    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width" />

    <title>ModulCRM</title>

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/new_style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/animations.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/swiper.css">
    <link rel="shortcut icon" href="images/favicon.png">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <![endif]-->

</head>
<body>

<div id="top"></div>

<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-xs-4 logo">
                <a href="main.html"><img src="images/LOGO.png" alt="Логотип"></a>
            </div>
            <button  class="toggle-canvas"><i class="md md-menu"></i></button>
            <div class="nav offset-canvas">
                <ul id="top-menu">
                    <li><a href="main.html">Что это?</a></li>
                    <li><span id="opportunity">Возможности</span></li>
                </ul>
            </div>
            <button class="btn btn-2 btn-2a pull-right" data-toggle="modal" data-target="#modal1">Регистрация</button>
        </div>
    </div>
    <div class="submenu">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="list-inline">
                        <li><a href="about_crm.html">CRM</a></li>
                        <li><a href="cms.html">CMS</a></li>
                        <li><a href="multilanding.html">Мультилендинги</a></li>
                        <li><a href="traffic.html">Распределение траффика</a></li>
                        <li><a href="analitycs.html">Аналитика</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('content')

<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.carouFredSel-6.1.0-packed.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script src="js/jquery.cycle.all.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/jquery.form.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery.scrollme.js"></script>
<script src="js/typed.js" type="text/javascript"></script>
<script src="js/swiper.js"></script>
<script src="js/script.js"></script>
<!-- // <script src="js/jquery.particleground.min.js"></script> -->
@yield('scripts')



</body>
</html>