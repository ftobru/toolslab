@extends('layouts.simple_private')

@section('demo-header')
    <div class="headerbar demo-header">
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">

                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="info-btn">
            <a class="btn" href="#" data-toggle="offcanvas" data-backdrop="false">Обзор</a>
            <a class="btn" href="#" data-toggle="offcanvas" data-backdrop="false">Помощь</a>
            <a class="btn" href="#" data-toggle="offcanvas" data-backdrop="false">Тарифы</a>
        </div>

        <ul class="header-nav start-btn">
            <li>
                <button class="btn btn-default btn-danger" id="registration" data-toggle="modal" data-target="#reg_ToolsLab"><span class="hidden-xs">Сделай свою компанию эффективной! <span>17 дней бесплатно!</span></span><span class="visible-xs">Попробуй!</span></button>
            </li>
        </ul>

    </div>
@stop