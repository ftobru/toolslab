@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_delete')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')

    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/replacement-times">Time-замены</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование time-замены</h2>
            </div>
        </div>
        <form id="edit_form" method="POST" action="/{{{$company->name}}}/landingControl/replacement-time/edit/{{{$time->id}}}" class="form-horizontal edit_links" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular101" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название">Название <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" value="{{{$time->name}}}" type="text" class="form-control" id="regular101" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <input type="text" id="autocomplete1" class="form-control" data-source="../../html/forms/data/countries.json.html" placeholder="" style="display:none;">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" data-toggle="tooltip" data-placement="auto" title="Введите теги для товара">Промежуток времени <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon">с</span>
                                            <div class="input-group-content">
                                                <input name="start_time" value="{{{$time->start_time}}}" type="text" class="form-control time-mask">
                                                <label></label>
                                            </div>
                                            <span class="input-group-addon">по</span>
                                            <div class="input-group-content">
                                                <input name="finish_time" value="{{{$time->finish_time}}}" type="text" class="form-control time-mask">
                                                <div class="form-control-line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите теги для товара">Тег замены
                                    <span ><i class="md md-help"></i></span>
                                    <span class="popover-btn" data-container="body" data-toggle="popover" data-placement="auto" data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i class="md md-info"></i></span>
                                </label>
                                <div class="col-sm-9">
                                    <input name="tag" value="{{{$time->tag}}}" type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular69" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите параметр">Контент<span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="meaning" class="form-control" id="regular69">{{{$time->meaning}}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-xxl">
                <div class="col-sm-12">
                    <ul class="list-unstyled list-inline">
                        <li>
                            <button type="button" id="delete-btn1" class="btn btn-default-light ink-reaction btn-sm">Удалить</button>
                        </li>
                        <li>
                            <button type="button" id="reset-btn1" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                        </li>
                        <li>
                            <button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 opacity-75">
                    <p class="no-margin">Обновлено <span>{{ date("d.m.Y",strtotime($time->updated_at)) }}</span> в <span>{{ date("G:i",strtotime($time->updated_at)) }}</span></p>
                    <p class="no-margin">Создано <span>{{ date("d.m.Y",strtotime($time->created_at)) }}</span> в <span>{{ date("G:i",strtotime($time->created_at)) }}</span></p>
                </div>
            </div>
        </form>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript">

        var elf = null;
        var fileLinks = {};
        var currentFileSelectorId = '';

        function processSelectedFile(filePath, elemId) {
            fileLinks[elemId] = filePath;
            var filename = filePath.replace(/^.*[\\\/]/, '')
            $('#'+elemId).val(filename);

            var data = JSON.stringify(fileLinks);
            $('#fileLinks').val(data);
        }

        function resetBtn() {
            location.reload();
        }

        function deleteBtn() {
            var data = {
                entity_id:{{{$time->id}}}
            };

            $.ajax({
                url: '/{{{$company->name}}}/landingControl/ajax/replacementTimes/delete',
                method: "POST",
                data: data,
                context: this,
                success: function (result, status, xhr) {
                    $(location).attr('href','/{{{$company->name}}}/landingControl/replacement-times');
                },
                error: function (xhr, status, error) {
                    toastr.error('Произошла ошибка при удалении.');
                }
            });
        }

        $(document).ready(function () {

            $(".form-control.time-mask").inputmask('h:s', {placeholder: 'hh:mm'});

            $('#delete-btn1').click(function() {
                deleteBtn();
            });
            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                $('#edit_form').submit();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-delete-btn').click(function() {
                deleteBtn();
            });


            $.each($(".btn.fileupload"), function(key, value){
                $(value).click(function(e) {
                   var btn = $(e.target).closest(".btn");
                    currentFileSelectorId = 'fileuploadurl' + btn.data().fieldid.toString();
                    if(!elf) {
                         elf = $('<div></div>').dialogelfinder({
                            lang: 'ru', // locale
                            customData: {
                                _token: '<?= csrf_token() ?>'
                            },
                            url: '/{{{$company->name}}}/fileManager/connector',  // connector URL
                            dialog: {
                                modal: true,
                                width: 700,
                                height: 450,
                                title: "Select your file",
                                zIndex: 99999,
                                resizable: true
                            },
                            resizable: false,
                            getFileCallback: function (file) {
                                processSelectedFile(file.path, currentFileSelectorId);
                                elf.hide();
                            }
                        }).elfinder('instance');
                    } else {
                        elf.show();
                    }
                });
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

            // Расчет показателей
            var Calculator = {
                new_price: 0,
                old_price: 0,
                prime_cost: 0,
                init: function () {
                    var self = this;

                    this.new_price = parseInt($('#regular93').val());
                    this.old_price = parseInt($('#regular94').val());
                    this.prime_cost = parseInt($('#regular96').val());

                    self.resolve();

                    // new price
                    $('#regular93').bind('input', function (e) {
                        self.new_price = parseInt($(this).val());
                        self.resolve();
                    });
                    // old price
                    $('#regular94').bind('input', function (e) {
                        self.old_price = parseInt($(this).val());
                        self.resolve();
                    });
                    // prime_cost
                    $('#regular96').bind('input', function (e) {
                        self.prime_cost = parseInt($(this).val());
                        self.resolve();
                    });


                },
                resolve: function () {
                    var discount_summ = this.old_price - this.new_price;
                    var profit = this.new_price - this.prime_cost;
                    var discount_percent = (discount_summ / this.old_price) * 100;

                    // profit
                    $('#regular97').val(profit);
                    $('#regular98').val(discount_percent);
                    $('#regular99').val(discount_summ);

                }
            }
            Calculator.init();

        });
    </script>
@stop