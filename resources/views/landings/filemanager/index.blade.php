@extends('layouts.menubar_general')

@section('left-header-buttons')
@stop

@section('content')
                <div id="elfinder"></div>
@stop

@section('modals')

@stop

@section('scripts')

    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="/js/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $().ready(function() {
            $('#elfinder').elfinder({
                // set your elFinder options here
                lang: 'ru', // locale
                customData: {
                    _token: '<?= csrf_token() ?>'
                },
                url: '/{{{$company_alias}}}/fileManager/connector'  // connector URL
            });
        });
    </script>
@stop