@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_delete')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/templates">Шаблоны</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Шаблон страницы</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <form id="template-form" class="form-horizontal" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="regular105" class="col-sm-4 control-label">Название</label>
                                <div class="col-sm-8">
                                    <input name="name" value="{{{$template->name}}}" type="text" class="form-control" id="regular105" placeholder="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Теги</label>
                                <div class="col-sm-8">
                                    <input type="text" name="tags" value="{{{$tags}}}" class="form-control" data-role="tagsinput" style="display: none;">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="regular106" class="col-sm-4 control-label">Описание</label>
                                <div class="col-sm-8">
                                    <textarea name="description" class="form-control" id="regular106" placeholder="">{{{$template->description}}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular107" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Домен <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <select name="domain_id" id="regular107" class="form-control select2-list" data-placeholder="">
                                        <option>&nbsp;</option>
                                        @foreach($domains as $domain)
                                            <option value="{{{$domain->id}}}">{{{$domain->name}}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular108" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="url" type="text" class="form-control" id="regular108" value="{{{$template->url}}}" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular85" class="col-sm-4 control-label" data-toggle="tooltip" data-placement="auto" title="Template-index">Путь к index <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="fileuploadurl0" readonly placeholder="">
                                </div>
                                <div class="col-sm-2"><a href="#" data-fieldId="0" class="btn btn-default-light btn-sm fileupload" data-toggle="tooltip" data-placement="auto" title="Выбрать на сервере">Выбрать</a></div>
                            </div>
                            <div class="form-group">
                                <label for="regular86" class="col-sm-4 control-label">Статус</label>
                                <div id="status_container">
                                    <div class="col-sm-8">
                                        @if($status === 0)
                                            <div class="no">
                                                <div class="alert alert-danger">Файл не найден! Проверьте правильность
                                                    пути.
                                                </div>
                                            </div>
                                        @endif
                                        @if($status === 1)
                                            <div class="no">
                                                <div class="alert alert-success">Путь указан правильно! Index файл
                                                    подключен.
                                                </div>
                                            </div>
                                        @endif
                                        @if($status === 2)
                                            <div class="no">
                                                <div class="alert alert-warning">Укажите путь к Index файлу вашего сайта.
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="template-ya-metrika" class="col-sm-3 control-label">Счетчик Я.Метрики</label>
                                <div class="col-sm-9">
                                    <textarea name="ya_metrika" class="form-control" id="template-ya-metrika" placeholder="">{{{$template->ya_metrika}}}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="template-ga" class="col-sm-3 control-label">Счетчик G.Analytics</label>
                                <div class="col-sm-9">
                                    <textarea name="google_analytics" class="form-control" id="template-ga" placeholder="">{{{$template->google_analytics}}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control" id="fileLinks" name="fileLinks" >
        </form>
        <div class="row margin-bottom-xxl">
            <div class="col-sm-12">
                <ul class="list-unstyled list-inline">
                    <li><button onclick="deleteEntity()" type="button" class="btn btn-default-light ink-reaction btn-sm">Удалить</button></li>
                    <li><button onclick="resetBtn()" type="button" class="btn btn-default-light ink-reaction btn-sm">Отменить</button></li>
                    <li><button onclick="submitForm()" type="button" class="btn ink-reaction btn-primary btn-sm">Сохранить</button></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 opacity-75">
                <p class="no-margin">Обновлено <span>{{ date("d.m.Y",strtotime($template->updated_at)) }}</span> в <span>{{ date("G:i",strtotime($template->updated_at)) }}</span></p>
                <p class="no-margin">Создано <span>{{ date("d.m.Y",strtotime($template->created_at)) }}</span> в <span>{{ date("G:i",strtotime($template->created_at)) }}</span></p>
            </div>
        </div>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript" src="/js/jquery-ui.min.js"></script>

    <script type="text/javascript">

        var elf = null;
        var fileLinks = {};
        var currentFileSelectorId = '';

        function processSelectedFile(filePath, elemId) {
            fileLinks[elemId] = filePath;
            var filename = filePath.replace(/^.*[\\\/]/, '')
            $('#'+elemId).val(filename);
            var data = JSON.stringify(fileLinks);
            $('#fileLinks').val(data);
            $('#status_container div.alert').removeAttr('class').attr('class', 'alert alert-success').html('Путь указан правильно! Сохраните изменения.');
        }

        $.each($(".btn.fileupload"), function(key, value){
            $(value).click(function(e) {
                e.preventDefault();
                var btn = $(e.target).closest(".btn");
                currentFileSelectorId = 'fileuploadurl' + btn.data().fieldid.toString();
                if(!elf) {
                    elf = $('<div></div>').dialogelfinder({
                        lang: 'ru', // locale
                        customData: {
                            _token: '<?= csrf_token() ?>'
                        },
                        url: '/{{{$company->name}}}/fileManager/connector',  // connector URL
                        dialog: {
                            modal: true,
                            width: 700,
                            height: 450,
                            title: "Выберите файл шаблона",
                            zIndex: 99999,
                            resizable: true
                        },
                        resizable: false,
                        getFileCallback: function (file) {
                            processSelectedFile(file.path, currentFileSelectorId);
                            elf.hide();
                        }
                    }).elfinder('instance');
                } else {
                    elf.show();
                }
            });
        });




        $('select[name="domain_id"]').val({{{$template->domain_id}}});


        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/templates";
        }

        $('#template-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "domain_id": {
                            required: true
                        },
                        "fileLinks": {
                            required: true
                        }
                    },
                    messages: {
                        "domain_id": {
                            required: "Выберите домен"
                        },
                        "name": {
                            required: "Введите название сплита",
                            minlength: "Минимальная длинна названия сплита - 1 символ",
                            maxlength: "Введите более короткое название"
                        },
                        "fileLinks": {
                            required: "Нужно указать путь до файла шаблона"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#template-form').valid()) {
              var name = $('#template-form input[name="name"]').val();
              var description = $('#template-form textarea[name="description"]').val();
              var domain_id = $('#template-form select[name="domain_id"]').val();
              var url =  $('#template-form input[name="url"]').val();
              var ya = $('#template-form textarea[name="ya_metrika"]').val();
              var ga = $('#template-form textarea[name="google_analytics"]').val();
              var tags = $('#template-form input[name="tags"]').val();
              var fileLink = $('#template-form input[name="fileLinks"]').val();

              var data = {
                  name: name,
                  description: description,
                  domain_id: domain_id,
                  url: url,
                  ya_metrika: ya,
                  google_analytics: ga,
                  path: JSON.parse(fileLink)['fileuploadurl0'],
                  tags: tags,
                  id: {{{$template->id}}}
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/templates/update",
                  method: "PUT",
                  data: data,
                  success: function (result) {
                      toastr.success("Шаблон успешно обновлен");
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка сохранения");
                  }
              });
          }
        }

        function deleteEntity() {
            var data = {
                id: {{{$template->id}}}
            };

            $.ajax({
                url: "/{{{$company->name}}}/landingControl/ajax/templates/delete",
                method: "POST",
                data: data,
                success: function (result) {
                    window.location = '/{{{$company->name}}}/landingControl/templates';
                },
                error: function (result) {
                    toastr.error("Произошла ошибка удаления");
                }
            });
        }


        $(document).ready(function () {

            var filePath = '{{{$template->path}}}';
            var filename = filePath.replace(/^.*[\\\/]/, '');

            $('#fileLinks').val('{"fileuploadurl0":"' + '{{{$template->path}}}' + '"}');
            $('#fileuploadurl0').val(filename);

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-delete-btn').click(function() {
                deleteEntity();
            });


            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop