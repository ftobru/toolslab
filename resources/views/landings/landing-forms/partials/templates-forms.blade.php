
{{--Стандартные поля формы--}}
<script id="template-field-name" type="text/x-handlers-template">
    <input name="name" type="text" class="form-control">
    <label for="name">@{{label}}</label>
</script>

<script id="template-field-phone" type="text/x-handlers-template">
    <input name="phone" type="text" class="form-control">
    <label for="phone">@{{label}}</label>
</script>

<script id="template-field-email" type="text/x-handlers-template">
    <input name="email" type="text" class="form-control">
    <label for="email">@{{label}}</label>
</script>

{{--Поле филда формы--}}
<script id="template-custom-text-field" type="text/x-handlers-template">
    <div class="form-group floating-label text-left" id="base-field-@{{tag}}@{{number}}" style="display: none;">
        <input name="@{{tag}}" type="text" class="form-control">
        <label for="@{{label}}">@{{label}}</label>
    </div>
</script>

{{--Строка в таблице кастомных полей--}}
<script id="template-custom-field-row" type="text/x-handlers-template">
    <tr data-field-id="@{{id}}" data-field-name="@{{name}}">
        <td>@{{label}}</td>
        <td>@{{name}}</td>
        <td><input type="text" name="field-header" value="@{{label}}"/></td>
        <td><div class="checkbox checkbox-styled"><label><input name="requered" type="checkbox" value="0"><span class="checker" onclick="setRequiredState(this)"></span></label></div></td>
        <td><a href="javascript:void(0);" onclick="deleteField(this, @{{id}})" class="btn btn-icon-toggle ink-reaction"><i class="md md-delete"></i></a></td>
    </tr>
</script>

<script id="template-custom-field-row-saved" type="text/x-handlers-template">
    <tr data-field-id="@{{id}}" data-field-name="@{{name}}">
        <td>@{{label}}</td>
        <td>@{{name}}</td>
        <td><input type="text" name="field-header" value="@{{placeholder}}"/></td>
        <td><div class="checkbox checkbox-styled"><label><input name="requered" type="checkbox" value="0" @{{checked}}><span class="checker" onclick="setRequiredState(this)"></span></label></div></td>
        <td><a href="javascript:void(0);" onclick="deleteField(this, @{{id}})" class="btn btn-icon-toggle ink-reaction"><i class="md md-delete"></i></a></td>
    </tr>
</script>

