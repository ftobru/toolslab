
@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @if($editPermission)
        @include('partials.cabinet.button_add_simple')
    @endif
@stop

@section('switcher')
@stop

@section('content')
        <div class="filter-menu" id="off-canvas1" style="left: 60px;">
            <nav>
                <div class="expanded">
                    <a href="../../html/dashboards/dashboard.html">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label">
                                <input type="text" class="form-control" style="margin-top: 8px;" id="filter-link-name">
                                <label for="filter-link-name">ID формы:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control" id="date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control" id="date-end" name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
            <div class="section-body">
                <div class="table_wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card" id="droptarget-hidden-columns">
                                <div class="card-head">
                                    <header>Скрытые столбцы</header>
                                </div>
                                <div class="card-body">
                                    <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                        <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                    </div>
                                </div>
                                <!--end .card-body -->
                            </div>

                            {{--<div><span class="print-me">[Печать]</span></div>--}}
                            <div class="k-grid-container" id="grid"></div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
@stop

@section('modals')
@stop

@section('scripts')
    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>

    <script type="text/javascript">

        GridConfig = {};
        window.Filters = {};
        FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 1;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_landing_forms_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/landingControl/ajax/LandingForms/';
        // Путь до карты
        GridConfig.baseCardUrl = '/' + '{{{$company->name}}}' + '/landingControl/landingForms/edit/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'LandingForms';
        // Можно ли редактировать
        GridConfig.editable = false;
        // Можно ли делать группировку
        GridConfig.groupable = false;
        // Шаблон копирования строк
        GridConfig.copyFields = ['name'];
        // Конфиг колонок грида
        GridConfig.columns = [
            {
                field: "id",
                title: "ID",
                width: "50px",
                locked: true,
                lockable: false
            },
            {
                field: "id_attr",
                title: "ID формы",
                width: "220px",
                locked: true,
                lockable: false
            },
            {
                field: "tags1",
                title: 'Теги задач',
                template: "#= cellTemplate(tags, 'task-tags') #",
                width: "130px"
            },
            {
                field: "tags2",
                title: 'Теги сделок',
                template: "#= cellTemplate(tags, 'deals-tags') #",
                width: "130px"
            },
            {
                field: "tags3",
                title: 'Теги контакта',
                template: "#= cellTemplate(tags, 'client-tags') #",
                width: "130px"
            },
            {
                field: "created_at",
                title: "Созданa",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px"
            },
            {
                field: "updated_at",
                title: "Изменён",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px"
            },
            @if($editPermission)
            {
                command: [
                    {name: "card", text: "", click: formEdit , className: "md md-list", width: "32px"},
                    {name: "delete", text: "", click: deleteForm, width: "32px", className: "md md-delete"}
                ],
                title: "&nbsp;",
                width: "170px",
                attributes: {"class": "button-column"}
            }
            @endif
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string"},
                url: {type: "string"},
                open_url: {type: "boolean"},
                description: {type: "string"},
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'id_attr',
            'date_start',
            'date_end'
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'id_attr',
            'date_start',
            'date_end'
        ];

        @if($editPermission)
        function formEdit(e) {
            e.preventDefault();
            var item = this.dataItem($(e.currentTarget).closest("tr"));
            var idx = this.dataSource.indexOf(item);
            var entityID = item.id;
            window.location.href = window.location.protocol + '//' + window.location.host + GridConfig.baseCardUrl + entityID;
        }
        function deleteForm(e) {
            e.preventDefault();
            var item = this.dataItem($(e.currentTarget).closest("tr"));
            var idx = this.dataSource.indexOf(item);
            var entityID = item.id;

            var data = {id: entityID};
            allowSubmit = false;
            $.ajax({
                url: "/{{{$company->name}}}/landingControl/ajax/LandingForms/delete",
                method: "POST",
                data: data,
                success: function (result) {
                    toastr.success("Форма успешно удалена");
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    allowSubmit = true;
                },
                error: function (result) {
                    toastr.error("Произошла ошибка при удалении формы");
                    allowSubmit = true;
                }
            });
        }
        @endif

        function cellTemplate(value, type) {
            switch (type) {
                case 'client-tags':
                    if (value) {
                        value = JSON.parse(value);
                        for (var item in value) {
                            if(value[item].hasOwnProperty('client_tags')) {
                                if(value[item].client_tags) {
                                    return value[item].client_tags;
                                } else {
                                    return '-';
                                }
                            }
                        }
                    }
                    break;
                case 'deals-tags':
                    if (value) {
                        value = JSON.parse(value);
                        for (var item in value) {
                            if (value[item].hasOwnProperty('order_tags')) {
                                if (value[item].order_tags) {
                                    return value[item].order_tags;
                                } else {
                                    return '-';
                                }
                            }
                        }
                    }
                    return 'Нет';
                    break;
                case 'task-tags':
                    if (value) {
                        value = JSON.parse(value);
                        for (var item in value) {
                            if(value[item].hasOwnProperty('task_tags')) {
                                if (value[item].task_tags) {
                                    return value[item].task_tags;
                                } else {
                                    return '-';
                                }
                            }
                        }
                    }
                    return 'Нет';
                    break;
            }
            return '[unknown]';
        }

        $(document).ready(function () {

            $("#header_add_simple_btn").attr("href", "/{{{$company->name}}}/landingControl/landingForms/create");

            Filters = {

                isInit: false,

                id_attr_input: null,

                // Селекторы дат
                date_start: null,
                date_end: null,

                initFilters: function () {

                    var self = this;

                    this.id_attr_input = $('#filter-link-name');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');

                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    this.id_attr_input.keyup(function () {
                        FilterBuilder.addFilter('id_attr', {
                            field: "id_attr",
                            operator: "startWith",
                            value: self.id_attr_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },


                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "id_attr" && value.operator == 'startWith') {
                        $('#filter-link-name').val(value.value);
                        $('#filter-link-name').change();
                        FilterBuilder.addFilter('id_attr', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                }
            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            }

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });

    </script>
@stop