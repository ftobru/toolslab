@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/landingForms">Формы захвата</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование формы захвата</h2>

                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и
                        открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока,
                        окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.
                        Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при
                        нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие
                        и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого
                        блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие
                        кнопоки.
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <form id="form-part1" class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="regular20" class="col-sm-3 control-label" data-toggle="tooltip"
                                       data-placement="auto" title="Уникальное значение">ID формы <span
                                            class="hidden-md hidden-sm hidden-xs"><i
                                                class="md md-help"></i></span></label>

                                <div class="col-sm-9">
                                    <input name="id_attr" type="text" class="form-control" id="regular20"
                                           value="{{$form->id_attr}}" placeholder="">

                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular21" class="col-sm-3 control-label" data-toggle="tooltip"
                                       data-placement="auto" title="Введите название вашей формы">Название <span
                                            class="hidden-md hidden-sm hidden-xs"><i
                                                class="md md-help"></i></span></label>

                                <div class="col-sm-9">
                                    <input name="name" type="text" value="{{$form->name}}" class="form-control"
                                           id="regular21" placeholder="">

                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto"
                                       title="Тип отображения">Тип формы <span class="hidden-md hidden-sm hidden-xs"><i
                                                class="md md-help"></i></span></label>

                                <div class="col-sm-9">
                                    <div id="tabs-1" class="tabs button-tabs">
                                        <input id="tab-1-1" name="form_type" value="0" type="radio"
                                        @if($form->form_type === \LandingControl\Reference\LandingFormReference::FORM_TYPE_POPUP)
                                               checked
                                                @endif
                                                />
                                        <input id="tab-1-2" name="form_type" value="1" type="radio"
                                        @if($form->form_type === \LandingControl\Reference\LandingFormReference::FORM_TYPE_INTEGRATED)
                                               checked
                                                @endif
                                                />
                                        <input id="tab-1-3" type="radio"/>

                                        <label for="tab-1-1" class="first-label">Всплывающая</label>
                                        <label for="tab-1-2" class="last-label">Встроенная</label>
                                        <label for="tab-1-3" style="display: none">Гео-сплит</label>

                                        <div class="radio-content">
                                            Готовая форма откроется в новом окне.
                                        </div>
                                        <div class="radio-content">
                                            Можно встроить в верстку вашего сайта.
                                            <div>
                                                <span contenteditable>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia doloribus deserunt nesciunt repudiandae a odio perferendis inventore, dolorum atque alias rerum quidem hic accusantium, cum optio iure ea numquam impedit.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique
                                pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card edit-form">
                    <div class="card-body">
                        <div class="card-head">
                            <ul class="nav nav-tabs nav-styled" data-toggle="tabs">
                                <li class="active"><a href="#first3">Форма</a></li>
                                <li><a href="#second3">Поля</a></li>
                                <!-- <li><a href="#third3">Действие</a></li> -->
                            </ul>
                            <div class="pull-right">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-default">Тест
                                    всплывающего окна
                                </button>
                            </div>
                        </div>
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="first3">
                                <form id="form-content" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label for="input_title" class="col-sm-3 control-label" data-toggle="tooltip"
                                               data-placement="auto" title="Введите заголовок формы">Заголовок <span
                                                    class="hidden-md hidden-sm hidden-xs"><i
                                                        class="md md-help"></i></span></label>

                                        <div class="col-sm-9">
                                            <input name="header_text" type="text" class="form-control" id="input_title"
                                                   placeholder="">

                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input_after_title" class="col-sm-3 control-label">После
                                            заголовка</label>

                                        <div class="col-sm-9">
                                            <textarea name="after_header_text" class="form-control"
                                                      id="input_after_title" placeholder=""></textarea>

                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input_before_btn" class="col-sm-3 control-label">Перед
                                            кнопкой</label>

                                        <div class="col-sm-9">
                                            <textarea name="before_btn_text" class="form-control" id="input_before_btn"
                                                      placeholder=""></textarea>

                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input_btn" class="col-sm-3 control-label">Текст кнопки</label>

                                        <div class="col-sm-9">
                                            <input name="btn_text" type="text" class="form-control" id="input_btn"
                                                   placeholder="">

                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="choice_color" class="col-sm-3 control-label">Цвет кнопки</label>

                                        <div class="col-sm-9">

                                            <input type="text" id="autocomplete1" class="form-control"
                                                   data-source="../../html/forms/data/countries.json.html"
                                                   placeholder="Countries" style="display: none;">
                                            <input type="text" name="btn_color" id="choice_color"
                                                   class="form-control bscp" value="#8fff00" data-colorpicker-guid="1">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input_after_btn" class="col-sm-3 control-label">После кнопки</label>

                                        <div class="col-sm-9">
                                            <textarea name="after_btn_text" class="form-control" id="input_after_btn"
                                                      placeholder=""></textarea>

                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!--end tab-pane-->
                            <div class="tab-pane" id="second3">
                                <div class="table-responsive">

                                    <h2> Основные поля формы </h2>

                                    <form id="base-form-fields">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Имя</th>
                                                <th>Тег</th>
                                                <th>Заголовок</th>
                                                <th>Обязательно</th>
                                                <th>Отображать</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr data-name="name">
                                                <td>Имя</td>
                                                <td>name</td>
                                                <td><input onkeyup="changeBaseFieldsLabel(this)" type="text"
                                                           name="field-header" value="Имя"/></td>
                                                <td>
                                                    <div class="checkbox checkbox-styled"><label><input name="requered"
                                                                                                        type="checkbox"
                                                                                                        value="0"><span
                                                                    class="checker"
                                                                    onclick="setRequiredState(this)"></span></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox checkbox-styled"><label><input
                                                                    onchange="changeBaseFieldsVisiblity(this)"
                                                                    name="show-name" type="checkbox" value="1"
                                                                    checked><span class="checker active"></span></label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr data-name="phone">
                                                <td>Телефон</td>
                                                <td>phone</td>
                                                <td><input onkeyup="changeBaseFieldsLabel(this)" type="text"
                                                           name="field-header" value="Телефон"/></td>
                                                <td>
                                                    <div class="checkbox checkbox-styled"><label><input name="requered"
                                                                                                        type="checkbox"
                                                                                                        value="0"><span
                                                                    class="checker"
                                                                    onclick="setRequiredState(this)"></span></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox checkbox-styled"><label><input
                                                                    onchange="changeBaseFieldsVisiblity(this)"
                                                                    name="show-phone" type="checkbox" value="1" checked><span
                                                                    class="checker active"></span></label></div>
                                                </td>
                                            </tr>
                                            <tr data-name="email">
                                                <td>E-mail</td>
                                                <td>email</td>
                                                <td><input onkeyup="changeBaseFieldsLabel(this)" type="text"
                                                           name="field-header" value="E-mail"/></td>
                                                <td>
                                                    <div class="checkbox checkbox-styled"><label><input name="requered"
                                                                                                        type="checkbox"
                                                                                                        value="0"><span
                                                                    class="checker"
                                                                    onclick="setRequiredState(this)"></span></label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="checkbox checkbox-styled"><label><input
                                                                    onchange="changeBaseFieldsVisiblity(this)"
                                                                    name="show-email" type="checkbox" value="1" checked><span
                                                                    class="checker active"></span></label></div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>


                                    <h2> Дополнительные поля формы </h2>

                                    <form id="form-fields">
                                        <table class="table table-striped">
                                            <thead>
                                            <tr>
                                                <th>Имя</th>
                                                <th>Тег</th>
                                                <th>Заголовок</th>
                                                <th>Обязательно</th>
                                                <th>Управление</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </form>
                                    <ul class="list-inline">
                                        <li>
                                            <a href="/{{{$company->name}}}/saas/forms/create" target="_blank"
                                               class="text-primary"><i class="md md-add"></i> Добавить поле</a>
                                        </li>
                                        <li>
                                            <select id="field_selector" class="form-control input-sm" name="field"
                                                    placeholder="Добавить поле">
                                            </select>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="form_generate card">
                    <div class="card-body">
                        <form id="form-preview" class="form form-validate floating-label text-center" role="form"
                              novalidate="novalidate">
                            <div class="text-center">
                                <h2 id="title">Попробуйте ToolsLab сейчас!</h2>
                            </div>
                            <p id="after_title">Текст текст какое-то описание</p>

                            <div class="form-group floating-label text-left" id="base-field-name">
                                {{--   <input name="name" type="text" class="form-control" id="text1">
                                   <label for="name">Имя</label>--}}
                            </div>
                            <div class="form-group floating-label text-left" id="base-field-phone">
                                {{--<input name="phone" type="text" class="form-control" id="regular2">
                                <label for="phone">Контактный телефон</label>--}}
                            </div>
                            <div class="form-group floating-label text-left" id="base-field-email">
                                {{--<input name="email" type="text" class="form-control" id="regular2">
                                <label for="email">e-mail</label>--}}
                            </div>
                            <div class="margin-bottom-xxl">
                                <p id="before_btn"></p>
                            </div>
                            <div class="text-center margin-bottom-xxl">
                                <button type="button" class="btn btn-danger" id="btn">Попробовать бесплатно сейчас
                                </button>
                            </div>
                            <p id="after_btn"></p>
                        </form>
                    </div>
                </div>
                <!-- <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-body">
                        <div class="tab-pane" id="third3">
                            <p class="text-primary">Присваиваемые теги</p>

                            <form id="form-tags" class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="regular29" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Данные теги передадутся задаче">Задаче <span
                                                class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <input value="{{$tag['task_tags']}}" name="task_tags" type="text" class="form-control" data-role="tagsinput"
                                               style="display: none;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="regular30" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Данные теги передадутся контакту">Контакту <span
                                                class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <input value="{{$tag['client_tags']}}" name="client_tags" type="text" class="form-control"
                                               data-role="tagsinput" style="display: none;">
                                    </div>
                                </div>
                                <div class="margin-bottom-xxl">
                                    <div class="form-group">
                                        <label for="regular31" class="col-sm-3 control-label" data-toggle="tooltip"
                                               data-placement="auto" title="Данные теги передадутся сделке">Сделке <span
                                                    class="hidden-md hidden-sm hidden-xs"><i
                                                        class="md md-help"></i></span></label>

                                        <div class="col-sm-9">
                                            <input name="order_tags" type="text" class="form-control"
                                                   value="{{$tag['order_tags']}}" data-role="tagsinput" style="display: none;">
                                        </div>
                                    </div>
                                </div>
                                <div class="margin-bottom-lg">
                                    <p class="text-primary text-lg">После отправки формы: </p>
                                </div>
                                <div id="tabs-2" class="tabs button-tabs">
                                    <input id="tab-2-1" name="callback_type" value="0" type="radio"
                                    @if($form->callback_type === \LandingControl\Reference\LandingFormReference::CALLBACK_POPUP)
                                           checked
                                    @endif
                                            />
                                    <input id="tab-2-2" name="callback_type" value="1" type="radio"
                                    @if($form->callback_type === \LandingControl\Reference\LandingFormReference::CALLBACK_REDIRECT)
                                           checked
                                    @endif
                                            />
                                    <input id="tab-2-3" name="tabs-group-1" type="radio"/>

                                    <label for="tab-2-1" class="first-label">Всплывающий текст</label>
                                    <label for="tab-2-2" class="last-label">Редирект</label>
                                    <label for="tab-2-3" style="display: none">Гео-сплит</label>

                                    <div class="radio-content">
                                        <div class="form-group">
                                            <label for="input_title" class="col-sm-3 control-label">Текст</label>

                                            <div class="col-sm-9">
                                                <input name="callback_text" value="{{$form->callback_text}}" type="text" class="form-control">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="radio-content">
                                        <div class="form-group">
                                            <label for="input_title" class="col-sm-3 control-label">Страница/URL</label>

                                            <div class="col-sm-9">
                                                <input name="callback_url" type="text" class="form-control" value="{{$form->callback_url}}"
                                                       id="input_title" placeholder="">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- end tab-pane-->
                    </div>
                </div>
            </div>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique
                                pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row margin-bottom-xxl">
            <div class="col-sm-12">
                <ul class="list-unstyled list-inline">
                    <li>
                        <button type="button" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                    </li>
                    <li>
                        <button id="submit-btn-form" type="button" class="btn ink-reaction btn-primary btn-sm">
                            Сохранить
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </section>
@stop

@section('modals')
    <div class="fade" id="modal-default">
        <div id="modal-dialog">
            <div class="modal-content">
                <div class="form_generate">
                    <div class="card-body">
                        <form id="popup-form-preview" class="form form-validate floating-label text-center" role="form"
                              novalidate="novalidate">
                            <div class="text-center">
                                <h2 id="modal_title">Попробуйте ToolsLab сейчас!</h2>
                            </div>
                            <p id="modal_after_title">Текст текст какое-то описание</p>

                            <div class="form-group floating-label text-left" id="base-field-name2">
                                {{--  <input name="name" type="text" class="form-control">
                                  <label for="name">Имя</label>--}}
                            </div>
                            <div class="form-group floating-label text-left" id="base-field-phone2">
                                {{--<input name="phone" type="text" class="form-control">
                                <label for="phone">Контактный телефон</label>--}}
                            </div>
                            <div class="form-group floating-label text-left" id="base-field-email2">
                                {{--<input name="email" type="text" class="form-control">
                                <label for="email">e-mail</label>--}}
                            </div>
                            <div class="margin-bottom-xxl">
                                <p id="modal_before_btn">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Fugiat labore nulla veniam distinctio ullam, adipisci magni ea, voluptate id
                                    voluptatem rerum iure dolor molestias dicta, consequatur perferendis et inventore
                                    cum.</p>
                            </div>
                            <div class="text-center margin-bottom-xxl">
                                <button type="button" class="btn btn-danger" id="modal_btn">Попробовать бесплатно
                                    сейчас
                                </button>
                            </div>
                            <p id="modal_after_btn">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat
                                labore nulla veniam distinctio ullam, adipisci magni ea, voluptate id voluptatem rerum
                                iure dolor molestias dicta, consequatur perferendis et inventore cum.</p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script type="application/javascript" src="/js/handlebars.min.js"></script>

    @include('landings.landing-forms.partials.templates-forms')

    <script type="text/javascript">

        var saved_form_data = {
            id: {{$form->id}},
            name: '{{$form->name}}',
            id_attr: '{{$form->id_attr}}',
            form_type: {{$form->form_type}},
            callback_type: {{$form->callback_type}},
            callback_url: '{{$form->callback_url}}',
            callback_text: '{{$form->callback_text}}',
            after_header_text: '{{$form->after_header_text}}',
            header_text: '{{$form->header_text}}',
            before_header_text: '{{$form->before_header_text}}',
            after_btn_text: '{{$form->after_btn_text}}',
            btn_text: '{{$form->btn_text}}',
            before_btn_text: '{{$form->before_btn_text}}',
            btn_color: '{{$form->btn_color}}',
            company_id: '{{$form->company_id}}',
            fields: '{!!$form->fields!!}',
            tags: '{!!$form->tags!!}',
            base_fields: '{!!$form->base_fields!!}',
            created_at: '{{$form->created_at}}',
            updated_at: '{{$form->updated_at}}'
        };

        function initFieldValues() {
            $('#form-content input[name=header_text]').val(saved_form_data.header_text).trigger("keyup");
            ;
            $('#form-content textarea[name=after_header_text]').val(saved_form_data.after_header_text).trigger("keyup");
            ;
            $('#form-content textarea[name=before_btn_text]').val(saved_form_data.before_btn_text).trigger("keyup");
            ;
            $('#form-content textarea[name=after_btn_text]').val(saved_form_data.after_btn_text).trigger("keyup");
            ;
            $('#form-content input[name=btn_text]').val(saved_form_data.btn_text).trigger("keyup");
            ;
            $('#form-content input[name=btn_color]').val(saved_form_data.btn_color).trigger("keyup");

            // init base fields
            var baseFields = JSON.parse(saved_form_data.base_fields);
            $.each(baseFields, function (k, v) {
                $('#base-form-fields tr[data-name=' + v.name + '] input[name=field-header]').val(v.placeholder);
                $('#base-form-fields tr[data-name=' + v.name + '] input[name=requered]')
                        .prop("checked", v.required)
                        .trigger('onchange');
                $('#base-form-fields tr[data-name=' + v.name + '] input[name=show-' + v.name + ']')
                        .prop("checked", v.show)
                        .trigger('onchange');
            });

            // additional fields
            var additionalFields = JSON.parse(saved_form_data.fields);
            $.each(additionalFields, function (k, v) {
                selectedForms.push(v.field_id);
                var row_source = $('#template-custom-field-row-saved').html();
                var row_template = Handlebars.compile(row_source);
                $('#form-fields tbody').append(row_template({
                    label: v.base.label,
                    id: v.field_id,
                    name: v.base.name,
                    checked: v.required ? 'checked' : '',
                    placeholder: v.placeholder
                }));
                $('#field_selector').select2("val", "");
                // Добавление в форму
                var source = $('#template-custom-text-field').html();
                var field_template = Handlebars.compile(source);
                var tag = v.base.name;
                var label = v.placeholder;
                $(field_template({
                    label: label,
                    number: '',
                    tag: tag
                })).insertAfter($('#form-preview .form-group').last()).show('slow');
                $(field_template({
                    label: label,
                    number: 2,
                    tag: tag
                })).insertAfter($('#popup-form-preview .form-group').last()).show('slow');
            });

            // init tags
            var tags_info = JSON.parse(saved_form_data.tags);
            $.each(tags_info, function (key, value) {
                if (value.hasOwnProperty('task_tags')) {
                    $('#form-tags input[name=task_tags]').val(value.task_tags);
                }
                if (value.hasOwnProperty('client_tags')) {
                    $('#form-tags input[name=client_tags]').val(value.client_tags);
                }
                if (value.hasOwnProperty('order_tags')) {
                    $('#form-tags input[name=order_tags]').val(value.order_tags);
                }
            });
        }

        var selectedForms = [];

        function FormatResult(item) {
            var markup = "";
            if (item.name !== undefined) {
                markup += "<option value='" + item.id + "'>" + item.name + "</option>";
            }
            return markup;
        }

        function FormatSelection(item) {
            return item.name;
        }

        function changeBaseFieldsLabel(field) {
            var base_fields = ['name', 'phone', 'email'];
            var fieldName = $(field).closest('tr').data().name;
            if (base_fields.indexOf(fieldName) > -1) {
                $('#form-preview label[for=' + fieldName + ']').html($(field).val());
                $('#popup-form-preview label[for=' + fieldName + ']').html($(field).val());
            }
        }

        function changeBaseFieldsVisiblity(field) {
            var $field = $(field);
            var $tr = $field.closest('tr');
            var isChecked = field.checked;
            var fieldName = $tr.data().name;
            var formField1 = $('#base-field-' + fieldName);
            var formField2 = $('#base-field-' + fieldName + '2');

            if (isChecked) {
                var source = $('#template-field-' + fieldName).html();
                var label = $tr.find('input[name=field-header]').val();
                var template = Handlebars.compile(source);
                formField1.html(template({label: label}));
                formField2.html(template({label: label}));
                formField1.show('slow');
                formField2.show('slow');
            } else {
                formField1.hide('slow');
                formField2.hide('slow');
                formField1.html('');
                formField2.html('');
            }
        }

        var allowSubmit = true;

        function submitForm() {
            if (allowSubmit) {
                var data = {};
                var mainForm = $('#form-part1').serializeArray();
                var tagData = $('#form-tags').serializeArray();
                var content = $('#form-content').serializeArray();
                var fieldsArray = $('#form-fields tbody tr');


                var baseFieldsArray = ['name', 'phone', 'email'];
                var baseFields = {};
                $.each(baseFieldsArray, function (k, v) {
                    var $tr = $('tr[data-name=' + v + ']');
                    var show = $tr.find('input[name=show-' + v + ']').is(':checked');
                    var required = $tr.find('input[name=requered]').is(':checked');
                    var placeholder = $tr.find('input[name="field-header"]').val();
                    var data = {
                        name: v,
                        placeholder: placeholder,
                        required: required,
                        show: show
                    };
                    baseFields[v] = data;
                });

                var fields = [];
                $.each(fieldsArray, function (k, v) {
                    var field_id = $(v).data('fieldId');
                    var placeholder = $(v).find('input[name="field-header"]').val();
                    var required = false;
                    if ($(v).find('span.checker').hasClass('active')) {
                        required = true;
                    }

                    fields.push({field_id: field_id, placeholder: placeholder, required: required});
                });

                data.fields = fields;

                data.id = {{$form->id}};
                data.tags = {};
                var i = -1;
                $.each(tagData, function (k, v) {
                    if (v.name === "task_tags" || v.name === "client_tags" || v.name === "order_tags") {
                        i++;
                        data.tags[i] = {};
                        data.tags[i][v.name] = v.value;
                    } else {
                        data[v.name] = v.value;
                    }
                });

                $.each(content, function (k, v) {
                    data[v.name] = v.value;
                });

                $.each(mainForm, function (k, v) {
                    data[v.name] = v.value;
                });

                data.fields = JSON.stringify(data.fields);
                data.tags = JSON.stringify(data.tags);
                data.base_fields = JSON.stringify(baseFields);

                allowSubmit = false;
                $.ajax({
                    url: "/{{{$company->name}}}/landingControl/ajax/LandingForms/update",
                    method: "PUT",
                    data: data,
                    success: function (result) {
                        toastr.success("Изменения успешно сохранены");
                        allowSubmit = true;
                    },
                    error: function (result) {
                        toastr.error("Произошла ошибка при редактировании формы");
                        allowSubmit = true;
                    }
                });
            }
        }

        function deleteField(e, id) {
            var tag = $(e).closest('tr').data().fieldName;
            $('#base-field-' + tag).hide("fast").delay(1000).queue(function () {
                $('#base-field-' + tag).remove();
            });
            $('#base-field-' + tag + '2').hide("fast").delay(1000).queue(function () {
                $('#base-field-' + tag + '2').remove();
            });
            $(e).closest('tr').remove();

            var index = selectedForms.indexOf(id);
            selectedForms.splice(index, 1);
            console.log(selectedForms);
        }

        function setRequiredState(item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active')
            } else {
                $(item).addClass('active')
            }
        }

        $(document).ready(function () {


            $('#reset-btn1, #header-cancel-btn').click(function () {
                resetBtn();
            });
            $('#header-save-btn, #submit-btn-form').click(function () {
                submitForm();
            });

            // ********** Get forms via remote service *******************
            var searchTerm = null;
            var forms = [];

            var remoteDataConfig = {
                placeholder: "Выберите поле...",
                width: 300,
                minimumInputLength: 0,
                ajax: {
                    url: '/{{{$company->name}}}/saas/ajax/forms/get',
                    dataType: 'json',
                    type: 'POST',
                    data: function (term, page) {
                        searchTerm = term;
                        return {
                            en_type: 2,
                            metaData: {
                                filter: {
                                    label: term,
                                    en_type: 2
                                }
                            }
                        }
                    },
                    processResults: function (data, page) {
                        forms = data.forms;
                        return {
                            results: $.map(data.forms, function (obj) {
                                if ($.inArray(parseInt(obj.id), selectedForms) === -1) {
                                    return {id: obj.id, text: obj.label};
                                }
                            })
                        };
                    }
                },
                formatResult: function (option) {
                    return "<div>" + option.label + "</div>";
                },
                formatSelection: function (option) {
                    return option.label;
                }

            };
            $("#field_selector").select2(remoteDataConfig)
                    .on("change", function (e) {
                        var id = parseInt(e.currentTarget.value);
                        selectedForms.push(id);
                        var resultField = $.grep(forms, function (e) {
                            return e.id == id;
                        })[0];
                        if (resultField) {
                            var row_source = $('#template-custom-field-row').html();
                            var row_template = Handlebars.compile(row_source);
                            $('#form-fields tbody').append(row_template({
                                label: resultField.label,
                                id: resultField.id,
                                name: resultField.name
                            }));
                            $('#field_selector').select2("val", "");
                            // Добавление в форму
                            var source = $('#template-custom-text-field').html();
                            var field_template = Handlebars.compile(source);
                            var tag = resultField.name;
                            var label = resultField.label;
                            $(field_template({
                                label: label,
                                number: '',
                                tag: tag
                            })).insertAfter($('#form-preview .form-group').last()).show('slow');
                            $(field_template({
                                label: label,
                                number: 2,
                                tag: tag
                            })).insertAfter($('#popup-form-preview .form-group').last()).show('slow');
                        }
                    });
            // ************** Get forms via remote service ****************


            var errors = @if($errors->any())"{{$errors->first()}}"
            @else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"
            @else null @endif;
            if (success) {
                toastr.success(success);
            }


            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

            $('#add-field-btn').on('click', function (e) {
                var template = '<tr> \
                <td><input type="text" name="field-name" value=""/></td> \
                <td><input type="text" name="field-tag" value="<>"/></td> \
                <td><input type="text" name="field-header" value=""/></td> \
                <td><div class="checkbox checkbox-styled"><label><input name="requered" type="checkbox" value=""><span></span></label></div></td> \
                <td><a href="#" onclick="deleteField(this)" class="btn btn-icon-toggle ink-reaction"><i class="md md-delete"></i></a></td> \
                </tr>';
                $('#form-fields tbody').append(template);
            });


            $("#input_title").keyup(function () {
                $('#title, #modal_title').text($(this).val());
            });
            $("#input_after_title").keyup(function () {
                $('#after_title, #modal_after_title').text($(this).val());
            });
            $("#input_before_btn").keyup(function () {
                $('#before_btn, #modal_before_btn').text($(this).val());
            });
            $("#input_btn").keyup(function () {
                $('#btn, #modal_btn').text($(this).val());
            });
            $("#input_after_btn").keyup(function () {
                $('#after_btn, #modal_after_btn').text($(this).val());
            });
            $("#input_text1").change(function () {
                $('#text1, #modal_text1').text($(this).val());
            });
            $("#choice_color").change(function () {
                $("#btn, #modal_btn").css("background-color", $("#choice_color").val()).css("border-color", $("#choice_color").val());
            });
            initFieldValues();
        });
    </script>
    <script>
        $(function () {
            $('#choice-color').colorpicker();

        });
    </script>
@stop