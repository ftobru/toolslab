@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')

    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/replacement-geo">Geo-замены</a></li>
            <li class="active">Редактирование geo-замены</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование geo-замены</h2>
            </div>
        </div>
        <form id="edit_form" method="POST" action="/{{{$company->name}}}/landingControl/replacement-geo/edit/{{{$geo->id}}}" class="form-horizontal edit_links" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular33" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название города в именительном падеже">Город <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$geo->city->city}}}" name="city" type="text" class="form-control" id="regular33" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular34" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название города в предложном падеже">Город(е) <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$geo->city->city_e}}}" name="city_e" type="text" class="form-control" id="regular34" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular35" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название города в родительном падеже">Город(а) <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$geo->city->city_a}}}" name="city_a" type="text" class="form-control" id="regular35" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular36" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите код города">Телефон <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$geo->phone}}}" name="phone" type="text" class="form-control" id="regular36" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular37" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите код города">Адрес <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$geo->address}}}" name="address" type="text" class="form-control" id="regular37" placeholder="">
                                </div>
                            </div>
                            <div class="margin-top-lg">
                                <a class="text-primary margin-bottom-xxl">+ Добавить еще</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-xxl">
                <div class="col-sm-12">
                    <ul class="list-unstyled list-inline">
                        <li>
                            <button type="button" id="reset-btn1" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                        </li>
                        <li>
                            <button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    </section>
@stop

@section('modals')
@stop

@section('scripts')

    <script type="text/javascript">

        function resetBtn() {
            location.reload();
        }

        $(document).ready(function () {

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                $('#edit_form').submit();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop