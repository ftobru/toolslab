@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_delete')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/geo-splits">GEO-сплиты</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование geo-сплита</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <div class="row">
            <form id="geo-split-form" class="form-horizontal" role="form">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular105" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Название <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control" id="regular105" placeholder="" value="{{{$split->name}}}">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular106" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Описание <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="regular106" placeholder="">{{{$split->description}}}</textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="domain_selector" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Домен <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <select name="domain_id" id="domain_selector" class="form-control select2-list" data-placeholder="">
                                        <option>&nbsp;</option>
                                        @foreach($domains as $domain)
                                        <option value="{{{$domain->id}}}">{{{$domain->name}}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="url_input" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="url" type="text" class="form-control" id="url_input" placeholder="" value="{{{$split->url}}}">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="checkbox" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Введите путь">Открытый URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9 opacity-75">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            <input name="open_url" type="checkbox" id="checkbox"><span> Открытый URL показывает URL конечного элемента.<br/>Закрытый - URL гео-сплита</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Введите путь">Ссылка <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <div class="form-control-alt opacity-75"><a id="generated_link"  href="#">Необходимо выбрать домен</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head">
                        <header class="text-primary">Распределить трафик по городам<span class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover" data-placement="auto" data-content='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos quis beatae minus fuga cupiditate magni voluptatem in fugiat ut doloribus veniam eius blanditiis dolorum hic consequuntur non placeat, ratione cumque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique hic, nam culpa ad labore, iste sed deleniti molestias deserunt facilis exercitationem ea aliquam esse necessitatibus corporis, sapiente quia veritatis odit.'><i class="md md-help"></i></span></header>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="links_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>URL</th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Просмотры"><i
                                                    class="md md-visibility"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Уникальные посетители"><i
                                                    class="md md-person"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Отклики/заявки"><i
                                                    class="md md-assignment-turned-in"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Продажи"><i
                                                    class="md md-attach-money"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Конверсия в отклики"><i
                                                    class="md">CV1</i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Конверсия в продажи"><i
                                                    class="md">CV2</i></span></th>
                                    <th style="min-width:100px;">Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($cities_directions)
                                    @foreach($cities_directions as $key => $direct)
                                        @foreach(array_get($cities_directions[$key], 'links', []) as $link)
                                            <tr data-bind-geokey="{{{$key}}}" data-bind-linkid="{{{$link['id']}}}">
                                                <td>{{{$cities_directions[$key]['name']}}}</td>
                                                <td><a href="#">{{{$link['link']}}}</a></td>
                                                <td>{{{$link['hits']}}}</td>
                                                <td>{{{$link['hosts']}}}</td>
                                                <td>?</td>
                                                <td>{{{$link['count_leads']}}}</td>
                                                <td>?</td>
                                                <td>?</td>
                                                <td>
                                                    <a href="#" onclick="removeDirect({{{$link['id']}}}, '{{{$key}}}')"
                                                       class="btn btn-icon-toggle"><i class="md md-delete"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <ul class="list-inline">
                                <li><a href="/{{{$company->name}}}/landingControl/links/geodirect?tmp={{{$split->id}}}" class="text-primary" data-original-title="" title=""><i class="md md-add"></i> Добавить направление</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('modals')
    <div class="modal fade" id="modal1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="overflow: scroll">
                <div class="modal-header text-center">
                    <!-- <h3 class="modal-title text-primary"></h3> -->
                    <div class="headerbar">
                        <div class="pull-left">
                            <ul class="list-unstyled header-nav-options list-inline">
                                <li class="dropdown">
                                    <a class="btn btn-icon-toggle filter_btn opacity-50" data-toggle="dropdown">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                    <ul class="dropdown-menu list-unstyled width-6 style-default-light small-padding" role="menu" aria-labelledby="dLabel">
                                        <li>
                                            <form class="form">
                                                <span class="text-primary text-lg">Фильтр</span>
                                                <ul>
                                                    <li>
                                                        <div class="form-group no-padding no-margin">
                                                            <label id="autocomplete1" class="form-control autocomplete" data-source="../../html/forms/data/countries.json.html" style="display:none;"></label>
                                                            <select class="form-control select2-list input-sm" data-placeholder="Состояние">
                                                                <option>&nbsp;</option>
                                                                <option value="т0">Все</option>
                                                                <option value="т1">Новые</option>
                                                                <option value="т2">Активные</option>
                                                                <option value="т3">Успешные</option>
                                                                <option value="т3">Неуспешные</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group no-padding no-margin">
                                                            <select class="form-control select2-list input-sm" data-placeholder="Пункт2">
                                                                <option>&nbsp;</option>
                                                                <option value="т0">Все</option>
                                                                <option value="т1">Новые</option>
                                                                <option value="т2">Активные</option>
                                                                <option value="т3">Успешные</option>
                                                                <option value="т3">Неуспешные</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </form>
                                        </li>

                                    </ul>
                                </li><!--end .dropdown -->
                                <li>
                                    <!-- Search form -->
                                    <form class="navbar-search" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="headerSearch" placeholder="Вы ищите...">
                                        </div>
                                        <button type="submit" class="btn btn-icon-toggle ink-reaction opacity-50"><i class="fa fa-search"></i></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="k-grid-container" id="grid"></div>

                    <ul class="margin-bottom-xxl text-right list-inline">
                        <li><button type="submit" class="btn ink-reaction btn-default-light">Отменить</button></li>
                        <li><button type="button" id="showSelection" class="btn ink-reaction btn-primary">Применить</button></li>
                    </ul>

                </div>

            </div>
        </div>
    </div>
@stop

@section('scripts')


    <script type="text/javascript">

        var validator;
        $('select[name="domain_id"]').val({{{$split->domain_id}}});
        $('input[name="open_url"]').prop('checked', @if($split->open_url) true @else false @endif);

        var curDomain = null;
        var curUrl = '';
        function updateLink() {
            if(curDomain) {
                $('#generated_link').attr('target', "_blank");
                $('#generated_link').attr('href', curDomain + '/' + curUrl + "?nostat");
                $('#generated_link').text(curDomain + '/' + curUrl);
            }
        }

        // Удаляет ссылочку из таблицы ссылок по id
        function removeDirect(id, key) {

                var data = {
                    link_id: id,
                    key: key,
                    en_id: {{{$split->id}}}
                };

                $.ajax({
                    url: "/{{{$company->name}}}/landingControl/ajax/geoSplits/removeDirect",
                    method: "POST",
                    context: {id:id, key:key},
                    data: data,
                    success: function (result) {
                        toastr.success("Ссылка успешно отсоединена");
                        $('tr[data-bind-geokey='+key+'][data-bind-linkid='+id+']').remove();
                    },
                    error: function (result) {
                        toastr.error("Произошла ошибка");
                    }
                });
        }

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/geo-splits";
        }

        // Удаляет гео сплит
        function remove() {

            var data = {
                id: {{{$split->id}}}
            };

            $.ajax({
                url: "/{{{$company->name}}}/landingControl/ajax/geoSplits/delete",
                method: "POST",
                data: data,
                success: function (result) {
                    toastr.success('Успешно удалено.');
                    window.location.href = "/{{{$company->name}}}/landingControl/geo-splits";
                },
                error: function (result) {
                    toastr.error("Произошла ошибка при попытке удаления");
                }
            });
        }

        validator = $('#geo-split-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "domain_id": {
                            required: true
                        }
                    },
                    messages: {
                        "domain_id": {
                            required: "Выберите домен"
                        },
                        "name": {
                            required: "Введите название сплита",
                            minlength: "Минимальная длинна названия сплита - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#geo-split-form').valid()) {
              var name = $('#geo-split-form input[name="name"]').val();
              var description = $('#geo-split-form textarea[name="description"]').val();
              var domain_id = $('#geo-split-form select[name="domain_id"]').val();
              var url =  $('#geo-split-form input[name="url"]').val();
              var open_url = $('#geo-split-form input[name="open_url"]').prop("checked");


              var data = {
                  name: name,
                  description: description,
                  domain_id: domain_id,
                  url: url,
                  open_url: open_url,
                  id: {{{$split->id}}}
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/geoSplits/update",
                  method: "PUT",
                  data: data,
                  success: function (result) {
                      toastr.success("Geo-split успешно обновлен");
                      //window.location.href = "/{{{$company->name}}}/landingControl/geo-split/edit/{{{$split->id}}}";
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка при обновлении");
                  }
              });
          }
        }

        $(document).ready(function () {
            $('#domain_selector').select2()
                    .on("change", function(e) {
                        var $this = $(this);
                        var domain_id = $this.val();
                        curDomain = $("#domain_selector option[value="+ domain_id +']').text();
                        updateLink();
                    });

            $('#url_input').on("change", function(e) {
                var $this = $(this);
                curUrl = $this.val();
                updateLink();
            });

            // init link
            var domain_id = $('#domain_selector').val();
            curDomain = $("#domain_selector option[value="+ domain_id +']').text();
            curUrl = $('#url_input').val();
            updateLink();

            $('#reset-btn1, #header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#header-delete-btn').click(function() {
                remove();
            });

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });
        });

    </script>
@stop