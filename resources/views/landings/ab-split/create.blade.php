@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/ab-splits">AB-сплиты</a></li>
            <li class="active">создание</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Создание ab-сплита</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <div class="row">
            <form id="geo-split-form" class="form-horizontal" role="form">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular105" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Название <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control" id="regular105" placeholder="" value="{{{$temp->name}}}">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular106" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Описание <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="regular106" placeholder="">{{{$temp->description}}}</textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="domain_selector" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Домен <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <select name="domain_id" id="domain_selector" class="form-control select2-list" data-placeholder="">
                                        <option>&nbsp;</option>
                                        @foreach($domains as $domain)
                                        <option value="{{{$domain->id}}}">{{{$domain->name}}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="url_input" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="url" type="text" class="form-control" id="url_input" placeholder="" value="{{{$temp->url}}}">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="checkbox" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Введите путь">Открытый URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9 opacity-75">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            <input name="open_url" type="checkbox" id="checkbox"><span> Открытый URL показывает URL конечного элемента.<br/>Закрытый - URL гео-сплита</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Введите путь">Ссылка <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <div class="form-control-alt opacity-75"><a id="generated_link"  href="#">Необходимо выбрать домен</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head">
                        <header class="text-primary">Распределить трафик равномерно <span class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover" data-placement="auto" data-content='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos quis beatae minus fuga cupiditate magni voluptatem in fugiat ut doloribus veniam eius blanditiis dolorum hic consequuntur non placeat, ratione cumque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique hic, nam culpa ad labore, iste sed deleniti molestias deserunt facilis exercitationem ea aliquam esse necessitatibus corporis, sapiente quia veritatis odit.'><i class="md md-help"></i></span></header>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="links_table" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>URL</th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Просмотры"><i
                                                    class="md md-visibility"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Уникальные посетители"><i
                                                    class="md md-person"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Отклики/заявки"><i
                                                    class="md md-assignment-turned-in"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Продажи"><i
                                                    class="md md-attach-money"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Конверсия в отклики"><i
                                                    class="md">CV1</i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip"
                                              data-placement="auto" title="Конверсия в продажи"><i
                                                    class="md">CV2</i></span></th>
                                    <th style="min-width:100px;">Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($links as $link)
                                    <tr data-bind-id="{{{$link['id']}}}">
                                        <td>{{{$link['name']}}}</td>
                                        <td><a href="#">{{{$link['link']}}}</a></td>
                                        <td>{{{$link['hits']}}}</td>
                                        <td>{{{$link['hosts']}}}</td>
                                        <td>?</td>
                                        <td>{{{$link['count_leads']}}}</td>
                                        <td>?</td>
                                        <td>?</td>
                                        <td>
                                            <a href="#" onclick="removeDirect({{{$link['id']}}})"
                                               class="btn btn-icon-toggle"><i class="md md-delete"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <ul class="list-inline">
                                <li><span class="text-lg">Добавить направление:</span></li>
                                <li class="text-primary links no-padding"><span id="open_link_grid_btn" data-toggle="modal" data-target="#modal1">Выбрать ссылку</span></li>
                                <li class="text-primary links no-padding"><a href="/{{{$company->name}}}/landingControl/links/constructor?tmp={{{$temp->id}}}">Конструктор ссылок</a></li>
                                <li class="text-primary links no-padding"><a href="/{{{$company->name}}}/landingControl/links/multiplier?tmp={{{$temp->id}}}">Перемножатор</a></li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('modals')
    <div class="modal fade" id="modal1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" style="overflow: scroll; width:100%;">
                <div class="modal-header text-center">
                    <div class="headerbar">
                        <div class="pull-left">
                            <ul class="list-unstyled header-nav-options list-inline">
                                <li class="dropdown">
                                    <a class="btn btn-icon-toggle filter_btn opacity-50" data-toggle="dropdown">
                                        <i class="fa fa-filter"></i>
                                    </a>
                                    <ul class="dropdown-menu list-unstyled width-6 style-default-light small-padding" role="menu" aria-labelledby="dLabel">
                                        <li>
                                            <form class="form">
                                                <span class="text-primary text-lg">Фильтр</span>
                                                <ul>
                                                    <li>
                                                        <div class="form-group no-padding no-margin">
                                                            <label id="autocomplete1" class="form-control autocomplete" data-source="../../html/forms/data/countries.json.html" style="display:none;"></label>
                                                            <select class="form-control select2-list input-sm" data-placeholder="Состояние">
                                                                <option>&nbsp;</option>
                                                                <option value="т0">Все</option>
                                                                <option value="т1">Новые</option>
                                                                <option value="т2">Активные</option>
                                                                <option value="т3">Успешные</option>
                                                                <option value="т3">Неуспешные</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="form-group no-padding no-margin">
                                                            <select class="form-control select2-list input-sm" data-placeholder="Пункт2">
                                                                <option>&nbsp;</option>
                                                                <option value="т0">Все</option>
                                                                <option value="т1">Новые</option>
                                                                <option value="т2">Активные</option>
                                                                <option value="т3">Успешные</option>
                                                                <option value="т3">Неуспешные</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <!-- Search form -->
                                    <form class="navbar-search" role="search">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="headerSearch" placeholder="Вы ищите...">
                                        </div>
                                        <button type="submit" class="btn btn-icon-toggle ink-reaction opacity-50"><i class="fa fa-search"></i></button>
                                    </form>
                                </li>
                            </ul>
                        </div>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="k-grid-container" id="grid"></div>
                    <ul class="margin-bottom-xxl text-right list-inline">
                        <li><button type="submit" class="btn ink-reaction btn-default-light">Отменить</button></li>
                        <li><button type="button" id="showSelection" class="btn ink-reaction btn-primary">Применить</button></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    {{--ГРИД--}}
    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>


    <script type="text/javascript">

        var validator;
        $('select[name="domain_id"]').val({{{$temp->domain_id}}});
        $('input[name="open_url"]').prop('checked', @if($temp->open_url) true @else false @endif);

        var curDomain = null;
        var curUrl = '';
        function updateLink() {
            if(curDomain) {
                $('#generated_link').attr('target', "_blank");
                $('#generated_link').attr('href', curDomain + '/' + curUrl + "?nostat");
                $('#generated_link').text(curDomain + '/' + curUrl);
            }
        }


        /*грид*/
        GridConfig = {};
        window.Filters = {};
        FilterBuilder = {};

        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/landingControl/ajax/links/';
        // Конфиг колонок грида
        GridConfig.columns = [
//            {
//                field: "id",
//                title: "ID",
//                width: "50px",
//                locked: true,
//                lockable: false
//            },
            {
                title: '',
                template: "<input type='checkbox' class='checkbox' />",
                width: "40px"
            },
            {
                field: "type",
                title: 'Тип',
                template: "#= cellTemplate(type, 'type') #",
                width: "130px"
            },
            {
                field: "tag",
                title: 'Тег',
                width: "130px"
            },
//            {
//                field: "name",
//                title: "Название",
//                width: "220px",
//                locked: true,
//                template: "#= cellTemplate(name, 'name') #",
//                lockable: false
//            },
            {
                field: "link",
                title: "Ссылка",
                width: "220px"
            },
            {
                field: "short_link",
                title: "Короткая ссылка",
                width: "130px",
            },
            {
                field: "link_base",
                title: "Основа",
                template: "#= cellTemplate(link_base, 'link_base') #",
                width: "130px"
            },
            {
                field: "replacement_product",
                title: "Товар-замена",
                template: "#= cellTemplate(replacement_product, 'replacement_product') #",
                width: "130px"
            },
            {
                field: "replacement_link",
                title: "Link-замена",
                template: "#= cellTemplate(replacement_link, 'replacement_link') #",
                width: "130px"
            },
            {
                field: "insert_link",
                title: "Link-вставка",
                template: "#= cellTemplate(insert_link, 'insert_link') #",
                width: "130px"
            },
            {
                field: "utm",
                title: "UTM метка",
                template: "#= cellTemplate(utm, 'utm') #",
                width: "130px"
            },
            {
                field: "open_url",
                title: 'Открытая',
                template: "#= cellTemplate(open_url, 'open-url') #",
                width: "130px"
            },
            {
                field: "domain",
                title: "Домен",
                template: "#= cellTemplate(domain, 'domain-name') #",
                width: "130px",
                editor: "readonlyEditor"
            },
//            {
//                field: "created_at",
//                title: "Созданa",
//                format: "{0: dd-MM-yyyy HH:mm:ss}",
//                width: "130px"
//            },
//            {
//                field: "updated_at",
//                title: "Измененa",
//                format: "{0: dd-MM-yyyy HH:mm:ss}",
//                width: "130px"
//            },
//            {
//                command: [
//                    {name: "card", text: "", click: abSplitEdit , className: "md md-list", width: "32px"}
//                ],
//                title: "&nbsp;",
//                width: "170px",
//                attributes: {"class": "button-column"}
//            }
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string", editable: false},
                domain: {editable: false},
                utm: {editable: false},
                insert_link: {editable: false},
                replacement_link: {editable: false},
                replacement_product: {editable: false},
                link_base: {editable: false},
                short_link: {editable: false},
                tag: {editable: false},
                type: {editable: false},
                link: {editable: false},
                url: {type: "string", editable: false},
                open_url: {type: "boolean", editable: false},
                description: {type: "string", editable: false},
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'name',
            'domain_id',
            'url',
            'open_url',
            'description',
            'date_start',
            'date_end',
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'name',
            'domain_id',
            'url',
            'open_url',
            'description',
            'date_start',
            'date_end',
        ];

        // Карточка сущности.
        function abSplitEdit(e) {
            e.preventDefault();
            var item = this.dataItem($(e.currentTarget).closest("tr"));
            var idx = this.dataSource.indexOf(item);
            var entityID = item.id;
            window.location.href = window.location.protocol + '//' + window.location.host + GridConfig.baseCardUrl + entityID;
        }

        function cellTemplate(value, type) {
            switch (type) {
                case 'domain-name':
                    if (value) {
                        return value.name;
                    }
                    return;
                    break;
                case 'link_base':
                    if(value) {
                        return value.name;
                    }
                    break;
                case 'replacement_product':
                    if(value) {
                        return value[0].name;
                    }
                    break;
                case 'insert_link':
                    if(value) {
                        return value[0].name;
                    }
                    break;
                case 'replacement_link':
                    if(value) {
                        return value[0].name;
                    }
                    break;
                case 'utm':
                    if(value) {
                        return value[0].name;
                    }
                    break;

                case 'type':
                    switch (value) {
                        case 0:
                            return 'Шаблон';
                            break;
                        case 1:
                            return 'АБ-сплит';
                            break;
                        case 2:
                            return 'Гео-сплит';
                            break;
                        default:
                            return '-';
                            break;
                    }
                    return;
                    break;
                case 'open-url':
                    if (value) {
                        return 'Да';
                    }
                    return 'Нет';
                    break;
                case 'name':
                    return '<a href="' + GridConfig.baseCardUrl +'">' + value +'</a>';
                    break;
            }
            return '-';
        }

        var checkedIds = {
            @foreach($temp->link_ids as $id)
                {{{$id}}}: true,
            @endforeach
        };

        // Удаляет ссылочку из таблицы ссылок по id
        function removeDirect(id) {
            if(checkedIds.hasOwnProperty(id)){
                delete checkedIds[id];

                var checked = [];
                for(var i in checkedIds){
                    if(checkedIds[i]){
                        checked.push(i);
                    }
                }

                var data = {
                    link_ids: checked,
                    id: {{{$temp->id}}}
                };

                $.ajax({
                    url: "/{{{$company->name}}}/landingControl/ajax/abSplits/updateTmp",
                    method: "POST",
                    context: id,
                    data: data,
                    success: function (result) {
                        toastr.success("Ссылка успешно отсоединена");
                        $('tr[data-bind-id='+id+']').remove();
                    },
                    error: function (result) {
                        toastr.error("Произошла ошибка");
                        checkedIds[id] = true;
                    }
                });
            }
        }

        //on click of the checkbox:
        function selectRow() {
            var checked = this.checked,
                    row = $(this).closest("tr"),
                    grid = $("#grid").data("kendoGrid"),
                    dataItem = grid.dataItem(row);

            checkedIds[dataItem.id] = checked;
            if (checked) {
                //-select the row
                row.addClass("k-state-selected");
            } else {
                //-remove selection
                row.removeClass("k-state-selected");
                delete checkedIds[dataItem.id];
            }
        }
        //on dataBound event restore previous selected rows:
        function onDataBound(e) {
            var view = this.dataSource.view();
            for(var i = 0; i < view.length;i++){
                if(checkedIds[view[i].id]){
                    this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked","checked");
                }
            }
        }

            /*--грид--*/



        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/ab-splits";
        }

        validator = $('#geo-split-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "domain_id": {
                            required: true
                        }
                    },
                    messages: {
                        "domain_id": {
                            required: "Выберите домен"
                        },
                        "name": {
                            required: "Введите название сплита",
                            minlength: "Минимальная длинна названия сплита - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function updateTempEntity(links_deleted, links_added) {

            links_deleted = typeof links_deleted !== 'undefined' ? links_deleted : null;
            links_added = typeof links_added !== 'undefined' ? links_added : null;


            var name = $('#geo-split-form input[name="name"]').val();
            var description = $('#geo-split-form textarea[name="description"]').val();
            var domain_id = $('#geo-split-form select[name="domain_id"]').val();
            var url =  $('#geo-split-form input[name="url"]').val();
            var open_url = $('#geo-split-form input[name="open_url"]').prop("checked");

            var checked = [];
            for(var i in checkedIds){
                if(checkedIds[i]){
                    checked.push(i);
                }
            }

            var data = {
                name: name,
                description: description,
                url: url,
                open_url: open_url,
                link_ids: checked,
                id: {{{$temp->id}}}
            };

            if(!isNaN(parseInt(domain_id))) {
                data['domain_id'] = domain_id;
            }

            $.ajax({
                url: "/{{{$company->name}}}/landingControl/ajax/abSplits/updateTmp",
                method: "POST",
                data: data,
                context: {
                    links_deleted: links_deleted,
                    links_added: links_added
                },
                success: function (result) {
                    // Просто дропаем строки
                    if (links_deleted && links_deleted.length > 0) {
                        for(var index in links_deleted) {
                            $('tr[data-bind-id=' + links_deleted[index] + ']').remove();
                        }
                    }
                    if (links_added && links_added.length > 0) {
                        // добавляем в табличку строки и всё.
                        var data = {id: links_added};
                        $.ajax({
                            url: "/{{{$company->name}}}/landingControl/ajax/links/getByID",
                            method: "POST",
                            data: data,
                            success: function (result) {
                                for(var key_link in result) {
                                    var link = result[key_link];
                                    var rowTemplate = '<tr data-bind-id="' + link.id + '"> \
                                    <td>'+link.name+'</td> \
                                    <td><a href="#">'+link.link+'</a></td> \
                                    <td>' + link.hits + '</td> \
                                    <td>' + link.hosts + '</td> \
                                    <td>?</td> \
                                    <td>' + link.count_leads + '</td> \
                                    <td>?</td> \
                                    <td>?</td> \
                                    <td> \
                                    <a href="#" onclick="removeDirect('+ link.id +')" \
                                    class="btn btn-icon-toggle"><i class="md md-delete"></i></a> \
                                    </td> \
                                    </tr>';

                                    $('#links_table tbody').append(rowTemplate);
                                }
                            },
                            error: function (result) {
                                toastr.error("Произошла ошибка");
                            }
                        });


                    }
                },
                error: function (result) {
                    toastr.error("Произошла ошибка сохранения");
                }
            });
        }

        function submitForm() {
          if( $('#geo-split-form').valid()) {
              var name = $('#geo-split-form input[name="name"]').val();
              var description = $('#geo-split-form textarea[name="description"]').val();
              var domain_id = $('#geo-split-form select[name="domain_id"]').val();
              var url =  $('#geo-split-form input[name="url"]').val();
              var open_url = $('#geo-split-form input[name="open_url"]').prop("checked");

              var checked = [];
              for(var i in checkedIds){
                  if(checkedIds[i]){
                      checked.push(i);
                  }
              }

              var data = {
                  name: name,
                  description: description,
                  domain_id: domain_id,
                  url: url,
                  link_ids: checked,
                  open_url: open_url,
                  id: {{{$temp->id}}}
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/abSplits/saveTmp",
                  method: "POST",
                  data: data,
                  success: function (result) {
                      toastr.success("AB-split успешно создан");
                      window.location.href = "/{{{$company->name}}}/landingControl/ab-split/edit/{{{$temp->id}}}";
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка сохранения");
                  }
              });
          }
        }

        var tempCheckedIds = $.extend(true, {}, checkedIds);

        $(document).ready(function () {
            $('#domain_selector').select2()
                    .on("change", function(e) {
                        var $this = $(this);
                        var domain_id = $this.val();
                        if(domain_id) {
                            curDomain = $('#domain_selector option[value='+ domain_id +']').text();
                            updateLink();
                        }
                    });

            $('#url_input').on("change", function(e) {
                var $this = $(this);
                curUrl = $this.val();
                updateLink();
            });

            // init link
            var domain_id = $('#domain_selector').val();
            if(domain_id !== '') {
                curDomain = $("#domain_selector option[value="+ domain_id +']').text();
                curUrl = $('#url_input').val();
                updateLink();
            }

            // Временная переменная.
            $('#modal1').on('hidden.bs.modal', function () {
                checkedIds = $.extend(true, {}, tempCheckedIds);
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
                console.log('hidden.bs.modal темп=чеклист');
            });
            $('#modal1').on('shown.bs.modal', function () {
                tempCheckedIds = $.extend(true, {}, checkedIds);
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
                console.log('shown.bs.modal чеклист=темп');
            });

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('#regular105, #regular106, #domain_selector, #url_input, #checkbox').change(function(){
                updateTempEntity();
            });

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

            // инит грида
            Filters = {
                isInit: false,

                city_input: null,
                phone_input: null,
                address_input: null,

                // Селекторы дат
                date_start: null,
                date_end: null,

                initFilters: function () {

                    var self = this;

                    this.city_input = $('#filter-link-name');
                    this.phone_input = $('#filter-link-code');
                    this.address_input= $('#filter-link-param');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');

                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    this.city_input.keyup(function () {
                        FilterBuilder.addFilter('name', {
                            field: "name",
                            operator: "startWith",
                            value: self.city_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.phone_input.keyup(function () {
                        FilterBuilder.addFilter('code', {
                            field: "code",
                            operator: "startWith",
                            value: self.phone_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.address_input.keyup(function () {
                        FilterBuilder.addFilter('param', {
                            field: "param",
                            operator: "startWith",
                            value: self.address_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });


                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },


                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "name" && value.operator == 'startWith') {
                        $('#filter-link-name').val(value.value);
                        FilterBuilder.addFilter('name', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                }
            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            }

//            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
//                GridController.init();
//                GridController.initGrid();
//            });

            var dataSource = {
                serverPaging: true,
                serverSorting: true,
                serverFiltering: true,
                serverGrouping: false,
                allowUnsort: true,
                type: "json",
                transport: {
                    read: function (e) {
                        GridConfig.gridPagerConfig.page = $('#grid')
                                .data("kendoGrid")
                                .dataSource.page();
                        $.ajax({
                            url: GridConfig.baseGridCrudUrl + "get",
                            method: "POST",
                            dataType: "json",
                            data: {
                                options: e.data
                            },
                            context: this,
                            success: function (result, status, xhr) {
                               /* if (!Filters.isInit) {
                                    Filters.firstInitFilters(result.filters_data); */
                                    Filters.isInit = true;

                                e.success(JSON.parse(JSON.stringify(result)));
                            },
                            error: function (xhr, status, error) {
                                e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                            }
                        });
                    }
                },
                batch: true,
                schema: {
                    data: 'data',
                    total: "total",
                    groups: "groups",
                    model: GridConfig.model
                },
                filter: this.filter,
                selectable: "multiple",
                pageSize: GridConfig.gridPagerConfig.perPage,
                page: GridConfig.gridPagerConfig.page
            };
            var grid = $('#grid').kendoGrid({
                mobile: true,
                dataSource: dataSource,
                groupable: false,
                columnMenu: true,
                //height: 650,
                //define dataBound event handler
                dataBound: onDataBound,
                filterable: true,
                scrollable: true,
                sortable: true,
                reorderable: false,
                editable: true, // true - for in-grid edit
                resizable: true,
                pageable: {
                    input: true,
                    numeric: true,
                    pageSize: GridConfig.gridPagerConfig.perPage,
                    pageSizes: [25, 100, 250]
                },
                columns: GridConfig.columns,
                columnMenuInit: function (e) {
                    var columnsItem = $(e.container[0]).find('li.k-columns-item');
                    $(columnsItem).next().remove();
                    $(columnsItem).remove();
                }
            }).data("kendoGrid");
            //bind click event to the checkbox
            grid.table.on("click", ".checkbox" , selectRow);

            $("#showSelection").bind("click", function () {
                console.log('showSelection click');
                var new_links = [];
                var deleted_links = [];
                for (var propertyName in checkedIds) {
                    if (!tempCheckedIds.hasOwnProperty(propertyName)) {
                        new_links.push(propertyName);
                    }
                }
                for (propertyName in tempCheckedIds) {
                    if (!checkedIds.hasOwnProperty(propertyName)) {
                        deleted_links.push(propertyName);
                    }
                }
                tempCheckedIds = $.extend(true, {}, checkedIds);
                $('#modal1').modal('hide');
                updateTempEntity(deleted_links, new_links);
            });
        });

    </script>
@stop