@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/utms">UTM-метки</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование utm</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <form id="utm-form" class="form-horizontal" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular43" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">Название шаблона <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$utm->name}}}" name="name" type="text" class="form-control" id="regular43" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular44" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">Описание <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="regular44" placeholder="">{{{$utm->description}}}</textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular45" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">utm_source <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$utm->utm_source}}}" name="utm_source" type="text" class="form-control" id="regular45" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular46" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">utm_compaign <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$utm->utm_compaign}}}" name="utm_compaign" type="text" class="form-control" id="regular46" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular47" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">utm_medium <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$utm->utm_medium}}}" name="utm_medium" type="text" class="form-control" id="regular47" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular48" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">utm_term <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$utm->utm_term}}}" name="utm_term" type="text" class="form-control" id="regular48" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular49" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего шаблона">utm_content <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input value="{{{$utm->utm_content}}}" name="utm_content" type="text" class="form-control" id="regular49" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-xxl">
                <div class="col-sm-12">
                    <ul class="list-unstyled list-inline">
                        <li><button id="reset-btn1" type="button" class="btn btn-default-light ink-reaction btn-sm">Отменить</button></li>
                        <li><button id="save_utm_btn" type="button" class="btn ink-reaction btn-primary btn-sm">Сохранить</button></li>
                    </ul>
                </div>
            </div>
        </form>
    </section>
@stop

@section('modals')
@stop

@section('scripts')

    <script type="text/javascript">

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/utms";
        }

        $('#utm-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "description": {
                            maxlength: 255
                        },
                        "utm_source": {
                            maxlength: 255
                        },
                        "utm_compaign": {
                            maxlength: 255
                        },
                        "utm_medium": {
                            maxlength: 255
                        },
                        "utm_term": {
                            maxlength: 255
                        },
                        "utm_content": {
                            maxlength: 255
                        }
                    },
                    messages: {
                        "name": {
                            required: "Введите название для UTM",
                            minlength: "Минимальная длинна названия - 1 символ",
                            maxlength: "Введите более короткое название"
                        },
                        "description": {
                            maxlength: "Максимальная длина описания 255 символов"
                        },
                        "utm_source": {
                            maxlength: "Максимальная длина 255 символов"
                        },
                        "utm_compaign": {
                            maxlength: "Максимальная длина 255 символов"
                        },
                        "utm_medium": {
                            maxlength: "Максимальная длина 255 символов"
                        },
                        "utm_term": {
                            maxlength: "Максимальная длина 255 символов"
                        },
                        "utm_content": {
                            maxlength: "Максимальная длина 255 символов"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#utm-form').valid()) {
              var name = $('#utm-form input[name="name"]').val();
              var description = $('#utm-form textarea[name="description"]').val();
              var utm_source = $('#utm-form input[name="utm_source"]').val();
              var utm_compaign = $('#utm-form input[name="utm_compaign"]').val();
              var utm_medium = $('#utm-form input[name="utm_medium"]').val();
              var utm_term = $('#utm-form input[name="utm_term"]').val();
              var utm_content = $('#utm-form input[name="utm_content"]').val();

              var data = {
                  name: name,
                  description: description,
                  utm_source: utm_source,
                  utm_compaign: utm_compaign,
                  utm_medium: utm_medium,
                  utm_term: utm_term,
                  utm_content: utm_content,
                  id: {{{$utm->id}}}
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/utms/update",
                  method: "PUT",
                  data: data,
                  success: function (result) {
                      toastr.success("UTM успешно обновлена");
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка обновления");
                  }
              });
          }
        }


        $(document).ready(function () {

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#save_utm_btn').click(function() {
                submitForm();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });
        });
    </script>
@stop