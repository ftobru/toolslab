
@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_add_simple')
@stop

@section('switcher')
@stop

@section('content')
    <div class="offcanvas">
    </div>

    <div class="card tab-content filter-page">
        <div class="filter-menu" id="off-canvas1" style="left: 60px;">
            <nav>
                <div class="expanded">
                    <a href="../../html/dashboards/dashboard.html">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label no-margin">
                                <input type="text" class="form-control input-sm" style="margin-top: 8px;" id="filter-link-name">
                                <label for="filter-link-name">Название:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group floating-label">
                                <input type="text" class="form-control input-sm" style="margin-top: 8px;" id="filter-link-key">
                                <label for="filter-link-code">Ключ:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-end" name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
            <div class="section-body">
                <div class="table_wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card" id="droptarget-hidden-columns">
                                <div class="card-head">
                                    <header>Скрытые столбцы</header>
                                </div>
                                <div class="card-body">
                                    <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                        <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                    </div>
                                </div>
                                <!--end .card-body -->
                            </div>

                            {{--<div><span class="print-me">[Печать]</span></div>--}}
                            <div class="k-grid-container" id="grid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!--end content-->
@stop

@section('modals')

@stop

@section('scripts')

    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>

    <script type="text/javascript">

        GridConfig = {};
        window.Filters = {};
        FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 1;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_replacement_products_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/landingControl/ajax/replacementProducts/';
        // Путь до карты
        GridConfig.baseCardUrl = '/{{{$company->name}}}/landingControl/replacement-product/edit/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'replaceProducts';
        // Можно ли редактировать
        GridConfig.editable = false;
        // Можно ли делать группировку
        GridConfig.groupable = false;
        // Шаблон копирования строк
        GridConfig.copyFields = ['name'];
        // Конфиг колонок грида
        GridConfig.columns = [
            {
                field: "id",
                title: "ID",
                width: "50px",
                locked: true,
                lockable: false
            },
            {
                field: "name",
                title: "Название",
                width: "220px"
            },
            {
                field: "description",
                title: 'Описание',
                width: "200px"
            },
            {
                field: "key",
                title: "Ключ",
                width: "130px"
            },
            {
                field: "product",
                title: "Товар",
                template: "#= cellTemplate(product, 'product-name') #",
                width: "130px",
                editor: "readonlyEditor"
            },
            {
                field: "created_at",
                title: "Созданa",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px"
            },
            {
                field: "updated_at",
                title: "Измененa",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px"
            },
            {
                command: [
                    {name: "card", text: "", click: linkEdit, className: "md md-list", width: "32px"}
                ],
                title: "&nbsp;",
                width: "170px",
                attributes: {"class": "button-column"}
            }
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string"},
                code: {type: "string"},
                param: {type: "string"},
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'key',
            'name',
            'date_start',
            'date_end'
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'key',
            'name',
            'date_start',
            'date_end'
        ];

        // Карточка сущности.
        function linkEdit(e) {
            e.preventDefault();
            var item = this.dataItem($(e.currentTarget).closest("tr"));
            var idx = this.dataSource.indexOf(item);
            var entityID = item.id;

            window.location.href = window.location.protocol + '//' + window.location.host + GridConfig.baseCardUrl + entityID;
        }

        function cellTemplate(value, type) {
            switch (type) {
                case 'product-name':
                    if (value) {
                        return value.name;
                    }
                    return;
                    break;
            }
            return '[unknown]';
        }

        $(document).ready(function () {

            $("#header_add_simple_btn").attr("href", "/{{{$company->name}}}/landingControl/replacement-product/create");

            Filters = {

                isInit: false,

                city_input: null,
                key_input: null,
                address_input: null,

                // Селекторы дат
                date_start: null,
                date_end: null,

                initFilters: function () {

                    var self = this;

                    this.city_input = $('#filter-link-name');
                    this.key_input = $('#filter-link-key');
                    this.address_input= $('#filter-link-param');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');

                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    this.city_input.keyup(function () {
                        FilterBuilder.addFilter('name', {
                            field: "name",
                            operator: "startWith",
                            value: self.city_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.key_input.keyup(function () {
                        FilterBuilder.addFilter('key', {
                            field: "key",
                            operator: "startWith",
                            value: self.key_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.address_input.keyup(function () {
                        FilterBuilder.addFilter('param', {
                            field: "param",
                            operator: "startWith",
                            value: self.address_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });


                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },


                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "name" && value.operator == 'startWith') {
                        $('#filter-link-name').val(value.value);
                        FilterBuilder.addFilter('name', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                }
            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            }

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });

    </script>
@stop