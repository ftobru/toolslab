@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/replacement-products">Товар-замены</a></li>
            <li class="active">создание</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Создание товар-замены</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <div class="row">
            <form id="replacement-product-form" class="form-horizontal" role="form">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular105" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Название <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control" id="regular105" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular106" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Описание <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="regular106" placeholder=""></textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="key_input" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите ">Ключ <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="key" type="text" class="form-control" id="key_input"  placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="split_domain" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Выберите домен">Товар <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <select name="product_id" id="split_domain" class="form-control select2-list" data-placeholder="">
                                        <option>&nbsp;</option>
                                        @foreach($products as $product)
                                            <option value="{{{$product->id}}}">{{{$product->name}}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@stop

@section('modals')

@stop

@section('scripts')

    <script type="text/javascript">



        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/replacement-products";
        }

        $('#replacement-product-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "key": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "product_id": {
                            required: true
                        }
                    },
                    messages: {
                        "product_id": {
                            required: "Выберите товар"
                        },
                        "name": {
                            required: "Введите название замены",
                            minlength: "Минимальная длинна названия замены - 1 символ",
                            maxlength: "Введите более короткое название"
                        },
                        "key": {
                            required: "Введите ключ",
                            minlength: "Минимальная длинна ключа - 1 символ",
                            maxlength: "Введите более короткий ключ"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#replacement-product-form').valid()) {
              var name = $('#replacement-product-form input[name="name"]').val();
              var description = $('#replacement-product-form textarea[name="description"]').val();
              var product_id = $('#replacement-product-form select[name="product_id"]').val();
              var key =  $('#replacement-product-form input[name="key"]').val();

              var data = {
                  name: name,
                  description: description,
                  product_id: product_id,
                  key: key
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/replacementProducts/create",
                  method: "POST",
                  data: data,
                  success: function (result) {
                      toastr.success("Товар-замена успешно создана");
                      window.location.href = "/{{{$company->name}}}/landingControl/replacement-products";
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка сохранения");
                  }
              });
          }
        }

        $(document).ready(function () {

            $('select[name="product_id"]').select2({ width: '100%' });

            $('#reset-btn1, #header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

            $('#tabs-1 label').click(function(e){
                var a = e;
                if(!$(e.currentTarget).hasClass("active")) {
                    $('#tabs-1 label').removeClass("active");
                    $(e.currentTarget).addClass("active");
                }
            });

        });
    </script>
@stop