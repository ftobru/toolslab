@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')

    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/replacement-links">Link-замены</a></li>
            <li class="active">создание</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Создание link-замены</h2>
            </div>
        </div>
        <form id="create_form" method="POST" action="/{{{$company->name}}}/landingControl/replacement-link/create" class="form-horizontal create_link" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular66" class="col-sm-4 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название link-вставки">Название <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-8">
                                    <input name="name" type="text" class="form-control" id="regular66" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular67" class="col-sm-4 control-label" data-toggle="tooltip" data-placement="auto" title="Введите параметр link-вставки">Параметр <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-8">
                                    <input name="param" type="text" class="form-control" id="regular67" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular68" class="col-sm-4 control-label" data-toggle="tooltip" data-placement="auto" title="Введите ключ-значение">Ключ-значение <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-8">
                                    <input name="key" type="text" class="form-control" id="regular68" placeholder="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите теги для товара">Тег вставки
                                    <span ><i class="md md-help"></i></span>
                                    <span class="popover-btn" data-html="true" data-container="body" data-toggle="popover" data-placement="auto" data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i class="md md-info"></i></span>
                                </label>
                                <div class="col-sm-9">
                                    <input name="code" type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular69" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите параметр">Контент<span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="value" class="form-control" id="regular69"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-xxl">
                <div class="col-sm-12">
                    <ul class="list-unstyled list-inline">
                        <li>
                            <button type="button" id="reset-btn1" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                        </li>
                        <li>
                            <button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button>
                        </li>
                    </ul>
                </div>
            </div>
        </form>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript">

        var elf = null;
        var fileLinks = {};
        var currentFileSelectorId = '';

        function processSelectedFile(filePath, elemId) {
            fileLinks[elemId] = filePath;
            var filename = filePath.replace(/^.*[\\\/]/, '')
            $('#'+elemId).val(filename);

            var data = JSON.stringify(fileLinks);
            $('#fileLinks').val(data);
        }

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/replacement-links";
        }

        $(document).ready(function () {

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                $('#create_form').submit();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop