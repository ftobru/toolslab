@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_delete')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/links">Ссылки</a></li>
            <li class="active">конструктор</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование ab-сплита</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <div class="row">
            <form id="ab-split-form" class="form-horizontal" role="form">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular105" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Название <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" value="{{{$abSplit->name}}}" type="text" class="form-control" id="regular105" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular106" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Описание <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="description" class="form-control" id="regular106" placeholder="">{{{$abSplit->description}}}</textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular107" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Домен <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <select name="domain_id" id="regular107" class="form-control select2-list" data-placeholder="">
                                        <option>&nbsp;</option>
                                        @foreach($domains as $domain)
                                        <option value="{{{$domain->id}}}">{{{$domain->name}}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular108" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="url" type="text" class="form-control" id="regular108" value="{{{$abSplit->url}}}" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="checkbox" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Введите путь">Открытый URL <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9 opacity-75">
                                    <div class="checkbox checkbox-inline checkbox-styled">
                                        <label>
                                            <input name="open_url" type="checkbox" id="checkbox"><span> Открытый URL показывает URL конечного элемента.<br/>Закрытый - URL гео-сплита</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="" data-original-title="Введите путь">Ссылка <span class="hidden-md hidden-sm hidden-xs"><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <div class="form-control-alt opacity-75"><a target="_blank" href="http://supertest.landmin.ru/ab-test2?nostat">http://supertest.landmin.ru/ab-test2</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-sm-5 hidden-sm hidden-xs">
                <div class="note-wrap">
                    <div class="tip">
                        <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-head">
                        <header class="text-primary">Распределить трафик по городам <span class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover" data-placement="auto" data-content='Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos quis beatae minus fuga cupiditate magni voluptatem in fugiat ut doloribus veniam eius blanditiis dolorum hic consequuntur non placeat, ratione cumque. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique hic, nam culpa ad labore, iste sed deleniti molestias deserunt facilis exercitationem ea aliquam esse necessitatibus corporis, sapiente quia veritatis odit.'><i class="md md-help"></i></span></header>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>URL</th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip" data-placement="auto" title="Просмотры"><i class="md md-visibility"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip" data-placement="auto" title="Уникальные посетители"><i class="md md-person"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip" data-placement="auto" title="Отклики/заявки"><i class="md md-assignment-turned-in"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip" data-placement="auto" title="Продажи"><i class="md md-attach-money"></i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip" data-placement="auto" title="Конверсия в отклики"><i class="md">CV1</i></span></th>
                                    <th><span class="hidden-md hidden-sm hidden-xs" data-toggle="tooltip" data-placement="auto" title="Конверсия в продажи"><i class="md">CV2</i></span></th>
                                    <th style="min-width:100px;">Управление</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Санкт-Петербург</td>
                                    <td><a href="#">http://localhostjshdjhjs.ru</a></td>
                                    <td>78</td>
                                    <td>3</td>
                                    <td>76</td>
                                    <td>78</td>
                                    <td>76</td>
                                    <td>78</td>
                                    <td>
                                        <a href="#" class="btn btn-icon-toggle"><i class="md md-mode-edit"></i></a>
                                        <a href="#" class="btn btn-icon-toggle"><i class="md md-delete"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Другие города</td>
                                    <td><a href="#">http://localhostjshdjhjs.ru</a></td>
                                    <td>78</td>
                                    <td>3</td>
                                    <td>76</td>
                                    <td>78</td>
                                    <td>76</td>
                                    <td>78</td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                            <a href="#" class="text-primary"><i class="md md-add"></i> Добавить направление</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript">

        $('select[name="domain_id"]').val({{{$abSplit->domain_id}}});

        $('input[name="open_url"]').prop('checked', @if($abSplit->open_url) true @else false @endif);

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/ab-splits";
        }

        $('#ab-split-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "domain_id": {
                            required: true
                        }
                    },
                    messages: {
                        "domain_id": {
                            required: "Выберите домен"
                        },
                        "name": {
                            required: "Введите название сплита",
                            minlength: "Минимальная длинна названия сплита - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#ab-split-form').valid()) {
              var name = $('#ab-split-form input[name="name"]').val();
              var description = $('#ab-split-form textarea[name="description"]').val();
              var domain_id = $('#ab-split-form select[name="domain_id"]').val();
              var url =  $('#ab-split-form input[name="url"]').val();
              var open_url = $('#ab-split-form input[name="open_url"]').prop("checked");

              var data = {
                  name: name,
                  description: description,
                  domain_id: domain_id,
                  url: url,
                  open_url: open_url,
                  id: {{{$abSplit->id}}}
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/abSplits/update",
                  method: "PUT",
                  data: data,
                  success: function (result) {
                      toastr.success("AB-split успешно обновлен");
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка сохранения");
                  }
              });
          }
        }

        function deleteEntity() {
            var data = {
                id: {{{$abSplit->id}}}
            };

            $.ajax({
                url: "/{{{$company->name}}}/landingControl/ajax/abSplits/delete",
                method: "POST",
                data: data,
                success: function (result) {
                    window.location = '/{{{$company->name}}}/landingControl/ab-splits';
                },
                error: function (result) {
                    toastr.error("Произошла ошибка удаления");
                }
            });
        }


        $(document).ready(function () {

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-delete-btn').click(function() {
                deleteEntity();
            });


            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop