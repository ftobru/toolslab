@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <div class="section-body">
            <ol class="breadcrumb">
                <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
                <li><a href="/{{{$company->name}}}/landingControl/links">Ссылки</a></li>
                <li class="active">перемножатор</li>
            </ol>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary">Перемножатор</h2>
                </div>
            </div>
            <form class="form-horizontal" role="form">
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Основа</h3>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="tabs-1" class="tabs button-tabs">
                                            <input id="tab-1-1" name="tabs-group-1" type="radio" checked />
                                            <input id="tab-1-2" name="tabs-group-1" type="radio" />
                                            <input id="tab-1-3" name="tabs-group-1" type="radio" />

                                            <label for="tab-1-1" class="first-label label-btn">Шаблон</label>
                                            <label for="tab-1-2" class=" label-btn">АБ-тест</label>
                                            <label for="tab-1-3" class="last-label label-btn">Гео-сплит</label>

                                            <div class="radio-content">
                                                <p class="opacity-75 text-sm choice">Выберите страницу шаблона:</p>
                                                <select id="optgroup" multiple="multiple">
                                                    <optgroup label="Товар-замена">
                                                        <option value="AK">пункт1</option>
                                                        <option value="HI">пункт1</option>
                                                    </optgroup>
                                                    <optgroup label="Линк-вставка">
                                                        <option value="CT">пункт1</option>
                                                        <option value="DE">пункт1</option>
                                                        <option value="WV">пункт1</option>
                                                    </optgroup>
                                                    <optgroup label="Линк-замена">
                                                        <option value="ол">пункт1</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                            <div class="radio-content">
                                                <p class="opacity-75 text-sm choice">Выберите страницу с АБ-тестом:</p>
                                                <select id="optgroup1" multiple="multiple">
                                                    <optgroup label="Товар-замена">
                                                        <option value="AK">пункт1</option>
                                                        <option value="HI">пункт1</option>
                                                    </optgroup>
                                                    <optgroup label="Линк-вставка">
                                                        <option value="CT">пункт1</option>
                                                        <option value="DE">пункт1</option>
                                                        <option value="WV">пункт1</option>
                                                    </optgroup>
                                                    <optgroup label="Линк-замена">
                                                        <option value="ол">пункт1</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                            <div class="radio-content">
                                                <p class="opacity-75 text-sm choice">Выберите страницу с Гео-сплитом:</p>
                                                <select id="optgroup2" multiple="multiple">
                                                    <optgroup label="Товар-замена">
                                                        <option value="AK">пункт1</option>
                                                        <option value="HI">пункт1</option>
                                                    </optgroup>
                                                    <optgroup label="Линк-вставка">
                                                        <option value="CT">пункт1</option>
                                                        <option value="DE">пункт1</option>
                                                        <option value="WV">пункт1</option>
                                                    </optgroup>
                                                    <optgroup label="Линк-замена">
                                                        <option value="ол">пункт1</option>
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Мультидендинг</h3>
                                <select id="optgroup3-1" multiple="multiple">
                                    <optgroup label="Товар-замена">
                                        <option value="AK">пункт1</option>
                                        <option value="HI">пункт1</option>
                                    </optgroup>
                                    <optgroup label="Линк-вставка">
                                        <option value="CT">пункт1</option>
                                        <option value="DE">пункт1</option>
                                        <option value="WV">пункт1</option>
                                    </optgroup>
                                    <optgroup label="Линк-замена">
                                        <option value="ол">пункт1</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">UTM-метки</h3>
                                <div class="form-group">
                                    <label for="regular119" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего товара">Шаблон<span ><i class="md md-help"></i></span></label>
                                    <div class="col-sm-9">
                                        <select id="optgroup3" multiple="multiple">
                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Eastern Time Zone">
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="IN">Indiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="VT">Vermont</option><option value="VA">Virginia</option>
                                                <option value="WV">West Virginia</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Описание</h3>
                                <div class="margin-bottom-xxl">
                                    <p class="opacity-75 text-sm">Важно! Чтобы легче найти сгенерированные ссылки, впишите уникальный тег в поле снизу.<br/> Подробнее в сноске.</p>
                                </div>
                                <div class="form-group">
                                    <label for="regular119" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего товара">Тег <span ><i class="md md-help"></i></span></label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" value="IT-отдел" data-role="tagsinput" style="display: none;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="regular119" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашего товара">Комментарий<span ><i class="md md-help"></i></span></label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="regular119" placeholder=""></textarea>
                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary no-margin">Генерация ссылок</h3>
                                <p class="opacity-75 text-sm no-side-padding">Внимние! Будет сгенерировано ссылок: <span>275 </span></p>
                                <span class="opacity-75 text-light">Большое количество ссылок может снизить удобство работы с таблицей ссылок. Генерируйте большие объемы ссылок, только когда есть четкое понимание для чего это нужно.</span>
                                <div>
                                    <div class="text-center margin-bottom-xxl margin-top-lg">
                                        <span class="btn ink-reaction btn-warning btn-lg hidden-xs" data-toggle="collapse" data-target="#accordion1-1">Сгенерировать и сохранить</span>
                                    </div>
                                    <div id="accordion1-1" class="collapse">
                                        <span class="opacity-75 text-lg text-primary">Ссылки успешно сгенерированы!</span>
                                        <!-- <p class="text-light text-danger no-side-padding">Такая ссылка уже была создана ранее. Ее URL: http://loclhost/jhj.ru</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row margin-bottom-xxl">
            <div class="col-sm-12">
                <ul class="list-unstyled list-inline">
                    <li><button type="submit" class="btn btn-default-light ink-reaction btn-sm">Удалить</button></li>
                    <li><button type="submit" class="btn btn-default-light ink-reaction btn-sm">Отменить</button></li>
                </ul>
            </div>
        </div>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript">

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/ab-splits";
        }

        $('#geo-split-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "domain_id": {
                            required: true
                        }
                    },
                    messages: {
                        "domain_id": {
                            required: "Выберите домен"
                        },
                        "name": {
                            required: "Введите название сплита",
                            minlength: "Минимальная длинна названия сплита - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#geo-split-form').valid()) {
              var name = $('#geo-split-form input[name="name"]').val();
              var description = $('#geo-split-form textarea[name="description"]').val();
              var domain_id = $('#geo-split-form select[name="domain_id"]').val();
              var url =  $('#geo-split-form input[name="url"]').val();
              var open_url = $('#geo-split-form input[name="open_url"]').prop("checked");

              var data = {
                  name: name,
                  description: description,
                  domain_id: domain_id,
                  url: url,
                  open_url: open_url
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/abSplits/create",
                  method: "POST",
                  data: data,
                  success: function (result) {
                      toastr.success("AB-split успешно создан");
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка сохранения");
                  }
              });
          }
        }

        $(document).ready(function () {

            $('#optgroup, #optgroup1, #optgroup2,#optgroup3, #optgroup3-1').multiSelect({});

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop