@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/geo-splits">Geo-сплиты</a></li>
            <li class="active">добавление направления</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Создание направления geo-сплита</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <form id="geo-split-form" class="form-horizontal" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular111" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Название <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" type="text" class="form-control" id="regular111" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular112" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Описание <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="description"  class="form-control" id="regular112" placeholder=""></textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular113" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей формы">Города<span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <select id="regular113" name="cities" class="form-control select2-list input-sm"  multiple>
                                            <option value="0">Все города</option>
                                            @foreach($cities['groups'] as $group)
                                                <optgroup label="{{{$group['value']}}}">
                                                    @foreach($group['items']->toArray() as $city)
                                                        <option value="{{{$city['idx']}}}">{{{$city['city']}}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-head">
                            <header class="text-primary">Трафик отправлять на:</header>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="tabs-1" class="tabs button-tabs">
                                        <input id="tab-1-1" name="tabs-group-1" type="radio" checked />
                                        <input id="tab-1-2" name="tabs-group-1" type="radio" />
                                        <input id="tab-1-3" name="tabs-group-1" type="radio" />

                                        <label for="tab-1-1" class="first-label active" data-en-type="0">Шаблон</label>
                                        <label for="tab-1-2" class="last-label" data-en-type="1">АБ-тест</label>
                                        <label for="tab-1-3" style="display:none;">Гео-сплит</label>

                                        <div class="radio-content">
                                            <div class="form-group">
                                                <label for="template_selector" class="col-sm-3 col-xs-12 control-label" data-toggle="tooltip" data-placement="auto" title="Выберите шаблон">Страница: <span ><i class="md md-help"></i></span></label>
                                                <div class="col-sm-5 col-xs-7">
                                                    <select id="template_selector" name="template_id" class="form-control select2-list" data-placeholder="">
                                                        <option>&nbsp;</option>
                                                        @foreach($templates['data'] as $template)
                                                            <option value="{{{$template['id']}}}">{{{$template['name']}}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{--<div class="col-sm-3 col-xs-4">
                                                    <div class="form-group">
                                                        <span class="btn ink-reaction btn-default-light" data-toggle="modal" data-target="#modal1"><i class="md md-search"></i></span>
                                                    </div>
                                                </div>--}}
                                            </div>
                                        </div>
                                        <div class="radio-content">
                                            <div class="form-group">
                                                <label for="split_selector" class="col-sm-3 col-xs-12 control-label" data-toggle="tooltip" data-placement="auto" title="Выберите АБ-сплит">АБ-тест: <span ><i class="md md-help"></i></span></label>
                                                <div class="col-sm-5 col-xs-7">
                                                    {{--<input type="text" class="form-control" id="regular110" placeholder="">--}}
                                                    {{--<div class="form-control-line"></div>--}}
                                                    <select id="split_selector" name="ab_split_id" class="form-control select2-list" data-placeholder="">
                                                        <option>&nbsp;</option>
                                                        @foreach($ab_splits['data'] as $ab_split)
                                                            <option value="{{{$ab_split['id']}}}">{{{$ab_split['name']}}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                               {{-- <div class="col-sm-3 col-xs-4">
                                                    <div class="form-group">
                                                        <button class="btn ink-reaction btn-default-light" data-toggle="modal" data-target="#modal1"><i class="md md-search"></i></button>
                                                    </div>
                                                </div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="row margin-bottom-xxl">
            <div class="col-sm-12">
                <ul class="list-unstyled list-inline">
                    <li><button onclick="goBack()" type="button" class="btn btn-default-light ink-reaction btn-sm">Отменить</button></li>
                    <li><button id="submit-btn" type="button" class="btn ink-reaction btn-primary btn-sm">Сохранить</button></li>
                </ul>
            </div>
        </div>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <script type="text/javascript">

        function goBack() {
            window.history.back();
        }

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/geo-splits";
        }

        $('#geo-split-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "domain_id": {
                            required: true
                        }
                    },
                    messages: {
                        "domain_id": {
                            required: "Выберите домен"
                        },
                        "name": {
                            required: "Введите название сплита",
                            minlength: "Минимальная длинна названия сплита - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#geo-split-form').valid()) {
              var name = $('#geo-split-form input[name="name"]').val();
              var description = $('#geo-split-form textarea[name="description"]').val();
              var cities = $('#geo-split-form select[name="cities"]').val();
              var type = parseInt($('#tabs-1 label.active').data().enType);
              var link_base_id;

              switch (type) {
                  case 0:
                      link_base_id = parseInt($('#geo-split-form select[name="template_id"]').val());
                      break;
                  case 1:
                      link_base_id = parseInt($('#geo-split-form select[name="ab_split_id"]').val());
                      break;
              }

              var data = {
                  name: name,
                  description: description,
                  cities: cities,
                  type: type,
                  callback_en_id: {{{$temp->id}}},
                  link_base_id: link_base_id
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/links/attacheGeoDirection",
                  method: "POST",
                  data: data,
                  success: function (result) {
                      toastr.success("Направление добавлено.");
                      if(result.redirect) {
                          window.location.href = result.redirect;  
                      }
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка сохранения");
                  }
              });
          }
        }

        $(document).ready(function () {

            $('#tabs-1 label').click(function (e) {
                var a = e;
                if (!$(e.currentTarget).hasClass("active")) {
                    $('#tabs-1 label').removeClass("active");
                    $(e.currentTarget).addClass("active");
                    var id = $('#tabs-1 label.active').prop('for');
                    $('#' + id).prop('checked', true);
                }
            });


            $('#regular113, #split_selector, #template_selector').select2();

            $('#reset-btn1, #header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-save-btn, #submit-btn').click(function() {
                submitForm();
            });

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop