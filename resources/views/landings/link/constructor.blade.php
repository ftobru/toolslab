@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
@stop

@section('content')
    <section>
        <div class="section-body">
            <ol class="breadcrumb">
                <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
                <li><a href="/{{{$company->name}}}/landingControl/links">Ссылки</a></li>
                <li class="active">конструктор</li>
            </ol>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary">Конструктор ссылок</h2>
                </div>
            </div>
            <form id="constructor-form" class="form-horizontal" role="form">
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Основа</h3>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="tabs-1" class="tabs button-tabs">
                                            <input id="tab-1-1" name="tabs-group-1" type="radio" checked/>
                                            <input id="tab-1-2" name="tabs-group-1" type="radio"/>
                                            <input id="tab-1-3" name="tabs-group-1" type="radio"/>

                                            <label for="tab-1-1" data-en-type="0"
                                                   class="first-label active">Шаблон</label>
                                            <label for="tab-1-2" data-en-type="1">АБ-тест</label>
                                            <label for="tab-1-3" data-en-type="2" class="last-label">Гео-сплит</label>

                                            <div class="radio-content">
                                                <p class="opacity-75 text-sm choice">Выберите страницу шаблона:</p>

                                                <div class="col-sm-5">
                                                    <select name="template_id" id="template_select"
                                                            class="form-control select2-list input-sm"
                                                            data-placeholder="">
                                                        <option disabled selected>Не выбрано</option>
                                                        @foreach($templates as $template)
                                                            <option value="{{{$template['id']}}}">{{{$template['name']}}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="submit" class="btn ink-reaction btn-default btn-sm"><i
                                                                class="md md-search"></i></button>
                                                </div>
                                                <div class="col-sm-5 opacity-75" id="template_label">
                                                </div>
                                            </div>
                                            <div class="radio-content">
                                                <p class="opacity-75 text-sm choice">Выберите страницу с АБ-тестом:</p>

                                                <div class="col-sm-5">
                                                    <select name="ab_test_id" id="ab_test_select"
                                                            class="form-control select2-list input-sm"
                                                            data-placeholder="">
                                                        <option disabled selected>Не выбрано</option>
                                                        @foreach($ab_tests as $ab_test)
                                                            <option value="{{{$ab_test['id']}}}">{{{$ab_test['name']}}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="submit" class="btn ink-reaction btn-default btn-sm"><i
                                                                class="md md-search"></i></button>
                                                </div>
                                                <div class="col-sm-5 opacity-75" id="ab_label">
                                                </div>
                                            </div>
                                            <div class="radio-content">
                                                <p class="opacity-75 text-sm choice">Выберите страницу с
                                                    Гео-сплитом:</p>

                                                <div class="col-sm-5">
                                                    <select name="geo_split_id" id="geo_split_select"
                                                            class="form-control select2-list input-sm"
                                                            data-placeholder="">
                                                        <option disabled selected>Не выбрано</option>
                                                        @foreach($geo_splits as $geo_split)
                                                            <option value="{{{$geo_split['id']}}}">{{{$geo_split['name']}}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button type="submit" class="btn ink-reaction btn-default btn-sm"><i
                                                                class="md md-search"></i></button>
                                                </div>
                                                <div class="col-sm-5 opacity-75" id="geo_label">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Мультидендинг</h3>

                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label for="replacement_product_select" class="col-sm-4 control-label"
                                                   data-toggle="tooltip" data-placement="auto"
                                                   title="Выберите товар-замену">Товар-замена<span><i
                                                            class="md md-help"></i></span></label>

                                            <div class="col-sm-8">
                                                <select name="replacement_product_id" id="replacement_product_select"
                                                        class="form-control select2-list input-sm" data-placeholder="">
                                                    <option selected>Не выбрано</option>
                                                    @foreach($replacement_product as $product)
                                                        <option value="{{{$product['id']}}}">{{{$product['name']}}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="prod_param" class="col-sm-5 opacity-75"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label for="replacement_link_select" class="col-sm-4 control-label"
                                                   data-toggle="tooltip" data-placement="auto"
                                                   title="Выберите линк-замену">Линк-замена<span><i
                                                            class="md md-help"></i></span></label>

                                            <div class="col-sm-8">
                                                <select name="replacement_link_id" id="replacement_link_select"
                                                        class="form-control select2-list input-sm" data-placeholder="">
                                                    <option selected>Не выбрано</option>
                                                    @foreach($replacement_link as $link)
                                                        <option value="{{{$link['id']}}}">{{{$link['name']}}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="replacement_link_param" class="col-sm-5 opacity-75"></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <div class="form-group">
                                            <label for="insert_link_select" class="col-sm-4 control-label"
                                                   data-toggle="tooltip" data-placement="auto"
                                                   title="Выберите линк-вставку">Линк-вставка<span><i
                                                            class="md md-help"></i></span></label>

                                            <div class="col-sm-8">
                                                <select name="insert_link_id" id="insert_link_select"
                                                        class="form-control select2-list input-sm" data-placeholder="">
                                                    <option selected>Не выбрано</option>
                                                    @foreach($insert_link as $link)
                                                        <option value="{{{$link['id']}}}">{{{$link['name']}}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="insert_link_param" class="col-sm-5 opacity-75"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{--<div class="row" id="template_row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">HTML-шаблон</h3>

                                <div class="form-group">
                                    <label for="html_select" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Выберите шаблон HTML верстки">Шаблон<span><i
                                                    class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <select name="html_id" id="html_select" class="form-control select2-list"
                                                data-placeholder="">
                                            <option disabled selected>Не выбрано</option>
                                            @foreach($HTMLs as $html)
                                                <option value="{{{$html['id']}}}">{{{$html['name']}}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}

               {{-- <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Форма захвата</h3>

                                <div class="form-group">
                                    <label for="html_select" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Выберите форму захвата">Форма<span><i
                                                    class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <select name="form_id" id="form_select" class="form-control select2-list"
                                                data-placeholder="">
                                            <option disabled selected>Не выбрано</option>
                                            @foreach($forms as $form)
                                                <option value="{{{$form['id']}}}">{{{$form['name']}}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--}}

                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">UTM-метки</h3>

                                <div class="form-group">
                                    <label for="utm_select" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Выберите шаблон UTM метки">UTM<span><i
                                                    class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <select name="utm_id" id="utm_select" class="form-control select2-list"
                                                data-placeholder="">
                                            <option disabled selected>Не выбрано</option>
                                            @foreach($utm as $u)
                                                <option value="{{{$u['id']}}}">{{{$u['name']}}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Описание</h3>

                                <div class="form-group">
                                    <label for="regular119" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Введите название вашего товара">Тег <span><i
                                                    class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <input type="text" name="tag"
                                               class="form-control" {{--data-role="tagsinput" style="display: none;"--}}>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="link_description" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto"
                                           title="Введите название вашего товара">Комментарий<span><i
                                                    class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <textarea class="form-control" name="description" id="link_description"
                                                  placeholder=""></textarea>

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-primary">Генерация ссылок</h3>

                                <div class="margin-bottom-xxl">
                                    <div class="text-center">
                                        <span id="generate_link" class="btn ink-reaction btn-warning btn-lg hidden-xs"
                                              {{--data-toggle="collapse" data-target="#accordion1-1"--}}>Сгенерировать и сохранить</span>
                                    </div>
                                    <div id="accordion1-1" class="collapse">
                                        <p class="opacity-75 text-sm no-side-padding">Сгенерируется: <span>1 </span>ссылка
                                        </p>

                                        <div class="margin-bottom-xxl">
                                            <div class="form-group">
                                                <label for="regular119" class="col-sm-3 control-label"
                                                       data-toggle="tooltip" data-placement="auto"
                                                       title="Введите название вашего товара">Link<span><i
                                                                class="md md-help"></i></span></label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="regular119"
                                                           placeholder="">

                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="regular119" class="col-sm-3 control-label"
                                                       data-toggle="tooltip" data-placement="auto"
                                                       title="Введите название вашего товара">Short<span><i
                                                                class="md md-help"></i></span></label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="regular119"
                                                           placeholder="">

                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="opacity-75 text-sm text-primary">Ссылка успешно сгенерирована!</p>
                                        <!-- <p class="text-light text-danger no-side-padding">Такая ссылка уже была создана ранее. Ее URL: http://loclhost/jhj.ru</p> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="row margin-bottom-xxl">
            <div class="col-sm-12">
                <ul class="list-unstyled list-inline">
                    <li>
                        <button type="submit" class="btn btn-default-light ink-reaction btn-sm">Удалить</button>
                    </li>
                    <li>
                        <button type="submit" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                    </li>
                </ul>
            </div>
        </div>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript">


        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/ab-splits";
        }

        $('#constructor-form').validate({
                    rules: {
                        "template_id": {
                            required: function (element) {
                                return $('#tab-1-1').prop('checked');
                            }
                        },
                        "ab_test_select": {
                            required: function (element) {
                                return $('#tab-1-2').prop('checked');
                            }
                        },
                        "geo_split_id": {
                            required: function (element) {
                                return $('#tab-1-3').prop('checked');
                            }
                        },
                        "replacement_product_select": {
                            required: true
                        },
                        "description": {
                            maxlength: 255
                        },
                        "tag": {
                            maxlength: 255
                        }
                    },
                    messages: {
                        "template_id": {
                            required: "Выберите основу из списка шаблонов"
                        },
                        "ab_test_select": {
                            required: "Выберите основу из списка АБ-тестов"
                        },
                        "geo_split_id": {
                            required: "Выберите основу из списка ГЕО-Сплитов"
                        },
                        "replacement_product_select": {
                            required: "Выберите товар-замену из списка"
                        },
                        "description": {
                            maxlength: "Слишком длинное описание"
                        },
                        "tag": {
                            maxlength: "Слишком длинный тег"
                        }
                    }
                }
        );

        function submitForm() {
            if ($('#constructor-form').valid()) {
                var description = $('#constructor-form textarea[name="description"]').val();
                var tag = $('#constructor-form input[name="tag"]').val();
                var utm_id = parseInt($('#constructor-form select[name="utm_id"]').val());
                var insert_link_id = parseInt($('#constructor-form select[name="insert_link_id"]').val());
                var replacement_link_id = parseInt($('#constructor-form select[name="replacement_link_id"]').val());
                var replacement_product_id = parseInt($('#constructor-form select[name="replacement_product_id"]').val());
                var type = parseInt($('#tabs-1 label.active').data().enType);

                var link_base_id;
                switch (type) {
                    case 0:
                        link_base_id = parseInt($('#constructor-form select[name="template_id"]').val());
                        break;
                    case 1:
                        link_base_id = parseInt($('#constructor-form select[name="ab_test_select"]').val());
                        break;
                    case 2:
                        link_base_id = parseInt($('#constructor-form select[name="geo_split_select"]').val());
                        break;
                }

                var data = {
                    type: type,
                    link_base_id: link_base_id,
                    description: description,
                    @if($callback_en_id)
                    callback_en_id: {{{$callback_en_id}}},
                    @endif
                    tag: tag
                };



                if(!isNaN(replacement_product_id)) {
                    data['replacement_product_id'] = replacement_product_id;
                }
                if(!isNaN(replacement_link_id)) {
                    data['replacement_link_id'] = replacement_link_id;
                }
                if(!isNaN(insert_link_id)) {
                    data['insert_link_id'] = insert_link_id;
                }
                if(!isNaN(utm_id)) {
                    data['utm_id'] = utm_id;
                }


                $.ajax({
                    url: "/{{{$company->name}}}/landingControl/ajax/links/constructLink",
                    method: "POST",
                    data: data,
                    success: function (result) {
                        if(result[0].hasOwnProperty('redirect_link')) {
                            window.location.href = result[0].redirect_link;
                        }
                        toastr.success("Ссылки успешно созданы");
                    },
                    error: function (result) {
                        toastr.error("Произошла ошибка");
                    }
                });
            }
        }

        $(document).ready(function () {

            $('#template_select, #ab_test_select, #geo_split_select, #utm_select, #html_select').select2();
            $('#replacement_product_select, #replacement_link_select, #insert_link_select, #form_select').select2();


            $(document.body).on("change","#replacement_link_select",function(){
                var id = this.value;
                if (parseInt(id)) {
                    var data = {
                        entity_id: id
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/landingControl/ajax/replacementLinks/getById",
                        method: "POST",
                        data: data,
                        success: function (result) {
                            var param = result.param;
                            var key_value = JSON.parse(result.key_value);
                            var val = Object.keys(key_value)[0];
                            $('#replacement_link_param').html('replacement_links_' + param + '=' + val);
                        },
                        error: function (result) {
                        }
                    });
                } else {
                    $('#replacement_link_param').html('');
                }
            });

            $(document.body).on("change","#replacement_product_select",function(){
                var id = this.value;
                if(parseInt(id)) {
                    var data = {
                        entity_id: id
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/landingControl/ajax/replacementProducts/getById",
                        method: "POST",
                        data: data,
                        success: function (result) {
                            var value = result.key;
                            $('#prod_param').html('prod='+value);
                        },
                        error: function (result) {
                        }
                    });
                } else {
                    $('#prod_param').html('');
                }

            });

            $(document.body).on("change","#insert_link_select",function(){
                var id = this.value;
                if (parseInt(id)) {
                    var data = {
                        entity_id: id
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/landingControl/ajax/insertLinks/getById",
                        method: "POST",
                        data: data,
                        success: function (result) {
                            var param = result.param;
                            var key_value = JSON.parse(result.key_value);
                            var val = Object.keys(key_value)[0];
                            $('#insert_link_param').html('insert_links_' + param + '=' + val);
                        },
                        error: function (result) {
                        }
                    });
                } else {
                    $('#insert_link_param').html('');
                }
            });

            $(document.body).on("change","#template_select",function(){
                var id = this.value;
                if (parseInt(id)) {
                    var data = {
                        entity_id: id
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/landingControl/ajax/templates/getById",
                        method: "POST",
                        data: data,
                        success: function (result) {
                            var link = result.domain.name + '/' + result.url;
                            $('#template_label').html(link);
                        },
                        error: function (result) {
                        }
                    });
                } else {
                    $('#template_label').html('');
                }
            });

            $(document.body).on("change","#ab_test_select",function(){
                var id = this.value;
                if (parseInt(id)) {
                    var data = {
                        entity_id: id
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/landingControl/ajax/abSplits/getById",
                        method: "POST",
                        data: data,
                        success: function (result) {
                            var link = result.domain.name + '/' + result.url;
                            $('#ab_label').html(link);
                        },
                        error: function (result) {
                        }
                    });
                } else {
                    $('#ab_label').html('');
                }
            });

            $(document.body).on("change","#geo_split_select",function(){
                var id = this.value;
                if (parseInt(id)) {
                    var data = {
                        entity_id: id
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/landingControl/ajax/geoSplits/getById",
                        method: "POST",
                        data: data,
                        success: function (result) {
                            var link = result.domain.name + '/' + result.url;
                            $('#geo_label').html(link);
                        },
                        error: function (result) {
                        }
                    });
                } else {
                    $('#geo_label').html('');
                }
            });


            //domain_label
            //template_select

            $('#tabs-1 label').click(function (e) {
                var a = e;
                if (!$(e.currentTarget).hasClass("active")) {
                    $('#tabs-1 label').removeClass("active");
                    $(e.currentTarget).addClass("active");
                    var id = $('#tabs-1 label.active').prop('for');
                    $('#' + id).prop('checked', true);
                }
            });

            $('#reset-btn1, #header-cancel-btn').click(function () {
                resetBtn();
            });
            $('#generate_link').click(function () {
                submitForm();
            });

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop