
@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @if($editPermission)
    <li class="dropdown margin-right tools-dropdown">
        <a href="javascript:void(0);" class="dropdown-toggle btn ink-reaction btn-raised btn-primary data text-default-bright tools" data-toggle="dropdown" aria-expanded="false"><span class="hidden-xs">Инструменты</span><span class="hidden-lg hidden-md hidden-sm"><i class="md md-settings"></i></span></a>
        <ul class="dropdown-menu animation-dock">
            <li><a href="/{{{$company->name}}}/landingControl/links/constructor" class="text-primary">Конструктор ссылок</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/links/multiplier" class="text-primary">Перемножатор</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/utms" class="text-primary">UTM-шаблоны</a></li>
        </ul>
    </li>
    @endif
@stop

@section('switcher')
@stop

@section('content')
    <div class="offcanvas">
    </div>

    <div class="card tab-content filter-page">
        <div class="filter-menu" id="off-canvas1" style="left: 60px;">
            <nav>
                <div class="expanded">
                    <a href="../../html/dashboards/dashboard.html">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label">
                                <input type="text" class="form-control input-sm" style="margin-top: 8px;" id="filter-link-name">
                                <label for="filter-link-name">Название:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-end" name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
            <div class="section-body">
                <div class="table_wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card" id="droptarget-hidden-columns">
                                <div class="card-head">
                                    <header>Скрытые столбцы</header>
                                </div>
                                <div class="card-body">
                                    <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                        <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                    </div>
                                </div>
                                <!--end .card-body -->
                            </div>

                            {{--<div><span class="print-me">[Печать]</span></div>--}}
                            <div class="k-grid-container" id="grid"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!--end content-->
@stop

@section('modals')

@stop

@section('scripts')

    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>

    <script type="text/javascript">

        GridConfig = {};
        window.Filters = {};
        FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 1;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_links_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/landingControl/ajax/links/';
        // Путь до карты
        GridConfig.baseCardUrl = '/' + '{{{$company->name}}}' + '/landingControl/links/edit/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'links';
        // Можно ли редактировать
        GridConfig.editable = false;
        // Можно ли делать группировку
        GridConfig.groupable = false;
        // Шаблон копирования строк
        GridConfig.copyFields = ['name'];
        // Конфиг колонок грида
        GridConfig.columns = [
//            {
//                field: "id",
//                title: "ID",
//                width: "50px",
//                locked: true,
//                lockable: false
//            },
            {
                field: "type",
                title: 'Тип',
                template: "#= cellTemplate(type, 'type') #",
                width: "130px"
            },
            {
                field: "tag",
                title: 'Тег',
                width: "130px"
            },
//            {
//                field: "name",
//                title: "Название",
//                width: "220px",
//                locked: true,
//                template: "#= cellTemplate(name, 'name') #",
//                lockable: false
//            },
            {
                field: "link",
                title: "Ссылка",
                width: "220px"
            },
            {
                field: "short_link",
                title: "Короткая ссылка",
                width: "130px"
            },
            {
                field: "link_base",
                title: "Основа",
                template: "#= cellTemplate(link_base, 'link_base') #",
                width: "130px"
            },
            {
                field: "replacement_product",
                title: "Товар-замена",
                template: "#= cellTemplate(replacement_product, 'replacement_product') #",
                width: "130px"
            },
            {
                field: "replacement_link",
                title: "Link-замена",
                template: "#= cellTemplate(replacement_link, 'replacement_link') #",
                width: "130px"
            },
            {
                field: "insert_link",
                title: "Link-вставка",
                template: "#= cellTemplate(insert_link, 'insert_link') #",
                width: "130px"
            },
            {
                field: "utm",
                title: "UTM метка",
                template: "#= cellTemplate(utm, 'utm') #",
                width: "130px"
            },
            {
                field: "open_url",
                title: 'Открытая',
                template: "#= cellTemplate(open_url, 'open-url') #",
                width: "130px"
            },
            {
                field: "domain",
                title: "Домен",
                template: "#= cellTemplate(domain, 'domain-name') #",
                width: "130px",
                editor: "readonlyEditor"
            },
//            {
//                field: "created_at",
//                title: "Созданa",
//                format: "{0: dd-MM-yyyy HH:mm:ss}",
//                width: "130px"
//            },
//            {
//                field: "updated_at",
//                title: "Измененa",
//                format: "{0: dd-MM-yyyy HH:mm:ss}",
//                width: "130px"
//            },
//            {
//                command: [
//                    {name: "card", text: "", click: abSplitEdit , className: "md md-list", width: "32px"}
//                ],
//                title: "&nbsp;",
//                width: "170px",
//                attributes: {"class": "button-column"}
//            }
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string"},
                url: {type: "string"},
                open_url: {type: "boolean"},
                description: {type: "string"},
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'name',
            'domain_id',
            'url',
            'open_url',
            'description',
            'date_start',
            'date_end',
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'name',
            'domain_id',
            'url',
            'open_url',
            'description',
            'date_start',
            'date_end',
        ];

        // Карточка сущности.
        function abSplitEdit(e) {
            e.preventDefault();
            var item = this.dataItem($(e.currentTarget).closest("tr"));
            var idx = this.dataSource.indexOf(item);
            var entityID = item.id;
            window.location.href = window.location.protocol + '//' + window.location.host + GridConfig.baseCardUrl + entityID;
        }

        function cellTemplate(value, type) {
            switch (type) {
                case 'domain-name':
                    if (value) {
                        return value.name;
                    }
                    return;
                    break;
                case 'link_base':
                    if(value) {
                        return value.name;
                    }
                    break;
                case 'replacement_product':
                    if(value) {
                        return value[0].name;
                    }
                    break;
                case 'insert_link':
                    if(value) {
                        return value[0].name;
                    }
                    break;
                case 'replacement_link':
                    if(value) {
                        return value[0].name;
                    }
                    break;
                case 'utm':
                    if(value) {
                        return value[0].name;
                    }
                    break;

                case 'type':
                    switch (value) {
                        case 0:
                            return 'Шаблон';
                            break;
                        case 1:
                            return 'АБ-сплит';
                            break;
                        case 2:
                            return 'Гео-сплит';
                            break;
                        default:
                            return '-';
                            break;
                    }
                    return;
                    break;
                case 'open-url':
                    if (value) {
                        return 'Да';
                    }
                    return 'Нет';
                    break;
                case 'name':
                    return '<a href="' + GridConfig.baseCardUrl +'">' + value +'</a>';
                    break;
            }
            return '-';
        }

        $(document).ready(function () {

            $("#header_add_simple_btn").attr("href", "/{{{$company->name}}}/landingControl/links/create");

            Filters = {

                isInit: false,

                city_input: null,
                phone_input: null,
                address_input: null,

                // Селекторы дат
                date_start: null,
                date_end: null,

                initFilters: function () {

                    var self = this;

                    this.city_input = $('#filter-link-name');
                    this.phone_input = $('#filter-link-code');
                    this.address_input= $('#filter-link-param');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');

                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    this.city_input.keyup(function () {
                        FilterBuilder.addFilter('name', {
                            field: "name",
                            operator: "startWith",
                            value: self.city_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.phone_input.keyup(function () {
                        FilterBuilder.addFilter('code', {
                            field: "code",
                            operator: "startWith",
                            value: self.phone_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.address_input.keyup(function () {
                        FilterBuilder.addFilter('param', {
                            field: "param",
                            operator: "startWith",
                            value: self.address_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });


                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },


                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "name" && value.operator == 'startWith') {
                        $('#filter-link-name').val(value.value);
                        FilterBuilder.addFilter('name', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                }
            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            }

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });

    </script>
@stop