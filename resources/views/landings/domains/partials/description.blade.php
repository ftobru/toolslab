<h2 class="text-primary">Домены</h2>
<div class="preview-wrp">
    <p>По умолчанию у Вас есть возможность использовать домен user123.landmin.ru</p>
    <p> Вы можете подключитьсвои домены,дляэтого нужно:</p>
    <ol>
        <li>Нажать кнопку "добавить домен" и указать свой домен</li>
        <li>В настройках DNS Вашего домена указать следующие записи: A XXX XXXX XXXX</li>
        <li>Дождаться делегирования (активации) Вашего домена. (24 часа)</li>
    </ol>
    <ul class="list-inline">
        <li><a href="#" class="links text-primary">Подробная инструкция</a></li>
        <li><a href="#" class="links text-primary">Связаться с теххподдержкой</a></li>
    </ul>
</div>