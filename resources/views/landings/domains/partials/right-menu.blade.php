<div class="offcanvas create-company">
    <div id="offcanvas-search" class="offcanvas-pane width-8">
        <div class="offcanvas-body">
            <div class="card-head tables-tabs">
                <ul class="nav nav-tabs " data-toggle="tabs">
                    <li class="active"><a href="#first8">Наш домен</a></li>
                    <li><a href="#second8">Свой домен</a></li>
                </ul>
            </div><!--end .card-head -->
            <br/>
            <div class="tab-content">
                <div class="tab-pane active" id="first8">
                    <form class="margin-no form floating-label">
                        <p>Вы можете создать до 5 доменов формата name.modulcrm.ru</p>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group floating-label">
                                    <input type="text" class="form-control input-sm" id="sub-domain">
                                    <label for="sub-domain" class="control-label">name</label><div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="col-xs-6 margin-top-lg no-padding">.modulcrm.ru</div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default-light ink-reaction btn-sm">Отмена</button>
                            <button id="create-domain" type="button" class="btn ink-reaction btn-primary btn-sm">Создать</button>
                        </div>
                    </form>
                </div><!--end tab-pane-->
                <div class="tab-pane" id="second8">
                    <form class="margin-no form floating-label">
                        <p>Укажите свое доменное имя ниже</p>
                        <div class="form-group floating-label">
                            <input type="text" class="form-control input-sm" id="domain">
                            <label for="domain" class="control-label">example.ru</label><div class="form-control-line"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default-light ink-reaction btn-sm">Отмена</button>
                            <button id="add-domain" type="button" class="btn ink-reaction btn-primary btn-sm">Добавить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>