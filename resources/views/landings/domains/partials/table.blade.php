<table class="table no-padding table-striped no-margin table-bordered domens-table">
    <thead>
    <tr>
        <th>Привязка</th>
        <th>Домен</th>

        <th>Статус</th>
        <th>Управление</th>
    </tr>
    </thead>
    <tbody>
    @if(!empty($domains) && count($domains) > 0)
    @foreach($domains as $domain)
        <tr id="{!!$domain->id!!}">
        <td>
            <p><span>{{date('d.m.Y', strtotime($domain->updated_at))}}</span> в <span>{{date('H:i', strtotime($domain->updated_at))}}</span></p>
        </td>
        <td>{{$domain->name}}</td>
        <td>@if($domain->status === \LandingControl\Reference\DomainReference::STATUS_ACTIVE)
            <span class="style-success padding-xs">Активирован
            </span>
                @else
                <span class="style-error padding-xs">Не активен
                </span>
                @endif
        </td>
        <td><a class="btn btn-default" href="#">Редактировать</a></td>
    </tr>
        @endforeach
    @else
        <tr>
            <td colspan="5">Не одного домена не зарегистрировано</td>
        </tr>
    @endif
    </tbody>
</table>