@extends('layouts.menubar_general')
@section('buttons')
    @include('partials.cabinet.button_add')
@stop
@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="#">Мои компании</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">Домены</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
            @include('landings.domains.partials.description')
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                        @include('landings.domains.partials.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('landings.domains.partials.right-menu')
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            function addDomain(domain){
                window.location.href = '{{route('landingControl.domains', ['company_alias' => $currentCompany->name])}}';
            };
            $('#add-domain').click(function(){
                var domain = $('#domain').val();
                $.ajax({
                    url : '{{ route('landingControl.domains.ajax.postAjaxCreateDomain', ['company_alias' => $currentCompany->name])}}',
                    dataType: 'json',
                    data: {name: domain},
                    type: 'POST'
                }).success(function(response){
                    toastr.success('Домен успешно зарегистрирован. Успешного бизнеса!');

                    addDomain(response);
                }).error(function(e){
                    toastr.error('Произошла ошибка, обратитесь в тех. поддержку');
                });

            });

            $('#create-domain').click(function(){
                var domain = $('#sub-domain').val();
                $.ajax({
                    url : '{{ route('landingControl.domains.ajax.postAjaxCreateSubDomain', ['company_alias' => $currentCompany->name])}}',
                    dataType: 'json',
                    data: {name: domain + '.modulcrm.ru'},
                    type: 'POST'
                }).success(function(response){
                    toastr.success('Домен успешно зарегистрирован. Успешного бизнеса!');

                    addDomain(response);
                }).error(function(e){
                    toastr.error('Произошла ошибка, обратитесь в тех. поддержку');

                });

            });

            $('.tip').truncate( {
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });
        });
    </script>
    @if(isset($socketKey))
        <script type="text/javascript">
            $(document).ready(function(){
                console.log('start socket');
                var socket = io.connect('{!! $socketServer !!}');
                socket.on('domain{!! $socketKey !!}', function (data) {
                    console.log(data);
                    $('#' + data).find('.style-error').removeClass('style-error').attr('class', 'style-success padding-xs').text('Активирован');
                    toastr.success('Один из ваших доменов активирован');
                });
            });
        </script>
    @endif
@stop