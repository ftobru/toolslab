@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="#">parent1</a></li>
            <li><a href="#">parent2</a></li>
            <li class="active">page</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Шаблон страницы</h2>
                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                </div>
            </div>
        </div>
        <form id="create-form" class="form-horizontal" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="template-name" class="col-sm-4 control-label">Название</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="name" id="template-name" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Теги</label>
                                <div class="col-sm-8">
                                    <input type="text" name="tags" class="form-control" data-role="tagsinput" style="display: none;">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="template-description" class="col-sm-4 control-label">Описание</label>
                                <div class="col-sm-8">
                                    <textarea name="description" class="form-control" id="template-description" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">

                           {{-- <div class="form-group">
                                <div class="col-xs-3 opacity-50"><span data-toggle="tooltip" data-placement="auto"
                                                                       title="Загрузите фото">Выберите путь до index<i
                                                class="md md-help"></i></span></div>
                                <div class="col-xs-7">
                                    <input type="text" class="form-control" id="fileuploadurl0" readonly
                                           placeholder="">
                                </div>
                                <div class="col-xs-2 text-right">
                                    <ul class="list-inline">
                                        <li>
                                            <a class="btn btn-default fileupload" data-fieldId="0" data-toggle="tooltip" data-placement="auto"
                                               title="Загрузите с сервера">
                                                    <span>
                                                        Выбрать
                                                    </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>--}}

                            <div class="form-group">
                                <label for="regular85" class="col-sm-4 control-label" data-toggle="tooltip" data-placement="auto" title="Template-index">Путь к index <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="fileuploadurl0" readonly placeholder="">
                                </div>
                                <div class="col-sm-2"><a href="#" data-fieldId="0" class="btn btn-default-light btn-sm fileupload" data-toggle="tooltip" data-placement="auto" title="Выбрать на сервере">Выбрать</a></div>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label for="regular86" class="col-sm-4 control-label">Статус</label>--}}
                                {{--<div class="col-sm-8">--}}
                                    {{--<div class="no"><div class="alert alert-danger">Файл не найден! Проверьте правильность пути.</div></div>--}}
                                    {{--<div class="no"><div class="alert alert-success">Путь указан правильно! Index файл подключен.</div></div>--}}
                                    {{--<div class="no"><div class="alert alert-warning">Укажите путь к Index файлу вашего сайта.</div></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="template-ya-metrika" class="col-sm-3 control-label">Счетчик Я.Метрики</label>
                                <div class="col-sm-9">
                                    <textarea name="ya_metrika" class="form-control" id="template-ya-metrika" placeholder=""></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="template-ga" class="col-sm-3 control-label">Счетчик G.Analytics</label>
                                <div class="col-sm-9">
                                    <textarea name="google_analytics" class="form-control" id="template-ga" placeholder=""></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control" id="fileLinks" name="fileLinks" >
        </form>
       {{-- <div class="row margin-bottom-xxl">
            <div class="col-sm-12">
                <ul class="list-unstyled list-inline">
                    <li><button type="submit" class="btn btn-default-light ink-reaction btn-sm">Отменить</button></li>
                    <li><button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button></li>
                </ul>
            </div>
        </div>--}}
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript">

        var elf = null;
        var fileLinks = {};
        var currentFileSelectorId = '';

        function processSelectedFile(filePath, elemId) {
            fileLinks[elemId] = filePath;
            var filename = filePath.replace(/^.*[\\\/]/, '')
            $('#'+elemId).val(filename);

            var data = JSON.stringify(fileLinks);
            $('#fileLinks').val(data);
        }

        $.each($(".btn.fileupload"), function(key, value){
            $(value).click(function(e) {
                e.preventDefault();
                var btn = $(e.target).closest(".btn");
                currentFileSelectorId = 'fileuploadurl' + btn.data().fieldid.toString();
                if(!elf) {
                    elf = $('<div></div>').dialogelfinder({
                        lang: 'ru', // locale
                        customData: {
                            _token: '<?= csrf_token() ?>'
                        },
                        url: '/{{{$company->name}}}/fileManager/connector',  // connector URL
                        dialog: {
                            modal: true,
                            width: 700,
                            height: 450,
                            title: "Выберите файл шаблона",
                            zIndex: 99999,
                            resizable: true
                        },
                        resizable: false,
                        getFileCallback: function (file) {
                            processSelectedFile(file.path, currentFileSelectorId);
                            elf.hide();
                        }
                    }).elfinder('instance');
                } else {
                    elf.show();
                }
            });
        });

        function resetBtn() {
            window.location.href = "/{{{$company->name}}}/landingControl/html-templates";
        }

        $('#create-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "fileLinks": {
                            required: true
                        }
                    },
                    messages: {
                        "fileLinks": {
                            required: "Выберите html файл"
                        },
                        "name": {
                            required: "Введите название для шаблона",
                            minlength: "Минимальная длинна названия шаблона - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function submitForm() {
          if( $('#create-form').valid()) {
              var name = $('#create-form input[name="name"]').val();
              var description = $('#create-form textarea[name="description"]').val();

              var tags = $('#create-form input[name="tags"]').val();
              var ya = $('#create-form textarea[name="ya_metrika"]').val();
              var ga = $('#create-form textarea[name="google_analytics"]').val();
              var fileLink = $('#create-form input[name="fileLinks"]').val();

              var data = {
                  name: name,
                  description: description,
                  tags: tags,
                  ya_metrika: ya,
                  google_analytics: ga,
                  path: JSON.parse(fileLink)['fileuploadurl0']
              };

              $.ajax({
                  url: "/{{{$company->name}}}/landingControl/ajax/HTMLTemplates/create",
                  method: "POST",
                  data: data,
                  success: function (result) {
                      toastr.success("HTML шаблон успешно успешно создан");
                      window.location.href = "/{{{$company->name}}}/landingControl/html-templates";
                  },
                  error: function (result) {
                      toastr.error("Произошла ошибка в процессе создания");
                  }
              });
          }
        }


        $(document).ready(function () {

            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                submitForm();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop