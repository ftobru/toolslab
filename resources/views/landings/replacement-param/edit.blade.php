
@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_delete')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/landingControl/replacement-params">Переменные</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование переменных</h2>
            </div>
        </div>
        <form id="edit_form" method="POST" action="/{{{$company->name}}}/landingControl/replacement-param/edit/{{{$parameter->id}}}" class="form-horizontal edit_links" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="regular88" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название">Название <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" value="{{{$parameter->name}}}" type="text" class="form-control" id="regular88" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class=" tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите теги для товара">Тег вставки
                                    <span ><i class="md md-help"></i></span>
                                    <span class="popover-btn" data-container="body" data-toggle="popover" data-placement="auto" data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i class="md md-info"></i></span>
                                </label>
                                <div class="col-sm-9">
                                    <input name="tag" value="{{{$parameter->tag}}}" type="text" class="form-control" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="regular69" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите параметр">Контент<span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <textarea name="meaning" class="form-control" id="regular69">{{{$parameter->meaning}}}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-xxl">
                <div class="col-sm-12">
                    <ul class="list-unstyled list-inline">
                        <li>
                            <button type="button" id="delete-btn1" class="btn btn-default-light ink-reaction btn-sm">Удалить</button>
                        </li>
                        <li>
                            <button type="button" id="reset-btn1" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                        </li>
                        <li>
                            <button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 opacity-75">
                    <p class="no-margin">Обновлено <span>{{ date("d.m.Y",strtotime($parameter->updated_at)) }}</span> в <span>{{ date("G:i",strtotime($parameter->updated_at)) }}</span></p>
                    <p class="no-margin">Создано <span>{{ date("d.m.Y",strtotime($parameter->created_at)) }}</span> в <span>{{ date("G:i",strtotime($parameter->created_at)) }}</span></p>
                </div>
            </div>
        </form>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <script type="text/javascript">

        function resetBtn() {
            location.reload();
        }

        function deleteBtn() {
            var data = {
                entity_id:{{{$parameter->id}}}
            };

            $.ajax({
                url: '/{{{$company->name}}}/landingControl/ajax/replacementParams/delete',
                method: "POST",
                data: data,
                context: this,
                success: function (result, status, xhr) {
                    $(location).attr('href','/{{{$company->name}}}/landingControl/replacement-params');
                },
                error: function (xhr, status, error) {
                    toastr.error('Произошла ошибка при удалении.');
                }
            });
        }

        $(document).ready(function () {

            $('#delete-btn1').click(function() {
                deleteBtn();
            });
            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                $('#edit_form').submit();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-delete-btn').click(function() {
                deleteBtn();
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

        });
    </script>
@stop