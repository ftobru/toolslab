<!-- Пользователь существует-->
<script id="template-user-not-exist" type="text/x-handlers-template">
    <span>@{{value.email}}</span>? Такой пользователь не зарегистрирован в нашей системе. Нажмите кнопку "отправить приглашение", мы прикрепим этого пользователя к Вашей компании и отправим ему e-mail приглашение. Как только пользователь подтвердит свой e-mail, он сразу же сможет работать в Вашей компании...
</script>
<!-- Пользователь не существует-->
<script id="template-user-exist" type="text/x-handlers-template">
    <span>@{{value.email}}</span> Пользователь зарегистрирован в нашей системе. Нажмите кнопку "отправить приглашение", мы прикрепим этого пользователя к Вашей компании и отправим ему приглашение. Как только пользователь подтвердит ваше приглашение, он сразу же сможет работать в Вашей компании...
</script>
<!-- Пользователь не существует-->
<script id="template-role-row" type="text/x-handlers-template">
    <tr class="gradeX">
        <td>@{{display_name}}</td>
        <td>@{{description}}</td>
        <td><a href="/@{{company_name}}/saas/role/edit/@{{role_id}}">Настроить</a></td>
    </tr>
</script>
