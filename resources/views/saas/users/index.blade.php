@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('switcher')
    <ul data-toggle="tabs" class="nav nav-tabs">
        <li class="active"><a href="#first9">Сотрудники</a></li>
        <li><a href="#third9">Роли</a></li>
    </ul>
@stop

@section('buttons')
    @include('partials.cabinet.button_add_simple')
@stop

@section('content')
    <div class="offcanvas"></div>


        <div class="card tab-content filter-page">
            <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
                <div class="section-body">
                    <div class="filter-menu" id="off-canvas1" style="left: 60px;">
                        <nav>
                            <div class="expanded">
                                <a href="#">
                                    <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                                </a>
                            </div>
                            <ul>
                                <form class="form">
                                    <li>
                                        <div class="form-group floating-label">
                                            <input type="text" class="form-control" style="margin-top: 8px;" id="user-name-filter">
                                            <label for="user-name-filter">Имя:</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group floating-label">
                                            <input type="text" class="form-control" style="margin-top: 8px;" id="user-patr-filter">
                                            <label for="user-patr-filter">Отчество:</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group floating-label">
                                            <input type="text" class="form-control" style="margin-top: 8px;" id="user-lastname-filter">
                                            <label for="user-lastname-filter">Фамилия:</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group floating-label">
                                            <input type="text" class="form-control" style="margin-top: 8px;" id="user-email-filter">
                                            <label for="user-email-filter">e-mail:</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group floating-label">
                                            <input type="text" class="form-control" style="margin-top: 8px;" id="user-phone-filter">
                                            <label for="user-phone-filter">Телефон:</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <select id="status-selector" class="form-control select2-list input-sm"
                                                    data-placeholder="Выберите статус">
                                                <option>&nbsp;</option>
                                                <option value="all">Все статусы</option>
                                                @if ( !empty($statuses) )
                                                    @foreach ($statuses as $key=>$val)
                                                        <option value="{{$val}}">{{$key}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-group">
                                            <select id="role-selector" class="form-control select2-list input-sm"
                                                    data-placeholder="Выберите роль">
                                                <option>&nbsp;</option>
                                                <option value="all">Все роли</option>
                                                @if ( !empty($roles) )
                                                    @foreach ($roles as $key=>$val)
                                                        <option value="{{$val['id']}}">{{$val['display_name']}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </li>
                                </form>
                            </ul>
                        </nav>
                    </div>
                    <div class=" section-body">
                        <div class="table_wrap">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card" id="droptarget-hidden-columns">
                                        <div class="card-head">
                                            <header>Скрытые столбцы</header>
                                        </div>
                                        <div class="card-body">
                                            <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                                <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                            </div>
                                        </div>
                                        <!--end .card-body -->
                                    </div>
                                    <div class="k-grid-container" id="grid"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="tab-pane" id="third9">
            <div class="section-body">
                <div class="table_wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="datatable1" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Роль</th>
                                        <th>Описание</th>
                                        <th>Настроить</th>
                                    </tr>
                                    </thead>
                                    <tbody id="roles_list">
                                    @foreach($roles as $role)
                                        <tr class="gradeX">
                                            <td>{{{$role['display_name']}}}</td>
                                            <td>{{{$role['description']}}}</td>
                                            <td><a href="/{{{$company->name}}}/saas/role/edit/{{{$role['id']}}}">Настроить</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>




    <!--end content-->
@stop

@section('modals')
    <div class="offcanvas create-company">
        <div id="offcanvas-rate-info" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <div class="card-head tables-tabs">
                    <ul class="nav nav-tabs " data-toggle="tabs">
                        <li class="active"><a href="#first8">Сотрудники</a></li>
                        <li><a href="#second8">Роли</a></li>
                    </ul>
                </div>
                <br/>
                <div class="tab-content">
                    <div class="tab-pane active" id="first8">
                        <form class="margin-no form floating-label">
                            <p>Лицензия рабочих мест (чел): <span id="enabled-users-counter">{{{$UserAddRateInfo['enabledUsers']}}}</span></p>
                            <p>Занято: <span id="current-users-counter">{{{$UserAddRateInfo['currentUsers']}}}</span></p>
                            @if(!$UserAddRateInfo['result'])
                                <p>Свободных мест больше нет <a href="#">Измените свой тарифный план.</a></p>
                            @endif
                            <div class="form-group">
                                <button onclick="closeRateInfoTab()" type="button" class="btn btn-default-light ink-reaction btn-sm">Отмена</button>
                                <button onclick="showInviteTab()" type="button" class="btn ink-reaction btn-primary btn-sm">Продолжить</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="second8">
                        <form id="role-create-form" class="form">
                            <div class="form-group floating-label">
                                <input name="display_name" type="text" class="form-control input-sm">
                                <label for="display_name">Название</label>
                            </div>
                            <div class="form-group floating-label">
                                <textarea name="description" class="form-control input-sm" rows="3" placeholder=""></textarea>
                                <label for="description">Описание</label>
                            </div>
                            <button type="button" onclick="createRoleSubmit()" class="btn ink-reaction btn-primary btn-sm">Добавить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="role-create-callback" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <br/>

                <div class="tab-content">
                    <form class="form">
                        <p>Роль добавлена. Настроить права можно при редактировании роли.</p>

                        <div class="form-group">
                            <button type="button" onclick="hideRoleCreateCallbackTab()" class="btn btn-default-light ink-reaction btn-sm">Закрыть</button>
                            <button type="button" onclick="showRoleCreateTab()" class="btn ink-reaction btn-primary btn-sm">Добавить еще</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="offcanvas-invite-user" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <div>
                    <span class="text-lg text-bold text-primary">Приглашение пользователя</span>
                    <br/>

                    <div class="tab-content">
                        <div class="tab-pane active" id="first8">
                            <form id="invite-form" class="margin-no form floating-label">
                                <div class="form-group floating-label">
                                    <input type="email" name="email" class="form-control input-sm">
                                    <label for="email">E-mail</label>
                                </div>
                                <p id="invite-user-status"></p>
                                <div class="form-group">
                                    <button onclick="hideInviteTab()" type="button"
                                            class="btn btn-default-light ink-reaction btn-sm">Отмена
                                    </button>
                                    <button onclick="submitInvite()" type="button"
                                            class="btn ink-reaction btn-primary btn-sm">Отправить приглашение
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')

    @include('saas.users.partials.templates-users')

    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>

    <script type="application/javascript" src="/js/handlebars.min.js"></script>

    <script type="text/javascript">

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function showRoleCreateTab() {
            $('#role-create-form [name=display_name]').val('');
            $('#role-create-form [name=description]').val('');
            window.materialadmin.AppOffcanvas.openOffcanvas('#offcanvas-rate-info');
        }

        function createRoleSubmit() {
            if($('#role-create-form').valid()) {
                var display_name = $('#role-create-form [name=display_name]').val();
                var description = $('#role-create-form [name=description]').val();

                var data = {
                    name: display_name,
                    description: description
                };

                $.ajax({
                    url: '/{{{$company->name}}}/saas/ajax/roles/create',
                    method: "POST",
                    data: data,
                    context: this,
                    success: function (result, status, xhr) {
                        showRoleCreateCallbackTab();
                        var template_data = {
                            role_id: result.role.id,
                            description: result.role.description,
                            display_name: result.role.display_name,
                            company_name: '{{{$company->name}}}'
                        };
                        var source   = $("#template-role-row").html();
                        var template = Handlebars.compile(source);
                        $('#roles_list').append(template(template_data));
                    },
                    error: function (xhr, status, error) {
                        toastr.error('Произошла ошибка создания роли.');
                    }
                });
            }
        }

        function showInviteTab() {
            window.materialadmin.AppOffcanvas.openOffcanvas('#offcanvas-invite-user');
        }

        function showRoleCreateCallbackTab() {
            window.materialadmin.AppOffcanvas.openOffcanvas('#role-create-callback');
        }

        function hideRoleCreateCallbackTab() {
            window.materialadmin.AppOffcanvas.closeOffcanvas('#role-create-callback');
        }

        function hideInviteTab() {
            window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-invite-user');
        }

        function closeRateInfoTab() {
            window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-rate-info');
        }

        function submitInvite(btn) {
            var email = $('#invite-form [name=email]').val();
            if (IsEmail(email)) {
                var data = {
                    email: email,
                    company_id: {{{$company->id}}}
                };
                $.ajax({
                    url: '/{{{$company->name}}}/saas/ajax/users/inviteUser',
                    method: "POST",
                    data: data,
                    context: this,
                    success: function (result, status, xhr) {
                        if (result.success === true) {
                            toastr.success('Приглашение успешно отправлено.');
                            hideInviteTab();
                        } else {
                            toastr.error('Произошла ошибка при попытке отправить приглашение.');
                        }
                    },
                    error: function (xhr, status, error) {
                        e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                        toastr.error('Произошла ошибка при попытке отправить приглашение. Попробуйте позже.');
                    }
                });

            } else {
                toastr.error('Введен неправильный email');
            }
        }

        // Check that user email exist.
        function checkUserEmailExist() {
            var email = $('#invite-form [name=email]').val();
            if(IsEmail(email)) {
                var data = {email: email};
                $.ajax({
                    url: '/{{{$company->name}}}/saas/ajax/users/checkEmailExists',
                    method: "POST",
                    data: data,
                    context: this,
                    success: function (result, status, xhr) {
                        if(result.result.exist === true ) {
                            var source   = $("#template-user-exist").html();
                            var template = Handlebars.compile(source);
                            $('#invite-user-status').html(template({value:result.result}));
                        } else {
                            var source   = $("#template-user-not-exist").html();
                            var template = Handlebars.compile(source);
                            $('#invite-user-status').html(template({value:result.result}));
                        }
                    },
                    error: function (xhr, status, error) {
                        e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                        $('#invite-user-status').html('');
                    }
                });
            } else {
                $('#invite-user-status').html('');
            }
        }

        function CustomDeleteRow(e) {
            e.preventDefault();
            var item = this.dataItem($(e.currentTarget).closest("tr"));
            var idx = this.dataSource.indexOf(item);

            var isDelete = confirm("Вы хотите удалить пользователя из компании?");

            if(isDelete) {
                var data = {
                    user_id: item.id
                };
                $.ajax({
                    url: GridConfig.baseGridCrudUrl + "delete",
                    method: "POST",
                    data: data,
                    context: this,
                    success: function (result, status, xhr) {
                        $('#grid').data("kendoGrid").dataSource.remove(item);
                        e.success(JSON.parse(JSON.stringify(result)));
                    },
                    error: function (xhr, status, error) {
                        e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                    }
                });
            }
        }

        GridConfig = {};
        window.Filters = {};
        FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 1;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_users_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/saas/ajax/users/';
        // Путь до карты
        GridConfig.baseCardUrl = '/' + '{{{$company->name}}}' + '/saas/card/user/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'users';
        // Можно ли редактировать
        GridConfig.editable = false;
        // Можно ли делать группировку
        GridConfig.groupable = false;

        // Шаблон копирования строк
        GridConfig.copyFields = [
            'name',
            'patronymic',
            'status',
            'description',
            'email',
            'phone'
        ];
        // Конфиг колонок грида
        GridConfig.columns = [
            {field: "id", title: "ID", width: "50px"},
            {
                field: "name",
                title: "Имя",
                width: "150px"
            },
            {
                field: "patronymic",
                title: "Отчество",
                width: "150px"
            },
            {
                field: "last_name",
                title: "Фамилия",
                width: "150px"
            },
            {
                field: "email",
                title: "e-mail",
                width: "150px"
            },
            {
                field: "phone",
                title: "Телефон",
                width: "150px"
            },
            {
                field: "display_name",
                title: "Роль",
                width: "150px"
            },
            {
                field: "status",
                title: 'Статус',
                width: "130px",
                template: "#= cellTemplate(status, 'status') #",
            },
            {
                command: [
                    {
                        name: "delete",
                        text: "",
                        click: CustomDeleteRow,
                        className: "md md-delete",
                        width: "32px"
                    }
                ],
                title: "&nbsp;",
                width: "170px",
                attributes: {"class": "button-column"}
            }
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string"},
                patronymic: {type: "string"},
                last_name: {type: "string"},
                phone: {type: "string", defaultValue: ''},
                email: {type: "string", defaultValue: ''},
                status: {type: "number"},
                display_name: {type: "string"}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Конфиг аггрегатора группировок грида
        GridConfig.aggregateFields = [
            {field: "name", aggregate: "count"},
            {field: "status", aggregate: "count"},
            {field: "type", aggregate: "count"}
        ];
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'status',
            'name',
            'patr',
            'lastname',
            'phone',
            'email',
            'role'
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'status',
            'name',
            'patr',
            'lastname',
            'phone',
            'email',
            'role'
        ];

        GridConfig.noServerSorting = true;
        GridConfig.noServerPaging = true;

        // Форматирует вывод статусов и типов в гриде.
        function cellTemplate(value, type) {
            var x;
            switch (type) {
                case 'type':
                    x = $.grep(Filters.typesDataSource, function (n, i) {
                        return n.value == value;
                    });
                    if (x.length > 0) {
                        return x[0].text;
                    }
                    break;
                case 'status':
                    x = $.grep(Filters.statusesDataSource, function (n, i) {
                        return n.value == value;
                    });
                    if (x.length > 0) {
                        return x[0].text;
                    }
                    break;
            }
            return '-';
        }

        // Возвращает в шаблон группировок значение группировки
        function getStatusLabel(value) {

            var x = $.grep(Filters.statusesDataSource, function (item) {
                return item.value == value;
            });
            return x[0].text;
        }

        // Возвращает в шаблон группировок значение группировки
        function getTypeLabel(value) {

            var x = $.grep(Filters.typesDataSource, function (item) {
                return item.value == value;
            });
            return x[0].text;
        }

        $(document).ready(function () {

            $('#role-create-form').validate({
                rules: {
                    "display_name": {
                        required: true,
                        minlength: 2
                    },
                    "description": {
                        maxlength: 255
                    }
                },
                messages: {
                    "display_name": {
                        required: "Введите название роли",
                        minlength: "Минимальная длинна названия роли 2 символа"
                    },
                    "description": {
                        maxlength: "Максимальное количество символов 255"
                    }
                }
            });

            $('#invite-form [name=email]').on('keyup', function () {
                checkUserEmailExist();
            });
            $('#header_add_simple_btn').click(function (e) {
                // Refresh rate data
                $.ajax({
                    url: '/{{{$company->name}}}/saas/ajax/users/checkAddUser',
                    method: 'POST',
                    success: function (result) {
                        $('#enabled-users-counter').html(result.enabledUsers);
                        $('#current-users-counter').html(result.currentUsers);
                    },
                    error: function (result) {
                    }
                });

                if($('#offcanvas-rate-info').hasClass('active')){
                    window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-rate-info');
                } else {
                    window.materialadmin.AppOffcanvas.openOffcanvas('#offcanvas-rate-info');
                }
            });

            Filters = {

                isInit: false,

                city_input: null,
                patr_input: null,
                lastname_input: null,
                phone_input: null,
                email_input: null,

                // Наши слайдеры.
                profit: null,
                old_price: null,
                new_price: null,

                // Наши селекторы
                typeSelector: null,
                statusSelector: null,


                // Допустимые значения dropdownlist
                typesDataSource: {},
                statusesDataSource: {},

                // Массивы диапазонов слайдеров.
                limits: null,

                initFilters: function () {
                    var self = this;

                    // Наши слайдеры )
                    this.profit = $("#slider-range-profit");
                    this.old_price = $("#slider-range-old-price");
                    this.new_price = $("#slider-range-price");

                    this.city_input = $("#user-name-filter");
                    this.lastname_input = $("#user-lastname-filter");
                    this.patr_input = $("#user-patr-filter");
                    this.email_input = $("#user-email-filter");
                    this.phone_input = $("#user-phone-filter");

                    //Селекторы
                    this.typeSelector = $('#type-selector');
                    this.statusSelector = $('#status-selector');
                    this.roleSelector = $('#role-selector');

                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    this.city_input.keyup(function () {
                        FilterBuilder.addFilter('name', {
                            field: "name",
                            operator: "startWith",
                            value: self.city_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.patr_input.keyup(function () {
                        FilterBuilder.addFilter('patr', {
                            field: "patr",
                            operator: "startWith",
                            value: self.patr_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.lastname_input.keyup(function () {
                        FilterBuilder.addFilter('lastname', {
                            field: "lastname",
                            operator: "startWith",
                            value: self.lastname_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.phone_input.keyup(function () {
                        FilterBuilder.addFilter('phone', {
                            field: "phone",
                            operator: "startWith",
                            value: self.phone_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.email_input.keyup(function () {
                        FilterBuilder.addFilter('email', {
                            field: "email",
                            operator: "startWith",
                            value: self.email_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    // Селектор статуса
                    this.statusSelector.select2({
                        minimumResultsForSearch: -1,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('status');
                                } else {
                                    FilterBuilder.addFilter('status', {
                                        field: "status",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    this.roleSelector.select2({
                        minimumResultsForSearch: -1,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('role');
                                } else {
                                    FilterBuilder.addFilter('role', {
                                        field: "role",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },


                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "status" && value.operator == 'eq') {
                        $("#status-selector").select2("val", value.value);
                        FilterBuilder.addFilter('status', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                    if (value.field == "type" && value.operator == 'eq') {
                        $("#type-selector").select2("val", value.value);
                        FilterBuilder.addFilter('status', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                    if (value.field == "name" && value.operator == 'startWith') {
                        $('#product-name').val(value.value);
                        FilterBuilder.addFilter('name', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                },

                initDropDownListDataSource: function () {
                    var self = this;
                    var option;
                    this.statusesDataSource = [];
                    $.each(this.limits.statuses, function (index, value) {
                        option = {text: index, value: value};
                        self.statusesDataSource.push(option);
                    });
                }
            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            }

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });

    </script>
@stop