@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/saas/employees">Пользователи</a></li>
            <li class="active">Редактирование роли</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Редактирование роли</h2>

                <div class="preview-wrp">
                    <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и
                        открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока,
                        окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.
                        Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при
                        нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие
                        и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого
                        блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие
                        кнопоки.
                    </div>
                </div>
            </div>
        </div>
        <form id="create-form" class="form crm_edit floating-label">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <div class="card">
                        <div class="card-body">
                            <!-- <h3>Роль</h3> -->
                            <div class="form-group floating-label">
                                <input name="display_name" type="text" class="form-control" value="{{{$role->display_name}}}">
                                <label for="display_name">Роль</label>
                            </div>
                            <!-- <h3>Описание</h3> -->
                            <div class="form-group floating-label">
                                <textarea name="description" class="form-control" rows="3"
                                          placeholder="">{{{$role->description}}}</textarea>
                                <label for="description">Описание</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                    distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique
                                    pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12">
                    <h3>Права на модули</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-head">
                                <ul class="nav nav-tabs nav-styled" data-toggle="tabs">
                                    <li class="active"><a href="#crm_permissions_tab">CRM</a></li>
                                    <li class=""><a href="#landing_permissions_tab">Страницы</a></li>
                                </ul>
                            </div>
                            <div class="tab-content crm_dropdowns">
                                <div class="tab-pane active" id="crm_permissions_tab">
                                    <div class="row">
                                        <?php $i = 0; ?>
                                        <?php $smallFactor = 2;?>
                                        @foreach($permissions_groups['crm']['components'] as $component_key => $component)
                                            @if(!isset($component['class']))
                                            <div class="col-md-4 col-sm-6">
                                                <h4>{{{$component['label']}}}</h4>
                                                @foreach($allPermissionsArray['crm'][$component_key] as $perm_key => $perm)
                                                <div class="row">
                                                    @if(isset($perm['display_name']))
                                                        <label class="col-md-4">{{$perm['display_name']}}</label>
                                                    @else
                                                        <label class="col-md-4">{{$perm[array_keys($perm)[0]]['display_name']}}</label>
                                                    @endif

                                                    <dl class="dropdown col-lg-7 col-md-8" data-permissions-count="{{{sizeof($allPermissionsArray['crm'][$component_key][$perm_key]['options'])}}}"
                                                        data-permission="crm.{{$component_key}}.{{$perm_key}}" data-option="{{$allPermissionsArray['crm'][$component_key][$perm_key]['option']}}">
                                                        <dt>
                                                            <a>
                                                               <span><span class="permit"
                                                                           style="background-color: {{$allPermissionsArray['crm'][$component_key][$perm_key]['options'][$allPermissionsArray['crm'][$component_key][$perm_key]['option']]['color']}}">&nbsp;</span>{{$allPermissionsArray['crm'][$component_key][$perm_key]['options'][$allPermissionsArray['crm'][$component_key][$perm_key]['option']]['label']}}</span>

                                                                <div class="arrow"><i class="md md-expand-more"></i>
                                                                </div>
                                                            </a>
                                                        </dt>
                                                        <dd>
                                                            <ul class="list-unstyled" style="display: none;">
                                                                @foreach($allPermissionsArray['crm'][$component_key][$perm_key]['options'] as $opt)
                                                                    <li><a href="javascript:void(0)" data-option="{{{$opt['value']}}}"><span
                                                                                    class="permit"
                                                                                    style="background-color: {{{$opt['color']}}}">&nbsp;</span>{{{$opt['label']}}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </dd>
                                                    </dl>
                                                </div>
                                                @endforeach
                                            </div>
                                            @else
                                                <div class="col-md-4 col-sm-6 {{{$component['class']}}}">
                                                    <h4>{{{$component['label']}}}</h4>
                                                    @foreach($allPermissionsArray['crm'][$component_key] as $perm_key => $perm)
                                                    <div class="row">
                                                        @if(isset($perm['display_name']))
                                                            <p class="col-md-12 no-margin">{{$perm['display_name']}}</p>
                                                        @else
                                                            <p class="col-md-12 no-margin">{{$perm[array_keys($perm)[0]]['display_name']}}</p>
                                                        @endif
                                                        @foreach($perm as $key => $checkbox)
                                                                <div class="col-xs-6">
                                                                    <div class="checkbox checkbox-inline checkbox-styled">
                                                                        <label><input data-permission="{{$checkbox['perm_key']}}" type="checkbox" {{{$checkbox['option']}}}><span></span>{{{$checkbox['label']}}}</label>
                                                                    </div>
                                                                </div>
                                                        @endforeach
                                                    </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                                <?php $i++; ?>
                                                @if($i===$smallFactor)
                                                    <div class="clear hidden-lg hidden-md visible-sm visible-xs"
                                                         style="clear: both"></div>
                                                    @if($smallFactor == 2)
                                                        <?php $smallFactor = 1; ?>
                                                    @elseif ($smallFactor == 1)
                                                        <?php $smallFactor = 3; ?>
                                                    @elseif ($smallFactor == 3)
                                                        <?php $smallFactor = 2; ?>
                                                    @endif
                                                @endif
                                                @if($i%3 === 0)
                                                    <div class="clear hidden-sm" style="clear: both"></div>
                                                    <?php $i = 0; ?>

                                                @endif

                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane" id="landing_permissions_tab">
                            <div class="row">
                                <?php $i = 0; ?>
                                @foreach($permissions_groups['landingControl']['components'] as $component_key => $component)

                                    @if(!isset($component['class']))
                                        <div class="col-md-4 col-sm-6">
                                            <h4>{{{$component['label']}}}</h4>
                                            {{--права--}}

                                                    @if(isset($allPermissionsArray['landingControl']))
                                                    @foreach($allPermissionsArray['landingControl'][$component_key] as $perm_key => $perm)
                                                        <div class="row">
                                                            @if(isset($perm['display_name']))
                                                                <label class="col-md-4">{{$perm['display_name']}}</label>
                                                            @else
                                                                <label class="col-md-4">{{$perm[array_keys($perm)[0]]['display_name']}}</label>
                                                            @endif

                                                            <dl class="dropdown col-lg-7 col-md-8" data-permissions-count="{{{sizeof($allPermissionsArray['landingControl'][$component_key][$perm_key]['options'])}}}"
                                                                data-permission="landingControl.{{$component_key}}.{{$perm_key}}" data-option="{{$allPermissionsArray['landingControl'][$component_key][$perm_key]['option']}}">
                                                                <dt><a>
                                                                          <span><span class="permit"
                                                                                      style="background-color: {{$allPermissionsArray['landingControl'][$component_key][$perm_key]['options'][$allPermissionsArray['landingControl'][$component_key][$perm_key]['option']]['color']}}">&nbsp;</span>{{$allPermissionsArray['landingControl'][$component_key][$perm_key]['options'][$allPermissionsArray['landingControl'][$component_key][$perm_key]['option']]['label']}}</span>

                                                                        <div class="arrow"><i class="md md-expand-more"></i></div>
                                                                    </a>
                                                                </dt>
                                                                <dd>
                                                                    <ul class="list-unstyled" style="display: none;">
                                                                        @foreach($allPermissionsArray['landingControl'][$component_key][$perm_key]['options'] as $opt)
                                                                            <li><a href="javascript:void(0)" data-option="{{{$opt['value']}}}"><span
                                                                                            class="permit"
                                                                                            style="background-color: {{{$opt['color']}}}">&nbsp;</span>{{{$opt['label']}}}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            @else
                                                <div class="col-md-4 col-sm-6 {{{$component['class']}}}">
                                                    <h4>{{{$component['label']}}}</h4>
                                                    @if(isset($allPermissionsArray['landingControl']))
                                                    @foreach($allPermissionsArray['landingControl'][$component_key] as $perm_key => $perm)
                                                        <div class="row">
                                                            @if(isset($perm['display_name']))
                                                                <p class="col-md-12 no-margin">{{$perm['display_name']}}</p>
                                                            @else
                                                                <p class="col-md-12 no-margin">{{$perm[array_keys($perm)[0]]['display_name']}}</p>
                                                            @endif
                                                            @foreach($perm as $key => $checkbox)
                                                                <div class="col-xs-6">
                                                                    <div class="checkbox checkbox-inline checkbox-styled">
                                                                        <label><input data-permission="{{$checkbox['perm_key']}}" type="checkbox" {{{$checkbox['option']}}}><span></span>{{{$checkbox['label']}}}</label>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    @endforeach
                                                    @endif
                                                </div>
                                            @endif
                                                <?php $i++; ?>
                                                @if($i%3 === 0)
                                                    <div class="clear hidden-sm" style="clear: both"></div>
                                                    <?php $i=0; ?>
                                                @endif
                                                @if($i%2 === 0)
                                                    <div class="clear hidden-lg hidden-md visible-sm visible-xs" style="clear: both"></div>
                                                @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <button id="reset-btn1" type="button" class="btn btn-default-light ink-reaction">Отмена</button>
                            <button id="submit-btn1" type="button" class="btn ink-reaction btn-primary">Сохранить</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@stop

@section('scripts')

    <script type="text/javascript">

        function resetBtn() {
            alert('reset');
        }

        $('#create-form').validate({
                    rules: {
                        "name": {
                            required: true,
                            minlength: 1,
                            maxlength: 255
                        },
                        "fileLinks": {
                            required: true
                        }
                    },
                    messages: {
                        "fileLinks": {
                            required: "Выберите html файл"
                        },
                        "name": {
                            required: "Введите название для шаблона",
                            minlength: "Минимальная длинна названия шаблона - 1 символ",
                            maxlength: "Введите более короткое название"
                        }
                    }
                }
        );

        function submitForm() {
            if ($('#create-form').valid()) {
                var display_name = $('#create-form input[name=display_name]').val();
                var description = $('#create-form textarea[name="description"]').val();

                var crm_selects = {};
                var crm_checkboxes = {};
                var landing_selects = {};

                $.each($('#crm_permissions_tab dl'), function(key, value){
                    crm_selects[$(value).data().permission] = {};
                    crm_selects[$(value).data().permission]['value'] = parseInt($(value).data().option);
                    crm_selects[$(value).data().permission]['count'] = parseInt($(value).data().permissionsCount);
                });

                $.each($('#landing_permissions_tab dl'), function(key, value){
                    landing_selects[$(value).data().permission] = {};
                    landing_selects[$(value).data().permission]['value'] = parseInt($(value).data().option);
                    landing_selects[$(value).data().permission]['count'] = parseInt($(value).data().permissionsCount);
                });

                $.each($('#crm_permissions_tab input[type=checkbox]'), function(key, value){
                    crm_checkboxes[$(value).data().permission] = $(value).prop('checked');
                });

                var data = {
                    display_name: display_name,
                    description: description,
                    crm_selects: crm_selects,
                    landing_selects: landing_selects,
                    crm_checkboxes: crm_checkboxes,
                    id:{{{$role->id}}}
                };

                $.ajax({
                    url: "/{{{$company->name}}}/saas/ajax/roles/update",
                    method: "PUT",
                    data: data,
                    success: function (result) {
                        toastr.success("Роль успешно обновлена");
                        //window.location.href = "/{{{$company->name}}}/landingControl/html-templates";
                    },
                    error: function (result) {
                        toastr.error("Произошла ошибка в процессе обновления роли");
                    }
                });
            }
        }

        $(document).ready(function () {

            //dropdown
            $(".dropdown dt > a").click(function() {
                $(this).parents('.dropdown').find('dd ul').toggle();
            });
            // на выбор
            $(".dropdown dd ul li > a").click(function() {
                var text = $(this).html();
                $(this).parents('.dropdown').find('dt a span').html(text).css('color', "#000");
                $(this).parents('.dropdown').find('dd ul').hide();
                $(this).closest('dl').data('option', $(this).data('option'));
                return false;
            });
            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
                var allDropDowns = $(".dropdown dd ul");
                $clicked = $clicked.closest('dl');
                $.each(allDropDowns, function (key, item) {
                    if (!$(item).closest('dl').is($clicked)) {
                        $(item).hide();
                    }
                });
            });
            // end dropdown

            $('#reset-btn1, #header-cancel-btn').click(function () {
                resetBtn();
            });
            $('#submit-btn1, #header-save-btn').click(function () {
                submitForm();
            });

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });
            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

            // Чекбоксы
            $('.checkbox.checkbox-inline.checkbox-styled').on('click', function(e) {
                var checkbox = $(e.currentTarget).find('input');
                if (checkbox.prop("checked")) {
                    checkbox.prop("checked", false)
                } else {
                    checkbox.prop("checked", true)
                }
            });
        });
    </script>
@stop