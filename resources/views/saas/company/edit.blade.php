@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="#">Настройки</a></li>
            <li class="active">Настройки компании</li>
        </ol>
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-primary">Настройки компании</h2>
            </div>
        </div>
        <form id="company-edit-form" class="form-horizontal" role="form">
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="company_display_name" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Название компании <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="display_name" value="{{{$company->display_name}}}" id="company_display_name" type="text" class="form-control" placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="company_name" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите алиас вашей компании">Алиас компании <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="name" value="{{{$company->name}}}" id="company_name" type="text" class="form-control"  placeholder="">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название отделов">Отделы <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <input name="divisions" value="{{{$divisions}}}" type="text" class="form-control" value="IT-отдел" data-role="tagsinput">
                                </div>
                            </div>

                         {{--    <div class="form-group">
                                <label for="regular44" class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto" title="Введите название вашей компании">Часовой пояс <span ><i class="md md-help"></i></span></label>
                                <div class="col-sm-9">
                                    <select  id="regular44" class="form-control select2-list" data-placeholder="">
                                        <option value="AK">&nbsp;</option>
                                        <option value="AK">UTC/GMT+11 (Камчатка)</option>
                                        <option value="HI">UTC/GMT+11 (Магадан)</option>
                                        <option value="AK">UTC/GMT+11 (Камчатка)</option>
                                        <option value="HI">UTC/GMT+11 (Магадан)</option>
                                        <option value="AK">UTC/GMT+11 (Камчатка)</option>
                                        <option value="HI">UTC/GMT+11 (Магадан)</option>
                                        <option value="AK">UTC/GMT+11 (Камчатка)</option>
                                        <option value="HI">UTC/GMT+11 (Магадан)</option>
                                        <option value="AK">UTC/GMT+11 (Камчатка)</option>
                                        <option value="HI">UTC/GMT+11 (Магадан)</option>
                                        <option value="AK">UTC/GMT+11 (Камчатка)</option>
                                        <option value="HI">UTC/GMT+11 (Магадан)</option>
                                    </select>
                                </div>
                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 hidden-sm hidden-xs">
                    <div class="note-wrap">
                        <div class="tip">
                            <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row margin-bottom-xxl">
                <div class="col-sm-12">
                    <ul class="list-unstyled list-inline">
                        <li><button id="update-company-btn" type="button" class="btn ink-reaction btn-primary btn-sm">Сохранить</button></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 opacity-75">
                    <p class="no-margin">Обновлено <span>{{ date("d.m.Y",strtotime($company->updated_at)) }}</span> в <span>{{ date("G:i",strtotime($company->updated_at)) }}</span></p>
                    <p class="no-margin">Создано <span>{{ date("d.m.Y",strtotime($company->created_at)) }}</span> в <span>{{ date("G:i",strtotime($company->created_at)) }}</span></p>
                </div>
            </div>
        </form>
    </section>
@stop

@section('modals')

@stop

@section('scripts')
    <script type="text/javascript">
        $().ready(function () {
            $('.form select').select2();
            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });
            $('#company-edit-form').validate({
                rules: {
                    "display_name": {
                        required: true,
                        minlength: 1,
                        maxlength:255
                    },
                    "name": {
                        required: true,
                        minlength: 1,
                        maxlength:128
                    }
                },
                messages: {
                    "display_name": {
                        required: "Введите название поля",
                        minlength: "Минимальная длинна названия - 1 символ",
                        maxlength: "Введите более короткое название"
                    },
                    "name": {
                        required: "Введите название поля",
                        minlength: "Минимальная длинна названия - 1 символ",
                        maxlength: "Введите более короткое название"
                    }
                },
                submitHandler: function () {

                }
            });
            $('#update-company-btn').click(function () {
                if ($('#company-edit-form').valid()) {
                    var name = $('input[name="name"]').val();
                    var display_name = $('input[name="display_name"]').val();
                    var divisions = $('input[name="divisions"]').val();
                    var data = {
                        display_name: display_name,
                        divisions: divisions,
                    };
                    if (name !== '{{{$company->name}}}') {
                        data.name = name;
                    }
                    $.ajax({
                        url: "/{{{$company->name}}}/saas/ajax/company/update",
                        method: "PUT",
                        data: data,
                        success: function (result) {
                            toastr.success("Изменения успешно применены");
                            if('{{$company->name}}' !== result.name) {
                                window.location = '/' + result.name + '/saas/companySettings';
                            }
                        },
                        error: function (result) {
                            toastr.error("Произошла ошибка при обновлении");
                        }
                    });
                }
            });
        });
    </script>
@stop