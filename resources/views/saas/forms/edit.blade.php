@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('content')
    <section class="tab-pane active" id="first9">
        <div class="section-body no-margin">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary">Редактирование дополнительного поля {{{$form->label}}}</h2>
                    <div class="preview-wrp">
                        <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие кнопоки.</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class=" col-lg-10 col-lg-offset-1 col-sm-12">
                    <div class="card">
                        <form id="create_form" class="form" role="form">
                            <div class="card-body row">
                                <div class="col-lg-4 col-sm-6 variables">

                                    <div class="form-group floating-label">
                                        <input name="label" value="{{{$form->label}}}" type="text" class="form-control input-sm" id="regular9">
                                        <label for="regular9">Заголовок</label>
                                    </div>
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" value="">
                                            <span class="opacity-75">Скрыть в статистику</span>
                                        </label>
                                    </div>
												<span class="text-sm opacity-50 note">
													Smart-поле «День рождения» – еще один повод напомнить клиенту о себе. Постройте список контактов с предстоящими днями рождения, сформируйте поздравительные рассылки, постройте списки обзвона. Включите автоматическое создание задачи для того, чтобы не пропустить события.
												</span>
                                </div>
                                <div class="col-lg-4 col-lg-offset-1 variables col-sm-5 col-sm-offset-1">
                                    <ul class="list divider-full-bleed list-events ui-sortable" data-sortable="true">
                                        <li class="tile ui-draggable ui-draggable-handle" >
                                            <div class="form-group floating-label">
                                                <input type="text" class="form-control input-sm" id="regular10">
                                                <label for="regular10">Значение по умолчанию</label>
                                                <span class="btn btn-icon-toggle btn-default del"><i class="md md-delete"></i></span>
                                            </div>
                                        </li>
                                    </ul>
                                    {{--<span id="add-status-field" class="add-tags">Добавить вариант</span>--}}
                                    <div class="form-group">
                                        <p></p>
                                        <div class="margin-bottom-xl">
                                            <!-- <select class="form-control select2-list input-sm" data-placeholder="exapmpe">
                                                <option>&nbsp;</option>
                                                <option value="рр">Тег1</option>
                                                <option value="рр">Тег2</option>
                                                <option value="рр">Тег3</option>
                                            </select> -->
                                            <div class="form-group floating-label">
                                                <input type="text" class="form-control input-sm" id="tag">
                                                <label for="tag">Тег для вывода:</label>
                                            </div>
                                        </div>
													<span class="text-sm opacity-50 note">
														Smart-поле «День рождения» – еще один повод напомнить клиенту о себе. Постройте список контактов с предстоящими днями рождения, сформируйте поздравительные рассылки, постройте списки обзвона. Включите автоматическое создание задачи для того, чтобы не пропустить события.
													</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button id="cancel_btn" type="button" class="btn btn-default-light ink-reaction">Отменить</button>
                                        <button id="create_btn" type="button" class="btn ink-reaction btn-primary">Сохранить</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--end .section-body -->
    </section>
@stop

@section('modals')

@stop

@section('scripts')
    <script type="text/javascript">
        $().ready(function () {
            $('.form select').select2();
            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });
            $('#create_form').validate({
                rules: {
                    "label": {
                        required: true,
                        minlength: 1
                    }
                },
                messages: {
                    "label": {
                        required: "Введите название поля",
                        minlength: "Минимальная длинна названия - 1 символ"
                    }
                },
                submitHandler: function () {

                }
            });
            $('#create_btn').click(function () {
                if ($('#create_form').valid()) {
                    var label = $('input[name="label"]').val();
                    var data = {
                        label: label,
                        id: {{{$form->id}}},
                    };
                    $.ajax({
                        url: "/{{{$company->name}}}/saas/ajax/forms/update",
                        method: "PUT",
                        data: data,
                        success: function (result) {
                            toastr.success("Доп. поле успешно сохранено");
                        },
                        error: function (result) {
                            toastr.error("Произошла ошибка при обновлении");
                        }
                    });
                }
            });
            $('#cancel_btn').click(function () {
                window.history.back();
            });
        });
    </script>
@stop