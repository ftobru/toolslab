@extends('layouts.simple_private')

@section('buttons')
    @include('partials.cabinet.button_add')
    <div class="offcanvas"></div>
@stop


@section('content')
        <section class="profile companies">
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="text-primary">Мои компании </h2>
                    </div>
                </div>
                <?php
                $i = 0;
                $isClosed = false;
                ?>
                @foreach ($user_companies as $company)
                    <?php
                    if ($i % 2 == 0) {
                        echo '<div class="row user-companies-row">';
                        $isClosed = false;
                    };
                    ?>
                    <div class="col-sm-6">
                        <a href="/{{$company->name}}/crm/dashboard">
                            <div class="card">
                                <div class="card-body no-padding">
                                    <div class="alert alert-callout no-margin alert-primary-bright">
                                        <h3>{{ $company->display_name}}</h3>
                                        @if($company->status == 1)
                                            <p>Активна до <span>27.07.2014 </span><span> (3 дня)</span></p>
                                        @else
                                            <p>Заблокирована</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                    if ($i % 2 != 0) {
                        echo '</div>';
                        $isClosed = true;
                    };
                    $i++;
                    ?>
                @endforeach
                <?php
                if (!$isClosed) {
                    echo '</div>';
                    $isClosed = true;
                };
                ?>


                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="text-primary">Доступные компании</h2>
                    </div>
                </div>
                <?php
                $i = 0;
                $isClosed = false;
                ?>
                @foreach ($available_companies as $company)
                    @if ($i % 2 == 0)
                        <div class="row available-companies-row">;
                        <?php $isClosed = false;?>
                    @endif

                    <div class="col-sm-6">
                        <a href="/{{$company->name}}/crm/dashboard">
                            <div class="card">
                                <div class="card-body no-padding">
                                    <div class="alert alert-callout no-margin alert-primary-bright">
                                        <h3>{{ $company->display_name}}</h3>
                                        @if($company->status == \Saas\References\CompanyReference::STATUS_ACTIVE)
                                            {{-- <p>Активна до <span>27.07.2014 </span><span> (3 дня)</span></p>--}}
                                        @else
                                            {{--<p>Заблокирована</p>--}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php
                    if ($i % 2 != 0) {
                        echo '</div>';
                        $isClosed = true;
                    };
                    $i++;
                    ?>
                @endforeach
                <?php
                if (!$isClosed) {
                    echo '</div>';
                    $isClosed = true;
                };
                ?>

            </div>
            <!--end .row -->
            <!--end .section-body -->
        </div>
        </section>
@stop

@section('modals')
    <div class="offcanvas create-company">

        <div id="offcanvas-search" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <p>Создать компанию?</p>

                <p>При создании новой компании с Вашего баланса будут сняты средства за первый месяц использования
                    минимального тарифного.</p>

                <p>Тариф LiteLab - NNN рублей</p>

                <div class="form-group">
                    <button type="submit"
                            data-dismiss="offcanvas"
                            id="btn-cancel-company-create"
                            class="btn btn-default-light ink-reaction">
                        Отмена
                    </button>
                    <button type="submit"
                            {{--data-dismiss="offcanvas"--}}
                            href="#offcanvas-search2"
                            data-toggle="offcanvas"
                            data-backdrop="false"
                            class="btn ink-reaction btn-primary">
                        Создать компанию
                    </button>
                </div>
            </div>
        </div>

        <div id="offcanvas-search2" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <h3 class="text-primary">Создание новой компании</h3>

                <form id="create-company-form">
                    <div class="form-group">
                        <label for="company-name" class="control-label">Название</label>
                        <input type="text" class="form-control" id="company-name" name="company-name">
                        <div class="form-group floating-label has-error" id="login-error" style="display:none;">
                        <div class="form-control-line"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button
                                data-dismiss="offcanvas"
                                type="submit"
                                class="btn btn-default-light ink-reaction">
                            Отмена
                        </button>
                        <input type="button"
                               value="Продолжить"
                               id="btn-create-company"
                               onclick="createNewCompany(this)"
                               {{--data-dismiss="offcanvas"--}}
                               class="btn ink-reaction btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script type="text/javascript">

        var lock = false;

        function createNewCompany(button) {
            button.disabled = true;
            button.value = 'Отправка...';

            $('#login-error').hide('fast');
            if ($('#create-company-form').valid()) {
                var display_name = $('#company-name').val();
                var data = {display_name: display_name};
                $.ajax({
                    url: "/companies/ajax/createCompany",
                    method: "POST",
                    data: data,
                    success: function (result) {
                        if (result.result === true) {
                            var company = result.company;

                            var lastElem = $(".row.user-companies-row").last();
                            var count = lastElem.children().length;
                            if (count == 2) {
                                lastElem.after('<div class="row user-companies-row"></div>');
                                lastElem = $(".row.user-companies-row").last();
                            }
                            var status;
                            if (company.status == 1)
                                status = '<p>Активна до <span>27.07.2014 </span><span> (3 дня)</span></p>';
                            else {
                                status = '<p>Заблокирована</p>';
                            }
                            var id = 'mc' + performance.now();
                            id = id.replace('.', '-');
                            lastElem.append(' <div class="col-sm-6" id="' + id + '" style="display: none;"> \
                            <a href="/' + company.name + '/crm/dashboard"> \
                            <div class="card"> \
                            <div class="card-body no-padding"> \
                            <div class="alert alert-callout no-margin alert-primary-bright"> \
                            <h3>' + company.display_name + '</h3>' + status + ' </div> \
                            </div> \
                            </div> \
                            </a> \
                            </div>');
                            $('#' + id).show('slow');
                            window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-search2');

                        } else {
                            //TODO show error
                        }

                        button.disabled = false;
                        button.value = 'Создать';
                    },
                    error: function (result) {
                        button.disabled = false;
                        button.value = 'Создать';
                    }
                });
            } else {
                button.disabled = false;
                button.value = 'Создать';
            }
        }

        $(document).ready(function () {

            $('#btn-create-company').attr('disabled', false);

            $('#btn-cancel-company-create').click(function () {
                $('#offcanvas-search').modal('hide');
            });

            $('#create-company-form').validate({
                rules: {
                    "company-name": {
                        required: true,
                        minlength: 2
                    }
                },
                messages: {
                    "company-name": {
                        required: "Введите название компании",
                        minlength: "Минимальная длинна названия 2 символа"
                    }
                },
                submitHandler: function() {
                }
            });
        });

    </script>
@stop