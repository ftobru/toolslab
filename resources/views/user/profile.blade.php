@extends('layouts.simple_private')

@section('content')
        <section class="edit-profile1">
            <div class="section-body">
                <div class="row">
                    <div class="col-xs-11">
                        <h2 class="text-primary">Редактирование профиля</h2>
                    </div>
                </div>
                <div class="row">
                    <!-- <div class="col-md-4 col-sm-8 col-sm-offset-1"> -->
                    <div class="col-md-4 col-sm-12">
                        <form class="form floating-label" id="form-changename" role="form" novalidate="novalidate" {{--onsubmit="sendDataToChange('/user/ajax/changeName', this); return false;"--}}>
                            <div class="card">
                                <div class="card-head style-primary">
                                    <header>ФИО, пол</header>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="gender" class="control-label gender">Пол</label>
                                        <div class="radio_wrap">
                                            <label class="radio-inline radio-styled">
                                                <input type="radio" name="gendre"><span>Мужской</span>
                                            </label>
                                            <label class="radio-inline radio-styled">
                                                <input type="radio" name="gendre" checked=""><span>Женский</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group floating-label">
                                        <input type="text" name="name" class="form-control" id="regular1" value="{{ $user->name }}">
                                        <label for="regular1">Имя</label>
                                    </div>
                                    <div class="form-group floating-label">
                                        <input type="text" name="patronymic" class="form-control" id="regular2" value="{{ $user->patronymic }}">
                                        <label for="regular2">Отчество</label>
                                    </div>
                                    <div class="form-group floating-label">
                                        <input type="text" name="last_name"  class="form-control" id="regular3" value="{{ $user->last_name}}" {{--data-rule-minlength="2" aria-required="true"--}}>
                                        <label for="regular3">Фамилия</label>
                                    </div>
                                    <div class="form-group floating-label">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <div class="checkbox checkbox-inline checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" name="getDistribution" checked><span></span>
                                                        Подписаться на рассылку
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group floating-label has-error" id="has-error" style="display:none;"></div>
                                    <div class="card-actionbar">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary ink-reaction">Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <form class="form floating-label" id="form-changeemailphone" role="form" novalidate="novalidate" {{--onsubmit="sendDataToChange('/user/ajax/changeEmailPhone', this); return false;"--}}>
                            <div class="card">
                                <div class="card-head style-primary">
                                    <header>Регистрационная информация</header>
                                </div>
                                <div class="card-body">
                                    <p>e-mail при регистрации: <span>{{ $user->email }}</span></p>
                                    <div class="form-group floating-label">
                                        <input type="email" name="email" class="form-control" id="email">
                                        <label for="email">Новый e-mail</label>
                                    </div>
                                    <p>Телефон при регистрации: <span>{{ $user->phone }}</span></p>
                                    <div class="form-group floating-label">
                                        <input type="tel" name="phone" class="form-control" id="phone">
                                        <label for="phone">Новый телефон</label>
                                    </div>
                                    <div class="form-group floating-label has-error" id="has-error" style="display:none;"></div>
                                    <div class="card-actionbar">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary ink-reaction"><span>Сохранить и </span>выслать потверждение</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <form class="form floating-label" id="form-changepassword" role="form" novalidate="novalidate" {{--onsubmit="sendDataToChange('/user/ajax/changePassword/', this); return false;"--}}>
                            <div class="card">
                                <div class="card-head style-primary">
                                    <header>Смена пароля</header>
                                </div>
                                <div class="card-body">
                                    <div class="form-group floating-label">
                                        <input type="password" name="oldPassword" class="form-control" id="oldPassword">
                                        <label for="Password1">Старый пароль</label>
                                    </div>
                                    <div class="form-group floating-label">
                                        <input type="password" name="newPassword" class="form-control" id="newPassword">
                                        <label for="Password2">Новый пароль</label>
                                    </div>
                                    <div class="form-group floating-label has-error" id="has-error" style="display:none;"></div>
                                    <div class="card-actionbar">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary ink-reaction">Сохранить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

@stop

@section('modals')
    <div id="menubar" class="menubar-inverse">
        <div class="menubar-fixed-panel">
            <div>
                <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
            <div class="expanded">
                <a href="#">
                    <span class="text-lg text-bold text-primary ">Личный&nbsp;кабинет</span>
                </a>
            </div>
        </div>
        <div class="menubar-scroll-panel">
            <ul id="main-menu" class="gui-controls">
                <li class="gui-folder">
                    <a>
                        <div class="gui-icon"><i class="md md-work"></i></div>
                        <span class="title">Компании</span>
                    </a>
                    <ul>
                        <li><a href="#"><span class="title">Профиль</span></a></li>
                        <li><a href="#"><span class="title">Баланс</span></a></li>
                        <li><a href="#"><span class="title">Помощь</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>


@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#form-changename').validate({
                rules: {
                    "name": {
                        required: true,
                        minlength: 2
                    },
                    "patronymic": {
                        minlength: 2
                    },
                    "last_name": {
                        minlength: 2
                    }
                },
                messages: {
                    "name": {
                        required: "введите ваше имя",
                        minlength: "Минимальная длинна пароля 2 символов"
                    },
                    "patronymic": {
                        minlength:  "Минимальная длина отчества 2 символа"
                    },
                    "last_name": {
                        minlength:  "Минимальная длина фамилии 2 символа"
                    }
                },
                submitHandler: function() {
                    var formId = "#"+this.currentForm.id;
                    var errorBlockId = formId+' #has-error';
                    $.ajax({
                        url: "/user/ajax/changeName",
                        dataType: "json",
                        method: "POST",
                        data: $(formId).serialize(),

                        success: function (result, status, xhr) {
                            if(result.result) {
                                $(errorBlockId).html('<span class="help-block" style="color: #0aa89e;">Данные успешно изменены.<br><br></span>');
                            } else {
                                $(errorBlockId).html('<span class="help-block">Возникли проблемы при сохранении. Попробуйте еще раз.<br><br></span>');
                            }
                            $(errorBlockId).show('slow');
                        },
                        error: function (result) {
                            $('#login-error').html('<span class="help-block">Непредвиденная ошибка<br><br></span>');
                            $('#login-error').show('slow');
                        }
                    });
                }
            });
            $('#form-changepassword').validate({
                rules: {
                    "oldPassword": {
                        required: true,
                        minlength: 6
                    },
                    "newPassword": {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    "oldPassword": {
                        required: "введите ваш текущий пароль",
                        minlength: "минимальная длинна пароля 6 символовов"
                    },
                    "newPassword": {
                        required: "введите новый пароль",
                        minlength: "минимальная длинна пароля 6 символовов"
                    }
                },
                submitHandler: function() {
                    var formId = "#"+this.currentForm.id;
                    var errorBlockId = formId+' #has-error';
                    $.ajax({
                        url: "/user/ajax/changePassword",
                        dataType: "json",
                        method: "POST",
                        data: $(formId).serialize(),

                        success: function (result, status, xhr) {
                            console.log(status);
                            console.log(xhr);
                            if(result.result) {
                                window.location.href = "/auth/login";
                            } else {
                                $(errorBlockId).html('<span class="help-block">Пользователь с таким паролем не найден.<br><br></span>');
                            }
                            $(errorBlockId).show('slow');
                        },
                        error: function (result) {
                            $('#login-error').html('<span class="help-block">Непредвиденная ошибка<br><br></span>');
                            $('#login-error').show('slow');
                        }
                    });
                }
            });

            $('#form-changeemailphone').validate({
                rules: {
                    "email": {
                        email: true
                    },
                    "phone": {
                        minlength: 6
                    }
                },
                messages: {
                    "email": {
                        email: "введите корректный email"
                    },
                    "phone": {
                        minlength: "минимальная длина 6"
                    }
                },
                submitHandler: function() {
                    var formId = "#"+this.currentForm.id;
                    var errorBlockId = formId +' #has-error';
                    var serializedData = $(formId + " input").map(function () {
                        return $(this).val().trim() == "" ? null : this;
                    }).serialize();

                    if ( $.trim(serializedData) == "" ) {
                        $(errorBlockId).html('<span class="help-block">Нет данных для сохранения.<br><br></span>').show('slow');
                        return;
                    }

                    $.ajax({
                        url: "/user/ajax/changeEmailPhone",
                        dataType: "json",
                        method: "POST",
                        data: serializedData,//$(formId+" :input[value!='']").serialize(),

                        success: function (result, status, xhr) {
                            if(result.result) {
                                $(errorBlockId).html('<span class="help-block" style="color: #0aa89e;">Данные успешно изменены.<br><br></span>');
                            } else {
                                $(errorBlockId).html('<span class="help-block">Возникли проблемы при сохранении. Попробуйте еще раз.<br><br></span>');
                            }
                            $(errorBlockId).show('slow');
                        },
                        error: function (result) {
                            $(errorBlockId).html('<span class="help-block">Пользователь с таким email уже есть.<br><br></span>');
                        }
                    });
                }
            });
        });


    </script>
@stop

