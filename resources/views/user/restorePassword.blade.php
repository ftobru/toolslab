@extends('layouts.master_simple')


@section('content')

        <section>
            <div class="section-body contain-lg">
                <div class="col-md-6 col-md-offset-3 col-sm-7 col-sm-offset-2">
                    <form class="form floating-label" id="form-checkemail" action="dashboard.html" accept-charset="utf-8" method="post">
                        <div class="card">
                            <div class="card-head style-primary">
                                <header>Восстановление пароля</header>
                            </div>
                            <div class="card-body">
                                <p>Для восстановления пароля вы должны указать свой e-mail.</p>
                                <p>Вам будет отправлено письмо с ключом активации для изменения вашего пароля.</p>

                                <div class="form-group floating-label">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-envelope fa-lg"></span></span>
                                        <div class="input-group-content">
                                            <input type="email" class="form-control" id="email" name="email" value="">
                                            <label for="Email1">E-mail</label>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group floating-label has-error" id="has-error" style="display:none;"></div>
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-primary btn-raised" type="submit">Отправить</button>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </div>
                        </div>
                    </form>
                    <form class="form floating-label form-validate" id="form-confirm" action="dashboard.html" accept-charset="utf-8" method="post" style="display:none;">
                        <div class="card">
                            <div class="card-head style-primary">
                                <header>Восстановление пароля</header>
                            </div>
                            <div class="card-body">
                                <p>На Вашу почту отправлено письмо с инструкцией восстановления пароля вашего кабинета.</p>
                                <p>Если письмо не пришло к Вам проверьте ветку "спам" в Вашей почтовом ящике.</p>
                                <br/>
                                <div class="row">
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-primary btn-raised" type="submit" onclick="window.location.href='/auth/login/'; return false;">ОК</button>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#form-checkemail').validate({
            rules: {
                "email": {
                    required: true,
                    email: true,
                    minlength: 8
                }
            },
            messages: {
                "email": {
                    required: "Введите email",
                    minlength: "Минимальная длинна 8 символов",
                    email: "Введите корректный email"
                }
            },
            submitHandler: function() {
                var formId = "#"+this.currentForm.id;
                var errorBlockId = formId+' #has-error';
                $.ajax({
                    url: "{{URL::route('ajax.user.checkUserEmail')}}",
                    dataType: "json",
                    method: "POST",
                    data: $(formId).serialize(),

                    success: function (data, status, xhr) {
                        if(data.result) {
                            // показываем блок что на email ушла ссылка
                            $(formId).hide();
                            $("#form-confirm").fadeTo(1000, 1);
                        } else {
                            $(errorBlockId).html('<span class="help-block">Пользователь с таким email не найден<br><br></span>').show("slow");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
    });


</script>
@stop

