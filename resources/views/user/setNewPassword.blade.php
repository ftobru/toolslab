@extends('layouts.master_simple')


@section('content')

    <section>
        <div class="section-body contain-lg">

            <div class="text-center">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal1">Восстановление пароля</button>
            </div>

            <div class="modal fade" id="modal1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header text-center">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title">Восстановление пароля</h3>
                        </div>
                        <div class="card-body">
                            <form class="form floating-label" id="form-newpassword" role="form" novalidate="novalidate">
                                <div class="form-group floating-label">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock fa-lg"></span></span>
                                        <div class="input-group-content">
                                            <input type="password" class="form-control" id="password1" name="password1" required="" data-rule-minlength="5" aria-required="true">
                                            <label for="password1">Пароль</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group floating-label">
                                    <div class="input-group">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-lock fa-lg"></span></span>
                                        <div class="input-group-content">
                                            <input type="password" class="form-control" id="password2" name="password2" required="" data-rule-minlength="5" aria-required="true">
                                            <label for="password1">Подтвердите пароль</label>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="remember_token" value="{{ $remember_token }}">

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger">Сохранить</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@stop

@section('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-\s]+$/i.test(value);
        }, "Пароль должен содержать только латинские буквы и цифры");

        $('#form-newpassword').validate({
            rules: {
                "password1": {
                    required: true,
                    minlength: 6,
                    loginRegex: true
                },
                "password2": {
                    required: true,
                    minlength: 6,
                    equalTo: "#password1"
                }
            },
            messages: {
                "password1": {
                    required: "Введите пароль",
                    minlength: "Минимальная длинна 6 символов",
                    loginRegex: "Пароль должен содержать только латинские буквы и цифры"
                },
                "password2": {
                    required: "Повторите пароль",
                    minlength: "Минимальная длинна 6 символов",
                    equalTo: "Пароли не совпадают"
                }
            },
            submitHandler: function() {
                var formId = "#"+this.currentForm.id;
                var errorBlockId = formId+' #has-error';
                $.ajax({
                    url: "/user/ajax/newPassword",
                    dataType: "json",
                    method: "POST",
                    data: $(formId).serialize(),

                    success: function (data, status, xhr) {
                        if(data.result) {
                            window.location.href = "/auth/login/";
                        } else {
                            $(errorBlockId).html('<span class="help-block">При смене пароля возникла ошибка<br><br></span>').show("slow");
                        }
                    },
                    error: function (result) {
                    }
                });
            }
        });
    });

</script>
@stop