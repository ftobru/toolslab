@extends('layouts.menubar_general')


@section('content')
    <div class="tab-content dashboard">
        <h1 class="title text-center">{{$currentCompany->display_name}}</h1>

        <section class="tab-pane active" id="first9">
            <div class="section-body">
                <div class="card-head ">
                    <ul class="nav nav-tabs tabs-center" data-toggle="tabs">
                        <li class="active"><a href="#first22">Сегодня</a></li>
                        <li><a href="#second22">Текущая неделя</a></li>
                        <li><a href="#third22">Текущий месяц</a></li>
                    </ul>
                </div>
                <div class="card-body tab-content">
                    <div class="tab-pane active" id="first22">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card height-10">
                                    <div class="card-head">
                                        <header>Новых сделок</header>
                                        <span class="sum text-primary">000</span>
                                    </div>
                                    <div class="card-body">
                                        <span>Первичный контакт: <strong>1</strong></span>
                                        <span class="pull-right text-success text-sm">0,18% <i
                                                    class="md md-trending-up"></i></span>

                                        <div class="progress progress-hairline">
                                            <div class="progress-bar progress-bar-primary-dark" style="width:43%"></div>
                                        </div>
                                        <span>Переговоры: <strong>0</strong></span>
                                        <span class="pull-right text-success text-sm">0,68% <i
                                                    class="md md-trending-up"></i></span>

                                        <div class="progress progress-hairline">
                                            <div class="progress-bar progress-bar-primary-dark" style="width:11%"></div>
                                        </div>
                                        <span>Принимают решение: <strong>12</strong></span>
                                        <span class="pull-right text-danger text-sm">21,08% <i
                                                    class="md md-trending-down"></i></span>

                                        <div class="progress progress-hairline">
                                            <div class="progress-bar progress-bar-danger" style="width:93%"></div>
                                        </div>
                                        <span>Согласование договора <strong>0</strong></span>
                                        <span class="pull-right text-success text-sm">0,18% <i
                                                    class="md md-trending-up"></i></span>

                                        <div class="progress progress-hairline">
                                            <div class="progress-bar progress-bar-primary-dark" style="width:63%"></div>
                                        </div>
                                    </div>
                                    <!--end .card-body -->
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card height-05">
                                            <div class="card-head">
                                                <header>Успешных сделок</header>
                                                <span class="sum text-warning">000</span>
                                            </div>
                                            <div class="card-body">
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-success"
                                                         style="width:63%"></div>
                                                </div>
                                            </div>
                                            <!--end .card-body -->
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card height-05">
                                            <div class="card-head">
                                                <header>Сделок без задач</header>
                                                <span class="sum text-success">000</span>
                                            </div>
                                            <div class="card-body">
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-warning"
                                                         style="width:63%"></div>
                                                </div>
                                            </div>
                                            <!--end .card-body -->
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="card">
                                            <div class="row">
                                                <div class="col-md-6 height-05">
                                                    <div class="card-head">
                                                        <header>Задач в работе</header>
                                                        <span class="sum sum1 text-success">000</span>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="progress progress-hairline">
                                                            <div class="progress-bar progress-bar-warning"
                                                                 style="width:43%"></div>
                                                        </div>
                                                    </div>
                                                    <!--end .card-body -->
                                                </div>
                                                <div class="col-md-6 height-05">
                                                    <div class="card-head">
                                                        <header>Выполненных</header>
                                                        <span class="sum sum1 text-success">000</span>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="progress progress-hairline">
                                                            <div class="progress-bar progress-bar-warning"
                                                                 style="width:43%"></div>
                                                        </div>
                                                    </div>
                                                    <!--end .card-body -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card height-05">
                                            <div class="card-head">
                                                <header>Примечаний</header>
                                                <span class="sum sum1 text-success">000</span>
                                            </div>
                                            <div class="card-body">
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-warning"
                                                         style="width:63%"></div>
                                                </div>
                                            </div>
                                            <!--end .card-body -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card height-05">
                                    <div class="card-head">
                                        <header>Цель</header>
                                        <span class="sum sum1 text-success">$000</span>
                                    </div>
                                    <div class="card-body">
                                        <span>Из $29,000,000: </span>
                                        <span class="pull-right text-danger text-sm">21,08% <i
                                                    class="md md-trending-down"></i></span>

                                        <div class="progress progress-hairline">
                                            <div class="progress-bar progress-bar-danger" style="width:93%"></div>
                                        </div>
                                    </div>
                                    <!--end .card-body -->
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="card height-05">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="card-head">
                                                        <header>Задач в работе</header>
                                                        <span class="sum sum1 text-success">000</span>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="progress progress-hairline">
                                                            <div class="progress-bar progress-bar-warning"
                                                                 style="width:43%"></div>
                                                        </div>
                                                    </div>
                                                    <!--end .card-body -->
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="card-head">
                                                        <header>Выполненных</header>
                                                        <span class="sum sum1 text-success">000</span>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="progress progress-hairline">
                                                            <div class="progress-bar progress-bar-warning"
                                                                 style="width:43%"></div>
                                                        </div>
                                                    </div>
                                                    <!--end .card-body -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card height-05">
                                            <div class="card-head">
                                                <header>Примечаний</header>
                                                <span class="sum sum1 text-success">000</span>
                                            </div>
                                            <div class="card-body">
                                                <div class="progress progress-hairline">
                                                    <div class="progress-bar progress-bar-warning"
                                                         style="width:63%"></div>
                                                </div>
                                            </div>
                                            <!--end .card-body -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="statuses-slider">
                                    <div>
                                        <div class="dashboard-statuses">
                                            <a href="#">Первичный контакт</a>

                                            <div data-count="0" data-budget="0">0 сделок: 0 руб</div>
                                        </div>
                                        <ul class="list list-editable ui-sortable" data-sortable="true">
                                            <span id="add-status-field"
                                                  class="add-tags">Быстрое добавление сделки</span>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div>
                                        <div class="dashboard-statuses">
                                            <a href="#">Переговоры</a>

                                            <div data-count="0" data-budget="0">0 сделок: 0 руб</div>
                                        </div>
                                        <ul class="list list-editable ui-sortable" data-sortable="true">
                                            <span id="add-status-field"
                                                  class="add-tags">Быстрое добавление сделки</span>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div>
                                        <div class="dashboard-statuses">
                                            <a href="#">Принимают решение</a>

                                            <div data-count="0" data-budget="0">0 сделок: 0 руб</div>
                                        </div>
                                        <ul class="list list-editable ui-sortable" data-sortable="true">
                                            <span id="add-status-field"
                                                  class="add-tags">Быстрое добавление сделки</span>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div>
                                        <div>
                                            <a href="#">Первичный контакт</a>

                                            <div data-count="0" data-budget="0">0 сделок: 0 руб</div>
                                        </div>
                                        <ul class="list list-editable ui-sortable" data-sortable="true">
                                            <span id="add-status-field"
                                                  class="add-tags">Быстрое добавление сделки</span>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                            <li class="ui-sortable-handle">
                                                <div class="checkbox checkbox-styled tile-text">
                                                    <i class="md md-more-vert"></i>
                                                    Отказ (холодный обзвон)
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h2>Контакты</h2>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Наименование</th>
                                        <th>Компания</th>
                                        <th>Телефон</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><a href="#">my company</a></td>
                                        <td></td>
                                        <td><a href="#">2223322</a></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><a href="#">her comp</a></td>
                                        <td></td>
                                        <td><a href="#">23322876644</a></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane" id="second22"><p>Ad ius duis dissentiunt, an sit harum primis persecuti,
                            adipisci tacimates mediocrem sit et. Id illud voluptaria omittantur qui, te affert nostro mel.
                            Cu conceptam vituperata temporibus has.</p>
                    </div>
                    <div class="tab-pane" id="third22"><p>Duo semper accumsan ea, quidam convenire cum cu, oportere
                            maiestatis incorrupte est eu. Soluta audiam timeam ius te, idque gubergren forensibus ad mel,
                            persius urbanitas usu id. Civibus nostrum fabellas mea te, ne pri lucilius iudicabit. Ut cibo
                            semper vituperatoribus vix, cum in error elitr. Vix molestiae intellegat omittantur an, nam cu
                            modo ullum scriptorem.</p>

                        <p>Quod option numquam vel in, et fuisset delicatissimi duo, qui ut animal noluisse erroribus.
                            Ea eum veniam audire. Per at postea mediocritatem, vim numquam aliquid eu, in nam sale
                            gubergren. Dicant vituperata consequuntur at sea, mazim commodo</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="tab-pane" id="second9">
            <div class="section-body">
                2222
            </div>
        </section>


    </div>
    <!--end content-->
@stop
