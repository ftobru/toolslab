@extends('layouts.landing-page')

@section('content')
<div class="iphone-detail padding-lg first-screen relative">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h1>Переменные</h1><span></span>

                <p class="light-grey">Есть значения, которые со временем могут меняться, например: телефон или количество выполненных заказов. Переверстывать все лендинги сложно и обычно просто лень.</p>
                <p class="light-grey">Измените переменную в одном месте в админ панели и она изменится везде.</p>
            </div>
            <div class="col-md-8 hidden-xs hidden-sm image-section relative text-center hover-scale">
                <img src="demo/images/lend_01.png" class="landings wow fadeIn" data-wow-delay="0.1s" alt="Переменные">
                <img src="demo/images/lend_02.png" class="landings wow fadeIn" data-wow-delay="0.3s" alt="Переменные">
                <img src="demo/images/lend_03.png" class="landings wow fadeIn" data-wow-delay="0.5s" alt="Переменные">
            </div>
        </div>
    </div>
    <canvas></canvas>
</div>
<div class="white-bg padding-md">
    <div class="container scrollme">
        <div class="row">
            <div class="col-md-7 image-section relative text-center">
                <div class="row replace1">
                    <div class="landings descr card">
                        <img src="demo/images/kart_01.png" alt="card">
                        <div class="arrow">
                            <i class="md md-keyboard-arrow-down"></i>
                        </div>
                    </div>
                    <div class="landings descr card">
                        <img src="demo/images/kart_02.png" alt="card">
                        <div class="arrow">
                            <i class="md md-keyboard-arrow-down"></i>
                        </div>
                    </div>
                    <div class="landings descr card hidden-md hidden-md">
                        <img src="demo/images/kart_03.png" alt="card" class="">
                        <div class="arrow">
                            <i class="md md-keyboard-arrow-down"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="landings descr card">
                        <img src="demo/images/l2.png" alt="landing">
                    </div>
                    <div class="landings descr card">
                        <img src="demo/images/l1.png" alt="landing">
                    </div>
                    <div class="landings descr card hidden-md hidden-md">
                        <img src="demo/images/l3.png" alt="landing">
                    </div>
                </div>
            </div>


            <div class="col-md-5 about-iphone">
                <div class="left20">
                    <h1>Товар-замена</h1><span></span>

                    <p class="light-grey">Заполните карточки товаров<span class="green-txt time"><i class="md md-access-time"></i>1мин/карточка </span>   и Ваша страница превращается в мультилендинг.</p>
                    <p class="light-grey">Теперь при заходе на сайт определенные данные будут на лету изменены на информацию о товаре.</p>

                    <ul class="list-unstyled blocknote">
                        <li class="light-grey name-list">Что это дает:</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Из 1 страницы делает множество, но скрытых от поиска.</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Лендинги становятся узко заточенными под товар.</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Конверсия более узких лендингов заметно выше.</li>
                    </ul>
                    <ul class="list-unstyled blocknote">
                        <li class="light-grey name-list">Когда это обязательно:</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Когда у Вас несколько товарных групп или товаров</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Когда хочется протестировать товар <sup data-tooltip="Например, создать 2 одинаковых товара с разной ценой" class="green-txt helper"><i class="md md-help"></i></sup></li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Когда много модификаций у товара <sup data-tooltip="Например, iphone 32/64/128, white/black" class="green-txt helper"><i class="md md-help"></i></sup></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="iphone-detail padding-md replace relative" id="features">
    <div class="container">
        <div class="row features">
            <div class="col-md-5 about-iphone">
                <h1>Динамические замены</h1><span></span>

                <p>На лету меняйте любые области сайта под определенные потоки трафика, делая при этом ваши страницы максимально релевантными </p>
                <ul class="list-unstyled blocknote">
                    <li class="light-grey name-list">Что это дает:</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Ваш сайт максимально релевантен <sup data-tooltip="Что искали в поисковике - то и увидели на сайте" class="green-txt helper"><i class="md md-help"></i></sup> поисковому запросу</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Ваш сайт говорит с пользователем на одном языке</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Это очень сильно повышает конверсию <sup data-tooltip="при грамотной настройке рекламы" class="green-txt helper"><i class="md md-help"></i></sup></li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Возможность тестировать разные варианты заголовков, текстов или картинок.</li>
                </ul>
            </div>
            <div class="col-md-7 hidden-xs image-section relative wow zoomIn">
                <div id="tabs">
                    <ul class="pull-left yandex-tabs">
                        <li>
                            <a id="tab13">
                                <span>Очки для водителя<b class="opacity8">|</b></span>
                            </a>
                        </li>
                        <li>
                            <a id="tab14" class="inactive">
                                <span>Очки против ксенона<b class="opacity8">|</b></span>
                            </a>
                        </li>
                        <li>
                            <a id="tab15" class="inactive">
                                <span>Антифары очки<b class="opacity8">|</b></span>
                            </a>
                        </li>
                        <li>
                            <a id="tab16" class="inactive">
                                <span>Поляризационные очки<b class="opacity8">|</b></span>
                            </a>
                        </li>
                    </ul>
                    <div class="pull-right tab-content">
                        <div class="container" id="tab13C"><img src="demo/images/m1.png" alt="image"></div>
                        <div class="container" id="tab14C"><img src="demo/images/m2.png" alt="image"></div>
                        <div class="container" id="tab15C"><img src="demo/images/m3.png" alt="image"></div>
                        <div class="container" id="tab16C"><img src="demo/images/m4.png" alt="image"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <canvas class="second"></canvas>
</div>
<div class="padding-md replace white-bg">
    <div class="container">
        <div class="row tabs-ico">
            <div class="col-md-7 image-section relative wow zoomIn">
                <div class="row">
                    <div id="tabs1">
                        <ul class="pull-left simple-tabs padding50">
                            <li>
                                <a id="tab17">
                                    <img src="demo/images/dostavka-1.png" alt="">
                                    <span>Бесплатная<br/>доставка</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab18" class="inactive">
                                    <img src="demo/images/garantia-1.png" alt="">
                                    <span>Год гарантии</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab19" class="inactive">
                                    <img src="demo/images/dostavka-1.png" alt="">
                                    <span>Акция с таймером</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab20" class="inactive">
                                    <img src="demo/images/40_sek.png" alt="">
                                    <span>Ролевантность<br/>траффиков</span>
                                </a>
                            </li>
                        </ul>
                        <div class="pull-left tab-content">
                            <div class="container" id="tab17C"><img src="demo/images/insert1.png" alt="image"></div>
                            <div class="container" id="tab18C"><img src="demo/images/insert2.png" alt="image"></div>
                            <div class="container" id="tab19C"><img src="demo/images/insert4.png" alt="image"></div>
                            <div class="container" id="tab20C"><img src="demo/images/insert3.png" alt="image"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 about-iphone">
                <h1>Динамические вставки</h1><span></span>

                <p class="light-grey">Функция схожа с Динамическими заменами, только содержит в себе полноценные информационные блоки.</p>
                <ul class="list-unstyled blocknote">
                    <li class="light-grey name-list">Зачем это нужно:</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Создайте целую серию тригеров для Вашего предложения. Запустите и протестируйте, оставьте только самые эффективные.</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Усильте релевантность входящего трафика, упомянув источник трафика или его сенмент. <sup data-tooltip="Например, iphone 32/64/128, white/black" class="green-txt helper"><i class="md md-help"></i></sup></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="iphone-detail padding-md relative">
    <div class="container replace">
        <div class="row features tabs-ico citys-tabs">
            <div class="col-md-5 about-iphone">
                <h1> Гео-замены</h1><span></span>

                <p class="light-grey">Определяет город посетителя и на лету меняет абсолютно любые данные на более релевантные. Вы можете отредактировать изменяемую информацию по любому городу за 1 минуту. </p>
                <ul class="list-unstyled blocknote">
                    <li class="light-grey name-list">Что это дает:</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Определять город посетителя и вставлять на сайт название его города в одном из 3х падежей.</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> В зависимости от города менять телефоны, адреса или любой другой контент.</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Конечно же повышает конверсию и создает эффект присутствия.</li>
                </ul>
            </div>
            <div class="col-md-7 hidden-xs image-section relative wow zoomIn">
                <div class="row">
                    <div id="tabs2">
                        <ul class="pull-left simple-tabs padding50">
                            <li>
                                <a id="tab21">
                                    <img src="demo/images/Irkutsk.png" alt="">
                                    <span>Иркутск</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab22" class="inactive">
                                    <img src="demo/images/Novosibitsk.png" alt="">
                                    <span>Новосибирск</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab23" class="inactive">
                                    <img src="demo/images/SaintPetersburg.png" alt="">
                                    <span>Санкт-Петербург</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab24" class="inactive">
                                    <img src="demo/images/Khabarovsk.png" alt="">
                                    <span>Хабаровск</span>
                                </a>
                            </li>
                        </ul>
                        <div class="pull-left tab-content">
                            <div class="container" id="tab21C"><img src="demo/images/geo01.png" alt="image"></div>
                            <div class="container" id="tab22C"><img src="demo/images/geo02.png" alt="image"></div>
                            <div class="container" id="tab23C"><img src="demo/images/geo03.png" alt="image"></div>
                            <div class="container" id="tab24C"><img src="demo/images/geo04.png" alt="image"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <canvas class="third"></canvas>
</div>
<div class="padding-md replace white-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-7 image-section relative wow zoomIn">
                <div class="row tabs-ico">
                    <div id="tabs3">
                        <ul class="pull-left simple-tabs padding50">
                            <li>
                                <a id="tab25">
                                    <img src="demo/images/time_morning.png" alt="">
                                    <span>Рабочее время</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab26" class="inactive">
                                    <img src="demo/images/time_night.png" alt="">
                                    <span>Нерабочее время</span>
                                </a>
                            </li>
                            <li>
                                <a id="tab27" class="inactive">
                                    <img src="demo/images/time_day.png" alt="">
                                    <span>Счастливый час</span>
                                </a>
                            </li>
                        </ul>
                        <div class="pull-left tab-content">
                            <div class="container" id="tab25C"><img src="demo/images/time_lend3.png" alt="image"></div>
                            <div class="container" id="tab26C"><img src="demo/images/time_lend1.png" alt="image"></div>
                            <div class="container" id="tab27C"><img src="demo/images/time_lend2.png" alt="image"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 about-iphone">
                <h1>Тайм-замена</h1><span></span>

                <p class="light-grey">Не упускайте трафик во внерабочее время и не обещайте лишнего. Будьте интерактивны с пользователем. Просто настройте временные вставки.</p>
                <ul class="list-unstyled blocknote">
                    <li class="light-grey name-list">Что это дает:</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Более конверсионно обрабатывать ночной трафик.</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Не давать ложных обещаний, например: “перезвонить через 10 минут” в 2 ночи.</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Уведомлять о часах работы.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="padding-md white-bg buttons relative">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 text-center">
                <p class="txt-bg green-txt">Восторг! Хочу скорее повысить конверсию своего лендинга!</p>
                <button type="button" class="btn btn-2 btn-2a" data-toggle="modal" data-target="#modal1">Попробовать бесплатно 14 дней!</button>
            </div>
            <div class="col-sm-6 text-center">
                <p class="txt-bg light-grey">- Окей, с мультилендингами вроде все понятно, есть что-нибудь еще жестче?</p>
                <p class="txt-bg light-grey">- О да, впереди <a href="/traffic" class="green-txt underline">Распределение трафика</a></p>
            </div>
        </div>
    </div>
    <canvas class="fourth"></canvas>
</div>
@endsection