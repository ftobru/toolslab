@extends('layouts.landing-page')

@section('content')
    <div class="iphone-detail padding-lg first-screen">
        <div class="container">
            <div class="row">
                <div class="bord text-center">
                    <h1>ModulCRM</h1><span></span>

                    <p>удобная система управления Лендингами, которая позволяет сильно увеличить конверсию за счет
                        динамического контента и адаптивного распределения трафика. + CRM + панель аналитики.</p>

                    <p class="philosophy">За счет чего достигается увеличение.<br/>Наша философия</p>
                    <img src="demo/images/roket_01.png" alt="ModulCRM" class="rocket_sm">
                    <img src="demo/images/person_02.png" alt="modulCRM" class="teacher">
                    <img src="demo/images/person_01.png" alt="ask" class="ask">
                </div>
            </div>
        </div>
        <canvas></canvas>
    </div>
    <div class="container padding-lg">
        <div class="row">
            <h1>Создание лендингов</h1>

            <h2 class="note green-txt">существует 3 стратегии создания лендинга:</h2>

            <div class="col-md-4 create_site">
                <div class="way_wrp">
                    <h2 class="green-txt text-center">Воспользоваться онлайн-конструктором</h2>

                    <p class="light-grey">Быстрое решение, сайт за 30 минут, от 1000 - 1900р в месяц.</p>

                    <div class="text-center rocket relative">
                        <img src="demo/images/rocket_01!.png" alt="rocket">

                        <div id="rocket"></div>
                    </div>
                    <ul class="plus toggle_nav">
                        <li>
                            <a href="#" class="title"><span><img src="demo/images/+.png" alt="Плюсы"></span>Хорошая
                                админка <span class="green-txt"><i class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li><span class="green-txt">+ </span>Дизайн, наполнение и правки своими руками</li>
                                <li><span class="green-txt">+ </span>Админ панель: управляйте сайтами, доменами,
                                    хостингом
                                </li>
                                <li><span class="green-txt">+ </span>Мини-CRM и формы захвата из коробки</li>
                                <li><span class="green-txt">+ </span>Статистика, аналитика, аб-тесты, воронки</li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="plus toggle_nav">
                        <li>
                            <a href="#" class="title"><span><img src="demo/images/-.png" alt="Минусы"></span> Ужасные
                                сайты <span class="green-txt"><i class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li><span class="green-txt">&minus; </span> Как правило плохая, неадаптивная<span
                                            class="red-txt">(!)</span> верстка
                                </li>
                                <li><span class="green-txt">&minus; </span> Ограниченный функционал, нет интерактива
                                    (калькулятор)
                                </li>
                                <li><span class="green-txt">&minus; </span> Нельзя сделать что-то уникальное, даже
                                    анимацию
                                </li>
                                <li><span class="green-txt">&minus; </span> Как правило, свой дизайн выглядит green</li>
                                <li><span class="green-txt">&minus; </span> Привязка к конструктору, нельзя забрать свой
                                    сайт
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 create_site">
                <div class="way_wrp">
                    <h2 class="green-txt text-center">Сверстать с нуля или на HTML-шаблоне</h2>

                    <p>Быстрое решение, сайт 2 дня, шаблон+верстка 3-10т/р</p>

                    <p>Долгое решение, сайт 2-4 недели, свой дизайн 20+т/р</p>

                    <div class="text-center rocket1 relative">
                        <img src="demo/images/rocket_02!.png" alt="rocket">

                        <div id="rocket1"></div>
                    </div>
                    <ul class="plus toggle_nav">
                        <li>
                            <a href="#" class="title"><span><img src="demo/images/+.png"
                                                                 alt="Плюсы"> Отличные сайты<span class="green-txt"><i
                                                class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li><span class="green-txt">+ </span>Полная свобода действий, уникальность</li>
                                <li><span class="green-txt">+ </span>Любой интерактив, любые скрипты</li>
                                <li><span class="green-txt">+ </span>Хорошая, валидная, адаптивная<span class="red-txt">(!)</span>
                                    верстка
                                </li>
                                <li><span class="green-txt">+ </span>Быстрое и качественное решение на шаблоне</li>
                                <li><span class="green-txt">+ </span>Это ваш сайт, размещайте на любом хостинге</li>
                                <li><span class="green-txt">+ </span>Итоговый результат явно лучше, чем на конструкторе
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="minus toggle_nav">
                        <li>
                            <a href="#" class="title"><span><img src="demo/images/-.png" alt="Минусы"> Нет админки<span
                                            class="green-txt"><i class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li><span class="green-txt">&minus; </span>Сложно управлять сайтами на хостинге</li>
                                <li><span class="green-txt">&minus; </span>Форма захватов - самая проблемная зона</li>
                                <li><span class="green-txt">&minus; </span>Лиды отсылаются на почту, а не CRM, нужна
                                    настрйока
                                </li>
                                <li><span class="green-txt">&minus; </span>Сложно собирать и обрабатывать статистику
                                </li>
                                <li><span class="green-txt">&minus; </span>Дольше+требует немного технического опыта
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 create_site">
                <div class="way_wrp create_main">
                    <h2 class="red-txt text-center">Гибридный способ на ModulCRM</h2>

                    <p class="light-grey">Взял все самое лучшее от этих двух способов и даже больше!</p>

                    <p class="light-grey">Сайт верстается с нуля, либо на шаблоне, так же, как и во втором способе.
                        Просто загрузите HTMLверстку любой сложности, получите все преимущества настоящих HTML
                        сайтов.</p>

                    <div class="text-center rocket2 rocket3 relative">
                        <img src="demo/images/rocket_02!.png" alt="rocket">

                        <div id="rocket_1"></div>
                    </div>
                    <ul class="toggle_nav plus">
                        <li>
                            <a href="#" class="title">Система представляет собой: <span class="green-txt"><i
                                            class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li>Хостинг + админ-панель, которая позволяет легко управлять страницами, доменами и
                                    тд
                                </li>
                                <li>За 5 минут позволит создать качественные формы захвата.</li>
                                <li>На борту имеется полноценная(!) CRM система, (с задачами, сделками, историей
                                    взаимодействия, аналитикой, правами доступа, доп полями.)
                                </li>
                                <li>Собирает мощную статистику по 38 параметрам</li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="toggle_nav plus">
                        <li>
                            <a href="#" class="title">Дополнительный функционал: <span class="green-txt"><i
                                            class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li class="title">Динамический контент! Ваш сайт на лету меняется в зависимости от:</li>
                                <li>&mdash; Посадочного товара</li>
                                <li>&mdash; Вашего времени</li>
                                <li>&mdash; Города посетителя</li>
                                <li>&mdash; Источника перехода</li>
                                <li>&mdash; UTM метки в ссылке</li>
                                <li>&mdash; прочих параметров</li>
                                <li>&mdash; (Контент меняется в зависимости от 1го параметра или от всех сразу)</li>
                                <li>&mdash; (Аналитика по эффективности каждого из изменений)</li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="toggle_nav plus">
                        <li>
                            <a href="#" class="title">Адаптивно распределяет трафик на лету по разным страницам в
                                зависимости от: <span class="green-txt"><i class="md md-keyboard-arrow-down"></i></span></a>
                            <ul>
                                <li>&mdash; Времени пользователя</li>
                                <li>&mdash; Его города</li>
                                <li>&mdash; При Аб-тесте</li>
                                <li>&mdash; (Аналитика по каждому из направлений)</li>
                                <li>&mdash; (Удобный конструктор направлений и аб-тестов)</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="relative">
        <div class="container" id="features">
            <div class="row features">
                <div class="col-xs-12 heading">
                    <h1>Контент</h1>
                </div>
                <div id="tabs" class="col-xs-12 aside clearfix">
                    <ul class="nav-ul">
                        <li class="role active"><a href="#tabs-1"><span>Релевантность + подмена</span></a></li>
                        <li class="traffic1"><a href="#tabs-2"><span>Распределение трафика</span></a></li>
                        <li class="marketing"><a href="#tabs-3"><span>Маркетинг и предложение</span></a></li>
                        <li class="stat"><a href="#tabs-4"><span>Статистика</span></a></li>
                        <li class="tests"><a href="#tabs-5"><span>Тесты</span></a></li>
                        <li class="crm1"><a href="#tabs-6"><span>CRM</span></a></li>

                    </ul>
                    <figure id="tabs-1" class="col-xs-8 feature-detail">
                        <p>В первую очередь важно, чтобы человек, перешедший на Ваш сайт, не разочаровался и получил
                            информацию наиболее близкую к той, что ожидал увидеть. </p>

                        <p>Мы связываем рекламные объявления с определенными видами посадочных страниц, чтобы страница
                            дополняла объявление и лучше конвертировала посетителя.</p>

                        <p>В этом поможет ModulCRM, где заложен мощный функционал по динамической подмене контента.</p>

                        <p>Изменения зависят от множества факторов:</p>

                        <p><span class="green-txt">&mdash;</span> Посадочного товара</p>

                        <p><span class="green-txt">&mdash;</span> Вашего времени</p>

                        <p><span class="green-txt">&mdash;</span> Города посетителя</p>

                        <p><span class="green-txt">&mdash;</span> Источника перехода</p>

                        <p><span class="green-txt">&mdash;</span> UTM метки в ссылке</p>

                        <p><span class="green-txt">&mdash;</span> прочих параметров</p>

                        <p><span class="green-txt">&mdash;</span> (Контент меняется в зависимости от 1го параметра или
                            от всех сразу)</p>

                        <p><span class="green-txt">&mdash;</span> (Аналитика по эффективности каждого из изменений)</p>
                    </figure>

                    <figure id="tabs-2" class="col-xs-8 feature-detail">
                        <p>В разное время суток, пользователь по-разному себя ведет, и это факт. Видео хорошо продают
                            вечером, но плохо днем. Утром пользователи предпочитают более короткие сайты, а ночью
                            принимают решения неохотно.</p>

                        <p>Повышайте конверсию, показывая пользователям в разное время разные специально заточенные
                            страницы</p>

                        <p>Наверняка, для пользователей из разных городов Вы можете делать разные по мощности
                            предложения.

                            <span><span class="green-txt">&mdash;</span> Посадочного товара</span>

                        </p>

                        <p>Работайте по всей стране, но давайте более конверсионные офферы там, где можете!</p>

                    </figure>
                    <figure id="tabs-3" class="col-xs-8 feature-detail">
                        <p class="light-grey">Безусловно у Вас должен быть проработан маркетинг и иметься хорошее
                            предложение (оффер) для входящего трафика.</p>

                        <p class="light-grey">Мы можем помочь со справочным материалом, либо даже помочь с обучением, но
                            это должно исходить из компании: только собственник может разобрать свой бизнес по косточкам
                            и выжать из него только самое важное для его целевой аудитории..</p>
                    </figure>
                    <figure id="tabs-4" class="col-xs-8 feature-detail">
                        <p class="light-grey">ModulCRM снимает статистику в разрезе на 38 параметров и получайте
                            развернутые отчеты.</p>

                        <p><span class="green-txt">Шаблон:</span> Flat lnd promo</p>

                        <p><span class="green-txt">Товар:</span> Очки Razer 001</p>

                        <p><span class="green-txt">Замена:</span> желтые линзы очки</p>

                        <p><span class="green-txt">Вставка:</span> Год гарантии</p>

                        <p><span class="green-txt">Форма:</span> Бесплатная примерка</p>

                        <p><span class="green-txt">Город:</span> Воронеж</p>

                        <p><span class="green-txt">IP:</span> 80.69.182.97</p>

                        <p><span class="green-txt">ОС:</span> Windows</p>

                        <p><span class="green-txt">Браузер:</span> Хром</p>

                        <p><span class="green-txt">Пребывание:</span> 17 сек</p>

                        <p><span class="green-txt">Переход </span>с: <a href="#">link +</a></p>

                        <p><span class="green-txt">UTM:</span> <a href="#">yandex +</a></p>
                    </figure>
                    <figure id="tabs-5" class="col-xs-8 feature-detail">
                        <p class="light-grey">Можно создать 1 сайт и надеяться, что он “лучший”. </p>

                        <p class="or green-txt">или</p>

                        <p class="light-grey">Можно загрузить в ModulCRM несколько шаблонов, создать 4 заголовка, 4
                            разных товара(фото-цены-описание), 4 разных тригера, 2 акции и получить АБ-тест из 256
                            вариантов.</p>

                        <p class="light-grey">Это не занимает много времени, однако дает рост конверсии и уверенность в
                            ней.</p>

                        <p class="light-grey">Не нужно тратить много времени на поиск лучшей страницы, просто выпишите
                            все гипотезы и протестируйте их сразу. </p>
                    </figure>
                    <figure id="tabs-6" class="col-xs-8 feature-detail">
                        <p class="light-grey">Полноценная CRM, в которой будет удобно работать всей компании. Все лиды
                            моментально появляются в системе, работает из коробки, не требует участия программистов или
                            платных интеграций.</p>
                    </figure>
                </div>
            </div>
        </div>
        <canvas class="second"></canvas>
    </div>
@endsection