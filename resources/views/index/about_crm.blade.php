@extends('layouts.landing-page')

@section('content')
    <div class="iphone-detail padding-lg first-screen first-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 hidden-xs image-section relative descr-img wow zoomIn hover-scale main-img">
                    <img src="demo/images/CRM.png" alt="Лендинг" style="width: 85%">
                </div>
                <div class="col-sm-5">
                    <h1>Что такое CRM?</h1><span></span>

                    <p class="light-grey"><span class="green-txt">CRM</span> - это сердце Вашего отдела продаж. В этой
                        системе хранятся данные о ваших клиентах, все взаимодействия с ними: звонки, примечания, заказы,
                        задачи для менеджеров и тд.</p>

                    <p class="light-grey">Если работа с клиентами протекает через CRM, то можно гарантировать увеличение
                        второй конверсии из лидов в продажи.</p>
                </div>
            </div>
        </div>
        <canvas></canvas>
    </div>
    <div class="container padding-lg complitly">
        <h1>Полноценная CRM</h1>

        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/cont_card.png" alt="">

                    <h2 class="green-txt">Карточка контакта “все в 1 экране”</h2>

                    <p class="light-grey">Удобный интерфейс позволяет просматривать и создавать всю информацию о товаре
                        не переключаясь на другие экраны.<br/>Это очень удобно.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/base.png" alt="">

                    <h2 class="green-txt">База данных в функциональных таблицах</h2>

                    <p class="light-grey">Таблицы - самое важное при работе с клиентской базой. Редактируйте, копируйте
                        в эксель, дублируйте, печатайте прям в таблице. Мы сделали еще уникальные функции: группировка
                        по параметрам, множественная сортировка или фиксация колонок.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">

                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/deals.png" alt="">

                    <h2 class="green-txt">Сделки с корзиной товаров и аналитикой</h2>

                    <p class="light-grey">В сервис внедрена упрощенная база товаров с возможностью создавать заказы или
                        шаблоны корзин. На товарах построена мощная аналитическая система.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/task1.png" alt="">

                    <h2 class="green-txt">Задачи для сотрудников</h2>

                    <p class="light-grey">Ни один клиент не останется потерянным без следующего шага. Система генерирует
                        задачи сама, либо менеджеры ставят их сами.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/dop.png" alt="">

                    <h2 class="green-txt">Дополнительные поля</h2>

                    <p class="light-grey">Создавайте без ограничений любые дополнительные поля, которые Вам нужны для
                        работы.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/dost.png" alt="">

                    <h2 class="green-txt">Права доступа:</h2>

                    <p class="light-grey">Разграничивайте права доступа к разным данным среди ваших сотрудников.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/api1.png" alt="">

                    <h2 class="green-txt">API и интеграции</h2>

                    <p class="light-grey">Интегрируйте сервис с любыми другими программами через наш API или
                        воспользуйтесь уже готовыми коннекторами.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="contact-card text-center hover-scale">
                    <img src="demo/images/resp.png" alt="">

                    <h2 class="green-txt">Адаптивность</h2>

                    <p class="light-grey">Сервис изначально сделан адаптивным, поэтому он хорошо будет отображаться на
                        устройствах с любым размером экрана.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="iphone-detail padding-md scrollme">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 about-iphone">
                    <h1>Интеграция с модулем Лендингов:</h1><span></span>

                    <p class="light-grey name-list">При отправке формы на лендинге:</p>
                    <ul class="list-unstyled blocknote">
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Заведется
                            карточка контакта “1 касание”
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> У контакта
                            появится сделка с товаром, который был привязан к лендингу.
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Поставится
                            задача для ответственного менеджера для звонка.
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> К контакту и
                            сделке подвяжется статистика из 38 параметров.
                        </li>
                    </ul>
                    <p class="light-grey">Все работает из коробки, не требует участия программистов или платных
                        интеграций.</p>
                </div>
                <div class="col-sm-6 image-section relative" id="col3-1">
                    <img class="wheel" id="wheel1" src="demo/images/cogwheel_01.png" alt="Интеграция"/>
                    <img class="wheel1" id="wheel2" src="demo/images/cogwheel_02.png" alt="Интеграция"/>
                </div>
            </div>
        </div>
        <canvas class="second"></canvas>
    </div>
@endsection