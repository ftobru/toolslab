@extends('layouts.landing-page')

@section('content')
    <div class="iphone-detail padding-lg first-screen real_time">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 text-center descr-img hover-scale">
                    <img src="demo/images/mag.png" alt="Захват данных">
                </div>
                <div class="col-sm-6">
                    <h1>Данные в реальном времени:</h1><span></span>

                    <ul class="list-unstyled blocknote">
                        <li class="light-grey name-list">Захват данных:</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Наши серверы
                            фиксируют всех посетителей Ваших сайтов
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> На лету
                            обрабатывают данные и заносят данные в Базу.
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Вся статистика
                            передается в CRM систему
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <canvas></canvas>
    </div>

    <div class="container padding-lg analitycs1">
        <div class="row">
            <div class="col-xs-12">
                <h1>Система фиксирует и расшифровывает:</h1>
            </div>
        </div>
        <div class="row">
            <div id="tabs" class="analitycs-tabs">
                <div class="col-sm-7">
                    <ul>
                        <li><a id="tab1">Мультилендинг, который просматривал пользователь:</a></li>
                        <li><a id="tab2">Произвольные UTM-метки:</a></li>
                        <li><a id="tab3">UTM-Метки Яндекс.Директа</a></li>
                        <li><a id="tab4">UTM-Метки Google Adwords</a></li>
                        <li><a id="tab5">Страницу перехода <sup data-tooltip="C какой страницы перешел пользователь"
                                                                class="green-txt"><i class="md md-help"></i></sup></a>
                        </li>
                        <li><a id="tab6">Персональные данные пользователя:</a></li>
                    </ul>
                </div>
                <div class="col-sm-5 about-iphone">
                    <div class="container" id="tab1C">
                        <ul class="list-unstyled blocknote">
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Шаблон:
                                розница акция
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Товар:
                                iphone 32GB white
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Замена:
                                заголовок (скидка)
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Вставка:
                                гарантия 3 года
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Кнопка:
                                4(возникли вопросы)
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Форма: 2
                                поля, консультация
                            </li>
                        </ul>
                    </div>
                    <div class="container" id="tab2C">
                        <ul class="list-unstyled blocknote">
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> UTM_source
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> UTM_medium
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> UTM_campain
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> UTM_term
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> UTM_content
                            </li>
                        </ul>
                    </div>
                    <div class="container" id="tab3C">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="green-txt">
                                <tr>
                                    <th>Параметр</th>
                                    <th>Описание</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="green-txt">{source_type}</td>
                                    <td>тип площадки: поиск или контекст</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{source}</td>
                                    <td>площадка, только для РСЯ</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{position_type}</td>
                                    <td>спецразмещение или гарантия</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{position}</td>
                                    <td>номер позиции объявления в блоке</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{keyword}</td>
                                    <td>ключевое слово</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{addphrases}</td>
                                    <td>инициирован ли показ дополнительными релевантными фразами</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{param1}</td>
                                    <td>первый настраиваемый параметр</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{param2}</td>
                                    <td>второй настраиваемый параметр</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="container" id="tab4C">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="green-txt">
                                <tr>
                                    <th>Параметр</th>
                                    <th>Описание</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="green-txt">{network}</td>
                                    <td>тип площадки: поиск или контекст</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{placement}</td>
                                    <td>площадка, только для КМС</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{placement}</td>
                                    <td>площадка, только для КМС</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{adposition}</td>
                                    <td>позиция объявления</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{creative}</td>
                                    <td>уникальный идентификатор объявления</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{matchtype}</td>
                                    <td>тип соответствия ключевого слова</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{keyword}</td>
                                    <td>ключевое слово</td>
                                </tr>
                                <tr>
                                    <td class="green-txt">{device}</td>
                                    <td>тип устройства</td>
                                </tr>
                                <tr>
                                    <td colspan="2">+ еще 12 utm-меток</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="container" id="tab5C">
                        <img src="demo/images/mail.png" alt="mail_search">
                    </div>
                    <div class="container" id="tab6C">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr class="green-txt">
                                    <th>IP</th>
                                    <th>Город</th>
                                    <th>Операционная система</th>
                                    <th>Браузер</th>
                                    <th>Время</th>
                                    <th>Продолжительность пребывания</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>80.69.182.97</td>
                                    <td>Новосибирск</td>
                                    <td>Mac OS</td>
                                    <td>Chrome</td>
                                    <td>23.15</td>
                                    <td>2 часа 13 минут</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="iphone-detail">
        <div class="container padding-lg benefits" id="features">
            <div class="row">
                <div id="tabs1">
                    <div class="col-md-5 col-sm-6">
                        <h1>Анализ и вывод:</h1>

                        <h2 class="light-grey green-txt">Страницы лендинга:</h2>
                        <ul class="list-unstyled blocknote">
                            <li class="light-grey">Уникальные посетители, просмотры, отклики, продажи, первую и вторую
                                конверсии - все это в реальном времени по всем:
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> страницам
                                мультилендинга
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i>
                                направлениям при распределении трафика (Гео-сплиты, Тайм-сплиты, АБ-тесты)
                            </li>
                        </ul>
                        <p class="light-grey">Позволяет оптимизировать рентабельность, опираясь не только на конверсию в
                            лидов, но и на конверсию в продажи</p>
                    </div>
                    <div class="col-md-7 col-sm-6 hidden-xs about-iphone descr-img text-center">
                        <img src="demo/images/lend_01.png" class="wow fadeIn" data-wow-delay="0.1s"
                             alt="Анализ и вывод">
                        <!-- <img src="images/lend_02.png" class="landings wow fadeIn" data-wow-delay="0.3s">
                        <img src="images/lend_03.png" class="landings wow fadeIn" data-wow-delay="0.5s"> -->
                    </div>
                </div>
            </div>
        </div>
        <canvas class="second"></canvas>
    </div>
@endsection