@extends('layouts.landing-page')

@section('content')
    <div class="iphone-detail padding-md time-split">
        <div class="container">
            <div class="row" id="tabs">
                <div class="col-lg-7 image-section relative wow zoomIn">
                    <ul class="pull-left time-tabs right80">
                        <li>
                            <a id="tab13" class="morning"></a>
                        </li>
                        <li>
                            <a id="tab14" class="inactive day"></a>
                        </li>
                        <li>
                            <a id="tab15" class="inactive evening"></a>
                        </li>
                        <li>
                            <a id="tab16" class="inactive night"></a>
                        </li>
                    </ul>
                    <div class="pull-left tab-content">
                        <div class="container" id="tab13C"><img src="demo/images/morning_l.png" alt="Интерфейс"></div>
                        <div class="container" id="tab14C"><img src="demo/images/day_l.png" alt="Интерфейс"></div>
                        <div class="container" id="tab15C"><img src="demo/images/evening_l.png" alt="Интерфейс"></div>
                        <div class="container" id="tab16C"><img src="demo/images/night_l.png" alt="Интерфейс"></div>
                    </div>
                </div>
                <div class="col-lg-5 col-xs-12 about-iphone">
                    <h1>Тайм-сплит</h1><span></span>

                    <ul class="list-unstyled blocknote">
                        <li class="light-grey name-list">Конверсия вашего сайта зависит от времени суток</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Утром
                            эффективнее короткие лендинги “только о главном”
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Днем можно
                            показывать обычные лендинги
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Вечером хорошо
                            продает видео.
                        </li>
                    </ul>
                    <ul class="list-unstyled blocknote">
                        <li class="light-grey name-list">Что дает:</li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Возможность
                            легко увеличить конверсию
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Распределять
                            трафик на материал, более удобный к просмотру
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Создавать
                            акции, привязанные к времени дня.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <canvas></canvas>
    </div>
    <div class="container padding-lg benefits" id="features">
        <div class="row features">
            <div class="col-lg-5 about-iphone">
                <h1>Гео-сплит</h1><span></span>

                <p class="light-grey">Если для определенных городов Вы можете сделать более интересное предложение -
                    сделайте его! Если в каких-то городах Ваша услуга недоступна, постарайтесь найти партнеров</p>
                <ul class="list-unstyled blocknote">
                    <li class="light-grey name-list">Конверсия вашего сайта зависит от времени суток</li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Возможность делать
                        более выгодные предложения для отобранных городов.
                    </li>
                    <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Позволяет хоть
                        как-то обработать трафик из городов, в которых Ваша услуга недоступна.
                    </li>
                </ul>
            </div>
            <div class="col-lg-7 image-section relative text-center cityes-wrp">
                <ul class="list-inline cityes">
                    <li>
                        <img src="demo/images/Novosibitsk.png" alt="city">

                        <div class="arrow">
                            <i class="md md-keyboard-arrow-down"></i>
                        </div>
                    </li>
                    <li>
                        <img src="demo/images/Khabarovsk.png" alt="city">

                        <div class="arrow">
                            <i class="md md-keyboard-arrow-down"></i>
                        </div>
                    </li>
                    <li>
                        <img src="demo/images/Irkutsk.png" alt="city">

                        <div class="arrow">
                            <i class="md md-keyboard-arrow-down"></i>
                        </div>
                    </li>
                </ul>
                <div>
                    <img src="demo/images/geo14.png" class="landings" alt="Гео-сплит">
                    <img src="demo/images/geo13.png" class="landings" alt="Гео-сплит">
                    <img src="demo/images/geo11.png" class="landings" alt="Гео-сплит">
                </div>
            </div>
        </div>
    </div>
    <div class="iphone-detail padding-md ab-split">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 image-section wow zoomIn">
                    <div class="select-wrp">
                        <div class="form-group">
                            <label for="template">Шаблонов</label>

                            <div class="select-style">
                                <select id="template" onchange="Add()">
                                    <option value="1">1</option>
                                    <option value="2" selected>2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div id="list"></div>
                        </div>
                        <div class="form-group">
                            <label for="goods">Товаров</label>

                            <div class="select-style">
                                <select id="goods" onchange="Add()">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4" selected>4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div id="list1"></div>
                        </div>
                        <div class="form-group">
                            <label for="insert">Вставок</label>

                            <div class="select-style">
                                <select id="insert" onchange="Add()">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div id="list2"></div>
                        </div>
                        <div class="form-group">
                            <label for="replace">Замен</label>

                            <div class="select-style">
                                <select id="replace" onchange="Add()">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5" selected>5</option>
                                </select>
                            </div>
                            <div id="list3"></div>
                        </div>
                    </div>
                    <div class="math">
                        <div class="pull-left">
                            <span class="green-txt txt-bg">Всего:</span>
                            <span class="txt-bg" id="summ">278</span>
                        </div>
                        <div class="pull-right text-right">
                            <p class="txt-bg"><span class="green-txt">+</span> Гео-сплит</p>

                            <p class="txt-bg"><span class="green-txt">+</span> Тайм-сплит</p>

                            <p class="txt-bg"><span class="green-txt">+</span> Формы</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 about-iphone">
                    <div class="left20">
                        <h1> АБ-сплит тест</h1><span></span>

                        <p>Тестируйте гипотезы, разделяя трафик на разные посадочные страницы. Либо нагенерируйте сотни
                            вариаций через специальный “перемножатор” и найдите лучшее решение!</p>
                        <ul class="list-unstyled blocknote">
                            <li class="light-grey name-list">Что дает:</li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Позволяет
                                создавать аб тесты на основе всех вариаций мультилендинга, которые есть в системе.
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Можно
                                настраивать абсолютно любые аб-тесты, отправляя трафик даже на Гео-сплиты/Тайм-сплиты.
                            </li>
                            <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Все это
                                доступно из коробки и имеет простой интерфейс.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <canvas class="second"></canvas>
    </div>
    <div class="padding-md white-bg buttons">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <p class="txt-bg green-txt">Выглядит непонятно, но чувствую, что это очень круто!</p>
                    <button type="button" class="btn btn-2 btn-2a" data-toggle="modal" data-target="#modal1">Хочу
                        попробовать бесплатно
                    </button>
                </div>
                <div class="col-sm-6 text-center">
                    <p class="txt-bg light-grey">- Это все интересно, но как обстоят дела с аналитикой?</p>

                    <p class="txt-bg light-grey">- Наш любимый вопрос: <a href="/analitycs" class="green-txt underline">Аналитика</a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#template').trigger('onchange');
        function Add(){
            var val = $('#template').val();
            var parent = document.getElementById('list');
            parent.innerHTML = '';
            for(i=0; i<val; i++){
                var child =  document.createElement('img');
                child.src="demo/images/template.png";
                input = parent.appendChild(child);
                input.type = 'text';
                input.name = 'name'+(parent.childNodes.length-1);
            }

            var val1 = document.getElementById('goods').value
            var parent1 = document.getElementById('list1');
            parent1.innerHTML = '';
            for(i=0; i<val1; i++){
                var child1 =  document.createElement('img');
                child1.src="demo/images/product.png";
                input = parent1.appendChild(child1);
                input.type = 'text';
                input.name = 'name'+(parent1.childNodes.length-1);
            }

            var val2 = document.getElementById('insert').value
            var parent2 = document.getElementById('list2');
            parent2.innerHTML = '';
            for(i=0; i<val2; i++){
                var child2 =  document.createElement('img');
                child2.src="demo/images/insert.png";
                input = parent2.appendChild(child2);
                input.type = 'text';
                input.name = 'name'+(parent2.childNodes.length-1);
            }

            var val3 = document.getElementById('replace').value
            var parent3 = document.getElementById('list3');
            parent3.innerHTML = '';
            for(i=0; i<val3; i++){
                var child3 =  document.createElement('img');
                child3.src="demo/images/replacements.png";
                input = parent3.appendChild(child3);
                input.type = 'text';
                input.name = 'name'+(parent3.childNodes.length-1);
            }
            var t=+document.getElementById('template').value
            var g=+document.getElementById('goods').value
            var i=+document.getElementById('insert').value
            var r=+document.getElementById('replace').value
            summ=t*g*i*r
            document.getElementById('summ').innerHTML  = summ
        }
    </script>
@endsection