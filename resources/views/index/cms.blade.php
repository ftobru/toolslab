@extends('layouts.landing-page')

@section('content')
    <div class="iphone-detail padding-md first-screen scrollme" data-speed="5">
        <div class="container">
            <div class="row">
                <div class="col-md-7 image-section relative">
                    <div class="hidden-sm hidden-xs">
                        <img src="demo/images/morning_l.png" alt="Лендинг" class="col3 animateme" id="col3-1"
                             data-from="0" data-to="1" data-easing="easeout" data-translatex="-80" data-translatey="200"
                             data-scale="0">
                        <img src="demo/images/day_l.png" alt="Лендинг" class="col3 animateme" id="col3-2" data-from="0"
                             data-to="1" data-easing="linear" data-translatex="-180" data-translatey="200"
                             data-scale="0">
                        <img src="demo/images/evening_l.png" alt="Лендинг" class="col3 animateme" id="col3-3"
                             data-from="0" data-to="1" data-easing="easeinout" data-translatex="-330"
                             data-translatey="200" data-scale="0">
                        <img src="demo/images/night_l.png" alt="Лендинг" class="col3 animateme" id="col3-4"
                             data-from="0" data-to="1" data-easing="easein" data-translatex="-470" data-translatey="200"
                             data-scale="0">

                        <img src="demo/images/folder.png" class="folder animateme" alt="CRM" data-from="1" data-to="2"
                             data-easing="easeinout" data-translatex="0" data-translatey="200">
                    </div>

                    <div class="wrap animateme" data-from="1" data-to="2" data-easing="easeinout" data-scale="1.2">

                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide"><img src="demo/images/morning_l.png" alt="Лендинг"></div>
                                <div class="swiper-slide"><img src="demo/images/day_l.png" alt="Лендинг"></div>
                                <div class="swiper-slide"><img src="demo/images/evening_l.png" alt="Лендинг"></div>
                                <div class="swiper-slide"><img src="demo/images/night_l.png" alt="Лендинг"></div>
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                </div>
                <div class="col-md-5 about-iphone">
                    <h1>CMS удобная система управления лендингами</h1><span></span>

                    <p>Любой лендинг, неважно, сделан он на шаблоне своими силами или с нуля студией, все они состоят из
                        небольшого набора файлов.</p>

                    <p>Просто загрузите эти файлы в систему самостоятельно, либо наши специалисты могут сделать это за
                        вас.</p>
                </div>
            </div>
        </div>
        <canvas></canvas>
    </div>


    <div class="line-list">
        <div class="container">
            <div class="row">
                <h1>Все! теперь Вы без участия программиста можете сами обслуживать свои лендинги:</h1>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-6 new1">
                    <span>Создавать новые страницы</span>
                </div>
                <div class="col-sm-3 col-xs-6 new2">
                    <span>Выключать и удалять ненужные</span>
                </div>
                <div class="col-sm-3 col-xs-6 new3">
                    <span>Менять URL страниц</span>
                </div>
                <div class="col-sm-3 col-xs-6 new4">
                    <span>Добавлять любые интеграции <sup
                                data-tooltip="Например, добавить на сайт Яндекс Метрику или подключить онлайн чат"
                                class="green-txt helper"><i class="md md-help"></i></sup></span>
                </div>
            </div>
        </div>
    </div>


    <div class="container padding-lg benefits">
        <div class="row features text-center list-benefits">
            <div class="col-sm-3 wow zoomIn" data-wow-delay="0.2s">
                <img src="demo/images/hosting.png" alt="img">

                <h3 class="green-txt">Мощный хостинг</h3>

                <p>У нас связка своих выделенных серверов в защищенном дата-центре</p>
            </div>
            <div class="col-sm-3 wow zoomIn" data-wow-delay="0.4s">
                <img src="demo/images/2GB_of_memory.png" alt="img">

                <h3 class="green-txt">2 GB памяти</h3>

                <p>Хватит, чтобы загрузить более 500 лендингов.</p>
            </div>
            <div class="col-sm-3 wow zoomIn" data-wow-delay="0.6s">
                <img src="demo/images/api.png" alt="img">

                <h3 class="green-txt">API - подключение</h3>

                <p>Все данные можно вытягивать через API и интегрировать с другими сервисами</p>
            </div>
            <div class="col-sm-3 wow zoomIn" data-wow-delay="0.8s">
                <img src="demo/images/reliably.png" alt="img">

                <h3 class="green-txt">Безопасность</h3>

                <p>Наша команда круглосуточно мониторит безопасность Вашего сайта.</p>
            </div>
        </div>
    </div>


    <div class="iphone-detail padding-md scrollme">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 about-iphone">
                    <h1>Домены</h1><span></span>

                    <p>Создайте до 5 доменов под нашей системой или используйте свои. В четыре клика меняйте домены и
                        адреса у Ваших страниц.</p>

                    <p>С помощью файлового менеджера загрузить файлы на сервер сможет даже ребенок.</p>
                </div>
                <div class="col-sm-6 image-section">
                    <div class="monitor">
                        <div class="type-wrap">
                            <p class="green-txt">Наш домен:</p>

                            <p><span id="typed">site</span>.modulCRM.</p>

                            <p class="green-txt">Ваш домен:</p>

                            <p><span id="typed1">yourdimen.modulCRM.ru</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <canvas class="second"></canvas>
    </div>

    <div class="padding-lg white-bg forms-section scrollme">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 relative text-center animated wow fadeInLeft">
                    <img src="demo/images/forma.png" alt="form" class="forma1">
                </div>
                <div class="col-sm-6 about-iphone padding-right">
                    <h1 class="green-txt">Формы - Быстро и легко создать форму захвата!</h1>

                    <p class="light-grey name-list">Форма захвата является самой важной и проблемной частью посадочной
                        страницы. </p>
                    <ul class="list-unstyled blocknote">
                        <li class="animated fadeIn" data-wow-delay="1s"><i class="md md-play-arrow"></i> Их нужно
                            тестировать и проверять.
                        </li>
                        <li class="animated fadeIn" data-wow-delay="1.5s"><i class="md md-play-arrow"></i> Для их
                            создания нужен программист.
                        </li>
                        <li class="animated fadeIn" data-wow-delay="2s"><i class="md md-play-arrow"></i> Как правило все
                            лиды в примитивном формате отправляются на почту.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="line-list">
        <div class="container">
            <div class="row">
                <h1>Легко создать, без участия программиста:</h1>

                <div class="col-sm-4 new1">
                    <span>Выберите нужные данные для захвата из CRM</span>
                </div>
                <div class="col-sm-4 new1">
                    <span>Напишите заголовки и текст</span>
                </div>
                <div class="col-sm-4 new5">
                    <span>Привяжите форму к кнопке</span>
                </div>
            </div>
        </div>
    </div>

    <div class="iphone-detail benefits padding-lg">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Получайте больше:</h1><span></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 contacts wow zoomIn" data-wow-delay="0.2s">
                    <ul class="list-unstyled blocknote">
                        <li><h3 class="no-margin green-txt">Контакты</h3></li>
                        <li><i class="md md-play-arrow"></i> Лид будет создан и занесен в CRM</li>
                        <li><i class="md md-play-arrow"></i> Все данные будут в своих полях в CRM</li>
                    </ul>
                </div>
                <div class="col-sm-6 create wow zoomIn" data-wow-delay="0.4s">
                    <ul class="list-unstyled blocknote">
                        <li><h3 class="no-margin green-txt">Задача</h3></li>
                        <li><i class="md md-play-arrow"></i> Будет создана задача с нужным ответственным</li>
                        <li><i class="md md-play-arrow"></i> При долгом ожидании ответственный может смениться, потеряв
                            сделку
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 tasks wow zoomIn" data-wow-delay="0.6s">
                    <ul class="list-unstyled blocknote">
                        <li><h3 class="no-margin green-txt">Сделка</h3></li>
                        <li><i class="md md-play-arrow"></i> Будет создана сделка с рекламируемым товаром</li>
                        <li><i class="md md-play-arrow"></i> В заказе содержится стоимость, себестоимость и тд</li>
                    </ul>
                </div>
                <div class="col-sm-6 tag wow zoomIn" data-wow-delay="0.8s">
                    <ul class="list-unstyled blocknote">
                        <li><h3 class="no-margin green-txt">Тег</h3></li>
                        <li><i class="md md-play-arrow"></i> Лендинги и формы могут передавать специальные метки, по
                            которым можно будет быстрее ориентироваться в пришедших лидах.<span
                                    data-tooltip="#юр.лицо, #опт, #консультация" class="green-txt helper"><i
                                        class="md md-help"></i></span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 analytics wow zoomIn" data-wow-delay="0.10s">
                    <ul class="list-unstyled blocknote">
                        <li><h3 class="no-margin green-txt">Аналитика</h3></li>
                        <li><i class="md md-play-arrow"></i> Понимает, какой мультилендинг отображался</li>
                        <li><i class="md md-play-arrow"></i> Запоминает, какая форма и кнопка привела лида</li>
                        <li><i class="md md-play-arrow"></i> Запоминает все UTM метки и страницу перехода</li>
                        <li><i class="md md-play-arrow"></i> Город, время, браузер и прочую информацию</li>
                    </ul>
                </div>
                <div class="col-sm-6 next_step wow zoomIn" data-wow-delay="0.11s">
                    <ul class="list-unstyled blocknote">
                        <li><h3 class="no-margin green-txt">Следующий шаг</h3></li>
                        <li><i class="md md-play-arrow"></i> Выстраивайте цепочки следующих шагов.</li>
                        <li><i class="md md-play-arrow"></i> Вы можете просто поблагодарить или предложить вступить в
                            вашу группу в вк.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <canvas class="third"></canvas>
    </div>


    <div class="padding-lg white-bg benefits forms-section scrollme text-center">
        <div class="container">
            <div class="row list-benefits">
                <div class="col-sm-3 wow fadeIn" data-wow-duration="0.5s" data-wow-delay="0.2s">
                    <img src="demo/images/easy.png" alt="img">

                    <h3 class="green-txt">Легко</h3>

                    <p>Понятный интерфейс для создания</p>
                </div>
                <div class="col-sm-3 wow fadeIn" data-wow-duration="0.7s" data-wow-delay="0.4s">
                    <img src="demo/images/fast.png" alt="img">

                    <h3 class="green-txt">Быстро</h3>

                    <p>На создание уйдет 5-7 минут</p>
                </div>
                <div class="col-sm-3 wow fadeIn" data-wow-duration="0.9s" data-wow-delay="0.6s">
                    <img src="demo/images/reliably.png" alt="img">

                    <h3 class="green-txt">Надежно</h3>

                    <p>Кнопки гарантированно работают на всех утройствах</p>
                </div>
                <div class="col-sm-3 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.8s">
                    <img src="demo/images/Functionally.png" alt="img">

                    <h3 class="green-txt">Функционально</h3>

                    <p>Большой выбор настроек и аналитики</p>
                </div>
            </div>
        </div>
    </div>

    <div class="iphone-detail benefits padding-md text-center buttons">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 text-center">
                    <p class="txt-bg green-txt">Давно искал систему управления лендингами.</p>

                    <p class="txt-bg green-txt">Это первая настоящая CMS.</p>
                    <button class="btn btn-2 btn-2a" data-toggle="modal" data-target="#modal1">Попробовать бесплатно 14
                        дней!
                    </button>
                </div>
                <div class="col-sm-6 text-center">
                    <p class="txt-bg light-grey">Окей, я загрузил страницу и создал несколько форм, что дальше?.</p>
                    <a href="/multilanding" class="green-txt underline txt-bg" style="line-htight: 8em;">Динамический
                        контент</a>
                </div>
            </div>
        </div>
        <canvas class="fourth"></canvas>
    </div>
@endsection