@extends('layouts.master_simple')

@section('header')
    <div class="headerbar">
        <div class="headerbar-right">
            <ul class="header-nav header-nav-options">
                <li>
                    <a href="" data-toggle="modal" id="btn-modal-register">Попробовать прямо сейчас</a>
                </li>

                <li>
                    <p>&nbsp|&nbsp</p>
                </li>

                <li>
                    @if($isLogin)
                        <a href="/auth/logout">Выход</a>
                    @else
                        <a href="/auth/login">Вход</a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
@stop

@section('content')
    <section class="profile">
        <div class="section-body">
            <div id="content">

            </div>
            <!--end content-->
        </div>
    </section>
@stop

@section('modals')
    <div class="modal fade" id="modal-register">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="modal-title text-primary">Попробуйте ToolsLab сейчас!</h3>
                </div>
                <div class="card-body">
                    <form class="form floating-label" id="registration-form-step1">

                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                <div class="input-group-content">
                                    <input type="text" class="form-control" id="name1" name="name1">
                                    <label for="name1">Имя</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-phone fa-lg"></span></span>
                                <div class="input-group-content">
                                    <label for="phone2"></label>
                                    <input data-inputmask="'mask': '(999) 999-9999'" type="text" id="phone2" name="phone2" class="form-control phone-ru"  placeholder="Телефон">
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope fa-lg"></span></span>

                                <div class="input-group-content">
                                    <input type="email" class="form-control" id="email1" name="email1">
                                    <label for="email1">E-mail</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label has-error" id="resp-error1" style="display:none;"></div>
                        <div class="modal-footer text-center">
                            <button type="submit" data-loading-text="Загрузка..."
                                    class="btn btn-danger"
                                    >Попробовать бесплатно сейчас
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#phone2").inputmask();

            $.validator.addMethod(
                    "regex",
                    function(value, element, regexp) {
                        var re = new RegExp(regexp);
                        return this.optional(element) || re.test(value);
                    },
                    "Номер телефона введен неправильно."
            );

            $("#registration-form-step1").validate({
                rules: {
                    "name1": {
                        required: true,
                        minlength: 2
                    },
                    "phone2": {
                        required: true,
                        regex: "^\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}"
                    },
                    "email1": {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    "name1": {
                        required: "Введите имя",
                        minlength: "Минимальная длинна имени 2 символов"
                    },
                    "phone1": {
                        required: "Введите номер телефона",
                        digits: "Номер должен состоять только из цифр",
                        minlength: "Номер должен состоять из 10 цифр",
                        maxlength: "Номер должен состоять из 10 цифр"
                    },
                    "email1": {
                        required: "Введите email",
                        email: "Введите корректный email"
                    }
                },
                submitHandler: function () {
                    $('#resp-error1').hide('fast');
                    $.cookie("name", $('#name1').val());
                    $.cookie("email", $('#email1').val());
                    $.cookie("phone", $('#phone2').val());
                    window.location.href = '/demo';
                }
            });

            $('#btn-modal-register').click(function () {
                if (!$.cookie("name") && !$.cookie("email") && !$.cookie("phone")) {
                    $('#modal-register').modal('toggle');
                } else {
                    window.location.href = '/demo';
                }
            });
        });

    </script>
@stop
