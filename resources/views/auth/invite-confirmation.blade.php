@extends('layouts.simple_private')

@section('header')
    <div class="headerbar">
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand">
                    <div class="brand-holder">
                        <a href="/">
                            <span class="text-lg text-bold text-primary">ModulCRM</span>
                        </a>
                    </div>
                </li>

            </ul>
        </div>
    </div>
@stop

@section('content')
    <section>
        <div class="section-body contain-lg">
            <div class="col-md-6 col-md-offset-3 col-sm-7 col-sm-offset-2">
                <div class="card-body">
                    <form class="form floating-label" id="registration-form-step2">
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                <div class="input-group-content">
                                    <input type="text" class="form-control" id="name2" name="name2">
                                    <label for="name2">Имя</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-phone fa-lg"></span></span>

                                <div class="input-group-content">
                                    <input type="text" id="phone2" data-inputmask="'mask': '(999) 999-9999'"
                                           name="phone2" class="form-control phone-ru">
                                    <span class="reg-phone"> +7</span>
                                    <label for="phone2"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                <div class="input-group-content">
                                    <input type="password" class="form-control" id="password21" name="password21">
                                    <label for="password21">Пароль</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label">
                            <div class="input-group">
                                <span class="input-group-addon"><span
                                            class="glyphicon glyphicon-user fa-lg"></span></span>

                                <div class="input-group-content">
                                    <input type="password" class="form-control" id="password22" name="password22">
                                    <label for="password22">Подтвердить пароль</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group floating-label has-error" id="resp-error2" style="display:none;">

                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btn-register-modal-post" data-loading-text="Загрузка..."
                                    class="btn btn-danger">Создать аккаунт
                            </button>
                            <p><i class="md md-lock"></i> Ваши данные надежно защищены</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#phone2').inputmask();
            $.validator.addMethod(
                    "regex",
                    function(value, element, regexp) {
                        var re = new RegExp(regexp);
                        return this.optional(element) || re.test(value);
                    },
                    "Перепроверьте формат ввода телефона."
            );

            //validation of registration
            $("#registration-form-step2").validate({
                rules: {
                    "name2": {
                        required: true,
                        minlength: 2
                    },
                    "phone2": {
                        required: true,
                        regex: "^\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}"
                        //regex: "^\\([0-9]{3}\\)\\s[0-9]{3}-[0-9]{4}"
                    },
                    "password21": {
                        required: true,
                        minlength: 6
                    },
                    "password22": {
                        required: true,
                        minlength: 6,
                        equalTo: "#password21"
                    }
                },
                messages: {
                    "name2": {
                        required: "Введите имя",
                        minlength: "Минимальная длинна имени 2 символов"
                    },
                    "phone2": {
                        required: "Введите номер телефона",
                        minlength: "Номер должен состоять из 10 цифр",
                        maxlength: "Номер должен состоять из 10 цифр"
                    },
                    "password21": {
                        required: "Введите пароль",
                        minlength: "Минимальная длинна пароля 6 символов"
                    },
                    "password22": {
                        required: "Повторите пароль",
                        minlength: "Минимальная длинна пароля 6 символов",
                        equalTo: "Пароли не совпадают"
                    }
                },
                submitHandler: function () {

                }
            });

            $('#btn-register-modal-post').on('click', function(e) {
                if ($("#registration-form-step2").valid()){
                    $('#resp-error2').hide('fast');

                    var name = $('#name2').val();
                    var phone = $('#phone2').val();
                    var pass1 = $('#password21').val();
                    var pass2 = $('#password22').val();

                    var data = {
                        name: name,
                        password: pass1,
                        phone: phone,
                        invite_id: {{{$invite->id}}}
                    };

                    $.ajax({
                        url: "/auth/ajax/completeInviteConfirmation",
                        method: "POST",
                        data: data,

                        success: function (result) {
                            if (result.result === true) {

                                var errorBlock = $('#resp-error2');
                                errorBlock.hide('fast');
                            }
                        },

                        error: function (result) {
                            var obj = JSON.parse(result.responseText);
                            if (obj.error) {
                                var errorBlock = $('#resp-error2');
                                switch (obj.error) {
                                    case 302:
                                        errorBlock.html('<span class="help-block">Такой телефон уже зарегистрирован</span>');
                                        break;
                                    default:
                                        errorBlock.html('<span class="help-block">Непредвиденная ошибка</span>');
                                        break;
                                }
                                errorBlock.show('slow');
                            }
                        }
                    });
                }
            });
        });
    </script>
@stop
