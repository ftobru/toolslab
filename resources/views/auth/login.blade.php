@extends('layouts.simple_private')

@section('header')
        <div class="headerbar">
            <div class="headerbar-left">
                <ul class="header-nav header-nav-options">
                    <li class="header-nav-brand" >
                        <div class="brand-holder">
                            <a href="/">
                                <span class="text-lg text-bold text-primary">ModulCRM</span>
                            </a>
                        </div>
                    </li>

                </ul>
            </div>
        </div>
@stop

@section('content')
    <section>
                <div class="section-body contain-lg">
                    <div class="col-md-6 col-md-offset-3 col-sm-7 col-sm-offset-2">
                        <form class="form floating-label" id="login-form">
                            <div class="card">
                                <div class="card-head style-primary">
                                    <header>Вход в личный кабинет</header>
                                </div>
                                <div class="card-body">
                                    <div class="form-group floating-label">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-user fa-lg"></span></span>
                                            <div class="input-group-content">
                                                <input type="email" class="form-control" id="email10" name="email10">
                                                <label for="email10">E-mail</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group floating-label">
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock fa-lg"></span></span>
                                            <div class="input-group-content">
                                                <input
                                                        type="password"
                                                        class="form-control"
                                                        id="password1"
                                                        name="password1"
                                                        >
                                                <label for="password1">Пароль</label>
                                            </div>
                                        </div>
                                        <p class="help-block"><a href="{{URL::route('user.restorePasssword')}}">Забыли пароль?</a></p>
                                    </div>
                                    <div class="form-group floating-label has-error" id="login-error" style="display:none;">
                                    </div>
                                    <br/>
                                    <div class="row">
                                        <div class="col-xs-7 text-left">
                                            <div class="checkbox checkbox-inline checkbox-styled">
                                                <label>
                                                    <input type="checkbox" id="remember_me" name="remember_me"> <span>Запомнить меня</span>
                                                </label>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-xs-5 text-right">
                                            <button type="submit" class="btn btn-primary btn-raised">Вход</button>
                                        </div><!--end .col -->
                                    </div><!--end .row -->
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
@stop

@section('scripts')
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $('#login-form').validate({
                rules: {
                    "password1": {
                        required: true,
                        minlength: 5
                    },
                    "email10": {
                        required: true,
                        email: true
                    }
                },
                messages: {
                    "password1": {
                        required: "Введите пароль",
                        minlength: "Минимальная длинна пароля 5 символов",
                    },
                    "email10": {
                        required: "Введите email",
                        email:  "Введите корректный email"
                    }
                },
                submitHandler: function() {
                    $('#login-error').hide('fast');

                    var email = $('#email10').val();
                    var password = $('#password1').val();
                    var remember =  $("#remember_me").is(":checked");

                    var data = {
                        "email": email,
                        "password": password,
                        "remember":  Boolean(remember)
                    };
                    $.ajax({
                        url: "/auth/ajax/login",
                        method: "POST",
                        data: data,

                        success: function (result, status, xhr) {
                            if(result.result) {
                                window.location.href = '/companies/my';
                            } else {
                                $('#login-error').html('<span class="help-block">Неправильный логин или пароль.</span>');
                            }
                            $('#login-error').show('slow');
                        },
                        error: function (result) {
                            $('#login-error').html('<span class="help-block">Непредвиденная ошибка</span>');
                            $('#login-error').show('slow');
                        }
                    });
                }
            });
        });
    </script>
@stop
