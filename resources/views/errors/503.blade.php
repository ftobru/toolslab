@extends('layouts.master_simple')

@section('content')

        <!-- BEGIN 503 MESSAGE -->
        <section>
            <div class="section-header">
                <ol class="breadcrumb">
                    <li><a href="{{{ URL::previous() }}}">Назад</a></li>
                    <li class="active">503</li>
                </ol>
            </div>
            <div class="section-body contain-lg">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h1><span class="text-xxxl text-light">503 <i class="fa fa-clock-o text-primary"></i></span></h1>
                        <h2 class="text-light">Сервис временно недоступен</h2>
                    </div><!--end .col -->
                </div><!--end .row -->
            </div><!--end .section-body -->
        </section>
        <!-- END 503 MESSAGE -->

@stop

