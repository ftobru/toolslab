
@extends('layouts.master_simple')

@section('content')
    <section>
        <div class="section-header">
            <ol class="breadcrumb">
                <li><a href="{{{ URL::previous() }}}">Назад</a></li>
                <li class="active">403</li>
            </ol>
        </div>
        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1><span class="text-xxxl text-light">403 <i
                                    class="fa fa-exclamation-triangle text-primary"></i></span></h1>

                    <h2 class="text-light">У Вас недостаточно прав для выполнения данного действия.</h2>
                </div>
            </div>
        </div>
    </section>
@stop