@extends('layouts.master_simple')


@section('content')
        <section>
            <div class="section-header">
                <ol class="breadcrumb">
                    <li><a href="{{{ URL::previous() }}}">Назад</a></li>
                    <li class="active">404</li>
                </ol>
            </div>
            <div class="section-body contain-lg">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h1><span class="text-xxxl text-light">404 <i class="fa fa-search-minus text-primary"></i></span></h1>
                        <h2 class="text-light">Страница не найдена</h2>
                    </div><!--end .col -->
                </div><!--end .row -->
            </div><!--end .section-body -->
        </section>
        <!-- BEGIN SEARCH SECTION -->
        <section>
            <div class="section-body contain-sm">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <input type="text" class="form-control" placeholder="Вы искали...">
                    <span class="input-group-btn"><button class="btn btn-primary" type="submit">Найти</button></span>
                </div>
            </div><!--end .section-body -->
        </section>
        <!-- END SEARCH SECTION -->
@stop

@section('scripts')
    <script type="text/javascript"></script>
@stop

