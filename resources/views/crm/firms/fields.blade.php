@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_add_simple')
@stop

@section('content')
    <div class="card-body tab-content filter-page analytics card">
        <div id="off-canvas1" class="filter-menu">
            <nav>
                <div class="expanded">
                    <a href="../../html/dashboards/dashboard.html">
                        <span class="text-lg text-bold text-primary">Доп.&nbsp;поля&nbsp;</span>
                    </a>
                </div>
                <form class="form">
                    <ul class="menu-inside">
                        <li><a href="/{{{$company->name}}}/crm/firms/fields">Компании</a></li>
                        <li><a href="/{{{$company->name}}}/crm/contacts/fields">Контакты</a></li>
                        <li><a href="/{{{$company->name}}}/crm/products/fields">Товары</a></li>
                    </ul>
                </form>
            </nav>
        </div><!--/off-canvas-->
        <section class="style-default-bright client-info tab-pane active">
            <div class="section-body variables">
                <ul class="list divider-full-bleed list-events ui-sortable" data-sortable="true">
                    @foreach($customFields as $field)
                        <li class="tile ui-draggable ui-draggable-handle" id="form-card-{{{$field->id}}}">
                            <div class="row">
                                <div class=" col-lg-10 col-lg-offset-1 col-sm-12">
                                    <div class="card">
                                        <form class="field-form" role="form">
                                            <div class="card-body">
                                                <div class="row margin-bottom-xxl">
                                                    <div class="pull-left col-sm-3">
                                                        <span class="text-primary title-result">{{{$field->label}}}</span>
                                                    </div>
                                                    <div class="pull-left col-sm-4">
                                                        <span> Тип: {{{$field->form_type_label}}}</span>
                                                    </div>
                                                    <div class="col-sm-5 text-right buttons">
                                                        <ul class="list-inline">
                                                            <li>
                                                                <button type="button" data-action="delete"
                                                                        data-formName="{{{$field->label}}}"
                                                                        data-formId="{{{$field->id}}}"
                                                                        class="btn btn-default-light ink-reaction btn-sm">
                                                                    Удалить
                                                                </button>
                                                            </li>
                                                            <li>
                                                                <button type="button" data-action="edit"
                                                                        data-formId="{{{$field->id}}}"
                                                                        class="btn ink-reaction btn-primary btn-sm">
                                                                    Изменить
                                                                </button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div><!--end .section-body -->
        </section>
    </div>
@stop

@section('modals')
    <div class="modal fade" id="modal1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3 class="modal-title text-primary">Удаление дополнительного поля</h3>
                </div>
                <div class="card-body">
                    Вы уверенны, что хотите удалить данное поле "<span class="field-name"></span>"? Совершенное действие будет необратимым.
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default ink-reaction">Отменить</button>
                    <button type="button" data-action="delete" data-formId="" class="btn ink-reaction btn-primary">Удалить</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">

        var currentFormId;

        $(window).ready(function(){

            $('#header_add_simple_btn').attr("href", '/{{{$company->name}}}/saas/forms/create');

            $('.field-form button[data-action="delete"]').on('click', function (e) {
                e.preventDefault();
                var _self = $(this);
                var formId = parseInt(_self.data('formid'));
                var name = _self.data('formname');
                $('span.field-name').text(name);
                $('#modal1 button[data-action="delete"]').data('formid', formId);
                $('#modal1').modal();
            });

            $('#modal1 button[data-action="delete"]').on('click', function() {
                var _self = $(this);
                var formId = parseInt(_self.data('formid'));
                var data = {id: formId};
                $('#modal1').modal('hide');
                $.ajax({
                    url: "/{{{$company->name}}}/saas/ajax/forms/delete",
                    method: "POST",
                    data: data,
                    success: function (result) {
                        toastr.success("Доп. поле успешно удалено");
                        $('#form-card-'+data.id).hide('slow', function(){ $target.remove(); });
                    },
                    error: function (result) {
                        toastr.error("Произошла ошибка");
                    }
                });

            });

            $('.field-form button[data-action="edit"]').click(function(e) {
                var _self = $(this);
                var formId = parseInt(_self.data('formid'));
                window.location = '/{{{$company->name}}}/saas/forms/edit/' + formId;
            });
        })
    </script>
@stop