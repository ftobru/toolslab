<div class="panel">
    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-12">
        <span class="text-primary text-lg">Обзор задач по типу</span>
								<span>
									<a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i class="md md-keyboard-arrow-down"></i></a>
								</span>
    </div>
    <div id="accordion2-12" class="collapse in">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="table-responsive no-margin no-side-padding">
                    <table class="table no-margin table-striped table-bordered">
                        <thead>
                        <tr class="warning">
                            <th><span data-toggle="tooltip" data-placement="auto" title="описание задач">Задачи</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="всего">Всего</span></th>
                            <th>Что тут? </th>
                        </tr>
                        </thead>
                        <tbody id="accordion8-4" class="collapse opacity-75" aria-expanded="true" role="alert">
                        <tr>
                            <td>Новые</td>
                            <td>40</td>
                            <td>18%</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>Просроченные</td>
                            <td>60</td>
                            <td>18%</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>Выполненные</td>
                            <td>220</td>
                            <td>18%</td>
                            <td>1</td>
                        </tr>
                        </tbody>
                        <tr data-toggle="collapse" data-target="#accordion8-4" aria-expanded="true" role="alert">
                            <td class="tools text-center text-primary" colspan="4">
                                <a class="btn btn-icon-toggle"><i class="md md-keyboard-control"></i></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>