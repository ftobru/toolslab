<div class="panel">
    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-1">
        <span class="text-primary text-lg">Обзор</span>
                                    <span>
                                        <a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i class="md md-keyboard-arrow-down"></i></a>
                                    </span>
    </div>
    <div id="accordion2-1" class="collapse in">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="table-responsive no-margin no-side-padding">
                    <table class="table no-margin table-striped table-bordered">
                        <thead>
                        <tr class="warning">
                            <th><span data-toggle="tooltip" data-placement="auto" title="описание сделок">Сделки</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="описание колва">Кол-во</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="описание чека">Чек</span></th>
                        </tr>
                        <tr class="text-bold">
                            <td>Все</td>
                            <td colspan="2">220</td>
                            <td colspan="2">34220</td>
                        </tr>
                        </thead>
                        <tbody id="accordion8-1" class="collapse opacity-75" aria-expanded="true" role="alert">
                        <tr>
                            <td>Новые</td>
                            <td>12</td>
                            <td>18%</td>
                            <td>1231</td>
                            <td>15%</td>
                        </tr>
                        <tr>
                            <td>Активные</td>
                            <td>12</td>
                            <td>18%</td>
                            <td>1231</td>
                            <td>15%</td>
                        </tr>
                        <tr>
                            <td>Успешные</td>
                            <td>120</td>
                            <td>18%</td>
                            <td>1231</td>
                            <td>15%</td>
                        </tr>
                        <tr>
                            <td>Неуспешные</td>
                            <td>120</td>
                            <td>18%</td>
                            <td>1231</td>
                            <td>15%</td>
                        </tr>
                        </tbody>
                        <tr data-toggle="collapse" data-target="#accordion8-1" aria-expanded="true" role="alert">
                            <td class="tools text-center text-primary" colspan="5">
                                <a class="btn btn-icon-toggle"><i class="md md-keyboard-control"></i></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>