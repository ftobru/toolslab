<div class="panel">
    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-11">
        <span class="text-primary text-lg">Обзор</span>
								<span>
									<a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i class="md md-keyboard-arrow-down"></i></a>
								</span>
    </div>
    <div id="accordion2-11" class="collapse in">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="table-responsive no-margin no-side-padding">
                    <table class="table no-margin table-striped table-bordered">
                        <thead>
                        <tr class="warning">
                            <th><span data-toggle="tooltip" data-placement="auto" title="описание задач">Комментарии</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="всего">Всего</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="всего">Новых</span></th>
                        </tr>
                        </thead>
                        <tbody id="accordion8-6" class="collapse opacity-75" aria-expanded="true" role="alert">
                        <tr>
                            <td>Создано</td>
                            <td>40</td>
                            <td>4%</td>
                            <td>2</td>
                            <td>2%</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
