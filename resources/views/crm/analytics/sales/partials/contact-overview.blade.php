<div class="panel">
    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-4">
        <span class="text-primary text-lg">Обзор</span>
								<span>
									<a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i class="md md-keyboard-arrow-down"></i></a>
								</span>
    </div>
    <div id="accordion2-4" class="collapse in">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="table-responsive no-margin no-side-padding">
                    <table class="table no-margin table-striped table-bordered">
                        <thead>
                        <tr class="warning">
                            <th><span data-toggle="tooltip" data-placement="auto" title="описание сделок">Контакты</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="описание колва">Всего</span></th>
                            <th colspan="2"><span data-toggle="tooltip" data-placement="auto" title="описание чека">Новых</span></th>
                        </tr>
                        <tr class="text-bold">
                            <td>Всего</td>
                            <td>220</td>
                            <td>22%</td>
                            <td>34220</td>
                            <td>78%</td>
                        </tr>
                        </thead>
                        <tbody id="accordion8-2" class="collapse opacity-75" aria-expanded="true" role="alert">
                        <tr>
                            <td>Всего касаний</td>
                            <td>120</td>
                            <td>18%</td>
                            <td>1231</td>
                            <td>15%</td>
                        </tr>
                        <tr>
                            <td>Всего контактов</td>
                            <td>220</td>
                            <td>18%</td>
                            <td>22222</td>
                            <td>10%</td>
                        </tr>
                        <tr>
                            <td>Всего компаний</td>
                            <td>220</td>
                            <td>18%</td>
                            <td>22222</td>
                            <td>10%</td>
                        </tr>
                        </tbody>
                        <tr data-toggle="collapse" data-target="#accordion8-2" aria-expanded="true" role="alert">
                            <td class="tools text-center text-primary" colspan="5">
                                <a class="btn btn-icon-toggle"><i class="md md-keyboard-control"></i></a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>