<div id="off-canvas1" class="filter-menu">
    <nav>
        <div class="expanded">
            <a href="#">
                <span class="text-lg text-bold text-primary">Фильтр&nbsp;</span>
            </a>
        </div>
        <form class="form">
            <label id="autocomplete1" class="form-control autocomplete" style="display:none;"></label>
            <ul>
                <li>
                    <div class="form-group">
                        <label class="opacity-75">Созданы</label>
                        <div class="input-daterange input-group" id="demo-date-range">
                            <span class="input-group-addon">с</span>
                            <div class="input-group-content">
                                <input type="text" class="form-control input-sm" name="start" />
                                <div class="form-control-line"></div>
                            </div>
                            <span class="input-group-addon">по</span>
                            <div class="input-group-content">
                                <input type="text" class="form-control input-sm" name="end" />
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="form-group no-padding">
                        <select class="form-control select2-list input-sm" data-placeholder="Выберите ответственного">
                            <option>&nbsp;</option>
                            <optgroup label="Название отдела">
                                <option value="AK">анна валентиновна</option>
                                <option value="HI">Валентин Владимирович</option>
                                <option value="рр">Мария Валентинвна</option>
                            </optgroup>
                        </select>
                    </div>
                </li>
                <li>
                    <ul class="list-unstyled list-inline margin-bottom-xxl">
                        <li><button type="submit" class="btn btn-default ink-reaction btn-sm">Отменить</button></li>
                        <li><button type="submit" class="btn ink-reaction btn-primary btn-sm">Применить</button></li>
                    </ul>
                </li>
            </ul>
        </form>
    </nav>
</div><!--/off-canvas-->