@extends('layouts.menubar_general')
    @section('left-header-buttons')
        <li>
            <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
               data-toggle="menubar1">
                <i class="fa fa-filter"></i>
            </a>
        </li>
        <li>
            <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </li>
    @stop
    @section('content')
        @include('crm.analytics.sales.partials.filter')
        <div class="filter-page analytics">
            <section class="style-default-bright client-info tab-pane active">
                <section class="force-padding">
                    <h3>Сделки</h3>
                    <div id="order-overview">
                        @include('crm.analytics.sales.partials.order-overview')
                    </div>
                    <div id="order-chart-price">
                        @include('crm.analytics.sales.partials.order-chart-price')
                    </div>
                    <div id="order-chart-count">
                        @include('crm.analytics.sales.partials.order-chart-count')
                    </div>

                    <h3>Контакты</h3>
                    <div id="contact-overview">
                        @include('crm.analytics.sales.partials.contact-overview')
                    </div>
                    <div id="contact-chart-all">
                        @include('crm.analytics.sales.partials.contact-chart-all')
                    </div>
                    <div id="contact-chart-new">
                        @include('crm.analytics.sales.partials.contact-chart-new')
                    </div>

                    <h3>Задачи</h3>
                    <div id="task-overview">
                        @include('crm.analytics.sales.partials.task-overview')
                    </div>

                    <div id="task-overview-type">
                        @include('crm.analytics.sales.partials.task-overview-type')
                    </div>
                    <div id="task-chart-price">
                        @include('crm.analytics.sales.partials.task-chart-price')
                    </div>
                    <div id="task-chart-price">
                        @include('crm.analytics.sales.partials.task-chart-types')
                    </div>

                    <h3>Комментарии</h3>
                    <div id="comments">
                        @include('crm.analytics.sales.partials.comments')
                    </div>
                </section>
            </section>

        </div>
    @stop
    @section('scripts')
        <script src="http://cdn.kendostatic.com/2015.1.429/js/kendo.all.min.js"></script>
        <script type="text/javascript">
            var StateManager = {


                stateMenu: false,
                width: null,
                stateFilter: null,
                stateContent: null,
                currentMenuWidth: 0,
                currentFilterLeft: 0,
                currentContentPaddingLeft:  0,
                isContentMoveable: false,
                isMobile: false,



                filterWidth: 320,
                // Левое меню - ПК  большой экран
                closeMenuWidthPCBig: 60,
                openMenuWidthPCBig: 240,
                // Левое меню - ПК   маленький экран
                closeMenuWidthPCSmall: 0,
                openMenuWidthPCSmall: 240,
                // Левое меню - Mobile  большой экран
                closeMenuWidthMobBig: 60,
                openMenuWidthMobBig: 240,
                // Левое меню - Mobile  min экран
                closeMenuWidthMobSmall: 0,
                openMenuWidthMobSmall: 240,

                prevState:null,



                setStateMenu: function() {
                    this.stateMenu ? this.openMenu() : this.closeMenu();
                },

                setWidthWindow : function(width) {

                },

                setFilterState: function() {
                    this.stateFilter ? this.openFilter() : this.closeFilter();
                },

                setContentState: function() {
                    if (this.isContentMoveable) {
                        //this.stateContent ? this.openContent() : this.closeContent();
                        var padding = 0;// this.currentMenuWidth;
                        if (this.stateFilter) {
                            padding += this.filterWidth;

                        }
                        console.log(padding);
                        $('#content section.style-default-bright').animate({paddingLeft: padding + "px"}, 200);

                    }
                },

                initialDefaultState: function(mobile) {
                    if(mobile == null) {
                        this.isMobile = false;
                    } else {
                        if(this.isBigDisplay()) {
                            $('#content section.style-default-bright').addClass('mob-max-content');
                            $('#off-canvas1').addClass('mob-max-filter');
                        } else {
                            //$('#content section.style-default-bright').addClass('mob-min-content');
                            //$('#off-canvas1').addClass('mob-min-filter');
                        }


                        this.isMobile = true;
                    }

                    var state;

                    if(!this.isMobile){
                        if(this.isBigDisplay()) {
                            this.stateMenu = false;
                            this.stateFilter = true;
                            this.stateContent = true;
                            this.isContentMoveable =true;

                            state = 0;

                        } else {
                            this.stateMenu = false;
                            this.stateFilter = false;
                            this.stateContent = false;
                            this.isContentMoveable =false;

                            state = 1;

                        }
                    } else {
                        if(this.isBigDisplay()) {
                            this.stateMenu = false;
                            this.stateFilter = false;
                            this.stateContent = false;
                            this.isContentMoveable =false;

                            state = 2;

                        } else {
                            this.stateMenu = false;
                            this.stateFilter = false;
                            this.stateContent = false;
                            this.isContentMoveable =false;

                            state = 3;

                        }

                        if(state!=this.prevState) {
                            switch (state) {
                                case 0:

                                    break;
                                case 1:
                                    this.closeFilter();
                                    this.closeMenu();
                                    break;
                            };
                        }

                        console.log("prev state" + this.prevState);
                        console.log("new state " + state);

                        this.prevState = state;
                    }

                    if(!this.isMobile){
                        if(this.isBigDisplay()) {
                            this.currentMenuWidth =this.openMenuWidthPCBig;
                        } else {
                            this.currentMenuWidth = this.openMenuWidthPCSmall;
                        }
                    } else {
                        if(this.isBigDisplay()) {
                            this.currentMenuWidth =this.closeMenuWidthMobBig;
                        } else {
                            this.currentMenuWidth = this.openMenuWidthMobSmall;
                        }
                    }
                },

                isBigDisplay: function() {
                    return $(window).width() > 768;
                },

                openMenu:  function() {
                    this.currentMenuWidth =this.closeMenuWidthPCBig;
                    if(!this.isMobile){
                        if(this.isBigDisplay()) {
                            this.currentMenuWidth =this.openMenuWidthPCBig;
                        } else {
                            this.currentMenuWidth = this.openMenuWidthPCSmall;
                        }
                    } else {
                        if(this.isBigDisplay()) {
                            this.currentMenuWidth =this.closeMenuWidthMobBig;
                        } else {
                            this.currentMenuWidth = this.openMenuWidthMobSmall;
                        }
                    }

                    this.stateMenu =true;
                },

                closeMenu:  function() {
                    this.currentMenuWidth =this.openMenuWidthPCBig;
                    if(!this.isMobile){
                        if(this.isBigDisplay()) {
                            this.currentMenuWidth =this.closeMenuWidthPCBig;
                        } else {
                            this.currentMenuWidth =this.closeMenuWidthPCSmall;
                        }
                    } else {
                        if(this.isBigDisplay()) {
                            this.currentMenuWidth =this.closeMenuWidthMobBig;
                        } else {
                            this.currentMenuWidth =this.closeMenuWidthMobSmall;
                        }
                    }

                    this.stateMenu=false;
                    // animate
                },

                closeFilter: function() {
                    this.currentFilterLeft = 0;
                    this.currentFilterLeft = -320;
                    $('#off-canvas1').animate({left: this.currentFilterLeft + "px"}, 200);
                    this.stateFilter = false;
                },

                openFilter: function() {
                    this.currentFilterLeft =0;
                    if(!this.isMobile){
                        if(this.isBigDisplay()){
                            this.currentFilterLeft = this.currentMenuWidth;
                        } else {
                            this.currentFilterLeft = 0;
                        }
                    } else {
                        if(this.isBigDisplay()) {
                            this.currentFilterLeft = this.currentMenuWidth;
                        } else {
                            this.currentFilterLeft = this.closeMenuWidthMobSmall;
                        }
                    }

                    $('#off-canvas1').animate({left:this.currentFilterLeft+"px"},200);
                    this.stateFilter = true;
                },

                init: function() {
                    this.setStateMenu();
                    this.setFilterState();
                    this.setContentState();
                }
            };

            var isMobile = {
                Android: function() {
                    return navigator.userAgent.match(/Android/i);
                },
                BlackBerry: function() {
                    return navigator.userAgent.match(/BlackBerry/i);
                },
                iOS: function() {
                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                },
                Opera: function() {
                    return navigator.userAgent.match(/Opera Mini/i);
                },
                Windows: function() {
                    return navigator.userAgent.match(/IEMobile/i);
                },
                any: function() {
                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                }
            };

            StateManager.initialDefaultState(isMobile.any());

            $('#trigger').on('click', function(){
                StateManager.stateFilter = (!StateManager.stateFilter);
                StateManager.init();
            });

            $('a[data-toggle="menubar"]').on('click', function(){
                StateManager.stateMenu = (!StateManager.stateMenu);
                StateManager.init();
            });

            $(window).resize(function(){
                var windowSize = $(window).width();
                StateManager.width = windowSize;
                //StateManager.initialDefaultState(isMobile.any());
                //StateManager.init();
            });
        </script>

        <script type="text/javascript">
            function createChart() {
                $("#chart").kendoChart({
                    title: {
                        text: "The AIDA model"
                    },
                    legend: {
                        position: "top"
                    },
                    seriesDefaults: {
                        dynamicHeight: false,
                        labels: {
                            template: "#= category #",
                            visible: true,
                            font: "15px sans-serif",
                            align: "right",
                            position: "center",
                            background: "transparent",
                            color: "#000",
                            padding: 5,
                            border: {
                                width: 1,
                                dashType: "dot",
                                color: "#000"
                            },
                            format: "N0"
                        }
                    },
                    series: [{
                        type: "funnel",
                        data: [{
                            category: "Awareness",
                            value: 4
                        }, {
                            category: "Interest",
                            value: 3
                        }, {
                            category: "Desire",
                            value: 2
                        }, {
                            category: "Action",
                            value: 1
                        }]
                    }],
                    tooltip: {
                        visible: true,
                        template: "#= category # - #= kendo.format('{0:P}', percentage) #"
                    }
                });
            }

            $(document).ready(function () {
                createChart();
                $('#sizeSlider').kendoSlider({
                    change: refresh,
                    min: 1,
                    max: 40
                });
                $('#color').kendoColorPicker({ change: refresh, value: "#000", buttons: false });
                $(".box").on("change", ":checkbox,:radio", refresh);

                $(document).bind("kendo:skinChange", createChart);
            });

            function refresh() {
                var chart = $("#chart").data("kendoChart"),
                        slider = $('#sizeSlider').data("kendoSlider"),
                        colorPicker = $('#color').data("kendoColorPicker"),
                        showBorder = $('#showBorder').is(':checked'),
                        funnelSeries = chart.options.series[0],
                        labepOptions = funnelSeries.labels,
                        labels = $("#labels").prop("checked"),
                        alignInputs = $("input[name='alignType']"),
                        alignLabels = alignInputs.filter(":checked").val(),
                        positionInputs = $("input[name='positionType']"),
                        position = positionInputs.filter(":checked").val();

                var borderOptions = showBorder ? {
                    width: 1,
                    dashType: "dot",
                    color: "#000"
                } : {
                    width:0
                };

                var seriesOptions = {
                    dynamicHeight: false,
                    labels: {
                        template: "#= category #",
                        visible: labels,
                        font: slider.value() + "px sans-serif",
                        align: alignLabels,
                        position:position,
                        background: "transparent",
                        color: colorPicker.value(),
                        padding: 5,
                        border: borderOptions,
                        format: "N0"
                    }
                };

                $('#showBorder').attr("disabled", !labels);
                alignInputs.attr("disabled", !labels);
                positionInputs.attr("disabled",!labels);
                slider.enable(labels);
                colorPicker.enable(labels);

                chart.setOptions({
                    seriesDefaults: seriesOptions,
                    transitions : false
                })
            }
        </script>
    @stop