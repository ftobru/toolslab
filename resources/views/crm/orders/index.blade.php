
@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('switcher')
    <ul class="nav nav-tabs ">
        <li class="active"><a href="#">Сделки</a></li>
        <li><a href="/{{$company->name}}/crm/contacts">Теги</a></li>
    </ul>
@stop

@section('content')
    <div class="offcanvas"></div>
    <div class="card tab-content filter-page">
        <div class="filter-menu" id="off-canvas1">
            <nav>
                <div class="expanded">
                    <a href="#">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label no-margin">
                                <input type="text" class="form-control input-sm" style="margin-top: 8px;" id="product-description">
                                <label for="product-description">Описание:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group no-margin">
                                <select id="status-selector" class="form-control select2-list input-sm" data-placeholder="Выберите статус">
                                    <option>&nbsp;</option>
                                    <option value="all">Все статусы</option>
                                    @foreach ($statuses as $key=>$val)
                                        <option value="{{$val}}">{{$key}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group no-margin">
                                <select id="client-selector" class="form-control select2-list input-sm" data-placeholder="Выберите клиента">
                                    <option>&nbsp;</option>
                                    <option value="all">Все клиенты</option>
                                    @if ($clients)
                                        @foreach ($clients as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group no-margin">
                                <select id="firm-selector" class="form-control select2-list input-sm" data-placeholder="Выберите компанию">
                                    <option>&nbsp;</option>
                                    <option value="all">Все фирмы</option>
                                    @if ($firms)
                                        @foreach ($firms as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <select id="resp-user-selector" class="form-control select2-list input-sm" data-placeholder="Выберите ответственного">
                                    <option>&nbsp;</option>
                                    <option value="all">Все ответственные</option>
                                    @if ($users)
                                        @foreach ($users as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-end" name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
            <div class="section-body">
                <div class="table_wrap">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="card" id="droptarget-hidden-columns">
                            <div class="card-head">
                                <header>Скрытые столбцы</header>
                            </div>
                            <div class="card-body">
                                <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                    <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                </div>
                            </div>
                            <!--end .card-body -->
                        </div>
                        <div class="k-grid-container" id="grid"></div>
                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>
@stop

@section('modals')
@stop

@section('scripts')

    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>

    <script type="text/javascript">

        var GridConfig = {};
        var Filters = {};
        var FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 1;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_deals_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/crm/ajax/deals/';
        // Путь до карты
        GridConfig.baseCardUrl = '/' + '{{{$company->name}}}' + '/crm/card/order/';
        GridConfig.clientCardUrl = '/' + '{{{$company->name}}}' + '/crm/card/client/';
        GridConfig.firmCardUrl = '/' + '{{{$company->name}}}' + '/crm/card/firm/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'deals';
        // Можно ли редактировать
        GridConfig.editable = false;
        // Можно ли делать группировку
        GridConfig.groupable = true;
        // Шаблон копирования строк
        GridConfig.copyFields = ['status', 'client_id'];
        // Конфиг колонок грида
        GridConfig.columns = [
            {
                field: "id",
                title: "ID",
                width: "50px",
                locked: true,
                lockable: false,
                groupable: false
            },
            {
                field: "status",
                title: 'Статус',
                width: "130px",
                template: "#= cellTemplate(status, 'status') #",
                aggregates: ["count"],
                groupHeaderTemplate: "Статус: #= getStatusLabel(value) # (Всего: #= count#)"
            },
            {
                field: "description",
                title: "Описание",
                width: "130px",
                editor: "readonlyEditor",
                groupable: false
            },
            {
                field: "client",
                title: "Контакт",
                template: "#= cellTemplate(client, 'client') #",
                width: "130px",
                editor: "readonlyEditor",
                //aggregates: ["count"],
                groupHeaderTemplate: "Контакт: #= getClientLabel(value) # (Всего: #= count#)"
            },
            {
                field: "firm",
                title: "Компания",
                template: "#= cellTemplate(firm, 'firm') #",
                width: "130px",
                editor: "readonlyEditor",
                aggregates: ["count"],
                groupHeaderTemplate: "Компания: #= getFirmLabel(value) # (Всего: #= count#)"
            },
            {
                field: "resp-user",
                title: "Ответственный",
                template: "#= cellTemplate(responsible_user, 'resp-user') #",
                width: "130px",
                editor: "readonlyEditor",
                aggregates: ["count"],
                groupHeaderTemplate: "Ответственный: #= getRespUserLabel(value) # (Всего: #= count#)"
            },
            {
                field: "carts-price",
                title: "Стоимость, руб.",
                template: "#= cellTemplate(carts, 'carts_price') #",
                width: "130px",
                editor: "readonlyEditor",
                groupable: false
            },
            {
                field: "carts-qty",
                title: "Количество",
                template: "#= cellTemplate(carts, 'carts_qty') #",
                width: "130px",
                editor: "readonlyEditor",
                groupable: false
            },
            {
                field: "created_at",
                title: "Создан",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px",
                editor: "readonlyEditor",
                groupable: false
            },
            {
                field: "updated_at",
                title: "Изменён",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px",
                editor: "readonlyEditor",
                groupable: false
            }
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                status: {type: "number"},
                client_id: {type: "number"},
                firm_id: {type: "number"},
                client: {client_id: {editable: false}},
                firm: {firm_id: {editable: false}},
                carts: {editable: false},
                responsible_user: {responsible_user_id: {editable: false}},
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Конфиг аггрегатора группировок грида
        GridConfig.aggregateFields = [
            {field: "client_id", aggregate: "count", groupHeaderTemplate: "Контакт: #= getClientLabel(client) # (Всего: #= count#)"},
            {field: "client", aggregate: "count"},
            {field: "firm", aggregate: "count"},
            {field: "firm_id", aggregate: "count"},

            {field: "status", aggregate: "count"}
        ];
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'description',
            'client_id',
            'firm_id',
            'resp_user_id',
            'status',
            'date_start',
            'date_end'
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'description',
            'client_id',
            'firm_id',
            'resp_user_id',
            'status',
            'date_start',
            'date_end'
        ];

        // Форматирует вывод статусов и типов в гриде.
        function cellTemplate(value, type) {
            var x;
            switch (type) {
                case 'resp-user':
                    if (value) {
                        return '<a href="#">' + value.name + '</a>';
                    }
                    return;
                    break;

                case 'carts_qty':
                    try {
                        var qty = 0;
                        $.each(value, function (key, cart) {
                            var content = JSON.parse(cart.content);
                            qty+= content.qty;
                        });
                        return qty;
                    } catch (err) {}
                    return "-";
                    break;
                case 'carts_price':
                    try {
                        var price = 0.00;
                        $.each(value, function (key, cart) {
                            var content = JSON.parse(cart.content);
                            price+= content.summary_price;
                        });
                        return price;
                    } catch (err) {}
                    return "-";
                    break;
                case 'firm':
                    if(value) {
                        return '<a href="' + GridConfig.firmCardUrl + value.id + '">' + value.name + '</a>';
                    }
                    return;
                    break;
                case 'client':
                    if (value) {
                        return '<a href="' + GridConfig.clientCardUrl + value.id + '">' + value.name + '</a>';
                    }
                    return;
                    break;
                case 'status':
                    x = $.grep(Filters.statusesDataSource, function (n, i) {
                        return n.value == value;
                    });
                    if (x.length > 0) {
                        return x[0].text;
                    }
                    break;
            }
            return '[unknown]';
        }

        // Возвращает в шаблон группировок значение группировки
        function getStatusLabel(value) {

            var x = $.grep(Filters.statusesDataSource, function (item) {
                return item.value == value;
            });
            return x[0].text;
        }

        function getClientLabel(value) {
            return "123";
        }

        function getFirmLabel(value) {
            return "123";
        }

        function getRespUserLabel(value) {
            return "123";
        }



        $(document).ready(function () {

            Filters = {
                isInit: false,

                // Наши слайдеры.
                profit: null,
                old_price: null,
                new_price: null,

                // Наши селекторы
                statusSelector: null,
                firmSelector:null,
                clientSelector: null,
                respUserSelector: null,

                // Селекторы дат
                date_start: null,
                date_end: null,

                // Допустимые значения dropdownlist
                statusesDataSource: {},

                // Массивы диапазонов слайдеров.
                limits: null,

                initFilters: function () {
                    // Наши слайдеры )
                    this.profit = $("#slider-range-profit");
                    this.old_price = $("#slider-range-old-price");
                    this.new_price = $("#slider-range-price");

                    //Селекторы
                    this.statusSelector = $('#status-selector');
                    this.firmSelector = $('#firm-selector');
                    this.clientSelector = $('#client-selector');
                    this.respUserSelector = $('#resp-user-selector');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');


                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    $('#product-description').keyup(function () {
                        FilterBuilder.addFilter('description', {
                            field: "description",
                            operator: "startWith",
                            value: $('#product-description').val()
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });

                    // Селектор статуса
                    this.statusSelector.select2({
                        minimumResultsForSearch: -1,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('status');
                                } else {
                                    FilterBuilder.addFilter('status', {
                                        field: "status",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    // Селектор firms
                    this.firmSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('firm_id');
                                } else {
                                    FilterBuilder.addFilter('firm_id', {
                                        field: "firm_id",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });
                    // Селектор client
                    this.clientSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('client_id');
                                } else {
                                    FilterBuilder.addFilter('client_id', {
                                        field: "client_id",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });
                    this.respUserSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('resp_user_id');
                                } else {
                                    FilterBuilder.addFilter('resp_user_id', {
                                        field: "resp_user_id",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                    //console.log('initSlidersAmount');
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },

                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;
                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "status" && value.operator == 'eq') {
                        $("#status-selector").select2("val", value.value);
                        FilterBuilder.addFilter('status', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                    if (value.field == "description" && value.operator == 'startWith') {
                        $('#product-description').val(value.value);
                        FilterBuilder.addFilter('description', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                //Устанавливает значения фильтров-слайдеров по массиву данных
                initSlidersValues: function (data) {
                    // usage: this.profit.slider("option", "values", [parseInt(data.profit.min), parseInt(data.profit.max)]);
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                    var self = this;
                    var option;
                    this.statusesDataSource = [];
                    $.each(this.limits.statuses, function (index, value) {
                        option = {text: index, value: value};
                        self.statusesDataSource.push(option);
                    });
                }

            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            };

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });
    </script>

@stop