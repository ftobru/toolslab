<!-- Карточка просроченной задачи-->
<script id="template-card-overdue-task" type="text/x-handlers-template">
    <div class="client-card card alert alert-callout alert-danger style-danger-light collapsed">
        <div class="card-head card-head-sm collapsed" data-toggle="collapse"
             data-target="#accordion-@{{value.id}}">
            <header>
                <span><i class="md md-insert-invitation"></i></span>
                                            <span>@{{value.head_date}} <span
                                                        class="opacity-75 text-sm">@{{ value.formatted_date }}</span></span>
                Исполнитель:<a href="#"
                               class="text-primary">@{{value.performer_user.name}} @{{value.performer_user.last_name}}</a>
            </header>
            <div class="tools">
                <div class="btn-group">
                    <a onclick="completeTask(this)" data-en-id="@{{value.id}}" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Выполнить"><i class="md md-done-all"></i></a>
                    <a onclick="editTask(this)" class="btn btn-icon-toggle" data-toggle="tooltip"
                       data-en-id="@{{value.id}}"
                       data-placement="auto"
                       title="Редактирование"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div>
        <div class="collapse in" id="accordion-@{{value.id}}">
            <div class="card-body">
                <p>Ответственный:<a href="#" class="text-primary">@{{value.responsible_user.name}} @{{value.responsible_user.last_name}}</a></p>
                <p>Комментарий: @{{value.comment}}</p>
            </div>
        </div>
    </div>
</script>
<!-- Карточка текущей задачи-->
<script id="template-card-active-task" type="text/x-handlers-template">
    <div class="client-card card alert alert-callout alert-danger">
        <div class="card-head card-head-sm collapsed" data-toggle="collapse"
             data-target="#accordion-@{{value.id}}">
            <header>
                <span><i class="md md-insert-invitation"></i></span>
                                            <span>@{{value.head_date}} <span
                                                        class="opacity-75 text-sm">@{{ value.formatted_date }}</span></span>
                Исполнитель:<a href="#"
                               class="text-primary">@{{value.performer_user.name}} @{{value.performer_user.last_name}}</a>
            </header>
            <div class="tools">
                <div class="btn-group">
                    <a onclick="completeTask(this)" data-en-id="@{{value.id}}" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Выполнить"><i class="md md-done-all"></i></a>
                    <a onclick="editTask(this)" class="btn btn-icon-toggle" data-toggle="tooltip"
                       data-en-id="@{{value.id}}"
                       data-placement="auto"
                       title="Редактирование"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div>
        <div class="collapse in" id="accordion-@{{value.id}}">
            <div class="card-body">
                <p>Ответственный:<a href="#" class="text-primary">@{{value.responsible_user.name}} @{{value.responsible_user.last_name}}</a></p>
                <p>Комментарий: @{{value.comment}}</p>
            </div>
        </div>
    </div>
</script>
<!-- Карточка закрытой задачи-->
<script id="template-card-completed-task" type="text/x-handlers-template">
    <div class="client-card card alert alert-callout alert-danger style-primary-bright">
        <div class="card-head card-head-sm collapsed" data-toggle="collapse"
             data-target="#accordion-@{{value.id}}">
            <header>
                <span><i class="md md-insert-invitation"></i></span>
                                            <span>@{{value.head_date}} <span
                                                        class="opacity-75 text-sm">@{{ value.formatted_date }}</span></span>
                Исполнитель:<a href="#"
                               class="text-primary">@{{value.performer_user.name}} @{{value.performer_user.last_name}}</a>
            </header>
            <div class="tools">
                <div class="btn-group">
                    <a onclick="editTask(this)"
                       class="btn btn-icon-toggle" data-toggle="tooltip"
                       data-en-id="@{{value.id}}"
                       data-placement="auto"
                       title="Редактирование"><i class="fa fa-pencil"></i></a>
                    <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                </div>
            </div>
        </div>
        <div class="collapse in" id="accordion-@{{value.id}}">
            <div class="card-body">
                <p>Ответственный:<a href="#" class="text-primary">@{{value.responsible_user.name}} @{{value.responsible_user.last_name}}</a></p>
                <p>Комментарий: @{{value.comment}}</p>
            </div>
        </div>
    </div>
</script>

