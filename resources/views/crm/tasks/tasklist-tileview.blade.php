@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_add_simple')
@stop

@section('switcher')
    <ul class="nav nav-tabs ">
        <li class="active"><a href="/{{$company->name}}/crm/tasks">Таблица</a></li>
        <li><a href="#">Мои задачи</a></li>
    </ul>
@stop

@section('content')
    <div class="card-body tab-content filter-page">

        <div class="filter-menu" id="off-canvas1">
            <nav>
                <div class="expanded">
                    <a href="#">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label no-margin">
                                <input type="text" class="form-control input-sm" style="margin-top: 8px;"
                                       id="product-description">
                                <label for="product-description">Комментарий:</label>
                            </div>
                        </li>
                        <li>
                         {{--   <div class="form-group no-margin">
                                <select id="state-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите состояние">
                                    <option>&nbsp;</option>
                                    <option value="all">Все состояния</option>
                                    @foreach ($states as $key=>$val)
                                        <option value="{{$val}}">{{$key}}</option>
                                    @endforeach
                                </select>
                            </div>--}}

                            <div class="form-group no-margin">
                                <select id="client-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите контакт">
                                    <option>&nbsp;</option>
                                    <option value="all">Все контакты</option>
                                    @if ($clients)
                                        @foreach ($clients as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group no-margin">
                                <select id="firm-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите компанию">
                                    <option>&nbsp;</option>
                                    <option value="all">Все компании</option>
                                    @if ($firms)
                                        @foreach ($firms as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                            <div class="form-group no-margin">
                                <select id="resp-user-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите ответственного">
                                    <option>&nbsp;</option>
                                    <option value="all">Все ответственные</option>
                                    @if ($users)
                                        @foreach ($users as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}} {{$val->last_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>

                       {{--     <div class="form-group">
                                <select id="perf-user-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите исполнителя">
                                    <option>&nbsp;</option>
                                    <option value="all">Все исполнители</option>
                                    @if ($users)
                                        @foreach ($users as $key=>$val)
                                            <option value="{{$val->id}}">{{$val->name}} {{$val->last_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>--}}
                        </li>

                        <li>
                            <div class="form-group">
                                <div id="deadline-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="deadline-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать срок</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm"
                                                       id="deadline-date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="deadline-date-end"
                                                       name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-start"
                                                       name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-end"
                                                       name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section class="style-default-bright client-info tab-pane active" id="first9">
            <div class="section-body no-margin">
                <div class="table_wrap">
                    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-1">
                        <span class="text-primary text-lg">Просроченные задачи (<span id="overdue-counter">{{{sizeof($tasks['overdue'])}}}</span>)</span>
									<span>
										<a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i
                                                    class="md md-keyboard-arrow-down"></i></a>
									</span>
                    </div>
                    <div id="accordion2-1" class="collapse in">
                        @if(isset($tasks['overdue']) && !empty($tasks['overdue']))
                            @foreach($tasks['overdue'] as $overdue_task)
                                <div class="client-card card alert alert-callout alert-danger style-danger-light collapsed"
                                     data-toggle="collapse" data-target="#accordion-{{{$overdue_task['id']}}}">
                                    <div class="card-head card-head-sm">
                                        <header>
                                            <span><i class="md md-insert-invitation"></i></span>
                                            <span>{{{$overdue_task['head_date']}}} <span
                                                        class="opacity-75 text-sm"><span>{{ date("d.m.Y",strtotime($overdue_task['date_perf'])) }}</span> в <span>{{ date("G:i",strtotime($overdue_task['date_perf'])) }}</span></span></span>
                                            Исполнитель:<a href="#" class="text-primary">{{{$overdue_task['performer_user']['name']}}} {{{$overdue_task['performer_user']['last_name']}}}</a>
                                        </header>
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a onclick="completeTask(this)" data-en-id="{{{$overdue_task['id']}}}" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Выполнить"><i class="md md-done-all"></i></a>
                                                <a onclick="editTask(this)" data-en-id="{{{$overdue_task['id']}}}" title="" data-placement="auto" data-toggle="tooltip" class="btn btn-icon-toggle" data-original-title="Редактирование"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="accordion-{{{$overdue_task['id']}}}">
                                        <div class="card-body">
                                            <p>Ответственный:<a href="#" class="text-primary">{{{$overdue_task['responsible_user']['name']}}} {{{$overdue_task['responsible_user']['last_name']}}}</a></p>
                                            <p>Комментарий: {{{$overdue_task['comment']}}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-2">
                        <span class="text-primary text-lg">Ативные задачи (<span id="active-counter">{{{sizeof($tasks['active'])}}}</span>)</span>
									<span>
										<a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i
                                                    class="md md-keyboard-arrow-down"></i></a>
									</span>
                    </div>
                    <div id="accordion2-2" class="collapse in">
                        @if(isset($tasks['active']) && !empty($tasks['active']))
                            @foreach($tasks['active'] as $active_task)
                                <div class="client-card card alert alert-callout alert-danger">
                                    <div class="card-head card-head-sm collapsed" data-toggle="collapse"
                                         data-target="#accordion-{{{$active_task['id']}}}">
                                        <header>
                                            <span><i class="md md-insert-invitation"></i></span>
                                            <span>{{{$active_task['head_date']}}} <span
                                                        class="opacity-75 text-sm"><span>{{ date("d.m.Y",strtotime($active_task['date_perf'])) }}</span> в <span>{{ date("G:i",strtotime($active_task['date_perf'])) }}</span></span></span>
                                            Исполнитель:<a href="#" class="text-primary">{{{$active_task['performer_user']['name']}}} {{{$active_task['performer_user']['last_name']}}}</a>
                                        </header>
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a onclick="completeTask(this)" data-en-id="{{{$active_task['id']}}}" class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Выполнить"><i class="md md-done-all"></i></a>
                                                <a onclick="editTask(this)" data-en-id="{{{$active_task['id']}}}" title="" data-placement="auto" data-toggle="tooltip" class="btn btn-icon-toggle" data-original-title="Редактирование"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="accordion-{{{$active_task['id']}}}">
                                        <div class="card-body">
                                            <p>Ответственный:<a href="#" class="text-primary">{{{$active_task['responsible_user']['name']}}} {{{$active_task['responsible_user']['last_name']}}}</a></p>
                                            <p>Комментарий: {{{$active_task['comment']}}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    <div class="card-head collapsed" data-toggle="collapse" data-target="#accordion2-3">
                        <span class="text-primary text-lg">Завершенные задачи (<span
                                    id="completed-counter">{{{sizeof($tasks['completed'])}}}</span>)</span>
									<span>
										<a href="#" class="text-primary btn btn-icon-toggle ink-reaction"><i
                                                    class="md md-keyboard-arrow-down"></i></a>
									</span>
                    </div>
                    <div id="accordion2-3" class="collapse in">
                        @if(isset($tasks['completed']) && !empty($tasks['completed']))
                            @foreach($tasks['completed'] as $completed_task)
                                <div class="client-card card alert alert-callout alert-danger style-primary-bright">
                                    <div class="card-head card-head-sm collapsed" data-toggle="collapse"
                                         data-target="#accordion-{{{$completed_task['id']}}}">
                                        <header>
                                            <span><i class="md md-insert-invitation"></i></span>
                                            <span>{{{$overdue_task['head_date']}}} <span
                                                        class="opacity-75 text-sm"><span>{{ date("d.m.Y",strtotime($completed_task['updated_at'])) }}</span> в <span>{{ date("G:i",strtotime($completed_task['updated_at'])) }}</span></span></span>
                                            Исполнитель:<a href="#" class="text-primary">{{{$completed_task['performer_user']['name']}}} {{{$completed_task['performer_user']['last_name']}}}</a>
                                        </header>
                                        <div class="tools">
                                            <div class="btn-group">
                                                <a onclick="editTask(this)"
                                                   class="btn btn-icon-toggle" data-toggle="tooltip"
                                                   data-en-id="{{{$completed_task['id']}}}"
                                                   data-placement="auto"
                                                   title="Редактирование"><i class="fa fa-pencil"></i></a>
                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collapse in" id="accordion-{{{$completed_task['id']}}}">
                                        <div class="card-body">
                                            <p>Ответственный:<a href="#" class="text-primary">{{{$completed_task['responsible_user']['name']}}} {{{$completed_task['responsible_user']['last_name']}}}</a></p>
                                            <p>Комментарий: {{{$completed_task['comment']}}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <!--end .section-body -->
        </section>
        <section class="style-default-bright tab-pane client-info" id="second9">
            <div class="section-body no-margin">
                <div class="table_wrap">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="datatable1" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Age</th>
                                        <th>Nickname</th>
                                        <th>Employee</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="gradeA">
                                        <td>Lera</td>
                                        <td>8</td>
                                        <td>Lera</td>
                                        <td>Lera</td>
                                    </tr>
                                    <tr class="gradeA">
                                        <td>Lesha</td>
                                        <td>66</td>
                                        <td>lesha_L</td>
                                        <td>Lera</td>
                                    </tr>
                                    <tr class="gradeX">
                                        <td>Yan</td>
                                        <td>5</td>
                                        <td>Yan_yan</td>
                                        <td>Lera</td>
                                    </tr>
                                    <tr class="gradeC">
                                        <td>Nikita</td>
                                        <td>9</td>
                                        <td>Nikita_n</td>
                                        <td>Lera</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!--end .table-responsive -->
                        </div>
                        <!--end .col -->
                    </div>
                    <!--end .row -->
                    <hr class="ruler-xxl"/>
                </div>
            </div>
            <!--end .section-body -->
        </section>
    </div>
@stop

@section('modals')
    <div class="offcanvas create-company">
        <div id="offcanvas-edit-task" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <form id="edit-task-form" class="form">
                    <span class="text-lg text-bold text-primary">Создание задачи</span>
                    <div class="form-group floating-label">
                        <textarea name="comment" type="text" class="form-control input-sm" style="margin-top: 8px;"></textarea>
                        <label for="comment">Комментарий к задаче:</label>
                    </div>
                    <div class="form-group">
                        <select name="order_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите заказ">
                            <option>&nbsp;</option>
                            @foreach ($newOrders as $key=>$val)
                                <option value="{{$newOrders[$key]->id}}">
                                    ID:{{$newOrders[$key]->id}} {{$newOrders[$key]->description}}
                                </option>
                            @endforeach
                        </select>
                        <label for="type">Заказ:</label>
                    </div>
                    <div class="form-group">
                        <select name="state" class="form-control select2-list input-sm"
                                data-placeholder="Выберите состояние">
                            <option>&nbsp;</option>
                            @foreach ($states as $key=>$val)
                                <option value="{{$val}}">{{$key}}</option>
                            @endforeach
                        </select>
                        <label for="state">Состояние:</label>
                    </div>
                    <div class="form-group">
                        <select name="type" class="form-control select2-list input-sm"
                                data-placeholder="Выберите тип">
                            <option>&nbsp;</option>
                            @foreach ($types as $key=>$val)
                                <option value="{{$val}}">{{$key}}</option>
                            @endforeach
                        </select>
                        <label for="type">Тип:</label>
                    </div>
              {{--      <div class="form-group">
                        <select name="client_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите контакт">
                            <option>&nbsp;</option>
                            @if ($clients)
                                @foreach ($clients as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="client_id">Контакт:</label>
                    </div>
                    <div class="form-group">
                        <select name="firm_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите компанию">
                            <option>&nbsp;</option>
                            @if ($firms)
                                @foreach ($firms as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="firm_id">Компания:</label>
                    </div>--}}
                    <div class="form-group">
                        <select name="responsible_user_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите ответственного">
                            <option>&nbsp;</option>
                            @if ($users)
                                @foreach ($users as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}} {{$val->last_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="responsible_user_id">Ответственный:</label>
                    </div>
                    <div class="form-group">
                        <select name="performer_user_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите исполнителя">
                            <option>&nbsp;</option>
                            @if ($users)
                                @foreach ($users as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}} {{$val->last_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="performer_user_id">Исполнитель:</label>
                    </div>
                    <div class="form-group">
                        <div id="deadline-date-range" class="input-daterange">
                            <div class="input-daterange input-group" id="deadline-date-range">
                                <label for="#">
                                    <span class="opacity-50">Выбрать срок:</span>
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm" name="date_perf">
                                        <div class="form-control-line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <button type="button" onclick="closeCreateTaskView()" class="btn btn-default-light ink-reaction btn-sm">Отмена</button>
                            <button type="button" onclick="submitEditTask(this)" class="btn ink-reaction btn-primary btn-sm">Сохранить</button>
                            <button type="button" onclick="submitDeleteTask(this)" class="btn ink-reaction btn-primary btn-sm">Удалить</button>
                        </div>
                    </div>
                    <input type="hidden" name="id">
                </form>
            </div>
        </div>
        <div id="offcanvas-new-task" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <form id="create-task-form" class="form">
                    <span class="text-lg text-bold text-primary">Создание задачи</span>
                    <div class="form-group floating-label">
                        <textarea name="comment" type="text" class="form-control input-sm" style="margin-top: 8px;"></textarea>
                        <label for="comment">Комментарий к задаче:</label>
                    </div>
                    <div class="form-group">
                        <select name="order_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите заказ">
                            <option>&nbsp;</option>
                            @foreach ($newOrders as $key=>$val)
                                <option value="{{$newOrders[$key]->id}}">
                                    ID:{{$newOrders[$key]->id}} {{$newOrders[$key]->description}}
                                </option>
                            @endforeach
                        </select>
                        <label for="type">Заказ:</label>
                    </div>
                    <div class="form-group">
                        <select name="state" class="form-control select2-list input-sm"
                                data-placeholder="Выберите состояние">
                            <option>&nbsp;</option>
                            @foreach ($states as $key=>$val)
                                <option value="{{$val}}">{{$key}}</option>
                            @endforeach
                        </select>
                        <label for="state">Состояние:</label>
                    </div>
                    <div class="form-group">
                        <select name="type" class="form-control select2-list input-sm"
                                data-placeholder="Выберите тип">
                            <option>&nbsp;</option>
                            @foreach ($types as $key=>$val)
                                <option value="{{$val}}">{{$key}}</option>
                            @endforeach
                        </select>
                        <label for="type">Тип:</label>
                    </div>
                    {{--      <div class="form-group">
                              <select name="client_id" class="form-control select2-list input-sm"
                                      data-placeholder="Выберите контакт">
                                  <option>&nbsp;</option>
                                  @if ($clients)
                                      @foreach ($clients as $key=>$val)
                                          <option value="{{$val->id}}">{{$val->name}}</option>
                                      @endforeach
                                  @endif
                              </select>
                              <label for="client_id">Контакт:</label>
                          </div>
                          <div class="form-group">
                              <select name="firm_id" class="form-control select2-list input-sm"
                                      data-placeholder="Выберите компанию">
                                  <option>&nbsp;</option>
                                  @if ($firms)
                                      @foreach ($firms as $key=>$val)
                                          <option value="{{$val->id}}">{{$val->name}}</option>
                                      @endforeach
                                  @endif
                              </select>
                              <label for="firm_id">Компания:</label>
                          </div>--}}
                    <div class="form-group">
                        <select name="responsible_user_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите ответственного">
                            <option>&nbsp;</option>
                            @if ($users)
                                @foreach ($users as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}} {{$val->last_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="responsible_user_id">Ответственный:</label>
                    </div>
                    <div class="form-group">
                        <select name="performer_user_id" class="form-control select2-list input-sm"
                                data-placeholder="Выберите исполнителя">
                            <option>&nbsp;</option>
                            @if ($users)
                                @foreach ($users as $key=>$val)
                                    <option value="{{$val->id}}">{{$val->name}} {{$val->last_name}}</option>
                                @endforeach
                            @endif
                        </select>
                        <label for="performer_user_id">Исполнитель:</label>
                    </div>
                    <div class="form-group">
                        <div id="deadline-date-range" class="input-daterange">
                            <div class="input-daterange input-group" id="deadline-date-range">
                                <label for="#">
                                    <span class="opacity-50">Выбрать срок:</span>
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm" name="date_perf">
                                        <div class="form-control-line"></div>
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <button type="button" onclick="closeCreateTaskView()" class="btn btn-default-light ink-reaction btn-sm">Отмена</button>
                            <button type="button" onclick="submitTaskCreation(this)" class="btn ink-reaction btn-primary btn-sm">Добавить</button>
                        </div>
                    </div>
                    {{-- <div class="text-center">
                         <button type="submit" class="btn ink-reaction btn-warning btn-lg"><i class="md md-done"></i> Завершить задачу</button>
                     </div>--}}
                </form>
            </div>
        </div>
        <div id="offcanvas-complete-task" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <form id="complete-task-form" class="form">
                    <span class="text-lg text-bold text-primary">Завершение задачи</span>
                    <div class="form-group floating-label">
                        <textarea name="final_comment" type="text" class="form-control input-sm" style="margin-top: 8px;"></textarea>
                        <label for="final_comment">Комментарий по завершению:</label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <button type="button" onclick="closeCreateTaskView()" class="btn btn-default-light ink-reaction btn-sm">Отмена</button>
                            <button type="button" onclick="submitTaskComplete(this)" class="btn ink-reaction btn-primary btn-sm">Завершить</button>
                        </div>
                    </div>
                    <input type="hidden" name="id">
                </form>
            </div>
        </div>
    </div>
@stop

@section('scripts')

    <script type="application/javascript" src="/js/handlebars.min.js"></script>

    @include('crm.tasks.partials.templates-tasks')

    <script type="text/javascript">

        function submitTaskCreation(button) {
            // deactivate btn
            button.disabled = true;
            button.value = 'Отправка...';
            // check valid
            if ($('#create-task-form').valid()) {
                // send ajax
                var data = $('#create-task-form').serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});
                $.ajax({
                    url: "/{{{$company->name}}}/crm/ajax/tasks/create",
                    method: "POST",
                    data: data,
                    success: function (result) {
                        toastr.success("Задача успешно добавлена");
                        button.disabled = false;
                        button.value = 'Добавить';
                        closeCreateTaskView();
                        FilterBuilder.applyFilters();
                    },
                    error: function (result) {
                        toastr.error(result.responseText);
                        button.disabled = false;
                        button.value = 'Добавить';
                    }
                });

            } else {
                button.disabled = false;
                button.value = 'Добавить';
            }
        }

        function submitTaskComplete(button) {
            // deactivate btn
            button.disabled = true;
            button.value = 'Отправка...';
            // check valid
            if ($('#complete-task-form').valid()) {
                // send ajax
                var data = $('#complete-task-form').serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});
                $.ajax({
                    url: "/{{{$company->name}}}/crm/ajax/tasks/complete",
                    method: "POST",
                    data: data,
                    success: function (result) {
                        toastr.success("Статус задачи успешно обновлен");
                        button.disabled = false;
                        button.value = 'Завершить';
                        closeCompleteTaskView();
                        FilterBuilder.applyFilters();
                    },
                    error: function (result) {
                        toastr.error(result.responseText);
                        button.disabled = false;
                        button.value = 'Завершить';
                    }
                });

            } else {
                button.disabled = false;
                button.value = 'Завершить';
            }
        }

        function submitEditTask(button) {
            // deactivate btn
            button.disabled = true;
            button.value = 'Отправка...';
            // check valid
            if ($('#edit-task-form').valid()) {
                // send ajax
                var data = $('#edit-task-form').serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});
                $.ajax({
                    url: "/{{{$company->name}}}/crm/ajax/tasks/update",
                    method: "PUT",
                    data: data,
                    success: function (result) {
                        toastr.success("Задача успешно обновлена");
                        button.disabled = false;
                        button.value = 'Сохранить';
                        closeCreateTaskView();
                        FilterBuilder.applyFilters();
                    },
                    error: function (result) {
                        toastr.error(result.responseText);
                        button.disabled = false;
                        button.value = 'Сохранить';
                    }
                });

            } else {
                button.disabled = false;
                button.value = 'Сохранить';
            }
        }

        function submitDeleteTask(button) {
            // deactivate btn
            button.disabled = true;
            button.value = 'Отправка...';
            // check valid
            if ($('#edit-task-form').valid()) {
                // send ajax
                var data = $('#edit-task-form').serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});
                $.ajax({
                    url: "/{{{$company->name}}}/crm/ajax/tasks/delete",
                    method: "POST",
                    data: data,
                    success: function (result) {
                        toastr.success("Задача успешно удалена");
                        button.disabled = false;
                        button.value = 'Удалить';
                        closeCreateTaskView();
                        FilterBuilder.applyFilters();
                    },
                    error: function (result) {
                        toastr.error(result.responseText);
                        button.disabled = false;
                        button.value = 'Удалить';
                    }
                });

            } else {
                button.disabled = false;
                button.value = 'Удалить';
            }
        }

        function closeCreateTaskView() {
            window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-new-task');
        }

        function closeCompleteTaskView() {
            window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-complete-task');
        }

        function editTask(btn) {
            var id = $(btn).data('enId');
            $('#edit-task-form [name=id]').val(parseInt(id));
            var data = {id: parseInt(id)};
            $.ajax({
                url: "/{{{$company->name}}}/crm/ajax/tasks/getByID",
                method: "POST",
                data: data,
                success: function (result) {
                    $('#edit-task-form [name=comment]').val(result.task.comment).change();
                    $('#edit-task-form [name=performer_user_id]').select2("val", result.task.performer_user_id);
                    $('#edit-task-form [name=responsible_user_id]').select2("val", result.task.responsible_user_id);
                    $('#edit-task-form [name=order_id]').select2("val", result.task.order_id);
                    $('#edit-task-form [name=type]').select2("val", result.task.type);
                    $('#edit-task-form [name=state]').select2("val", result.task.state);
                    $('#edit-task-form [name=date_perf]').datepicker("update", new Date(result.task.date_perf));
                    window.materialadmin.AppOffcanvas.openOffcanvas('#offcanvas-edit-task');
                },
                error: function (result) {
                    toastr.error(result.responseText);
                }
            });

        }

        function completeTask(btn) {
            var id = parseInt($(btn).data('enId'));
            $('#offcanvas-complete-task [name=id]').val(id);
            $('#offcanvas-complete-task [name=final_comment]').val("");
            window.materialadmin.AppOffcanvas.openOffcanvas('#offcanvas-complete-task');
        }

        function RedrawTiles(tasks) {
            var overdueSize = tasks.tasks.overdue.length;
            var activeSize = tasks.tasks.active.length;
            var completedSize = tasks.tasks.completed.length;

            $('#overdue-counter').text(overdueSize);
            $('#active-counter').text(activeSize);
            $('#completed-counter').text(completedSize);

            $('#accordion2-1').empty();
            $('#accordion2-2').empty();
            $('#accordion2-3').empty();

            $.each(tasks.tasks.overdue, function(key,value){
                var source   = $("#template-card-overdue-task").html();
                var template = Handlebars.compile(source);
                $('#accordion2-1').append(template({value:value}));
            });

            $.each(tasks.tasks.active, function(key,value){
                var source   = $("#template-card-active-task").html();
                var template = Handlebars.compile(source);
                $('#accordion2-2').append(template({value:value}));
            });

            $.each(tasks.tasks.completed, function(key,value){
                var source   = $("#template-card-completed-task").html();
                var template = Handlebars.compile(source);
                $('#accordion2-3').append(template({value:value}));
            });
        }

        function InitCreateForm() {
            $('#create-task-form [name=date_perf]').datepicker({
                locale: 'ru'
            });
            //$('#create-task-form [name=client_id],#create-task-form [name=firm_id]').select2({});
            $('#create-task-form [name=state]').select2({});
            $('#create-task-form [name=type]').select2({});
            $('#create-task-form [name=order_id]').select2({});
            $('#create-task-form [name=responsible_user_id]').select2({});
            $('#create-task-form [name=performer_user_id]').select2({});

            $('#edit-task-form [name=date_perf]').datepicker({
                locale: 'ru'
            });
            $('#edit-task-form [name=state]').select2({});
            $('#edit-task-form [name=type]').select2({});
            $('#edit-task-form [name=order_id]').select2({});
            $('#edit-task-form [name=responsible_user_id]').select2({});
            $('#edit-task-form [name=performer_user_id]').select2({});
        }

        var TileGridConfig = {};
        var Filters = {};
        var FilterBuilder = {};

        TileGridConfig.baseAjaxPath = '/' + '{{{$company->name}}}' + '/crm/ajax/tasks/';

        // Ключи фильтров для FilterBuilder
        TileGridConfig.filter_keys = [
            'client_id',
            'firm_id',
            'order_id',
            'resp_user_id',
            'perf_user_id',
            'state',
            'type',
            'date_perf_start',
            'date_perf_stop',
            'date_start',
            'date_end',
            'comment'
        ];
        // Ключи допустимых фильтров в хеше
        TileGridConfig.filter_keys_hash = [
            'client_id',
            'firm_id',
            'order_id',
            'resp_user_id',
            'perf_user_id',
            'state',
            'type',
            'date_perf_start',
            'date_perf_stop',
            'date_start',
            'date_end',
            'comment'
        ];

        $(document).ready(function () {
            //init create btn
            InitCreateForm();
            $('#header_add_simple_btn').click(function (e) {
                if($('#offcanvas-new-task').hasClass('active')){
                    window.materialadmin.AppOffcanvas.closeOffcanvas('#offcanvas-new-task');
                } else {
                    window.materialadmin.AppOffcanvas.openOffcanvas('#offcanvas-new-task');
                }
            });

            // validation of create form
            $('#create-task-form').validate({
                rules: {
                    "order_id": {
                        required: true
                    },
                    "state": {
                        required: true
                    },
                    "performer_user_id": {
                        required: true
                    },
                    "responsible_user_id": {
                        required: true
                    }

                },
                messages: {
                    "order_id": {
                        required: "Укажите заказ."
                    },
                    "state": {
                        required: "Укажите состояние задачи."
                    },
                    "performer_user_id": {
                        required: "Выберите ответственного по задаче"
                    },
                    "responsible_user_id": {
                        required: "Назначьте задачу пользователю"
                    }
                },
                submitHandler: function () {

                }
            });
            // validation of edit form
            $('#edit-task-form').validate({
                rules: {
                    "order_id": {
                        required: true
                    },
                    "state": {
                        required: true
                    },
                    "performer_user_id": {
                        required: true
                    },
                    "responsible_user_id": {
                        required: true
                    }

                },
                messages: {
                    "order_id": {
                        required: "Укажите заказ."
                    },
                    "state": {
                        required: "Укажите состояние задачи."
                    },
                    "performer_user_id": {
                        required: "Выберите ответственного по задаче"
                    },
                    "responsible_user_id": {
                        required: "Назначьте задачу пользователю"
                    }
                },
                submitHandler: function () {

                }
            });
            // validation form of complete form
            $('#complete-task-form').validate({
                rules: {
                    "final_comment": {
                        required: true
                    }
                },
                messages: {
                    "final_comment": {
                        required: "прокомментируйте результат задачи"
                    }
                }
            });

            Filters = {
                isInit: false,

                // Наши селекторы
                stateSelector: null,
                firmSelector: null,
                clientSelector: null,
                respUserSelector: null,
                perfUserSelector: null,

                // Селекторы дат
                date_start: null,
                date_end: null,
                deadline_date_start: null,
                deadline_date_end: null,

                // Допустимые значения dropdownlist
                statesDataSource: {},

                // Массивы диапазонов слайдеров.
                limits: null,

                initFilters: function () {

                    //Селекторы
                    this.stateSelector = $('#state-selector');
                    this.firmSelector = $('#firm-selector');
                    this.clientSelector = $('#client-selector');
                    this.respUserSelector = $('#resp-user-selector');
                    this.perfUserSelector = $('#perf-user-selector');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');
                    this.deadline_date_start = $('#deadline-date-start');
                    this.deadline_date_end = $('#deadline-date-end');

                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    $('#product-description').keyup(function () {
                        FilterBuilder.addFilter('comment', {
                            field: "comment",
                            operator: "startWith",
                            value: $('#product-description').val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.deadline_date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_perf_start', {
                            field: "date_perf",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.deadline_date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_perf_end', {
                            field: "date_perf",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });

                    // Селектор статуса
                    this.stateSelector.select2({
                        minimumResultsForSearch: -1,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('state');
                                } else {
                                    FilterBuilder.addFilter('state', {
                                        field: "state",
                                        operator: "eq",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    // Селектор firms
                    this.firmSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('firm_id');
                                } else {
                                    FilterBuilder.addFilter('firm_id', {
                                        field: "firm_id",
                                        operator: "eq",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    // Селектор client
                    this.clientSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('client_id');
                                } else {
                                    FilterBuilder.addFilter('client_id', {
                                        field: "client_id",
                                        operator: "eq",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    this.respUserSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('resp_user_id');
                                } else {
                                    FilterBuilder.addFilter('resp_user_id', {
                                        field: "resp_user_id",
                                        operator: "eq",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    this.perfUserSelector.select2({
                        minimumResultsForSearch: 5,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('perf_user_id');
                                } else {
                                    FilterBuilder.addFilter('perf_user_id', {
                                        field: "perf_user_id",
                                        operator: "eq",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                    //console.log('initSlidersAmount');
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },

                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;
                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "state" && value.operator == 'eq') {
                        $("#state-selector").select2("val", value.value);
                        FilterBuilder.addFilter('state', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                    if (value.field == "description" && value.operator == 'startWith') {
                        $('#product-description').val(value.value);
                        FilterBuilder.addFilter('description', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                //Устанавливает значения фильтров-слайдеров по массиву данных
                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                    var self = this;
                    var option;
                    this.statesDataSource = [];
                    $.each(this.limits.states, function (index, value) {
                        option = {text: index, value: value};
                        self.statesDataSource.push(option);
                    });
                }

            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            };
            Filters['firstInitFilters'] = function (data) {
                this.limits = data;
                this.initSlidersMaxMin();
                this.initDatesMaxMin();
                this.initDropDownListDataSource();
                var savedValues = FilterBuilder.getSavedFilterValues();
                if (savedValues) {
                    this.setValuesBySavedData(savedValues);
                } else {
                    this.setSliderValuesByLimits();
                }
                this.initSlidersAmount();
            };
            FilterBuilder['filters'] = {};
            FilterBuilder.filter_keys = TileGridConfig.filter_keys;
            FilterBuilder.filter_keys_hash = TileGridConfig.filter_keys_hash;
            FilterBuilder['addFilter'] = function (key, filter) {
                if ($.inArray(key, this.filter_keys) >= 0) {
                    this.filters[key] = filter;
                }
            };
            FilterBuilder.removeFilter = function (key) {
                if ($.inArray(key, this.filter_keys) >= 0) {
                    delete this.filters[key];
                }
            };
            FilterBuilder.applyFilters = function () {
                var filters = [];
                for (var j = 0; j < this.filter_keys.length; j++) {
                    var key = this.filter_keys[j];
                    if (this.filters.hasOwnProperty(key)) {
                        filters.push(this.filters[key]);
                    }
                }
                var data = {
                    filter: {filters: filters},
                    options: {}
                };
                $.ajax({
                    url: TileGridConfig.baseAjaxPath + "getTaskTiles",
                    method: "POST",
                    dataType: "json",
                    data: data,
                    context: this,
                    success: function (result, status, xhr) {
                        RedrawTiles(result);
                        if (!Filters.isInit) {
                            Filters.firstInitFilters(result.filters_data);
                            Filters.isInit = true;
                        } else {
                            Filters.updateSliderLimits(result.filters_data);
                        }
                        e.success(JSON.parse(JSON.stringify(result)));
                    },
                    error: function (xhr, status, error) {
                        e.error(xhr.responseText, status, eval("(" + xhr.responseText + ")"));
                    }
                });

                //localStorage["kendo-grid-filters" + GridConfig.storage_postfix] = kendo.stringify(filters);
            };
            FilterBuilder.getSavedFilterValues = function () {
                var hashedFilters = this.tryToLoadFiltersFromHash();
                if (hashedFilters != false) {
                    return hashedFilters;
                }
                var filtersValues = null;// = localStorage["kendo-grid-filters" + GridConfig.storage_postfix];
                if (filtersValues) {
                    return JSON.parse(filtersValues);
                }
                return false;
            };
            FilterBuilder.tryToLoadFiltersFromHash = function () {
                var self = this;
                var filters = {};
                if (window.location.hash != '') {
                    try {
                        var encoded = JSON.parse(decodeURIComponent(window.location.hash.substr(1)));
                        if ('filters' in encoded) {
                            $.each(encoded.filters, function (key, value) {
                                if ($.inArray(value.field, TileGridConfig.filter_keys_hash) >= 0) {
                                    filters[self.keyGenerator(value)] = value;
                                }
                            });
                            return filters;
                        }
                    } catch (e) {
                        console.log(e.name);
                    }
                }
                return false;
            };

            Filters.initFilters();

        });
    </script>
@stop