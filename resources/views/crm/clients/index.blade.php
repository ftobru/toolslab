
@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('switcher')
    <ul class="nav nav-tabs">
        <li class="active"><a href="#">Контакты</a></li>
        <li><a href="/{{$company->name}}/crm/firms">Компании</a></li>
    </ul>
@stop

@section('content')
    <div class="offcanvas">
    </div>

    <div class="card tab-content filter-page">
        <div class="filter-menu" id="off-canvas1" style="left: 60px;">
            <nav>
                <div class="expanded">
                    <a href="../../html/dashboards/dashboard.html">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label no-margin">
                                <input type="text" class="form-control input-sm" id="product-name">
                                <label for="product-name">Название:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group floating-label no-margin">
                                <input type="text" class="form-control input-sm" id="client-email-filter">
                                <label for="client-email-filter">e-mail:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group floating-label no-margin">
                                <input type="text" class="form-control input-sm" id="client-phone-filter">
                                <label for="client-phone-filter">Телефон:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group no-margin">
                                <select id="status-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите статус">
                                    <option>&nbsp;</option>
                                    <option value="all">Все</option>
                                    @foreach ($statuses as $key=>$val)
                                        <option value="{{$val}}">{{$key}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <select id="type-selector" class="form-control select2-list input-sm"
                                        data-placeholder="Выберите тип">
                                    <option>&nbsp;</option>
                                    <option value="all">Все</option>
                                    @foreach ($types as $key=>$val)
                                        <option value="{{$val}}">{{$key}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </li>
                        <li>
                            <div class="form-group no-margin">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control input-sm" id="date-end" name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
            <div class=" section-body">
                <div class="table_wrap">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="card" id="droptarget-hidden-columns">
                            <div class="card-head">
                                <header>Скрытые столбцы</header>
                            </div>
                            <div class="card-body">
                                <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                    <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                </div>
                            </div>
                            <!--end .card-body -->
                        </div>

                        {{--<div><span class="print-me">[Печать]</span></div>--}}
                        <div class="k-grid-container" id="grid"></div>

                    </div>
                </div>
                </div>
            </div>
        </section>
    </div>

    <!--end content-->
@stop

@section('modals')

    <div class="offcanvas create-company">

        <div id="offcanvas-search" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <p>Создать компанию?</p>

                <p>При создании новой компании с Вашего баланса будут сняты средства за первый месяц использования
                    минимального тарифного.</p>

                <p>Тариф LiteLab - NNN рублей</p>

                <div class="form-group">
                    <button type="submit"
                            data-dismiss="offcanvas"
                            id="btn-cancel-company-create"
                            class="btn btn-default-light ink-reaction">
                        Отмена
                    </button>
                    <button type="submit"
                            href="#offcanvas-search2"
                            data-toggle="offcanvas"
                            data-backdrop="false"
                            class="btn ink-reaction btn-primary">
                        Создать компанию
                    </button>
                </div>
            </div>
        </div>

        <div id="offcanvas-search2" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <h3 class="text-primary">Создание новой компании</h3>

                <form id="create-company">
                    <div class="form-group">
                        <label for="Firstname5" class="control-label">Название</label>
                        <input type="text" class="form-control" id="Username1">

                        <div class="form-control-line"></div>
                    </div>
                    <div class="form-group">
                        <button
                                data-dismiss="offcanvas"
                                type="submit"
                                class="btn btn-default-light ink-reaction">
                            Отмена
                        </button>
                        <input type="button"
                               value="Продолжить"
                               id="btn-create-company"
                               data-dismiss="offcanvas"
                               class="btn ink-reaction btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('scripts')

    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>

    <script type="text/javascript">

        GridConfig = {};
        window.Filters = {};
        FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 1;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_contacts_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/crm/ajax/contacts/';
        // Путь до карты
        GridConfig.baseCardUrl = '/' + '{{{$company->name}}}' + '/crm/card/0/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'contacts';
        // Можно ли редактировать
        GridConfig.editable = true;
        // Можно ли делать группировку
        GridConfig.groupable = true;
        // Шаблон копирования строк
        GridConfig.copyFields = ['name', 'status', 'type', 'description', 'email', 'phone'];
        // Для доп полей
        GridConfig.existedFields = ['id', 'updated_at', 'created_at', 'command', 'status', 'name', 'type', 'phone', 'email',
            'company_id' @foreach($customFields as $field),'{{{$field->name}}}'@endforeach];
        // Конфиг колонок грида
        GridConfig.columns = [
            {
                field: "id",
                title: "ID",
                width: "50px",
                locked: true,
                lockable: false,
                groupable: false
            },
            {
                field: "name",
                title: "Название",
                width: "250px",
                aggregates: ["count"],
                groupHeaderTemplate: "Название: #= value # (Всего: #= count#)",
                locked: true,
                lockable: false,
                groupable: false
            },
            {
                field: "status",
                title: 'Статус',
                editor: function (container, options) {
                    $('<input />')
                            .attr('data-bind', 'value:status')
                            .appendTo(container)
                            .kendoDropDownList({
                                dataSource: Filters.statusesDataSource,
                                dataTextField: 'text',
                                dataValueField: 'value'
                            });
                },
                width: "130px",
                template: "#= cellTemplate(status, 'status') #",
                aggregates: ["count"],
                groupHeaderTemplate: "Статус: #= getStatusLabel(value) # (Всего: #= count#)"
            },
            {
                field: "type",
                title: "Тип",
                editor: function (container, options) {
                    $('<input />')
                            .attr('data-bind', 'value:type')
                            .appendTo(container)
                            .kendoDropDownList({
                                dataSource: Filters.typesDataSource,
                                dataTextField: 'text',
                                dataValueField: 'value'
                            });
                },
                template: "#= cellTemplate(type, 'type') #",
                width: "130px",
                aggregates: ["count"],
                groupHeaderTemplate: "Тип: #= getTypeLabel(value) # (Всего: #= count#)"
            },
            {field: "description", title: "Описание", hidden: true, groupable: false},
            {
                field: "email", title: "email", aggregates: ["count"],
                groupHeaderTemplate: "email: #= value # (Всего: #= count#)",
                width: "130px"
            },
            {
                field: "phone", title: "телефон", aggregates: ["count"],
                groupHeaderTemplate: "Телефон: #= value # (Всего: #= count#)",
                width: "130px"
            },
            @foreach($customFields as $field)
            {field: "{{{$field->name}}}", title: "{{{$field->label}}}", width: "130px", sortable:false, groupable: false},
                @endforeach
            {
                field: "created_at",
                title: "Созданa",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px",
                editor: "readonlyEditor"
            },
            {
                field: "updated_at",
                title: "Изменён",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px",
                editor: "readonlyEditor"
            }
        ];
        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string", validation: {required: true}},
                description: {type: "string"},
                phone: {type: "string", defaultValue: ''},
                email: {type: "string", defaultValue: ''},
                status: {type: "number"},
                type: {type: "number"},
        @foreach($customFields as $field)
        {{{$field->name}}}:{type: "{{{$field->type}}}"},
        @endforeach
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Конфиг аггрегатора группировок грида
        GridConfig.aggregateFields = [
            {field: "name", aggregate: "count"},
            {field: "status", aggregate: "count"},
            {field: "type", aggregate: "count"}
        ];
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'type',
            'status',
            'phone',
            'email',
            'date_start',
            'date_end',
            'name'
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            'type',
            'status',
            'phone',
            'email',
            'date_start',
            'date_end',
            'name'
        ];

        // Форматирует вывод статусов и типов в гриде.
        function cellTemplate(value, type) {
            var x;
            switch (type) {
                case 'type':
                    x = $.grep(Filters.typesDataSource, function (n, i) {
                        return n.value == value;
                    });
                    if (x.length > 0) {
                        return x[0].text;
                    }
                    break;
                case 'status':
                    x = $.grep(Filters.statusesDataSource, function (n, i) {
                        return n.value == value;
                    });
                    if (x.length > 0) {
                        return x[0].text;
                    }
                    break;
            }
            return '[unknown]';
        }

        // Возвращает в шаблон группировок значение группировки
        function getStatusLabel(value) {

            var x = $.grep(Filters.statusesDataSource, function (item) {
                return item.value == value;
            });
            return x[0].text;
        }

        // Возвращает в шаблон группировок значение группировки
        function getTypeLabel(value) {

            var x = $.grep(Filters.typesDataSource, function (item) {
                return item.value == value;
            });
            return x[0].text;
        }

        $(document).ready(function () {

            Filters = {

                isInit: false,

                // Наши слайдеры.
                profit: null,
                old_price: null,
                new_price: null,

                // Наши селекторы
                typeSelector: null,
                statusSelector: null,

                // Селекторы дат
                date_start: null,
                date_end: null,

                phone_input: null,
                email_input: null,

                // Допустимые значения dropdownlist
                typesDataSource: {},
                statusesDataSource: {},

                // Массивы диапазонов слайдеров.
                limits: null,

                initFilters: function () {
                    var self = this;
                    // Наши слайдеры )
                    this.profit = $("#slider-range-profit");
                    this.old_price = $("#slider-range-old-price");
                    this.new_price = $("#slider-range-price");

                    this.email_input = $("#client-email-filter");
                    this.phone_input = $("#client-phone-filter");

                    //Селекторы
                    this.typeSelector = $('#type-selector');
                    this.statusSelector = $('#status-selector');

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');


                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    $('#product-name').keyup(function () {
                        FilterBuilder.addFilter('name', {
                            field: "name",
                            operator: "startWith",
                            value: $('#product-name').val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.phone_input.keyup(function () {
                        FilterBuilder.addFilter('phone', {
                            field: "phone",
                            operator: "startWith",
                            value: self.phone_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.email_input.keyup(function () {
                        FilterBuilder.addFilter('email', {
                            field: "email",
                            operator: "startWith",
                            value: self.email_input.val()
                        });
                        FilterBuilder.applyFilters();
                    });

                    this.date_start.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        FilterBuilder.applyFilters();
                    });

                    // Селектор типа
                    this.typeSelector.select2({
                        minimumResultsForSearch: -1, // hide search
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('type');
                                } else {
                                    FilterBuilder.addFilter('type', {
                                        field: "type",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });

                    // Селектор статуса
                    this.statusSelector.select2({
                        minimumResultsForSearch: -1,
                        closeOnSelect: true
                    })
                            .on("change", function (e) {
                                var value = e.currentTarget.value;
                                if (value == 'all') {
                                    FilterBuilder.removeFilter('status');
                                } else {
                                    FilterBuilder.addFilter('status', {
                                        field: "status",
                                        operator: "equal",
                                        value: parseInt(value)
                                    });
                                }
                                FilterBuilder.applyFilters();
                            });
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },


                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    //this.initSlidersAmount();
                    FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    if (value.field == "status" && value.operator == 'eq') {
                        $("#status-selector").select2("val", value.value);
                        FilterBuilder.addFilter('status', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                    if (value.field == "type" && value.operator == 'eq') {
                        $("#type-selector").select2("val", value.value);
                        FilterBuilder.addFilter('status', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                    if (value.field == "name" && value.operator == 'startWith') {
                        $('#product-name').val(value.value);
                        FilterBuilder.addFilter('name', {
                            field: value.field,
                            operator: value.operator,
                            value: value.value
                        });
                    }
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                },

                initSlidersValues: function (data) {
                },

                initSlidersMaxMin: function () {
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                },

                initDropDownListDataSource: function () {
                    var self = this;
                    var option;
                    this.statusesDataSource = [];
                    this.typesDataSource = [];
                    $.each(this.limits.statuses, function (index, value) {
                        option = {text: index, value: value};
                        self.statusesDataSource.push(option);
                    });
                    $.each(this.limits.types, function (index, value) {
                        option = {text: index, value: value};
                        self.typesDataSource.push(option);
                    });
                }
            };

            FilterBuilder = {
                keyGenerator: function (filter) {
                    return filter.field;
                }
            }

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });

    </script>
@stop