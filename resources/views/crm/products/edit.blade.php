@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('buttons')
    @include('partials.cabinet.button_delete')
    @include('partials.cabinet.button_cancel')
    @include('partials.cabinet.button_save')
@stop

@section('content')
    <section>
        <ol class="breadcrumb">
            <li><a href="/{{{$company->name}}}/crm/dashboard">{{{$company->display_name}}}</a></li>
            <li><a href="/{{{$company->name}}}/crm/products">Товары</a></li>
            <li class="active">редактирование</li>
        </ol>
        <div class="section-body no-margin">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="text-primary">Редактирование товара</h2>

                    <div class="preview-wrp">
                        <div class="tip">Тут будет описание этого блока, окторое уходит в многоточие и
                            открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого
                            блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие
                            кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и
                            открывается-закрывается при нажатии на соответствующие кнопоки. Тут будет описание этого
                            блока, окторое уходит в многоточие и открывается-закрывается при нажатии на соответствующие
                            кнопоки. Тут будет описание этого блока, окторое уходит в многоточие и
                            открывается-закрывается при нажатии на соответствующие кнопоки.
                        </div>
                    </div>
                </div>
            </div>
            <form id="product_form" class="form-horizontal edit_product" method="POST" action="/{{{$company->name}}}/crm/product/edit/{{{$product['id']}}}" role="form">
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="regular90" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Введите артикул товара">Артикул <span><i
                                                    class="md md-help"></i></span>
                                         <span class="popover-btn" data-container="body" data-animation="fade"
                                               data-toggle="popover" data-placement="auto" data-html="true"
                                               data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                     class="md md-info"></i></span>
                                    </label>

                                    <div class="col-sm-9">
                                        <input type="text" name="article" class="form-control" id="regular90"
                                               value="{{{$product['article']}}}" placeholder="">

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" data-toggle="tooltip" data-placement="auto"
                                           title="Введите название вашего товара">Название
                                        <span><i class="md md-help"></i></span>
                                        <span class="popover-btn" data-container="body" data-animation="fade"
                                              data-toggle="popover" data-placement="auto" data-html="true"
                                              data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                    class="md md-info"></i></span>
                                    </label>

                                    <div class="col-sm-9">
                                        <input name="name" type="text" class="form-control" value="{{{$product['name']}}}"
                                               placeholder="">

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Теги</label>

                                    <div class="col-sm-9">
                                        <input name="tags" type="text" class="form-control" value="{{{$tags}}}"
                                               data-role="tagsinput" style="display: none;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="regular92" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Введите описание вашего товара">Описание
                                        <span><i class="md md-help"></i></span>
                                      <span class="popover-btn" data-container="body" data-animation="fade"
                                            data-toggle="popover" data-placement="auto" data-html="true"
                                            data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                  class="md md-info"></i></span>
                                    </label>

                                    <div class="col-sm-9">
                                        <textarea name="description" class="form-control" id="regular92"
                                                  placeholder="">{{{$product['description']}}}</textarea>

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="regular93" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Цена товара">Цена товара <span><i
                                                    class="md md-help"></i></span>
                                          <span class="popover-btn" data-container="body" data-animation="fade"
                                                data-toggle="popover" data-placement="auto" data-html="true"
                                                data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                      class="md md-info"></i></span>
                                    </label>

                                    <div class="col-sm-9">
                                        <input name="new_price" type="text" class="form-control" id="regular93"
                                               placeholder="" value="{{{$product['new_price']}}}">

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="regular94" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Для удобного рассчета скидки">Старая цена <span><i
                                                    class="md md-help"></i></span>
                                      <span class="popover-btn" data-container="body" data-animation="fade"
                                            data-toggle="popover" data-placement="auto" data-html="true"
                                            data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                  class="md md-info"></i></span>
                                    </label>

                                    <div class="col-sm-9">
                                        <input name="old_price" type="text" class="form-control" id="regular94"
                                               placeholder="" value="{{{$product['old_price']}}}">

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="regular95" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="До какой цены можно опустить цену при заказе">Минимальная
                                        цена <span><i class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <input name="min_price" type="text" class="form-control" id="regular95"
                                               placeholder="" value="{{{$product['min_price']}}}">

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                @if($primeCost)
                                <div class="form-group">
                                    <label for="regular96" class="col-sm-3 control-label" data-toggle="tooltip"
                                           data-placement="auto" title="Для рассчета прибыли со сделки">Себестоимость
                                        <span><i class="md md-help"></i></span></label>

                                    <div class="col-sm-9">
                                        <input name="prime_cost" type="text" class="form-control" id="regular96"
                                               placeholder="" value="{{{$product['prime_cost']}}}">

                                        <div class="form-control-line"></div>
                                    </div>
                                </div>
                                @endif
                                <div class="panel-group" id="accordion2">
                                    <div>
                                        <div class="card-head card-head-xs collapsed" data-toggle="collapse"
                                             data-parent="#accordion2" data-target="#accordion2-1">
                                            <span class="opacity-50">Дополнительно</span>

                                            <div class="tools">
                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <div id="accordion2-1" class="collapse">
                                            @if($primeCost)
                                            <div class="form-group">
                                                <label for="regular97" class="col-sm-3 control-label"
                                                       data-toggle="tooltip" data-placement="auto"
                                                       title="Маржа с проданного товара">Прибыль <span><i
                                                                class="md md-help"></i></span></label>
                                                <div class="col-sm-9">
                                                    <input name="profit" type="text" class="form-control" id="regular97"
                                                           placeholder="" disabled>

                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <label for="regular98" class="col-sm-3 control-label"
                                                       data-toggle="tooltip" data-placement="auto"
                                                       title="Для отображения на страницах">Процент скидки <span><i
                                                                class="md md-help"></i></span>
                                                      <span class="popover-btn" data-container="body" data-animation="fade"
                                                            data-toggle="popover" data-placement="auto" data-html="true"
                                                            data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                                  class="md md-info"></i></span>
                                                </label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="regular98"
                                                           placeholder="" disabled>

                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="regular99" class="col-sm-3 control-label"
                                                       data-toggle="tooltip" data-placement="auto"
                                                       title="Для отображения на страницах">Сумма скидки <span><i
                                                                class="md md-help"></i></span>
                                                      <span class="popover-btn" data-container="body" data-animation="fade"
                                                            data-toggle="popover" data-placement="auto" data-html="true"
                                                            data-content='<div class="content-popover2 opacity-75 text-center"><span class="text-primary">Тег для вставки:</span></br/><textarea id="myText"><profit></profit></textarea><br/>Нажмите Ctrl+C для копирования</div>'><i
                                                                  class="md md-info"></i></span>
                                                </label>

                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="regular99"
                                                           placeholder="" disabled>

                                                    <div class="form-control-line"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end .panel -->
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-head">
                                <header class="text-primary">Параметры</header>
                            </div>
                            <div class="card-body">
                                @foreach($fields as $field)
                                    @if($field->form_type === 1)
                                        <div class="form-group">
                                            <label for="custom_field_{{{$field->name}}}" class="col-sm-3 control-label"
                                                   data-toggle="tooltip"
                                                   data-placement="auto" title="Цена товара">{{{$field->label}}}
                                                <span><i
                                                            class="md md-help"></i></span></label>

                                            <div class="col-sm-9">
                                                @if(isset($product[$field->name]))
                                                    @if(is_array($product[$field->name]))
                                                        <input type="text" value="{{{$product[$field->name][0]}}}"
                                                               name="{{{$field->name}}}" class="form-control"
                                                               id="custom_field_{{{$field->name}}}" placeholder="">
                                                    @endif
                                                    @if(is_string($product[$field->name]))
                                                        <input type="text" value="{{{$product[$field->name]}}}"
                                                               name="{{{$field->name}}}" class="form-control"
                                                               id="custom_field_{{{$field->name}}}" placeholder="">
                                                    @endif
                                                @else
                                                    <input type="text" value=""
                                                           name="{{{$field->name}}}" class="form-control"
                                                           id="custom_field_{{{$field->name}}}" placeholder="">
                                                @endif
                                                <div class="form-control-line"></div>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                                <span class="text-primary">Добавить дополнительные параметры можно в <a href="/{{{$company->name}}}/crm/products/fields">настройках</a></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5 hidden-sm hidden-xs">
                        <div class="note-wrap">
                            <div class="tip">
                                <div><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis neque deserunt
                                        distinctio dolores ipsum cumque laudantium doloribus quo quisquam, enim et
                                        similique pariatur aspernatur asperiores fuga odio explicabo placeat nemo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="card">
                            <div class="card-head">
                                <header class="text-primary">Добавить фотографию</header>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xs-3 opacity-50"><span data-toggle="tooltip" data-placement="auto"
                                                                           title="Загрузите фото">Фотография товара <i
                                                    class="md md-help"></i></span></div>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" id="fileuploadurl0" readonly
                                               placeholder="">
                                    </div>
                                    <div class="col-xs-2 text-right">
                                        <ul class="list-inline">
                                            <li>
                                                <a class="btn btn-default fileupload" data-fieldId="0" data-toggle="tooltip" data-placement="auto"
                                                   title="Загрузите с сервера">
                                                    <span>
                                                        <i class="md md-file-upload"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-3 opacity-50"><span data-toggle="tooltip" data-placement="auto"
                                                                           title="Дополнтельное фото">Доп. фотография <i
                                                    class="md md-help"></i></span></div>
                                    <div class="col-xs-7">
                                        <input type="text" class="form-control" id="fileuploadurl1" readonly
                                               placeholder="">
                                    </div>
                                    <div class="col-xs-2 text-right">
                                        <ul class="list-inline">
                                            <li>
                                                <a class="btn btn-default fileupload" data-fieldId="1" data-toggle="tooltip" data-placement="auto"
                                                   title="Загрузите с сервера">
                                                    <span>
                                                        <i class="md md-file-upload"></i>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row margin-bottom-xxl">
                    <div class="col-sm-12">
                        <ul class="list-unstyled list-inline">
                            <li>
                                <button type="button" id="delete-btn1" class="btn btn-default-light ink-reaction btn-sm">Удалить</button>
                            </li>
                            <li>
                                <button type="button" id="reset-btn1" class="btn btn-default-light ink-reaction btn-sm">Отменить</button>
                            </li>
                            <li>
                                <button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <input type="hidden" class="form-control" id="fileLinks" name="fileLinks" >
            </form>
        </div>

        <div class="row">
            <div class="col-xs-12 opacity-75">
                <p class="no-margin">Обновлено <span>{{ date("d.m.Y",strtotime($product['updated_at'])) }}</span> в <span>{{ date("G:i",strtotime($product['updated_at'])) }}</span></p>
                <p class="no-margin">Создано <span>{{ date("d.m.Y",strtotime($product['created_at'])) }}</span> в <span>{{ date("G:i",strtotime($product['created_at'])) }}</span></p>
            </div>
        </div>
    </section>
@stop

@section('modals')
@stop

@section('scripts')
    <!-- Elfinder -->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui-filemanager-theme.css">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/elfinder.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= asset('/packages/barryvdh/elfinder/css/theme.css') ?>">
    <script src="<?= asset('/packages/barryvdh/elfinder/js/elfinder.min.js') ?>"></script>
    <script src="<?= asset("/packages/barryvdh/elfinder/js/i18n/elfinder.ru.js") ?>"></script>

    <script type="text/javascript" src="/js/jquery-ui.min.js"></script>

    <script type="text/javascript">

        var elf = null;
        var fileLinks = {};
        var currentFileSelectorId = '';

        function processSelectedFile(filePath, elemId) {
            fileLinks[elemId] = filePath;
            var filename = filePath.replace(/^.*[\\\/]/, '')
            $('#'+elemId).val(filename);

            var data = JSON.stringify(fileLinks);
            $('#fileLinks').val(data);
        }

        function resetBtn() {
            location.reload();
        }

        function deleteBtn() {
            var data = {
                product_id:{{{$product['id']}}}
            };

            $.ajax({
                url: '/{{{$company->name}}}/crm/ajax/products/delete',
                method: "POST",
                data: data,
                context: this,
                success: function (result, status, xhr) {
                    $(location).attr('href','/{{{$company->name}}}/crm/products');
                },
                error: function (xhr, status, error) {
                    toastr.error('Произошла ошибка при удалении товара.');
                }
            });
        }

        $(document).ready(function () {

            var photos = '{{{$product['photos']}}}';
            photos = photos.replace(/&quot;/g,'"');

            $('#delete-btn1').click(function() {
                deleteBtn();
            });
            $('#reset-btn1').click(function() {
                resetBtn();
            });
            $('#header-save-btn').click(function() {
                $('#product_form').submit();
            });
            $('#header-cancel-btn').click(function() {
                resetBtn();
            });
            $('#header-delete-btn').click(function() {
                deleteBtn();
            });




            try{
                photos = JSON.parse(photos);
                if(photos.hasOwnProperty('fileuploadurl0')) {
                    fileLinks['fileuploadurl0'] = photos.fileuploadurl0;
                    $('#fileuploadurl0').val(photos.fileuploadurl0.replace(/^.*[\\\/]/, ''));
                }
                if(photos.hasOwnProperty('fileuploadurl1')) {
                    fileLinks['fileuploadurl1'] = photos.fileuploadurl1;
                    $('#fileuploadurl1').val(photos.fileuploadurl1.replace(/^.*[\\\/]/, ''));
                }

            } catch(e) {
                console.log('files not attached');
            }

            $.each($(".btn.fileupload"), function(key, value){
                $(value).click(function(e) {
                   var btn = $(e.target).closest(".btn");
                    currentFileSelectorId = 'fileuploadurl' + btn.data().fieldid.toString();
                    if(!elf) {
                         elf = $('<div></div>').dialogelfinder({
                            <?php if($locale){ ?>
                            lang: '<?= $locale ?>', // locale
                            <?php } ?>
                            customData: {
                                _token: '<?= csrf_token() ?>'
                            },
                            url: '/{{{$company_alias}}}/fileManager/connector',  // connector URL
                            dialog: {
                                modal: true,
                                width: 700,
                                height: 450,
                                title: "Select your file",
                                zIndex: 99999,
                                resizable: true
                            },
                            resizable: false,
                            getFileCallback: function (file) {
                                processSelectedFile(file.path, currentFileSelectorId);
                                elf.hide();
                            }
                        }).elfinder('instance');
                    } else {
                        elf.show();
                    }
                });
            });

            var errors = @if($errors->any())"{{$errors->first()}}"@else null @endif;
            if (errors) {
                toastr.error(errors);
            }

            var success = @if(isset($success))"{{$success}}"@else null @endif;
            if (success) {
                toastr.success(success);
            }

            $('.tip').truncate({
                length: 120,
                minTrail: 10,
                moreText: 'читать полностью',
                lessText: 'скрыть',
                ellipsisText: " ..."
            });

            $('.popover-btn').on('show.bs.popover', function (e) {
                setTimeout(function () {
                    document.getElementById("myText").select();
                }, 300);
            });

            // Расчет показателей
            var Calculator = {
                new_price: 0,
                old_price: 0,
                prime_cost: 0,
                init: function () {
                    var self = this;

                    this.new_price = parseInt($('#regular93').val());
                    this.old_price = parseInt($('#regular94').val());
                    this.prime_cost = parseInt($('#regular96').val());

                    self.resolve();

                    // new price
                    $('#regular93').bind('input', function (e) {
                        self.new_price = parseInt($(this).val());
                        self.resolve();
                    });
                    // old price
                    $('#regular94').bind('input', function (e) {
                        self.old_price = parseInt($(this).val());
                        self.resolve();
                    });
                    @if($primeCost)
                    // prime_cost
                    $('#regular96').bind('input', function (e) {
                        self.prime_cost = parseInt($(this).val());
                        self.resolve();
                    });
                    @endif
                },
                resolve: function () {
                    var discount_summ = this.old_price - this.new_price;
                    @if($primeCost)
                    var profit = this.new_price - this.prime_cost;
                    @endif
                    var discount_percent = (discount_summ / this.old_price) * 100;

                    @if($primeCost)
                    $('#regular97').val(profit);
                    @endif
                    if ($.isNumeric(discount_percent)) {
                        $('#regular98').val(discount_percent);
                    } else {
                        $('#regular98').val('-');
                    }
                    $('#regular99').val(discount_summ);

                }
            }
            Calculator.init();

        });
    </script>
@stop