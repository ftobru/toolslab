@extends('layouts.menubar_general')

@section('left-header-buttons')
    <li>
        <a id="trigger" class="btn btn-icon-toggle filter_btn" href="javascript:void(0);"
           data-toggle="menubar1">
            <i class="fa fa-filter"></i>
        </a>
    </li>
    <li>
        <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
            <i class="fa fa-bars"></i>
        </a>
    </li>
@stop

@section('content')
        <div class="filter-menu" id="off-canvas1" style="left: 60px;">
            <nav>
                <div class="expanded">
                    <a href="/">
                        <span class="text-lg text-bold text-primary ">Фильтр&nbsp;</span>
                    </a>
                </div>
                <ul>
                    <form class="form">
                        <li>
                            <div class="form-group floating-label">
                                <input type="text" class="form-control input-sm" style="margin-top: 8px;" id="product-name">
                                <label for="product-name">Название:</label>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label for="amount-new-price">Цена:</label>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="form-group floating-label">
                                            <input type="text" id="minCostNewPrice" value="0" class="form-control input-sm">
                                            <label for="product-name"></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 pull-right">
                                        <div class="form-group floating-label">
                                            <input type="text" id="maxCostNewPrice" value="1000" class="form-control input-sm">
                                            <label for="product-name"></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="slider-range-price" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 100%;"></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label for="amount-old-price">Старая цена:</label>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="form-group floating-label">
                                            <input type="text" id="minCostOldPrice" value="0" class="form-control">
                                            <label for="product-name"></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 pull-right">
                                        <div class="form-group floating-label">
                                            <input type="text" id="maxCostOldPrice" value="1000" class="form-control">
                                            <label for="product-name"></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="slider-range-old-price"
                                     class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all"
                                         style="left: 0%; width: 100%;"></div>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                          style="left: 0%;"></span>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                          style="left: 100%;"></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <label for="amount-profit">Цена:</label>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="form-group floating-label">
                                            <input type="text" id="minCostProfit" value="0" class="form-control">
                                            <label for="product-name"></label>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 pull-right">
                                        <div class="form-group floating-label">
                                            <input type="text" id="maxCostProfit" value="1000" class="form-control">
                                            <label for="product-name"></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="slider-range-profit"
                                     class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all"
                                         style="left: 0%; width: 100%;"></div>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                          style="left: 0%;"></span>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"
                                          style="left: 100%;"></span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group">
                                <div id="demo-date-range" class="input-daterange">
                                    <div class="input-daterange input-group" id="demo-date-range">
                                        <label for="#">
                                            <span class="opacity-50">Выбрать дату</span>
                                            <span class="input-group-addon">от</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control" id="date-start" name="start">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="input-group-addon">до</span>

                                            <div class="input-group-content">
                                                <input type="text" class="form-control" id="date-end" name="end">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </nav>
        </div>

        <section id="first9" class="style-default-bright tab-pane active" style="padding-left: 320px;">
            <div class="section-body">
                <div class="table_wrap">
                    <div class="row">
                    <div class="col-lg-12">
                        <div class="card" id="droptarget-hidden-columns">
                            <div class="card-head">
                                <header>Скрытые столбцы</header>
                            </div>
                            <div class="card-body">
                                <div class="columns-drop" style="min-height: 25px; min-width: 100%;">
                                    <em>Перетащите сюда столбцы чтобы скрыть их</em>
                                </div>
                            </div>
                            <!--end .card-body -->
                        </div>
                        <div class="k-grid-container" id="grid"></div>
                    </div>
                </div>
                </div>
            </div>
        </section>
@stop

@section('modals')

    <div class="offcanvas create-company">

        <div id="offcanvas-search" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <p>Создать компанию?</p>

                <p>При создании новой компании с Вашего баланса будут сняты средства за первый месяц использования
                    минимального тарифного.</p>

                <p>Тариф LiteLab - NNN рублей</p>

                <div class="form-group">
                    <button type="submit"
                            data-dismiss="offcanvas"
                            id="btn-cancel-company-create"
                            class="btn btn-default-light ink-reaction">
                        Отмена
                    </button>
                    <button type="submit"
                            href="#offcanvas-search2"
                            data-toggle="offcanvas"
                            data-backdrop="false"
                            class="btn ink-reaction btn-primary">
                        Создать компанию
                    </button>
                </div>
            </div>
        </div>

        <div id="offcanvas-search2" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <h3 class="text-primary">Создание новой компании</h3>

                <form id="create-company">
                    <div class="form-group">
                        <label for="Firstname5" class="control-label">Название</label>
                        <input type="text" class="form-control" id="Username1">

                        <div class="form-control-line"></div>
                    </div>
                    <div class="form-group">
                        <button
                                data-dismiss="offcanvas"
                                type="submit"
                                class="btn btn-default-light ink-reaction">
                            Отмена
                        </button>
                        <input type="button"
                               value="Продолжить"
                               id="btn-create-company"
                               data-dismiss="offcanvas"
                               class="btn ink-reaction btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('scripts')

    <link type="text/css" rel="stylesheet" href="/css/kendo.css">
    {{--<link type="text/css" rel="stylesheet" href="/css/jquery-ui-theme.css">--}}
    <script type="text/javascript" src="/js/pako_deflate.min.js"></script> {{--Нужен для сжатия огромных данных--}}
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/kendo.messages.ru-RU.min.js"></script>
    <script type="text/javascript" src="/js/kendo.culture.ru-RU.min.js"></script>


    <script type="text/javascript">

        var GridConfig = {};
        var Filters = {};
        var FilterBuilder = {};

        // Версия данных текущего грида.
        GridConfig.version = 2;
        // Префикс для хранения настроек грида в localStorage
        GridConfig.postfixPath = '_products_v';
        // Пути для транспорта грида
        GridConfig.baseGridCrudUrl = '/' + '{{{$company->name}}}' + '/crm/ajax/products/';
        // Путь до карты
        GridConfig.baseCardUrl = '/' + '{{{$company->name}}}' + '/crm/product/edit/';
        // Имя для генерируемых файлов грида (pdf ...)
        GridConfig.gridName = 'products';
        // Можно ли редактировать
        GridConfig.editable = true;
        // Можно ли делать группировку
        GridConfig.groupable = true;
        // Шаблон копирования строк
        GridConfig.copyFields = ['name', 'new_price', 'old_price', 'profit', 'article', 'phone'];
        // Конфиг колонок грида
        GridConfig.columns = [
            {field: "id", title: "ID", width: "50px", locked: true},
            {field: "article", title: "Артикул", width: "150px"},
            {field: "name", title: "Название", width: "250px"},
            {field: "new_price", title: 'Цена, руб.', format: "{0:0.00}", width: "130px"},
            {field: "old_price", title: "Старая цена, руб.", format: "{0:0.00}", width: "130px"},
            @if($primeCost)
            {field: "prime_cost", title: "Себес, руб.", format: "{0:0.00}", width: "130px"},
            {field: "profit", title: "Выгода, руб", format: "{0:0.00}", width: "130px"},
            @endif
            {
                field: "created_at",
                title: "Создан",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px",
                editor: "readonlyEditor"
            },
            {
                field: "updated_at",
                title: "Изменён",
                format: "{0: dd-MM-yyyy HH:mm:ss}",
                width: "130px",
                editor: "readonlyEditor"
            },
            {field: "description", title: "Описание", hidden: true, width: "150px"},
            {field: "min_price", title: "Минимальная цена, руб.", format: "{0:0.00}", hidden: true, width: "130px"},
            @foreach($customFields as $field)
            {field: "{{{$field->name}}}", title: "{{{$field->label}}}", width: "130px", sortable:false, hidden: true, groupable: false},
            @endforeach
    ];

        GridConfig.existedFields = ['id', 'updated_at', 'created_at', 'command', 'name', 'new_price', 'old_price', 'discount', 'description', 'article',
            'company_id', 'profit', 'prime_cost', 'min_price'  @foreach($customFields as $field),'{{{$field->name}}}'@endforeach];

        // Конфиг модели грида
        GridConfig.model = {
            id: "id",
            fields: {
                id: {editable: false, nullable: true},
                name: {type: "string", validation: {required: true}},
                article: {type: "string"},
                new_price: {type: "number"},
                min_price: {type: "number"},
                old_price: {type: "number"},
                @if($primeCost)
                profit: {type: "number", editable: false},
                prime_cost: {type: "number"},
                @endif
                description: {type: "string"},
        @foreach($customFields as $field)
         {{{$field->name}}}:{type: "{{{$field->type}}}"},
        @endforeach
                created_at: {type: 'date', editable: false},
                updated_at: {type: 'date', editable: false}
            }
        };
        // Конфиг пагинатора грида
        GridConfig.gridPagerConfig = {
            perPage: 25,
            page: 1
        };
        // Конфиг аггрегатора группировок грида
        GridConfig.aggregateFields = [
            {field: "name", aggregate: "count"},
            {field: "new_price", aggregate: "count"},
            {field: "old_price", aggregate: "count"},
            {field: "profit", aggregate: "count"},
            {field: "phone", aggregate: "count"},
        ];
        // Ключи фильтров для FilterBuilder
        GridConfig.filter_keys = [
            'old_price_min',
            'old_price_max',
            'new_price_min',
            'new_price_max',
            'profit_min',
            'profit_max',
            'date_start',
            'date_end',
            'name'
        ];
        // Ключи допустимых фильтров в хеше
        GridConfig.filter_keys_hash = [
            "old_price",
            "new_price",
            "profit",
            "date_start",
            "date_end",
            "name"
        ];

        $(document).ready(function () {
            Filters = {

                isInit: false,

                // Наши слайдеры.
                profit: null,
                old_price: null,
                new_price: null,

                // Селекторы дат. $('[name="start"]').datepicker("update", new Date('2011-9-05'));
                date_start: null,
                date_end: null,

                // Массивы диапазонов слайдеров.
                limits: null,

                initFilters: function () {
                    var self = this;

                    // Наши слайдеры )
                    this.profit = $("#slider-range-profit");
                    this.old_price = $("#slider-range-old-price");
                    this.new_price = $("#slider-range-price");

                    // Выбор дат
                    this.date_start = $('#date-start[name="start"]');
                    this.date_end = $('#date-end[name="end"]');


                    // Вешаем события на контролы фильтра, чтобы добавлять фильтры к гриду.
                    $('#product-name').keyup(function () {
                        window.FilterBuilder.addFilter('name', {
                            field: "name",
                            operator: "startWith",
                            value: $('#product-name').val()
                        });
                        window.FilterBuilder.applyFilters();
                    });
                    this.date_start.datepicker().on('changeDate', function (e) {
                        window.FilterBuilder.addFilter('date_start', {
                            field: "created_at",
                            operator: "start",
                            value: e.format("yyyy-mm-dd")
                        });
                        window.FilterBuilder.applyFilters();
                    });
                    this.date_end.datepicker().on('changeDate', function (e) {
                        window.FilterBuilder.addFilter('date_end', {
                            field: "created_at",
                            operator: "end",
                            value: e.format("yyyy-mm-dd")
                        });
                        window.FilterBuilder.applyFilters();
                    });

//                    this.new_price.slider({
//                        range: true,
//                        slide: function (event, ui) {
//                            $("#amount-price").val(ui.values[0] + " руб. - " + ui.values[1] + " руб.");
//                        },
//                        change: function (event, ui) {
//                            if (event.originalEvent) {
//                                var newRange = ui.values;
//                                window.FilterBuilder.addFilter('new_price_min', {
//                                    field: "new_price",
//                                    operator: "min",
//                                    value: parseInt(newRange[0])
//                                });
//                                window.FilterBuilder.addFilter('new_price_max', {
//                                    field: "new_price",
//                                    operator: "max",
//                                    value: parseInt(newRange[1])
//                                });
//                                window.FilterBuilder.applyFilters();
//                            }
//                        }
//                    });


                    // Новая цена
                    this.new_price.slider({
                        range: true
                    })
                            .on('slide', function (event, ui) {
                                $("input#minCostNewPrice").val($(self.new_price).slider('getValue')[0]);
                                $("input#maxCostNewPrice").val($(self.new_price).slider('getValue')[1]);
                            })
                            .on('change', function (event, ui) {
                                $("input#minCostNewPrice").val($(self.new_price).slider('getValue')[0]);
                                $("input#maxCostNewPrice").val($(self.new_price).slider('getValue')[1]);
                            })
                            .on('slideStop', function (event) {
                                var newRange = event.value;
                                self.changeSliderCallback('new_price', newRange);
                            });

                    $("input#minCostNewPrice").change(function(){
                        var value1=$("input#minCostNewPrice").val();
                        var value2=$("input#maxCostNewPrice").val();
                        if(parseInt(value1) > parseInt(value2)){
                            value1 = value2;
                            $("input#minCostNewPrice").val(value1);
                        }

                        var range = $(self.new_price).slider("getValue");
                        range[0] = value1;
                        $(self.new_price).slider("setValue", range);

                        var newRange = [parseInt(value1), parseInt(value2)];
                        self.changeSliderCallback('new_price', newRange);
                    });

                    $("input#maxCostNewPrice").change(function(){
                        var value1=$("input#minCostNewPrice").val();
                        var value2=$("input#maxCostNewPrice").val();
                        if(parseInt(value1) > parseInt(value2)){
                            value2 = value1;
                            $("input#maxCostNewPrice").val(value2);
                        }

                        var range = $(self.new_price).slider("getValue");
                        range[1] = value2;
                        $(self.new_price).slider("setValue", range);

                        var newRange = [parseInt(value1), parseInt(value2)];
                        self.changeSliderCallback('new_price', newRange);
                    });

                    // Старая цена
                    this.old_price.slider({
                        range: true
                    })
                            .on('slide', function (event, ui) {
                                $("input#minCostOldPrice").val($(self.old_price).slider("getValue")[0]);
                                $("input#maxCostOldPrice").val($(self.old_price).slider("getValue")[1]);
                            })
                            .on('change', function (event, ui) {
                                $("input#minCostOldPrice").val($(self.old_price).data('slider').options.value[0]);
                                $("input#maxCostOldPrice").val($(self.old_price).data('slider').options.value[1]);
                            })
                            .on('slideStop', function (event) {
                                var newRange = event.value;
                                self.changeSliderCallback('old_price', newRange);
                            });

                    $("input#minCostOldPrice").change(function(){
                        var value1=$("input#minCostOldPrice").val();
                        var value2=$("input#maxCostOldPrice").val();
                        if(parseInt(value1) > parseInt(value2)){
                            value1 = value2;
                            $("input#minCostOldPrice").val(value1);
                        }

                        var range = $(self.old_price).slider("getValue");
                        range[0] = value1;
                        $(self.old_price).slider("setValue", range);

                        var newRange = [parseInt(value1), parseInt(value2)];
                        self.changeSliderCallback('old_price', newRange);
                    });

                    $("input#maxCostOldPrice").change(function(){
                        var value1=$("input#minCostOldPrice").val();
                        var value2=$("input#maxCostOldPrice").val();
                        if(parseInt(value1) > parseInt(value2)){
                            value2 = value1;
                            $("input#maxCostOldPrice").val(value2);
                        }

                        var range = $(self.old_price).slider("getValue");
                        range[1] = value2;
                        $(self.old_price).slider("setValue", range);

                        var newRange = [parseInt(value1), parseInt(value2)];
                        self.changeSliderCallback('old_price', newRange);
                    });

                    // Profit slider
                    this.profit.slider({
                        range: true
                    })
                            .on('slide', function (event, ui) {
                                $("input#minCostProfit").val($(self.profit).slider("getValue")[0]);
                                $("input#maxCostProfit").val($(self.profit).slider("getValue")[1]);
                            })
                            .on('change', function (event, ui) {
                                $("input#minCostProfit").val($(self.profit).slider("getValue")[0]);
                                $("input#maxCostProfit").val($(self.profit).slider("getValue")[1]);
                            })
                            .on('slideStop', function (event) {
                                var newRange = event.value;
                                self.changeSliderCallback('profit', newRange);
                            });

                    $("input#minCostProfit").change(function(){
                        var value1=$("input#minCostProfit").val();
                        var value2=$("input#maxCostProfit").val();
                        if(parseInt(value1) > parseInt(value2)){
                            value1 = value2;
                            $("input#minCostProfit").val(value1);
                        }

                        var range = $(self.profit).slider("getValue");
                        range[0] = value1;
                        $(self.profit).slider("setValue", range);

                        var newRange = [parseInt(value1), parseInt(value2)];
                        self.changeSliderCallback('profit', newRange);
                    });

                    $("input#maxCostProfit").change(function(){
                        var value1=$("input#minCostProfit").val();
                        var value2=$("input#maxCostProfit").val();
                        if(parseInt(value1) > parseInt(value2)){
                            value2 = value1;
                            $("input#maxCostProfit").val(value2);
                        }

                        var range = $(self.profit).slider("getValue");
                        range[1] = value2;
                        $(self.profit).slider("setValue", range);

                        var newRange = [parseInt(value1), parseInt(value2)];
                        self.changeSliderCallback('old_price', newRange);
                    });



//                    this.profit.slider({
//                        range: true,
//                        slide: function (event, ui) {
//                            $("#amount-profit").val(ui.values[0] + " руб. - " + ui.values[1] + " руб.");
//                        },
//                        change: function (event, ui) {
//                            if (event.originalEvent) {
//                                var newRange = ui.values;
//                                window.FilterBuilder.addFilter('profit_min', {
//                                    field: "profit",
//                                    operator: "min",
//                                    value: parseInt(newRange[0])
//                                });
//                                window.FilterBuilder.addFilter('profit_max', {
//                                    field: "profit",
//                                    operator: "max",
//                                    value: parseInt(newRange[1])
//                                });
//                                window.FilterBuilder.applyFilters();
//                            }
//                        }
//                    });
                },

                changeSliderCallback: function (sliderKey, newRange) {
                    window.FilterBuilder.addFilter(sliderKey + '_min', {
                        field: sliderKey,
                        operator: "min",
                        value: parseInt(newRange[0])
                    });
                    window.FilterBuilder.addFilter(sliderKey + '_max', {
                        field: sliderKey,
                        operator: "max",
                        value: parseInt(newRange[1])
                    });
                    window.FilterBuilder.applyFilters();
                },

                // Подписывает диапазоны цен над слайдером.
                initSlidersAmount: function () {
                    var self = this;

                    $("input#minCostOldPrice").val($(self.old_price).slider("getValue")[0]);
                    $("input#maxCostOldPrice").val($(self.old_price).slider("getValue")[1]);

                    $("input#minCostNewPrice").val($(self.new_price).slider("getValue")[0]);
                    $("input#maxCostNewPrice").val($(self.new_price).slider("getValue")[1]);

                    $("input#minCostProfit").val($(self.profit).slider("getValue")[0]);
                    $("input#maxCostProfit").val($(self.profit).slider("getValue")[1]);
                },

                updateSliderLimits: function (data) {
                    this.limits = data;
                    this.initSlidersMaxMin();
                },

                initDropDownListDataSource: function () {
                },

                //Устанавливает значения по сохранённым данным
                setValuesBySavedData: function (savedValues) {
                    name = '';
                    date_start = '';
                    date_end = '';
                    var data = {};
                    var self = this;

                    // Ставим значения из памяти + применяем сразу фильтры
                    $.each(savedValues, function (index, value) {
                        self.applySavedFilter(value);
                    });
                    window.FilterBuilder.applyFilters();
                },

                // Передаем сюда фильтр с local storage. Он применится к гриду.
                applySavedFilter: function (value) {
                    var range;
                    if (value.field == "new_price") {
                        if (value.operator == 'min') {
                            range = this.new_price.slider("getValue");
                            range[0] = value.value;
                            this.new_price.slider("setValue", range);
                            FilterBuilder.addFilter('new_price_min', {
                                field: "new_price",
                                operator: "min",
                                value: parseInt(range[0])
                            });
                        }
                        if (value.operator == 'max') {
                            range = this.new_price.slider("getValue");
                            range[1] = value.value;
                            this.new_price.slider("setValue", range);
                            FilterBuilder.addFilter('new_price_max', {
                                field: "new_price",
                                operator: "max",
                                value: parseInt(range[1])
                            });
                        }
                    }
                    if (value.field == "old_price") {
                        if (value.operator == 'min') {
                            range = this.old_price.slider("getValue");
                            range[0] = value.value;
                            this.old_price.slider("setValue", range);
                            FilterBuilder.addFilter('old_price_min', {
                                field: "old_price",
                                operator: "min",
                                value: parseInt(range[0])
                            });
                        }
                        if (value.operator == 'max') {
                            range = this.old_price.slider("getValue");
                            range[1] = value.value;
                            this.old_price.slider("setValue", range);
                            FilterBuilder.addFilter('old_price_max', {
                                field: "old_price",
                                operator: "max",
                                value: parseInt(range[1])
                            });
                        }
                    }
                    if (value.field == "profit") {
                        if (value.operator == 'min') {
                            range = this.profit.slider("getValue");
                            range[0] = value.value;
                            this.profit.slider("setValue", range);
                            FilterBuilder.addFilter('profit_min', {
                                field: "profit",
                                operator: "min",
                                value: parseInt(range[0])
                            });
                        }
                        if (value.operator == 'max') {
                            range = this.profit.slider("getValue");
                            range[1] = value.value;
                            this.profit.slider("setValue", range);
                            FilterBuilder.addFilter('profit_max', {
                                field: "profit",
                                operator: "max",
                                value: parseInt(range[1])
                            });
                        }
                    }
                    // Выбор даты
                    if (value.field == "created_at") {
                        if (value.operator == 'start') {
                            this.date_start.datepicker("update", new Date(value.value));
                            this.date_start.datepicker("update");
                            FilterBuilder.addFilter('date_start', {
                                field: "created_at",
                                operator: "start",
                                value: value.value
                            });
                        }
                        if (value.operator == 'end') {
                            this.date_end.datepicker("update", new Date(value.value));
                            this.date_end.datepicker("update");
                            FilterBuilder.addFilter('date_end', {
                                field: "created_at",
                                operator: "end",
                                value: value.value
                            });
                        }
                    }
                    this.profit.data('slider').refresh();
                    this.old_price.data('slider').refresh();
                    this.new_price.data('slider').refresh();
                },

                //Устанавливает значения фильтров-слайдеров по мин и макс значениям.
                setSliderValuesByLimits: function () {
                    this.profit.data('slider').options.value = [parseInt(this.limits.profit.min), parseInt(this.limits.profit.max)];
                    this.old_price.data('slider').options.value = [parseInt(this.limits.old_price.min), parseInt(this.limits.old_price.max)];
                    this.new_price.data('slider').options.value = [parseInt(this.limits.new_price.min), parseInt(this.limits.new_price.max)];

                    this.profit.data('slider').refresh();
                    this.old_price.data('slider').refresh();
                    this.new_price.data('slider').refresh();
                },

                initSlidersValues: function (data) {
                    this.profit.data('slider').options.value = [parseInt(data.profit.min), parseInt(data.profit.max)];
                    this.old_price.data('slider').options.value = [parseInt(data.old_price.min), parseInt(data.old_price.max)];
                    this.new_price.data('slider').options.value = [parseInt(data.new_price.min), parseInt(data.new_price.max)];

                    this.profit.data('slider').refresh();
                    this.old_price.data('slider').refresh();
                    this.new_price.data('slider').refresh();
                },

                initSlidersMaxMin: function () {
                    this.new_price.data('slider').options.min = parseInt(this.limits.new_price.min);
                    this.new_price.data('slider').options.max = parseInt(this.limits.new_price.max);
                    this.new_price.slider("setValue", this.new_price.slider("getValue")); //force the view refresh
                    this.new_price.data('slider').refresh();

                    this.old_price.slider().data('slider').options.min = parseInt(this.limits.old_price.min);
                    this.old_price.slider().data('slider').options.max = parseInt(this.limits.old_price.max);
                    this.old_price.slider("setValue", this.old_price.slider("getValue")); //force the view refresh
                    this.old_price.data('slider').refresh();

                    this.profit.slider().data('slider').options.min = parseInt(this.limits.profit.min);
                    this.profit.slider().data('slider').options.min = parseInt(this.limits.profit.max);
                    this.profit.slider("setValue", this.profit.slider("getValue")); //force the view refresh
                    this.profit.data('slider').refresh();
                },

                initDatesMaxMin: function () {
                    this.date_start.datepicker("update", new Date(this.limits.created_at.min));
                    this.date_end.datepicker("update", new Date(this.limits.created_at.max));
                }
            };
            FilterBuilder['keyGenerator'] = function (filter) {
                var intervals = ['old_price', 'new_price', 'profit'];
                if ($.inArray(filter.field, intervals) >= 0) {
                    return filter.field + "_" + filter.operator;
                }
                return filter.field;
            };

            $.getScript("/js/kendo.toolslab-grid.base.js", function () {
                GridController.init();
                GridController.initGrid();
            });
        });
    </script>
@stop