@extends('layouts.menubar_general')

@section('right-header-button')
    @include('partials.cabinet.button_card')
@stop
@section('left-header-buttons')
        <li>
            <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);" data-original-title="" title="">
                <i class="fa fa-bars"></i>
            </a>
        </li>
        <li>
            <a class="btn btn-icon-toggle filter_btn" data-toggle="menubar1" href="javascript:void(0);" id="trigger" data-original-title="" title="">
                <i class="md md-account-box"></i>
            </a>
        </li>

@stop
@section('content')
    <label id="autocomplete1" class="form-control autocomplete" style="display: none;"></label><!--стили без нее форм не работают-->
    <div  class="card-body tab-content filter-page card">
        <div class="tabs-left">
            @include('crm.card.partials.firm_and_clients')
            <section class="style-default-bright client-info tab-pane active" id="first9">
                <div class="tab-content style-default">
                    <div class="tab-pane active" id="first5">
                        <div class="col-sm-12">
                            <div class="card-head">
                                <ul class="nav nav-tabs nav-justified nav-styled" data-toggle="tabs">
                                    <li class="active"><a href="#history">История</a></li>
                                    <li><a href="#tasks"><span>Задачи</span>
                                            <span id="old-tasks" class="msg style-danger">0</span>
                                            / <span id="count-tasks">0</span></a></li>
                                    <li><a href="#orders"><span>Сделки</span><span class="msg style-warning">0</span></a></li>
                                    <li><a href="#statistics">Статистика</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                @include('crm.card.partials.history')
                                @include('crm.card.partials.tasks')
                                @include('crm.card.partials.orders')
                                @include('crm.card.partials.statistics')
                            </div>
                        </div>
                    </div><!--end tab-pane-->
                </div>
            </section>
        </div>
    </div>

    <div class="offcanvas create-company">
        <div id="offcanvas-search" class="offcanvas-pane width-8">
            <div class="offcanvas-body">
                <div class="card-head tables-tabs">
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#comments-right">Комментарий</a></li>
                        <li><a href="#task-right">Задача</a></li>
                        <li><a href="#order-right">Сделка</a></li>
                    </ul>
                </div><!--end .card-head -->
                <br/>
                <div class="tab-content">
                    @include('crm.card.partials.add-comment-right')
                    @include('crm.card.partials.add-task-right')
                    @include('crm.card.partials.add-order-right')
                    <!--end tab-pane-->
                </div>
            </div><!--end .card-body -->
        </div>
    </div>

@stop
@section('modals')
    <div class="modal fade" id="modal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header text-center text-primary">
                    <span class="close close1 btn btn-icon-toggle text-primary" data-dismiss="modal" aria-hidden="true" data-toggle="tooltip" data-placement="auto" title="Закрыть"><i class="md md-close"></i></span>
                    <div class="pull-right">
                        <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Редактировать"><i class="fa fa-pencil"></i></span>
                    </div>
                    <h3 class="modal-title text-primary">Название</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped table-edit">
                        <caption>Название таблицы</caption>
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th class="hide">С/с</th>
                                    <th>Стоимость</th>
                                    <th>Кол&nbsp;-&nbsp;во</th>
                                    <th>Итого</th>
                                </tr>
                            </thead>
                        <tbody>
                        <tr>
                            <td>Мыло с длинным названием каким-то</td>
                            <td class="hide">10000</td>
                            <td><div contenteditable>10000</div></td>
                            <td><div contenteditable>100</div></td>
                            <td>1000000</td>
                        </tr>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-striped">
                        <caption>Название таблицы</caption>
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th class="hide">С/с</th>
                            <th>Стоимость</th>
                            <th>Кол&nbsp;-&nbsp;во</th>
                            <th>Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Мыло с длинным названием каким-то</td>
                            <td class="hide">10000</td>
                            <td><div contenteditable>10000</div></td>
                            <td><div contenteditable>100</div></td>
                            <td>1000000</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-default-light ink-reaction">Отменить</button>
                        <button type="submit" class="btn ink-reaction btn-primary">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@include('crm.card.partials.templates')
@section('scripts')
    <script type="application/javascript" src="/js/script-card.js"></script>
    <script type="application/javascript" src="/js/handlebars.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var FirmService = (function(){
                var endpoint = '/crm/card/',
                        self = this,
                        errorMessage = 'Произошла не предвиденная ошибка. Обратитесь в службу поддержки';
                return {
                    attach: function(clientIds, firmId) {
                        $.ajax({
                            url: endpoint + 'ajaxAttachFirm',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                clientIds: clientIds,
                                firmId: firmId
                            }
                        }).error(function(e) {
                            console.log(e);
                            toastr.error(errorMessage);

                        }).success(function(response){
                            console.log(response);
                            toastr.success('Вы прекрепили компанию к контактам');
                        })
                    },

                    create: function(clientIds, data) {
                        $.ajax({
                            url: endpoint + 'ajaxCreateFirm',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                data: data,
                                clientIds: clientIds
                            }
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },
                    update: function(firmId, data) {
                        $.ajax({
                            url: endpoint + 'ajaxUpdateFirm/' + firmId,
                            type: 'PUT',
                            dataType: 'json',
                            data: {
                                data: data
                            }
                        })
                    },

                    detach: function(firmId) {
                        $.ajax({
                            url: endpoint + 'ajaxUpdateFirm/' + id,
                            type: 'PUT',
                            dataType: 'json',
                            data: {
                                data: data
                            }
                        }).error(function(e){
                            console.log(e);
                        }).success(function(response){
                            console.log(response);
                        });
                    },

                    remove: function(firmId) {
                        $.ajax({
                            url: endpoint + 'ajaxRemoveFirm/' + id,
                            type: 'DELETE',
                            dataType: 'json',
                        }).error(function(e){
                            console.log(e);
                        }).success(function(response){
                            console.log(response);
                        });
                    },


                    getTasks: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetTasksByFirm/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },


                    getStatistics: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetStatisticsByFirm/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },

                    getOrders: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetOrdersByFirm/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },

                    find: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGeFirm/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response);
                        });
                    }
                };
            });

            var ClientService = (function(callback) {
                var endpoint = '/{{$currentCompany->name}}/crm/card/',
                        self = this,
                        data = {},
                        errorMessage = 'Произошла не предвиденная ошибка. Обратитесь в службу поддержки';


                return {
                    attach: function(firmId, clientId) {
                        return $.ajax({
                            url: endpoint + 'ajaxAttachClient',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                clientId: clientId,
                                firmId: firmId
                            }
                        }).error(function(e) {
                            console.log(e);
                            toastr.error(errorMessage);
                        });
                    },
                    create: function(firmId, data) {
                        $.ajax({
                            url: endpoint + 'ajaxCreateClient',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                data: data
                            }
                        }).error(function(e) {
                            console.log(e);
                            toastr.error(errorMessage);
                        }).success(function(response){
                            console.log(response);
                            toastr.success('Создание прошло успешно');
                        });
                    },

                    detach: function(clientId) {
                        $.ajax({
                            url: endpoint + 'ajaxDetachClient',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                clientId: clientId
                            }
                        }).error(function(e) {
                            console.log(e);
                            toastr.error(errorMessage);
                        }).success(function(response){
                            console.log(response);
                            toastr.success('Вы успешно открепили контакт');
                        });
                    },
                    remove: function(clientId) {
                        $.ajax({
                            url: endpoint + 'ajaxRemoveClient',
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                clientId: clientId
                            }
                        }).error(function(e) {
                            console.log(e);
                            toastr.error(errorMessage);
                        }).success(function(response){
                            console.log(response);
                            toastr.success('Вы успешно удалили контакт');
                        });
                    },

                    update: function(clientId, data) {
                        $.ajax({
                            url: endpoint + 'ajaxUpdateClient/' + clientId,
                            type: 'PUT',
                            dataType: 'json',
                            data: {
                                data: data
                            }
                        }).error(function(e){
                            console.log(e);
                            toastr.error(errorMessage);

                        }).success(function(response) {
                            console.log(response);
                            toastr.success('Вы успешно обновили информацию по контакту');
                        });
                    },

                    getOrders: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetOrdersByClientId/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },

                    getTasks: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetTasksByClientId/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },

                    find: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetClient/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e){
                            console.log(e);
                            toastr.error(errorMessage);
                        }).success(function(response){
                            callback(response);
                        });

                    }
                };
            });

            var TaskService = (function(){
                var endpoint = '/{{$currentCompany->name}}/crm/card/',
                        self = this,
                        errorMessage = 'Произошла не предвиденная ошибка. Обратитесь в службу поддержки';
                return {
                    find: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetTask/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },
                    findAll: function(ids) {
                        $.ajax({
                            url: endpoint + 'ajaxGetTasks',
                            type: 'POST',
                            dataType: 'json',
                            data: {ids: ids}
                        }).error(function(e){
                            console.log(e);
                        }).success(function(r){
                            console.log(r);
                        });
                    },
                    create: function(data) {
                        $.ajax({
                            url: endpoint + 'ajaxCreateTask',
                            type: 'POST',
                            dataType: 'json',
                            data: {data: data}
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(r){
                            console.log(r);
                        })
                    },
                    update: function(id, data) {
                        $.ajax({
                            url: endpoint + 'ajaxUpdateTask/' + id,
                            type: 'PUT',
                            dataType: 'json',
                            data: {
                                data: data
                            }
                        }).success(function(response){
                            toastr.success('Задача быпа успешно обновлена');
                        }).error(function(e){
                            toastr.error(errorMessage)
                        });

                    },

                    remove: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxRemoveTask/' + id,
                            type: 'DELETE',
                            dataType: 'json'
                        }).success(function(r){
                            console.log(r);
                            toastr.success('Задача была обновлена');
                        }).error(function(e){
                            console.log(e);
                            toastr.error(errorMessage)
                        })
                    },
                    done: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxDoneTask/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(err){
                            console.log(err);
                            toastr.error(errorMessage);
                        }).success(function(response) {
                            console.log(response);
                            toastr.success(errorMessage);
                        });
                    }
                };
            });

            var StatisticsService = (function() {

            });

            var OrderService = (function() {
                var endpoint = '/{{$currentCompany->name}}/crm/card/',
                        self = this,
                        errorMessage = 'Произошла не предвиденная ошибка. Обратитесь в службу поддержки';

                return {
                    find: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxGetOrder/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(response){
                            console.log(response)
                        });
                    },
                    findAll: function(ids) {
                        $.ajax({
                            url: endpoint + 'ajaxGetOrdes',
                            type: 'POST',
                            dataType: 'json',
                            data: {ids: ids}
                        }).error(function(e){
                            console.log(e);
                        }).success(function(r){
                            console.log(r);
                        });
                    },
                    create: function(data) {
                        $.ajax({
                            url: endpoint + 'ajaxCreateOrder',
                            type: 'POST',
                            dataType: 'json',
                            data: {data: data}
                        }).error(function(e) {
                            console.log(e);
                        }).success(function(r){
                            console.log(r);
                        })
                    },
                    update: function(id, data) {
                        $.ajax({
                            url: endpoint + 'ajaxUpdateOrder/' + id,
                            type: 'PUT',
                            dataType: 'json',
                            data: {
                                data: data
                            }
                        }).success(function(response){
                            toastr.success('Задача быпа успешно обновлена');
                        }).error(function(e){
                            toastr.error(errorMessage)
                        });

                    },

                    remove: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxRemoveOrder/' + id,
                            type: 'DELETE',
                            dataType: 'json'
                        }).success(function(r){
                            console.log(r);
                            toastr.success('Задача была обновлена');
                        }).error(function(e){
                            console.log(e);
                            toastr.error(errorMessage)
                        })
                    },
                    done: function(id) {
                        $.ajax({
                            url: endpoint + 'ajaxDoneOrder/' + id,
                            type: 'GET',
                            dataType: 'json'
                        }).error(function(err){
                            console.log(err);
                            toastr.error(errorMessage);
                        }).success(function(response) {
                            console.log(response);
                            toastr.success(errorMessage);
                        });
                    }
                }
            });

            var body = $('body');


            var FirmTemplate = (function() {
                var source, template;
                return {
                    edit: function(firm){
                        source = $('#template-edit-firm-card').html();
                        template = Handlebars.compile(source);
                        return template(firm);
                    },
                    attach: function() {
                        source = $('#template-attach-firm-card').html();
                        template = Handlebars.compile(source);
                        return template();
                    },
                    renderBlock: function(data) {
                        if (firm = data.firm) {
                            $('#firm-block').append(FirmTemplate().edit(firm));
                        } else {
                            $('#firm-block').append(FirmTemplate().attach());
                        }
                        source = $('#template-edit-client-card').html();
                        template = Handlebars.compile(source);
                        $(data.clients).each(function(index, client){
                            $('#card-client').append(template({client: client}));
                        });
                    }
                }
            });


            body.on('loadPage', function(event) {
                console.log('Initial page');
                var firm, clients, orders, tasks, firmId, clientId, result;
                firmId = '{{$firmId}}';
                clientId = '{{$clientId}}';

                if (firmId) {
                    result = FirmService().find(firmId);
                } else if (clientId) {
                    ClientService(FirmTemplate().renderBlock).find(clientId);
                }

            });

            $('.edit-order').on('click', function() {
                var id = $(this).attr('data-id');
                $(this).trigger('editOrder', [id]);
            });

            $('.card-order').on('click', function(){
                var id = $(this).attr('data-id');
                $(this).trigger('cardOrder', [id]);
            });

            $('.edit-task').on('click', function() {
                var id = $(this).attr('data-id');
                $(this).trigger('doneTask', [id]);
            });

            $('.done-task').on('click', function() {
                var id = $(this).attr('data-id');
                $(this).trigger('doneTask', [id]);
            });

            $('#client-name').on('change', function() {
                $(this).trigger('changeClientName');
            });

            $('[id^="client-orders-"]').on('click', function(){
                var id = $(this).attr('id').match(/[1-9]/gi);
                $(this).trigger('clientOrders', [ id ]);
            });

            $('[id^="client-tasks-"]').on('click', function(){
                var id = $(this).attr('id').match(/[1-9]/gi);
                $(this).trigger('clientTasks', [ id ]);
            });

            $('.remove-client').on('click', function(){
                $(this).trigger('removeClient', [
                    $(this).attr('data-id')
                ]);
            });

            $('.edit-client').on('click', function(){
                $(this).trigger('editClient', [
                    $(this).attr('data-id')
                ]);
            });

            $('.deattach-client').on('click', function(){
                $(this).trigger('deattachClient', [
                        $(this).attr('data-id')
                ]);
            });

            $('#save-add-firm').on('click', function() {
                $(this).trigger('saveAddFirm');
            });

            $('#cancel-add-firm').on('click', function(){
                $(this).trigger('cancelAddFirm');
            });

            $('#firm-name').on('change', function(){
                $(this).trigger('changeFirmName');
            });

            $('#firm-orders').on('click', function() {
                $(this).trigger('firmOrders');
            });

            $('#firm-tasks').on('click', function() {
                $(this).trigger('firmTasks');
            });

            $('#firm-information').on('click', function() {
                $(this).trigger('firmInformation');
            });

            $('#add-attach-firm').on('click', function(){
                $(this).trigger('addAttachFirm');
            });

            $('#cancel-attach-firm').on('click', function(){
                $(this).trigger('cancelAttachFirm');
            });

            $('#add-firm').on('click', function(){
                $(this).trigger('addFirm');
            });

            body.trigger('loadPage');

        });
    </script>
@stop