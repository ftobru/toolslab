<div class="tab-pane" id="second8">
    <form class="form-horizontal floating-label">
        <div class="form-group">
            <label class="col-sm-2 control-label">Контакт/сделка </label>
            <div class="col-xs-9 pull-right">
                <input type="text" class="form-control input-sm" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Сроки </label>
            <div class="col-xs-9 pull-right">
                <dl class="dropdown">
                    <dt><a><i class="md md-event"></i><span>Дата и время</span></a></dt>
                    <dd>
                        <ul class="list-unstyled">
                            <li><a>Завтра</a></li>
                            <li><a>Следующая неделя</a></li>
                            <li><a>Следующий месяц</a></li>
                            <li><a>Следующий год</a></li>
                            <li>
                                <div class="input-group date input-sm col-xs-11" id="demo-date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="input-group-content">
                                        <input type="text" class="form-control">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-7">
                                        <div class="input-group date input-sm" id="demo-date">
                                            <span class="input-group-addon"><i class="md md-schedule"></i></span>
                                            <div class="input-group-content">
                                                <select id="select1" name="select1" class="form-control">
                                                    <option value="">Любое</option>
                                                    <option value="08:30">08:30</option>
                                                    <option value="09:00">09:00</option>
                                                    <option value="09:30">09:30</option>
                                                    <option value="10:00">10:00</option>
                                                    <option value="10:30">10:30</option>
                                                </select>
                                                <label for="select1"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5">
                                        <button type="submit" class="btn btn-primary ink-reaction btn-xs btn-styled">Сохранить</button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </dd>
                </dl>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Кто </label>
            <div class="col-xs-9 pull-right">
                <select class="form-control select2-list input-sm" data-placeholder="Select an item">
                    <optgroup label="Alaskan/Hawaiian Time Zone">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                    </optgroup>
                    <optgroup label="Central Time Zone">
                        <option value="AL">Alabama</option>
                        <option value="AR">Arkansas</option>
                        <option value="IL">Illinois</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Тип </label>
            <div class="col-xs-9 pull-right">
                <select class="form-control select2-list input-sm" data-placeholder="Select an item">
                    <optgroup label="Alaskan/Hawaiian Time Zone">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                    </optgroup>
                    <optgroup label="Central Time Zone">
                        <option value="AL">Alabama</option>
                        <option value="AR">Arkansas</option>
                        <option value="IL">Illinois</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Задача </label>
            <div class="col-xs-9 pull-right">
                <textarea class="form-control input-sm"></textarea>
            </div>
        </div>
        <div class="form-group tags-wrap">
            <label id="autocomplete4" class="control-label col-sm-2" data-source="" >Теги</label>
            <div class="col-xs-9 pull-right">
                <select class="form-control select2-list input-sm"  multiple>
                    <optgroup label="Alaskan/Hawaiian Time Zone1">
                        <option value="AK">Alaska</option>
                        <option value="HI">Hawaii</option>
                    </optgroup>
                    <optgroup label="Pacific Time Zone1">
                        <option value="CA">California</option>
                        <option value="NV">Nevada</option>
                        <option value="OR">Oregon</option>
                        <option value="WA">Washington</option>
                    </optgroup>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12 text-center">
                <button type="submit" class="btn btn-default-light ink-reaction btn-sm">Удалить задачу</button>
                <button type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранение</button>
            </div>
        </div>
        <div class="text-center">
            <button type="submit" id="add-task" class="btn ink-reaction btn-warning btn-lg"><i class="md md-done"></i> Завершить задачу</button>
        </div>
    </form>
</div>