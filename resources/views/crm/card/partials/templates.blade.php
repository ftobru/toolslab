<script id="template-attach-firm-card" type="text/x-handlebars-template">
    <li>
        <div id="card-firm" class="card card-bordered company-card">
            <div class="card-head card-head-xs">
                <header data-toggle="tooltip" data-placement="top" title="" data-original-title="">Компания</header>
                <div class="tools">
                    <div class="btn-group">
                        <span
                                class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover" data-placement="right" data-html="true" data-content="<div class=&quot;content-popover&quot;><a id=&quot;action-create-company&quot; href=&quot;#&quot;><i class=&quot;md md-playlist-add&quot;></i>Создать компанию</a>
                                <a id=&quot;action-attach-company&quot; href=&quot;#&quot;><i  class=&quot;md md-attach-file&quot;></i>Привязать компанию</a></div>" data-original-title="" title="">
                            <i class="md md-playlist-add"></i>
                        </span>
                    </div>
                </div>
            </div>
            <!-- Block attach -->
            <div class="card-body" data-type="firm">
                <div class="form-group no-padding">
                    <input id="add-firm" class="form-control select2-list input-sm add_object" data-placeholder="Выберите компанию"title="">
                    <select>
                        <option>&nbsp;</option>
                        <optgroup>
                            @if(isset($allFirms))
                                @foreach($allFirms as $aFirm)
                                    <option value="{!!$aFirm->id!!}">{{$aFirm->name}}</option>
                                @endforeach
                            @else
                                <option value=""></option>
                            @endif
                        </optgroup>
                    </select>
                </div>
                <div class="form-group text-center">
                    <button id="cancel-attach-firm" class="btn btn-default ink-reaction btn-sm">Отменить</button>
                    <button id="add-attach-firm" class="btn ink-reaction btn-primary btn-sm" disabled>Добавить</button>
                </div>
            </div>
        </div>
    </li>
</script>
<!-- Фирма -->
<script id="template-edit-firm-card" type="text/x-handlebars-template">
<li class="active">
    <a href="#">
        <div class="card card-bordered">
            <form class="form form-validate" novalidate="novalidate">
                <div class="card-head card-head-xs">
                    <header data-toggle="tooltip" data-placement="top" title="@{{#with firm}} @{{name}} @{{/with }}">@{{#with firm}} @{{name}} @{{/with }}</header>
                    @{{#with firm }}
                    <div class="tools">
                        <div class="btn-group">
                            <span class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></span>
                            <span class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover" data-placement="right" data-html="true"
                                  data-content='<div class="content-popover"><a href="#"><i class="md md-attach-file"></i>Отвязать компанию</a><a href="#"><i class="md md-mode-edit"></i>
                                  Редактировать</a><a href="#"><i class="md md-delete"></i>Удалить компанию</a></div>'><i class="md md-keyboard-control"></i></span>
                        </div>
                    </div>
                    @{{/with }}
                </div>
                <div class="card-body" data-type="firm" data-id="@{{#with firm }} @{{id}} @{{/with}}">
                    <div class="card-head">
                        <ul class="nav nav-tabs inside-tabs" data-toggle="tabs">
                            <li class="active text-sm"><a href="#firm-information">Информация</a></li>
                            <li class="text-sm"><a href="#firm-tasks">Задачи
                                    <span class="msg style-danger count-orders">
                                        @{{#with firm}} @{{ tasks.length }} @{{else}} 0 @{{/with }} </span></a></li>
                            <li class="text-sm"><a href="#firm-orders">Сделки <span class="msg style-warning">
                                        @{{#with firm}} @{{ orders.length }} @{{else}} 0 @{{/with }}</span></a></li>
                        </ul>
                    </div>
                    <div class="small-padding tab-content">
                        <div class="tab-pane active" id="firm-information">
                            <div class="no-padding">
                                <table>
                                    <tr>
                                        <td>Название</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" @{{#with firm}} disabled="disabled" @{{/with }} value="@{{#with firm}} @{{name}} @{{/with }}" class="form-control input-sm" id="firm-name" placeholder="..."/>
                                                <div class="form-control-line"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Рабочий</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text"  @{{#with firm}} disabled="disabled" @{{/with }} value="@{{#with firm}} @{{phone}} @{{/with}}"  class="form-control input-sm" id="firm-phone-work" placeholder="..."/>
                                                <div class="form-control-line"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Прочий</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" @{{#with firm}} disabled="disabled" @{{/with }} value="@{{#with firm}} @{{other_phone}} @{{/with}}" class="form-control input-sm" id="firm-other-phone" placeholder="...">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="btn btn-icon-toggle"><i class="md md-add-circle-outline"></i></span>
                                        </td>
                                    </tr>
                                </table>
                                <div class="collapse" id="accordion8-1">
                                    <table>
                                        <tr>
                                            <td>
                                                Теги
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select @{{#with firm}} disabled="disabled" @{{/with }}  id="firm-tags" class="form-control select2-list input-sm" multiple>
                                                        <optgroup label="Теги">
                                                            @if(Crm\Models\Firm::existingTags()->count() > 0)
                                                            @foreach(Crm\Models\Firm::existingTags() as $tag)
                                                                <option value="{{$tag}}">{{$tag}}</option>
                                                            @endforeach
                                                                @endif
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="firm-control-button" hidden="hidden">
                                            <td colspan="2">
                                                <ul class="list-inline text-center">
                                                    <li><button id="cancel-add-firm" class="btn btn-default btn-xs margin-bottom-xl">Отменить</button></li>
                                                    <li><button id="save-add-firm" class="btn btn-primary btn-xs margin-bottom-xl">Сохранить</button></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="tools more-info" data-toggle="collapse" data-target="#accordion8-1" aria-expanded="true"
                                     role="alert">
                                    <span class="btn btn-icon-toggle btn-collapse"><i class="md md-keyboard-control"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="firm-tasks" data-id="@{{#with firm }} @{{firm-id}} @{{/with }}" data-information="tasks">
                            @{{#with firm }}
                                @{{#with tasks }}
                                    @{{#each tasks }}
                                    <div class="client-card card alert alert-callout alert-danger margin-bottom-xl style-danger-light"><a href="#" data-original-title="" title="">
                                        </a><div class="card-head card-head-xs"><a href="#" data-original-title="" title="">
                                            </a><header><a href="#" data-original-title="" title="">
                                                    <span data-original-title="" title=""><i class="md md-insert-invitation"></i></span>
                                                    <span data-original-title="" title=""><span class="opacity-75 text-sm" data-original-title="" title="">@{{created_at}}</span></span>
                                                </a><a href="#" class="text-primary text-sm" data-original-title="" title="">@{{ user }}</a>
                                            </header>
                                            <div class="pull-right opacity-50">
                                                <a class="btn btn-close" data-original-title="" title=""><i class="md md-done-all"></i></a>
                                                <a class="btn btn-close" data-original-title="" title=""><i class="fa fa-pencil"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    @{{ /each }}
                                @{{ else }}
                                    Не одной задачи еще не было создано.
                                @{{/with }}

                            @{{ else }}
                                Компания находится в процессе создания.
                            @{{/with }}
                        </div>
                        <div class="tab-pane text-xs" id="firm-orders" data-information="orders">
                            @{{#with firm }}
                                @{{#with orders }}
                                    @{{ #each orders}}
                                        <div class="client-card card alert alert-callout alert-warning margin-bottom-xl"><a href="#" data-original-title="" title="">
                                            </a><div class="card-head card-head-xs"><a href="#" data-original-title="" title="">
                                                </a><header><a href="#" data-original-title="" title="">
                                                        <span data-original-title="" title=""><i class="md md-local-grocery-store"></i></span>
                                                        <span class="text-warning" data-original-title="" title="">@{{id}} </span>
                                                        <span class="opacity-75 text-sm" data-original-title="" title="">@{{ created_at }}</span>
                                                    </a><a href="#" class="text-primary text-sm" data-original-title="" title="">Админ Админов</a>
                                                    <div class="wrapper">
                                                        <p class="pull-left"><span data-original-title="" title="">@{{ order.cart.summary_price }}</span> руб</p>
                                                        <p class="pull-left"><span class="style-danger status" data-original-title="" title="">@{{ order.status }}</span></p>
                                                    </div>
                                                </header>
                                                <div class="pull-right opacity-50">
                                                    <a class="btn btn-close" data-original-title="" title=""><i class="md md-assignment"></i></a>
                                                    <a class="btn btn-close" data-original-title="" title=""><i class="fa fa-pencil"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    @{{ /each }}
                                @{{ else }}
                            Задач пока не было создано
                                @{{/with }}

                                @{{ else }}
                            Компания находится в процессе создания.
                            @{{/with }}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </a>
</li>

</script>
<!-- Редактирование карточки контакта -->
<script id="template-edit-client-card" type="text/x-handlebars-template">
    @{{log client}}
    <li>
        <a href="#send" data-original-title="@{{#with client}}@{{name}}@{{/with}}" title=""></a>

        <div  data-type="client"  class="card card-bordered">
            <a href="#" data-original-title="" title=""></a>
            <form class="form form-validate" novalidate="novalidate"><a href="#" data-original-title="" title="">
                    <div class="card-head card-head-xs">
                        <header id="add-client-header@{{#with client}}-@{{id}} @{{/with }}" data-toggle="tooltip" data-placement="auto" title=""
                                data-original-title="">
                            @{{#with client}} @{{name}} @{{/with }}
                        </header>
                        @{{#with client}}
                        <div class="tools">
                            <div class="btn-group">
                                <span class="btn btn-icon-toggle btn-collapse" data-original-title="" title=""><i
                                            class="fa fa-angle-down"></i></span>

                                <span class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover"
                                      data-placement="right"
                                      data-content='<div class=&quot;content-popover&quot;>
                                      <a class="deattach-client&quot; data-id=&quot;@{{id}}&quot; href=&quot;#&quot;>
                                      <i class="md md-attach-file &quot;></i>Отвязать контакт</a>
                                      <a href="&quot; class=&quot;edit-client&quot; data-id=&quot;@{{id}}&quot;><i class=&quot;md md-mode-edit &quot;></i>Редактировать</a>
                                      <a href=&quot;#&quot; class=&quot;remove-client&quot; data-id=&quot;@{{id}}&quot;><i class=&quot;md md-delete&quot; ></i>Удалить контакт</a>
                                      </div>'
                                      data-original-title="" title=""><i class="md md-keyboard-control"></i></span>
                            </div>
                        </div>
                        @{{/with}}
                    </div>
                </a>

                <div class="card-body"><a href="#" data-original-title="" title="">
                    </a>

                    <div class="card-head"><a href="#" data-original-title="" title="">
                        </a>
                        <ul class="nav nav-tabs inside-tabs" data-toggle="tabs"><a href="#"
                                                                                   data-original-title="" title="">
                            </a>
                            <li class="active text-sm"><a href="#" class="" data-original-title="" title=""></a><a
                                        href="#client-information@{{#with client}}-@{{id}} @{{/with}}" data-original-title="" title="">Информация</a></li>
                            <li class="text-sm"><a href="#client-tasks@{{#with client}}-@{{id}} @{{/with}}" data-original-title="" title="">Задачи <span
                                            class="msg style-danger" data-original-title="" title="">@{{#with client }}@{{ tasks.length }}@{{ else }} 0 @{{/with}}</span></a></li>
                            <li class="text-sm"><a href="#client-orders@{{#with client}}-@{{ id }} @{{/with}}"  data-original-title="" title="">Сделки <span
                                            class="msg style-warning" data-original-title="" title="">@{{#with client}} @{{tasks.length}} @{{else}} 0 @{{/with}}</span></a></li>
                        </ul>
                    </div>
                    <div class="small-padding tab-content">
                        <div class="tab-pane active" id="first02">
                            <div class="no-padding">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Имя</td>
                                        <td>
                                            <div class="form-group">
                                                <input id="client-name@{{#with client}}-@{{id }} @{{/with}} " @{{#with client}} disabled="disabled" @{{/with}} type="text" name="name"
                                                       value="@{{#with client}} @{{name }} @{{/with}}" class="form-control input-sm"
                                                        placeholder="...">
                                                <div class="form-control-line"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Рабочий</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" value="@{{#with client}}@{{phone}}@{{/with}}" class="form-control input-sm" name="phone"
                                                       id="client-phone@{{#with client}}-@{{id}} @{{/with}}" @{{#with client}} disabled="disabled" @{{/with}} placeholder="...">

                                                <div class="form-control-line"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Прочий</td>
                                        <td>
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm"
                                                       value="@{{#with client}}@{{phone}}@{{/with }}" @{{#with client}}
                                                       disabled="disabled" @{{/with}} id="client-other-phone@{{#with client}}-@{{id}} @{{/with}}"
                                                       placeholder="...">

                                                <div class="form-control-line"></div>
                                            </div>
                                            <span class="btn btn-icon-toggle" data-original-title="" title=""><i
                                                        class="md md-add-circle-outline"></i></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div class="collapse" id="accordion8-2">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                Метки
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <select class="form-control select2-list input-sm" multiple="" @{{#with firm}} disabled="disabled" @{{/with }}>
                                                        <optgroup label="Метки">
                                                            @if(Crm\Models\Client::existingTags()->count() > 0)
                                                            @foreach(Crm\Models\Client::existingTags() as $tag)
                                                                <option value="{{$tag}}">{{$tag}}</option>
                                                            @endforeach
                                                                @endif
                                                        </optgroup>
                                                    </select>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td colspan="2">
                                                <ul class="list-inline text-center">
                                                    <li><button class="btn btn-default btn-xs margin-bottom-xl">Отменить</button></li>
                                                    <li><button disabled="disabled" class="btn btn-primary btn-xs margin-bottom-xl">Сохранить</button></li>
                                                </ul>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tools more-info" data-toggle="collapse" data-target="#accordion8-2"
                                     aria-expanded="true" role="alert">
                                    <span class="btn btn-icon-toggle btn-collapse" data-original-title="" title=""><i
                                                class="md md-keyboard-control"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="client-tasks@{{#with client}}-@{{id}} @{{/with}}" data-id="@{{#with client}} @{{id}} @{{/with}}" data-information="tasks">
                            @{{#with client}}
                                @{{#with tasks}}
                                    @{{#each tasks}}
                                    <div class="client-card card alert alert-callout alert-danger margin-bottom-xl style-danger-light"><a href="#" data-original-title="" title="">
                                        </a><div class="card-head card-head-xs"><a href="#" data-original-title="" title="">
                                            </a>
                                            <header>
                                                <a href="#" data-original-title="" title="">
                                                    <span data-original-title="" title=""><i class="md md-insert-invitation"></i></span>
                                                    <span data-original-title="" title=""><span class="opacity-75 text-sm" data-original-title="" title="">@{{created_at}}</span></span>

                                                </a>
                                                <a href="#" class="text-primary text-sm" data-original-title="" title="">@{{ user }}</a>
                                            </header>
                                            <div class="pull-right opacity-50">
                                                <a class="btn btn-close" data-original-title="" title=""><i class="md md-done-all"></i></a>
                                                <a class="btn btn-close" data-original-title="" title=""><i class="fa fa-pencil"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    @{{/each }}
                                @{{else}}
                                        Не одной задачи еще не было создано.
                                @{{/with }}

                                    @{{ else }}
                                Компания находится в процессе создания.
                            @{{/with }}
                        </div>
                        <div class="tab-pane text-xs" id="firm-orders" data-information="orders">
                            @{{#with client}}
                                @{{#with orders}}
                                    @{{#each orders}}
                                    <div class="client-card card alert alert-callout alert-warning margin-bottom-xl"><a href="#" data-original-title="" title="">
                                        </a><div class="card-head card-head-xs"><a href="#" data-original-title="" title="">
                                            </a><header><a href="#" data-original-title="" title="">
                                                    <span data-original-title="" title=""><i class="md md-local-grocery-store"></i></span>
                                                    <span class="text-warning" data-original-title="" title="">@{{id}} </span>
                                                    <span class="opacity-75 text-sm" data-original-title="" title="">@{{ created_at }}</span>
                                                </a><a href="#" class="text-primary text-sm" data-original-title="" title="">@{{ user.name }}</a>
                                                <div class="wrapper">
                                                    <p class="pull-left"><span data-original-title="" title="">@{{ cart.summary_price }}</span> руб</p>
                                                    <p class="pull-left"><span class="style-danger status" data-original-title="" title="">@{{ status }}</span></p>
                                                </div>
                                            </header>
                                            <div class="pull-right opacity-50">
                                                <a class="btn btn-close" data-original-title="" title=""><i class="md md-assignment"></i></a>
                                                <a class="btn btn-close" data-original-title="" title=""><i class="fa fa-pencil"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    @{{/each }}
                                @{{ else }}
                                    Задач пока не было создано
                                @{{/with }}
                            @{{ else }}
                                Компания находится в процессе создания.
                            @{{/with }}
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </li>
</script>
<script id="template-attach-client-card" type="text/x-handlebars-template">
    <li>
        <div class="card card-bordered company-card border-none">
            <div class="card-head card-head-xs text-primary">
                <header data-toggle="tooltip" data-placement="top" title="">Контакты</header>
                <div class="tools">
                    <div class="btn-group">
                        <span class="btn btn-icon-toggle myPopover" data-container="body" data-toggle="popover" data-placement="right" data-content='<div class="content-popover"><a href="#"><i class="md md-playlist-add"></i>Новый контакт</a><a href="#"><i class="md md-attach-file"></i>Привязать контакт</a></div>'><i class="md md-playlist-add"></i></span>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group no-padding">
                    <select class="form-control select2-list input-sm add_object" id="attach-client" data-placeholder="Выберите контакт">
                        @{{#with clients }}
                        <optgroup label="Контакты">
                            @{{#each clients }}
                            <option value="@{{ id }}">@{{ name }}</option>
                            @{{/each }}
                        </optgroup>
                        @{{else }}
                        <option>&nbsp;</option>
                        @{{/with }}
                    </select>
                </div>
                <div class="form-group text-center">
                    <button class="cancel-attach-client btn btn-default ink-reaction btn-sm">Отменить</button>
                    <button id="" class="ok-attach-client btn ink-reaction btn-primary btn-sm">Добавить</button>
                </div>
            </div>
        </div>
    </li>
    @{{#with not clients }}

    <li>---- Нет контактов ---</li>

    @{{/with }}
</script>
<script id="template-task-item" type="text/x-handlers-template">
    <div class="client-card card alert alert-callout alert-danger">
        <div class="card-head card-head-sm">
            <header>
                <span><i class="md md-insert-invitation"></i></span>
                <span>Через 4 дня: <span class="opacity-75 text-sm">@{{task.created_at}}</span></span>
                <a href="#" class="text-primary">@{{ task.user }}</a>
            </header>
            <div class="tools">
                <div class="btn-group">
                    <a class="btn btn-icon-toggle done-task" data-id="@{{ task.id }}" data-toggle="tooltip" data-placement="auto" title="Выполнить"><i class="md md-done-all"></i></a>
                    <a class="btn btn-icon-toggle edit-task" data-id="@{{ task.id }}" data-toggle="tooltip" data-placement="auto" title="Редактирование"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <p>@{{ task.comment }}</p>
        </div>
    </div>
</script>
<script id="template-history-item" type="text/x-handlers-template">
        <div class="client-card card alert alert-callout alert-success">
            <div class="card-head card-head-sm">
                <header>
                    <span><i class="md md-insert-comment"></i></span>
                    <span><span>@{{history.date}} в @{{history.time}}</span></span>
                    <span>@{{history.name}}</span>
                </header>
                <div class="tools">
                    <div class="btn-group">
                        <a class="btn btn-icon-toggle"><i class="fa fa-pencil"></i></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p></p>
            </div>
        </div>
    </script>
<script id="template-order-item" type="text/x-handlers-template">
    <div class="card-head card-head-sm collapsed" data-toggle="collapse" data-target="#accordion-06">
        <header>
            <span><i class="md md-local-grocery-store"></i></span>
            <span class="text-warning">Сделка #@{{ order.id }} </span>
            <span class="opacity-75 text-sm">@{{ order.created_at }}</span>
            <a href="#" class="text-primary">@{{ order.user }}</a>
        </header>
        <div class="tools">
            <div class="btn-group">
                <a class="btn btn-icon-toggle card-order" data-id="@{{ order.id }}" data-toggle="tooltip" data-placement="auto" title="Карточка сделки"><i class="md md-assignment"></i></a>
                <a class="btn btn-icon-toggle edit-order" data-id="@{{ order.id }}" data-toggle="tooltip" data-placement="auto" title="Редактирование"><i class="fa fa-pencil"></i></a>
                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
            </div>
        </div>
    </div>
</script>
<script id="template-statistic-item" type="text/x-handlers-template">
    <div class="client-card card alert alert-callout alert-danger">
        <div class="card-head card-head-sm">
            <header>
                <span><i class="md md-insert-invitation"></i></span>
                <span>Через 4 дня: <span class="opacity-75 text-sm">@{{ statistcs.created_at }}</span></span>
                <a href="#" class="text-primary">Админ Админов</a>
            </header>
            <div class="tools">
                <div class="btn-group">
                    <a class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Выполнить"><i class="md md-done-all"></i></a>
                    <a class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Редактирование"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        </div>
    </div>
</script>
<script id="template-order-edit" type="text/x-handlers-template">
        <div id="card-edit-order">
        <div class="row">
            <div class="col-sm-4">
                <div class="no-side-padding">
                    <form class="form-horizontal floating-label">
                        <div class="margin-bottom-xxl">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="id-order">ID сделки </label>
                                <div class="col-xs-8 pull-right">
                                    <input type="text" id="card-edit-order-id" value="@{{ order.id }}" class="form-control input-sm" disabled>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="name">Название </label>
                                <div class="col-xs-8 pull-right">
                                    <input type="text" id="card-edit-order-name" value="@{{ order.description }}" class="form-control input-sm">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Ответственный </label>
                                <div class="col-xs-7 pull-right">
                                    <select id="card-edit-order-responsible-user" class="form-control select2-list input-sm height-30" data-placeholder="Select an item">
                                        <optgroup label="Пользователи">
                                            @if(isset($users) && count($users) > 0))
                                            @foreach($users as $user)
                                            <option @{{#with order.responsible_user_id === <?php echo $user->id?> }} selected @{{\with}} value="{{$user->id}}">{{$user->name}} {{$user->last_name}}</option>
                                            @endforeach
                                            @else
                                                <option value="">Нет доступных пользователей</option>
                                            @endif
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Заказчик </label>
                                <div class="col-xs-7 pull-right">
                                    <select class="form-control select2-list input-sm height-30" data-placeholder="Select an item">
                                        <optgroup data-group="0" label="Компании">
                                            @if(isset($firms) && count($firms) > 0)
                                                @foreach($firms as $firm)
                                            <option @{{#with order.firm_id === <?php echo $firm->id?>}} value="{{$firm->id}}">{{$firm->name}}</option>
                                                @endforeach
                                            @else
                                                <option value="">Нет доступных компаний</option>
                                            @endif
                                        </optgroup>
                                        <optgroup label="Контакты">
                                            @if(isset($clients) && count($clients) > 0)
                                            @foreach($clients as $client)
                                                <option @{{#with order.client_id === <?php echo $client->id?>}} value="{{$client->id}}">{{$client->name}}</option>
                                            @endforeach
                                            @else
                                                <option value="">Нет доступных контактов</option>
                                            @endif

                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Статус </label>
                                <div class="col-xs-8 pull-right">
                                    <dl class="dropdown dropdown-styled">
                                        <dt>
                                            <a><span>Задачи</span><div class="arrow"><i class="md md-expand-more"></i></div></a>
                                        </dt>
                                        <dd>
                                            <ul class="list-unstyled">
                                                @foreach(\Crm\References\OrderReference::statusesLabels() as $id => $label)
                                                    <li><a data-id="{{$id}}"><span><span class="permit" style="background-color: green"></span>{{$label}}</span></a></li>
                                                @endforeach
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="reg2">Комментарий </label>
                                <div class="col-xs-8 pull-right">
                                    <textarea id="reg2" class="form-control input-sm">@{{order.comment}}</textarea>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-7 pull-right">
                <div class="no-side-padding">
                    <div class="card-head card-head-xs">
                        <span class="text-primary text-lg">Заказ</span>
                        <div class="pull-right">
                            <div class="btn-group text-primary">
                                <span class="btn btn-icon-toggle" data-toggle="modal" data-target="#modal1" data-placement="auto" title="Редактировать"><i class="fa fa-pencil"></i></span>
                                <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Добавить товар"><i class="md md-add"></i></span>
                                <span class="btn btn-icon-toggle"><i class="md md-add-box" data-toggle="tooltip" data-placement="auto" title="Добавить из шаблона"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Название</th>
                                <th class="hide">С/с</th>
                                <th>Стоимость</th>
                                <th>Кол&nbsp;-&nbsp;во</th>
                                <th>Итого</th>
                            </tr>
                            </thead>
                            <tbody>
                            @{{#each order.cart}}
                            <tr>
                                <td>@{{ name }}</td>
                                <td class="hide">@{{prime_cost}}</td>
                                <td><div contenteditable>@{{price}}</div></td>
                                <td><div contenteditable>@{{ qty }}</div></td>
                                <td>@{{ @summary }}</td>
                            </tr>
                            @{{/each}}
                            </tbody>
                        </table>
                    </div>
                    <table class="table">
                        <tr>
                            <td>Товаров:</td>
                            <td>@{{order.cart.qty_products}}</td>
                        </tr>
                        <tr>
                            <td>Стоимость:</td>
                            <td>@{{order.cart.summary_price}}</td>
                        </tr>
                        <tr>
                            <td>Себестоимость:</td>
                            <td>@{{ order.cart.summary_prime_cost }}</td>
                        </tr>
                        <tr>
                            <td>Маржинальность:</td>
                            <td>@{{ order.cart.summary_price - order.cart.summary_prime_cost}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="col-sm-12">
                        <ul class="list-inline">
                            <li> <button id="remove-product-cart" type="submit" class="btn btn-default-light ink-reaction btn-sm">Удалить</button></li>
                            <li> <button id="cancel-cart" type="submit" class="btn btn-default-light ink-reaction btn-sm">Отменить</button></li>
                            <li> <button id="save-cart" type="submit" class="btn ink-reaction btn-primary btn-sm">Сохранить</button></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
