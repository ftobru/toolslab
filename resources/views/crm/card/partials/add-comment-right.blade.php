<div class="tab-pane active" id="first8">
    <form class="form" role="form">
        <div class="form-group floating-label">
            <textarea name="textarea2" id="comment" class="form-control input-sm" rows="3" placeholder="" ></textarea>
            <label for="textarea2">Введите текст комментария</label>
        </div>
        <div class="form-group">
            <!--  <button type="submit" class="btn btn-default-light ink-reaction btn-sm">Отмена</button> -->
            <button type="submit" id="add-comment" class="btn ink-reaction btn-primary btn-sm">Добавить</button>
        </div>
    </form>
</div><!--end tab-pane-->