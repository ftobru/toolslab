<div class="modal fade" id="modal1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center text-primary">
                <span class="close close1 btn btn-icon-toggle text-primary" data-dismiss="modal" aria-hidden="true" data-toggle="tooltip" data-placement="auto" title="Закрыть"><i class="md md-close"></i></span>
                <div class="pull-right">
                    <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Редактировать"><i class="fa fa-pencil"></i></span>
                    <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Добавить товар"><i class="md md-add"></i></span>
                    <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Добавить товар из шаблона"><i class="md md-add-box"></i></span>
                </div>
                <h3 class="modal-title text-primary">Название</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th class="hide">С/с</th>
                        <th>Стоимость</th>
                        <th>Кол&nbsp;-&nbsp;во</th>
                        <th>Итого</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Мыло с длинным названием каким-то</td>
                        <td class="hide">10000</td>
                        <td><div contenteditable>10000</div></td>
                        <td><div contenteditable>100</div></td>
                        <td>1000000</td>
                        <td><a href="#" class="btn btn-icon-toggle"><i class="md md-delete"></i></a></td>
                    </tr>
                    </tbody>
                </table>
                <table class="table">
                    <tr>
                        <td>Товаров:</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Стоимость:</td>
                        <td>50000</td>
                    </tr>
                    <tr>
                        <td>Себестоимость:</td>
                        <td>5000</td>
                    </tr>
                    <tr>
                        <td>Маржинальность:</td>
                        <td>50000</td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-default-light ink-reaction">Отменить</button>
                    <button type="submit" class="btn ink-reaction btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center text-primary">
                <span class="close close1 btn btn-icon-toggle text-primary" data-dismiss="modal" aria-hidden="true" data-toggle="tooltip" data-placement="auto" title="Закрыть"><i class="md md-close"></i></span>
                <div class="pull-right">
                    <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Редактировать"><i class="fa fa-pencil"></i></span>
                </div>
                <h3 class="modal-title text-primary">Название</h3>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-striped table-edit">
                    <caption>Название таблицы</caption>
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th class="hide">С/с</th>
                        <th>Стоимость</th>
                        <th>Кол&nbsp;-&nbsp;во</th>
                        <th>Итого</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Мыло с длинным названием каким-то</td>
                        <td class="hide">10000</td>
                        <td><div contenteditable>10000</td>
                        <td><div contenteditable>100</td>
                        <td>1000000</td>
                    </tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-striped">
                    <caption>Название таблицы</caption>
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th class="hide">С/с</th>
                        <th>Стоимость</th>
                        <th>Кол&nbsp;-&nbsp;во</th>
                        <th>Итого</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Мыло с длинным названием каким-то</td>
                        <td class="hide">10000</td>
                        <td><div contenteditable>10000</div></td>
                        <td><div contenteditable>100</div></td>
                        <td>1000000</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-default-light ink-reaction">Отменить</button>
                    <button type="submit" class="btn ink-reaction btn-primary">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>