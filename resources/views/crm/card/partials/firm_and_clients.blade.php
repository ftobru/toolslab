<div id="off-canvas1" class="filter-menu filter-clients style-default-light">
    <div class="sidebar-clients list-unstyled">
        <ul class="sidebar-clients no-padding" data-toggle="tabs">
            <li id="firm-block"></li>
            <!-- Block card exist firm or create firm -->

            <li id="blank-firm-card"></li>
            <!-- Block attach firm -->

            <li id="card-client"></li>
            <!-- Block clients -->
        </ul>
    </div>
</div><!--/off-canvas-->