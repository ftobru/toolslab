<div class="tab-pane" id="third8">
    <form class="form-horizontal floating-label">
        <div class="margin-bottom-xxl">
            <div class="form-group">
                <label class="col-sm-4 control-label" for="reg1">ID сделки </label>
                <div class="col-xs-8 pull-right">
                    <input type="text" id="reg1" class="form-control input-sm" disabled>
                    <div class="form-control-line"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="name">Название </label>
                <div class="col-xs-8 pull-right">
                    <input type="text" id="name" class="form-control input-sm">
                    <div class="form-control-line"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Ответственный </label>
                <div class="col-xs-7 pull-right">
                    <select class="form-control select2-list input-sm height-30" data-placeholder="Select an item">
                        <optgroup label="Пользователь">

                            @if(isset($users) && !empty($users))
                                @foreach($users as $user)
                                 <option value="{!! $user['id']!!}">{{ $user['name'] }} {{$user['last_name']}}</option>
                                @endforeach
                            @else
                            <option value=""></option>
                            @endif
                        </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label">Заказчик </label>
                <div class="col-xs-7 pull-right">
                    <select class="form-control select2-list input-sm height-30" data-placeholder="Выберите контакты">
                        @if(isset($clients) && !empty($clients) && count($clients) > 1)
                        <optgroup label="Контакты">
                            @foreach($clients as $client)
                                <option value="{!! $client->id !!}">{{$client->name}}</option>
                            @endforeach
                        </optgroup>
                            @elseif(isset($client))
                            <optgroup label="Контакты">
                                <option value="{!! $client->id !!}">{{$client->name}}</option>
                            </optgroup>
                        @endif
                        @if(isset($firm) && !empty($firm))
                        <optgroup label="Компания">
                            <option value="{!! $firm->id !!}">{{$firm->name}}</option>
                        </optgroup>
                        @endif
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Статус </label>
                <div class="col-xs-8 pull-right">
                    <dl class="dropdown">
                        <dt>
                            <a><span>Задачи</span><div class="arrow"><i class="md md-expand-more"></i></div></a>
                        </dt>
                        <dd>
                            <ul class="list-unstyled" style="display: none;">
                                @foreach(App::make('Crm\References\OrderReference')->getStatuses() as $status => $label)
                                <li><a><span><span class="permit" style="background-color: green"></span>{{$label}}</span></a></li>
                                @endforeach

                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="comment-order">Комментарий </label>
                <div class="col-xs-8 pull-right">
                    <textarea id="comment-order" class="form-control input-sm"></textarea>
                    <div class="form-control-line"></div>
                </div>
            </div>
        </div>
        <div class="no-side-padding">
            <div class="card-head card-head-xs">
                <span class="text-primary text-lg">Заказ</span>
                <div class="pull-right">
                    <div class="btn-group text-primary">
                        <span class="btn btn-icon-toggle" data-toggle="modal" data-target="#modal2" data-placement="auto"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="auto" title="Редактировать"></i></span>
                        <span class="btn btn-icon-toggle" data-toggle="tooltip" data-placement="auto" title="Добавить товар"><i class="md md-add"></i></span>
                        <span class="btn btn-icon-toggle" data-toggle="modal" ><i class="md md-equalizer" data-toggle="tooltip" data-placement="auto" title="Статистика"></i></span>
                    </div>
                </div>
            </div>
            <div hidden="hidden">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th class="hide">С/с</th>
                            <th>Стоимость</th>
                            <th>Кол&nbsp;-&nbsp;во</th>
                            <th>Итого</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Мыло</td>
                            <td class="hide">10000</td>
                            <td><div contenteditable>10000</div></td>
                            <td><div contenteditable>100</div></td>
                            <td>1000000</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <table class="table">
                    <tr>
                        <td>Товаров:</td>
                        <td>5</td>
                    </tr>
                    <tr>
                        <td>Стоимость:</td>
                        <td>50000</td>
                    </tr>
                    <tr>
                        <td>Себестоимость:</td>
                        <td>5000</td>
                    </tr>
                    <tr>
                        <td>Маржинальность:</td>
                        <td>50000</td>
                    </tr>
                </table>
            </div>

            <div class="form-group">
                <div class="col-sm-12 text-center">
                    <button type="submit" id="addd-product" class="btn ink-reaction btn-default-light btn-sm">Добавить товар</button>
                    <button type="submit" id="add-product-template" class="btn ink-reaction btn-default-light btn-sm">Взять из шаблона</button>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" id="add-order" class="btn ink-reaction btn-primary"><i class="md md-done"></i> Создать сделку</button>
            </div>
        </div>
    </form>
</div><!--end tab-pane-->