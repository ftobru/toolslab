@extends('layouts.simple_private')

@section('content')
    <section>
        <div class="section-body">
            <div class="row">
                <div class="col-md-5 col-lg-6 empty-top">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-head ">
                                    <header class="text-primary">Баланс счета</header>
                                    <div class="tools">
                                        <a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
                                        <!-- <a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a> -->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h2 class="text-primary">{{(int)$currentBalance->amount}} р</h2>
                                    <p>Средств хватит до <span>21.06</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-head ">
                                    <header class="text-primary">Расходы в месяц</header>
                                    <div class="tools">
                                        <a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
                                        <!-- <a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a> -->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h2 class="text-primary">{{(int)$invoiceCurrentMonth}} р</h2>
                                    <p>Подключено <span>{{$rates->count()}}</span> моду@if($rates->count() <= 4)ля
                                        @else
                                            лей
                                        @endif
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <form class="form" role="form">
                            <div class="card-head ">
                                <header class="text-primary">Пополнение баланса</header>
                                <div class="tools">
                                    <a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
                                    <!-- <a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a> -->
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group floating-label">
                                    <input type="text" id="input-amount" class="form-control" >
                                    <label for="regular2">Сумма <span>минимум 300 р</span></label>
                                </div>

                                <div class="form-group">
                                    <form method="POST" action="https://www.moneta.ru/assistant.htm">
                                        <input type="hidden" name="MNT_ID" value="{{env('PAYMENT_MNT_ID', "00000001")}}">
                                        <input type="hidden" name="MNT_TRANSACTION_ID" value="{{$transaction->transaction_id}}">
                                        <input type="hidden" name="MNT_CURRENCY_CODE" value="RUB">
                                        <input type="hidden" id="amount" name="MNT_AMOUNT" value="">
                                        <input type="hidden" name="MNT_TEST_MODE" value="{{env('PAYMENT_TEST', 1)}}">
                                        <input type="hidden" name="MNT_SUCCESS_URL"
                                               value="{{env('PAYMENT_SUCCESS_URL', 'http://localhost:8000/billing/profile?success')}}">
                                        <input type="hidden" name="MNT_FAIL_URL"
                                               value="{{env('PAYMENT_FAIL_URL', 'http://localhost:8000/billing/profile?fail')}}">
                                        <input type="hidden" name="MNT_RETURN_URL"
                                               value="{{env('PAYMENT_SUCCESS_URL', 'http://localhost:8000/billing/profile?normal')}}">
                                        <input type="hidden" name="MNT_CUSTOM1" value="user_id_{{\Auth::user()->id}}">
                                        <input type="submit" id="pay" class="btn ink-reaction btn-primary" value="Оплатить заказ">
                                    </form>

                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card">
                        <div class="card-head">
                            <header class="text-primary">Информация по оплате</header>
                            <div class="tools">
                                <a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
                                <!-- <a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a> -->
                            </div>
                        </div>
                        <div class="card-body">
                            <p>Описание, что все платежи защищены и прочая информация</p>
                            <p>Описание, что все платежи защищены и прочая информация</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                Laudantium veniam, ducimus perspiciatis ea vitae, cum dolorem enim
                                facilis
                                cumque rem deleniti. Suscipit hic, libero in. Voluptate provident error quasi quas!</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-lg-6">
                    <ul class="nav nav-tabs tabs-center" data-toggle="tabs">
                        <li class="active"><a href="#first33">Следующие платежи</a></li>
                        <li><a href="#second33">История платежей</a></li>
                    </ul>
                    <div class="card-body tab-content">
                        <div class="tab-pane active" id="first33">

                            @if($rates->count())
                                @foreach($rates as $rate)
                                    <?php $rateEntity = $rate->rate() ?>
                                <div id="{{$rateEntity->getIdentifier()}}" class="card">
                                        <div class="card-head">
                                            <header class="text-primary">
                                                <a href="#">{{$rateEntity->getDisplayName()}}</a></header>
                                            <div class="tools opacity-75">{{$rateEntity->getPrice()}}р/мес</div>
                                        </div>
                                        <div class="card-body">
                                            <p>{{$rateEntity->getDescription()}}</p>
                                            <p class="pull-left">Действует до:
                                                <span>{{date('d.m.Y', strtotime($rate->date_end))}}</span></p>
                                            <div class="form-group floating-label autopay">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <div class="checkbox checkbox-inline checkbox-styled">
                                                            <label>
                                                                <input type="checkbox"><span></span>
                                                                Продливать автоматически?
                                                                <a href="#" data-toggle="tooltip"
                                                                   data-placement="auto"
                                                                   title="Сумма за модуль будет автоматически
                                                                    списываться за 3 дня до
                                                                    истечения оплаченного периода">
                                                                    <i class="md md-help"></i></a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($rateEntity->getPrice() > 0)
                                            <div class="amount">
                                                <p class="pull-left">
                                                    <span data-price="{{$rateEntity->getPrice()}}">
                                                        {{$rateEntity->getPrice()}}</span> р/мес * </p>
                                                <div class="form-group floating-label pull-left">
                                                    <select id="select2" name="select2" class="form-control">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3" selected="selected">3</option>
                                                    </select>
                                                    <label for="select2"></label>
                                                </div>

                                                <p class="pull-left"> мес =
                                                    <span class="data-total">{{$rateEntity->getPrice() * 3}}</span>р</p>
                                                <button type="submit" class="btn ink-reaction btn-primary pull-left">
                                                    Оплатить</button>

                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                        <div class="card">
                                            <div class="card-body">
                                                <h2>Нет не одного тарифа не подключено</h2>
                                            </div>
                                        </div>
                                    @endif
                            </div>

                        <!--end tab-pane-->
                        <div class="tab-pane" id="second33">

                            <div class="card">
                                <div class="card-body no-padding">
                                    <div class="table-responsive no-margin">
                                        <table class="table table-striped no-margin">
                                            <thead>
                                            <tr>
                                                <th>Дата</th>
                                                <th>Время</th>
                                                <th>Модуль</th>
                                                <th>Сумма</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($invoicesPayment->count())
                                            @foreach($invoicesPayment as $invoice)
                                                <tr>
                                                    <td>{{$invoice->created_at->format('d-m-Y')}}</td>
                                                    <td>{{$invoice->created_at->format('H:i')}}</td>
                                                    <td>{{$invoice->rate()->getDisplayName()}}</td>
                                                    <td>{{$invoice->amount}} р</td>
                                                </tr>
                                            @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4">
                                                        Нет не одного оплаченого счета
                                                    </td>
                                                </tr>

                                            @endif
                                            </tbody>
                                        </table>
                                    </div><!--end .table-responsive -->
                                </div><!--end .card-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('scripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#pay').click(function(){
                var payment = $('#input-amount').val();
                $('#amount').val(payment);
            });
        });
    </script>
@stop