<div class="tab-pane" id="third5">
    <div class="row module-open">
        <div class="col-sm-12 card">
            <div class="row">

                <div class="col-lg-6">
                    <div>
                        <div><a class="fancybox" href="../../images/img1.jpg"><img id="largeImg" src="../../images/img1.jpg" alt="Image 6"></a></div>
                        <div class="thumbs">
                            <a href="../../images/img1.jpg" title="Изброжение1"><img src="../../images/img1.jpg" alt="image"></a>
                            <a href="../../images/img2.jpg" title="Image 3"><img src="../../images/img2.jpg" alt="image"></a>
                            <a href="../../images/img1.jpg" title="Image 2"><img src="../../images/img1.jpg" alt="image"></a>
                            <a href="../../images/img2.jpg" title="Image 3"><img src="../../images/img2.jpg" alt="image"></a>
                            <a href="../../images/img1.jpg" title="Image 2"><img src="../../images/img1.jpg" alt="image"></a>
                        </div>
                    </div>
                    <div class="about-module">
                        <div class="card-head ">
                            <ul class="nav nav-tabs nav-styled text-sm no-margin" data-toggle="tabs">
                                <li class="active"><a href="#first215">Описание</a></li>
                                <li class=""><a href="#second215">Версии</a></li>
                                <li><a href="#fourth215">Установка</a></li>
                            </ul>
                        </div>
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="first215">
                                Содержимоее 1 таба
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque delectus alias velit perspiciatis! Quod enim, similique libero neque quam, temporibus nobis. Dolores soluta natus explicabo voluptate odio corporis nisi deleniti!</p>
                            </div>
                            <div class="tab-pane" id="second215">
                                Содержимоее 2 таба
                            </div>
                            <div class="tab-pane" id="fourth215">
                                Содержимоее 4 таба
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card-body module_card">
                        <h2 class="text-primary text-bold">Диск</h2>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card-bordered border-primary margin-bottom-xxl">
                                    <div class="crm-text card-body padding-right-lg">
                                        <div class="text-lg">Без облачного хранилища, в наши дни, никуда. Вы можете загружать на диск свои лендинги. В будущем функционал расширится</div>
                                    </div>
                                    <img class="loader" src="../../images/cd.png" alt="img">
                                </div>
                                <div class="card-bordered border-primary">
                                    <div class="card-head">
                                        <header class="text-primary">Выберите необходимый объем:</header>
                                    </div>
                                    <div class="crm-text small-padding height-6">
                                        <form role="form" class="form">
                                            <div class="col-xs-6">
                                                <div class="row">
                                                    <div class="form-group floating-label pull-left left-no-padding">
                                                        <select class="form-control opacity-75" id="ratesDisk" name="robot">
                                                            <option value="203" selected>1 Gb</option>
                                                            <option value="204">5 Gb</option>
                                                            <option value="205">10 Gb</option>
                                                        </select>
                                                        <label for="sel"></label>
                                                    </div>
                                                    <p class="pull-left"><span class="text-xl">&minus; </span><span class="text-xl">100</span> <span class="fraction">руб/мес</span></p>
                                                </div>
                                                <div class="row">
                                                    <button id="connectDisk" class="btn btn-danger ink-reaction">Подключить 56 В</button>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-right">
                                                <!-- <div id="div1">текст1</div>
                                                <div id="div2">текст2</div>
                                                <div id="div3">текст3</div> -->
                                                <img src="../../images/robot_01.png" alt="robot" id="robot1" width="130">
                                                <img src="../../images/robot_02.png" alt="robot" id="robot2" width="130">
                                                <img src="../../images/robot_03.png" alt="robot" id="robot3" width="130">
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>