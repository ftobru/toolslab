<div class="tab-pane active" id="first5">
    <div class="row module-open">
        <div class="col-sm-12 card">
            <div class="row">
                <div class="col-lg-6">
                    <div>
                        <div><a class="fancybox" href="../../images/img1.jpg"><img id="largeImg" src="../../images/img1.jpg" alt="Image 6"></a></div>
                        <div class="thumbs">
                            <a href="../../images/img1.jpg" title="Изброжение1"><img src="../../images/img1.jpg" alt="image"></a>
                            <a href="../../images/img2.jpg" title="Image 3"><img src="../../images/img2.jpg" alt="image"></a>
                            <a href="../../images/img1.jpg" title="Image 2"><img src="../../images/img1.jpg" alt="image"></a>
                            <a href="../../images/img2.jpg" title="Image 3"><img src="../../images/img2.jpg" alt="image"></a>
                            <a href="../../images/img1.jpg" title="Image 2"><img src="../../images/img1.jpg" alt="image"></a>
                        </div>
                    </div>
                    <div class="about-module">
                        <div class="card-head ">
                            <ul class="nav nav-tabs nav-styled text-sm no-margin" data-toggle="tabs">
                                <li class="active"><a href="#first21">Описание</a></li>
                                <li class=""><a href="#second21">Версии</a></li>
                                <li><a href="#fourth21">Установка</a></li>
                            </ul>
                        </div>
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="first21">
                                Содержимоее 1 таба
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque delectus alias velit perspiciatis! Quod enim, similique libero neque quam, temporibus nobis. Dolores soluta natus explicabo voluptate odio corporis nisi deleniti!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates pisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti numquam nihil aut veritatis eaque, quisquam ratione ea tempora, autem inventore doloremque nam mollitia officiis, officia eveniet voluptates alias voluptas tempore!</p>
                            </div>
                            <div class="tab-pane" id="second21">
                                Содержимоее 2 таба
                            </div>
                            <div class="tab-pane" id="fourth21">
                                Содержимоее 4 таба
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card-body module_card">
                        <h2 class="text-primary text-bold">CRM</h2>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="card-bordered border-primary margin-bottom-xxl">
                                    <div class="crm-text card-body padding-right-lg">
                                        <div class="text-lg">Это сердце вашего бизнеса.</div>
                                        <div class="text-lg">Оно не должно останавливаться</div>
                                        <div class="text-lg">Поэтому CRM всегда будет <span class="text-bold">бесплатной</span></div>
                                    </div>
                                    <img src="../../images/heart.png" alt="heart" class="heart">
                                </div>
                                <div>
                                    <ul class="list-inline price-btn text-center">
                                        <li><a href="#" id="mounth-btn" class="style-default-light padding-xs active">Ежемесячно</a></li>
                                        <li><a href="#" id="year-btn" class="style-default-light padding-xs">На год(25%)</a></li>
                                    </ul>
                                    <div class="card-bordered border-primary margin-bottom-xxl margin-top-md">
                                        <div class="crm-text card-body text-center">
                                            <p class="row">Добавляйте сотрудников для более эффективной работы</p>
                                            <form role="form" class="form margin-15">
                                                <div class="row">
                                                    <div class="margin-bottom-xl">
                                                    <div class="pull-left1 left-no-padding no-select">
                                                        <label for="sel" class="opacity-50">Сотрудники</label>
                                                        <div>
                                                            <span id="minus" class="no-select add-user">-</span>
                                                            <input type="text" disabled="true" id="sel" class="numbertype" value="@if($countUsers > 5) {{$countUsers - 5}} @else 0 @endif  "/>
                                                            <span id="plus" class="no-select add-user">+</span>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left1">
                                                        <div class="opacity-75 margin-top-sm"> х <span id="mounth-price" class="text-xl"> {{$summa = ($price = ($rateEntities->get(\Billing\Services\Rates\CrmAddUserRate::IDENTIFIER)->getPrice()))}}</span> <span id="year-price" class="text-xl">
                                                                {{$summa - (($summa / 100) * 25)}}</span><span class="fraction">руб/мес</span> = <span id="price" class="text-bold text-xl">
                                                                @if($countUsers > 5) {{($summa - (($summa / 100) * 25)) * $countUsers}} @else 0 @endif</span> <span class="fraction">руб/мес</span>
                                                        </div>
                                                    </div>
                                                    <div>
                                                    </div>
                                                    </div>
                                                    <div class="row">
                                                        <button id="connectUsers" disabled="true" class="btn btn-primary">Подключить</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div id="list"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div><!--end tab-pane-->