<div class="tab-pane" id="second5">
    <div class="row module-open">
        <div class="col-sm-12 card">
            <div class="row">
                <div class="col-lg-6">
                    <div>
                        <div><a class="fancybox" href="../../images/img1.jpg"><img id="largeImg" src="../../images/img1.jpg" alt="Image 6"></a></div>
                        <div class="thumbs">
                            <a href="../../images/img1.jpg" title="Изброжение1"><img src="../../images/img1.jpg" alt="image"></a>
                            <a href="../../images/img2.jpg" title="Image 3"><img src="../../images/img2.jpg" alt="image"></a>
                            <a href="../../images/img1.jpg" title="Image 2"><img src="../../images/img1.jpg" alt="image"></a>
                            <a href="../../images/img2.jpg" title="Image 3"><img src="../../images/img2.jpg" alt="image"></a>
                            <a href="../../images/img1.jpg" title="Image 2"><img src="../../images/img1.jpg" alt="image"></a>
                        </div>
                    </div>
                    <div class="about-module">
                        <div class="card-head ">
                            <ul class="nav nav-tabs nav-styled text-sm no-margin" data-toggle="tabs">
                                <li class="active"><a href="#first212">Описание</a></li>
                                <li class=""><a href="#second212">Версии</a></li>
                                <li><a href="#fourth212">Установка</a></li>
                            </ul>
                        </div>
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="first212">
                                Содержимоее 1 таба
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque delectus alias velit perspiciatis! Quod enim, similique libero neque quam, temporibus nobis. Dolores soluta natus explicabo voluptate odio corporis nisi deleniti!</p>
                            </div>
                            <div class="tab-pane" id="second212">
                                Содержимоее 2 таба
                            </div>
                            <div class="tab-pane" id="fourth212	">
                                Содержимоее 4 таба
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card-body module_card">
                        <h2 class="text-primary">Landing</h2>
                        <ul class="list-inline price-btn text-center">
                            <li><a href="#" id="mounth-btn1" class="style-default-light padding-xs active">Ежемесячно</a></li>
                            <li><a href="#" id="year-btn1" class="style-default-light padding-xs">Годовой тариф (25%)</a></li>
                        </ul>
                        <div class="wrp_tariffs">
                            <div class="row">
                                <div class="col-md-6 no-padding">
                                    <div class="card card-type-pricing text-center">
                                        <div class="card-body style-gray">
                                            <h2 class="text-light">Normal</h2>
                                            <span class="text-xxl" id="mounth-price1">{{$monthPrice = $rateEntities->get(\Billing\Services\Rates\LandingControlHostNormalRate::IDENTIFIER)->getPrice()}} </span>
                                            <span class="text-xxl" id="year-price1">{{$monthPrice - (($monthPrice/ 100) * 25)}}</span>
                                            <span class="fraction">руб/мес</span>
                                        </div><!--end .card-body -->
                                        <div class="card-body no-padding">
                                            <div class="margin-top-lg">
                                                    @if($currentConnectRates->get(\Billing\Services\Rates\LandingControlHostNormalRate::IDENTIFIER))
                                                    <button disabled="true" class="btn btn-primary">
                                                        Подключить</button>
                                                    @else
                                                    <button id="connectLandingNormal"
                                                    @if($amount < $monthPrice)
                                                        disabled="true"
                                                    @endif
                                                            class="btn btn-primary">Подключить</button>
                                                    @endif

                                            </div>
                                            <ul class="list-unstyled">
                                                <li><span class="text-lg"><i class="md md-visibility"></i> 3000 посещений</span></li>
                                                <li><span class="text-lg"><i class="md md-tab-unselected"></i> 5 доменов</span></li>
                                                <li><span class="text-lg"><i class="md md-description"></i>  5 шаблонов</span></li>
                                                <li><span class="text-lg"><i class="md md-content-copy"></i>  50 страниц</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 no-padding">
                                    <div class="card card-type-pricing text-center">
                                        <div class="card-body style-primary style-pro">
                                            <h2 class="text-light">Pro</h2>

                                            <span class="text-xxl" id="mounth-price1">{{$monthPrice = $rateEntities->get(\Billing\Services\Rates\LandingControlHostProRate::IDENTIFIER)->getPrice()}}</span>
                                            <span class="text-xxl" id="year-price1">{{$monthPrice - (($monthPrice / 100) * 25)}}</span>
                                            <span class="fraction">руб/мес</span>
                                        </div>
                                        <div class="card-body no-padding">
                                            <div class="margin-top-lg">
                                                @if($currentConnectRates->get(\Billing\Services\Rates\LandingControlHostProRate::IDENTIFIER))
                                                    <button disabled="true" class="btn btn-primary">
                                                        Подключить</button>
                                                @else
                                                    <button id="connectLandingNormal"
                                                    @if($amount < $monthPrice)
                                                            disabled="true"
                                                            @endif
                                                            class="btn btn-primary">Подключить</button>
                                                @endif
                                            </div>
                                            <ul class="list-unstyled">

                                                <li><span class="text-lg"><i class="md md-visibility"></i> посещений <span class="text-lg text-primary ifn">&#8734;</span></span></li>
                                                <li><span class="text-lg"><i class="md md-tab-unselected"></i> доменов <span class="text-lg text-primary ifn">&#8734;</span></span></li>
                                                <li><span class="text-lg"><i class="md md-description"></i> шаблонов <span class="text-lg text-primary ifn">&#8734;</span></span></li>
                                                <li><span class="text-lg"><i class="md md-content-copy"></i> страниц <span class="text-lg text-primary ifn">&#8734;</span></span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wrp-case">
                            <div class="row" id="closed_case">
                                <div class="col-xs-12 no-padding"><img src="../../images/1.png" alt="" class="top-box"></div>
                                <div class="col-xs-12 no-padding">
                                    <div class="box">
                                        Тут мы перечислвяем различные функции, которые являютсяобщими для двух тарифов
                                        <ol class="no-margin">
                                            <li>функция первая</li>
                                            <li>функция торая оисание Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis itaque voluptatibus accusantium molestias magnam id rerum eligendi hic optio. Ratione veritatis nostrum, at fugit harum impedit, quaerat. Praesentium, asperiores, minima.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, velit vitae eos dicta quasi libero dolornte cupiditate. Totam officiis dolores earum ab, ad.</li>
                                        </ol>
                                        <span id="all_func">Посмотреть все функции</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 no-padding"><img src="../../images/2.png" alt="" class="top-box"></div>
                            </div>
                            <div class="row" id="opened_case">

                                <div class="col-xs-12 no-padding"><img src="../../images/open_topbox.png" alt="" class="top-box"></div>
                                <div class="col-xs-12 no-padding">
                                    <div class="box1">
                                        Тут мы перечислвяем различные функции, которые являютсяобщими для двух тарифов
                                        <ol class="no-margin">
                                            <li>функция первая</li>
                                            <li>функция торая оисание Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis itaque voluptatibus accusantium molestias magnam id rerum eligendi hic optio. Ratione veritatis nostrum, at fugit harum impedit, quaerat. Praesentium, asperiores, minima.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, velit vitae eos dicta quasi libero dolornte cupiditate. Totam officiis dolores earum ab, ad.</li>
                                        </ol>
                                    </div>
                                </div>
                                <div class="col-xs-12 no-padding">
                                    <img src="../../images/border.png" alt="box" class="top-box">
                                </div>
                                <div class="col-xs-12 no-padding">
                                    <div class="box1">
                                        Тут мы перечислвяем различные функции, которые являютсяобщими для двух тарифов
                                        <ol class="no-margin">
                                            <li>функция первая</li>
                                            <li>функция торая оисание Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis itaque voluptatibus accusantium molestias magnam id rerum eligendi hic optio. Ratione veritatis nostrum, at fugit harum impedit, quaerat. Praesentium, asperiores, minima.</li>
                                            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, velit vitae eos dicta quasi libero dolornte cupiditate. Totam officiis dolores earum ab, ad.</li>
                                        </ol>
                                        <span id="less_func"> Свернуть функции</span>
                                    </div>
                                </div>
                                <div class="col-xs-12 no-padding"><img src="../../images/open_b3.png" alt="" class="top-box"></div>
                            </div>
                        </div>

                        <div class="text-center">
                            <div class="text-lg text-primary margin-bottom-xl "><span>До повышения цен осталось 2 недели!</span></div>
                            <button class="btn ink-reaction btn-danger">Подключить <span class="hidden-xs">(тестовый период <span class="hidden-sm">15 дней</span>)</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>