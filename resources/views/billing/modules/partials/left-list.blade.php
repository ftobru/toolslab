<div id="off-canvas1" class="filter-menu filter-clients style-default-light">
    <div class="sidebar-clients list-unstyled more-card" data-toggle="tabs">
        <ul class="sidebar-clients modules no-padding">
            <li class="active">
                <a href="#first5">
                    <div class="card card-bordered">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-3 text-xxl text-primary">
                                    <i class="md md-track-changes"></i>
                                </div>
                                <div class="col-xs-9">
                                    <h3 class="no-margin">CRM-модуль</h3>
                                    <p class="opacity-75">Улучшает продажи</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#second5">
                    <div class="card card-bordered">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-3 text-xxl text-primary">
                                    <i class="md md-view-quilt"></i>
                                </div>
                                <div class="col-xs-9">
                                    <h3 class="no-margin">Landing-модуль</h3>
                                    <p class="opacity-75"><span class="text-primary"><i class="md md-done"></i> Подключено:</span> <span>


                                        @if($currentRate = $currentConnectRates->get(\Billing\Services\Rates\LandingControlHostLiteRate::IDENTIFIER, false))
                                            {{$currentRate->getName()}}
                                        @elseif($currentRate = $currentConnectRates->get(\Billing\Services\Rates\LandingControlHostNormalRate::IDENTIFIER, false))
                                            {{$currentRate->getName()}}
                                        @elseif($currentRate = $currentConnectRates->get(\Billing\Services\Rates\LandingControlHostProRate::IDENTIFIER, false))
                                            {{$currentRate->getName()}}
                                        @else
                                            Нет
                                        @endif

                                        </span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
            <li>
                <a href="#third5">
                    <div class="card card-bordered">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xs-3 text-xxl text-primary">
                                    <i class="md md-group-work"></i>
                                </div>
                                <div class="col-xs-9">
                                    <h3 class="no-margin">Диск-модуль</h3>
                                    <p class="opacity-75"><span class="text-primary"><i class="md md-done"></i> Подключено:</span> <span>4GB</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
    </div>
</div><!--/off-canvas-->
