
@extends('layouts.menubar_general')
@section('content')
    <label id="autocomplete1" class="form-control autocomplete" style="display: none;"></label><!--стили без нее форм не работают-->
    <div id="toTop">
        <div class="text-lg text-primary margin-bottom-xl"><span>До повышения цен осталось 2 недели!</span></div>
        <button class="btn ink-reaction btn-danger text-sm">Подключить <span class="hidden-xs">(тестовый период <span class="hidden-sm">15 дней</span>)</span></button>
    </div>

    <div class="card tab-content filter-page">

        <div class="tabs-left">
            @include('billing.modules.partials.left-list')

            <section class="style-default-bright client-info module-list tab-pane" id="first9">
                <section>
                    <div class="tab-content">
                        @include('billing.modules.partials.crm-module')
                        @include('billing.modules.partials.landing-module')
                        @include('billing.modules.partials.disk-module')
                    </div>
                </section>
            </section>
        </div>
    </div>
@stop
@section('scripts')
    <script src="/js/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#connectUsers').click(function() {
                var self = this;
                $(self).attr('disabled', 'true');

                var users = $('#sel').val();

                if (users < 1) {
                    toastr.error('Добавьте хотя бы одного пользователя')
                }
                $.ajax({
                    url: '{{route('billing.modules.ajax.postAjaxCrmAddUser', ['company_alias' => $currentCompany->name])}}',
                    data: { users : users },
                    type: 'POST',
                    dataType: 'json'
                }).error(function(e) {
                    toastr.error('Произошла ошибка, обратитесь в тех. поддержку');
                    $(self).removeAttr('disabled');
                }).success(function(request){
                    $(self).text('Подключен');
                    toastr.success('Тариф успешно подключин. Успешного бизнеса!');
                });
            });

            $('#connectLandingNormal').click(function(){
                var self = this;
                $(self).attr('disabled', true);
                $.ajax({
                    url: '',
                    dataType: 'json',
                    type: 'POST'
                }).success(function(){
                    $(self).text('Подключен');
                    toastr.success('Тариф успешно подключин. Успешного бизнеса!');
                }).error(function(){
                    toastr.error('Произошла ошибка, обратитесь в тех. поддержку');
                    $(self).attr('disabled', false);
                });
            });

            $('#connectLandingPro').click(function(){
                var self = this;
                $(self).attr('disabled', true);

                $.ajax({
                    url: '',
                    dataType: 'json',
                    type: 'POST'
                }).success(function(){
                    $(self).text('Подключен');
                    toastr.success('Тариф успешно подключин. Успешного бизнеса!');
                }).error(function(){
                    toastr.error('Произошла ошибка, обратитесь в тех. поддержку');
                    $(self).attr('disabled', false);
                });
            });

            $('#connectDisk').click(function(){
                var self = this;
                $(self).attr('disabled', true);
                var rateDisk = $('#ratesDisk').val();
                $.ajax({
                    url: '',
                    dataType: 'json',
                    type: 'POST',
                    data: {rate: parseInt(ratesDisk)}
                }).success(function(){
                    $(self).text('Подключен');
                    toastr.success('Тариф успешно подключин. Успешного бизнеса!');
                }).error(function(){
                    toastr.error('Произошла ошибка, обратитесь в тех. поддержку');
                    $(self).attr('disabled', false);
                });
                $(self).attr('disabled', false);
            });
        });
    </script>
    <script type="text/javascript">
        $(function() {
            $(window).scroll(function() {
                if($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });
            $('#toTop').click(function() {
                $('body,html').animate({scrollTop:0},800);
            });
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            function checkBalance(amount) {
                var balance = {{$amount}};

                return balance > parseInt(amount);
            };

            function resetSel() {
                $('#price').text(0);
                $('#connectUsers').attr('disabled', true);
            };

            $('.add-user').click(function(){
                var val = document.getElementById('sel').value;
                var parent = document.getElementById('list');
                parent.innerHTML = '';
                if (val == 0 || val < 0) {
                    resetSel();
                    document.getElementById('sel').value = 0;
                } else {
                    if ($('#year-btn').hasClass('active')) {
                        var selecter = '#year-price';
                    } else {
                        var selecter = '#mounth-price';
                    }
                    var amount = parseInt($(selecter).text()) * val;

                    if (checkBalance(amount)) {
                        $('#price').text(parseInt(amount));
                        $('#connectUsers').removeAttr('disabled');
                    } else {
                        resetSel();
                        toastr.error('Недостаточно средст');
                    }
                }

                for(i=0; i<val; i++) {
                    console.log(val);
                    if (val > 0) {
                        var child =  document.createElement('img');
                        child.src="../../images/people.png";
                        input = parent.appendChild(child);
                        input.type = 'text';
                        input.name = 'name'+(parent.childNodes.length-1);
                    }
                }

                if(val == 0){
                    child =  document.createElement('img');
                    child.src="../../images/heart.png";
                }
            });
        });
    </script>
    <script type="text/javascript">
        var i = null;
        var d = 0;
        var $numbertype = null;

        function ToggleValue() {
            $numbertype.val(parseInt($numbertype.val(), 10) + d);
        }

        $(function() {
            $numbertype = $(".numbertype");
            $("#minus,#plus").mousedown(function() {
                d = $(this).is("#minus") ? -1 : 1;
                i = setInterval(ToggleValue, 70);
            });

            $("#minus,#plus").on("mouseup mouseout", function() {
                clearInterval(i);
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#year-btn, #year-btn1').click(function(){
                $('#price').text(0);
                $('#sel').val(0);
                $('#mounth-price, #mounth-price1').hide();
                $('#year-price, #year-price1').show();
                $('#mounth-btn, #mounth-btn1').removeClass("active");
                $('#year-btn, #year-btn1').addClass("active");
            });
            $('#mounth-btn, #mounth-btn1').click(function(){
                $('#price').text(0);
                $('#sel').val(0);
                $('#year-price, #year-price1').hide();
                $('#mounth-price, #mounth-price1').show();
                $('#year-btn, #year-btn1').removeClass("active");
                $('#mounth-btn, #mounth-btn1').addClass("active");
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="robot"]').change(function(){
                var el = $(this).val();
                if (el==1)
                {
                    $('#robot3').hide();
                    $('#robot2').hide();
                    $('#robot1').show();
                }
                else if (el==2) {
                    $('#robot3').hide();
                    $('#robot2').show();
                    $('#robot1').hide();
                }
                else
                {
                    $('#robot1').hide();
                    $('#robot2').hide();
                    $('#robot3').show();
                }
            });
        });
    </script>
    <script type="text/javascript">
        $('.fraction').each(function(key, value) {
            $this = $(this)
            var split = $this.html().split("/")
            if( split.length == 2 ){
                $this.html('<span class="top">'+split[0]+'</span><span class="bottom">'+split[1]+'</span>')
            }
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#all_func").click(function(){
                $('#closed_case').hide();
                $('#opened_case').show();
            });

            $("#less_func").click(function(){
                $('#opened_case').hide();
                $('#closed_case').show();
            });
        });
    </script>
@stop


