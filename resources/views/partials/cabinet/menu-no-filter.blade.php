<div id="menubar" class="menubar-inverse ">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="/">
                <span class="text-lg text-bold text-primary ">Моя&nbsp;компания</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">
        <ul id="main-menu" class="gui-controls">
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="md md-settings"></i></div>
                    <span class="title">Настройки</span>
                </a>
                <ul>
                    <li><a href="#" ><span class="title">Общие настройки</span></a></li>
                    <li><a href="#" ><span class="title">Доп поля</span></a></li>
                    <li><a href="#" ><span class="title">Теги и статусы</span></a></li>
                    <li><a href="#" ><span class="title">Пользователи</span></a></li>
                    <li><a href="#" ><span class="title">Модули</span></a></li>
                    <li><a href="#" ><span class="title">Баланс</span></a></li>
                    <li><a href="#" ><span class="title">API</span></a></li>
                </ul>
            </li>
        </ul>
    </div><!--end .menubar-scroll-panel-->
</div>