
            <ul class="header-nav header-nav-options">

                <li>
                    <!-- Search form -->
                    <form class="navbar-search" role="search">
                        <div class="form-group">
                            <input type="text" class="form-control" name="headerSearch" placeholder="Вы ищите...">
                        </div>
                        <button type="submit" class="btn btn-icon-toggle ink-reaction"><i class="fa fa-search"></i></button>
                    </form>
                </li>
                <li class="dropdown message-wrap">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <i class="fa fa-bell"></i><sup id="count-message" class="badge style-danger">0</sup>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header">Уведомления</li>
                        {{--<li class="alert alert-callout alert-warning alert-primary-bright">--}}
                            {{--<p><small>Входящий вызов от <span>+788877766667</span></small></p>--}}
                            {{--<a href="#"><strong>БОСС. Рога и копыта</strong></a>--}}
                        {{--</li>--}}
                        <li class="more-message"><a href="../../html/pages/login.html">Посмотреть все сообщения<span class="pull-right"><i class="fa fa-arrow-right"></i></span></a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="btn btn-icon-toggle btn-default" data-toggle="dropdown">
                        <i class="md md-person"></i>
                    </a>
                    <ul class="dropdown-menu animation-expand">
                        <li class="dropdown-header"><p>Информация из личного кабинета пользоватея</p></li>
                        <li class="dropdown-progress">
                            <a href="/billing/profile">
                                Биллинг
                            </a>
                        </li><!--end .dropdown-progress -->
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
            </ul>
