<ul class="header-nav header-nav-toggle">
    <li class="dropdown">
        <a href="#" class="btn btn-icon-toggle hidden-lg hidden-md hidden-sm visible-xs" data-toggle="dropdown" data-original-title="" title=""><i class="fa fa-ellipsis-h"></i></a>
        <button class="btn ink-reaction btn-raised btn-default data hidden-xs" data-toggle="dropdown">
            <span data-original-title="" title="">Данные</span>
        </button>
        <ul class="dropdown-menu animation-expand">
            <li><a href="#" data-original-title="" title=""><i class="md md-delete text-primary"></i>Отметить на удаление</a></li>
        </ul>
    </li>
    <li>
        <a href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false" class="btn btn-icon-toggle hidden-lg hidden-md hidden-sm visible-xs" data-original-title="" title=""><i class="fa fa-plus-square-o"></i></a>
        <a class="btn ink-reaction btn-raised btn-primary hidden-xs" href="#offcanvas-search" data-toggle="offcanvas" data-backdrop="false" data-original-title="" title=""><span data-original-title="" title="">Добавить +</span></a>
    </li>
</ul>

<div class="offcanvas"></div>
