<div id="menubar" class="menubar-inverse">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="/">
                <span class="text-lg text-bold text-primary ">{{object_get($currentCompany, 'display_name', 'ToolsLab')}}</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">
        {!!$menu_general->asUl(['id' => 'main-menu', 'class' => 'gui-controls'])!!}
    </div>
    <!--end .menubar-scroll-panel-->
</div>