<?php

/**
 * Class FormResourceCept
 */
class FormEntityResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/saas/formEntity';

    // tests
    public function getAllFormEntities()
    {
        $this->I->wantTo('Get all form entities');
        $this->haveFormEntity();
        $this->sendGET($this->endpoint);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleFormEntity()
    {
        $this->I->wantTo('Show form entity information');
        $id = $this->haveFormEntity();
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function createFormEntity()
    {
        $this->I->wantTo('Create form entity');
        $formId = $this->haveForm();
        $this->sendAjaxPostRequest($this->endpoint, [
            'form_id' => $formId,
            'en_id' => $this->faker->numberBetween(1, 10),
            'meaning' => ['dates'=>'aaa', 'count'=>1],
            'company_id' => $this->faker->numberBetween(1, 80),
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function updateFormEntity()
    {
        $faker = $this->faker;
        $this->I->wantTo('Update Form');
        $id = $this->haveFormEntity();
        $new_form_id = $this->haveForm();
        $this->sendPUT($this->endpoint . '/' . $id . '/', [
            'form_id'       => $new_form_id,
            'en_id'         => $faker->numberBetween(1, 10),
            'meaning'       => ['dates' => 'aaa', 'count' => 1],
            'company_id'    => $faker->numberBetween(1, 80),
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('Test sort form entities');
        for ($i = 0; $i < 30; $i++) {
            $fId = $this->haveForm();
            $this->haveFormEntity(['form_id'=>$fId, 'company_id'=>$i]);
        }

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'filter' => ['company_id'=>'11']
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/formsEntities');

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'sort' => ['company_id'=>'desc'],
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/formsEntities');

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'paginate' => ['perPage'=>25],
            ],
            'page' => 2
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/formsEntities');
    }
}

$apiTester = new ApiTester($scenario);
$formEntityCept = new FormEntityResourceCept($apiTester);

$formEntityCept->getAllFormEntities();
$formEntityCept->getSingleFormEntity();
$formEntityCept->createFormEntity();
$formEntityCept->updateFormEntity();
$formEntityCept->testSort();
