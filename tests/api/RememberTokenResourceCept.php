<?php


/**
 * Class RememberTokenResourceCept
 */
class RememberTokenResourceCept extends ApiTestCase
{
    public function getUserIdByToken()
    {
        $this->I->wantTo('Get user ID by token');

        $this->haveUser(['remember_token' => 'someToken']);
        $this->sendGET('/api/v1/auth/rememberToken', ['remember_token' => 'someToken']);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }


    public function setUserRememberToken()
    {
        $this->I->wantTo('Set user remember token');
        $email = $this->faker->email;
        $this->haveUser(['email' => $email]);
        $this->sendAjaxPostRequest('/api/v1/auth/rememberToken', ['email' => $email]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }
}

$I = new ApiTester($scenario);
$passwordCept = new RememberTokenResourceCept($I);
$passwordCept->getUserIdByToken();
$passwordCept->setUserRememberToken();
