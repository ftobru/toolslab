<?php

use App\Company;
use Faker\Factory as Faker;

/**
 * Class CompanyResourceCept
 */
class CompanyResourceCept extends ApiTestCase
{
    protected $endpoint = '/api/v1/saas/company';


    public function createCompany()
    {
        $this->I->wantTo('Create Company');
        $this->sendPOST($this->endpoint, [
            'name' => Faker::create()->word,
            'display_name' => Faker::create()->company
        ]);


        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();

    }


    public function getAllCompanies()
    {
        $this->I->wantTo('Get all company');
        $this->haveCompany();
        $this->sendGET($this->endpoint, ['token' => $this->jwtToken]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesJsonPath('$.companies');
    }



    public function getSimpleCompany()
    {
        $companyId = $this->haveCompany();
        $this->I->wantTo('Get to simple company');
        $this->sendGET($this->endpoint . '/' . $companyId);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        //$I->seeResponseContainsJson(['id' => $companyId, 'name' => $company->name]);
        $this->I->expect('there is no root array in response');
    }


    public function updateCompany()
    {
        $companyId = $this->haveCompany();
        $this->I->wantTo('Update company');

        $this->sendPUT($this->endpoint . '/' . $companyId, ['name' => 'test2', 'display_name' => 'test']);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function testSort()
    {
        $this->I->wantTo('Test sort companies');
        for ($i = 0; $i < 3; $i++) {
            $fId = $this->haveCompany(['name' => $i]);
        }

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'filter' => ['name'=>'2']
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/companies');

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'sort' => ['name'=>'desc'],
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/companies');

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'paginate' => ['perPage'=>25],
            ],
            'page' => 2
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/companies');
    }
}

$apiTester = new ApiTester($scenario);
$companyCert = new CompanyResourceCept($apiTester);
$companyCert->getAllCompanies();
$companyCert->createCompany();
$companyCert->getSimpleCompany();
$companyCert->updateCompany();
$companyCert->testSort();
