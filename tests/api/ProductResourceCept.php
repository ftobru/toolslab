<?php

use Faker\Factory as Faker;

/**
 * Class ProductResourceCept
 */
class ProductResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/crm/product';

    // tests

    public function getAllProducts()
    {
        $this->I->wantTo('Get all products');
        $this->haveProduct();
        $this->sendGET($this->endpoint);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleProduct()
    {
        $this->I->wantTo('Get product information');
        $id = $this->haveProduct();
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function createProduct()
    {
        $this->I->wantTo('Create Product');
        $this->sendPOST($this->endpoint, [
            'name' => $this->faker->name,
            'description' => $this->faker->name,
            'article' => $this->faker->name,
            'new_price' => $this->faker->randomFloat(2, 0, 100),
            'old_price' => $this->faker->randomFloat(2, 100, 200),
            'profit' => $this->faker->randomFloat(2, 0, 100),
            'company_id' => $this->haveCompany(),
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function updateProduct()
    {
        $this->I->wantTo('Update Product');
        $id = $this->haveProduct();

        $this->sendPUT($this->endpoint . '/' . $id, [
            'name' => $this->faker->name,
            'description' => $this->faker->name,
            'article' => $this->faker->name,
            'new_price' => $this->faker->randomFloat(2, 0, 100),
            'old_price' => $this->faker->randomFloat(2, 100, 200),
            'profit' => $this->faker->randomFloat(2, 0, 100),
            'company_id' => $this->haveCompany(),
        ]);

        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('Test sort products');
        for ($i = 0; $i < 40; $i+=2) {
            $this->haveProduct(['name' => $i]);
        }
        for ($i = 1; $i < 40; $i+=2) {
            $this->haveProduct(['name' => $i]);
        }

        $companyId = $this->haveCompany();

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'filter' => ['name'=>'2']
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/products/id');

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'sort' => ['name'=>'desc'],
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/products/id');

        $this->sendGET($this->endpoint, [
            'metaData' => [
                'paginate' => ['perPage'=>25],
            ],
            'page' => 1
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/products/id');
    }
}

$apiTester = new ApiTester($scenario);
$productCept = new ProductResourceCept($apiTester);

$productCept->getAllProducts();
$productCept->getSingleProduct();
$productCept->createProduct();
$productCept->updateProduct();
$productCept->testSort();
