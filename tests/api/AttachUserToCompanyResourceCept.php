<?php

use Faker\Factory as Faker;

class AttachUserToCompany extends ApiTestCase
{

    /** @var \Faker\Generator  */
    protected $faker;

    protected $endpoint = '/api/v1/saas/attachUserToCompany';

    /**
     * Attach
     */
    public function store()
    {
        $I = $this->I;
        $companyId = $this->haveCompany();
        $userId = $this->haveUser();

        $I->wantTo('Attach user to company');
        $this->sendPost($this->endpoint, [
            'company' => ['id' => $companyId],
            'user' => ['id' => $userId],
        ]);
        $I->seeResponseJsonMatchesJsonPath('$.user');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }

}
$apiTester = new ApiTester($scenario);
$attachCept = new AttachUserToCompany($apiTester);
$attachCept->store();