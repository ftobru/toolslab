<?php

/**
 * Class ReplacementGeoResourceCept
 */
class ReplacementGeoResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/replacementGeo';

    // tests

    public function getCompanyEntities()
    {
        $this->I->wantTo('Get all company ReplacementGeo');
        $cid = $this->haveCompany();
        $this->haveReplacementGeo(['company_id' => $cid]);
        $this->haveReplacementGeo(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEntityId()
    {
        $this->I->wantTo('Show ReplacementGeo information');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementGeo(['company_id' => $companyId]);
        $this->haveReplacementGeo(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new ReplacementGeo');
        $this->sendPOST(
            $this->endpoint,
            [
                'phone' => $this->faker->phoneNumber,
                'city' => ['2' => 'Hi2', 'a' => 1],
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update ReplacementGeo');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementGeo(['company_id' => $companyId]);
        $this->sendPUT(
            $this->endpoint . '/' . $id . '/',
            [
                'phone' => $this->faker->phoneNumber,
                'city' => ['2' => 'Hi2', 'a' => 1],
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('ReplacementGeo sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveReplacementGeo(
                [
                    'phone' => $i,
                    'city' => json_encode(['2' => 'Hi2', 'a' => 1]),
                    'company_id' => $cid,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['phone'=>5]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementGeo');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['phone'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementGeo');

    }

    public function delete()
    {
        $this->I->wantTo('delete ReplacementGeo');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementGeo(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$id);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$resourceCept = new ReplacementGeoResourceCept($apiTester);

$resourceCept->getCompanyEntities();
$resourceCept->getByEntityId();
$resourceCept->create();
$resourceCept->update();
$resourceCept->delete();
$resourceCept->testSort();
