<?php

use Faker\Factory as Faker;
use Illuminate\Support\Str;

/**
 * Class ActivationCodeCheckerCept
 */
class ActivationCodeCheckerCept extends ApiTestCase
{
    protected $endpoint = '/api/v1/saas/checkActivationCode';



    /**
     * Check activation code.
     */
    public function checkCode()
    {
        $this->I->wantTo('check code activation approve');
        $result = $this->haveUser(['activation_code' => 'testtest']);
        $this->sendGET($this->endpoint, ['activation_code' => 'testtest']);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesJsonPath('result');
    }
}

$I = new ApiTester($scenario);
$activationCert = new ActivationCodeCheckerCept($I);
$activationCert->checkCode();
