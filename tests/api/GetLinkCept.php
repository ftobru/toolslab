<?php

class GetLinkTest
{

    public function all(ApiTester $apiTester)
    {
        $apiTester->sendGET('/landing/scripts/timeLinks', ['links' => [1, 3]]);
        $apiTester->seeResponseCodeIs(200);
    }
}

$getLinkClass = new GetLinkTest();
$getLinkClass->all(new ApiTester($scenario));
