<?php

/**
 * Class ReplacementLinkResourceCept
 */
class ReplacementLinkResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/replacementLink';

    // tests

    public function getCompanyEntities()
    {
        $this->I->wantTo('Get all company ReplacementLinks');
        $cid = $this->haveCompany();
        $this->haveReplacementLink(['company_id' => $cid]);
        $this->haveReplacementLink(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEntityId()
    {
        $this->I->wantTo('Show ReplacementLink information');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementLink(['company_id' => $companyId]);
        $this->haveReplacementLink(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new ReplacementLink');
        $this->sendPOST(
            $this->endpoint,
            [
                'name' =>  $this->faker->name,
                'code' => $this->faker->company,
                'param' => $this->faker->colorName,
                'key_value' => ['2' => 'Hi2', 'a' => 1],
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update ReplacementLink');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementLink(['company_id' => $companyId]);
        $this->sendPUT(
            $this->endpoint . '/' . $id . '/',
            [
                'name' => 'updated_' . $this->faker->name,
                'code' => $this->faker->company,
                'param' => $this->faker->colorName,
                'key_value' => ['2' => 'Hi2', 'a' => 1],
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('ReplacementLink sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveReplacementLink(
                [
                    'name' =>  $i,
                    'code' => $this->faker->company,
                    'param' => $this->faker->colorName,
                    'key_value' => json_encode(['2' => 'Hi2', 'a' => 1]),
                    'company_id' => $cid,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['name'=>5]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementLinks');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['name'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementLinks');

    }

    public function delete()
    {
        $this->I->wantTo('delete ReplacementLink');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementLink(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$id);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$resourceCept = new ReplacementLinkResourceCept($apiTester);

$resourceCept->getCompanyEntities();
$resourceCept->getByEntityId();
$resourceCept->create();
$resourceCept->update();
$resourceCept->delete();
$resourceCept->testSort();
