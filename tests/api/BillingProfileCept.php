<?php


use Billing\Services\Rates\CrmUsersLiteRate;

class BillingProfileCept
{
    /** @var  ApiTester */
    protected $I;

    /** @var  \Faker\Generator */
    protected $faker;

    /**
     * @param ApiTester $tester
     */
    public function __construct(ApiTester $tester)
    {
        $this->I = $tester;
        $this->faker = \Faker\Factory::create();

    }


    /**
     * @param array $data
     * @return mixed
     */
    public function haveCompany($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->company,
            'display_name' => $this->faker->company,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('companies', $data);
    }
    public function getCurrentBalance()
    {
        $this->I->sendGET('/billing/ajax/profile/balance');
        $this->I->seeResponseCodeIs(200);
    }

    public function getExpensesMonth()
    {
        $this->I->sendGET('/billing/ajax/profile/expensesMonth');
        $this->I->seeResponseCodeIs(200);
    }


    public function paymentTariff()
    {
        $this->I->sendPOST('/billing/ajax/profile/paymentTariff', [
            'rate_id' => CrmUsersLiteRate::IDENTIFIER,
            'month' => 2,
            'company_id' => $this->haveCompany()
        ]);

        $this->I->seeResponseCodeIs(500);
    }
}

$billingProfileCept = new BillingProfileCept(new ApiTester($scenario));
$billingProfileCept->getCurrentBalance();
$billingProfileCept->getExpensesMonth();
$billingProfileCept->paymentTariff();