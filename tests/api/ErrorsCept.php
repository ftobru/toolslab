<?php 

use Faker\Factory as Faker;


class ErrorCept extends ApiTestCase
{

    protected $endpoint404 = '/api/v1/404';
    protected $endpoint422 = '/api/v1/saas/user';



    public function testCase404()
    {
        $this->I->wantTo('Test error was json');
        $this->sendAjaxPostRequest($this->endpoint404, [
                'testData' => $this->faker->words()
            ]);
        $this->I->seeResponseCodeIs(404);
        $this->I->seeResponseIsJson();
    }

    public function testCase422()
    {
        $this->sendAjaxPostRequest($this->endpoint422, [
            'testData' => $this->faker->words()
        ]);
        $this->I->seeResponseCodeIs(422);
        $this->I->seeResponseIsJson();
    }
}

$errorCept = new ErrorCept(new ApiTester($scenario));

$errorCept->testCase404();
$errorCept->testCase422();