<?php

/**
 * Class OwnCompanyResourceCept
 */
class OwnCompanyResourceCept extends ApiTestCase
{
    /** @var  string */
    protected $endpoint = '/api/v1/saas/ownCompany';

    public function getAllCompany()
    {
        $this->I->wantTo('Get all company');
        $this->sendGET($this->endpoint);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseJsonMatchesJsonPath('$.company');
    }
}
$tester = new ApiTester($scenario);
$ownCompanyCept = new OwnCompanyResourceCept($tester);
$ownCompanyCept->getAllCompany();
