<?php
use Faker\Factory as Faker;

/**
 * Class BillingAccountResourceCept
 */
class BillingPaymentResourceCept extends ApiTestCase
{

    /** @var  string */
    protected $endpoint = '/api/v1/billing/payment';

    public function getPayments()
    {
        $I = $this->I;
        $I->wantTo('Get billing account');
        $this->sendGET($this->endpoint);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}
$apiTester = new ApiTester($scenario);
/** @var BillingPaymentResourceCept $billing */
$billing = new BillingPaymentResourceCept($apiTester);
$billing->getPayments();
