<?php
// Here you can initialize variables that will be available to your tests

use Faker\Factory as Faker;

/**
 * Class ApiTestCase
 */
abstract class ApiTestCase
{
    /** @var ApiTester  */
    protected $I;

    /** @var  string */
    protected $jwtToken;

    /** @var  string */
    protected $oauthToken;

    /** @var  string */
    private $endJwtTokenPoint = '/api/v1/auth/login';

    /** @var  string */
    private $endOauthTokenPoint = '/service/oauth/token';

    /** @var  string */
    private $secretClient = 'MySecretKey';

    /** @var  string */
    protected $clientId = 1;

    /** @var  string */
    protected $endpoint;

    protected $user;

    protected $userCompanyId = 0;

    /** @var \Faker\Generator  */
    protected $faker;
    /**
     * @return ApiTester
     */
    public function getI()
    {
        return $this->I;
    }

    /**
     * @param string $oauthToken
     */
    public function setOauthToken($oauthToken)
    {
        $this->oauthToken = $oauthToken;

    }

    /**
     * @return string
     */
    public function getJwtToken()
    {
        return $this->jwtToken;
    }

    /**
     * @return string
     */
    public function getOauthToken()
    {
        return $this->oauthToken;
    }

    /**
     * @return string
     */
    public function getEndJwtTokenPoint()
    {
        return $this->endJwtTokenPoint;
    }

    /**
     * @return string
     */
    public function getEndOauthTokenPoint()
    {
        return $this->endOauthTokenPoint;
    }

    /**
     * @return string
     */
    public function getSecretClient()
    {
        return $this->secretClient;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param ApiTester $I
     */
    public function __construct(ApiTester $I)
    {
        /** @var  I */
        $this->I = $I;
        $this->faker = Faker::create();
        $this->setOuthToken();
        $this->login();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveOauthToken($data = [])
    {

        $data = array_merge([
            'secret' => $this->secretClient,
            'name' => 'front',
            'id' => $this->clientId,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);

        return $this->I->haveRecord('oauth_clients', $data);
    }

    /**
     * @return mixed
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setOuthToken()
    {
        $clientId = $this->haveOauthToken();
        $this->I->sendPOST($this->endOauthTokenPoint, [
            'client_id' => $this->clientId,
            'client_secret' => $this->secretClient,
            'grant_type' => 'client_credentials'
        ]);

        $this->oauthToken = json_decode($this->I->grabResponse(), true)['access_token'];

    }

    protected function login()
    {
        $this->userCompanyId = $this->haveCompany();
        $email = Faker::create()->email;
        $this->haveUser(['email' => $email, 'userCompanyId' => $this->userCompanyId]);
        $this->I->wantTo('Auth');
        $this->sendPOST($this->endJwtTokenPoint, [
            'email' => $email,
            'password' => 'test',

        ], false);

        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesJsonPath('$.token');

        $response = json_decode($this->I->grabResponse(), true);
        $this->jwtToken = $response['token'];
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendAjaxGetRequest($uri, $params = [], $tokenJwtEnable = true)
    {
        if ($tokenJwtEnable) {
            $params = array_merge([
                'token' => $this->jwtToken,
                'access_token' => $this->oauthToken,
                'grant_type' => 'client_credentials'
            ], $params);
        } else {
            $params = array_merge(['access_token' => $this->oauthToken], $params);
        }
        return $this->I->sendAjaxGetRequest($uri, $params);
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendGET($uri, $params = [], $tokenJwtEnable = true)
    {
        if ($tokenJwtEnable) {
            $params = array_merge([
                'token' => $this->jwtToken,
                'access_token' => $this->oauthToken,
                'grant_type' => 'client_credentials'
            ], $params);
        } else {
            $params = array_merge(['access_token' => $this->oauthToken], $params);
        }
        return $this->I->sendGET($uri, $params);
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendDELETE($uri, $params = [], $tokenJwtEnable = true)
    {
        if ($tokenJwtEnable) {
            $params = array_merge([
                'token' => $this->jwtToken,
                'access_token' => $this->oauthToken,
                'grant_type' => 'client_credentials'
            ], $params);
        } else {
            $params = array_merge(['access_token' => $this->oauthToken], $params);
        }
        return $this->I->sendDELETE($uri, $params);
    }



    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendAjaxPostRequest($uri, $params = [], $tokenJwtEnable = true)
    {
        $uri = $this->uriBuilder($uri, $tokenJwtEnable);
        $params = $this->buildParams($params);
        return $this->I->sendAjaxPostRequest($uri, $params);
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendPOST($uri, $params = [], $tokenJwtEnable = true)
    {

        $uri = $this->uriBuilder($uri, $tokenJwtEnable);
        $params = array_merge($params, ['access_token' => $this->oauthToken]);
        return $this->I->sendPOST($uri, $params);
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendPOSTWithFiles($uri, $params = [], $files = null)
    {

        $uri = $this->uriBuilder($uri, true);
        $params = array_merge($params, ['access_token' => $this->oauthToken]);
        return $this->I->sendPOST($uri, $params, $files);
    }



    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendPUT($uri, $params = [], $tokenJwtEnable = true)
    {

        $uri = $this->uriBuilder($uri, $tokenJwtEnable);
        $params = array_merge($params, ['access_token' => $this->oauthToken]);
        return $this->I->sendPUT($uri, $params);
    }

    /**
     * @param $uri
     * @param bool $tokenJwtEnable
     * @return string
     */
    protected function uriBuilder($uri, $tokenJwtEnable = true)
    {
        if ($tokenJwtEnable) {
            $uri .= '?token=' . $this->jwtToken;
        }
        return $uri;
    }

    /**
     * @param array $params
     * @return array
     */
    protected function buildParams($params = [])
    {
        return array_merge($params, ['access_token' => $this->oauthToken]);
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendAjaxPutRequest($uri, $params = [], $tokenJwtEnable = true)
    {
        $uri = $this->uriBuilder($uri, $tokenJwtEnable);

        return $this->I->sendAjaxRequest('PUT', $uri, $params);
    }

    /**
     * @param $uri
     * @param array $params
     * @param bool $tokenJwtEnable
     * @return mixed
     */
    public function sendAjaxDeleteRequest($uri, $params = [], $tokenJwtEnable = true)
    {
        $uri = $this->uriBuilder($uri, $tokenJwtEnable);

        return $this->I->sendAjaxRequest('DELETE', $uri, $params);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveCompany($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->company,
            'display_name' => $this->faker->company,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('companies', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveInsertLink($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->name,
            'code' => $this->faker->company,
            'param' => $this->faker->colorName,
            'key_value' => json_encode(['1'=>'Hi']),
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('insert_links', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveDomain($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->domainName,
            'status' => $this->faker->numberBetween(-1, 1),
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('domains', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveReplacementLink($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->name,
            'code' => $this->faker->company,
            'param' => $this->faker->colorName,
            'key_value' => json_encode(['1'=>'Hi']),
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('replacement_links', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveReplacementGeo($data = [])
    {
        $data = array_merge([
            'city' =>  json_encode(['Name'=>'Novosibirsk']),
            'phone' => $this->faker->phoneNumber,
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('replacement_geo', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveReplacementParam($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->name,
            'tag' =>  $this->faker->text(255),
            'meaning' =>  $this->faker->text(255),
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('replacement_params', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveReplacementTime($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->name,
            'tag' =>  $this->faker->text(255),
            'meaning' =>  $this->faker->text(255),
            'company_id' => $this->haveCompany(),
            'finish_time' =>  new DateTime(),
            'start_time' => new DateTime(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('replacement_time', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveCart($data = [])
    {
        $data = array_merge([
            'order_id' => $this->haveOrder(),
            'content' => json_encode(['product'=>'test_product']),
            'qty' => 1,
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->I->haveRecord('cart', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveUser($data = [])
    {
        $this->userCompanyId = array_get($data, 'userCompanyId', null);
        unset($data['userCompanyId']);

        $data = array_merge([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'password' => bcrypt('test'),
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime()
        ], $data);

        $userId = $this->I->haveRecord('users', $data);


        if ($this->userCompanyId) {
            //$this->I->haveInDatabase('company_user', ['user_id'=>$userId,'company_id'=>$this->userCompanyId]);
            //$this->I->haveRecord('company_user', ['user_id'=>$userId,'company_id'=>$this->userCompanyId]);
            DB::table('company_user')->insert(
                ['user_id'=>$userId,'company_id'=>$this->userCompanyId]
            );
        }
        return $userId;
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveTask($data = [])
    {
        $data = array_merge([
            'client_id' => $this->haveClient(),
            'order_id' => $this->haveOrder(),
            'type' => 1,
            'responsible_user_id' => $this->haveUser(),
            'performer_user_id' => $this->haveUser(),
            'comment' => $this->faker->address,
            'state' => $this->faker->numberBetween(0, 1),
            'company_id' => $this->haveCompany(),
            'date_perf' => new DateTime(),
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime()
        ], $data);

        return $this->I->haveRecord('tasks', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveOrder($data = [])
    {
        $data = array_merge([
            'client_id' => $this->haveClient(),
            'status' => 1,
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime()
        ], $data);

        return $this->I->haveRecord('orders', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveForm($data = [])
    {
        $companyID = $this->haveCompany();

        $data = array_merge([
            'en_type'       => $this->faker->numberBetween(1, 2),
            'form_type'     => $this->faker->numberBetween(1, 10),
            'placeholder'   => $this->faker->name,
            'validator'     => json_encode(['test_key'=>'test_value']),
            'label'         => $this->faker->name,
            'company_id'    => $companyID,
            'default_value' => json_encode(['test_key'=>'test_value']),
            'created_at'    => new DateTime(),
            'updated_at'    => new  DateTime()
        ], $data);

        return $this->I->haveRecord('forms', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveFormEntity($data = [])
    {
        $formId = $this->haveForm();

        $data = array_merge([
            'form_id'       => $formId,
            'en_id'         => $this->faker->numberBetween(1, 10),
            'meaning'       => json_encode(['test_key' => 'test_value']),
            'company_id'    => $this->haveCompany(),
            'created_at'    => new DateTime(),
            'updated_at'    => new  DateTime()
        ], $data);

        return $this->I->haveRecord('forms_entity', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveProduct($data = [])
    {
        $data = array_merge([
            'name' =>  $this->faker->name,
            'description' => $this->faker->name,
            'article' => $this->faker->name,
            'new_price' => $this->faker->randomFloat(2, 0, 100),
            'old_price' => $this->faker->randomFloat(2, 100, 200),
            'profit' => $this->faker->randomFloat(2, 0, 100),
            'company_id' => $this->haveCompany(),
            'created_at'=> new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $this->I->haveRecord('products', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveRole($data = [])
    {
        $data = array_merge([
            'name' => 'Administrator',
            'display_name' => 'Администратор',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $this->I->haveRecord('roles', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function haveClient($data = [])
    {
        $data = array_merge([
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $this->haveCompany(),
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $this->I->haveRecord('clients', $data);
    }
}
