<?php

/**
 * Class ReplacementParamResourceCept
 */
class ReplacementParamResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/replacementParam';

    // tests

    public function getCompanyEntities()
    {
        $this->I->wantTo('Get all company ReplacementParams');
        $cid = $this->haveCompany();
        $this->haveReplacementParam(['company_id' => $cid]);
        $this->haveReplacementParam(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEntityId()
    {
        $this->I->wantTo('Show ReplacementParam information');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementParam(['company_id' => $companyId]);
        $this->haveReplacementParam(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new ReplacementParam');
        $this->sendPOST(
            $this->endpoint,
            [
                'name' =>  $this->faker->name,
                'tag' =>  $this->faker->text(255),
                'meaning' =>  $this->faker->text(255),
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update ReplacementParam');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementParam(['company_id' => $companyId]);
        $this->sendPUT(
            $this->endpoint . '/' . $id . '/',
            [
                'name' =>  $this->faker->name,
                'tag' =>  $this->faker->text(255),
                'meaning' =>  $this->faker->text(255),
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('ReplacementParam sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveReplacementParam(
                [
                    'name' =>  $i,
                    'tag' =>  $i,
                    'meaning' =>  $this->faker->text(255),
                    'company_id' => $cid,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['name'=>5]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementParams');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['name'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementParams');

    }

    public function delete()
    {
        $this->I->wantTo('delete ReplacementParam');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementParam(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$id);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$resourceCept = new ReplacementParamResourceCept($apiTester);

$resourceCept->getCompanyEntities();
$resourceCept->getByEntityId();
$resourceCept->create();
$resourceCept->update();
$resourceCept->delete();
$resourceCept->testSort();
