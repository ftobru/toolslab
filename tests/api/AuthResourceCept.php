<?php


use App\References\UserReference;
use Faker\Factory as Faker;

/**
 * Class AuthResourceCept
 */
class AuthResourceCept extends ApiTestCase
{

    public function registrationUser()
    {
        $this->I->wantTo('Registration user');

        $this->sendPOST('/api/v1/auth/registration', [
            'users' => [
                'name' => Faker::create('RU_ru')->name,
                'email' => Faker::create()->email,
                'password' => Faker::create()->password(),
                'phone' => Faker::create()->phoneNumber,
            ],
        ]);
        $this->I->grabRecord('users', ['id' => 8523]);
        $this->I->grabResponse();
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesJsonPath('registration');
    }

    public function checkRememberToken()
    {
        $id = $this->haveUser(['remember_token' => 'testtesttesttesttesetsetest']);
        $this->I->wantTo('Check remember token');
        $this->sendGET('/api/v1/auth/rememberToken', ['remember_token' => 'testtesttesttesttesetsetest']);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function passwordRemember()
    {
        $id = $this->haveUser([
            'password' => bcrypt('test'), 'remember_token' => 'testtesttesttesttesetsetest'
        ]);
        $this->I->wantTo('remember password');
        $this->sendPOST('/api/v1/auth/password', [
            'password' => 'test',
            'remember_token' => 'testtesttesttesttesetsetest'
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }


    public function logoutUser()
    {
        $this->I->wantTo('Logout user');
        $this->sendAjaxGetRequest('/api/v1/auth/logout', []);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function editUserRegistrationData()
    {
        $this->I->wantTo('Edit user registration data');

        $id = $this->haveUser([
            'password' => 'test',
            'remember_token' => 'testtesttesttesttesetsetest',
            'phone' => '98887776666',
            'email' => 'testmail@test.com',
            'status' => 0,
        ]);



        // must Response error
        $this->sendPOST('/api/v1/auth/registration', [
            'users' => [
                'name' => Faker::create('RU_ru')->name,
                'email' => 'newtestmail@test.ru',
                'password' => 'newpasswd',
                'phone' => '98887776666',
            ],
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();

        // Check
        $this->sendPOST('/api/v1/auth/registration', [
            'users' => [
                'name' => Faker::create('RU_ru')->name,
                'email' => 'zzz@test.ru',
                'password' => 'newpasswd',
                'phone' => '90007776666',
                'status' => 0,
            ],
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $resp = json_decode($this->I->grabResponse());
        if (property_exists($resp, 'registration')) {
            foreach ($resp->registration as $stdObj) {
                if (property_exists($stdObj, 'user')) {
                    $reg_token = $stdObj->user->registration_token;
                    $id = $stdObj->user->id;
                    $this->sendPUT('/api/v1/auth/registration/' . $id, [
                        'email' => 'zzz1111111@test.ru',
                        'phone' => '90007771111',
                        'registration_token' => $reg_token
                    ]);
                    $this->I->seeResponseCodeIs(200);
                    $this->I->seeResponseIsJson();
                }
            }
        }
    }
}

$apiTester = new ApiTester($scenario);
$authCept = new AuthResourceCept($apiTester);
$authCept->registrationUser();
$authCept->checkRememberToken();
$authCept->passwordRemember();
$authCept->logoutUser();
$authCept->editUserRegistrationData();
