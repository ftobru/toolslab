<?php

/**
 * Class DomainResourceCept
 */
class DomainResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/domain';

    // tests

    public function getCompanyEntities()
    {
        $this->I->wantTo('Get all company Domains');
        $cid = $this->haveCompany();
        $this->haveDomain(['company_id' => $cid]);
        $this->haveDomain(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEntityId()
    {
        $this->I->wantTo('Show Domain information');
        $companyId = $this->haveCompany();
        $id = $this->haveDomain(['company_id' => $companyId]);
        $this->haveDomain(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new Domain');
        $this->sendPOST(
            $this->endpoint,
            [
                'name' =>  $this->faker->domainName,
                'status' => $this->faker->numberBetween(-1, 1),
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update Domain');
        $companyId = $this->haveCompany();
        $id = $this->haveDomain(['company_id' => $companyId]);
        $this->sendPUT(
            $this->endpoint . '/' . $id . '/',
            [
                'name' => 'updated-' . $this->faker->domainName,
                'status' => $this->faker->numberBetween(-1, 1),
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('Domain sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveDomain(
                [
                    'name' =>  $i,
                    'status' => 1,
                    'company_id' => $cid,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['name'=>5]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/domains');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['name'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/domains');

    }

    public function delete()
    {
        $this->I->wantTo('delete Domain');
        $companyId = $this->haveCompany();
        $id = $this->haveDomain(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$id);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$resourceCept = new DomainResourceCept($apiTester);

$resourceCept->getCompanyEntities();
$resourceCept->getByEntityId();
$resourceCept->create();
$resourceCept->update();
$resourceCept->delete();
$resourceCept->testSort();
