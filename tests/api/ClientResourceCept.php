<?php

/**
 * Class ClientResourceCept
 */
class ClientResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/crm/client';

    // tests

    public function getCompanyClients()
    {
        $this->I->wantTo('Get all clients');
        $companyId = $this->haveCompany();
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $companyId,
        ]);
        $this->sendGET($this->endpoint, ['company_id' => $companyId]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleClient()
    {
        $this->I->wantTo('Get client information');
        $companyId = $this->haveCompany();
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $companyId,
        ]);
        $response = json_decode($this->I->grabResponse());
        $this->sendGET($this->endpoint . '/' . $response->client->id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function createClient()
    {
        $this->I->wantTo('Create client');
        $companyID = $this->haveCompany();

        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $companyID,
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function updateClient()
    {
        $this->I->wantTo('Update client');
        $id = $this->haveClient();

        $this->sendPUT($this->endpoint . '/' . $id, [
            'tags' => ['tag3, tag4'],
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $this->haveCompany(),
        ]);

        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('Get all clients');
        $companyId = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveClient([
                'company_id' => $companyId,
                'status' => $i,
                'type' => $i+1,
            ]);
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'sort' => ['type'=>'desc'],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'sort' => ['status'=>'desc'],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['type'=> 5],
            ]
        ]);

        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['status'=> 5],
            ]
        ]);

        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

    }

    public function testTagFilter()
    {
        $companyId = $this->haveCompany();
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $companyId,
        ]);
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag2, tag3'],
            'status' => $this->faker->randomDigit,
            'type' => $this->faker->randomDigit,
            'company_id' => $companyId,
        ]);
        $this->I->wantTo('Test tag filter for clients');
        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['withAnyTag' => ['tag1']],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['withAllTags' => ['tag2', 'tag3']],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$clientCept = new ClientResourceCept($apiTester);

$clientCept->getCompanyClients();
$clientCept->getSingleClient();
$clientCept->createClient();
$clientCept->updateClient();
$clientCept->testSort();
$clientCept->testTagFilter();
