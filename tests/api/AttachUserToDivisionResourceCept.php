<?php

use Faker\Factory as Faker;

/**
 * Class AttachUserToCompanyDivision
 */
class AttachUserToDivision extends ApiTestCase
{

    /** @var \Faker\Generator  */
    protected $faker;

    protected $endpoint = '/api/v1/saas/attachUserToCompanyDivision';

    /**
     * Attach
     */
    public function store()
    {
        $I = $this->I;
        $companyId = $this->haveCompany();
        $userId = $this->haveUser();

        $I->wantTo('Attach user to company divisions');
        $this->sendPost($this->endpoint, [
            'company' => ['id' => $companyId],
            'user' => ['id' => $userId],
            'divisions' => ['HR', 'Develop']
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('$.profile');
    }

}
$apiTester = new ApiTester($scenario);
$attachCept = new AttachUserToDivision($apiTester);
$attachCept->store();
