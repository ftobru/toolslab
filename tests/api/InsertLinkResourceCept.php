<?php

/**
 * Class InsertLinkResourceCept
 */
class InsertLinkResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/insertLink';

    // tests

    public function getCompanyEntities()
    {
        $this->I->wantTo('Get all company InsertLinks');
        $cid = $this->haveCompany();
        $this->haveInsertLink(['company_id' => $cid]);
        $this->haveInsertLink(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEntityId()
    {
        $this->I->wantTo('Show InsertLink information');
        $companyId = $this->haveCompany();
        $id = $this->haveInsertLink(['company_id' => $companyId]);
        $this->haveInsertLink(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new InsertLink');
        $this->sendPOST(
            $this->endpoint,
            [
                'name' =>  $this->faker->name,
                'code' => $this->faker->company,
                'param' => $this->faker->colorName,
                'key_value' => ['2' => 'Hi2', 'a' => 1],
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update InsertLink');
        $companyId = $this->haveCompany();
        $id = $this->haveInsertLink(['company_id' => $companyId]);
        $this->sendPUT(
            $this->endpoint . '/' . $id . '/',
            [
                'name' => 'updated_' . $this->faker->name,
                'code' => $this->faker->company,
                'param' => $this->faker->colorName,
                'key_value' => ['2' => 'Hi2', 'a' => 1],
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('InsertLink sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveInsertLink(
                [
                    'name' =>  $i,
                    'code' => $this->faker->company,
                    'param' => $this->faker->colorName,
                    'key_value' => json_encode(['2' => 'Hi2', 'a' => 1]),
                    'company_id' => $cid,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['name'=>5]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/insertLinks');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['name'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/insertLinks');

    }

    public function delete()
    {
        $this->I->wantTo('delete InsertLink');
        $companyId = $this->haveCompany();
        $id = $this->haveInsertLink(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$id);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$resourceCept = new InsertLinkResourceCept($apiTester);

$resourceCept->getCompanyEntities();
$resourceCept->getByEntityId();
$resourceCept->create();
$resourceCept->update();
$resourceCept->delete();
$resourceCept->testSort();
