<?php

/**
 * Class ReplacementTimeResourceCept
 */
class ReplacementTimeResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/replacementTime';

    // tests

    public function getCompanyEntities()
    {
        $this->I->wantTo('Get all company ReplacementTimes');
        $cid = $this->haveCompany();
        $this->haveReplacementTime(['company_id' => $cid]);
        $id = $this->haveReplacementTime(['company_id' => $cid]);
        $this->I->grabRecord('replacement_time',['id' => $id]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEntityId()
    {
        $this->I->wantTo('Show ReplacementTime information');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementTime(['company_id' => $companyId]);
        $this->haveReplacementTime(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new ReplacementTime');
        $this->sendPOST(
            $this->endpoint,
            [
                'name' =>  $this->faker->name,
                'tag' =>  $this->faker->text(255),
                'start_time' => $this->faker->time(),
                'finish_time' => $this->faker->time(),
                'meaning' =>  $this->faker->text(255),
                'company_id' => $this->haveCompany(),
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update ReplacementTime');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementTime(['company_id' => $companyId]);
        $this->sendPUT(
            $this->endpoint . '/' . $id . '/',
            [
                'name' =>  'updated name',
                'tag' =>  'some tag',
                'meaning' =>  'abc',
                'start_time' => $this->faker->time(),
                'finish_time' => $this->faker->time(),
                'company_id' => $companyId,
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('ReplacementTime sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveReplacementTime(
                [
                    'name' =>  $i,
                    'tag' =>  $i,
                    'meaning' =>  $this->faker->text(255),
                    'company_id' => $cid,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['name'=>5]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementTimes');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['name'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/replacementTimes');

    }

    public function delete()
    {
        $this->I->wantTo('delete ReplacementTime');
        $companyId = $this->haveCompany();
        $id = $this->haveReplacementTime(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$id);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$resourceCept = new ReplacementTimeResourceCept($apiTester);

$resourceCept->getCompanyEntities();
$resourceCept->getByEntityId();
$resourceCept->create();
$resourceCept->update();
$resourceCept->delete();
$resourceCept->testSort();
