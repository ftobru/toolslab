<?php

/**
 * Class TaskResourceCept
 */
class TaskResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/crm/task';

    // tests

    public function getCompanyTasks()
    {
        $this->I->wantTo('Get all company tasks');
        $cid = $this->haveCompany();
        $this->haveTask(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleTask()
    {
        $this->I->wantTo('Show task information');
        $cid = $this->haveCompany();
        $tid = $this->haveTask(['company_id' => $cid]);
        $this->sendGET($this->endpoint . '/' . $tid);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function createTask()
    {
        $this->I->wantTo('Create new task');
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag2, tag3'],
            'client_id' => $this->haveClient(),
            'order_id' => $this->haveOrder(),
            'type' => 1,
            'responsible_user_id' => $this->haveUser(),
            'performer_user_id' => $this->haveUser(),
            'comment' => $this->faker->address,
            'state' => $this->faker->numberBetween(0, 1),
            'company_id' => $this->haveCompany(),
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function updateTask()
    {
        $faker = $this->faker;
        $this->I->wantTo('Update task');
        $id = $this->haveTask();
        $this->sendPUT($this->endpoint . '/' . $id . '/', [
            'tags' => ['tag2, tag3'],
            'client_id' => $this->haveClient(),
            'order_id' => $this->haveOrder(),
            'type' => 1,
            'responsible_user_id' => $this->haveUser(),
            'performer_user_id' => $this->haveUser(),
            'comment' => $this->faker->address,
            'state' => $this->faker->numberBetween(0, 1),
            'company_id' => $this->haveCompany(),
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('task sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveTask(
                [
                    'company_id' => $cid,
                    'type' => $i,
                    'state' => $i + 1,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['type'=>11]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/tasks');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['type'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/tasks');

    }

    public function deleteTask()
    {
        $this->I->wantTo('test delete task');
        $tid = $this->haveTask();
        $this->sendDELETE($this->endpoint.'/'.$tid);
        $this->I->grabResponse();
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testTagFilter()
    {
        $companyId = $this->haveCompany();
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag2, tag3'],
            'client_id' => $this->haveClient(),
            'order_id' => $this->haveOrder(),
            'type' => 1,
            'responsible_user_id' => $this->haveUser(),
            'performer_user_id' => $this->haveUser(),
            'comment' => $this->faker->address,
            'state' => $this->faker->numberBetween(0, 1),
            'company_id' => $companyId,
        ]);
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'client_id' => $this->haveClient(),
            'order_id' => $this->haveOrder(),
            'type' => 1,
            'responsible_user_id' => $this->haveUser(),
            'performer_user_id' => $this->haveUser(),
            'comment' => $this->faker->address,
            'state' => $this->faker->numberBetween(0, 1),
            'company_id' => $companyId,
        ]);
        $this->I->wantTo('Test tag filter');
        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['withAnyTag' => ['tag1']],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['withAllTags' => ['tag2', 'tag3']],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$taskCept = new TaskResourceCept($apiTester);

$taskCept->getCompanyTasks();
$taskCept->getSingleTask();
$taskCept->createTask();
$taskCept->updateTask();
$taskCept->deleteTask();
$taskCept->testSort();
//$taskCept->testTagFilter();
