<?php
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FilemanagerCept
 */
class FilemanagerCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/landing/filemanager/connectors';

    protected $storage = '/FileStorage';

    // tests

    /**
     *  mode=getinfo&path=/UserFiles/Image/logo.png&getsize=true
     */
    public function getInfo()
    {
        $this->I->wantTo('Get file info');
        $params = [
            'mode' => 'getinfo',
            'path' => $this->storage . '/' . $this->getFolderName() . '/tmp/test.png',
            'getsize' => true
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    /**
     * mode=getfolder&path=/UserFiles/Image/&getsizes=true&type=images
     */
    public function getFolder()
    {
        $this->I->wantTo('Get folder');
        $params = [
            'mode' => 'getfolder',
            'path' => $this->storage . '/' . $this->getFolderName() . '/',
            'getsize' => true,
            'type' => 'images'
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    /**
     * mode=rename&old=/UserFiles/Image/logo.png&new=id.png
     */
    public function rename()
    {
        $this->I->wantTo('Rename');
        $params = [
            'mode' => 'rename',
            'old' => $this->storage . '/' . $this->getFolderName() . '/new.png',
            'new' => 'new2.png'
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
        $params = [
            'mode' => 'rename',
            'old' => $this->storage . '/' . $this->getFolderName() . '/new2.png',
            'new' => 'new.png'
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    /**
     * mode=move&old=/uploads/images/original/Image/logo.png&new=/moved/&root=/uploads/images/
     */
    public function move()
    {
        $this->I->wantTo('move');
        $params = [
            'mode' => 'move',
            'old' => $this->storage . '/' . $this->getFolderName() . '/logo.png',
            'new' => '/tmp/',
            'root' => $this->storage . '/' . $this->getFolderName() . '/'
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
        $params = [
            'mode' => 'move',
            'old' => $this->storage . '/' . $this->getFolderName() . '/tmp/logo.png',
            'new' => '/',
            'root' => $this->storage . '/' . $this->getFolderName() . '/'
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    /**
     * mode=delete&path=/UserFiles/Image/logo.png
     */
    public function delete()
    {
        $this->I->wantTo('delete');
        $params = [
            'mode' => 'delete',
            'path' => $this->storage . '/' . $this->getFolderName() . '/logo.png',
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function add()
    {
        $this->I->wantTo('add');
        $params = [
            'mode' => 'add',
            'currentpath' => $this->storage . '/' . $this->getFolderName() . '/tmp/'
        ];
        $path = $this->faker->file(codecept_data_dir() . 'filemanagerResource/', codecept_data_dir());
        $this->sendPOSTWithFiles(
            $this->endpoint,
            $params,
            [new UploadedFile($path, 'test.png', null, 1221)]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
        $params = [
            'mode' => 'add',
            'currentpath' => $this->storage . '/' . $this->getFolderName() . '/tmp/'
        ];
        $path = $this->faker->file(codecept_data_dir() . 'filemanagerResource/', codecept_data_dir());
        $this->sendPOSTWithFiles(
            $this->endpoint,
            $params,
            [new UploadedFile($path, 'new.png', null, 1221)]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function addFolder()
    {
        $this->I->wantTo('addfolder');
        $params = [
            'mode' => 'addfolder',
            'path' => $this->storage . '/' . $this->getFolderName() . '/',
            'name' => 'new12Folder'
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function download()
    {
        $this->I->wantTo('download');
        $params = [
            'mode' => 'download',
            'path' => $this->storage . '/' . $this->getFolderName() . '/new.png',
        ];
        $this->sendGET($this->endpoint, $params);
        $this->I->seeResponseCodeIs(200);
    }

    public function replace()
    {
        $this->I->wantTo('replace');
        $params = [
            'mode' => 'replace',
            'newfilepath' => $this->storage . '/' . $this->getFolderName() . '/tmp/new.png'
        ];
        $path = $this->faker->file(codecept_data_dir() . 'filemanagerResource/', codecept_data_dir());
        $this->sendPOSTWithFiles(
            $this->endpoint,
            $params,
            [new UploadedFile($path, 'new.png', null, 1221)]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    private function getFolderName()
    {
        $cid = $this->userCompanyId;
        $this->I->seeRecord('companies', ['id' => $cid]);
        $company = $this->I->grabRecord('companies', ['id' => $cid]);
        return $this->userCompanyId . '_' . md5($company->created_at);
    }
}

$apiTester = new ApiTester($scenario);
$filemanagerCept = new FilemanagerCept($apiTester);

$filemanagerCept->add();
$filemanagerCept->getInfo();
$filemanagerCept->rename();
$filemanagerCept->move();
$filemanagerCept->getFolder();
$filemanagerCept->delete();
$filemanagerCept->addFolder();
$filemanagerCept->download();
$filemanagerCept->replace();
