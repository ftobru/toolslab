<?php

/**
 * Class CartResourceCept
 */
class CartResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/crm/cart';

    // tests

    public function getCompanyCarts()
    {
        $this->I->wantTo('Get all company carts');
        $cid = $this->haveCompany();
        $this->haveCart(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleCart()
    {
        $this->I->wantTo('Show cart information');
        $companyId = $this->haveCompany();
        $cartId = $this->haveCart(['company_id' => $companyId]);
        $this->sendGET($this->endpoint . '/' . $cartId);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function create()
    {
        $this->I->wantTo('Create new cart');
        $product1 = $this->haveProduct(['new_price' => 5.50]);
        $product2 = $this->haveProduct(['new_price' => 10.63]);
        $this->sendPOST(
            $this->endpoint,
            [
                'content' => [
                    'products' => [
                        [
                            'id' => $product1,
                            'qty' => 2
                        ],
                        [
                            'id' => $product2,
                            'qty' => 2
                        ],
                    ]
                ],
                'order_id' => $this->haveOrder(),
                'company_id' => $this->haveCompany(),
                'qty' => 1,
            ]
        );
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function update()
    {
        $this->I->wantTo('Update cart');
        $companyId = $this->haveCompany();
        $cartId = $this->haveCart(['company_id' => $companyId]);
        $product1 = $this->haveProduct(['new_price' => 5.50]);
        $product2 = $this->haveProduct(['new_price' => 10.63]);
        $this->sendPUT(
            $this->endpoint . '/' . $cartId . '/',
            [
                'content' => [
                    'products' => [
                        [
                            'id' => $product1,
                            'qty' => 2
                        ],
                        [
                            'id' => $product2,
                            'qty' => 2
                        ],
                    ]
                ],
                'order_id' => $this->haveOrder(),
                'company_id' => $this->haveCompany(),
                'qty' => 1,
            ]
        );
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('cart sort');
        $cid = $this->haveCompany();
        $orderId1 = $this->haveOrder();
        $orderId2 = $this->haveOrder();
        for ($i = 0; $i < 30; $i++) {
            $orderId = ($i%2 == 0) ? $orderId1 : $orderId2;
            $this->haveCart(
                [
                    'order_id' => $orderId,
                    'company_id' => $cid,
                    'qty' => $i,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['order_id'=>$orderId1]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/carts');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['qty'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/carts');

    }

    public function delete()
    {
        $this->I->wantTo('test delete cart');
        $companyId = $this->haveCompany();
        $cartId = $this->haveCart(['company_id' => $companyId]);
        $this->sendDELETE($this->endpoint.'/'.$cartId);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$cartCept = new CartResourceCept($apiTester);

$cartCept->getCompanyCarts();
$cartCept->getSingleCart();
$cartCept->create();
$cartCept->update();
$cartCept->delete();
$cartCept->testSort();
