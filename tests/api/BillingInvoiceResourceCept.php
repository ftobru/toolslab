<?php
use Faker\Factory as Faker;

/**
 * Class BillingAccountResourceCept
 */
class BillingInvoiceResourceCept extends ApiTestCase
{

    /** @var  string */
    protected $endpoint = '/api/v1/billing/invoice';

    public function getInvoices()
    {
        $I = $this->I;
        $I->wantTo('Get billing account');
        $this->sendGET($this->endpoint);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}
$apiTester = new ApiTester($scenario);
/** @var BillingInvoiceResourceCept $billing */
$billing = new BillingInvoiceResourceCept($apiTester);
$billing->getInvoices();
