<?php 

use Faker\Factory as Faker;

/**
 * Class OtherCompanyResource
 */
class OtherCompanyResource extends ApiTestCase
{
    /** @var  string */
    protected $endpoint = '/api/v1/saas/otherCompany';

    public function getAllCompany()
    {
        $this->I->wantTo('Get all company');
        $this->sendAjaxGetRequest($this->endpoint);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseJsonMatchesJsonPath('company');
    }
}

$tester = new ApiTester($scenario);
$otherCept = new OtherCompanyResource($tester);
$otherCept->getAllCompany();
