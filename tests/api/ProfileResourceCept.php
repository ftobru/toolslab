<?php 

use Faker\Factory as Faker;

/**
 * Class ProfileResourceCept
 */
class ProfileResourceCept extends ApiTestCase
{

    /** @var   */
    protected $endpoint = '/api/v1/saas/profile';

    public function show()
    {
        $this->I->wantTo('Show profile');
        $this->sendAjaxGetRequest($this->endpoint);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function edit()
    {
        $this->I->wantTo('Edit profile');
        $this->sendAjaxPostRequest($this->endpoint, [
            'name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'patronymic' => $this->faker->name,
            'password' => $this->faker->password(),
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->email
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiCept = new ApiTester($scenario);
$profileCept = new ProfileResourceCept($apiCept);
$profileCept->show();
$profileCept->edit();
