<?php

/**
 * Class PasswordResourceCept
 */
class PasswordResourceCept extends ApiTestCase
{

    public function changePassword()
    {
        $this->I->wantTo('Change user password');

        $this->haveUser([
            'password' => bcrypt('oldPassword'),
            'remember_token' => 'someToken',
        ]);

        $this->sendPOST('/api/v1/auth/password', [
            'password' => bcrypt('newPassword'),
            'remember_token' => 'someToken',
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }
}

$I = new ApiTester($scenario);
$passwordCept = new PasswordResourceCept($I);
$passwordCept->changePassword();
