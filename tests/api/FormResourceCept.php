<?php

use Faker\Factory as Faker;

/**
 * Class FormResourceCept
 */
class FormResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/saas/form';

    // tests
    /**
     * @test
     * @param ApiTester $I
     */
    public function getAllForms()
    {
        $this->I->wantTo('Get all forms');
        $id1 = $this->haveUser($this->I);
        $this->sendGET('/api/v1/saas/form');
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleForm()
    {
        $this->I->wantTo('Show form information');
        $id = $this->haveForm();
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function createForm()
    {
        $faker = $this->faker;
        $this->I->wantTo('Create Form');
        $companyID = $this->haveCompany();
        $this->sendPost($this->endpoint, [
            'en_type'       => $faker->numberBetween(1, 2),
            'form_type'     => $faker->numberBetween(1, 10),
            'placeholder'   => $faker->name,
            'validator'     => ['dates' => 'aaa', 'count' => 1],
            'label'         => $faker->name,
            'company_id'    => $companyID,
            'default_value' => ['dates' => 'aaa', 'count' => 1]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function updateForm()
    {
        $faker = $this->faker;
        $this->I->wantTo('Update Form');
        $id = $this->haveForm();
        $companyID = $this->haveCompany();

        $this->sendPUT($this->endpoint . '/' . $id . '/', [
            'en_type'       => $faker->numberBetween(1, 2),
            'form_type'     => $faker->numberBetween(1, 10),
            'placeholder'   => $faker->name,
            'validator'     => ['dates' => 'bbb', 'count' => 1],
            'label'         => $faker->name,
            'company_id'    => $companyID,
            'default_value' => ['dates' => 'ccc', 'count' => 2]
        ]);

        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getByEnType()
    {
        $cid1 = $this->haveForm(['en_type' => 1]);
        $cid2 = $this->haveForm(['en_type' => 1]);
        $this->sendGET($this->endpoint, ['en_type' => 1]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('Test sort forms');
        for ($i = 0; $i < 30; $i++) {
            $this->haveForm(['form_type' => 1, 'en_type' =>1]);
        }
        for ($i = 0; $i < 30; $i++) {
            $this->haveForm(['form_type' => 2, 'en_type' =>2]);
        }

        $this->sendGET($this->endpoint, [
            'en_type'       => 1,
            'metaData' => [
                'filter' => ['form_type'=>'1']
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/forms');

        $this->sendGET($this->endpoint, [
            'en_type'       => 1,
            'metaData' => [
                'sort' => ['form_type'=>'desc'],
            ]
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/forms');

        $this->sendGET($this->endpoint, [
            'en_type'       => 1,
            'metaData' => [
                'paginate' => ['perPage'=>25],
            ],
            'page' => 1
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/forms');
    }
}

$apiTester = new ApiTester($scenario);
$formCept = new FormResourceCept($apiTester);

$formCept->getSingleForm();
$formCept->createForm();
$formCept->updateForm();
$formCept->getByEnType();
$formCept->testSort();
