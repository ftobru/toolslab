<?php 
use Faker\Factory as Faker;

/**
 * Class BillingAccountResourceCept
 */
class BillingAccountResourceCept extends ApiTestCase
{

    /** @var  string */
    protected $endpoint = '/api/v1/billing/account';

    public function getAccount()
    {
        $I = $this->I;
        $I->wantTo('Get billing account');
        $this->sendGET($this->endpoint);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
    }
}
$apiTester = new ApiTester($scenario);
/** @var BillingAccountResourceCept $billing */
$billing = new BillingAccountResourceCept($apiTester);
$billing->getAccount();
