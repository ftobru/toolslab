<?php 
use Faker\Factory as Faker;

class UserResourceCest extends ApiTestCase
{
    /** @var string  */
    protected $endpoint = '/api/v1/saas/user';

    public function getAllUsers()
    {
        $id1 = $this->haveUser();
        $this->I->wantTo('Get all users');
        $this->sendGET($this->endpoint);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseContainsJson(['id' => $id1]);
    }

    public function createUser()
    {
        $this->I->wantTo('Create User');
        $company = $this->haveCompany();
        $role = $this->haveRole();

        $this->sendAjaxPostRequest($this->endpoint, [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password(),
            'phone' => $this->faker->phoneNumber,
            'roles' => [$role],
            'companies' => $company,
        ]);

        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function getSingleUser()
    {
        $this->I->wantTo('Show user information');
        $id = $this->haveUser();
        $this->sendGET($this->endpoint . '/' . $id);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }


    public function updateUser()
    {
        $faker = $this->faker;
        $this->I->wantTo('Update user');

        $id = $this->haveUser();

        $this->sendPUT($this->endpoint . '/' . $id . '/', [
            'name' => $faker->name,
            'email' => $faker->email,
            'phone' => $faker->phoneNumber,
            'password' => bcrypt('test'),
        ]);

        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$userCept = new UserResourceCest($apiTester);
$userCept->getAllUsers();
$userCept->createUser($apiTester);
$userCept->getSingleUser($apiTester);
