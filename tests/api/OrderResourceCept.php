<?php

/**
 * Class OrderResourceCept
 */
class OrderResourceCept extends ApiTestCase
{
    /** @var string */
    protected $endpoint = '/api/v1/crm/order';

    // tests

    public function getCompanyOrders()
    {
        $this->I->wantTo('Get all company orders');
        $cid = $this->haveCompany();
        $this->haveOrder(['company_id' => $cid]);
        $this->sendGET($this->endpoint, ['company_id' => $cid]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function getSingleOrder()
    {
        $this->I->wantTo('Show order information');
        $cid = $this->haveCompany();
        $tid = $this->haveOrder(['company_id' => $cid]);
        $this->sendGET($this->endpoint . '/' . $tid);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function createOrder()
    {
        $this->I->wantTo('Create new order');
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'client_id' => $this->haveClient(),
            'status' => 1,
            'company_id' => $this->haveCompany(),
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
    }

    public function updateOrder()
    {
        $this->I->wantTo('Update order');
        $id = $this->haveOrder();
        $this->sendPUT($this->endpoint . '/' . $id . '/', [
            'tags' => ['tag1, tag2'],
            'client_id' => $this->haveClient(),
            'status' => 1,
            'company_id' => $this->haveCompany(),
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testSort()
    {
        $this->I->wantTo('order sort');
        $cid = $this->haveCompany();
        for ($i = 0; $i < 30; $i++) {
            $this->haveOrder(
                [
                    'company_id' => $cid,
                    'status' => $i,
                ]
            );
        }

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'filter' => ['status'=>11]
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/orders');

        $this->sendGET($this->endpoint, [
            'company_id' => $cid,
            'metaData' => [
                'sort' => ['status'=>'desc']
            ]
        ]);
        $this->I->seeResponseCodeIs(200);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseJsonMatchesXpath('/orders');

    }

    public function deleteOrder()
    {
        $this->I->wantTo('test delete order');
        $orderId = $this->haveOrder();
        $this->sendDELETE($this->endpoint.'/'.$orderId);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }

    public function testTagFilter()
    {
        $companyId = $this->haveCompany();
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag1, tag2'],
            'client_id' => $this->haveClient(),
            'status' => 1,
            'company_id' => $this->haveCompany(),
        ]);
        $this->sendPOST($this->endpoint, [
            'tags' => ['tag2, tag3'],
            'client_id' => $this->haveClient(),
            'status' => 1,
            'company_id' => $this->haveCompany(),
        ]);
        $this->I->wantTo('Test tag filter for clients');
        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['withAnyTag' => ['tag1']],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);

        $this->sendGET($this->endpoint, [
            'company_id' => $companyId,
            'metaData' => [
                'filter' => ['withAllTags' => ['tag2', 'tag3']],
            ]
        ]);
        $this->I->seeResponseIsJson();
        $this->I->seeResponseCodeIs(200);
    }
}

$apiTester = new ApiTester($scenario);
$orderCept = new OrderResourceCept($apiTester);

$orderCept->getCompanyOrders();
$orderCept->getSingleOrder();
$orderCept->createOrder();
$orderCept->updateOrder();
$orderCept->deleteOrder();
$orderCept->testSort();
$orderCept->testTagFilter();
