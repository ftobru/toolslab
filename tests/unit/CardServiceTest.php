<?php


use Crm\Models\Firm;
use Crm\References\ClientReference;
use Crm\References\FirmReference;

class CardServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  \Faker\Generator */
    protected $faker;

    /** @var  UnitTestHelper */
    protected $helper;

    /** @var  \Crm\Services\CardService */
    protected $cardService;

    protected $user;
    protected $company;

    protected function _before()
    {
        $this->cardService = App::make('CardService');
        $this->helper = new UnitTestHelper($this->tester);
        $this->faker = Faker\Factory::create();
        $this->user = $this->helper->haveUser();
        $this->company = $this->helper->haveCompany(['user_id' => $this->user]);
    }

    protected function _after()
    {
    }

    // tests
    public function testAttachClientToFirm()
    {
        $this->assertTrue($this->cardService->attachClientToFirm(
            $this->helper->haveContact(['company_id' => $this->company]),
            $this->helper->haveFirm(['company_id' => $this->company])
        ));

    }

    public function testAttachFirmToClients()
    {
        $this->assertInstanceOf('\Illuminate\Support\Collection', $this->cardService->attachFirmToClients(
            Firm::find($this->helper->haveFirm(['company_id' => $this->company])),
            [
                $this->helper->haveContact(['company_id' => $this->company]),
                $this->helper->haveContact(['company_id' => $this->company]),
                $this->helper->haveContact(['company_id' => $this->company])
            ]
        ));

    }

    public function testGetFirmById()
    {
        $this->assertInstanceOf('\Crm\Models\Firm', $this->cardService->getFirmById(
            $this->helper->haveFirm(['company_id' => $this->company])
        ));
    }

    public function testGetClientById()
    {
        $this->assertInstanceOf('\Crm\Models\Client', $this->cardService->getClientById(
            $this->helper->haveContact(['company_id' => $this->company])
        ));
    }

    public function testCreateFirm()
    {
        $this->assertInstanceOf('\Crm\Models\Firm', $this->cardService->createFirm([
            'name' => $this->faker->name,
            'status' => FirmReference::STATUS_ACTIVE,
            'description' => $this->faker->text(),
            'company_id' => $this->company
        ]));
    }

    public function testCreateClient()
    {
        $this->assertInstanceOf('\Crm\Models\Client', $this->cardService->createClient([
            'status' => ClientReference::STATUS_ACTIVE,
            'type' => ClientReference::TYPE_NORMAL,
            'company_id' => $this->company
        ]));
    }

    public function testGetTasksByType()
    {
        foreach ($this->cardService->types() as $type) {
            if ($type === 'client') {
                $id = $this->helper->haveContact(['company_id' => $this->company]);
            } elseif ($type === 'firm') {
                $id = $this->helper->haveFirm(['company_id' => $this->company]);
            } else {
                $id = rand(1, 100);
            }

            $this->assertInstanceOf(
                '\Illuminate\Support\Collection',
                $this->cardService->getTasksByType($type, $id)
            );
        }
    }



    public function testGetOrderByType()
    {
        foreach ($this->cardService->types() as $type) {
            if ($type === 'client') {
                $id = $this->helper->haveContact(['company_id' => $this->company]);
            } elseif ($type === 'firm') {
                $id = $this->helper->haveFirm(['company_id' => $this->company]);
            } else {
                $id = rand(1, 100);
            }

            $this->assertInstanceOf(
                '\Illuminate\Support\Collection',
                $this->cardService->getOrderByType($type, $id)
            );
        }
    }



}