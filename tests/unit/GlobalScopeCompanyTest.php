<?php


class GlobalScopeCompanyTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testOnToMany()
    {
        $users = \Saas\Models\User::all();
        $this->assertInstanceOf('\Illuminate\Database\Eloquent\Collection', $users);


    }

    public function testBelongTo()
    {
        $orders = \Crm\Models\Order::all();
        $this->assertInstanceOf('\Illuminate\Database\Eloquent\Collection', $orders);
    }




}