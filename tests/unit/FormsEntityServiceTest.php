<?php


/**
 * Class FormsEntityServiceTest
 */
class FormsEntityServiceTest extends \Codeception\TestCase\Test
{
    /** @var \UnitTester */
    protected $tester;

    /** @var  \Faker\Generator */
    protected $faker;

    /** @var \App\Services\FormsEntityService $formsEntityService */
    protected $formsEntityService;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
        $this->formsEntityService  = App::make('FormsEntityService');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Test delete() method.
     *
     */
    public function testDelete()
    {
        $formEntity = $this->createTestFormEntity();
        $count = $this->formsEntityService->delete($formEntity->id);
        $this->assertTrue($count > 0, "$count form entities were deleted.");
    }

    /**
     * Test create() method.
     *
     */
    public function testCreate()
    {
        $formEntity = $this->createTestFormEntity();
        $this->assertInstanceOf('\App\FormsEntity', $formEntity);
    }

    /**
     * Test update() method.
     *
     */
    public function testUpdate()
    {
        /** @var \App\FormsEntity $formEntity */
        $formEntity = $this->createTestFormEntity();

        $formEntityID = $formEntity->id;
        $newData = $this->generateFakeFormEntityData();
        $this->formsEntityService->update($formEntityID, $newData);
        $updatedFormEntity = $this->formsEntityService->findById($formEntityID);

        $this->assertJsonStringEqualsJsonString($updatedFormEntity->meaning, json_encode($newData['meaning']));
        $this->assertTrue(
            $updatedFormEntity->id == $formEntityID
            && $updatedFormEntity->form_id == $newData['form_id']
            && $updatedFormEntity->company_id == $newData['company_id']
            && $updatedFormEntity->en_id == $newData['en_id']
        );
    }

    /**
     * Test getAllFieldsByCompanyId() method.
     *
     */
    public function testGetFieldsByCompanyId()
    {
        $formEntity = $this->createTestFormEntity();
        $formCompanyID = $formEntity->company_id;
        $this->assertInstanceOf('\App\FormsEntity', $formEntity);
        $collection = $this->formsEntityService->getFieldsByCompanyId($formCompanyID);
        $this->assertTrue($collection instanceof \Illuminate\Support\Collection);
    }

    /**
     * Test findById() method.
     *
     */
    public function testFindById()
    {
        $formEntity = $this->createTestFormEntity();
        $formEntityID = $formEntity->id;
        $result = $this->formsEntityService->findById($formEntityID);
        $this->assertTrue($result instanceof \App\FormsEntity);
    }

    // Helpers.

    /**
     * @return \App\FormsEntity
     */
    private function createTestFormEntity()
    {
        return $this->formsEntityService->create($this->generateFakeFormEntityData());
    }

    /**
     * @return array
     */
    private function generateFakeFormEntityData()
    {
        $formsService = App::make('FormsService');
        $form = $formsService->create($this->generateFakeFormData());
        $companyID = $this->haveCompany();

        $data = [
            'meaning' => ['test_key' => 'test_val'],
            'form_id' => $form->id,
            'company_id' => $companyID,
            'en_id' => $this->faker->numberBetween(1, 2),
        ];

        return $data;
    }

    /**
     * @return array
     */
    private function generateFakeFormData()
    {
        $companyID = $this->haveCompany();

        $data = [
            'en_type' => $this->faker->numberBetween(1, 2),
            'form_type' => $this->faker->numberBetween(1, 10),
            'placeholder' => $this->faker->numberBetween(1, 10),
            'validator' => json_encode(['test_key'=>'test_value']),
            'label' => $this->faker->name,
            'company_id'=> $companyID,
        ];

        return $data;
    }

    /**
     * @param array $data
     * @return mixed
     * @internal param ApiTester $I
     */
    private function haveCompany($data = [])
    {
        $data = array_merge([
            'name' => 'test',
            'display_name' => 'Тестовая компания',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $this->tester->haveRecord('companies', $data);
    }
}