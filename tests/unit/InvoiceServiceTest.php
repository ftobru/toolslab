<?php


use Billing\Services\Rates\CrmUsersLiteRate;

class InvoiceServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  \Billing\Services\InvoiceService */
    protected $service;
    /** @var  \Billing\Services\RateService */
    protected $rateService;

    /** @var  UnitTestHelper */
    protected $helper;

    protected function _before()
    {
        $this->service = App::make('InvoiceService');
        $this->rateService = App::make('RateService');
        $this->helper = new UnitTestHelper($this->tester);
    }

    protected function _after()
    {
    }


    public function testBill()
    {
        $user = $this->helper->haveUser();
        $company = $this->helper->haveCompany(['user_id' => $user]);

        $rate  = $this->rateService->connectRate($user, $company, CrmUsersLiteRate::IDENTIFIER);

        $this->assertInstanceOf('Billing\Models\BillingInvoice', $invoice = $this->service->bill($rate));
        return $invoice;
    }


    /**
     * @depends testBill
     */
    public function testPay($payload)
    {
        $user = $this->helper->haveUser();
        $company = $this->helper->haveCompany(['user_id' => $user]);
        $account = $this->helper->haveBillingAccount();
        $this->helper->attachUserToAccount($user, $account);

        $rate  = $this->rateService->connectRate($user, $company, CrmUsersLiteRate::IDENTIFIER);

        $this->assertInstanceOf('Billing\Models\BillingInvoice', $invoice = $this->service->bill($rate));

        $this->assertInstanceOf('Billing\Models\BillingInvoice', $this->service->pay($invoice));
    }


}