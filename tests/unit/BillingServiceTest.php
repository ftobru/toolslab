<?php


class BillingServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  Faker\Generator */
    protected $faker;
    /** @var  \Saas\Services\CompanyService */
    protected $companyService;

    protected function _before()
    {
        $this->faker = Faker\Factory::create();
        $this->companyService = App::make('CompanyService');
    }

    protected function _after()
    {
    }


    /**
     * Fake company data generator/
     *
     * @return array
     */
    private function generateFakeCompanyData($userId)
    {
        $data = [
            'name' => $this->faker->company,
            'display_name' => $this->faker->company,
            'user_id' => $userId,
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ];

        return $data;
    }


    /**
     * @return int userID
     */
    private function createTestUser()
    {
        $data = $this->generateFakeUserData();
        $data = array_merge([
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);

        /** @var \Saas\Models\User $user */
        $user = $this->tester->haveRecord('users', $data);
        return $user;
    }

    /**
     * @param bool $unset Unset user Roles and Companies from user.
     * @return array
     */
    private function generateFakeUserData()
    {

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
            'phone' => $this->faker->phoneNumber,
        ];
        return $data;
    }

    /**
     * @param UnitTester $tester
     * @param array $data
     * @return mixed
     */
    private function haveCompany(UnitTester $tester, $data = [])
    {
        $data = array_merge([
            'name' => 'test',
            'display_name' => 'Тестовая компания',

            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $tester->haveRecord('companies', $data);
    }



    // tests
    /**
     * @return array
     * @throws Exception
     */
    public function testControlCompany()
    {

        $userId = $this->createTestUser();

        /** @var \Saas\Repositories\Eloquent\CompanyEloquentRepository $repository */
        $repository = App::make('Saas\Repositories\Interfaces\CompanyRepositoryInterface');
        $attributes = [
            'name' => $this->faker->name,
            'display_name' => $this->faker->text(),
            'user_id' => $userId
        ];
        $company = BillingControl::repository($repository)->create($attributes);
        $this->assertInstanceOf('Saas\Models\Company', $company);

        return $company;
    }
}