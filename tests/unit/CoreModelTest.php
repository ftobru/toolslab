<?php

use Crm\Models\Product;

/**
 * Class CoreModelTest
 */
class CoreModelTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var Faker\Generator
     */
    protected $faker;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
    }

    protected function _after()
    {
    }

    // tests
    /**
     *
     */
    public function testInsertCompanyIdAttribute()
    {
        $company = $this->tester->haveRecord('companies', [
            'name' => 'test',
            'display_name' => 'Testing company',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        Input::merge(['company_alias' => 'test']);

        $product = new Product();
        $product->name = $this->faker->name;
        $product->article = $this->faker->name;
        $product->new_price = $this->faker->randomFloat();
        $product->old_price = $this->faker->randomFloat();
        $product->description = $this->faker->text();
        $product->profit = $this->faker->randomFloat();
        $this->assertTrue($product->save());

        $product = Product::find($product->id);
        $this->assertEquals($product->company_id, $company);
    }

}