<?php

use Codeception\TestCase\Test;

/**
 * Class CompanyServiceTest
 */
class CompanyServiceTest extends Test
{
    /** @var \UnitTester*/
    protected $tester;

    /** @var  \Faker\Generator */
    protected $faker;

    /** @var \Saas\Services\CompanyService $companyService */
    protected $companyService;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
        $this->companyService = App::make('CompanyService');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Test create() method.
     *
     */
    public function testCreate()
    {
        $company = $this->createTestCompany();
        $this->assertInstanceOf('\Saas\Models\Company', $company);
        $this->assertTrue($company->id > 0);
    }

    /**
     * Test generateCompanyName() method.
     *
     */
    public function testGenerateCompanyName()
    {
        $company = $this->createTestCompany();
        $generatedName = $this->companyService->generateCompanyName($company->id);
        $this->assertTrue(is_string($generatedName));
        $this->assertTrue('c' . $company->id == $generatedName);
    }

    /**
     * Test delete() method.
     *
     */
    public function testDelete()
    {
        /** @var \Saas\Models\Company $company */
        $company = $this->createTestCompany();
        $count = $this->companyService->delete($company->id);
        $this->assertTrue($count > 0, "$count companies were deleted.");
    }

    /**
     * Test registry() method.
     *
     */
    public function testRegistry()
    {
        $allCompanies = $this->companyService->registry();
        $this->assertInstanceOf('\Illuminate\Support\Collection', $allCompanies);
    }

    /**
     * Test update() method.
     *
     */
    public function testUpdate()
    {
        $company = $this->createTestCompany();
        $companyID = $company->id;

        $newData = $this->generateFakeCompanyData();
        $this->companyService->update($companyID, $newData);
        $updatedCompany = $this->companyService->show($companyID);

        $this->assertTrue(
            $updatedCompany->id == $companyID
            && $updatedCompany->created_at == $newData['created_at']
            && $updatedCompany->updated_at == $newData['updated_at']
            && $updatedCompany->display_name == $newData['display_name']
            && $updatedCompany->name == $newData['name']
        );

    }

    /**
     * Test activeRepository() method.
     *
     */
    public function testActiveRepository()
    {
        $metadata = [
            'sort' => [],
            'filter' => [
                'name',
            ],
        ];

        // TODO: make additional check here.

        $companies = $this->companyService->activeRepository($metadata);
        $this->assertInstanceOf('\Illuminate\Support\Collection', $companies);

    }

    /**
     * Test attachUserToCompany() method.
     *
     */
    public function testAttachUserToCompany()
    {
        $company = $this->createTestCompany();
        $userID = $this->createTestUser();
        $companyID = $company->id;
        $this->companyService->attachUserToCompany($companyID, $userID);
        $this->assertTrue($company->users()->find($userID) != null);
    }

    // helpers

    /**
     * @return \Saas\Models\Company
     */
    private function createTestCompany()
    {
        /** @var \Saas\Models\Company $company */
        $company = $this->companyService->create($this->generateFakeCompanyData());
        return $company;
    }

    /**
     * Fake company data generator/
     *
     * @return array
     */
    private function generateFakeCompanyData()
    {
        $data = [
            'name' => $this->faker->company,
            'display_name' => $this->faker->company,
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ];

        return $data;
    }

    /**
     * @return int userID
     */
    private function createTestUser()
    {
        $data = $this->generateFakeUserData(true);
        $data = array_merge([
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);

        /** @var \Saas\Models\User $user */
        $user = $this->tester->haveRecord('users', $data);
        return $user;
    }

    /**
     * @param bool $unset Unset user Roles and Companies from user.
     * @return array
     */
    private function generateFakeUserData($unset = false)
    {
        $company = $this->haveCompany($this->tester);
        $role = $this->haveRole($this->tester);

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
            'phone' => $this->faker->phoneNumber,
        ];
        if (!$unset) {
            $data['roles'] = [$role];
            $data['companies'] = [$company];
        }
        return $data;
    }

    /**
     * @param UnitTester $tester
     * @param array $data
     * @return mixed
     */
    private function haveRole(UnitTester $tester, $data = [])
    {
        $data = array_merge([
            'name' => 'Administrator',
            'display_name' => 'Администратор',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $tester->haveRecord('roles', $data);
    }

    /**
     * @param UnitTester $tester
     * @param array $data
     * @return mixed
     */
    private function haveCompany(UnitTester $tester, $data = [])
    {
        $data = array_merge([
            'name' => 'test',
            'display_name' => 'Тестовая компания',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $tester->haveRecord('companies', $data);
    }

}