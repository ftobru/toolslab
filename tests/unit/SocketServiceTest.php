<?php


use Saas\References\SocketReference;

class SocketServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;


    /** @var  \Saas\Services\SocketService */
    protected $service;

    /** @var  Faker\Generator */
    protected $faker;

    /** @var  UnitTestHelper */
    protected $helper;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
        $this->service = App::make('SocketService');
        $this->helper = new UnitTestHelper($this->tester);
    }

    protected function _after()
    {
    }


    public function testPushMessage()
    {
        $user = $this->helper->haveUser([
            'id' => 631,
            'password' => '$2y$10$ziOIgx48nj4wjtNeAOQw8.ftoGvVkXfMfsQknIw4BxrH.atqmwr3i'
        ]);
        $this->assertTrue(
            is_integer($this->service->push(SocketReference::CHANNEL_MESSAGE_PUSH, 631, $this->faker->text()))
        );
    }

}