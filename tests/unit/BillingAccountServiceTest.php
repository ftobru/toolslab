<?php


use Illuminate\Support\Collection;

class BillingAccountServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  UnitTestHelper */
    protected $helper;

    /** @var  \Billing\Services\AccountService */
    protected $service;

    const SUMM = 100;

    protected function _before()
    {
        $this->helper = new UnitTestHelper($this->tester);
        $this->service = App::make('AccountService');
    }

    protected function _after()
    {
    }

    public function testRecharge()
    {
        $user = $this->helper->haveUser();
        $billingAccount = $this->helper->haveBillingAccount();
        $this->helper->attachUserToAccount($user, $billingAccount);

        $this->assertInstanceOf('Billing\Models\BillingAccount', $this->service->recharge(self::SUMM, $user));

        return $user;
    }

    /**
     * @param $payload
     * @depends testRecharge
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     * @expectedException \Billing\Exceptions\AccountNotFoundException
     */
    public function testRemovalAccountNotFound($payload)
    {

        $this->assertInstanceOf('Billing\Models\BillingAccount', $this->service->removal(self::SUMM, $payload));
        return $payload;
    }

    /**
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     * @expectedException \Billing\Exceptions\InsufficientFundsException
     */
    public function testRemovalInsufficientFunds()
    {
        $account = $this->helper->haveBillingAccount();
        $payload = $this->helper->haveUser();
        $this->helper->attachUserToAccount($payload, $account);
        $this->assertInstanceOf('Billing\Models\BillingAccount', $this->service->removal(100000, $payload));

        return $payload;
    }

    /**
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     * @expectedException \Billing\Exceptions\AmountCanNotBeNegativeException
     */
    public function testRemovalAmountCanNotBeNegative()
    {
        $account = $this->helper->haveBillingAccount(['amount' => 100]);
        $payload = $this->helper->haveUser();
        $this->helper->attachUserToAccount($payload, $account);
        $this->service->recharge(self::SUMM, $payload);
        $this->assertInstanceOf('Billing\Models\BillingAccount', $this->service->removal(-1, $payload));

        return $payload;
    }


    /**
     * @throws \Billing\Exceptions\AccountNotFoundException
     * @throws \Billing\Exceptions\AmountCanNotBeNegativeException
     * @throws \Billing\Exceptions\InsufficientFundsException
     */
    public function testRemoval()
    {
        $account = $this->helper->haveBillingAccount(['amount' => 100]);
        $payload = $this->helper->haveUser();
        $this->helper->attachUserToAccount($payload, $account);
        $this->service->recharge(self::SUMM + 1, $payload);

        $this->assertInstanceOf('Billing\Models\BillingAccount', $this->service->removal(self::SUMM, $payload));

        return $payload;
    }







}