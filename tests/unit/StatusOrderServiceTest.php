<?php


use Crm\Models\Order;

class StatusOrderServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  \Crm\Services\OrderStatusService */
    protected $service;

    /** @var  Faker\Generator */
    protected $faker;

    protected function _before()
    {
        $this->service = App::make('Crm\Services\OrderStatusService');
        $this->faker = Faker\Factory::create('ru_RU');
    }

    protected function _after()
    {
    }

    private function createAvalibleStatuses()
    {
        $company = $this->tester->haveRecord('companies', [
            'name' => 'test',
            'display_name' => 'Testing company',
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);

        return $this->tester->haveRecord('statuses_orders', [
            'company_id' => $company,
            'statuses' => json_encode([]),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
    }


    public function testAdd()
    {
        $statusesId = $this->createAvalibleStatuses();
        $this->assertTrue(is_integer($statusesId));
        $this->service->addStatus($this->faker->text());
        $this->tester->seeRecord('statuses_orders', ['id' => $statusesId]);
        $statuses = $this->service->getStatuses();
        $this->assertArrayHasKey(1000, $statuses);
    }


    public function testDelete()
    {
        $statusId = 1000;
        $this->createAvalibleStatuses();
        $this->assertTrue($this->service->deleteStatuesByStatusId($statusId));
        $statuses = $this->service->getStatuses();
        $this->assertArrayNotHasKey(1000, $statuses);
    }

}