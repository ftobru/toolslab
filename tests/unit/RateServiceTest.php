<?php


use Billing\Reference\BillingAccountReference;
use Billing\Services\Rates\CrmAddUserRate;
use Billing\Services\Rates\CrmUsersLiteRate;
use Billing\Services\RateService;
use Illuminate\Support\Str;
use Saas\Models\Company;
use Saas\Models\User;
use Billing\Models\BillingAccount;
use Saas\References\UserReference;

class RateServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  RateService */
    protected $rateService;

    /** @var  User */
    protected $user;

    /** @var  Faker\Generator */
    protected $faker;

    /** @var  Company */
    protected $company;

    /** @var  BillingAccount */
    protected $billingAccount;


    /**
     *
     */
    protected function _before()
    {
        $this->rateService = App::make('RateService');
        $this->user = new User();
        $this->company = new Company();
        $this->billingAccount = new BillingAccount();
        $this->faker = Faker\Factory::create('ru_RU');
    }

    protected function _after()
    {

    }

    /**
     * @return mixed
     */
    private function createUser()
    {
        /** @var User $user */
        $user = new $this->user;
        $user->email = $this->faker->email;
        $user->password = bcrypt($this->faker->password());
        $user->status = UserReference::STATUS_ACTIVE;
        $user->name = $this->faker->name;
        $user->phone = $this->faker->phoneNumber;
        $user->registration_token = Str::random();
        $user->save();
        $user->billingAccount()->attach($this->createBillingAccount());
        return $user;
    }

    private function createBillingAccount($data = [])
    {
        $data = array_merge([
            'amount' => 0,
            'count_operations' => 0,
            'status' => BillingAccountReference::STATUS_ACTIVE,
            'type' => array_get(BillingAccountReference::types(), 0, 0),
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return $this->tester->haveRecord('billing_accounts', $data);
    }


    private function createCompany(User $user)
    {
        $company = $this->company;
        $company->name = $this->faker->name;
        $company->display_name = $this->faker->text();
        $company->user_id = $user->id;
        $company->save();
        $company->users()->attach($user->id);
        return $company;
    }

    public function testCheckRateIdentifier()
    {
        $this->tester->wantTo('check identifier rate');

        $this->assertTrue($this->rateService->checkRateIdentifier(CrmUsersLiteRate::IDENTIFIER));

        $this->assertInstanceOf(
            'Billing\Services\Rates\Interfaces\RateInterface',
            $this->rateService->getRateByIdentifier(CrmUsersLiteRate::IDENTIFIER)
        );
        $this->assertFalse($this->rateService->checkRateIdentifier($this->faker->numberBetween(1000, 2000)));
//
//        $this->assert(
//            'Billing\Services\Rates\Interfaces\RateInterface',
//            $this->rateService->getRateByIdentifier($this->faker->numberBetween(1000, 2000))
//        );

    }


    public function testConnectRates()
    {
        $rateId = CrmUsersLiteRate::IDENTIFIER;
        $user = $this->createUser();
        $company = $this->createCompany($user);

        $this->assertInstanceOf('Saas\Models\User', $user);
        $this->assertInstanceOf('Saas\Models\Company', $company);

        $this->assertInstanceOf(
            'Billing\Models\BillingRatesUser',
            $this->rateService->connectRate($user->id, $company->id, $rateId)
        );

    }

    private function connectionRate($identifierRate = CrmUsersLiteRate::IDENTIFIER)
    {
        $user = $this->createUser();
        $company = $this->createCompany($user);

        $this->rateService->connectRate($user->id, $company->id, $identifierRate);

        return $company;
    }

    public function testGetSummaryOfAllRatesByUserId()
    {
        $company = $this->connectionRate();

        $this->assertTrue(is_numeric($summary = $this->rateService->getSummaryOfAllRatesByUserId($company->user_id)));

        $this->assertEquals($summary, CrmUsersLiteRate::DEFAULT_PRICE);
    }

    /**
     * @throws \Billing\Exceptions\RateNotFoundException
     */
    public function testGetSummaryOfAllRatesByCompanyId()
    {
        $company = $this->connectionRate();
        $this->rateService->connectRate($company->user_id, $company->id, CrmAddUserRate::IDENTIFIER);
        $this->assertTrue(is_numeric($summary = $this->rateService->getSummaryOfAllRatesByCompanyId($company->id)));
        $this->assertEquals(
            CrmUsersLiteRate::DEFAULT_PRICE + CrmAddUserRate::DEFAULT_PRICE,
            $summary
        );
    }


    public function testCreateCompany()
    {
        $user = $this->createUser();
        $company = $this->createCompany($user);

        $this->assertTrue($this->rateService->connectDefaultRates($user->id, $company->id));

        $this->assertEquals(
            $this->rateService->getSummaryOfDefaultRates(),
            $this->rateService->getSummaryOfAllRatesByCompanyId($company->id)
        );
    }

    public function testCheckConnectionRate()
    {
        $company = $this->connectionRate();
        $this->assertTrue($this->rateService->checkConnectionRate($company->id, CrmUsersLiteRate::IDENTIFIER));
    }

    public function testGetRateGroupByRateIdentifier()
    {
        $this->assertTrue(
            is_integer($this->rateService->getGroupIdentifierByRateIdentifier(CrmUsersLiteRate::IDENTIFIER))
        );
    }

    public function testGetOddMoney()
    {
        $user = $this->createUser();
        $company = $this->createCompany($user);

        $this->rateService->connectDefaultRates($user->id, $company->id);
        $rate = $this->rateService->connectRate($user->id, $company->id, CrmAddUserRate::IDENTIFIER);
        $money = $this->rateService->getOddMoney($rate);
        $this->assertTrue(is_numeric($money));
    }
}