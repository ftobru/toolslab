<?php


class AccessServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  UnitTestHelper */
    protected $helper;

    /** @var  UserService */
    protected $service;

    protected function _before()
    {
        $this->helper = new UnitTestHelper($this->tester);
    }

    protected function _after()
    {
    }

    // tests
    public function testCanPermission()
    {
        $user = $this->helper->haveUser();
        $company = $this->helper->haveCompany(['user_id' => $user]);
        $this->assertTrue(is_bool(AccessService::canPermission('owner', $company, $user)));
    }

}