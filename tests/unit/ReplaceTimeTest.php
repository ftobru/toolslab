<?php


class ReplaceTimeTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  UnitTestHelper */
    protected $helper;

    protected function _before()
    {
        $this->helper = new UnitTestHelper($this->tester);
    }

    protected function _after()
    {
    }

    // tests
    public function testDateTime()
    {
        $userId = $this->helper->haveUser();
        $companyId = $this->helper->haveCompany(['user_id' => $userId]);

        $replaceTime = new \LandingControl\Models\ReplacementTime();
        $replaceTime->name = 'test';
        $replaceTime->tag = 'test2';
        $replaceTime->meaning = 'test3';
        $replaceTime->start_time = \Carbon\Carbon::now();
        $replaceTime->finish_time = Carbon\Carbon::now();
        $replaceTime->company_id = $companyId;
        $replaceTime->save();

        $this->assertInstanceOf('LandingControl\Models\ReplacementTime', $replaceTime);
    }

}