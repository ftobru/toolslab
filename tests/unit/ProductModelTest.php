<?php


use Crm\Models\Product;

class ProductModelTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var Faker\Generator
     */
    protected $faker;

    /** @var  \Illuminate\Database\Eloquent\Model */
    protected $model;
    /** @var  UnitTestHelper */
    protected $helper;

    /** @var  \Saas\Repositories\Eloquent\FormEloquentRepository */
    protected $customFieldRepository;

    /** @var  \Saas\Repositories\Eloquent\FormsEntityEloquentRepository */
    protected $customFieldValueRepository;

    /**
     *
     */
    protected function _before()
    {
        $this->faker = Faker\Factory::create();
        $this->model = new Product();
        $this->helper = new UnitTestHelper($this->tester);
        $this->customFieldRepository = App::make('Saas\Repositories\Interfaces\FormRepositoryInterface');
        $this->customFieldValueRepository = App::make('Saas\Repositories\Interfaces\FormsEntityRepositoryInterface');
    }

    protected  $companyId;

    protected function createCustomField()
    {
        $user = $this->helper->haveUser();
        $company = $this->helper->haveCompany(['user_id' => $user]);
        $this->companyId = $company;

        $form = $this->customFieldRepository->create([
            'en_type' => Config::get('custom-fields.en_types.' . Product::class),
            'form_type' => Config::get('custom-fields.form_types.text'),
            'default_value' => json_encode(['test_default_value']),
            'label' => 'Test Text Form',
            'placeholder' => 'test placeholder',
            'validator' => json_encode(['required']),
            'company_id' => $company,
            'name' => 'test_field'
        ]);

        return $this->customFieldValueRepository->create([
            'form_id' => $form->id,
            'en_id' => Config::get('custom-fields.en_types.' . Product::class),
            'meaning' => json_encode(['testValue']),
            'company_id' => $company,
        ]);

    }

    protected function _after()
    {
    }

    // tests
    public function testCustomField()
    {
        $form = $this->createCustomField();

        $payload = $this->helper->haveProduct(['company_id' => $form->company_id, 'test_field' => 'test']);
        /** @var Product $product */
        $product = Product::find($payload->id);

        //$this->assertObjectHasAttribute('test_field', $product = Product::find($payload->id));
        //$product->update(['test_field' => '11']);
        //dd($product = Product::find($product->id));
        //$this->assertObjectHasAttribute('test_field', $product = Product::find($product->id));
        //dd($product);

        //$this->assertEquals($product->test_field, '11');
        $this->model->all();
    }

}