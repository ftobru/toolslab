<?php


/**
 * Class FormsServiceTest
 */
class FormsServiceTest extends \Codeception\TestCase\Test
{
    /** @var \UnitTester */
    protected $tester;

    /** @var  \Faker\Generator */
    protected $faker;

    /** @var \Saas\Services\FormsService $formsService */
    protected $formsService;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
        $this->formsService = App::make('FormsService');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Test delete() method.
     *
     */
    public function testDelete()
    {
        $form = $this->createTestForm();
        $count = $this->formsService->delete($form->id);
        $this->assertTrue($count > 0, "$count users were deleted.");
    }

    /**
     * Test create() method.
     *
     */
    public function testCreate()
    {
        $form = $this->createTestForm();
        $this->assertInstanceOf('\App\Form', $form);
    }

    /**
     * Test update() method.
     *
     */
    public function testUpdate()
    {
        /** @var \Saas\Models\Form $form */
        $form = $this->createTestForm();

        $formID = $form->id;
        $newData = $this->generateFakeFormData();
        $this->formsService->update($formID, $newData);
        $updatedForm = $this->formsService->findById($formID);
        $this->assertJsonStringEqualsJsonString($updatedForm->validator, json_encode($newData['validator']));
        $this->assertTrue(
            $updatedForm->id == $formID
            && $updatedForm->en_type == $newData['en_type']
            && $updatedForm->form_type == $newData['form_type']
            && $updatedForm->placeholder == $newData['placeholder']
            && $updatedForm->label == $newData['label']
        );
    }

    /**
     * Test getAllFieldsByCompanyId() method.
     *
     */
    public function testGetAllFieldsByCompanyId()
    {
        $form = $this->createTestForm();
        $formCompanyID = $form->company_id;
        $this->assertInstanceOf('\App\Form', $form);
        $collection = $this->formsService->getAllFieldsByCompanyId($formCompanyID);
        $this->assertTrue($collection instanceof \Illuminate\Support\Collection);
    }

    /**
     * Test findByEntityType() method.
     *
     */
    public function testFindByEntityType()
    {
        $form = $this->createTestForm();
        $en_type = $form->en_type;
        $this->assertInstanceOf('\App\Form', $form);
        $collection = $this->formsService->findByEntityType($en_type);
        $this->assertTrue($collection instanceof \Illuminate\Support\Collection);
    }

    /**
     * Test findById() method.
     *
     */
    public function testFindById()
    {
        $form = $this->createTestForm();
        $formID = $form->id;
        $result = $this->formsService->findById($formID);
        $this->assertTrue($result instanceof \Saas\Models\Form);
    }

    // Helpers

    /**
     * @return mixed
     */
    private function createTestForm()
    {
        return $this->formsService->create($this->generateFakeFormData());
    }

    /**
     * @return array
     */
    private function generateFakeFormData()
    {
        $companyID = $this->haveCompany();

        $data = [
            'en_type' => $this->faker->numberBetween(1, 2),
            'form_type' => $this->faker->numberBetween(1, 10),
            'placeholder' => $this->faker->numberBetween(1, 10),
            'validator' => ["abc" => 1, "count" => 2],
            'label' => $this->faker->name,
            'company_id'=> $companyID,
        ];

        return $data;
    }

    /**
     * @param array $data
     * @return mixed
     * @internal param ApiTester $I
     */
    private function haveCompany($data = [])
    {
        $data = array_merge([
            'name' => 'test',
            'display_name' => 'Тестовая компания',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $this->tester->haveRecord('companies', $data);
    }
}
