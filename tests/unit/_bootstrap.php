<?php
// Here you can initialize variables that will be available to your tests

use Billing\Reference\BillingAccountReference;
use Carbon\Carbon;
use Crm\Models\Product;
use Crm\References\ClientReference;
use Crm\References\FirmReference;
use Crm\References\OrderReference;
use Crm\References\TaskReference;

class UnitTestHelper
{
    /** @var UnitTester  */
    protected $tester;

    /** @var  Faker\Generator */
    protected $faker;

    /** @var  \Saas\Repositories\Eloquent\UserEloquentRepository */
    protected $userRepository;

    public function __construct(UnitTester $test)
    {
        $this->tester= $test;
        $this->faker = Faker\Factory::create();

        $this->userRepository = App::make('Saas\Repositories\Interfaces\UserRepositoryInterface');
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveUser($data = [])
    {
        $data = array_merge([
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'phone' => $this->faker->phoneNumber,
            'password' => bcrypt('test'),
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime()
        ], $data);

        return $this->tester->haveRecord('users', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveCompany($data = [])
    {
        $data = array_merge([
            'name' => $this->faker->name,
            'display_name' => $this->faker->text(),
            'user_id' => $this->faker->numberBetween(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], $data);

        return $this->tester->haveRecord('companies', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveBillingAccount($data = [])
    {
        $data = array_merge([
            'amount' => 0,
            'count_operations' => 0,
            'status' => BillingAccountReference::STATUS_ACTIVE,
            'type' => BillingAccountReference::TYPE_NORMAL,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ], $data);

        return $this->tester->haveRecord('billing_accounts', $data);
    }

    /**
     * @param $userId
     * @param $billingAccountId
     */
    public function attachUserToAccount($userId, $billingAccountId)
    {
        /** @var \Saas\Models\User $user */
        $user = $this->userRepository->find($userId);
        return $user->billingAccount()->attach($billingAccountId);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveFirm($data = [])
    {
        $data = array_merge([
            'name' => $this->faker->name,
            'status' => FirmReference::STATUS_ACTIVE,
            'description' => $this->faker->text(),
            'company_id' => rand(1, 100),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], $data);

        return $this->tester->haveRecord('firms', $data);
    }

    /**
     * @param array $data
     * @return int
     */
    public function haveContact($data = [])
    {
        $data = array_merge([
            'status' => ClientReference::STATUS_ACTIVE,
            'type' => ClientReference::TYPE_NORMAL,
            'company_id' => rand(1, 100),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], $data);

        return $this->tester->haveRecord('clients', $data);
    }

    /**
     * @param array $data
     * @return int
     */
    public function haveOrder($data = [])
    {
        $data = array_merge([
            'client_id' => rand(1, 100),
            'firm_id' => rand(1, 100),
            'status' => OrderReference::STATUS_NEW,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], $data);

        return $this->tester->haveRecord('orders', $data);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function haveProduct($data = [])
    {
        $data = array_merge([
            'name' => $this->faker->name,
            'description' => $this->faker->text(),
            'article' => implode(',', $this->faker->words(20)),
            'new_price' => rand(1, 4),
            'old_price' => rand(1, 5),
            'profit' => rand(1, 3),
            'company_id' => rand(1, 10000),
            'min_price' => rand(1, 2),
            'photos' => json_encode(['test']),
            'prime_cost' => rand(1, 4),
            'created_at' => New DateTime(),
            'updated_at' => new DateTime()
        ], $data);
        return Product::create($data);
        //return $this->tester->haveRecord('products', $data);
    }

    /**
     * @param array $data
     */
    public function haveTask($data = [])
    {
        $data = array_merge([
            'client_id' => rand(1, 100),
            'firm_id' => rand(1, 100),
            'state' => TaskReference::STATE_NEW,
            'type' => TaskReference::TYPE_NORMAL,
            'date_perf' => Carbon::now(),
            'responsible_user_id' => rand(1, 100),
            'performer_user_id' => rand(1, 100),
            'comment' => $this->faker->text(),
            'company_id' => rand(1, 100),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ], $data);

        $this->tester->haveRecord('tasks', $data);
    }



}
