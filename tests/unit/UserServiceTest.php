<?php

use Codeception\TestCase\Test;

/**
 * Class UserServiceTest
 */
class UserServiceTest extends Test
{
    /** @var \UnitTester */
    protected $tester;

    /** @var  \Faker\Generator */
    protected $faker;

    /** @var \Saas\Services\UserService $userService */
    protected $userService;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
        $this->userService = App::make('UserService');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Test create() method.
     *
     */
    public function testCreate()
    {
        $user = $this->createTestUser();
        $this->assertInstanceOf('\Saas\Models\User', $user);
    }

    /**
     * Test registration() method.
     *
     */
    public function testRegistration()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->userService->registration($this->generateFakeUserData());
        $this->assertInstanceOf('\Saas\Models\User', $user['user']);
    }


    /**
     * Test changePassword() method.
     *
     */
    public function testChangePassword()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();
        $new_password = $this->faker->password;
        $user = $this->userService->changePassword($user, $new_password);
        $this->assertInstanceOf('\Saas\Models\User', $user);
        $this->assertTrue(Hash::check($new_password, $user->password));
    }

    /**
     * Test login() method.
     *
     */
    public function testLogin()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();
        $new_password = $this->faker->password;
        $user = $this->userService->changePassword($user, $new_password);
        $userID = $this->userService->login($user->email, $new_password);
        $this->assertInstanceOf('\Saas\Models\User', $user);
        $this->tester->seeRecord('users', array('id' => $userID));
    }

    /**
     * Test generateActivationCode() method.
     *
     */
    public function testGenerateActivationCode()
    {
        $code = $this->userService->generateActivationCode();
        $this->assertTrue(is_string($code));
    }

    /**
     * Test activeRepository() method.
     *
     */
    public function testActiveRepository()
    {
        $this->generateTestSortData(5);
        $metadata = [
            'sort' => [],
            'filter' => [
                'name',
            ],
        ];

        // TODO: make additional check here.

        $users = $this->userService->activeRepository($metadata);

        $this->assertTrue(is_array($users));
    }

    /**
     * Test show() method.
     *
     */
    public function testShow()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();
        $needleUser = $this->userService->findById($user->id);
        $this->assertTrue($needleUser->id == $user->id);
    }

    /**
     * Test validator() method.
     *
     */
    public function testValidator()
    {
        $validator = $this->userService->validator($this->generateFakeUserData(true));
        $this->assertInstanceOf('\Illuminate\Validation\Validator', $validator);
    }

    /**
     * Test findById() method.
     *
     */
    public function testFindById()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();
        $needleUser = $this->userService->findById($user->id);
        $this->assertTrue($needleUser->id == $user->id);
    }

    /**
     * Test delete() method.
     *
     */
    public function testDelete()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();
        $count = $this->userService->delete($user->id);
        $this->assertTrue($count > 0, "$count users were deleted.");
    }

    /**
     * Test update() method.
     *
     */
    public function testUpdate()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();

        $userID = $user->id;
        $newData = $this->generateFakeUserData(true);
        $this->userService->update($userID, $newData);
        $updatedUser = $this->userService->findById($userID);

        $this->assertTrue(
            $updatedUser->id == $userID
            && $updatedUser->email == $newData['email']
            && $updatedUser->phone == $newData['phone']
            && $updatedUser->name == $newData['name']
        );
    }

    /**
     * Test logout() method.
     *
     */
    public function testLogout()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->createTestUser();
        $new_password = $this->faker->password;
        $user = $this->userService->changePassword($user, $new_password);
        $userID = $this->userService->login($user->email, $new_password);
        $this->assertTrue($this->userService->logout());
    }

    /**
     * Test findUserByPhone() method.
     *
     */
    public function testFindUserByPhone()
    {
        $phoneToFind = $this->faker->phoneNumber;
        /** @var \Saas\Models\User $user */
        $id = $this->tester->haveRecord('users', [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
            'phone' => $phoneToFind,
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ]);

        $user = $this->userService->findUserByPhone($phoneToFind);
        $this->assertInstanceOf('\Saas\Models\User', $user);
        $this->assertTrue($user->id == $id);
    }

    /**
     * Test findUserByEmail() method.
     *
     */
    public function testFindUserByEmail()
    {
        $emailToFind = $this->faker->email;
        /** @var \Saas\Models\User $user */
        $id = $this->tester->haveRecord('users', [
            'name' => $this->faker->name,
            'email' => $emailToFind,
            'password' => $this->faker->password,
            'phone' => $this->faker->phoneNumber,
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ]);

        $user = $this->userService->findUserByEmail($emailToFind);
        $this->assertInstanceOf('\Saas\Models\User', $user);
        $this->assertTrue($user->id == $id);
    }

    /**
     * Test findUserByRememberToken() method.
     *
     */
    public function testFindUserByRememberToken()
    {
        $tokenToFind = 'someToken';
        /** @var \Saas\Models\User $user */
        $id = $this->tester->haveRecord('users', [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
            'phone' => $this->faker->phoneNumber,
            'remember_token' => $tokenToFind,
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ]);

        $user = $this->userService->findUserByRememberToken($tokenToFind);
        $this->assertInstanceOf('\Saas\Models\User', $user);
        $this->assertTrue($user->id == $id);
    }

    /**
     * Test findUserToCompany() method.
     *
     */
    public function _testFindUserToCompany()
    {
        // TODO: add test logic.
    }

    // helpers.

    /**
     * @param UnitTester $tester
     * @param array $data
     * @return mixed
     */
    private function haveRole(UnitTester $tester, $data = [])
    {
        $data = array_merge([
            'name' => 'Administrator',
            'display_name' => 'Администратор',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $tester->haveRecord('roles', $data);
    }

    /**
     * @return \Saas\Models\User
     */
    private function createTestUser()
    {
        /** @var \Saas\Models\User $user */
        $user = $this->userService->create($this->generateFakeUserData());
        return $user;
    }

    /**
     * @param bool $unset Unset user Roles and Companies from user.
     * @return array
     */
    private function generateFakeUserData($unset = false)
    {
        $company = $this->haveCompany($this->tester);
        $role = $this->haveRole($this->tester);

        $data = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
            'phone' => $this->faker->phoneNumber,
            'registration_token' => uniqid()
        ];
        if (!$unset) {
            $data['roles'] = [$role];
            $data['companies'] = [$company];
        }
        return $data;
    }

    /**
     * @param UnitTester $tester
     * @param array $data
     * @return mixed
     */
    private function haveCompany(UnitTester $tester, $data = [])
    {
        $data = array_merge([
            'name' => 'test',
            'display_name' => 'Тестовая компания',
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ], $data);
        return $tester->haveRecord('companies', $data);
    }

    /**
     * @param int $repositorySize
     */
    private function generateTestSortData($repositorySize = 3)
    {

        $company = $this->haveCompany($this->tester);
        $role = $this->haveRole($this->tester);

        for ($i = 0; $i < $repositorySize; $i++) {
            $data = [
                'name' => $i,
                'email' => $i . "@gmail.com",
                'phone' => $i . $i,
                'roles' => [$role],
                'companies' => [$company],
            ];
            $user = $this->userService->create($data);
        }
    }

    public function testActivityUser()
    {
        $this->createTestUser();
        $user = \Saas\Models\User::all()->random();
        Auth::loginUsingId($user->id);
        $user = \Saas\Models\User::find($user->id);
        $this->assertNotNull($user->last_activity);

    }

}