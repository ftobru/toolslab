<?php

use Codeception\TestCase\Test;

/**
 * Class RoleServiceTest
 */
class RoleServiceTest extends Test
{
    /** @var \UnitTester*/
    protected $tester;

    /** @var  \Faker\Generator */
    protected $faker;

    /** @var \App\Services\RoleService $roleService */
    protected $roleService;

    /** @var \App\Services\CompanyService $companyService */
    protected $companyService;

    protected function _before()
    {
        $this->faker = Faker\Factory::create('ru_RU');
        $this->roleService = App::make('RoleService');
        $this->companyService = App::make('CompanyService');
    }

    protected function _after()
    {
    }

    // tests

    /**
     * Test getByCompanyId() method.
     */
    public function testGetByCompanyId()
    {
        $company = $this->createTestCompany();
        $collection = $this->roleService->getByCompanyId($company->id);
        $this->assertTrue(($collection instanceof \Illuminate\Support\Collection)||($collection==null));
    }

    // helpers

    /**
     * @return \App\Company
     */
    private function createTestCompany()
    {
        /** @var \App\Company $company */
        $company = $this->companyService->create($this->generateFakeCompanyData());
        return $company;
    }

    /**
     * Fake company data generator/
     *
     * @return array
     */
    private function generateFakeCompanyData()
    {
        $data = [
            'name' => $this->faker->company,
            'display_name' => $this->faker->company,
            'created_at' => new DateTime(),
            'updated_at' => new  DateTime(),
        ];

        return $data;
    }

}