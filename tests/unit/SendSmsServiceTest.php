<?php


class SendSmsServiceTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $phone;
    protected $message;
    /** @var  Faker\Generator */
    protected $faker;

    protected function _before()
    {
        $this->phone = '(983) 122 5325';
        $this->message = 'Test';
        $this->faker = Faker\Factory::create();
    }

    protected function _after()
    {
    }


    public function testSend()
    {
        $this->assertArrayHasKey(
            'id',
                json_decode(SmscSender::send([$this->phone], $this->message . $this->faker->numberBetween(1, 100), true)
            )
        );
    }

}